<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*

| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|    example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|    http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|    $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|    $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
 */

/************************** Unsecure Pages Routing ************************************/

$default_controller = "Shop";
/*$default_controller    = "Shop";*/
$route['404_override'] = '';

$route['translate_uri_dashes'] = false;

/************************** Admin Pages Routing************************************/
$route['admin'] = "admin/login";

$route['customers_edit/(:any)']         = "admin/user/edit/$1";
$route['customers_details/(:any)']      = "admin/user/details/$1";
$route['customers_add']                 = "admin/user/add/";
$route['customers_delete/(:any)']       = "admin/user/delteuser/$1";
$route['customers_orders/(:any)']       = "admin/user/user_orders/$1";
$route['customers_address/(:any)']      = "admin/user/address/$1";
$route['customers_address_book/(:any)'] = "admin/user/list_addresses/$1";
$route['buyer_orders/(:any)']           = "admin/user/buyer_orders/$1";
$route['view_order/(:any)']             = "admin/user/view_order/$1";

/************************** Admin Pages Routing************************************/

/************************** Front Pages Routing************************************/

$route['thank-you']              = "Contact/contact_thanks";
$route['what-is-educki']         = "Contact/what_is_educki";
$route['guide-to-educki/(:any)'] = "GuideToEducki/listing_details/$1";
$route['guide-to-educki']        = "GuideToEducki";
$route['community-guidelines']   = "Contact/community_guidelines";
$route['educki-protect']         = "Educki_protect";
$route['faqs']                   = "faq";
$route['faq-Search']             = "faq/faq_search";
$route['contact']                = "Contact/index";
$route['subscribe-us']           = "Contact/register_subscriber";
$route['storesubscription/(:any)/(:num)']    = "Stripe_payment/subscribe/$1/$2";
$route['changesubscription/(:any)/(:num)']    = "Stripe_payment/changesubscription/$1/$2";
$route['cancelsubscription/(:any)']    = "Stripe_payment/changesubscription/$1";
$route['changetiertofree/(:any)']    = "Stripe_payment/changeTierToFree/$1";
$route['updateplan']    = "Stripe_payment/updatePlan";
$route['change-tier-to-free-by-store-end-date']    = "Stripe_payment/changeTierToFreeByStoreEndDate";
$route['educki-privacy-policy']  = "privacy_policy/index";
$route['educki-terms-of-use']    = "terms/index";
$route['pricing--benefits']      = "pricing_benefits";

$route['check-apply'] = "Career/check_apply";

$route['check-register-email']    = "register/is_unique_register_email";
$route['register-data']           = 'register/register_user';
$route['login-user']              = 'login/login_user';
$route['facebook-login']          = 'login/loginWithFacebook';
$route['logout']                  = "login/logout";
$route['forgot-password']         = "login/forgotPassword";
$route['reset-password/(:any)']   = "login/resetPassword/$1";
$route['retrive-password/(:any)'] = "login/retrivepassword/$1";
$route['Career']                  = "career/index";
$route['career_details/(:any)']   = "career/career_details/$1";
$route['apply_career']            = "career/apply_career";

$route['messages']                          = "messages/index";
$route['messages/(:any)']                   = "messages/index/$1";
$route['delete-conversation/(:any)/(:num)'] = "messages/delete/$1/$2";
$route['delete-chat']                       = "messages/delete_chats";
$route['blog']                              = "Blog/index";
$route['blog/(:num)']                       = "Blog/index/$1";
$route['blog/(:any)']                       = "Blog/category_blog/$1";
$route['blog/(:any)/(:num)']                = "Blog/category_blog/$1/$2";
$route['blog-details/(:any)']               = "blog/blog_details/$1";

$route['create-new-product']  = "products/addNewProduct";
$route['get-product-details'] = "products/getProductDetails";

$route['edit-product/(:any)']              = "products/editProduct/$1";
$route['uploadProdImg']                    = "products/upload_prod_image";
$route['editUploadProdImg']                = "products/edit_upload_prod_image";
$route['editUploadProdImg/(:num)']         = "products/edit_upload_prod_image/$1";
$route['removeProdImg']                    = "products/remove_images";
$route['removeEditProdImg']                = "products/edit_remove_images";
$route['removeEditProdImg/(:any)']         = 'products/edit_remove_images/$1';
$route['removeProdImg/(:any)']             = 'products/remove_images/$1';
$route['get-sub-cat']                      = 'products/get_sub_category';
$route['get-sub-sub-cat']                  = 'products/get_sub_sub_category';
$route['get-req-filter-uqe']               = 'products/get_required_filter_unique';
$route['get-req-filter-uqe-edit/(:any)']   = 'products/get_required_filter_unique_edit/$1';
$route['get-addi-filter-uqe']              = 'products/get_additional_filter_unique';
$route['get-addi-filter-uqe-edit/(:any)']  = 'products/get_additional_filter_unique_edit/$1';
$route['get-sub-conditional-uqe']          = 'products/get_sub_conditional_unique';
$route['get-sub-conditional-req-uqe']      = 'products/get_sub_conditional_req_unique';
$route['get-req-filter-multi']             = "products/get_required_filter_multi";
$route['get-req-filter-multi-edit/(:any)'] = "products/get_required_filter_multi_edit/$1";
$route['get-sub-conditional-req-multi']    = "products/get_sub_conditional_req_multi";
$route['valid-sku-add']                    = "products/valid_sku_add";
$route['valid-sku-edit/(:num)']            = "products/valid_sku_edit/$1";

$route['become-a-seller']      = "seller/index";
$route['store-profile/(:any)'] = "seller/account_details/$1";
$route['edit-store']           = "seller/edit_account_details";
$route['seller-dashboard']     = "dashboard/seller_dashboard";
$route['my-dashboard']         = "dashboard/index";

$route['my-profile'] = "users/edit_profile";

$route['my-address']            = "address/index";
$route['add-address']           = "address/add_address";
$route['edit-address/(:any)']   = "address/edit_address/$1";
$route['delete-address/(:any)'] = "address/delete/$1";

$route['my-wishlist']           = "wishlist/index";
$route['my-wishlist/(:num)']    = "wishlist/index/$1";
$route['my-orders']             = "order/user_orders";
$route['my-orders/(:num)']      = "order/user_orders/$1";
$route['saveShipping']         	= "order/save_shipping";
$route['my-order/(:any)']       = "order/user_order_detail/$1";
$route['seller-fans']           = "fans/index";
$route['seller-fans/(:num)']    = "fans/index/$1";
$route['seller-store-settings'] = "store_settings/index";
$route['store-hide']            = "store_settings/hide_store";

$route['change-password']        = "users/change_password";
$route['check-password']         = "users/check_password";
$route['check-current-password'] = "users/check_current_password";

$route['store/(:any)']        = "Store_details/index/$1";
$route['store/(:any)/(:num)'] = "Store_details/index/$1/$2";
$route['story/(:any)']        = "Store_details/my_story/$1";
$route['register-fan']        = "Store_details/add_fan";

$route['facebook-store'] = "Facebookstore/index";
$route['submit-message'] = "Messages/submitMessage";

$route['manage-products']              = "products/index";
$route['manage-products/index']        = "products/index";
$route['manage-products/index/(:any)'] = "products/index";
$route['delete-product/(:any)']        = "products/product_delete/$1";
$route['product-preview']              = "preview/product_preview";
$route['search_product']               = "products/search_product";

$route['search_product/(:any)']               = "products/search_product/$1";
$route['search_product/(:any)/(:any)']        = "products/search_product/$1/$2";
$route['search_product/(:any)/(:any)/(:any)']        = "products/search_product/$1/$2/$3";
$route['search_product/(:any)/(:any)/(:any)/(:any)']        = "products/search_product/$1/$2/$3/$4";

$route['bluk_product_delete']          = "products/bluk_product_delete";
$route['export_product_csv']           = "products/download_csv";
$route['bluk_product_copy']            = "products/bluk_product_copy";

$route['my-wishlist']               = "wishlist/index";
$route['my-wishlist/(:num)']        = "wishlist/index/$1";
$route['addToWishList']             = "wishlist/add_to_wish_list";
$route['removeWishListItem/(:num)'] = "wishlist/remove_wish_item/$1";
$route['add-product-wishlist']      = "wishlist/add_to_wishlist_shop";

$route['question']           = "seller/ask_a_question";
$route['check_availability'] = "seller/check_availability";
$route['tier_info']          = "seller/tier_info";
$route['create_store']       = "seller/create_store";

$route['favorite-shops/(:any)'] = "seller/favourite_shops/$1";
$route['favorite-shop/(:any)'] = "terms/favourite_shops/$1";
$route['favorite-shops/(:any)/(:num)'] = "seller/favourite_shops/$1/$2";
$route['favorite-shop/(:any)/(:num)'] = "terms/favourite_shops/$1/$2";
$route['sort']                   = "Shop/sort";

$route['about/(:any)']           = "about/index/$1";
$route['delete-favorite-shops'] = "seller/delete_favourite_shops";
$route['delete-store-image']     = "seller/delete_page_img";
$route['tier-pay-paypal']        = "seller/edit_tiers_paypal";

$route['delete_page_img'] = "users/delete_page_img";

/************************** Cart Pages Routing************************************/
$route['cart'] = "cart/index";

$route['addToCart']          = "cart/add_to_cart_ajax";
$route['news-advice']        = "News_Advice/index";
$route['news-advice/(:any)']        = "News_Advice/admin";
$route['removeItems']        = "cart/removeCartItem";
$route['cart-shipping']      = "cart/cart_shipping";
$route['cart-payment']       = "cart/cart_billing_payment";
$route['cart-confirm-order'] = "cart/cart_confirm_payment";
$route['changeitems']        = "cart/changeCartItem";
$route['loadaddress']        = "cart/load_address";
$route['productReport']      = "store_details/product_report";
$route['cart-thank-you']     = "Cart/cart_thanks";
$route['save-order']         = 'Cart/paymentSave';

$route['manage-order']         = "Order/index";
$route['manage-order/(:num)']  = "Order/index/$1";
$route['order-waiting']        = "Order/order_waiting";
$route['order-pickup']         = "Order/order_pickup";
$route['order-pickup/(:num)']         = "Order/order_pickup/$1";
$route['order-waiting/(:num)'] = "Order/order_waiting/$1";
$route['order-history']        = "Order/order_history";
$route['order-history/(:num)'] = "Order/order_history/$1";
$route['order-details/(:num)'] = "Order/order_details/$1";
$route['order-pickup-details/(:num)'] = "Order/order_pickup_details/$1";
$route['order-history-details/(:num)'] = "Order/order_history_details/$1";
$route['search_order']	= "Order/search_order";
$route['search_order/(:any)']	= "Order/search_order/$1";
$route['search_order/(:any)/(:any)']	= "Order/search_order/$1/$2";
$route['search_order/(:any)/(:any)/(:any)']	= "Order/search_order/$1/$2/$3";
$route['search_order/(:any)/(:any)/(:any)/(:any)']        = "Order/search_order/$1/$2/$3/$4";


$route['update-tracking'] = "Order/update_tracking";
$route['confirm-order']   = "Order/confirm_order";
$route['confirm-order-app']   = "Order/confirm_order_app";
$route['print_order/(:num)']     = "Order_print/print_order/$1";
$route['print_my_order/(:num)']     = "Order_print/print_my_order/$1";


$route['smart-search'] = "Main/smart_search";
$route['save-search']  = "Main/save_search";

$route['search_my_order']     		= "Order/search_my_order";
$route['search_my_order/(:any)']    = "Order/search_my_order/$1";
$route['search_my_order/(:any)/(:any)']     	= "Order/search_my_order/$1/$2";
$route['search_my_order/(:any)/(:any)/(:any)']  = "Order/search_my_order/$1/$2/$3";

$route['compare-prods']       = "Main/compare_prods";
$route['clear-compare-list']  = "Main/clear_compare_list";
$route['test-module']         = "Main/testmodule";
$route['compare-details']     = "Main/compare_details";
$route['remove-compare-prod'] = "Main/remove_compare_prod";
$route['compare-prods-2']     = "Main/compare_prods_2";
$route['pay']                 = "Subscription/index";
$route['compare-popup'] = "Main/compare_product_filters";

$route['cancelorder'] = "Order/cancelorder";
$route['reorder'] = "Order/reorder";

$route['userReport']      = "store_details/user_report";

$route['edit-tier']      = "seller/edit_tier";

$route['set-session']      = "Pricing_benefits/set_pandb_session";
$route['destroy-session']      = "Pricing_benefits/unset_pandb_session";
$route['bluk-upload']      = "Products/bulk_file_validate";


$route['jobs-sale-price']      = "Cjobs/removeOnSale";
$route['whichProdNotAddCart']      = "cart/whichProdNotAddCart";

// $route['api/(:any)'] = "Api/$1/$2";
// $route['api/(:any)'] = "Api/$1/$2/$3";


$route['api/(:any)'] = "Api/$1";
$route['app/(:any)/(:any)'] = "App/$1/$2";
$route['app/(:any)/(:any)/(:any)'] = "App/$1/$2/$3";

$route['processing-order'] = "Cart/payOrderAPI";
$route['load-filters'] = "About/load_con_filters";

/************************** Front Pages Routing************************************/

//$controller_exceptions = array('forgot-password','admin','customers_details','customers_add','customers_delete','customers_orders','customers_address','contactfvdfvd','thank-you','what-is-educki','guide-to-educkia','faq','community-guidelines3','educki-protectgbetbtebte1255','educki-privacy-policy','educki-terms-of-use-22','pricing--benefits-dsf-sdf-sdf-sdf-sdf-sdqweqwe','check-register-email','register-data','my-dashboard','logout','facebook-login','login-user','messages','seller');
$controller_exceptions = array('cancelsubscription','change-tier-to-free-by-store-end-date','updateplan','changetiertofree','changesubscription','storesubscription','load-filters','processing-order','app','whichProdNotAddCart','jobs-sale-price','bluk-upload','print_my_order','tier-pay-paypal', 'delete-store-image', 'edit-product', 'uploadProdImg', 'forgot-password', 'admin', 'customers_details', 'customers_add', 'customers_delete', 'customers_orders', 'customers_address', 'contactfvdfvd', 'thank-you', 'what-is-educki', 'guide-to-educkia', 'faq', 'community-guidelines3', 'educki-protectgbetbtebte1255', 'educki-privacy-policy', 'educki-terms-of-use', 'pricing--benefits', 'check-register-email', 'register-data', 'my-dashboard', 'logout', 'facebook-login', 'login-user', 'messages', 'seller', 'my-profile', 'my-address', 'add-address', 'edit-address', 'my-wishlist', 'my-order', 'seller-fans', 'seller-store-settings', 'change-password', 'check-password', 'blog', 'question', 'tier_info', 'check_availability', 'create_store', 'favourite-shops', 'cart', 'addToCart', 'news-advice', 'removeItems', 'cart-shipping', 'cart-payment', 'changeitems', 'loadaddress', 'cart-confirm-order', 'productReport', 'cart-thank-you', 'save-order', 'manage-order', 'Order-waiting', 'order-history', 'order-details', 'search_order', 'bluk_product_copy', 'bluk_product_delete', 'update-tracking', 'save-search', 'search_my_order', 'confirm-order','confirm-order-app', 'print_order', 'compare-prods', 'test-module', 'compare-details', 'remove-compare-prod', 'search_product','submit-message','api','news-advice-admin','compare-popup','delete-chat','delete-conversation','order-pickup-details','order-history-details','cancelorder');

////// Not changeable Start///////////////////
$route['default_controller'] = $default_controller;
//$route["^(".implode('|', $GLOBALS['language_alias']).")/(".implode('|', $controller_exceptions).")(.*)"] = '$2';
//$route["^(".implode('|', $GLOBALS['language_alias']).")?/(.*)"] = $default_controller.'/$2';
$route["^((?!\b" . implode('\b|\b', $controller_exceptions) . "\b).*)$"] = $default_controller . '/$1';
//foreach($GLOBALS['language_alias'] as $language)
//$route[$language] = $default_controller.'/index';

/* End of file routes.php */
/* Location: ./application/config/routes.php */

////// Not changeable Start///////////////////
$route['default_controller'] = $default_controller;
//$route["^(".implode('|', $GLOBALS['language_alias']).")/(".implode('|', $controller_exceptions).")(.*)"] = '$2';
//$route["^(".implode('|', $GLOBALS['language_alias']).")?/(.*)"] = $default_controller.'/$2';
$route["^((?!\b" . implode('\b|\b', $controller_exceptions) . "\b).*)$"] = $default_controller . '/$1';
//foreach($GLOBALS['language_alias'] as $language)
//$route[$language] = $default_controller.'/index';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
