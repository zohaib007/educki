<!doctype html>
<html>
<head>

<!-- Favicon icon ->
<link rel="shortcut icon" href="<?=base_url()?>front_assets/images/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?=base_url()?>front_assets/images/favicon.ico" type="image/x-icon">
<link rel="icon" type="image/png" href="<?=base_url()?>front_assets/images/favicon.png">
<!-- Favicon icon -->

<meta charset="UTF-8">
<meta name="title" content="eDucki | Marketplace of individual sellers/parents of kids clothes, shoes, toys, book, & more kids items">
<meta name="description" content="eDucki | Marketplace of individual sellers/parents of kids clothes, shoes, toys, book, & more kids items">
<meta name="keywords" content="eDucki | Marketplace of individual sellers/parents of kids clothes, shoes, toys, book, & more kids items">
<meta name="author" content="Dotlogics">

<meta name="viewport" content="width=device-width">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Welcome to eDucki</title>
<!--css starts here-->
<link href="<?=base_url()?>front_assets/css/e_ducki.css" rel="stylesheet" type="text/css"/>
<link href="<?=base_url()?>front_assets/fonts/web_fonts/fonts.css" rel="stylesheet" type="text/css"/>
<!--css ends here-->

<!-- FOR RESPONSIVE STARTS -->
<!-- Google Chrome Frame for IE -->
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge" /> -->
<!-- mobile meta (hooray!) -->
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" /> 
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no" />
<!--
<link href="<?=base_url()?>front_assets/css/responsive-css.css" rel="stylesheet" type="text/css" media="projection,screen" />
-->
<!-- FOR RESPONSIVE ENDS -->

</head>

<body>

	<div class="ed_container">
		
        <div class="wrapper">
       		
            <div class="ed-cvr">
            
            	<table cellpadding="0" cellspacing="0" border="0">
                	<tr>
                    	<td>
                            <div class="ed_coming_soon">
                                
                                <h1>eDucki</h1>
                                <h3>
                                    Coming Soon! <br/>
                                    February - 2018
                                </h3>
                                
                                <p>	
                                	Find your community! Be the first to experience an awesome
                                    new online market for all your kid’s needs. Parents will be
                                    able to shop, sell, trade, and donate everything kids! 
            						<br/><br/>
                                    <span>eDucki </span>is a place where parents can go to give and get advice,
                                    read articles, and save money all at the same time.
                                </p>
                                <?php echo form_open('',array('name'=>'subscribe_me')); ?>
                                <div class="ed-search">
                                	<input type="text" value="<?=set_value('sub_email')?>" name="sub_email" placeholder="Enter Your Email"/>
                                    <input type="submit" value="Notify Me"/>
                                	<div class="ed-error"><?=str_replace('<p>','',str_replace('</p>','',form_error('sub_email')))?></div>
                                    <?php if($this->session->flashdata('sendmailOk') != '') {
										echo '<div class="ed-error">Thank you for subscribing!</div>';
									}
									?>
                                </div>
                                <?php echo form_close(); ?>
                                <div class="ed-social">
                                    <a target="_blank" href="https://www.facebook.com/eDucki/"><img src="<?=base_url()?>front_assets/images/ed-fb.png" alt="Facebook" /></a>
                                    <a target="_blank" href="https://twitter.com/eDuckiCommunity"><img src="<?=base_url()?>front_assets/images/ed-twtr.png" alt="Twitter" /></a>
                                    <a target="_blank" href="https://www.instagram.com/educkicommunity/"><img src="<?=base_url()?>front_assets/images/ed-insta.png" alt="Instagram" /></a>
                                    <a target="_blank" href="https://it.linkedin.com/company/educki"><img src="<?=base_url()?>front_assets/images/ed-in.png" alt="Linkedin" /></a>
                                </div>
                                
                            
                            </div>	
            			</td>
                	</tr>
              	</table>
            
            </div>
        
        </div>   		     
    
    </div>


</body>
</html>