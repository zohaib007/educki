<?php
class Api extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('common/common_model');
        $this->load->helper(array('form', 'url'));
        $this->load->model('front/guidetoeducki/Guidetoeducki_model');
        $this->load->model('front/faq/faq_model');
        $this->load->model('front/contact_model');
        $this->load->model('front/career/career_model');
        $this->load->model('search_model');
        $this->load->model('front/cart/cart_model');
        $this->load->model('front/dashboard/dashboard_model');
        $this->load->model('Message_model');
        $this->load->model('front/address/address_model');
        $this->load->model('front/order/order_model');
        $this->load->model('front/users/users_model');
        $this->load->model('front/seller/seller_model');
        $this->load->model('front/shop/shop_model');
        $this->load->library('upload');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        date_default_timezone_set('America/New_York');
    }

    public function index($value = '')
    {
        echo "Hello";
    }

    public function register_subscriber()
    {
        $data['result']      = 0;
        $datadb['sub_email'] = $this->input->get('email');
        $check['is_exist']   = $this->common_model->commonSelect2('tbl_subscriber', 'sub_email', $datadb['sub_email'], 'sub_is_delete', 0)->row();
        if (count($check['is_exist']) <= 0) {
            $datadb['sub_created_date'] = date('Y-m-d h:i:s');
            $result = $this->common_model->commonSave('tbl_subscriber', $datadb);
            if($result){
                $is_send = send_email("mikesmith1166@gmail.com", "no-reply@educki.com", "Subscriptions", $datadb['sub_email']." has Subscribed");
            }

            if($is_send){
                $data['status'] = 200;
                $data['message'] = "Thank you for subscribing with us!";
            }            
        } else {
            $data['status'] = 201;
            $data['message'] = "You are already subscribed!";
        }

        echo json_encode($data);
    }

    public function home_page()
    {
        $data['title']       = "eDucki";
        $data['home_data']   = $this->common_model->commonselect('tbl_homepage', 'home_id', 1)->row();
        $data['home_banner'] = $this->common_model->getCombox('tbl_homepage_banner', 'banner_id', 'ASC')->result();
        $data['home_member'] = $this->common_model->getCombox('tbl_homepage_member', 'mem_id', 'ASC')->result();
        $data['get_started'] = "<div class=\"cate-head2\"><h2>" . $data['home_data']->gettingStartTitle . "</h2></div>";
        for ($i = 0; $i < 3; $i++) {
            $nameimg   = 'start_image' . ($i + 1);
            $nametitle = 'startTitle' . ($i + 1);
            if ($i != 2) {
                $data['get_started'] .= "<div class=\"get-blk\"><img src=\"" . base_url() . "resources/home_image/" . $data['home_data']->$nameimg . "\" alt=\"step 1\">" . $data['home_data']->$nametitle . "<span class=\"btm-arow\"><img src=\"images/downward-arrow.png\"/></span></div>";
            } else {
                $data['get_started'] .= "<div class=\"get-blk\"><img src=\"" . base_url() . "resources/home_image/" . $data['home_data']->$nameimg . "\" alt=\"step 1\">" . $data['home_data']->$nametitle . "</div>";
            }
        }

        $data['get_started'] .= "<div class=\"join-btn logged_in\"><a href=\"register.html\">Join Now</a></div>";
        $query = "
                SELECT cat_url,cat_image, cat_name
                FROM tbl_categories
                WHERE cat_level = 1 AND cat_is_delete = 0 AND cat_status = 1
                ";
        $data['home_category'] = $this->db->query($query)->result();

        $data['categories'] = "<div class='cate-head'><h2>" . $data['home_data']->category_title . "</h2></div><div class='slider-con'><div class='wrapper'><div class='responsive slider'>";
        foreach ($data['home_category'] as $cat) {
            $data['categories'] .= '<div><a href="javascript:void" class="pro-name cat_main_click" data-name="'.$cat->cat_name.'" data-url="'.$cat->cat_url.'"><img src="'.base_url()."resources/cat_image/".$cat->cat_image.'">'.$cat->cat_name.'</a></div>';
        }
        $data['categories'] .= "</div></div></div>";

        if (count($data['home_banner']) <= 0) {
            $data['banner'] = "";
        } else {
            $data['banner'] = "";
            foreach ($data['home_banner'] as $banner) {
                $data['banner'] .= "<div class='banner' style='background-image: url(" . base_url() . "resources/home_image/" . $banner->banner_image . ");'><div class='wrapper'><table><tr><td><div class='bnnr-txt'><h2>Destination for Clothes & Kid’s Accessories</h2><h3>Buy  •  Sell  •  Trade  •  Donate</h3></div></td></tr></table></div></div>";
            }
            $data['categories'] .= "</div></div></div>";
        }

        $query = "
                SELECT store.store_logo_image, store.store_url, store.store_name
                FROM tbl_stores store
                INNER JOIN tbl_user user ON store.user_id = user.user_id
                WHERE
                store_is_hide = 0 AND user.user_is_delete = 0 AND user.user_status = 1 AND user.user_is_featured = 1
                ";

        $data['featured_sellers'] = $this->db->query($query)->result();

        $data['sellers'] = "<div class='cate-head'><h2>" . $data['home_data']->feaSeller . "</h2></div><div class='slider-con'><div class='wrapper'><div class='responsive slider'>";
        foreach ($data['featured_sellers'] as $seller) {
            $data['sellers'] .= "<div><a href='javascript:void' id='open-store' class='pro-name' data-id='".$seller->store_id."'><img src='" . base_url() . "resources/seller_account_image/logo/$seller->store_logo_image'>" . $seller->store_name . "</a></div>";
        }
        $data['sellers'] .= "</div></div></div>";

        $query = "
                SELECT store.store_longitude,store.store_latitude,store.store_id,store.store_url,store.store_longitude,store.store_latitude
                FROM tbl_stores store
                INNER JOIN tbl_user user ON store.user_id = user.user_id
                WHERE
                store_is_hide = 0 AND user.user_is_delete = 0 AND user.user_status = 1
                ";
        $data['home_store'] = $this->db->query($query)->result();

        $data['total_stores'] = count($data['home_store']);

        $data['stores_data'] = '{ "stores": [';
        foreach($data['home_store'] as $i => $store) {
            if($store->store_longitude!=''&& $store->store_latitude!=''){
                $data['stores_data'] .= '{"store_id":'.$store->store_id.',"store_url": "'.$store->store_id.'", "longitude":'.$store->store_longitude.',"latitude":'.$store->store_latitude.'}'; 
                if($i<count($data['home_store'])-1){
                    $data['stores_data'] .= ",";
                }
            }
        }
        $data['stores_data'] .= ']}';

        $data['stores_data'] = str_replace(',]}',']}',$data['stores_data']);

        /*echo "<pre>";
        print_r($data);
        exit;*/

        echo json_encode($data);
    }

    public function privcay_policy()
    {
        $data['pagedata'] = $this->common_model->commonselect('tbl_pages', 'pg_id', 8)->result();
        echo json_encode($data);
    }

    public function terms_of_service()
    {
        $data['pagedata'] = $this->common_model->commonselect('tbl_pages', 'pg_id', 9)->result();
        echo json_encode($data);
    }

    public function blogs()
    {
        $data['blog_category'] = $this->common_model->commonselect('tbl_blog_category', 'blog_cat_status', 1)->result();
        $data['blog_cat']      = "";
        foreach ($data['blog_category'] as $cat) {
            $data['blog_cat'] .= "<li><a href='javascript:void' class='cat-blogs $cat->blog_cat_url' data-url='" . $cat->blog_cat_url . "'>" . $cat->blog_cat_name . "</a></li>";
        }
        echo json_encode($data);
    }

    public function load_blogs()
    {
        $url = $this->input->get('url');

        $data['url'] = $url;

        $this->db->select('*');
        $this->db->from('tbl_blog');
        $this->db->join('tbl_blog_category', 'tbl_blog_category.blog_cat_id = tbl_blog.blog_category_id');
        $this->db->where("tbl_blog_category.blog_cat_status", 1);
        if ($url != '') {
            $this->db->where("tbl_blog_category.blog_cat_url", $url);
        }
        $this->db->where("blog_status", 1);
        $query               = $this->db->get();
        $data['total_blogs'] = count($query->result());

        $counter = $this->input->get('counter') * 10;

        $this->db->select('*');
        $this->db->from('tbl_blog');
        $this->db->join('tbl_blog_category', 'tbl_blog_category.blog_cat_id = tbl_blog.blog_category_id');
        $this->db->where("tbl_blog_category.blog_cat_status", 1);
        $this->db->where("blog_status", 1);
        if ($url != '') {
            $this->db->where("tbl_blog_category.blog_cat_url", $url);
        }
        $this->db->limit(10, $counter);
        $this->db->order_by("blog_id", "desc");
        $query         = $this->db->get();
        $data['blogs'] = $query->result();

        $data['blogs_html'] = "";
        foreach ($data['blogs'] as $i => $blog) {
            $class='';
            if($i == count($data['blogs'])-1){
                $class = "lst-blg";
            }else{
                $class = "";
            }

            $blog_url = '"' . $blog->blog_url . '"';
            $data['blogs_html'] .= "<div class='blog-list-bx ".$class."'><div class='blog-list-inner pad-tp-0'><div class='blog-lst-wrp'><div class='blog-list-tit'><h2>" . $blog->blog_name . "</h2></div><div class='blog-list-date'><p>By <span>" . $blog->blog_author . "</span> on " . date('F d, Y', strtotime($blog->blog_created_date)) . "</p></div></div><div class='blog-lst-img'><img src='" . base_url() . "resources/blog_image/" . $blog->blog_image . "' alt='' title='' class='img-responsive'></div><div class='blog-lst-wrp2'><div class='blog-list-txt'><p>" . limited_text(clean_string($blog->blog_description), 200) . "</p></div><div class='blog-lst-btn'><a href='javascript:void' class='blog-details' onclick='setBlogIdUrl(" . $blog->blog_id . "," . $blog_url . ")'  data-url='" . $blog->blog_url . "'>Read <span>&gt;</span> </a></div></div></div></div>";
        }

        if (count($data['blogs']) == 0) {
            $data['no-data'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No data found.</div>";
        }

        $data['counter'] = $counter + 10;

        echo json_encode($data);
    }

    public function load_blog_details()
    {

        $data['blog_category'] = $this->common_model->commonselect('tbl_blog_category', 'blog_cat_status', 1)->result();
        $data['blog_cat']      = "";
        foreach ($data['blog_category'] as $cat) {
            $data['blog_cat'] .= "<li><a href='javascript:void' class='cat-blogs $cat->blog_cat_url' data-url='" . $cat->blog_cat_url . "'>" . $cat->blog_cat_name . "</a></li>";
        }

        $url = $this->input->get('url');

        $this->db->select('*');
        $this->db->from('tbl_blog');
        $this->db->join('tbl_blog_category', 'tbl_blog_category.blog_cat_id = tbl_blog.blog_category_id');
        $this->db->where("tbl_blog_category.blog_cat_status", 1);
        $this->db->where("tbl_blog.blog_url", $url);
        $this->db->where("blog_status", 1);
        $query                = $this->db->get();
        $data['blog_details'] = $query->row();

        $data['curCat'] = $data['blog_details']->blog_cat_url;

        $data['blogs_html'] .= "
        <div class='blog-list-bx'>
            <div class='blog-list-inner pad-tp-0'>
                <div class='blog-lst-wrp'>
                    <div class='blog-list-tit'>
                        <h2>" . $data['blog_details']->blog_name . "</h2>
                    </div>
                    <div class='blog-list-date'>
                        <p>By <span>" . $data['blog_details']->blog_author . "</span> on " . date('F d, Y', strtotime($data['blog_details']->blog_created_date)) . "</p>
                    </div>
                </div>
                <div class='blog-lst-img'>
                    <img src='" . base_url() . "resources/blog_image/" . $data['blog_details']->blog_image . "' alt='' title='' class='img-responsive'>
                </div>
                <div class='blog-lst-wrp2'>
                    <div class='blog-list-txt'>
                        <p>" . clean_string($data['blog_details']->blog_description) . "</p>
                    </div>
                </div>
            </div>
        </div>

        ";

        $data['page_url'] = base_url('blog-details/' . $data['blog_details']->blog_url);
        $data['page_id']  = $data['blog_details']->blog_id;

        echo json_encode($data);
    }

    public function pricing_benefits()
    {
        $data['pagedata']   = $this->common_model->commonselect('tbl_pricing_benefits', 'p_id', 1)->row();
        $data['tierdata']   = $this->common_model->getCombox('tbl_tier_list')->result();
        $data['base_value'] = $data['tierdata'][1]->tier_amount;

        $data['html'] = "";
        $data['html'] .= "<div class='banfit-low-bx'><table><tbody><tr><td><img src='" . base_url() . "resources/pricing_benefits/" . $data['pagedata']->image_1 . "' alt='' title=''></td></tr></tbody></table><p>" . $data['pagedata']->img_description_1 . "</p></div>";
        $data['html'] .= "<div class='banfit-low-bx'><table><tbody><tr><td><img src='" . base_url() . "resources/pricing_benefits/" . $data['pagedata']->image_2 . "' alt='' title=''></td></tr></tbody></table><p>" . $data['pagedata']->img_description_2 . "</p></div>";
        $data['html'] .= "<div class='banfit-low-bx mrg-rgt-0'><table><tbody><tr><td><img src='" . base_url() . "resources/pricing_benefits/" . $data['pagedata']->image_3 . "' alt='' title=''></td></tr></tbody></table><p>" . $data['pagedata']->img_description_3 . "</p></div>";
        $data['plans'] = "";
        foreach ($data['tierdata'] as $i => $tier) {
            $pickup   = '';
            $months   = '';
            $fb_store = '';
            $amount   = '';
            if ($tier->is_pickup != 0) {
                $pickup = 'Local Pick Up Option';
            }
            if ($tier->is_fb_store != 0) {
                $fb_store = 'Facebook Store';
            }
            if ($tier->tier_months != 0) {
                $months = $tier->tier_months . ' Months';
            }

            if ($tier->tier_amount != 0) {
                $amount = $tier->tier_amount;
            } else {
                $amount = 'Free';
            }
            $class_1 = '';
            if ($i != 1) {
                $class_1 = 'plan-free border-lft';
            } else {
                $class_1 = 'plan-free';
            }
            $class_2 = '';
            if ($i == 0) {
                $class_2 = 'plan-free-hed';
            } elseif ($i == 1) {
                $class_2 = 'plan-free-bacs';
            } elseif ($i == 2) {
                $class_2 = 'plan-free-pro';
            } elseif ($i == 3) {
                $class_2 = 'plan-free-delx';
            }

            $data['plans'] .= "<div class='" . $class_1 . "'><div class='" . $class_2 . "'><table><tbody><tr><td><h3>" . $tier->tier_name . "</h3></td></tr></tbody></table></div><div class='plan-free-feature'><h4>" . $tier->tier_name . "</h4></div><div class='transc-fee-sec'><p>" . $tier->tier_fee . "% Transaction Fee</br>" . $pickup . "</br>" . $fb_store . "</br>" . $months . "</p><div class='plan-amount'><h5>$" . $amount . "/month</h5></div><div class='plan-lst'><a href='javascript:void' class='pricing_click' data-id='".$tier->tier_id."'>Sign up!</a></div></div></div>";
        }

        echo json_encode($data);
    }

    public function about_us()
    {
        $query = "SELECT *
                FROM tbl_how_work_page";
        $data['what_is_educki'] = $this->db->query($query)->row();

        $query = "SELECT *
                FROM tbl_pages_sub subpages
                WHERE subpages.sub_pg_status = 1 AND subpages.sub_is_delete = 0 AND subpages.sub_pg_id = 4
                ORDER BY side_menu_order ASC";
        $data['menu'] = $this->db->query($query)->result();

        $total_menu = count($data['menu']);

        $data['html'] = "";
        foreach ($data['menu'] as $i => $menu) {
            $calss = '';
            if ($i == $total_menu - 1) {
                $class = 'last-acc';
            } else {
                $class = '';
            }

            $file = '';
            if ($menu->sub_id == 5) {
                $file = 'what-is-eDucki.html';
                $data['html'] .= "<div class='accordion_head " . $class . "'><a href='" . $file . "'><span class='plusminus'></span>" . $menu->sub_pg_title . "</a></div>";
            } elseif ($menu->sub_id == 10) {
                $file = 'guide-to-eDucki.html';
                $data['html'] .= "<div class='accordion_head " . $class . "'><a href='" . $file . "'><span class='plusminus'></span>" . $menu->sub_pg_title . "</a></div>";
            } elseif ($menu->sub_id == 9) {
                $file = 'community-guidelines.html';
                $data['html'] .= "<div class='accordion_head " . $class . "'><a href='" . $file . "'><span class='plusminus'></span>" . $menu->sub_pg_title . "</a></div>";
            } elseif ($menu->sub_id == 14) {
                $file = 'eDucki-protect.html';
                $data['html'] .= "<div class='accordion_head " . $class . "'><a href='" . $file . "'><span class='plusminus'></span>" . $menu->sub_pg_title . "</a></div>";
            } elseif ($menu->sub_id == 6) {
                $file = 'faq.html';
                $data['html'] .= "<div class='accordion_head " . $class . "'><a href='" . $file . "'><span class='plusminus'></span>" . $menu->sub_pg_title . "</a></div>";
            } elseif ($menu->sub_id == 15) {
                $file = 'contact.html';
                $data['html'] .= "<div class='accordion_head " . $class . "'><a href='" . $file . "'><span class='plusminus'></span>" . $menu->sub_pg_title . "</a></div>";
            } else {
                $file = $menu->sub_id;
                $data['html'] .= "<div class='accordion_head " . $class . "'><a href='javascript:void' data-id='".$file."' data-page='new-about-page.html' class='gte-listing'><span class='plusminus'></span>" . $menu->sub_pg_title . "</a></div>";
            }
        }

        echo json_encode($data);
    }

    public function what_is_educki()
    {
        $data['pagedata'] = $this->common_model->commonselect('tbl_how_work_page', 'work_id', 1)->row();
        echo json_encode($data);
    }

    public function guide_to_educki()
    {
        $data['pagedata'] = $this->common_model->commonselect('tbl_pages_sub', 'sub_id', 10)->row();
        $data['list']     = $this->Guidetoeducki_model->view_list(10);

        $data['html'] = '';
        foreach ($data['list'] as $i => $list) {
            $file = '';
            if ($list['listing_id'] == 7) {
                $file = 'get-started.html';
                $data['html'] .= "<a href='" . $file . "' class='guide-link'><img src='" . base_url() . "resources/page_image/listing/banner/" . $list['lisintg_image'] . "' alt=''/><h3>" . $list['listing_title'] . "</h3></a>";
            } else {
                $file = $list['listing_id'];
                $data['html'] .= "<a href='javascript:void' data-id='".$file."' data-page='other-listing.html' class='guide-link guide-list'><img src='" . base_url() . "resources/page_image/listing/banner/" . $list['lisintg_image'] . "' alt=''/><h3>" . $list['listing_title'] . "</h3></a>";
            }
        }

        $data['html'] .= "<div class='need-assistence'>Still need assistance? Check out our <a href='contact.html'>Support Center.</a></div>";

        echo json_encode($data);
    }

    public function get_started_list()
    {
        $data['pagedata'] = $this->common_model->commonselect('tbl_pages_listing', 'listing_id', 7)->row();
        $data['list']     = $this->common_model->commonselect('tbl_pages_sub_listing', 'list_listing_id', 7)->result();

        $data['html'] = '';
        foreach ($data['list'] as $i => $list) {
            $data['html'] .= "<div class='get-started-blk'><img src='" . base_url() . "resources/page_image/sub_listing/" . $list->lisintg_image . "'><p>" . clean_string($list->listing_description) . "</p></div>";
        }

        if ($data['pagedata']->listing_description != '') {
            $data['html'] .= clean_string($data['pagedata']->listing_description);
        }

        $data['html'] .= "<div class='need-assistence'>Still need assistance? Check out our <a href='contact.html'>Support Center.</a></div>";

        echo json_encode($data);
    }

    public function other_listing()
    {
        $id               = $this->input->get('id');
        $data['pagedata'] = $this->common_model->commonselect('tbl_pages_listing', 'listing_id', $id)->row();
        $data['list']     = $this->common_model->commonselect('tbl_pages_sub_listing', 'list_listing_id', $id)->result();

        $count        = count($data['list']);
        $data['html'] = '';
        foreach ($data['list'] as $i => $list) {
            $class = '';
            if ($i == $count - 1) {
                $class = 'dotted';
            } else {
                $class = '';
            }

            $data['html'] .= "<div class='ed-work-blk " . $class . "'><img src='" . base_url() . "resources/page_image/sub_listing/" . $list->lisintg_image . "'><p>" . clean_string($list->listing_description) . "</p></div>";
        }

        if ($data['pagedata']->listing_description != '') {
            $data['html'] .= "<p>" . clean_string($data['pagedata']->listing_description) . "</p>";
        }

        $data['html'] .= "<div class='need-assistence'>Still need assistance? Check out our <a href='contact.html'>Support Center.</a></div>";

        echo json_encode($data);
    }

    public function community()
    {
        $data['pagedata'] = $this->common_model->commonselect('tbl_pages_sub', 'sub_id', 9)->row();
        echo json_encode($data);
    }

    public function protect()
    {
        $data['pagedata'] = $this->common_model->commonselect('tbl_pages_sub', 'sub_id', 14)->row();
        echo json_encode($data);
    }

    public function faq()
    {
        $search           = $this->input->get('search');
        $data['pagedata'] = $this->common_model->commonselect('tbl_pages_sub', 'sub_id', 6)->row();
        $data['list']     = $this->faq_model->view_questions(6, $search);

        $data['html'] = '';
        foreach ($data['list'] as $i => $list) {
            $data['html'] .= "<div class='accordion_head'><h3>" . $list['sub_sub_pg_title'] . "</h3><span class='plusminus'>+</span></div><div class='accordion_body' style='display: none;'><p>" . clean_string($list['sub_sub_pg_description']) . "</p></div>";
        }

        if (count($data['list']) == 0) {
            $data['html'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';float:left;width:100%;font-weight: normal;color: #000;text-align: center;\">No data found.</div>";
        }

        echo json_encode($data);
    }

    public function about_new()
    {
        $id               = $this->input->get('id');
        $data['pagedata'] = $this->common_model->commonselect('tbl_pages_sub', 'sub_id', $id)->row();
        echo json_encode($data);
    }

    public function contact()
    {
        $data['pagedata'] = $this->common_model->commonselect('tbl_pages_sub', 'sub_id', 15)->row();
        echo json_encode($data);
    }

    public function send_contact_form()
    {
        $dataDB['fname']        = stripcslashes($this->input->post('name'));
        $dataDB['subject']      = stripcslashes($this->input->post('subject'));
        $dataDB['email']        = stripcslashes($this->input->post('email'));
        $dataDB['comment']      = stripcslashes($this->input->post('message'));
        $dataDB['date_created'] = date('Y-m-d h:i:s');

        if ($this->input->post('category') != "") {
            $dataDB['cat_id']   = stripcslashes($this->input->post('category'));
            $dataDB['cat_name'] = stripcslashes($this->input->post('cat_name'));
        }

        $File1      = $_FILES['fileupload'];
        $field_name = 'fileupload';
        $result     = $this->file_upload($File1, $field_name);

        if ($result) {
            $dataDB['filename'] = $result;
            $file               = base_url() . "resources/contact_form_file/" . $dataDB['filename'];
        } else {
            $file = '';
        }

        $message = "<b>Name :</b>" . $dataDB['fname'] . "<br>";
        $message .= "<b>Email :</b> " . $dataDB['email'] . "<br>";
        if ($dataDB['subject'] != '') {
            $message .= "<b>Subject :</b> " . $dataDB['subject'] . "<br>";
        } else {
            $message .= "<b>Subject :</b> N/A" . "<br>";
        }
        if ($this->input->post('category') != "") {
            $message .= "<b>Category :</b> " . $dataDB['cat_name'] . "<br>";
        } else {
            $message .= "<b>Category :</b> N/A" . "<br>";
        }
        $message .= "<b>Message :</b> " . $dataDB['comment'] . "<br>";
        /*$to   = "mikesmith1166@gmail.com";
        $from = $dataDB['email'];

        send_email_2($to, $from, 'Contact form inquiry', $message, $file);*/

        //sending mail to user

        $email_data           = $this->common_model->commonselect('tbl_email_temp', 'e_email_slug', 'contact-us')->row();
        $emailData['subject'] = $email_data->e_email_subject;
        $message              = $email_data->e_email_text;
        $message              = str_replace("{{username}}", $dataDB['fname'], $message);
        $emailData['message'] = $message;

        send_email_2($dataDB['email'], "mikesmith1166@gmail.com", $emailData['subject'], $message, $file);
        //sending mail to user
        $this->contact_model->add_contact_form($dataDB);
    }

    private function file_upload($Files1, $field_name)
    {

        $filename = array();
        if ($Files1['name'] !== "") {
            $config = array(
                'allowed_types' => 'pdf|doc|docx|ppt|txt|jpg|jpeg|png', //doc|docx|ppt|ppt|txt|
                'upload_path'   => FCPATH . 'resources/contact_form_files/',
                'file_name'     => 'file_' . date('Y_m_d_h_i_s'),
            );
            $this->load->library('upload');
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($field_name)) {

                //Print message if file is not uploaded
                return array($field_name . '_error' => $this->upload->display_errors());
                //return false;
            } else {
                $dataDP          = $this->upload->data();
                return $filename = $dataDP['file_name'];
            }
        }
    }

    public function login_user()
    {
        $email    = $this->input->post('email');
        $password = $this->input->post('password');
        //$check    = $this->input->post('check');
        $regData = $this->db->query("
                            SELECT *
                            FROM tbl_user
                            WHERE user_email ='" . $email . "'
                            AND user_password ='" . $password . "'
                            AND user_status = 1 AND user_is_delete = 0
                        ")->row();

        /*echo "<pre>";
        print_r($_POST);
        print_r($regData);
        exit;*/

        if (!empty($regData)) {            
            $data['result']    = '200';
            $data['userfname'] = $regData->user_fname;
            $data['user_id']   = $regData->user_id;
            $data['userlname'] = $regData->user_lname;
            $data['useremail'] = $regData->user_email;
            $data['usertype']  = $regData->user_type;

            $data['cartId'] = $this->input->get('_cartId');

            $sessiondata = array(
                'userid'    => $regData->user_id,
                'useremail' => $regData->user_email,
                'userfname' => $regData->user_fname,
                'userlname' => $regData->user_lname,
                'cartId'    => $cartId,
                'usertype' => $regData->user_type,
                'logged_in' => true,
            );


            $this->session->set_userdata($sessiondata);

            $loginData['login_user_id']    = $regData->user_id;
            $loginData['login_date']       = date('Y-m-d H:i:s');
            $lastlogin['user_last_active'] = date('Y-m-d H:i:s');
            $this->common_model->commonSave('tbl_user_login', $loginData);
            $this->common_model->commonUpdate('tbl_user', $lastlogin, 'user_id', $regData->user_id);

            $result = $this->common_model->commonSelect("tbl_cart", "cart_user_id", $regData->user_id)->row();
            $cartId = '';
            if(!empty($result)){
                $is_prod =  $this->common_model->commonSelect("tbl_cart_detail", "cart_id", $result->cart_id)->row();
                if(!empty($is_prod)){
                    $data['cartId'] = $cartId = $result->cart_id;
                }else{
                    $this->cart_model->removeCart($result->cart_id);
                    $cartId = $this->input->get('_cartId');
                    if($cartId != ''){
                        $addUser['cart_user_id'] = $regData->user_id;
                        $this->db->where('cart_id', $cartId);
                        $this->db->update('tbl_cart', $addUser);
                    }
                }
            }else{
                $cartId = $this->input->get('_cartId');
                if($cartId != '')
                {
                    $addUser['cart_user_id'] = $regData->user_id;
                    $this->db->where('cart_id', $cartId);
                    $this->db->update('tbl_cart', $addUser);
                }
            }

            $expires = time() + (5400000);
            if($cartId != ''){
                setcookie('educki_cartId', $this->input->get('_cartId'), $expires, '/');
            }
            setcookie('educki_userId', $regData->user_id, $expires, '/');


        } else {
            $data['result'] = '202';
        }

        echo json_encode($data);
    }

    public function get_cart_data()
    {
        $user_id    = $this->input->get('user_id');
        $result = $this->common_model->commonSelect("tbl_cart","cart_user_id",$user_id)->row();
       /* echo '<pre>';
        echo $this->db->last_query();
        print_r($user_id);
        exit;*/
        $cartId = $result->cart_id;
        $data['cart_count'] = $this->totalCartItems($cartId);
        $data['cart_id']  = $cartId;

        echo json_encode($data);
    }

    function totalCartItems($cart_id){

        $data['cart_product_details'] = $this->getProductPrices($cart_id);
        
        foreach ($data['cart_product_details'] as $product) {
            $product_details = getproduct_details_2($product->cart_prod_id);
            
            if ($product_details == '') {
                $this->cart_model->removeCartItem($cart_id, $product->cart_prod_id, $product->cart_detail_id);
            }
        }

        return $this->db->query("SELECT SUM(cart_prod_qnty) AS total_cart_items FROM tbl_cart_detail cd
                JOIN tbl_products p ON cd.cart_prod_id = p.prod_id
                JOIN tbl_cart c ON cd.cart_id = c.cart_id
                WHERE c.cart_id = '" . $cart_id . "' ")->row('total_cart_items');
    }

    function getProductPrices($cartId) {
        return $this->db->query("SELECT cd.cart_prod_id,cd.cart_detail_id,cd.cart_details,cd.cart_id, cd.cart_prod_qnty, 
                    (
                    CASE
                     WHEN p.prod_onsale = 1 && p.sale_start_date <= '" . date('Y-m-d') . "' && p.sale_end_date >= '" . date('Y-m-d') . "' THEN p.sale_price
                     WHEN p.prod_onsale = 1 && p.sale_start_date = '' && p.sale_end_date = '' THEN p.sale_price
                     ELSE p.prod_price
                    END
                    ) AS unit_prod_price,
                    (
                    CASE
                     WHEN p.prod_onsale = 1 && p.sale_start_date <= '" . date('Y-m-d') . "' && p.sale_end_date >= '" . date('Y-m-d') . "' THEN p.sale_price * cd.cart_prod_qnty
                     WHEN p.prod_onsale = 1 && p.sale_start_date = '' && p.sale_end_date = '' THEN p.sale_price * cd.cart_prod_qnty
                     ELSE p.prod_price * cd.cart_prod_qnty
                    END
                    ) AS total_prod_price  FROM tbl_cart_detail cd
                    JOIN tbl_products p ON cd.cart_prod_id = p.prod_id
                    JOIN tbl_cart c ON cd.cart_id = c.cart_id
                    WHERE c.cart_id = '" . $cartId . "' ")->result();
    }

    public function careers()
    {
        $count_where       = 'WHERE career_is_deleted = 0 AND career_is_active = 1';
        $where             = 'career_is_deleted = 0 AND career_is_active = 1';
        $jointable         = 'tbl_states';
        $joincondition     = 'tbl_states.stat_id = tbl_career.career_state';
        $jointype          = 'inner';
        $orderby           = 'career_id';
        $order             = 'desc';
        $orderby           = 'career_id';
        $data['list']      = $this->career_model->career_applied_user_listing("tbl_career", $where, $jointable, $joincondition, $jointype, '', '', $is_count = 0, $orderby, $order = 'DESC')->result();
        $data['page_data'] = $this->common_model->commonselect('tbl_career_page', 'carrier_page_id', 1)->row();
        $data['html']      = "<div class='crr-blk-img'><img src='" . base_url() . "resources/career_image/career_page/" . $data['page_data']->career_page_image_first . "' alt=''/><h4>" . $data['page_data']->career_page_title_first . "</h4><p>" . clean_string($data['page_data']->career_page_description_first). "</p></div>";
        $data['html'] .= "<div class='crr-blk-img'><img src='" . base_url() . "resources/career_image/career_page/" . $data['page_data']->career_page_image_second . "' alt=''/><h4>" . $data['page_data']->career_page_title_second . "</h4><p>" . clean_string($data['page_data']->career_page_description_second) . "</p></div>";
        $data['html'] .= "<div class='crr-blk-img'><img src='" . base_url() . "resources/career_image/career_page/" . $data['page_data']->career_page_image_third . "' alt=''/><h4>" . $data['page_data']->career_page_title_third . "</h4><p>" . clean_string($data['page_data']->career_page_description_third) . "</p></div>";
        $data['html'] .= '<h3>The Perks</h3>';
        $data['html'] .= "<div class='crr-blk-ico'><img src='images/career-img-ico.png' alt=''/><p>Work Alongside World-Class Talent</p></div>";
        $data['html'] .= "<div class='crr-blk-ico'><img src='images/career-img-ico.png' alt=''/><p>Fully Sponsored Health Care and Benefit Plan</p></div>";
        $data['html'] .= "<div class='crr-blk-ico'><img src='images/career-img-ico.png' alt=''/><p>Culture of Learning and Innovation</p></div>";
        $data['html'] .= "<div class='crr-blk-ico'><img src='images/career-img-ico.png' alt=''/><p>Opportunities for Career Growth with Training and Support</p></div>";
        $data['html'] .= "<div class='crr-blk-ico'><img src='images/career-img-ico.png' alt=''/><p>Flexible Vacation / Paid Time Off Policy</p></div>";
        $data['html'] .= "<div class='crr-blk-ico'><img src='images/career-img-ico.png' alt=''/><p>Parental Leave</p></div>";
        $data['html'] .= "<div class='crr-blk-ico'><img src='images/career-img-ico.png' alt=''/><p>Healthy and Exciting Lunches and Snacks Offered Daily</p></div>";
        $data['html'] .= "<div class='crr-blk-ico'><img src='images/career-img-ico.png' alt=''/><p>Personal Style Encouraged (or not, whatever you’re into)</p></div>";
        $data['opening'] = "<h2>Current Openings</h2>";
        $left            = "<div class='crrnt-left'>";
        $right           = "<div class='crrnt-right'>";
        foreach ($data['list'] as $i => $opening) {
            $left .= "<a href='javascript:void' data-url='" . $opening->career_url . "' class='job' style='text-decoration: none;'><p>" . limited_text($opening->career_title, 30) . "</p></a>";
            $right .= "<p>" . $opening->career_city . ", " . $opening->career_state . "</p>";
        }
        $left .= "</div>";
        $right .= "</div>";
        $data['opening'] .= $left;
        $data['opening'] .= $right;
        echo json_encode($data);
    }

    public function career_details()
    {
        $data['linked_in'] = $this->db->query("SELECT linkedin
                                                FROM tbl_site_setting
                                            ")->row()->linkedin;
        $career_url          = $this->input->get('url');
        $data['career_data'] = $this->career_model->view_details($career_url);

        echo json_encode($data);
    }

    public function apply_job()
    {
        $return['result'] = '';
        $coverFiles1['name'] = '';
        $resumeFiles1['name'] = '';
        $data['career_id']                    = $this->input->post('career_id');
        $data['career_user_first_name']       = $this->input->post('fname');
        $data['career_user_last_name']        = $this->input->post('lname');
        $data['career_user_email']            = $this->input->post('email');
        // $data['career_user_phone']            = $this->input->post('phone');
        $data['career_user_website']          = $this->input->post('website');
        $data['career_user_linkedin_profile'] = $this->input->post('linkedin');
        $data['career_user_hear_from']        = $this->input->post('hear');
        $data['career_user_applied_date']     = date('Y-m-d h:i:s');
        $check = $this->check_apply($data['career_id'],$data['career_user_email']);
        if($check){
            $career_id = $this->common_model->commonSave('tbl_careers_applied_user', $data);
            if ($career_id != '') {
                $career_data = $this->common_model->commonselect('tbl_career', 'career_id', $this->input->post('career_id'))->row();
                $name = $data['career_user_first_name'] . ' ' . $data['career_user_last_name'];
                $career_name = $career_data->career_title;
                $Resume_Link = base_url() . 'resources/applied_user_career_images/resume/' . $resumefile;
                $cover_Link = base_url() . 'resources/applied_user_career_images/cover/' . $coverfile;
                $email_data['subject'] = 'Applied User Details';
                $email_msg = '<b>' . $name . '</b> has applied for a post of <b>' . $career_name . '</b>.<br>';
                $email_msg .= '<b>Email : </b>' . $data['career_user_email'].'<br>';
                if($data['career_user_linkedin_profile']!=''){
                    $email_msg .= '<b>Linkedin Profile :</b>' . $data['career_user_linkedin_profile'].'<br>';
                }
                else{
                    $email_msg .= '<b>Linkedin Profile :</b> N/A'.'<br>';
                }
                if($data['career_user_website']!=''){
                    $email_msg .= '<b>Website :</b>' . $data['career_user_website'].'<br>';
                }
                else{
                    $email_msg .= '<b>Website :</b> N/A'.'<br>';
                }
                if($data['career_user_hear_from']!=''){
                    $email_msg .= '<b>How did you hear about this job? : </b>' . $data['career_user_hear_from'].'<br>';
                }
                else{
                    $email_msg .= '<b>How did you hear about this job? : </b> N/A'.'<br>';
                }
                if ($resumeFiles1['name'] != ""){
                    $email_msg .= '<b>Rescume Link : </b>' . $Resume_Link .'<br>';
                }
                if ($coverFiles1['name'] != "") {
                    $email_msg .= '<b>Cover Letter Link : </b>' . $cover_Link .'<br>';
                }
                //get_social_media_links()->site_email
                send_email('mikesmith1166@gmail.com','mikesmith1166@gmail.com',"New Applicant",$email_msg);            
                $return['result'] = '200';
                $return['message'] = 'Application submitted Successfully.';
            }else{
                $return['result'] = '202';
                $return['message'] = 'There was a problem in submitting application.';
            }
        }else{
            $return['result'] = '205';
            $return['message'] = 'You have already applied for ths job.';
        }
        
        echo json_encode($return);
    }

    public function check_apply($career_id,$email){
        $result = $this->common_model->commonselect2('tbl_careers_applied_user','career_id',$career_id,'career_user_email',$email)->result();
        if(count($result)>0){
            return false;
        }else{
            return true;
        }
    }

    public function forgotPassword()
    {

        $email         = $this->input->get('email');    

        $regData = $this->db->query("
                        SELECT *
                        FROM tbl_user
                        WHERE user_email = '" . $email . "'
                        AND user_status = 1  AND user_is_delete = 0
                    ")->row();

        if(count($regData)>0){
            $data_save['user_activation_code_password'] = md5($email . time());

            $this->common_model->commonUpdate('tbl_user', $data_save, "user_id", $regData->user_id);

            $url   = base_url('reset-password/' . $data_save['user_activation_code_password']);
            $click = "<a href='" . $url . "'>Click here </a>";

            $email_data           = $this->common_model->commonselect('tbl_email_temp', 'e_email_slug', 'password-reset')->row();
            $emailData['subject'] = $email_data->e_email_subject;
            $message              = $email_data->e_email_text;
            $message              = str_replace("{{username}}", $regData->user_fname, $message);
            $message              = str_replace("{{link}}", $url, $message);
            $emailData['message'] = $message;
            $emailData['to']      = $regData->user_email;
            //$regData->user_email
            $is_send = send_email_2($email, 'mikesmith1166@gmail.com', $email_data->e_email_subject, $message);

            if ($is_send) {
                $return['result'] = '200';
                $return['message'] = "We have sent an email to " . $email . ".";
            } else {
                $return['result'] = '202';
                $return['message'] = "An error occur due to some problem.";
            }
        }else{
            $return['result'] = '205';
            $return['message'] = "Email does not exist.";
        }

        echo json_encode($return);
    }

    public function is_unique_register_email(){
        $email = $this->input->post('email');
        $regData = $this->db->query("
                            SELECT *
                            FROM tbl_user
                            WHERE user_email = '".$email."' AND user_is_delete = 0
                        ")->row();
        if(count($regData) > 0){
            $return['result'] = '202';
            $return['message'] = 'This email address already exist.';
        }else{
            $return['result'] = '200';
            $return['message'] = 'Email authenticated.';
        }

        echo json_encode($return);
    }

    public function register_user()
    {
        $array['user_email'] = $this->input->post('email');
        $array['user_password'] = $this->input->post('password');
        $array['user_password_encryp'] = md5($this->input->post('password'));
        $array['user_fname'] = $this->input->post('fname');
        $array['user_lname'] = $this->input->post('lname');
        $array['user_zip'] = $this->input->post('zipcode');
        $array['user_age'] = $this->input->post('age');
        $array['user_interests'] = $this->input->post('interest1');
        $array['user_other_interest'] = $this->input->post('otherInst1');
        $array['user_interests_second'] = $this->input->post('interest2');
        $array['user_other_interest_second'] = $this->input->post('otherInst2');
        $array['user_business_owner'] = $this->input->post('small_bus');
        $array['user_sex'] = $this->input->post('gender');
        $array['user_status'] = 1;
        $array['user_register_date'] = date('Y-m-d H:i:s');
        add_action('init', 'add_user');
        $username = $this->input->post('email');
        $password = $this->input->post('password');
        $email = $this->input->post('email');

        $user_id = wp_create_user( $username, $password, $email );

        $user_id = $this->common_model->commonSave('tbl_user',$array);
        
        if ( $user_id ) {
            
            //email to admin
            $message = "A new user has regitered. <br><br>";
            
            $tmp = $array;
            
            unset($tmp['user_password']);
            unset($tmp['user_password_encryp']);
            
            $tmp['user_name'] = $tmp['user_fname']." ".$tmp['user_lname'];
            
            unset($tmp['user_fname']);
            unset($tmp['user_lname']);
            unset($tmp['user_status']);        
            
            foreach ( $tmp as $k => $ar ) {
                
                $ar = trim($ar);
                
                if ( !empty($ar) ) {
                    
                    if ( $k == "user_business_owner" ) {
                        if ( $ar == 1 ) {
                            $ar = "Yes";
                        } else {
                            $ar = "No";
                        }
                    }
                    
                    if ( $k == "user_register_date" ) {
                        $ar = date("d-m-Y h:i a", strtotime($ar));
                    }
                    
                    if ( $k == "user_other_interest" ) {
                        $k = "User Interests 1";
                    } else if ( $k == "user_interests_second" ) {
                        $k = "User Interests 2";
                    } else if ( $k == "user_other_interest_second" ) {
                        $k = "User Interests 3";
                    } else if ( $k == "user_sex" ) {
                        $k = "Gender";
                    } else {
                        $k = ucwords(str_replace("_", " ", $k));
                    }

                    $message .= "<strong>$k: </strong>$ar<br>";
                    
                }
                
            }
            
            send_email(getSiteSettings('site_email'), "no-reply@educki.com", "New User Registered", $message);
            //email to admin
            
            //email to user

            $email_data           = $this->common_model->commonselect('tbl_email_temp', 'e_email_slug', 'welcome')->row();
            $emailData['subject'] = $email_data->e_email_subject;
            $message              = $email_data->e_email_text;
            $message              = str_replace("{{username}}", $tmp['user_name'], $message);

            //$array['user_email']

            send_email($array['user_email'], "no-reply@educki.com", $emailData['subject'], $message);
            
            //email to user
            $return['result'] = '200';
            $return['message'] = 'User has been registered Successfully.';
            
        }else{
            $return['result'] = '202';
            $return['message'] = 'There was a problem in registering user.';
        }

        echo json_encode($return);
    }

    public function smart_search(){        
        $text = trim($this->db->escape_str($this->input->get('txt')));
        $result = $this->search_model->smartSearch($text);
        $return['rows'] = 0;
        $return['html'] = '';
        if ($result->num_rows() > 0) {
            $return['rows'] = $result->num_rows();
            foreach ($result->result() as $row) {
                $pos = stripos($row->title, $text);
                $len = strlen($text);
                $keyw = '<strong>' . substr($row->title, $pos, $len) . '</strong>';
                $url = '';
                if ($row->tbl == 'Products') {
                    $url = productDetailUrl($row->id)."/".$row->url;
                    $prod_id = $row->id;
                    $return['html'] .= "<li><a href=\"javascript:void\" class=\"searc_clcik\" data-url=\"$row->url\" data-type=\"$row->tbl\" data-id=\"$prod_id\" data-term=\"$row->title\">".str_replace($text, $keyw, $row->title) . " in <span>" . $row->tbl . "</span></a></li>";
                } elseif ($row->tbl == 'Stores') {
                    $url = base_url('store/' . $row->url);
                    $return['html'] .= "<li><a href=\"javascript:void\" class=\"searc_clcik\" data-url=\"$row->url\" data-type=\"$row->tbl\" data-term=\"$row->title\">" . str_replace($text, $keyw, $row->title) . " in <span>" . $row->tbl . "</span></a></li>";
                } elseif ($row->tbl == 'Blogs') {
                    $url = base_url('blog-details/'.$row->url);
                    $return['html'] .= "<li><a href=\"javascript:void\" class=\"searc_clcik\" data-url=\"$row->url\" data-type=\"$row->tbl\" data-term=\"$row->title\">" . str_replace($text, $keyw, $row->title) . " in <span>" . $row->tbl . "</span></a></li>";
                }
            }
            $return['status'] = 200;
        } else {
            $return['status'] = 202;
        }
        echo json_encode($return);
    }

    public function save_search() {        
        $cat_id = '';
        $text = trim($this->db->escape_str($this->input->get('term')));
        $cat = trim($this->db->escape_str($this->input->get('cat')));
        $link = trim($this->db->escape_str($this->input->get('url')));
        if($cat=='Products'){
            $prod_id = trim($this->db->escape_str($this->input->get('prod_id')));
            $cat_id = $this->db->query("SELECT category_id
                                        FROM tbl_product_category WHERE prod_id = ".$prod_id."
                                        ")->row()->category_id;
            $data['cat_id'] = $cat_id;
        }else if($cat=='Stores'){
            $store_id = $this->db->query("SELECT store_id 
                                        FROM tbl_stores WHERE store_url = '".$link."'
                                        ")->row()->store_id;
            $return['store_id'] = $store_id;
        }

        $data['search_term'] = $text;
        $data['search_category'] = $cat;
        $data['search_datetime'] = date('Y-m-d H:i:s');
        $data['user_ip'] = getHostByName(php_uname('n'));

        $return_id = $this->db->insert('tbl_search_terms', $data);

        if($return_id){
            $return['status'] = '200';
            $return['message'] = 'Search saved successfully.';
        }else{
            $return['status'] = '202';
            $return['message'] = 'Problem in dsaving Search.';
        }
        
        echo json_encode($return);
    }

    public function get_menu()
    {
        $data['menu'] = '';
        $type = $this->input->get('user_type');
        if($type=='Seller'){
            $data['menu'] .= "<li><a href='seller-dashboard.html'>Seller Dashboard</a></li>";
            $data['menu'] .= "<li><a href='javascript:void' id='edit-store'>My Store</a></li>";
            $data['menu'] .= "<li><a href='manage-products.html'>Manage Products</a></li>";
            $data['menu'] .= "<li><a href='manage-orders.html'>Manage Orders</a></li>";
            $data['menu'] .= "<li><a href='my-fans.html'>My Fans</a></li>";
            $data['menu'] .= "<li><a href='store-settings.html'>Store Settings</a></li>";
            $data['menu'] .= "<li><a href='mailto:info@educki.com'>Ask eDucki?</a></li>";
        }

        if($type!='Seller'){
            $data['menu'] .= "<li><a href='javascript:void' id='create-store'>Become a Seller</a></li>";
        }

        $data['menu'] .= "<li><a href='message-center.html'>Inbox</a></li>";
        $data['menu'] .= "<li><a href='my-dashboard.html'>My Dashboard</a></li>";
        $data['menu'] .= "<li><a href='my-profile.html'>My Profile</a></li>";        
        $data['menu'] .= "<li><a href='my-orders.html'>My Orders</a></li>";
        $data['menu'] .= "<li><a href='address-book.html'>Address Book</a></li>";
        $data['menu'] .= "<li><a href='my-wishlist.html'>My Wishlist</a></li>";        

        echo json_encode($data);
    }

    public function seller_dashboard() {
        $order_status = 'Awaiting Tracking';
        $data['userid'] = $this->input->get('user_id');
        $data['store_info'] = $this->common_model->commonselect('tbl_stores','user_id',$data['userid'])->row();
        $data['counts'] = $this->dashboard_model->product_views($data['userid']);
        $data['most_view_product'] = $this->dashboard_model->most_view_product($data['store_info']->store_id);
        
        if ( !$data['most_view_product'] ) {
            $data['most_view_product'] = [];
        }        
        
        $data['todays_order'] = $this->dashboard_model->orders($data['store_info']->store_id);
        $data['last_orders'] = $this->dashboard_model->get_user_orders($data['store_info']->store_id,$order_status, 5, 0);

        $data['total_sales'] = getTotalSalesByStore($data['store_info']->store_id);
        if(empty($data['total_sales'])){
            $data['total_sales'] = 0.00;
        }

        $data['store_profit'] = $this->dashboard_model->get_store_profit($data['store_info']->store_id);
        $data['orders_stats'] = $this->dashboard_model->get_order_stats($data['store_info']->store_id);

        $data['month_sales'] = $this->dashboard_model->get_sales_per_month($data['store_info']->store_id);
        $data['months'] = [0,0,0,0,0,0,0,0,0,0,0,0];
        for($i = 0; $i <= 11; $i++) {
            if(($data['month_sales'][$i]->sale_month-1)>0){
                $data['months'][$data['month_sales'][$i]->sale_month-1] = number_format($data['month_sales'][$i]->today_total_sale, 2, '.', '');
            }            
        }

        $data['orders'] = '';
        foreach ($data['last_orders'] as $i => $order){
            $profit = $this->calculateSellerProfit($order['order_prod_store_id'],$order['order_id']);
            $data['orders'] .= "<tr><td class='pad-lft-0'><a href='javascript:void' class='prod_click' data-id='".$order['order_prod_id']."'><img src='".base_url("resources/prod_images/thumb/".$order['order_prod_image'])."' alt='Product Image' title='' class=''></a><p style='float: left;width: 100%;'><a href='javascript:void' class='prod_click' style='text-decoration: none;' data-id='".$order['order_prod_id']."'>".limited_text(clean_string($order['order_prod_description']),50)."</a></p></td><td class='text-center'>$".number_format($order['order_prod_total_price'],2,'.','')."</td><td class='text-center'>$".number_format($order['order_prod_ship_price'],2,'.','')."</td><td class='text-center'>".$order['order_prod_qty']."</td><td class='text-center'>$".number_format($profit, 2, '.', '')."</td></tr>";
        }

        if(count($data['last_orders'])<=0){
            $data['orders_head'] = 0;
            $data['orders'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';float:left;width:100%;font-weight: normal;color: #000;text-align: center;\">No data found.</div>";
        }

        $data['views'] = '';
        foreach ($data['most_view_product'] as $i => $prod) {
            $data['views'] .= "<tr><td class='pad-lft-0'><a href='javascript:void' class='prod_click' data-id='".$prod['product_id']."'><img src='".base_url("resources/prod_images/thumb/".$prod['img_name'])."' alt='' title='' class=''></a><p style='float: left;width: 100%;'><a href='javascript:void' class='prod_click' style='text-decoration: none;' data-id='".$prod['product_id']."'>".limited_text(clean_string($prod['prod_title']),40)."</a></p></td><td class='text-center'>&nbsp;</td><td class='text-center'>$".(($prod['prod_onsale']==1)?number_format($prod['sale_price'],2,'.',''):$prod['prod_price'])."</td><td class='text-center'>".(($prod['prod_occurrence']>0)?$prod['prod_occurrence']:'0')."</td></tr>";
        }

        if(count($data['most_view_product'])<=0){
            $data['views_head'] = 0;
            $data['views'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';float:left;width:100%;font-weight: normal;color: #000;text-align: center;\">No data found.</div>";
        }

        echo json_encode($data);
    }

    function calculateSellerProfit($store_id, $order_id){
        $query = "
                SELECT 
                SUM(store_total) as totalProfit
                FROM
                tbl_order_seller_info os 
                WHERE 
                os.store_id = $store_id AND 
                os.order_id = $order_id
                GROUP BY os.order_id 
                ";
        $result = $this->db->query($query)->row('totalProfit');
    
        return $result;
    }

    public function get_store_data()
    {
        $data['userid'] = $this->input->get('user_id');
        $data['page_data'] = $this->common_model->commonSelect('tbl_stores', 'user_id', $data['userid'])->row();
        $data['base_url'] = base_url();

        echo json_encode($data);
    }

    public function update_discount() {
        $store_id = $this->input->get('store_id');
        $dataS['store_discount'] = $this->input->get('perDiscount');
        $dataS['store_discount_item'] = $this->input->get('numberItems');
        $result = $this->common_model->commonUpdate('tbl_stores', $dataS, 'store_id', $store_id);
        if($result){
            $return['message'] = 'Item updated successfully.';
            $return['status'] = '200';
        }else{
            $return['message'] = 'There was a problem.';
            $return['status'] = '202';
        }

        echo json_encode($return);
    }

    public function hide_store(){
        $data_save['store_is_hide'] = $this->input->get('value');
        $store_id = $this->input->get('store_id');
        $response = $this->common_model->commonUpdate('tbl_stores', $data_save, 'store_id', $store_id);
        if ($response == NULL) {
            $return['status'] = '202';
            $return['message'] = 'There was a problem.';
        } else {
            $return['status'] = '200';
            $return['message'] = 'Item updated successfully.';
        }

        echo json_encode($return);
    }

    public function my_fans()
    {
        $data['userid']   = $this->input->get('user_id');
        $data['store_id'] = $this->common_model->commonSelect('tbl_stores', 'user_id', $data['userid'])->row()->store_id;

        /*-----------Previous Week ----------------*/

        $previous_week = strtotime("-1 week +1 day");
        $start_week    = strtotime("last sunday midnight", $previous_week);
        $end_week      = strtotime("next saturday", $start_week);
        $start_week    = date("Y-m-d", $start_week);
        $end_week      = date("Y-m-d", $end_week);

        $data['last_weeks'] = $this->db->query("
                                        SELECT COUNT(*) as lastweek
                                        FROM tbl_fans fan
                                        INNER JOIN tbl_user user ON user.user_id = fan.fan_id
                                        WHERE user.user_is_delete = 0  AND user.user_status = 1 AND 
                                        fan_date <= '" . $end_week . "' AND fan_date >= '" .
            $start_week . "' AND store_id = " . $data['store_id'] . "
                                    ")->row();        
        $data['one_month'] = $this->db->query("
                                        SELECT COUNT(*) as month
                                        FROM tbl_fans fan
                                        INNER JOIN tbl_user user ON user.user_id = fan.fan_id
                                        WHERE user.user_is_delete = 0  AND user.user_status = 1 AND 
                                        fan_date <= '" . date("Y-n-j", strtotime("last day of previous month")) . "' AND fan_date >= '" .
            date("Y-n-j", strtotime("first day of previous month")) . "' AND store_id = " . $data['store_id'] . "
                                    ")->row();        

        /*-----------------6 Months-------------------*/

        $until    = new DateTime(date('m-01-Y'));
        $interval = new DateInterval('P6M'); //6 months
        $from     = $until->sub($interval); //$until->format('Y-m-d')

        $data['six_month'] = $this->db->query("
                                        SELECT COUNT(*) as sixmonths
                                        FROM tbl_fans fan
                                        INNER JOIN tbl_user user ON user.user_id = fan.fan_id
                                        WHERE user.user_is_delete = 0  AND user.user_status = 1 AND 
                                        fan_date <= '" . date("Y-n-j", strtotime("last day of previous month")) . "' AND fan_date > '" .
            $from->format('Y-m-d') . "' AND store_id = " . $data['store_id'] . "
                                        ")->row();

        $data['year'] = $this->db->query("
                                        SELECT COUNT(*) as year
                                        FROM tbl_fans fan
                                        INNER JOIN tbl_user user ON user.user_id = fan.fan_id
                                        WHERE user.user_is_delete = 0 AND user.user_status = 1 AND
                                        fan_date <= '" . date("Y-n-j", strtotime("last year December 31st")) . "' AND fan_date >= '" .
            date("Y-n-j", strtotime("last year January 1st")) . "' AND store_id = " . $data['store_id'] . "
                                        ")->row();        

        $data['total'] = $this->db->query("
                                        SELECT COUNT(*) as total
                                        FROM tbl_fans fan
                                        INNER JOIN tbl_user user ON user.user_id = fan.fan_id
                                        WHERE user.user_is_delete = 0  AND user.user_status = 1 AND fan.store_id = " . $data['store_id'] . "
                                        ")->row();

        echo json_encode($data);
    }

    public function load_fans()
    {

        $data['userid']   = $this->input->get('user_id');
        $url = $this->input->get('url');
        $data['store_id'] = $this->common_model->commonSelect('tbl_stores', 'user_id', $data['userid'])->row()->store_id;
        $url_search = "";

        if($url!=''){
            $url_search = "(store.store_name LIKE '%".$url."%' OR user.user_fname LIKE '%".$url."%' OR user.user_lname LIKE '%".$url."%') AND";
        }

        $data['total_fans'] = $this->db->query("
                                        SELECT user.user_city, user.user_state,user.user_profile_picture,
                                        user.user_fname, user.user_lname,user.user_register_date,
                                        store.store_id, store.store_name, store.store_logo_image, store.store_city,
                                        store.store_state, store.store_is_hide, store.store_url
                                        FROM tbl_fans fan
                                        INNER JOIN tbl_user user ON fan.fan_id = user.user_id
                                        LEFT JOIN tbl_stores store ON user.user_id = store.user_id
                                        WHERE ".$url_search." user.user_status = 1 AND user.user_is_delete = 0 AND user.user_flagged_status = 0 AND fan.store_id = " . $data['store_id'] . "  ORDER BY fan.fan_id DESC")->result();

        $counter = $this->input->get('counter') * 10;

        $data['Allfans'] = $this->db->query("
                                        SELECT user.user_city, user.user_state,user.user_profile_picture,
                                        user.user_fname, user.user_lname,user.user_register_date,
                                        store.store_id, store.store_name, store.store_logo_image, store.store_city,
                                        store.store_state, store.store_is_hide, store.store_url
                                        FROM tbl_fans fan
                                        INNER JOIN tbl_user user ON fan.fan_id = user.user_id
                                        LEFT JOIN tbl_stores store ON user.user_id = store.user_id
                                        WHERE ".$url_search." user.user_status = 1 AND user.user_is_delete = 0 AND user.user_flagged_status = 0 AND fan.store_id = " . $data['store_id'] . "  ORDER BY fan.fan_id DESC LIMIT ".$counter.",10")->result();

        $data['last_query'] = $this->db->last_query();        
        $data['fan_html'] = "";
        foreach ($data['Allfans'] as $i => $fan) {
            $class='';
            if($i == count($data['Allfans'])-1){
                $class = " lst-shp";
            }else{
                $class = "";
            }
            $img = base_url();
            $url = "";
            $name = "";

            if($fan->store_is_hide==0 AND $fan->store_url!=''){ 
                $url = $fan->store_url;
            }else{ 
                $url = "javascript:void(0)"; 
            }

            if($fan->store_logo_image!=''){ 
                $img .= "resources/seller_account_image/logo/thumb/".$fan->store_logo_image; 
            }else{
                if($fan->user_profile_picture!=''){
                    $img .= "resources/user_profile_image/thumb/".$fan->user_profile_picture; 
                }else{
                    $img .= "front_resources/images/myfan-thumb01.jpg";
                }
            }

            if($fan->store_name!=''){ 
                $name = $fan->store_name; 
            }else{ 
                $name = $fan->user_fname.$fan->user_lname[0]; 
            }

            $state = '';

            if($fan->store_city!='') { 
                $state .= $fan->store_city;
            }else{
                $state .= $fan->user_city;
            } 

            if($fan->store_state!=''){ 
                $state .= ", ".$fan->store_state;
            }elseif($fan->user_state!=''){
                $state .= ", ".$fan->user_state;
            }

            $data['fan_html'] .= "<div class='fav-shp ".$class."'><a href='javascript:void' class='store_click' data-id='".$fan->store_id."'><img src='".$img."'></a><a href='javascript:void' data-id='".$fan->store_id."' class='fan-anchor store_click'><div class='fav-shp-rgt'><p>".$name."</p><span>".$state."</span><p>Member Since ".date('Y',strtotime($fan->user_register_date))."</p></div></a></div>";
        }

        if (count($data['Allfans']) == 0) {
            $data['no-data'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';float:left;width:100%;font-weight: normal;color: #000;text-align: center;\">No data found.</div>";
        }

        $data['counter'] = $counter + 10;

        /*echo "<pre>";
        print_r($data['Allfans']);
        exit;*/

        echo json_encode($data);
    }

    public function load_messages()
    {
        $data['userid']   = $this->input->get('user_id');
        $type   = $this->input->get('type');
        $utext   = $this->input->get('utext');
        $counter = $this->input->get('counter') * 10;
        $Allconversation = $this->fetchAllConversations($data['userid']);
        $data['total_con']  = count($Allconversation);
        if($utext!=''){
            $data['Allconversation'] = $this->fetchAllConversations($data['userid'], 10, $counter,$utext,$type);
        }else{
            $data['Allconversation'] = $this->fetchAllConversations($data['userid'], 10, $counter);
        }

        $data['chat_html'] = "";

        foreach($data['Allconversation'] as $i => $chat){
            $name = '';
            if($chat->buyer == $data['userid']) {
                $name = $chat->sellername . ' ' . $chat->seller_lname[0];
            }else{
                $name = $chat->buyername . ' ' . $chat->buyer_lname[0];
            }

            $class='';
            $class_2='';
            if($chat->buyer==$data['userid']){
                if($chat->buyer_unread > 0) {
                    $class = 'altr-clr';
                    $class_2 = 'notified';
                } else { 
                    $class = '';
                    $class_2 = '';
                }
            }else{
                if($chat->seller_unread > 0){
                    $class = 'altr-clr';
                    $class_2 = 'notified';
                } else { 
                    $class = '';
                    $class_2 = '';
                }
            }

            $unread = '';
            
            if ($chat->buyer != $data['userid']) {
                if ($chat->seller_unread > 0){
                    $unread = $chat->seller_unread;
                }
            }else{
                if ($chat->buyer_unread > 0) {
                    $unread = $chat->buyer_unread;
                }
            }

            $data['chat_html'] .= "<div class='msg-centre-con ".$class."'><div class='wrapper'><div class='crt-order-frst-rw'><div class='signed-in-checkbox'><label><input type='checkbox' name='ulist1[]' value='".$chat->conversation_id."'/><span></span></label></div><div class='dlt-edt'><a href='javascript:void' class='msg-noti ".$class_2." view_chat' data-id='".$chat->conversation_id."'><span class='noti-count'>".$unread."</span></a><a href='javascript:void' class='delete_chat' data-id='".$chat->conversation_id."'><img src='images/delete.png' alt='Delete'></a></div><div class='crt-order-frst-bx2'><div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Date:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>".date("m-d-Y", strtotime($chat->timestamp))."</p></div></div><div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Sender:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>".$name."</p></div></div><div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Product ID:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>".$chat->product_id."</p></div></div><div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Message:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>".limited_text(get_latest_message($chat->conversation_id),60)."</p></div></div></div></div></div></div>";
        }

        if(count($data['Allconversation'])<=0){
            $data['chat_html'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';float:left;width:100%;font-weight: normal;color: #000;text-align: center;\">No data found.</div>";
        }

        $data['counter'] = $counter + 10;

        $data['utext'] = $utext;
        $data['type'] = $type;

        /*echo "<pre>";
        print_r($data);
        exit;*/

        echo json_encode($data);
    }

    public function fetchAllConversations($userId, $per_page = '', $page = '',$text = '',$type='')
    {
        $this->db->select('*');
        $this->db->select('(select user_fname from tbl_user where user_id = buyer) as buyername');
        $this->db->select('(select user_lname from tbl_user where user_id = buyer) as buyer_lname');
        $this->db->select('(select user_email from tbl_user where user_id = buyer) as buyer_email');
        $this->db->select('(select user_fname from tbl_user where user_id = seller) as sellername');
        $this->db->select('(select user_lname from tbl_user where user_id = seller) as seller_lname');
        $this->db->select('(select user_email from tbl_user where user_id = seller) as seller_email');
        $this->db->select('(SELECT COUNT(id) FROM tbl_messages where receiver = buyer AND tbl_messages.conversation_id = tbl_chat_conversations.conversation_id and is_read="0") as buyer_unread');
        $this->db->select('(SELECT COUNT(id) FROM tbl_messages where receiver = seller AND tbl_messages.conversation_id = tbl_chat_conversations.conversation_id and is_read="0") as seller_unread');
        $this->db->join('tbl_user','tbl_chat_conversations.buyer = tbl_user.user_id OR tbl_chat_conversations.seller = tbl_user.user_id','inner');
        $this->db->where("(buyer = '$userId' OR seller = '$userId')");
        $this->db->where("FIND_IN_SET('".$userId."',active_for) !=", 0);
        if($text!=''){
            if($type==1){
                $this->db->where("(tbl_user.user_fname = '$text' OR tbl_user.user_lname = '$text')");
            }else if($type==2){
                $this->db->where("tbl_chat_conversations.product_id = '$text'");
            }
        }
        $this->db->group_by("tbl_chat_conversations.id");
        $this->db->order_by("id", "DESC");

        $query = '';
        if ($per_page >0) {
            $this->db->limit($per_page, $page);
            $query = $this->db->get('tbl_chat_conversations');
        } else {
            $query = $this->db->get('tbl_chat_conversations');
        }

        return $query->result();
    }

    public function delete_chats()
    {            
        $chats = explode(",", $this->input->get('ulist1'));
        $user   = $this->input->get('user_id');
        foreach($chats as $key){
            $this->delete_multiple($key,$user);
        }
        $return['message'] = 'Item deleted successfully.';
        $return['status'] = '200';

        echo json_encode($return);
    }

    public function delete_multiple($conversation,$user)
    {
        if ($conversation) {
            $this->Message_model->DeleteConversation($user, $conversation);
        }

    }

    public function delete()
    {
        $conversation = $this->input->get('conversation');
        $user = $this->input->get('user_id');
        if ($conversation) {
            $this->Message_model->DeleteConversation($user, $conversation);
            $return['message'] = 'Item deleted successfully.';
            $return['status'] = '200';
        }else{
            $return['message'] = 'There was a problem in deleting.';
            $return['status'] = '202';
        }

        echo json_encode($return);
    }

    public function fetch_messages()
    {
        $conversation_id = $this->input->get('con_id');
        $user_id = $this->input->get('user_id');
        $conversation            = $this->Message_model->get_conversation_attr($conversation_id);
        if($conversation){

            if($conversation['buyer']==$user_id){
                $data['receiver'] = $conversation['seller'];
            }else{
                $data['receiver'] = $conversation['buyer'];
            }

            $data['prod_id'] = $conversation['product_id'];
            $data['user_id'] = $user_id;
            $data['conversation_id'] = $conversation['conversation_id'];
            $data['conversation']    = $this->Message_model->fetchMessages($user_id, $conversation_id);
            $data['status'] = 200;
            $data['message'] = 'Messages fetched successfully.';
            $data['html'] = '';
            foreach ($data['conversation'] as $i => $msg) {
                $class = '';
                $sender = '';
                if($i%2==0){
                    $class = 'altr-clr';
                }else{
                    $class = '';
                }
                if ($msg->sender == $user_id) {
                    $sender = "Me";
                } else {
                    $sender = $msg->sendername . ' ' . $msg->sender_lname;
                }

                $data['html'] .= "<div class='msg-centre-con ".$class."'><div class='wrapper'><div class='crt-order-frst-rw'><div class='crt-order-frst-bx2'><div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Date:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>".date("m-d-Y", strtotime($msg->timestamp))."</p></div></div><div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Sender:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>".$sender."</p></div></div><div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Product ID:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>".$data['prod_id']."</p></div></div><div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Messages:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>".$msg->message."</p></div></div></div></div></div></div>";
            }
        }else{
            $data['status'] = 202;
            $data['message'] = 'There was a problem.';
        }

        if(count($data['conversation'])<=0){
            $data['html'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;float:left;width:100%;color: #000;text-align: center;\">No data found.</div>";
        }

        echo json_encode($data);
    }

    public function submitMessage()
    {
        $sender   = $this->input->get('user_id');
        $receiver = $this->input->get('receiver');
        $product  = $this->input->get('prod_id');
        $message  = $this->input->get('message');
        $user_email  = get_user_email($receiver);
        $data['email'] = $user_email;

        if(!empty($user_email)){
            $data['message'] = 'There was a problem.';
            $data['status'] = 202;
            if (!empty($sender) && !empty($receiver) && !empty($message) && !empty($user_email)) {
                $confirm = $this->Message_model->submitMessage($sender, $product, $receiver, $message);            
                if ($confirm) {
                    $message = "You received a new message."."<br><br><br>";
                    $message .= "Please login to view the message : ".base_url();
                    $to   = $user_email;
                    $from = 'contact@educki.com';
                    $is_send = send_email_2($to, $from, 'New Message', $message);
                    if($is_send){
                        $data['message'] = 'Message sent successfully.';
                        $data['status'] = 200;
                    }else{
                        $data['message'] = 'Problem sending email.';
                        $data['status'] = 203;
                    }                
                }
            }
        }else{
            $data['message'] = 'User is no longer available.';
            $data['status'] = 204;
        }

        echo json_encode($data);
    }

    public function loginWithFacebook()
    {
         $expires = time() + (5400000);
        $facebook_id = $this->input->get('facebook_id');
        $user_email = $this->input->get('email');
        $name = explode(" ", $this->input->get('name'));

        $return['cartId'] = $this->input->get('_cartId');


        $user_id = '';
        $regData = $this->db->query("
                                    SELECT *
                                    FROM tbl_user
                                    WHERE user_facebook_id  = '".$facebook_id."' AND user_is_delete = 0
                                    ")->row();
        if (count($regData) > 0 && $regData->user_email == $user_email) {
            $loginData['login_user_id']    = $regData->user_id;
            $loginData['login_date']       = date('Y-m-d H:i:s');
            $lastlogin['user_last_active'] = date('Y-m-d H:i:s');
            $this->common_model->commonUpdate('tbl_user', $lastlogin, 'user_id', $regData->user_id);
            $this->common_model->commonSave('tbl_user_login', $loginData);
            $return['user_data'] = $regData;
            $return['status'] = '200';
            $return['status'] = 'User exists - proceed to logging in';
        } else if (count($regData) > 0 && $regData->user_email != $user_email) {
            $regData2 = $this->db->query("
                            SELECT *
                            FROM tbl_user
                            WHERE user_email  = '".$user_email."' AND user_is_delete = 0
                        ")->row();
            if (count($regData2) <= 0) {
                $update['user_email'] = $user_email;
                $this->common_model->commonUpdate('tbl_user', $update, 'user_facebook_id', $facebook_id);
                $loginData['login_user_id']    = $regData2->user_id;
                $loginData['login_date']       = date('Y-m-d H:i:s');
                $lastlogin['user_last_active'] = date('Y-m-d H:i:s');
                $this->common_model->commonUpdate('tbl_user', $lastlogin, 'user_id', $regData2->user_id);
                $this->common_model->commonSave('tbl_user_login', $loginData);
                $return['user_data'] = $regData2;
                $return['status'] = '201';
                $return['status'] = 'User exists - Updating email - proceed to logging in';
                exit;
            } else {
                $return['status'] = '202';
                $return['status'] = 'User exists';
            }
        } else {
            // fb_id not exist
            $regData3 = $this->db->query("
                        SELECT *
                        FROM tbl_user
                        WHERE user_email  = '".$user_email."' AND user_is_delete = 0
                    ")->row();
            if (count($regData3) > 0) {
                $return['status'] = '202';
                $return['status'] = 'User exists';
            } else {
                $array['user_fname'] = $name[0];
                $array['user_lname'] = $name[1];
                $array['user_status'] = 1;
                $array['user_type'] = 'Buyer';
                $array['user_email'] = $user_email;
                $array['user_facebook_id'] = $facebook_id; 
                $user_id = $this->common_model->commonSave('tbl_user', $array);
                $loginData['login_user_id']    = $user_id;
                $loginData['login_date']       = date('Y-m-d H:i:s');
                $lastlogin['user_last_active'] = date('Y-m-d H:i:s');
                $this->common_model->commonUpdate('tbl_user', $lastlogin, 'user_id', $user_id);
                $this->common_model->commonSave('tbl_user_login', $loginData);
                $return['user_data'] = $this->db->query("
                                        SELECT *
                                        FROM tbl_user
                                        WHERE user_id  = '".$user_id."' 
                                    ")->row();
                $return['status'] = '203';
                $return['status'] = 'User Registered Proceeding to login';
            }
        }

        if($return['status'] != 202){

            $result = $this->common_model->commonSelect("tbl_cart", "cart_user_id", $return['user_data']->user_id)->row();
            $cartId = '';
            if(!empty($result)){
                $is_prod =  $this->common_model->commonSelect("tbl_cart_detail", "cart_id", $result->cart_id)->row();
                if(!empty($is_prod)){
                    $return['cartId'] = $cartId = $result->cart_id;
                }else{
                    $this->cart_model->removeCart($result->cart_id);
                    $cartId = $return['cartId'];
                    if($cartId != ''){
                        $addUser['cart_user_id'] = $regData->user_id;
                        $this->db->where('cart_id', $cartId);
                        $this->db->update('tbl_cart', $addUser);
                        setcookie('educki_cartId', $cartId, $expires, '/');

                    }
                }
            }else{
                $cartId = $return['cartId'];
                if($cartId != ''){
                    $addUser['cart_user_id'] = $regData->user_id;
                    $this->db->where('cart_id', $cartId);
                    $this->db->update('tbl_cart', $addUser);
                    setcookie('educki_cartId', $cartId, $expires, '/');

                }
            }
            $sessiondata = array(
                'userid'    => $return['user_data']->user_id,
                'useremail' => $return['user_data']->user_email,
                'userfname' => $return['user_data']->user_fname,
                'userlname' => $return['user_data']->user_lname,
                'cartId'    => $cartId,
                'usertype' => $return['user_data']->user_type,
                'logged_in' => true,
            );            

            $this->session->set_userdata($sessiondata);
            setcookie('educki_userId', $regData->user_id, $expires, '/');

        }


        echo json_encode($return);
    }

    public function my_dashboard()
    {
        $data['userid'] = $this->input->get('user_id');

        $data['user'] = $this->db->query("
                                    SELECT *
                                    FROM tbl_user
                                    WHERE
                                    user_id = " . $data['userid'] . "
                                    AND user_status = 1 AND user_is_delete = 0
                                    ")->row();

        /*$data['orders'] = $this->db->query("
                            SELECT *
                            FROM tbl_orders
                            WHERE buyer_id = " . $data['userid'] . " ORDER BY order_id DESC LIMIT 0,3")->result();*/
        $data['orders'] = $this->order_model->get_my_orders($data['userid'], 3, 0);
        $data['total_orders'] = count($data['orders']);

        $data['html'] = '';

        foreach ($data['orders'] as $i => $o){
            $o_prod = getOrderProductByOrderId($o->order_id);
            $data['html'] .= "<tr><td class='pad-lft-0'><a href='javascript:void' class='prod_click' data-id='".$o->order_prod_id."'><img src='".base_url('resources/prod_images/thumb/'.getrodImage($o->order_prod_id))."' alt='' title='' class=''></a><p style='font-size:14px;width:100%;'><a href='javascript:void'  style='text-decoration:none;' class='prod_click' data-id='".$o->order_prod_id."'>".$o->order_prod_name."</a></p></td><td>".date('m-d-Y', strtotime($o->order_date))."</td><td>$".number_format($o->order_grand_total, 2, '.', '')."</td><td>".$o->order_status."</td><td><a data-id='".$o->order_id."' class='view_order' href='javascript:void'>View Order</a><a data-orderid='".$o->order_id."' class='re_order' href='javascript:void(0);'>Reorder</a></td></tr>";
        }

        if(count($data['orders'])<=0){
            $data['html'] = "<tr><td colspan='5'><div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;float:left;width:100%;color: #000;text-align: center;\">No data found.</div></td></tr>";
        }

        $data['address_count'] = 0;

        $data['default_shipping'] = $this->common_model->default_shipping($data['userid']);
        $data['default_billing'] = $this->common_model->default_billing($data['userid']);

        if($data['default_billing']->add_id==$data['default_shipping']->add_id){
            $data['same'] = 1;
        }else{
            $data['same'] = 0;
        }

        $data['default_bill'] = '';
        $data['default_bill'] .= "<h5>Default Billing Address<a href='javascript:void' data-id='".$data['default_billing']->add_id."' class='view-all edit-address'>Edit Address</a></h5>";
        if(!empty($data['default_billing'])){
            $data['bill'] = 1;
            $data['default_bill'] .= "<p>".$data['default_billing']->nick_name."</p>";
            $data['default_bill'] .= "<p>".$data['default_billing']->first_name." ".$data['default_billing']->last_name."</p>";
            $data['default_bill'] .= "<p>".(($data['default_billing']->phone)?$data['default_billing']->phone:'N/A')."</p>";
            $data['default_bill'] .= "<p>".$data['default_billing']->street."</p>";
            $data['default_bill'] .= "<p>".$data['default_billing']->city."</p>";
            $data['default_bill'] .= "<p>".get_country_name($data['default_billing']->country)."</p>";
            if ($data['default_billing']->country == "US"){
                $data['default_bill'] .= "<p>".get_state_name($data['default_billing']->state)."</p>";
            } else {
                $data['default_bill'] .= "<p>".$data['default_billing']->state_other."</p>";
            }
            $data['default_bill'] .= "<p>".$data['default_billing']->zip."</p>";
        }else{
            $data['bill'] = 0;
            $data['address_count']++;
            $data['default_bill'] = "<h5>Default Billing Address</h5>";
            $data['default_bill'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;float:left;width:100%;color: #000;text-align: center;\">No data found.</div>";
        }

        $data['default_ship'] = '';
        $data['default_ship'] .= "<h5>Default Shipping Address<a href='javascript:void' data-id='".$data['default_shipping']->add_id."' class='view-all edit-address'>Edit Address</a></h5>";
        if(!empty($data['default_shipping'])){
            $data['ship'] = 1;
            $data['default_ship'] .= "<p>".$data['default_shipping']->nick_name."</p>";
            $data['default_ship'] .= "<p>".$data['default_shipping']->first_name." ".$data['default_shipping']->last_name."</p>";
            $data['default_ship'] .= "<p>".(($data['default_shipping']->phone)?$data['default_shipping']->phone:'N/A')."</p>";
            $data['default_ship'] .= "<p>".$data['default_shipping']->street."</p>";
            $data['default_ship'] .= "<p>".$data['default_shipping']->city."</p>";
            $data['default_ship'] .= "<p>".get_country_name($data['default_shipping']->country)."</p>";
            if ($data['default_shipping']->country == "US"){
                $data['default_ship'] .= "<p>".get_state_name($data['default_shipping']->state)."</p>";
            } else {
                $data['default_ship'] .= "<p>".$data['default_shipping']->state_other."</p>";
            }            
        }else{
            $data['ship'] = 0;
            $data['address_count']++;
            $data['default_ship'] = "<h5>Default Shipping Address</h5>";
            $data['default_ship'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;float:left;width:100%;color: #000;text-align: center;\">No data found.</div>";
        }

        echo json_encode($data);
    }

    public function change_password()
    {
        $user_id = $this->input->get('user_id');
        $old_password = $this->input->get('old_password');
        $new_password = $this->input->get('new_password');

        $data['user'] = $this->db->query("
                                    SELECT *
                                    FROM tbl_user
                                    WHERE
                                    user_id = ".$user_id."
                                    AND user_status = 1 AND user_is_delete = 0
                                    ")->row();

        if ($old_password==$data['user']->user_password){
            $data_save['user_password'] = $new_password;
            $data_save['user_password_encryp'] = md5($new_password);
            $this->common_model->commonUpdate('tbl_user', $data_save, 'user_id', $user_id);

            /*require_once(ABSPATH.'wp-load.php');
            $userdata = get_user_by('email', $data['user']->user_email); //get user wordpress Table id by email 
            $return['wp_data'] = $userdata;
            $wp_data_save['ID'] = $user_id; //user wordpress table id
            $wp_data_save['user_pass'] = $new_password;
            wp_set_password($wp_data_save['user_pass'], $wp_data_save['ID']);
            wp_update_user($wp_data_save);*/

            $return['status'] = 200;
            $return['message'] = 'Password Changed Successfully.';
        }else if ($old_password!=$data['user']->user_password){
            $return['status'] = 202;
            $return['message'] = 'Please Enter your correct password.';
        }

        echo json_encode($return);
    }

    public function get_addresses(){

        $data['userid'] = $this->input->get('user_id');
        $data['default_shipping'] = $this->common_model->default_shipping($data['userid']);
        $data['default_billing'] = $this->common_model->default_billing($data['userid']);
        $data['address_list'] = $this->address_model->address_list($data['userid']);

        if($data['default_billing']->add_id==$data['default_shipping']->add_id){
            $data['same'] = 1;
            if($data['default_billing'] !=''){
            $data['same_add'] = "<a href='javascript:void' style='color: #fff !important;font-family: \"source_sans_proregular\";font-size: 14px;' class='edit-address' data-id='".$data['default_billing']->add_id."'>Edit</a> | <a href='javascript:void' style='color: #fff !important;font-family: \"source_sans_proregular\";font-size: 14px;' class='delete-address' data-id='".$data['default_billing']->add_id."'>Delete</a>";
        }else{
                $data['same_add'] = '';
            }
        }else{
            $data['same_add'] = "";
            $data['same'] = 0;
        }

        $data['default_bill'] = "";

        if(!empty($data['default_billing'])){
            $data['default_bill'] .= "<div class='banything-row'><div class='banything-lbl'>Nickname</div><div class='banything-dtl'>".$data['default_billing']->nick_name."</div></div>";
            $data['default_bill'] .= "<div class='banything-row'><div class='banything-lbl'>Name</div><div class='banything-dtl'>".$data['default_billing']->first_name." ".$data['default_billing']->last_name."</div></div>";
            $data['default_bill'] .= "<div class='banything-row'><div class='banything-lbl'>Telephone</div><div class='banything-dtl'>".(($data['default_billing']->phone!='')?$data['default_billing']->phone:'N/A')."</div></div>";
            $data['default_bill'] .= "<div class='banything-row'><div class='banything-lbl'>Street</div><div class='banything-dtl'>".$data['default_billing']->street."</div></div>";
            $data['default_bill'] .= "<div class='banything-row'><div class='banything-lbl'>City</div><div class='banything-dtl'>".$data['default_billing']->city."</div></div>";
            if($data['default_billing']->country=='US'){
                $data['default_bill'] .= "<div class='banything-row'><div class='banything-lbl'>State</div><div class='banything-dtl'>".get_state_name($data['default_billing']->state)."</div></div>";
            }else{
                $data['default_bill'] .= "<div class='banything-row'><div class='banything-lbl'>State</div><div class='banything-dtl'>".$data['default_billing']->state_other."</div></div>";
            }
            $data['default_bill'] .= "<div class='banything-row'><div class='banything-lbl'>Country</div><div class='banything-dtl'>".get_country_name($data['default_billing']->country)."</div></div>";
            $data['default_bill'] .= "<div class='banything-row'><div class='banything-lbl'>Zip</div><div class='banything-dtl'>".$data['default_billing']->zip."</div></div>";
            if($data['same']!=1)
            {
                $data['default_bill'] .= "<div class='banything-row'><ul><li><a href='javascript:void' class='edit-address' data-id='".$data['default_billing']->add_id."'>Edit</a></li><li>|</li><li><a href='javascript:void' class='delete-address' data-id='".$data['default_billing']->add_id."'>Delete</a></li></ul></div>";
            }
        }else{
            $data['default_bill'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;float:left;width:100%;color: #000;text-align: center;\">No data found.</div>";
        }

        $data['default_ship'] = "";

        if(!empty($data['default_shipping'])){
            $data['default_ship'] .= "<div class='banything-row'><div class='banything-lbl'>Nickname</div><div class='banything-dtl'>".$data['default_shipping']->nick_name."</div></div>";
            $data['default_ship'] .= "<div class='banything-row'><div class='banything-lbl'>Name</div><div class='banything-dtl'>".$data['default_shipping']->first_name." ".$data['default_shipping']->last_name."</div></div>";
            $data['default_ship'] .= "<div class='banything-row'><div class='banything-lbl'>Telephone</div><div class='banything-dtl'>".(($data['default_shipping']->phone!='')?$data['default_shipping']->phone:'N/A')."</div></div>";
            $data['default_ship'] .= "<div class='banything-row'><div class='banything-lbl'>Street</div><div class='banything-dtl'>".$data['default_shipping']->street."</div></div>";
            $data['default_ship'] .= "<div class='banything-row'><div class='banything-lbl'>City</div><div class='banything-dtl'>".$data['default_shipping']->city."</div></div>";
            if($data['default_shipping']->country=='US'){
                $data['default_ship'] .= "<div class='banything-row'><div class='banything-lbl'>State</div><div class='banything-dtl'>".get_state_name($data['default_shipping']->state)."</div></div>";
            }else{
                $data['default_ship'] .= "<div class='banything-row'><div class='banything-lbl'>State</div><div class='banything-dtl'>".$data['default_shipping']->state_other."</div></div>";
            }
            $data['default_ship'] .= "<div class='banything-row'><div class='banything-lbl'>Country</div><div class='banything-dtl'>".get_country_name($data['default_shipping']->country)."</div></div>";
            $data['default_ship'] .= "<div class='banything-row'><div class='banything-lbl'>Zip</div><div class='banything-dtl'>".$data['default_shipping']->zip."</div></div>";
            if($data['same']!=1)
            {
                $data['default_ship'] .= "<div class='banything-row'><ul><li><a href='javascript:void' class='edit-address' data-id='".$data['default_shipping']->add_id."'>Edit</a></li><li>|</li><li><a href='javascript:void' class='delete-address' data-id='".$data['default_shipping']->add_id."'>Delete</a></li></ul></div>";
                
            }
        }else{
            $data['default_ship'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;float:left;width:100%;color: #000;text-align: center;\">No data found.</div>";
        }        

        $data['add_address'] = '';

        $add_counter = 0;

        foreach ($data['address_list'] as $i => $add){
            $add_counter++;
            $data['add_address'] .= "<div class='banything-row'><div class='banything-lbl'>Nickname</div><div class='banything-dtl'>".$add->nick_name."</div></div>";
            $data['add_address'] .= "<div class='banything-row'><div class='banything-lbl'>Name</div><div class='banything-dtl'>".$add->first_name." ".$add->last_name."</div></div>";
            $data['add_address'] .= "<div class='banything-row'><div class='banything-lbl'>Telephone</div><div class='banything-dtl'>".(($add->phone!='')?$add->phone:'N/A')."</div></div>";
            $data['add_address'] .= "<div class='banything-row'><div class='banything-lbl'>Street</div><div class='banything-dtl'>".$add->street."</div></div>";
            $data['add_address'] .= "<div class='banything-row'><div class='banything-lbl'>City</div><div class='banything-dtl'>".$add->city."</div></div>";
            if($add->country=='US'){
                $data['add_address'] .= "<div class='banything-row'><div class='banything-lbl'>State</div><div class='banything-dtl'>".get_state_name($add->state)."</div></div>";
            }else{
                $data['add_address'] .= "<div class='banything-row'><div class='banything-lbl'>State</div><div class='banything-dtl'>".$add->state_other."</div></div>";
            }
            $data['add_address'] .= "<div class='banything-row'><div class='banything-lbl'>Country</div><div class='banything-dtl'>".get_country_name($add->country)."</div></div>";
            $data['add_address'] .= "<div class='banything-row'><div class='banything-lbl'>Zip</div><div class='banything-dtl'>".$add->zip."</div></div>";
            $data['add_address'] .= "<div class='banything-row'><ul><li><a href='javascript:void' class='edit-address' data-id='".$add->add_id."'>Edit</a></li><li>|</li><li><a href='javascript:void' class='delete-address' data-id='".$add->add_id."'>Delete</a></li></ul></div>";
            if($i!=(count($data['address_list'])-1)){
                $data['add_address'] .= "<div class='banything-wrapper'></div>";
            }
        }

        if($add_counter==0){
            $data['add_address'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;float:left;width:100%;color: #000;text-align: center;\">No data found.</div>";
        }

        echo json_encode($data);
    }

    public function delete_address(){
        $address_id = $this->input->get('add_id');
        $response = $this->common_model->commonDelete('tbl_user_addreses', 'add_id', $address_id);
        if ($response != NULL) {
            $return['status'] = 200;
            $return['message'] = 'Address Deleted Successfully.';
        }else{
            $return['status'] = 202;
            $return['message'] = 'There was a problem.';
        }

        echo json_encode($return);
    }

    public function get_address_data()
    {
        $data['allstate'] = $this->db->query("SELECT stat_id, stat_name FROM tbl_states")->result();
        $data['allcountries'] = $this->db->query("SELECT iso, name FROM tbl_country")->result();
        $data['tiers'] = $this->db->query("SELECT * FROM tbl_tier_list ORDER BY tier_id ASC")->result();

        $return['countries'] = "<option value='0'>Select Country</option>";
        foreach ($data['allcountries'] as $i => $country){
            $return['countries'] .= "<option value='".$country->iso."' ".(($country->iso=='US')?'selected':'').">".$country->name."</option>";
        }

        $return['states'] = "<option value='0'>Select State</option>";
        foreach ($data['allstate'] as $i => $state){
            $return['states'] .= "<option value='".$state->stat_id."'>".$state->stat_name."</option>";
        }

        $return['tier'] = '<option value="0">Select</option>';
        foreach ($data['tiers'] as $i => $tir){
            $return['tier'] .= '<option value="'.$tir->tier_id.'" '.(($tir->tier_id==3)?'selected':'').'>Tier '.($tir->tier_id-1).' '.(($tir->tier_id==1)?'Free':' ('.$tir->tier_months.' months)').' – '.(($tir->tier_amount>0)?'$'.number_format($tir->tier_amount,2,'.','').'/month,':'').' '.$tir->tier_fee.'% transaction fee</option>';
        }

        echo json_encode($return);
    }

    public function add_address(){
        $data_save['add_user_id'] = $this->input->get('user_id');
        $data_save['nick_name'] = $this->input->get('nick_name');
        $data_save['first_name'] = $this->input->get('first_name');
        $data_save['last_name'] = $this->input->get('last_name');
        $data_save['zip'] = $this->input->get('zip_code');
        $data_save['street'] = $this->input->get('street');
        $data_save['city'] = $this->input->get('city');
        $data_save['created_date'] = date('Y-m-d h:i:s');
        $data_save['country'] = $this->input->get('country');
        $data_save['state'] = ($this->input->get('country') == "US" ? $this->input->get('state') : "");
        $data_save['state_other'] = ($this->input->get('country') != "US" ? $this->input->get('other_state') : "");
        $data_save['phone'] = $this->input->get('phone');
        $billing = $this->input->get('default_billing');
        $shipping = $this->input->get('default_shipping');
        $data_save['default_billing_check'] = 'N';
        $data_save['default_shipping_check'] = 'N';

        if (!empty($billing)) {
            $data_save['default_billing_check'] = 'Y';
            $default_billing['default_billing_check'] = 'N';
            $this->common_model->commonUpdate('tbl_user_addreses', $default_billing, "add_user_id", $data_save['add_user_id']);
        }
        if (!empty($shipping)) {
            $data_save['default_shipping_check'] = 'Y';
            $default_shipping['default_shipping_check'] = 'N';
            $this->common_model->commonUpdate('tbl_user_addreses', $default_shipping, "add_user_id", $data_save['add_user_id']);
        }        

        $inserted_id = $this->common_model->commonSave('tbl_user_addreses', $data_save);
        if ($inserted_id != null) {
            $return['status'] = 200;
            $return['message'] = 'Address added successfully.';
        } else {
            $return['status'] = 202;
            $return['message'] = 'There was a problem.';
        }

        echo json_encode($return);
    }

    public function get_edit_address(){
        $add_id = $this->input->get('add_id');
        $address = $this->common_model->commonSelect('tbl_user_addreses', 'add_id', $add_id)->row();

        echo json_encode($address);
    }

    public function edit_address(){

        $user_id = $this->input->get('user_id');
        $add_id = $this->input->get('add_id');

        $data_save['nick_name'] = $this->input->get('nick_name');
        $data_save['first_name'] = $this->input->get('first_name');
        $data_save['last_name'] = $this->input->get('last_name');
        $data_save['zip'] = $this->input->get('zip_code');
        $data_save['street'] = $this->input->get('street');
        $data_save['city'] = $this->input->get('city');
        $data_save['created_date'] = date('Y-m-d h:i:s');
        $data_save['country'] = $this->input->get('country');
        $data_save['state'] = ($this->input->get('country') == "US" ? $this->input->get('state') : "");
        $data_save['state_other'] = ($this->input->get('country') != "US" ? $this->input->get('other_state') : "");
        $data_save['phone'] = $this->input->get('phone');
        $billing = $this->input->get('default_billing');
        $shipping = $this->input->get('default_shipping');
        $data_save['default_billing_check'] = 'N';
        $data_save['default_shipping_check'] = 'N';

        if (!empty($billing)) {
            $data_save['default_billing_check'] = 'Y';
            $default_billing['default_billing_check'] = 'N';
            $this->common_model->commonUpdate('tbl_user_addreses', $default_billing, "add_user_id", $user_id);
        }
        if (!empty($shipping)) {
            $data_save['default_shipping_check'] = 'Y';
            $default_shipping['default_shipping_check'] = 'N';
            $this->common_model->commonUpdate('tbl_user_addreses', $default_shipping, "add_user_id", $user_id);
        }        

        $response = $this->common_model->commonUpdate('tbl_user_addreses', $data_save, 'add_id', $add_id);

        if ($response != null) {
            $return['status'] = 200;
            $return['message'] = 'Address updated successfully.';
        } else {
            $return['status'] = 202;
            $return['message'] = 'There was a problem.';
        }

        echo json_encode($return);
    }

    public function get_wish_list()
    {
        $user_id = $this->input->get('user_id');
        $per_page = $this->input->get('per_page');
        $from = $this->input->get('counter');

        $counter = $this->input->get('counter') * $per_page;

        $query = "
                SELECT 
                prod.*, wish.id, store.store_name, store.store_id, store.store_url, images.img_name, category.cat_url as catURL, subCat.cat_url as subCatURL, subSubCat.cat_url as subSubCatURL
                FROM tbl_wish_list wish
                INNER JOIN tbl_products prod ON prod.prod_id = wish.product_id
                INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
                INNER JOIN tbl_product_images images ON prod.prod_id = images.img_prod_id
                INNER JOIN tbl_product_category prodCat ON prod.prod_id = prodCat.prod_id
                INNER JOIN tbl_categories category ON prodCat.category_id = category.cat_id
                LEFT JOIN tbl_categories subCat ON prodCat.sub_category_id = subCat.cat_id
                LEFT JOIN tbl_categories subSubCat ON prodCat.sub_sub_category_id = subSubCat.cat_id
                WHERE
                prod_is_delete = 0
                AND
                prod_status = 1
                AND
                wish.user_id = ".$user_id."
                                AND prod.prod_is_delete = 0
                                AND prod.qty > 0
                GROUP BY prod.prod_id
                ORDER BY wish.id $sort
                ";
        $return['total_wish'] = count($this->db->query($query)->result());

        $query = "
                SELECT 
                prod.*, wish.id, store.store_name, store.store_id, store.store_url, images.img_name, category.cat_url as catURL, subCat.cat_url as subCatURL, subSubCat.cat_url as subSubCatURL
                FROM tbl_wish_list wish
                INNER JOIN tbl_products prod ON prod.prod_id = wish.product_id
                INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
                INNER JOIN tbl_product_images images ON prod.prod_id = images.img_prod_id
                INNER JOIN tbl_product_category prodCat ON prod.prod_id = prodCat.prod_id
                INNER JOIN tbl_categories category ON prodCat.category_id = category.cat_id
                LEFT JOIN tbl_categories subCat ON prodCat.sub_category_id = subCat.cat_id
                LEFT JOIN tbl_categories subSubCat ON prodCat.sub_sub_category_id = subSubCat.cat_id
                WHERE
                prod_is_delete = 0
                AND
                prod_status = 1
                AND
                wish.user_id = ".$user_id."
                                AND prod.prod_is_delete = 0
                                AND prod.qty > 0
                GROUP BY prod.prod_id
                ORDER BY wish.id $sort
                 LIMIT ".$counter.",".$per_page."
                ";
        $data['result'] = $this->db->query($query)->result();

        $return['html'] = '';

        foreach ($data['result'] as $i => $wish) {
            $featured = '';
            $price = '';
            if($wish->prod_feature==1){
                $featured = "<span class='featured-absolt'></span>";
            }else{
                $featured = "";
            }
            if($wish->prod_onsale==1){
                $price = "<span class='sp3'>$".$wish->sale_price."</span><span class='sp4'>$".$wish->prod_price."</span>";
            }else{
                $price = "<span class='sp3'>$".$wish->prod_price."</span>";
            }

            $return['html'] .= "<div class='cloths-bx-2'><div class='cloths-bx-2-img'><a href='javascript:void' class='prod_click' data-id='".$wish->prod_id."'><img src='".base_url()."resources/prod_images/".$wish->img_name."' alt='' title='' class='img-responsive'/></a>".$featured."<button class='cross-btn' style='display: block;' data-id='".$wish->prod_id."'>x</button></div>";
            $return['html'] .= "<div class='cloths-bx-2-rgt'><div class='cloths-bx-hed2'><a href='javascript:void' data-toggle='tooltip' data-placement='top' title='Products Detail' class='prod_click' data-id='".$wish->prod_id."'>".$wish->prod_title."</a></div>";
            $return['html'] .= "<div class='cloths-bx-lowr3'><a href='javascript:void' data-toggle='tooltip' data-placement='top' title='Seller Store' class='store_click' data-id='".$wish->store_id."'>".$wish->store_name."</a><p>Price:".$price."</p></div>";
            $return['html'] .= "<div class='cloths-bx-lowr4'><input type='checkbox' class='compare-prod' data-id='".$wish->prod_id."' id='c".$wish->prod_id."' name='cc' /><label for='c".$wish->prod_id."'><span></span><p>Compare</p></label><a href='javascript:void' data-toggle='tooltip' data-placement='bottom' title='Add to Wishlist'></a></div></div></div>";
        }

        if(count($data['result'])<=0){
            $return['html'] = "<div class='float:left;width:100%;' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;float:left;width:100%;color: #000;text-align: center;\">No data found.</div>";
        }

        $return['counter'] = $counter + $per_page;

        echo json_encode($return);
    }

    public function remove_wish_item()
    {
        $wish_id = $this->input->get('prod_id');
        $user_id = $this->input->get('user_id');

        $this->db->where('product_id', $wish_id);
        $this->db->where('user_id', $user_id);
        $result = $this->db->delete('tbl_wish_list');

        if($result){
            $return['status'] = 200;
            $return['message'] = 'Your item has been deleted successfully.';
        }else{
            $return['status'] = 202;
            $return['message'] = 'There was a problem.';
        }

        echo json_encode($return);
    }

    public function get_profile()
    {
        $data['userid'] = $this->input->get('user_id');
        $user = $this->db->query("SELECT * FROM tbl_user WHERE user_id = " . $data['userid'] . " AND user_status = 1")->row();

        echo json_encode($user);
    }

    public function upload_profile_picture()
    {
        $ext = explode(".",urldecode($_FILES["file"]["name"]));
        $ext = $ext[count($ext)-1];
        $new_image_name = 'user_'. date('Y_m_d_h_i_s').'.'.$ext;
        $test = move_uploaded_file($_FILES["file"]["tmp_name"], "/public_html/dev/resources/user_profile_image/".str_replace('"',"",$new_image_name));
        echo json_encode($new_image_name);
    }

    function edit_profile(){
        $user_id = $this->input->get('user_id');
        $data_save['user_fname'] = $this->input->get('first_name');
        $data_save['user_lname'] = $this->input->get('last_name');
        $data_save['user_zip'] = $this->input->get('zip_code');
        $data_save['user_age'] = $this->input->get('user_age');
        $data_save['user_sex'] = $this->input->get('user_sex');
        $data_save['user_interests'] = $this->input->get('user_interests');
        $data_save['user_interests_second'] = $this->input->get('user_interests_second');
        $data_save['user_business_owner'] = $this->input->get('user_business_owner');
        $data_save['user_other_interest'] = ($this->input->get('user_interests') == "Other") ? $this->input->get('user_other_interest') : '';
        $data_save['user_other_interest_second'] = ($this->input->get('user_interests_second') == "Other") ? $this->input->get('user_other_interest_second') : '';

        if ($this->input->get('new_password') != '') {
            $data_save['user_password'] = $this->input->get('new_password');
            $data_save['user_password_encryp'] = md5($this->input->get('new_password'));
        }

        $userimage = str_replace('"',"",$this->input->get('img_name'));
        if($userimage){
            $data_save['user_profile_picture'] = $userimage;
        }

        $this->common_model->commonUpdate('tbl_user', $data_save, 'user_id', $user_id);

        require_once(ABSPATH.'wp-load.php');
        $userdata = get_user_by('email', $email); //get user wordpress Table id by email         
        $wp_data_save['ID'] = $userdata->ID; //user wordpress table id
        $wp_data_save['first_name'] = $this->input->get('first_name');
        $wp_data_save['last_name'] = $this->input->get('last_name');
        if ($userimage != '') {
            $wp_data_save['profile_image'] = $userimage;
        }
        if ($this->input->get('new_password')) {
            $wp_data_save['user_pass'] = $this->input->get('new_password');
            wp_set_password($wp_data_save['user_pass'], $wp_data_save['ID']);
        }
        wp_update_user($wp_data_save);  //wordpress user update function

        $return['status'] = 200;
        $return['messages'] = 'Profile edited successfully.';
        echo json_encode($return);
    }

    public function get_pending_orders()
    {        
        $data['step'] = $this->input->get('step');
        $data['userid'] = $this->input->get('user_id');            
        $store_id = $this->common_model->commonselect('tbl_stores', 'user_id', $data['userid'])->row()->store_id;
        $data['store_id'] = $store_id;
        $data['amount_received'] = number_format(store_amount_received($store_id), 2, '.', '');
        $data['amount_pending'] = number_format(store_amount_pending($store_id), 2, '.', '');

        $step = $data['step'];

        if($step==1){
            $order_status = array('Awaiting Tracking', 'Awaiting Pickup');
        }elseif($step==2){
            $order_status = array('In Process', 'Awaiting Pickup');
        }elseif($step==3){
            $order_status = 'Awaiting Pickup';
        }else{
            $order_status = array('completed', 'cancel');
        }

        $data['total_records'] = count($this->order_model->get_user_orders_app($store_id, $step, $order_status));
        
        $counter = $this->input->get('order_counter')*10;

        $data['results'] = $this->order_model->get_user_orders_app($store_id, $step, $order_status,10, $counter);

        $data['counter'] = $counter + 10;

        $data['html'] = '';

        foreach ($data['results'] as $i => $order) {

            $border = '';
            $days='';
            $order_status = '';
            $confirm = '';

            if($i==count($data['results'])-1){
                $border = " style='border-bottom: 0px;'";
            }else{
                $border = "";
            }

            if($order->order_prod_shipping=='Charge for Shipping'){
                $days = $order->order_prod_ship_days+(7-$order->DaysDiff);
            }elseif($order->order_prod_shipping=='Offer free Shipping'){
                $days = $order->order_prod_free_ship_days+(7-$order->DaysDiff);
            }else{
                $days = (7-$order->DaysDiff);
            }

            if($step==1){
                $order_status = 'Awaiting Tracking';
                $confirm = "";
            }elseif($step==2){
                $order_status = $order->order_prod_status;
                $confirm = "<div class='confirm submit'><a href='javascript:void' data-prodid='".$order->order_prod_id."' data-storeid='".$order->order_id."' onclick='status(this)'>Confirm</a></div>";
            }elseif($step==3){
                $order_status = 'Awaiting Pickup';
                $confirm = "<div class='confirm submit'><a href='javascript:void' data-prodid='".$order->order_prod_id."' data-storeid='".$order->order_id."' onclick='status(this)'>Confirm</a></div>";
            }else{
                $order_status = $order->order_prod_status;
                $confirm = "";
            }

            $profit = calculateSellerProfit($store_id,$order->order_id);

            $data['html'] .= "<div class='crt-order-frst-rw'".$border.">";
            $data['html'] .= "<div class='crt-order-frst-bx'><img src='".base_url()."resources/prod_images/".$order->order_prod_image."' alt='' title='' class='img-responsive'><p>".$order->order_prod_name."</p><div class='dlt-edt'><a href='javascript:void' class='view-order' data-id='".$order->order_id."'><img src='images/order-menu.png' alt='Edit'></a></div>".$confirm."</div>";
            $data['html'] .= "<div class='crt-order-frst-bx2'><div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Date:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>".date('m-d-Y', strtotime($order->order_date))."</p></div></div>";                                        
            $data['html'] .= "<div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Order #:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>".$order->order_id."</p></div></div>";
            if($step==1){
                $data['html'] .= "<div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Tracking #:</h5></div><div class='crt-order-frst-bx2-rw-right'><p class='prc-ext'>Required ".(($days > 0 && $days == 1) ? $days . " Day" : $days ." Days")."</p></div></div>";
            }elseif($step==2){
                $data['html'] .= "<div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Tracking #:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>".(($order->order_prod_tracking_number))."</p></div></div>";
            }elseif($step==4){
                $data['html'] .= "<div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Tracking #:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>".((!empty($order->order_prod_tracking_number))?$order->order_prod_tracking_number:'N/A')."</p></div></div>";
            }
            $data['html'] .= "<div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Price:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>$".number_format($order->order_prod_unit_price, 2, '.', '')."</p></div></div>";                                        
            $data['html'] .= "<div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Shipping:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>$".number_format($order->order_prod_ship_price, 2, '.', '')."</p></div></div>";
            $data['html'] .= "<div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Status:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>".$order_status."</p></div></div>";
            $data['html'] .= "<div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>My Profit:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>$".$profit."</p></div></div></div></div>";        
        }

        if($data['total_records']<=0){
            $data['html'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;float:left;width:100%;color: #000;text-align: center;\">No data found.</div>";
        }

        /*echo "<pre>";
        print_r($data);
        exit;*/

        echo json_encode($data);
    }

    public function confirm_order(){
        $order_id = $this->input->get('order_id');
        $prod_id = $this->input->get('Prod_id');
        $user_id = $this->input->get('user_id');
        $response = $this->confirm_order_app($order_id,$prod_id,$user_id);
        if ($response == 1) {
            $return['status'] = 202;
            $return['message'] = 'There was a problem.';
            die;
        } else {
            $this->disburse_funds($order_id,$user_id);
            $data['order_products'] = $this->db->query("SELECT * FROM tbl_order_products o WHERE o.`order_id` = " . $order_id . "")->result();
            $data['completed_products'] = $this->db->query("SELECT * FROM tbl_order_products o WHERE o.`order_id` = " . $order_id . " AND order_prod_status = 'completed'")->result();
            if(count($data['order_products'])==count($data['completed_products'])){
                $dbdata['order_status'] = 'completed';
                $dbdata['order_completion_date'] = date('Y-m-d H:i:s');
                $this->common_model->commonUpdate('tbl_orders', $dbdata, "order_id", $order_id);
            }
            $return['status'] = 202;
            $return['message'] = 'Order has been updated successfully.';
        }
        echo json_encode($return);
    }

    public function confirm_order_app($order_id,$prod_id,$user_id){
        $data['userid'] = $user_id;
        $data['store_info'] = $this->common_model->commonselect('tbl_stores', 'user_id', $data['userid'])->row();
        $data['prod_info'] = $this->common_model->commonselect2('tbl_order_products', 'order_prod_id', $prod_id,'order_id',$order_id)->row();
        $store_id = $data['store_info']->store_id;
        $data['store_id'] = $data['store_info']->store_id;
        $this->db->select('*');
        $this->db->from('tbl_order_products');
        $this->db->join('tbl_orders', 'tbl_order_products.order_id = tbl_orders.order_id');
        $this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');
        $this->db->where('tbl_orders.order_id', $order_id);
        $this->db->where('tbl_order_products.order_prod_store_id', $store_id);
        if($data['prod_info']->order_prod_shipping!='Local Pick Up'){
            $this->db->where('tbl_order_products.order_prod_shipping !=', 'Local Pick Up');
        }else{
            $this->db->where('tbl_order_products.order_prod_shipping', 'Local Pick Up');
        }
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            $response = $result->row();
        }
        $update_data = array(
            'order_prod_status' => 'completed',
            'order_prod_completion_date' => date('Y-m-d h:i:s'),
        );

        if($data['prod_info']->order_prod_shipping!='Local Pick Up'){
            $response = $this->common_model->commonUpdate3('tbl_order_products', $update_data, 'order_id', $order_id, 'order_prod_store_id', $store_id,'order_prod_shipping !=','Local Pick Up');
        }else{
            $response = $this->common_model->commonUpdate3('tbl_order_products', $update_data, 'order_id', $order_id, 'order_prod_store_id', $store_id,'order_prod_shipping','Local Pick Up');
        }

        if ($response == TRUE) {
            return 0;
        } else {
            return 1;
        }
    }

    private function disburse_funds($order_id,$user_id){
        $data['completed'] = $this->db->query("
                                    SELECT * 
                                    FROM tbl_order_products o 
                                    WHERE o.`order_id` = " . $order_id . " AND 
                                    order_prod_status = 'completed' AND
                                    seller_id = ".$user_id."
                                    ")->result();
        $data['all'] = $this->db->query("
                                    SELECT * 
                                    FROM tbl_order_products o 
                                    WHERE o.`order_id` = " . $order_id . " AND 
                                    seller_id = ".$user_id."
                                    ")->result();
        if(count($data['completed']) == count($data['all'])) {
            $store_id = $this->db->query("select store_id  from tbl_stores where user_id = ".$user_id."")->row();
            $capture_id = $this->db->query("
                                    SELECT paypal_capture_id 
                                    FROM tbl_order_seller_info s 
                                    WHERE s.`order_id` = " . $order_id . " AND 
                                    s. store_id = $store_id->store_id
                                    ")->row();
            if($capture_id->paypal_capture_id != ''){
                $var = $this->paypal->GetToken();
                $var = json_decode($var);
                $disburse_api['paypalCaptureDetail'] = $this->paypal->DisburseFunds($var->token_type, $var->access_token, $capture_id->paypal_capture_id);
                $this->common_model->commonUpdate2('tbl_order_seller_info', $disburse_api, "order_id", $order_id, 'store_id',$store_id->store_id );
                return 'Updated';
            }else{
                return NULL;
            }
            
        }else {
            return NULL;
        }
    }

    public function order_details(){
        $OrderId = $this->input->get('order_id');
        $data['userid'] = $this->input->get('user_id');
        $data['step'] = $this->input->get('step');
        $data['store_info'] = $this->common_model->commonselect('tbl_stores', 'user_id', $data['userid'])->row();
        $store_id = $data['store_info']->store_id;
        $data['store_id'] = $data['store_info']->store_id;
        if($data['step']==3){
            $data['order_detail_results'] = $this->order_model->view_order_pickup_details($OrderId, $store_id);
        }else{
            $data['order_detail_results'] = $this->order_model->view_order_details($OrderId, $store_id);
        }        
        $data['results'] = $this->order_model->view_order($OrderId, $store_id);
        $data['default_shipping_address'] = json_decode($data['results']->order_shipping_address);
        $data['default_billing_address'] = json_decode($data['results']->order_billing_address);

        $date1 = date_create(date("Y-m-d", strtotime($data['results']->order_date)));
        $date2 = date_create(date("Y-m-d"));
        $diff = date_diff($date1,$date2);
        $data['dayz'] =  7 - $diff->format("%a");

        $data['bill'] = number_format($data['results']->order_grand_total, 2, '.', '');

        $data['order_hmtl'] = '';

        $data['order_hmtl'] .= "<div class='banything-row'><div class='banything-lbl'>Order Date:</div><div class='banything-dtl'>".date('F d, Y', strtotime($data['results']->order_date))."</div></div>";
        $data['order_hmtl'] .= "<div class='banything-row'><div class='banything-lbl'>Order ID:</div><div class='banything-dtl'>".$data['results']->order_id."</div></div>";
        $data['order_hmtl'] .= "<div class='banything-row'><div class='banything-lbl'>Order Total:</div><div class='banything-dtl'>$".number_format($data['results']->order_grand_total, 2, '.', '')."</div></div>";

        $status = '';

        if($data['step']==1){
            $status = 'Awaiting Tracking';
        }elseif($data['step']==2){
            $status = 'In Process';
        }elseif($data['step']==3){
            $status = 'Awaiting Pickup';
        }elseif($data['step']==4){
            if($data['results']->order_status == 'cancel'){
                $status = 'Canceled';
            }else{
                $status = 'Completed';
            }
        }
        $data['order_hmtl'] .= "<div class='banything-row'><div class='banything-lbl'>Order Status:</div><div class='banything-dtl'>".$status."</div></div>";

        $data['address_hmtl'] = '';

        if(!empty($data['default_shipping_address'])){
            $data['address_hmtl'] .= "<div class='shipbil-lcol'><div class='inany-wrapper'><div class='inany-row'><h3>Shipping Address</h3></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Name:</div><div class='inany-dtl'>".$data['default_shipping_address']->shipping_first_name." ".$data['default_shipping_address']->shipping_last_name."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Telephone:</div><div class='inany-dtl'>".(($data['default_shipping_address']->shipping_phone!='')?$data['default_shipping_address']->shipping_phone:'N/A')."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Street:</div><div class='inany-dtl'>".$data['default_shipping_address']->shipping_street."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>City:</div><div class='inany-dtl'>".$data['default_shipping_address']->shipping_city."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>State:</div><div class='inany-dtl'>".(($data['default_shipping_address']->shipping_country=='US')?get_state_name($data['default_shipping_address']->shipping_state):$data['default_shipping_address']->shipping_state_other)."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Country:</div><div class='inany-dtl'>".get_country_name($data['default_shipping_address']->shipping_country)."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Zip:</div><div class='inany-dtl'>".$data['default_shipping_address']->shipping_zip."</div></div></div></div>";
        }

        if(!empty($data['default_billing_address'])){
            $data['address_hmtl'] .= "<div class='shipbil-lcol'><div class='inany-wrapper'><div class='inany-row'><h3>Billing Address</h3></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Name:</div><div class='inany-dtl'>".$data['default_billing_address']->billing_first_name." ".$data['default_billing_address']->billing_last_name."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Telephone:</div><div class='inany-dtl'>".(($data['default_billing_address']->billing_phone!='')?$data['default_billing_address']->billing_phone:'N/A')."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Street:</div><div class='inany-dtl'>".$data['default_billing_address']->billing_street."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>City:</div><div class='inany-dtl'>".$data['default_billing_address']->billing_city."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>State:</div><div class='inany-dtl'>".(($data['default_billing_address']->billing_country=='US')?get_state_name($data['default_billing_address']->billing_state):$data['default_billing_address']->billing_state_other)."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Country:</div><div class='inany-dtl'>".get_country_name($data['default_billing_address']->billing_country)."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Zip:</div><div class='inany-dtl'>".$data['default_billing_address']->billing_zip."</div></div></div></div>";
        }

        $data['order_summary'] = '';

        $data['order_summary'] .= "<div class='inany-row'><h3>Order Summary</h3></div>";
        $data['order_summary'] .= "<div class='inany-row'><div class='inany-lbl'>Order Sub Total:</div><div class='inany-dtl'>$".number_format($data['results']->order_sub_total, 2, '.', '')."</div></div>";
        $data['order_summary'] .= "<div class='inany-row'><div class='inany-lbl'>Order Discount:</div><div class='inany-dtl'>$".number_format($data['results']->order_discount, 2, '.', '')."</div></div>";
        $data['order_summary'] .= "<div class='inany-row'><div class='inany-lbl'>Shipping &amp; Handing:</div><div class='inany-dtl'>$".number_format($data['results']->order_shipping_price, 2, '.', '')."</div></div>";

        $data['products'] = '';
        foreach ($data['order_detail_results'] as $i => $prod){
            $data['filters'] = json_decode($prod['order_prod_filters']);
            $filters = '';
            if(count($data['filters']) > 0){
                foreach ($data['filters'] as $i => $filter) {
                    $filters .= "<p><b>".$filter->filterName."</b>:".$filter->filtervalue."</p>";
                }
            }

            $ack = '';
            if ($prod['order_prod_completion_date'] != NULL){
                $ack .= "<p>Acknowledged On<br/>";
                $ack .= date('F j, Y', strtotime($prod['order_prod_completion_date']))."</p>";
            }            

            $shipping='';
            if($prod['order_prod_shipping']=='Charge for Shipping'){
                $shipping = $prod['order_prod_shipping_methods'];
            }else{
                $shipping = '';
            }

            $date1 = date_create(date("Y-m-d", strtotime($prod['order_date'])));
            $date2 = date_create(date("Y-m-d"));
            $diff = date_diff($date1,$date2);
            $dayz =  7 - $diff->format("%a");

            $days='';
            if($prod['order_prod_shipping']=='Charge for Shipping'){
                $days = $prod['order_prod_ship_days']+$dayz;
            }elseif($prod['order_prod_shipping']=='Offer free Shipping'){
                $days = $prod['order_prod_free_ship_days']+$dayz;
            }else{
                $days = +$dayz;
            }

            $pending = '';
            if($prod['order_prod_status']!='Delivered' && $prod['order_prod_status']!='In Process' && $prod['order_prod_status']!='completed' && $prod['order_prod_status']!='cancel'){
                $pending = true;
            }else{
                $pending = false;
            }

            $edit = '';
            if(empty($prod['order_prod_tracking_number']) && $prod['order_prod_status'] != 'completed'){
                $edit = "<a href='javascript:void' class='editMethod' data-prod='".$prod['order_prod_id']."' data-id='".(($shipping!='')?$shipping:'')."'>Edit</a>";
            }else{
                $edit = "";
            }

            $data['products'] .= "<div class='btm-odbdr'><div class='odrList-wrap'><div class='myfan-listwrap'><div class='myfan-listrow'>";
            $data['products'] .= "<div class='myfan-img'><table cellpadding='0' cellspacing='0' border='0'><tbody><tr><td><a href='javascript:void'><img src='".base_url()."resources/prod_images/thumb/".$prod['order_prod_image']."' alt=''></a></td></tr></tbody></table></div><div class='myfan-right'><div class='myfan-rinner'><div class='myfan-rtoptxt'><p>".$prod['order_prod_name']."</p><p>".$filters."</p><p>".(($prod['order_prod_return'] != '')?$prod['order_prod_return']:'')."<br> Qty: ".$prod['order_prod_qty']."</p>".$ack."</div>";
            $data['products'] .= "<div class='edt-ship'><div class='shipbil-rcol sec03'><div class='fan-con'><div class='shipbil-rcol'><div class='inany-wrapper odrsummary'><div class='inany-row'><h3>Shipping Method</h3><div class='inany-lk'>".$edit."</div>";
            $data['products'] .= "<div class='inany-pop pop-".$prod['order_prod_id']."' style='display: none;'><div class='cart-pro-btm'><h2>Available Shipping Type:</h2><select id='select-value-".$prod['order_prod_id']."' name='select-value' class='select-value'><option value='Charge for Shipping' ".(($prod['order_prod_shipping'] == "Charge for Shipping")?"selected":"").">Charge for Shipping</option><option value='Offer free Shipping' ".(($prod['order_prod_shipping'] == "Offer free Shipping")?"selected":"").">Offer free Shipping</option><option value='Local Pick Up' ".(($prod['order_prod_shipping'] == "Local Pick Up")?"selected":"").">Local Pickup</option></select>";
            $data['products'] .= "<div id='ship-to-me-".$prod['order_prod_id']."' class='selection' style='display: none;'><h2>Shipping Method</h2><div class='clk-shp-sho'><div class='clk-shp-sho-rw'><div class='clk-shp-sho-rw-lft'><input type='radio' id='r-".$prod['order_prod_id']."-1' name='cc-".$prod['order_prod_id']."' value='USPS'><label for='r-".$prod['order_prod_id']."-1'><span></span><p>USPS</p></label></div><div class='clk-shp-sho-rw-lft'><input type='radio' id='r-".$prod['order_prod_id']."-2' name='cc-".$prod['order_prod_id']."' value='FedEx'><label for='r-".$prod['order_prod_id']."-2'><span></span><p>FedEx</p></label></div><div class='clk-shp-sho-rw-lft'><input type='radio' id='r-".$prod['order_prod_id']."-3' name='cc-".$prod['order_prod_id']."' value='DHL'><label for='r-".$prod['order_prod_id']."-3'><span></span><p>DHL</p></label></div><div class='clk-shp-sho-rw-lft'><input type='radio' id='r-".$prod['order_prod_id']."-4' name='cc-".$prod['order_prod_id']."' value='UPS'><label for='r-".$prod['order_prod_id']."-4'><span></span><p>UPS</p></label></div></div></div></div><div class='clk-shp-sho-rw-lft'><button type='button' id='save' style='padding: 5px 5px;margin-top:5px;'>Save</button></div></div></div></div>";
            $data['products'] .= "<div class='inany-row'>".(($shipping!='')?$shipping."</br>":'')."".$prod['order_prod_shipping']." ".(($pending)?"(".$days.")":'')."</div><div class='inany-row'><div class='inany-lbl'>Item Price:</div><div class='inany-dtl'>$".$prod['order_prod_total_price']."</div></div><div class='inany-row'><div class='inany-lbl'>Shipping:</div><div class='inany-dtl'>$".number_format($prod['order_prod_ship_price'], 2, '.', '')."</div></div></div></div></div></div></div></div></div></div></div></div></div>";
        }

        $data['products'] .= "<div class='btorder-row'><div class='btorder-lk'><a href='manage-orders.html'>&lt;&lt; Back to Orders</a></div></div>";

        /*echo "<pre>";
        print_r($data);
        exit;*/

        $data['acknowledge'] = '';
        if($data['results']->order_completion_date!=''){
            $data['acknowledge'] = "<div class='inany-row'><h3>Payment Status</h3></div><div class='inany-row'>Acknowledged On ".date('F d, Y', strtotime($data['results']->order_completion_date))."</div>";
        }else{
            $data['acknowledge'] = "";
        }
        echo json_encode($data);
    }

    public function user_orders() 
    {
        $data['buyer_id'] = $this->input->get('user_id');
        $data['total_records'] = count($this->order_model->get_my_orders_app($data['buyer_id']));

        $counter = $this->input->get('order_counter')*10;
        
        $data['results'] = $this->order_model->get_my_orders_app($data['buyer_id'], 10,$counter);

        $data['html'] = "";
        foreach ($data['results'] as $i => $order){
            $border = '';
            $tracking = '';
            if($i==count($data['results'])-1)
            {
                $border = " style='border-bottom: 0px;'";
            }
            else{
                $border = "";
            }

            if($order->order_prod_tracking_number !=''){
                $tracking = $order->order_prod_tracking_number;
            }elseif($order->order_prod_tracking_number =='' && $order->order_prod_shipping!='Local Pick Up' && $order->order_status!='cancel'){
                $tracking = "Waiting";
            }else{
                $tracking = "Local Pickup";
            }

            $status = '';
            if($order->order_status == 'cancel'){
                $status = 'Cancel';
            }elseif($order->order_status == 'Awaiting Tracking' || $order->order_status == 'Awaiting Pickup'){
                $status = 'Processing';
            }elseif($order->order_status == 'completed'){
                $status = 'Completed';
            }elseif($order->order_status == 'In Process'){
                $status = 'Processing';
            }

            $shipping = '';
            if ($order->order_shipping_price == 0) {
                $shipping = '0.00';
            } else {
                $shipping = $order->order_shipping_price;
            }

            $data['html'] .= "<div class='crt-order-frst-rw'".$border."><div class='crt-order-frst-bx'><img src='".base_url()."resources/prod_images/".$order->order_prod_image."' alt='' title='' class='img-responsive'><p>".$order->order_prod_name."</p><div class='dlt-edt'><a href='javascript:void' class='view_order' data-id='".$order->order_id."'><img src='images/order-menu.png' alt='Menu'></a></div></div>";
            $data['html'] .= "<div class='crt-order-frst-bx2'><div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Date:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>".date('m-d-Y', strtotime($order->order_date))."</p></div></div>";
            $data['html'] .= "<div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Order ID:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>".$order->order_id."</p></div></div>";
            $data['html'] .= "<div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Tracking #:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>".$tracking."</p></div></div>";
            $data['html'] .= "<div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Price:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>$".$order->order_sub_total."</p></div></div>";
            $data['html'] .= "<div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Shipping:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>$".$shipping."</p></div></div>";
            $data['html'] .= "<div class='crt-order-frst-bx2-rw'><div class='crt-order-frst-bx2-rw-left'><h5>Status:</h5></div><div class='crt-order-frst-bx2-rw-right'><p>".$status."</p></div></div></div></div>";
        }

        if($data['total_records']<=0){
            $data['html'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;float:left;width:100%;color: #000;text-align: center;\">No data found.</div>";
        }

        $data['counter'] = $counter + 10;

        /*echo "<pre>";
        print_r($data);
        exit;*/
        echo json_encode($data);
    }

    public function get_store_details()
    {
        $data['userid'] = $this->input->get('user_id');
        $store_id = $this->input->get('store_id');

        $data['pagedata'] = $this->db->query("SELECT *
                                              FROM tbl_stores store
                                              INNER JOIN tbl_user user  ON store.user_id = user.user_id
                                              WHERE store.store_is_hide = 0 AND store.store_id = '".$store_id."' AND user.user_is_delete = 0 AND user.user_status = 1")->row();    

        $data['seller_data'] = $this->common_model->commonselect('tbl_user', 'user_id', $data['pagedata']->user_id)->row();

        if($data['userid']!=''){

            $data['userdata']    = $this->common_model->commonselect('tbl_user', 'user_id', $data['userid'])->row();

            $data['fans'] = count($this->db->query("
                                            SELECT user.user_city, user.user_state,user.user_profile_picture,
                                            user.user_fname, user.user_lname,user.user_register_date,
                                            store.store_name, store.store_logo_image, store.store_city,
                                            store.store_state, store.store_is_hide, store.store_url
                                            FROM tbl_fans fan
                                            INNER JOIN tbl_user user ON fan.fan_id = user.user_id
                                            LEFT JOIN tbl_stores store ON user.user_id = store.user_id
                                            WHERE store.store_is_hide = 0 AND user.user_status = 1 AND user.user_is_delete = 0 AND user.user_flagged_status = 0 AND fan.store_id = " . $data['pagedata']->store_id. "")->result());


            $where2 = "fan_id ='" . $data['userid'] . "' AND store_id ='" . $data['pagedata']->store_id . "'";

            if ($data['pagedata']->store_discount > 0 && $data['pagedata']->store_discount_item > 0) {
                $data['offer'] = "<div class='pls-itm'><div class='wrapper'><div class='pls-itm-sec'><h4>".$data['pagedata']->store_discount."% off when you buy ".$data['pagedata']->store_discount_item."+ Items</h4></div></div></div>";
            }else{
                $data['offer'] = "";
            }

            $data['is_fan'] = count($this->common_model->commonselect_array('tbl_fans', $where2, 'fan_id')->row());
        }        

        $data['cat_html'] = $this->common_model->get_categories_html();

        echo json_encode($data);
        /*echo "<pre>";
        echo $this->db->last_query();
        print_r($data);
        exit;*/
    }

    public function get_store_prod()
    {
        $store_id = $this->input->get('store_id');

        $data['pagedata'] = $this->db->query("SELECT *
                                              FROM tbl_stores store
                                              INNER JOIN tbl_user user  ON store.user_id = user.user_id
                                              WHERE store.store_is_hide = 0 AND store.store_id = '".$store_id."' AND user.user_is_delete = 0 AND user.user_status = 1")->row();    

        $per_page = $this->input->get('per_page');

        $counter = $this->input->get('counter') * $per_page;

        $user_id = $this->input->get('user_id');

        $data['counter'] = $counter;
        $data['store_id'] = $store_id;

        $sort = '';

        if ($this->input->get('sort_by') != '') {
            $data['sort_by'] = @$this->input->get('sort_by');
            if ($data['sort_by'] == "new") {
                $sort = 'prod.prod_id DESC';
            } else if ($data['sort_by'] == "featured") {
                $sort = 'prod.prod_feature DESC';
            } else if ($data['sort_by'] == "price_lh") {
                $sort = 'prod.prod_price ASC';
            } else if ($data['sort_by'] == "pricehl") {
                $sort = 'prod.prod_price DESC';
            }
        }

        $is_limit = '';

        $data['total_rows'] = count($this->users_model->get_prod_list_app($store_id, $sort, $to = 0, $from = '', $is_limit = 0)->result_array());
        $data['wish_list'] = $this->users_model->get_prod_list_app($store_id, $sort, $counter,$per_page, 1)->result_array();

        $data['tab1'] = "";
        $data['tab2'] = "";

        if(count($data['wish_list'])<=0){
            $data['tab1'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No data found.</div>";
            $data['tab2'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No data found.</div>";
        }

        $data['tab1'] .= "<div class='cloths-row'>";
        $row_start = 0;
        foreach ($data['wish_list'] as $i => $wish_list){
            $class = '';
            $feature = '';

            if(($i+1)%2==0 && $i>0){
                $class = ' clths-rgt mrg-rgt-0';
            }else{
                $class = '';
            }

            if($wish_list['prod_feature']==1){
                $feature = 'featured-absolt';
            }else{
                $feature = '';
            }

            if($row_start == 1){
                $data['tab1'] .= "<div class='cloths-row'>";
                $row_start = 0;
            }

            $price_1 = '';
            $price_2 = '';

            if($wish_list['prod_onsale']==1){
                $price_1 = "Price:<span class='sp1'>$".$wish_list['sale_price']."</span><span class='sp2'>$".$wish_list['prod_price']."</span>";
                $price_2 = "Price:<span class='sp3'>$".$wish_list['sale_price']."</span><span class='sp4'>$".$wish_list['prod_price']."</span>";
            }else{
                $price_1 = "Price:<span class='sp1'>$".$wish_list['prod_price']."</span>";
                $price_2 = "Price:<span class='sp3'>$".$wish_list['prod_price']."</span>";
            }

            $active = '';
            if(is_numeric($user_id)){
                if($this->check_wishlist_item($wish_list['prod_id'],$user_id)){
                    $active = 'active';
                }
            }

            $data['tab1'] .= "<div class='cloths-bx".$class."'><div class='cloths-bx-img-sec'><a href='javascript:void' class='prod_click' data-id='".$wish_list['prod_id']."'><img src='".base_url()."resources/prod_images/".$wish_list['img_name']."' alt='' title='' class='img-responsive'/></a><span class='".$feature."'></span></div>";
            $data['tab1'] .= "<div class='cloths-bx-hed'><a href='javascript:void' data-toggle='tooltip' data-placement='top' title='Products Detail'  class='prod_click' data-id='".$wish_list['prod_id']."'>".$wish_list['prod_title']."</a></div>";
            $data['tab1'] .= "<div class='cloths-bx-lowr1'><a href='javascript:void' data-toggle='tooltip' data-placement='top' class='store_click'  data-id='".$wish_list['prod_store_id']."'>".$wish_list['store_name']."</a><p>".$price_1."</p></div>";
            $data['tab1'] .= "<div class='cloths-bx-lowr2'><input type='checkbox' class='compare-prod' data-id='".$wish_list['prod_id']."' id='c".$wish_list['prod_id']."' name='cc' /><label for='c".$wish_list['prod_id']."'><span></span><p>Compare</p></label><a href='javascript:void' data-id='".$wish_list['prod_id']."' class='not_logged_in wish ".$active." wish".$wish_list['prod_id']."' data-toggle='tooltip' data-placement='bottom' title='Add to Wishlist'></a></div></div>";

            $data['tab2'] .= "<div class='cloths-bx-2'><div class='cloths-bx-2-img'><a href='javascript:void' class='prod_click' data-id='".$wish_list['prod_id']."'><img src='".base_url()."resources/prod_images/".$wish_list['img_name']."' alt='' title='' class='img-responsive'/></a><span class='".$feature."'></span></div><div class='cloths-bx-2-rgt'>";
            $data['tab2'] .= "<div class='cloths-bx-hed2'><a href='javascript:void' data-toggle='tooltip' data-placement='top' title='Products Detail' class='prod_click' data-id='".$wish_list['prod_id']."'>".$wish_list['prod_title']."</a></div>";
            $data['tab2'] .= "<div class='cloths-bx-lowr3'><a href='javascript:void' data-toggle='tooltip' data-placement='top' title='Seller Store'>".$wish_list['store_name']."</a><p>".$price_2."</p></div>";
            $data['tab2'] .= "<div class='cloths-bx-lowr4'><input type='checkbox' class='compare-prod' data-id='".$wish_list['prod_id']."' id='c".$wish_list['prod_id']."' name='cc' /><label for='c".$wish_list['prod_id']."'><span></span><p>Compare</p></label><a href='javascript:void' data-id='".$wish_list['prod_id']."' class='not_logged_in wish ".$active." wish".$wish_list['prod_id']."' data-toggle='tooltip' data-placement='bottom' title='Add to Wishlist'></a></div></div></div>";

            if(($i+1)%2==0 && $i>0){
                $data['tab1'] .= "</div>";
                $row_start = 1;
            }
        }

        $data['counter'] = $counter + $per_page;

        echo json_encode($data);
    }

    function check_wishlist_item($prod_id = '',$user_id = ''){
        $query = "SELECT *
                FROM tbl_wish_list
                WHERE product_id = $prod_id AND user_id = $user_id";
        $results = $this->db->query($query);
        if (count($results->row()) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function add_to_wish_list()
    {
        $data_save = array();
        $user_id = $this->input->get('user_id');
        $prod_id = $this->input->get('prod_id');
        if ($user_id != '') {
            $result = $this->common_model->commonselect2('tbl_wish_list', 'product_id', $prod_id, 'user_id', $user_id);
            if ($result->num_rows() > 0) {
                $data['status'] = '200';
                $data['message'] = 'Item already exists in your wishlist.';
            } else {
                $data_save['user_id']       = $user_id;
                $data_save['product_id']    = $prod_id;
                $data_save['wish_datetime'] = date("Y-m-d H:i:s");
                $res                        = $this->common_model->commonSave('tbl_wish_list', $data_save);
                if ($res) {
                    $data['status'] = '201';
                    $data['message'] = 'Item added to your wishlist.';
                } else {
                    $data['status'] = '203';
                    $data['message'] = 'Error occured.';
                }
            }
        } else {
            $data['status'] = '203';
            $data['message'] = 'Please login to continue.';
        }

        echo json_encode($data);
    }

    public function add_fan()
    {
        $user_id = $this->input->get('user_id');
        $store_id = $this->input->get('store_id');
        $where = "fan_id ='" . $user_id . "' AND store_id ='".$store_id."'";

        $data['is_fan'] = count($this->common_model->commonselect_array('tbl_fans', $where, 'fan_id')->row());

        if ($data['is_fan'] == 0) {
            $array['store_id'] = $store_id;
            $array['fan_id']   = $user_id;
            $array['fan_date'] = date('Y-m-d H:i:s');
            $user_id           = $this->common_model->commonSave('tbl_fans', $array);
            $data['status'] = '200';
            $data['message'] = 'Made you a fan.';
        } else {
            $user_id           = $this->common_model->commonDelete('tbl_fans','fan_id',$user_id);
            $data['status'] = '201';
            $data['message'] = 'Already a fan.';
        }

        echo json_encode($data);
    }

    public function get_story_data()
    {
        $store_id = $this->input->get('store_id');
        
        $data['pagedata'] = $this->db->query("SELECT *
                                              FROM tbl_stores store
                                              INNER JOIN tbl_user user  ON store.user_id = user.user_id
                                              WHERE store.store_is_hide = 0 AND store.store_id = '".$store_id."' AND user.user_is_delete = 0 AND user.user_status = 1")->row();

        $data['stores'] = $this->seller_model->get_favourite_shops($data['pagedata']->user_id, 'DESC', 0, 3, 1)->result_array();

        $sort              = 'prod.prod_id DESC';
        $data['wish_list'] = $this->users_model->get_prod_list($data['pagedata']->store_id, $sort, 0, 3, 1)->result_array();

        $data['shop'] = "";

        foreach ($data['wish_list'] as $i => $prod){

            $price = '';

            if($prod['prod_onsale']==1){
                $price = "Price:<span class='sp1'>$".$prod['sale_price']."</span><span class='sp2'>$".$prod['prod_price']."</span>";
            }else{
                $price = "Price:<span class='sp3'>$".$prod['prod_price']."</span>";
            }
            $data['shop'] .= "<div><img src='".base_url()."resources/prod_images/".$prod['img_name']."'><a href class='pro-name'>".$prod['prod_title']."</a><a href='javascript:void' data-id='".$prod['prod_store_id']."' class='store_click' style='float: left;width: 100%;'><p>".$prod['store_name']."</p></a><p>".$price."</span></p></div>";
        }

        if(count($data['wish_list'])<=0){
            $data['shop'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No data found.</div>";
        }

        $data['store'] = "";

        foreach ($data['stores'] as $i => $store){
            $image = '';
            if($store['store_banner_image']!=''){
                $image = base_url()."resources/seller_account_image/banner/".$store['store_banner_image'];
            }else{
                $image = base_url()."resources/no_banner/no_banner.png";
            }
            $data['store'] .= "<div><a href='javascript:void' data-id='".$store['store_id']."' class='store_click'><img src='".$image."'><div class='fav-shp'></a><a href='javascript:void' data-id='".$store['store_id']."' class='store_click'><img src='".base_url()."resources/seller_account_image/logo/".$store['store_logo_image']."'></a><div class='fav-shp-rgt'><a href='javascript:void' data-id='".$store['store_id']."' class='store_click'><p>".$store['store_name']."</p></a><span>".$store['store_city'].", ".$store['store_state']."</span></div></div></div>";
        }        

        if(count($data['stores'])<=0){
            $data['store'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No data found.</div>";
        }
        /*echo "<pre>";
        print_r($data);
        exit;*/

        echo json_encode($data);
    }

    public function user_report()
    {
        $store_id = $this->input->get('store_id');
        $data['pagedata'] = $this->db->query("SELECT *
                                              FROM tbl_stores
                                              WHERE store_id = '".$store_id."'")->row();        
        $report_count = $data['pagedata']->store_reported + 1;
        $update_data = array('store_reported' => $report_count);
        $this->common_model->commonUpdate('tbl_stores', $update_data, 'store_id', $store_id);
        $history['flag_user_id'] = $data['pagedata']->user_id;
        $history['flag_date'] = date('Y-m-d H:i:s');
        $result = $this->common_model->commonSave('tbl_user_flag_history', $history);
        if($result){
            $return['status'] = 200;
            $return['message'] = 'Store reported Successfully.';
        }else{
            $return['status'] = 201;
            $return['message'] = 'There was a problem.';
        }

        echo json_encode($return);
    }

    public function update_tracking() {
        $order_id = $this->input->get('order_id');
        $user_id = $this->input->get('user_id');
        $tracking_number = $this->input->get('tracking_number');
        $response = $this->order_model->update_tracking_app($order_id,$user_id,$tracking_number);
        if ($response == 1) {
            $return['status'] = 201;
            $return['message'] = 'Network Error.';
        } else {
            $dbdata['order_status'] = 'In Process';
            $this->common_model->commonUpdate('tbl_orders', $dbdata, "order_id", $order_id);
            $return['status'] = 200;
            $return['message'] = 'Order has been updated successfully.';
        }

        echo json_encode($return);
    }

    public function save_shipping(){

        $return['status'] = 201;
        $return['message'] = 'You are not authorized to access this page.';

        $dataDB['order_prod_shipping'] = $this->input->get('ship_type');
        $dataDB['order_prod_shipping_methods'] = $this->input->get('ship_method');
        $data['product_id'] = $this->input->get('prod_id');
        $data['order_id'] = $this->input->get('order_id');

        $data['product_id'] = $this->common_model->commonselect2('tbl_order_products', 'order_id', $data['order_id'],'order_prod_id',$data['product_id'])->row()->order_products_id;

        if($data['order_prod_shipping']!='Local Pick Up'){
                $dataDB['order_prod_status'] = 'Awaiting Tracking';
        }else{
            $dataDB['order_prod_status'] = 'Awaiting Pickup';
        }

        $updated = $this->common_model->commonUpdate('tbl_order_products', $dataDB, "order_products_id",$data['product_id']);

        $is_all_local = 1;

        $order_id = $this->common_model->commonselect('tbl_order_products', 'order_products_id', $data['product_id'])->row()->order_id;

        $data['orders'] = $this->common_model->commonselect('tbl_order_products', 'order_id', $order_id)->result();

        foreach ($data['orders'] as $i => $prod) {
            if($prod->order_prod_shipping!='Local Pick Up'){
                $is_all_local = 0;
                break;
            }
        }

        if($is_all_local == 0){
            $dbdata['order_status'] = 'Awaiting Tracking';
        }else{
            $dbdata['order_status'] = 'Awaiting Pickup';
        }

        $updated = $this->common_model->commonUpdate('tbl_orders', $dbdata, "order_id", $order_id);

        if ($updated) {
            $return['status'] = 200;
            $return['message'] = 'Shipping Method Updated.';
        }

        echo json_encode($return);
    }

    public function user_order_detail(){

        $OrderId = $this->input->get('order_id');
        $data['userid'] = $this->input->get('user_id');
        $data['step'] = $this->input->get('step');

        $data['results'] = $this->db->query("
                            SELECT * FROM tbl_orders o
                            WHERE o.`order_id` = " . $OrderId . " AND o.buyer_id = " . $data['userid'] . "")->row();

        $data['order_detail_results'] = $this->db->query("SELECT * FROM tbl_order_products o WHERE o.`order_id` = " . $OrderId . "")->result_array();

        $data['total_prods'] = count($data['order_products']);
        if($data['results']->order_status!='completed'){
            foreach ($data['order_detail_results'] as $i => $prod) {
                if(!empty($prod['order_prod_tracking_number']) && $prod['order_prod_shipping']!='Local Pick Up'){
                    $data['cancel_flag'] = 0;
                    break;
                }else{
                    $data['cancel_flag'] = 1;
                }
            }
        }else{
            $data['cancel_flag'] = 0;
        }

        $data['default_shipping_address'] = json_decode($data['results']->order_shipping_address);
        $data['default_billing_address'] = json_decode($data['results']->order_billing_address);

        $data['bill'] = number_format($data['results']->order_grand_total, 2, '.', '');

        $data['order_hmtl'] = '';

        $data['order_hmtl'] .= "<div class='banything-row'><div class='banything-lbl'>Order Date:</div><div class='banything-dtl'>".date('F d, Y', strtotime($data['results']->order_date))."</div></div>";
        $data['order_hmtl'] .= "<div class='banything-row'><div class='banything-lbl'>Order ID:</div><div class='banything-dtl'>".$data['results']->order_id."</div></div>";
        $data['order_hmtl'] .= "<div class='banything-row'><div class='banything-lbl'>Order Total:</div><div class='banything-dtl'>$".number_format($data['results']->order_grand_total, 2, '.', '')."</div></div>";

        $status = '';

        if($data['results']->order_status == 'Awaiting Tracking' || $data['results']->order_status == 'Awaiting Pickup'){
            $status = 'Processing';
        }else if($data['results']->order_status == 'In Process'){
            $status = 'Awaiting Pickup';
        }else if($data['results']->order_status == 'completed'){
            $status = 'Completed';
        }else if($data['results']->order_status == 'cancel'){
            $status = 'Canceled';
        }

        $status = '';
        if($data['results']->order_status == 'cancel'){
            $status = 'Cancel';
        }elseif($data['results']->order_status == 'Awaiting Tracking' || $data['results']->order_status == 'Awaiting Pickup'){
            $status = 'Processing';
        }elseif($data['results']->order_status == 'completed'){
            $status = 'Completed';
        }elseif($data['results']->order_status == 'In Process'){
            $status = 'Processing';
        }

        $data['order_hmtl'] .= "<div class='banything-row'><div class='banything-lbl'>Order Status:</div><div class='banything-dtl'>".$status."</div></div>";

        $data['address_hmtl'] = '';

        if(!empty($data['default_shipping_address'])){
            $data['address_hmtl'] .= "<div class='shipbil-lcol'><div class='inany-wrapper'><div class='inany-row'><h3>Shipping Address</h3></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Name:</div><div class='inany-dtl'>".$data['default_shipping_address']->shipping_first_name." ".$data['default_shipping_address']->shipping_last_name."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Telephone:</div><div class='inany-dtl'>".(($data['default_shipping_address']->shipping_phone!='')?$data['default_shipping_address']->shipping_phone:'N/A')."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Street:</div><div class='inany-dtl'>".$data['default_shipping_address']->shipping_street."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>City:</div><div class='inany-dtl'>".$data['default_shipping_address']->shipping_city."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>State:</div><div class='inany-dtl'>".(($data['default_shipping_address']->shipping_country=='US')?get_state_name($data['default_shipping_address']->shipping_state):$data['default_shipping_address']->shipping_state_other)."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Country:</div><div class='inany-dtl'>".get_country_name($data['default_shipping_address']->shipping_country)."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Zip:</div><div class='inany-dtl'>".$data['default_shipping_address']->shipping_zip."</div></div></div></div>";
        }

        if(!empty($data['default_billing_address'])){
            $data['address_hmtl'] .= "<div class='shipbil-lcol'><div class='inany-wrapper'><div class='inany-row'><h3>Billing Address</h3></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Name:</div><div class='inany-dtl'>".$data['default_billing_address']->billing_first_name." ".$data['default_billing_address']->billing_last_name."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Telephone:</div><div class='inany-dtl'>".(($data['default_billing_address']->billing_phone!='')?$data['default_billing_address']->billing_phone:'N/A')."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Street:</div><div class='inany-dtl'>".$data['default_billing_address']->billing_street."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>City:</div><div class='inany-dtl'>".$data['default_billing_address']->billing_city."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>State:</div><div class='inany-dtl'>".(($data['default_billing_address']->billing_country=='US')?get_state_name($data['default_billing_address']->billing_state):$data['default_billing_address']->billing_state_other)."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Country:</div><div class='inany-dtl'>".get_country_name($data['default_billing_address']->billing_country)."</div></div>";
            $data['address_hmtl'] .= "<div class='inany-row'><div class='inany-lbl'>Zip:</div><div class='inany-dtl'>".$data['default_billing_address']->billing_zip."</div></div></div></div>";
        }

        $data['order_summary'] = '';

        $data['order_summary'] .= "<div class='inany-row'><h3>Order Summary</h3></div>";
        $data['order_summary'] .= "<div class='inany-row'><div class='inany-lbl'>Order Sub Total:</div><div class='inany-dtl'>$".number_format($data['results']->order_sub_total, 2, '.', '')."</div></div>";
        $data['order_summary'] .= "<div class='inany-row'><div class='inany-lbl'>Order Discount:</div><div class='inany-dtl'>$".number_format($data['results']->order_discount, 2, '.', '')."</div></div>";
        $data['order_summary'] .= "<div class='inany-row'><div class='inany-lbl'>Shipping &amp; Handing:</div><div class='inany-dtl'>$".number_format($data['results']->order_shipping_price, 2, '.', '')."</div></div>";

        $data['products'] = '';
        foreach ($data['order_detail_results'] as $i => $prod){
            $data['filters'] = json_decode($prod['order_prod_filters']);
            $filters = '';
            if(count($data['filters']) > 0){
                foreach ($data['filters'] as $i => $filter) {
                    $filters .= "<p><b>".$filter->filterName."</b>:".$filter->filtervalue."</p>";
                }
            }

            $ack = '';            

            $shipping='';
            if($prod['order_prod_shipping']=='Charge for Shipping'){
                $shipping = $prod['order_prod_shipping_methods'];
            }else{
                $shipping = '';
            }

            $date1 = date_create(date("Y-m-d", strtotime($data['results']->order_date)));
            $date2 = date_create(date("Y-m-d"));
            $diff = date_diff($date1,$date2);
            $dayz =  7 - $diff->format("%a");

            $days=0;
            if($prod['order_prod_shipping']=='Charge for Shipping'){
                $days = $prod['order_prod_ship_days']+$dayz;
            }elseif($prod['order_prod_shipping']=='Offer free Shipping'){
                $days = $prod['order_prod_free_ship_days']+$dayz;
            }else{
                $days += $dayz;
            }

            $pending = '';
            if($prod['order_prod_status']!='Delivered' && $prod['order_prod_status']!='In Process' && $prod['order_prod_status']!='completed' && $prod['order_prod_status']!='cancel'){
                $pending = true;
            }else{
                $pending = false;
            }

            $prod_status = '';
            $tracking = '';

            if($prod['order_prod_status']=='In Process'){
                $prod_status = 'Shipped';
            }elseif(($prod['order_prod_status']=='Awaiting Tracking' || $prod['order_prod_status']=='Awaiting Pickup') && $prod['order_prod_shipping']!='Local Pick Up'){
                $prod_status = 'Processing';
            }elseif(($prod['order_prod_status']=='Awaiting Tracking' || $prod['order_prod_status']=='Awaiting Pickup') && $prod['order_prod_shipping']=='Local Pick Up'){
                $prod_status = 'Awaiting Pickup';
            }elseif($prod['order_prod_status']=='completed'){
                $prod_status = 'Delivered';
            }elseif($prod['order_prod_status']=='cancel'){
                $prod_status = 'Canceled';
            }

            if($prod['order_prod_shipping']!='Local Pick Up'){
                if(!empty($prod['order_prod_tracking_number'])){
                    $tracking = $prod['order_prod_tracking_number'];
                }else{
                    $tracking = 'Waiting';
                }
            }

            if($tracking!=''){
                $ack .= "<p>Tracking #: ".$tracking."</p>";
            }

            $ack .= "<p>Status: ".$prod_status."</p>";

            if ($prod['order_prod_completion_date'] != NULL){
                $ack .= "<p>Acknowledged On<br/>";
                $ack .= date('F j, Y', strtotime($prod['order_prod_completion_date']))."</p>";
            }

            $data['products'] .= "<div class='btm-odbdr'><div class='odrList-wrap'><div class='myfan-listwrap'><div class='myfan-listrow'>";
            $data['products'] .= "<div class='myfan-img'><table cellpadding='0' cellspacing='0' border='0'><tbody><tr><td><a href='javascript:void'><img src='".base_url()."resources/prod_images/thumb/".$prod['order_prod_image']."' alt=''></a></td></tr></tbody></table></div><div class='myfan-right'><div class='myfan-rinner'><div class='myfan-rtoptxt'><p>".$prod['order_prod_name']."</p><p>".$filters."</p><p>".(($prod['order_prod_return'] != '')?$prod['order_prod_return']:'')."<br> Qty: ".$prod['order_prod_qty']."</p>".$ack."</div>";
            $data['products'] .= "<div class='edt-ship'><div class='shipbil-rcol sec03'><div class='fan-con'><div class='shipbil-rcol'><div class='inany-wrapper odrsummary'><div class='inany-row'><h3>Shipping Method</h3><div class='inany-lk'></div>";
            $data['products'] .= "<div class='inany-pop pop-".$prod['order_prod_id']."' style='display: none;'><div class='cart-pro-btm'><h2>Available Shipping Type:</h2><select id='select-value-".$prod['order_prod_id']."' name='select-value' class='select-value'><option value='Charge for Shipping' ".(($prod['order_prod_shipping'] == "Charge for Shipping")?"selected":"").">Charge for Shipping</option><option value='Offer free Shipping' ".(($prod['order_prod_shipping'] == "Offer free Shipping")?"selected":"").">Offer free Shipping</option><option value='Local Pick Up' ".(($prod['order_prod_shipping'] == "Local Pick Up")?"selected":"").">Local Pickup</option></select>";
            $data['products'] .= "<div id='ship-to-me-".$prod['order_prod_id']."' class='selection' style='display: none;'><h2>Shipping Method</h2><div class='clk-shp-sho'><div class='clk-shp-sho-rw'><div class='clk-shp-sho-rw-lft'><input type='radio' id='r-".$prod['order_prod_id']."-1' name='cc-".$prod['order_prod_id']."' value='USPS'><label for='r-".$prod['order_prod_id']."-1'><span></span><p>USPS</p></label></div><div class='clk-shp-sho-rw-lft'><input type='radio' id='r-".$prod['order_prod_id']."-2' name='cc-".$prod['order_prod_id']."' value='FedEx'><label for='r-".$prod['order_prod_id']."-2'><span></span><p>FedEx</p></label></div><div class='clk-shp-sho-rw-lft'><input type='radio' id='r-".$prod['order_prod_id']."-3' name='cc-".$prod['order_prod_id']."' value='DHL'><label for='r-".$prod['order_prod_id']."-3'><span></span><p>DHL</p></label></div><div class='clk-shp-sho-rw-lft'><input type='radio' id='r-".$prod['order_prod_id']."-4' name='cc-".$prod['order_prod_id']."' value='UPS'><label for='r-".$prod['order_prod_id']."-4'><span></span><p>UPS</p></label></div></div></div></div><div class='clk-shp-sho-rw-lft'><button type='button' id='save' style='padding: 5px 5px;margin-top:5px;'>Save</button></div></div></div></div>";
            $data['products'] .= "<div class='inany-row'>".(($shipping!='')?$shipping."</br>":'')."".$prod['order_prod_shipping']." ".(($pending)?"(".$days.")":'')."</div><div class='inany-row'><div class='inany-lbl'>Item Price:</div><div class='inany-dtl'>$".$prod['order_prod_total_price']."</div></div><div class='inany-row'><div class='inany-lbl'>Shipping:</div><div class='inany-dtl'>$".number_format($prod['order_prod_ship_price'], 2, '.', '')."</div></div></div></div></div></div></div></div></div></div></div></div></div>";
        }

        if($data['cancel_flag']!=0 && $data['results']->order_status!='cancel'){
            $data['products'] .= "<a href='javascript:void' id='cancel' style='float: left;width: auto;padding: 12px;text-decoration: none;background-color: #008ac5;color: white;border-radius: 5px;font-family:\"source_sans_proregular\";font-weight: normal;'>Cancel</a>";
        }        

        $data['products'] .= "<div class='btorder-row'><div class='btorder-lk'><a href='my-orders.html'>&lt;&lt; Back to Orders</a></div></div>";

      /*  echo "<pre>";
        print_r($data);
        exit;*/

        $data['acknowledge'] = '';
        if($data['results']->order_completion_date!=''){
            $data['acknowledge'] = "<div class='inany-row'><h3>Payment Status</h3></div><div class='inany-row'>Acknowledged On ".date('F d, Y', strtotime($data['results']->order_completion_date))."</div>";
        }else{
            $data['acknowledge'] = "";
        }
        echo json_encode($data);
    }

    public function cancelorder(){
        $order_id = $this->input->get('order_id');
        if ($order_id) {
            $response = $this->order_model->cancel_order($order_id);
            if ($response == 0) {
                $return['status'] = 200;
                $return['message'] = 'Order canceled Successfully.';
            }else{
                $return['status'] = 201;
                $return['message'] = 'Something went wrong.';
            }
        }else{
            $return['status'] = 201;
            $return['message'] = 'Something went wrong.';
        }

        echo json_encode($data);
    }

    function get_product_data(){

        $prod_id = $this->input->get('prod_id');
        $user_id = $this->input->get('user_id');

        $save_data['prod_cat'] = $this->common_model->commonselect('tbl_product_category', 'prod_id', $prod_id)->row();
        
        $product['product_data'] = $this->shop_model->get_product_data_lvl3($save_data['prod_cat']);


        if(empty($product['product_data'])){
            $return['status'] = 200;
            $return['message'] = "Unable to find the Product.";
        }else{
            $product['product_images'] = $this->shop_model->get_product_images($save_data['prod_cat']->prod_id);
            $product['seller_data'] = $this->shop_model->get_seller_details($save_data['prod_cat']->prod_id);
            $product['other_products'] = $this->shop_model->get_seller_other_products($save_data['prod_cat']->prod_id);

            $filters = $this->shop_model->getProduct1lvlRequiredFilter($save_data['prod_cat']->prod_id);            

            if ($filters != FALSE) {
                $product['filters_array'] = $filters;
                $product['required_filters'] = $this->common_model->unique_multidimentional_array($filters, 'filter_id');
            }

            $filters = $this->shop_model->getProduct1lvlAddFilter($save_data['prod_cat']->prod_id);
            if ($filters != FALSE) {
                $product['filters_array2'] = $filters;
                $product['required_filters2'] = $this->common_model->unique_multidimentional_array($filters, 'filter_id');
            }

            $data['zip'] = $product['seller_data']->store_zip;

            /*echo "<pre>";
            print_r($product);
            exit;*/

            $data['images'] = "";
            $data['thumbs'] = "";
            foreach ($product['product_images'] as $i => $img) {
                $data['images'] .= "<li><img src='".base_url()."resources/prod_images/".$img['img_name']."'  alt=''></li>";
                $data['thumbs'] .= "<li><img src='".base_url()."resources/prod_images/thumb/".$img['img_name']."'  alt=''></li>";
            }

            $data['more_products'] = "<div class='mre-prod-tit'><h6>Seller:<a href='javascript:void' class='store_click' data-id='".$product['seller_data']->store_id."'>".$product['seller_data']->user_fname." ".$product['seller_data']->user_lname."</a></h6><p>".$product['seller_data']->store_city.", ".$product['seller_data']->store_state."</p></div>";            
            $data['more_products'] .= "<div class='mre-prod-map' id='googleMap' style='width:100%; height:300px;'></div><div class='mre-prod-botm'><p>Other Products from this seller</p>";
            foreach ($product['other_products'] as $i => $prod){
                $data['more_products'] .= "<div class='mre-prod-botm-bx'><a href='javascript:void' class='prod_click' data-id='".$prod['prod_id']."'><img src='".base_url()."resources/prod_images/thumb/".$prod['img_name']."' alt='' title='' class='img-responsive'/></a></div>";
            }            
            $data['more_products'] .= "</div>";

            $price = '';

            if($product['product_data']->prod_onsale==1){
                $price = "<span class='sp1'>$".$product['product_data']->sale_price."</span><span class='sp2' style='color: #838383;text-decoration: line-through;padding-left: 7px;'>$".$product['product_data']->prod_price."</span>";
            }else{
                $price = "<span class='sp3'>$".$product['product_data']->prod_price."</span>";
            }

            $active = '';
            if(is_numeric( $user_id ) ){
                if($this->check_wishlist_item($prod_id,$user_id)){
                    $active = 'active';
                }
            }

            $video = "";

            if($product['product_data']->youtube_links!=''){
                $video = "<a href='".$product['product_data']->youtube_links."' class='watch-vid'>Watch Video</a>";
            }else{
                $video = "";
            }

            $data['offer'] = "";
            
            if ($product['seller_data']->store_discount > 0 && $product['seller_data']->store_discount_item > 0) {
                $data['offer'] = "<div class='pls-itm'><div class='wrapper'><div class='pls-itm-sec'><h4>".$product['seller_data']->store_discount."% off when you buy ".$product['seller_data']->store_discount_item."+ Items</h4></div></div></div>";
            }else{
                $data['offer'] = "";
            }

            $data['add_info'] = "";

            $ship_method = '';
            if($product['product_data']->prod_shipping_methods!='' && $product['product_data']->prod_shipping_methods!='0'){
                $ship_method = $product['product_data']->prod_shipping_methods;
            }else{
                $ship_method = "N/A";
            }

            $data['add_info'] .= "<div class='des-ad-panel'><h4 style='text-transform: uppercase;'>Condition</h4><p>".$product['product_data']->condition."</p></div>";
            $data['add_info'] .= "<div class='des-ad-panel'><h4 style='text-transform: uppercase;'>Return Policy</h4><p>".(($product['product_data']->prod_return=='Returns Not Accepts')?'Returns Not Accepted':$product['product_data']->prod_return)."</p></div>";
            $data['add_info'] .= "<div class='des-ad-panel'><h4 style='text-transform: uppercase;'>Available Quantity</h4><p>".$product['product_data']->qty."</p></div>";
            $data['add_info'] .= "<div class='des-ad-panel'><h4 style='text-transform: uppercase;'>Shipping</h4><p>".$product['product_data']->shipping."</p></div>";
            $data['add_info'] .= "<div class='des-ad-panel'><h4 style='text-transform: uppercase;'>Shipping Price</h4><p>".(($product['product_data']->shipping=="Charge for Shipping")?"$".$product['product_data']->ship_price:(($product['product_data']->shipping=="Offer free Shipping")?"Free":"Local Pickup"))."</p></div>";
            if($product['product_data']->shipping=="Charge for Shipping"){
                /*$data['add_info'] .= "<div class='des-ad-panel'><h4>Shipping Method</h4><p>".(($product['product_data']->prod_shipping_methods!='' && $product['product_data']->prod_shipping_methods!=0)?$product['product_data']->prod_shipping_methods:'N/A')."</p></div>";*/
                $data['add_info'] .= "<div class='des-ad-panel'><h4 style='text-transform: uppercase;'>Shipping Method</h4><p>".$ship_method."</p></div>";
            }            

            $data['details'] = "";            
            $data['filters'] = "";
            if(!empty($product['required_filters'])){
                $data['filters'] = "<div class='shop-det-filter'>";
                foreach ($product['required_filters'] as $i => $filter) {

                    $data['add_info'] .= "<div class='des-ad-panel'><h4 style='text-transform: uppercase;'>".$filter['filter_title']."</h4><p>";
                    $set_filters = "<div class='filter-blk'><label>".$filter['filter_title'].":</label>";
                    $set_filters .= "<select class='required-filters' data-name='".$filter['filter_title']."' name='".$filter['filter_slug']."' id='".$filter['filter_slug']."' data-prodid='".$filter['prod_id']."' >";
                    if($filter['cat_filter_values']!=''){
                        $values = explode(',',$filter['cat_filter_values']);
                        $count = 1;
                        foreach ($product['filters_array'] as $i => $value) {
                            if(in_array($value['filter_value'], $values) && $filter['filter_slug']==$value['filter_slug']){
                                if($count!=1){
                                    $data['add_info'] .= ", ";
                                }
                                $data['add_info'] .= $value['filter_value'];
                                $set_filters .= "<option value='".$value['filter_value']."'>".$value['filter_value']."</option>";
                                $count++;
                            }
                        }                    
                        $name = $filter['prod_id'].'_'.$filter['filter_slug'];
                        $data['filters'] .= $set_filters."</select>";
                        $data['filters'] .= "<input type='hidden' name='".$name."' data-name='".$filter['filter_title']."' data-slug='".$filter['filter_slug']."' class='required_filters' id='".$name."' value='' />";
                        $data['filters'] .= "</div>";

                        $data['add_info'] .= "</p></div>";
                    }else{
                        $data['value'] = $this->db->query("SELECT filter_value FROM tbl_product_filters_detail WHERE filter_id = ".$filter['filter_id']."")->result_array();
                        $set_filters = "<div class='filter-blk' id='".$filter['filter_slug']."-html'><label>".$filter['filter_title'].":</label>";
                        $set_filters .= "<select class='required-filters con-filter' data-name='".$filter['filter_title']."' name='".$filter['filter_slug']."' id='".$filter['filter_slug']."' data-prodid='".$filter['prod_id']."' >";
                        
                        $data['values'] = $this->db->query("SELECT f.filter_slug, fd.filter_title ,fd.filter_detail_id 
                                                                                    FROM tbl_product_filters f 
                                                                                    INNER JOIN tbl_cat_filter_detail fd ON f.filter_id = fd.filter_detail_title_id 
                                                                                    WHERE f.prod_id = ".$filter['prod_id']." AND f.filter_slug = '".$filter['filter_slug']."' 
                                                                                    GROUP BY fd.filter_detail_id 
                                                                                    ")->result_array();
                        $values[0] = '';

                        foreach ($data['values'] as $i => $op){
                            if($i==0){
                                $values[0] = $op;
                            }
                            $set_filters .= '<option value="'.$op["filter_title"].'" data-prod="'.$filter['prod_id'].'" data-id="'.$op["filter_detail_id"].'">'.stripcslashes($op["filter_title"]).'</option>';
                        }

                        $name = $filter['prod_id'].'_'.$filter['filter_slug'];
                        $data['filters'] .= $set_filters."</select>";
                        $data['filters'] .= "<input type='hidden' name='".$name."' data-name='".$filter['filter_title']."' data-slug='".$filter['filter_slug']."' class='required_filters' id='".$name."' value='' />";
                        $data['filters'] .= "</div>";

                        $values = $this->db->query("SELECT * 
                                                FROM tbl_product_filters_detail f 
                                                INNER JOIN tbl_cat_filter_detail fd ON fd.filter_detail_id = f.filter_detail_id 
                                                WHERE prod_id = ".$filter['prod_id']." AND f.filter_detail_id = ".$values[0]['filter_detail_id']."
                                                ")->result_array();

                        $set_filters = "<div class='filter-blk' id='".$filter['filter_slug']."-filters'><label>".$values[0]['filter_title'].":</label>";
                        $set_filters .= "<select class='required-filters sub-filters' data-name='".$values[0]['filter_title']."' name='".$values[0]['filter_slug']."' id='".$values[0]['filter_slug']."' data-prodid='".$values[0]['prod_id']."' >";
 
                        foreach ($values as $i => $op){
                            $set_filters .= '<option value="'.$op["filter_value"].'">'.stripcslashes($op["filter_value"]).'</option>';
                        }

                        $name = $values[0]['prod_id'].'_'.$values[0]['filter_slug'];
                        $data['filters'] .= $set_filters."</select>";
                        $data['filters'] .= "<input type='hidden' name='".$name."' data-name='".$values[0]['filter_title']."' data-slug='".$values[0]['filter_slug']."' class='required_filters' id='".$name."' value='' />";
                        $data['filters'] .= "</div>";
                        /*echo "<pre>";
                        print_r($values);
                        exit;*/
                        $count = 1;
                        foreach ($data['value'] as $i => $value) {
                            if($count!=1){
                                $data['add_info'] .= ", ";
                            }
                            $data['add_info'] .= $value['filter_value'];
                            $count++;
                        }
                        $data['add_info'] .= "</p></div>";
                    }
                }
                $data['filters'] .= "</div>";
            }

            if($product['product_data']->is_unique_filter==1){
                $data['filters'] = "";
            }

            if(!empty($product['required_filters2'])){
                /*echo "<pre>";
                print_r($product['required_filters2']);
                exit;*/
                foreach ($product['required_filters2'] as $i => $filter) {
                    if($filter['cat_filter_values']!=''){
                        $data['add_info'] .= "<div class='des-ad-panel'><h4 style='text-transform: uppercase;'>".$filter['filter_title']."</h4><p>";
                        $values = explode(',',$filter['cat_filter_values']);
                        $count = 1;
                        foreach ($product['filters_array2'] as $i => $value) {
                            if(in_array($value['filter_value'], $values) && $filter['filter_slug']==$value['filter_slug']){
                                if($count!=1){
                                    $data['add_info'] .= ", ";
                                }
                                $data['add_info'] .= $value['filter_value'];
                                $count++;
                            }
                        }
                        $data['add_info'] .= "</p></div>";
                    }else{
                        $data['add_info'] .= "<div class='des-ad-panel'><h4 style='text-transform: uppercase;'>".$filter['filter_title']."</h4><p>";
                        $data['value'] = $this->db->query("SELECT filter_value FROM tbl_product_filters_detail WHERE filter_id = ".$filter['filter_id']."")->result_array();

                        $count = 1;
                        foreach ($data['value'] as $i => $value) {
                            if($count!=1){
                                $data['add_info'] .= ", ";
                            }
                            $data['add_info'] .= $value['filter_value'];
                            $count++;
                        }
                        $data['add_info'] .= "</p></div>";
                    }
                }
            }

            /*echo "<pre>";
            print_r($product);
            exit;*/

            $productPrice = 0.0;
            $sDate = date('Y-m-d', strtotime($product['product_data']->sale_start_date));
            $eDate = date('Y-m-d', strtotime($product['product_data']->sale_end_date));
            $tDate = date('Y-m-d');
            if ($product['product_data']->prod_onsale == 1) {
                if ($product['product_data']->sale_start_date != '' && $product['product_data']->sale_end_date != '') {
                    if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                        $productPrice = number_format($product['product_data']->prod_price, 2, '.', '');
                    } else {
                        $productPrice = number_format($product['product_data']->prod_price, 2, '.', '');
                    }
                } else if ($product['product_data']->sale_start_date != '') {
                    if ($tDate >= $sDate) {
                        $productPrice = number_format($product['product_data']->prod_price, 2, '.', '');
                    } else {
                        $productPrice = number_format($product['product_data']->prod_price, 2, '.', '');
                    }
                } else if ($product['product_data']->sale_end_date != '') {
                    if ($tDate <= $eDate) {
                        $productPrice = number_format($product['product_data']->prod_price, 2, '.', '');
                    } else {
                        $productPrice = number_format($product['product_data']->prod_price, 2, '.', '');
                    }
                } else {
                    $productPrice = number_format($product['product_data']->prod_price, 2, '.', '');
                }
            } else {
                $productPrice = number_format($product['product_data']->prod_price, 2, '.', '');
            }

            $days = '';
            if($product['product_data']->shipping=='Charge for Shipping'){
                $days = $product['product_data']->ship_days;
            }else if($product['product_data']->shipping=='Offer free Shipping'){
                $days = $product['product_data']->free_ship_days;
            }else{
                $days = 7;
            }


            $data['details'] .="<div class='shop-det-rgt-upr'><h4>".$product['product_data']->prod_title."</h4><p><a href='javascript:void' id='store_click' data-id='".$product['seller_data']->store_id."'>".$product['seller_data']->user_fname." ".$product['seller_data']->user_lname."</a><span> (Visit their Shop)</span></p></div>".$data['filters'];
            $data['details'] .="<div class='shop-det-rgt-lowr-1'><h5>".$price."</h5><p>+ ".(($product['product_data']->shipping=='Charge for Shipping')?"$".$product['product_data']->ship_price." Shipping (".$product['product_data']->prod_shipping_methods.")":$product['product_data']->shipping)."</p></div>";
            $data['details'] .="<div class='shop-det-rgt-lowr-2'><a href='javascript:void(0);'data-price ='".$productPrice."' class='adcrt' data-product='".$product['product_data']->prod_id."' >Add to cart</a></div>";            
            $data['details'] .="<div class='shop-det-rgt-lowr-3'><a href='mailto:info@educki.com?Subject=Report' class='reprt-btn' onclick='report(".$product['product_data']->prod_id.")' style='text-decoration:none;'>Report</a><a href='javascript:void' class='addcompr-btn lal js__p_start compare-prod' data-id='".$product['product_data']->prod_id."'></a><a href='javascript:void' class='wishlist-shp wish not_logged_in ".$active."'></a>".$video."</div>";
            $data['details'] .="<div class='shop-det-rgt-lowr-5'><h6>Product Ships within ".$days." business days</h6>&nbsp;&nbsp;&nbsp;".(($product['product_data']->prod_description!='')?"<p>".clean_string($product['product_data']->prod_description)."</p>":'')."</div><input type='hidden' id='store_email' value='".$product['seller_data']->user_email."'><input type='hidden' id='store_id' value='".$product['seller_data']->user_id."'>";
        }

        /*echo "<pre>";
        print_r($data);
        exit;*/
        
        echo json_encode($data);
    }

    public function product_report(){
        
        $prod_id = $this->input->get('prod_id');
        $product['productdata'] = $this->common_model->commonselect('tbl_products', 'prod_id', $prod_id)->row();
        $report_count = $product['productdata']->prod_report + 1;
        $update_data = array('prod_report' => $report_count);
        $this->common_model->commonUpdate('tbl_products', $update_data, 'prod_id', $prod_id);
        $history['flag_prod_id'] = $prod_id;
        $history['flag_date'] = date('Y-m-d H:i:s');
        $result = $this->common_model->commonSave('tbl_item_flag_history', $history);
        if($result){
            $return['status'] = 200;
            $return['message'] = "Product reported Successfully.";
        }else{
            $return['status'] = 201;
            $return['message'] = "There was a problem.";
        }

        echo json_encode($return);
    }

    public function load_con_filters()
    {
        $prod_id = $this->input->get('prod_id');
        $filter_id = $this->input->get('filter_id');
        $title = $this->input->get('title');
        $id = $this->input->get('id');

        $data['status'] = 201;
        $data['message'] = "There was a problem.";

        $data['filters'] = "";

        if($prod_id != '' && $filter_id!=''){

            $values = $this->db->query("SELECT * 
                                                FROM tbl_product_filters_detail f 
                                                INNER JOIN tbl_cat_filter_detail fd ON fd.filter_detail_id = f.filter_detail_id 
                                                WHERE prod_id = ".$prod_id." AND f.filter_detail_id = ".$filter_id."
                                                ")->result_array();

            $set_filters = "<div class='filter-blk' id='".$id."-filters'><label>".$values[0]['filter_title'].":</label>";
            $set_filters .= "<select class='required-filters sub-filters' data-name='".$values[0]['filter_title']."' name='".$values[0]['filter_slug']."' id='".$values[0]['filter_slug']."' data-prodid='".$values[0]['prod_id']."' >";

            foreach ($values as $i => $op){
                $set_filters .= '<option value="'.$op["filter_value"].'">'.stripcslashes($op["filter_value"]).'</option>';
            }

            $name = $values[0]['prod_id'].'_'.$values[0]['filter_slug'];
            $data['filters'] .= $set_filters."</select>";
            $data['filters'] .= "<input type='hidden' name='".$name."' data-name='".$values[0]['filter_title']."' data-slug='".$values[0]['filter_slug']."' class='required_filters' id='".$name."' value='' />";
            $data['filters'] .= "</div>";

            $data['status'] = 200;
            $data['message'] = "Filters retreived successfully.";
        }

        echo json_encode($data);
    }

    public function prod_url()
    {
        $prod_id = $this->input->get('prod_id');

        $query = "
                    SELECT 
                    prod.prod_url, category.cat_url as catURL, subCat.cat_url as subCatURL, subSubCat.cat_url as subSubCatURL
                    FROM tbl_products prod
                    INNER JOIN tbl_product_category prodCat ON prod.prod_id = prodCat.prod_id
                    INNER JOIN tbl_categories category ON prodCat.category_id = category.cat_id
                    LEFT JOIN tbl_categories subCat ON prodCat.sub_category_id = subCat.cat_id
                    LEFT JOIN tbl_categories subSubCat ON prodCat.sub_sub_category_id = subSubCat.cat_id
                    WHERE
                    prod_is_delete = 0
                    AND
                    prod_status = 1
                    AND
                    qty > 0
                    AND
                    prod.prod_id = ".$prod_id."
                    GROUP BY prod.prod_id
                    ";
        $result = $this->db->query($query)->row_array();
        if(!empty($result)){
            $produrl = base_url($result['catURL']);
            if ($result['subCatURL'] != '') {
                $produrl .= '/' . $result['subCatURL'];
            }
            if ($result['subSubCatURL'] != '') {
                $produrl .= '/' . $result['subSubCatURL'];
            }

            $produrl .= '/' . $result['prod_url'];

            $return['url'] = $produrl;
            $return['status'] = 200;
            $return['message'] = "Url found successfully.";
        }else{
            $return['status'] = 201;
            $return['message'] = "There was a problem.";
        }

        echo json_encode($return);
    }

    public function ask_a_question(){
        $question = $this->input->get('question');
        $sender_id = $this->input->get('sender');
        $email_to = $this->input->get('receiver');
        $prod = $this->input->get('prod_id');

        $sender_info = $this->common_model->commonSelect('tbl_user', 'user_id', $sender_id)->row();
        $seller_info = $this->common_model->commonSelect('tbl_user', 'user_email', $email_to)->row();
        $prod_info = $this->common_model->commonSelect('tbl_products', 'prod_id', $prod)->row();
        $email_from = $sender_info->user_email;

        $email_data['subject'] = 'Ask a Question';

        $email_msg = 'Dear '.$seller_info->user_fname." ".$seller_info->user_lname.',<br><br>';
        $email_msg .= 'The user <b>'.$sender_info->user_fname." ".$sender_info->user_lname.'</b> has asked a question about the product <b>'.$prod_info->prod_title.'</b><br><br>';
        $email_msg .= 'Question: ' . $question . '<br><br>';
        $email_msg .= 'Thank you';

        $result = send_email_2($email_to,$email_from,'Question About Product on Educki',$email_msg);
        if($result){
            $return['status'] = 200;
            $return['message'] = "Question submitted successfully.";
        }else{
            $return['status'] = 201;
            $return['message'] = "There was a Problem.";
        }

        echo json_encode($return);
    }

    function check_availability() {
        $store_name = $this->input->get('store_name');
        $where = "store_name = '" . $store_name . "'";
        $results = $this->common_model->commonselect_array('tbl_stores', $where, 'store_id')->row();
        if ($results == NULL) {
            $return['status']=200;
            $return['message']="Store name available.";
        } else {
            $name = $this->suggest_name($store_name);
            $return['status']=201;
            $return['message']="Store name unavailable try this ".$name;
        }

        echo json_encode($return);
    }

    public function suggest_name($data) {
        $new_data = $data . mt_rand(0, 10000);
        $response = $this->seller_model->check_availability($new_data);
        if ($response == 0) {
            return $new_data;
        }
    }

    public function upload_store_logo()
    {
        $ext = explode(".",urldecode($_FILES["file"]["name"]));
        $ext = $ext[count($ext)-1];
        $new_image_name = 'logo_'. date('Y_m_d_h_i_s').'.'.$ext;
        $test = move_uploaded_file($_FILES["file"]["tmp_name"], "/public_html/dev/resources/seller_account_image/logo/".str_replace('"',"",$new_image_name));
        echo json_encode($new_image_name);
    }

    public function upload_store_banner()
    {
        $ext = explode(".",urldecode($_FILES["file"]["name"]));
        $ext = $ext[count($ext)-1];
        $new_image_name = 'banner_'. date('Y_m_d_h_i_s').'.'.$ext;
        $test = move_uploaded_file($_FILES["file"]["tmp_name"], "/public_html/dev/resources/seller_account_image/banner/".str_replace('"',"",$new_image_name));
        echo json_encode($new_image_name);
    }

    public function create_store()
    {
        $return['status'] = 202;
        $return['message'] = "There was a problem creating store.";

        $store_name = $this->input->get('store_name');
        $dataDB['user_id'] = $this->input->get('user_id');
        $status = $this->input->get('status');

        $data[] = '';

        if($status=='edit'){
            $data['page_data'] = $this->common_model->commonSelect('tbl_stores', 'user_id', $dataDB['user_id'])->row();
        }

        /*echo "<pre>";
        print_r($_GET);
        print_r($this->db->last_query());*/

        if($store_name){
            $response = $this->paypal_id($this->input->get('paypal_id'));

            if($status=='edit' && $this->input->get('paypal_id')==$data['page_data']->store_paypal_id){
                $response = 1;
            }

            if($response == 1){
                
                if($status=='edit' && $store_name==$data['page_data']->store_name){
                    $response = 0;
                }else if($status=='edit' && $store_name!=$data['page_data']->store_name){
                    $response = $this->seller_model->check_availability($store_name);
                }else if($status=='add'){
                    $response = $this->seller_model->check_availability($store_name);
                }

                if ($response == 0) {

                    $dataDB['store_tier_id'] = $this->input->get('store_plan');                

                    $tier['tier_data'] = $this->common_model->commonSelect('tbl_tier_list', 'tier_id', $dataDB['store_tier_id'])->row();
                    $end_date = '';
                    $time = strtotime(date("Y-m-d h:i:s"));
                    if ($tier_id == 1) {
                        $end_date = date('Y-m-d h:i:s');
                        $subscription_data['sub_free_tier'] = 'Y';
                    } elseif ($tier_id == 2) {
                        $end_date = date('Y-m-d h:i:s', strtotime('+'.$tier['tier_data']->tier_months.' months', $time));
                    } elseif ($tier_id == 3) {
                        $end_date = date('Y-m-d h:i:s', strtotime('+'.$tier['tier_data']->tier_months.' months', $time));
                    } else {
                        $end_date = date('Y-m-d h:i:s', strtotime('+'.$tier['tier_data']->tier_months.' months', $time));
                    }

                    $dataDB['store_name'] = $this->input->get('store_name');
                    if($status=='add'){
                        $dataDB['store_url'] = $this->common_model->create_slug($this->input->get('store_name'), 'tbl_stores','store_url');
                    }                
                    $dataDB['store_describe'] = $this->input->get('store_desc');
                    $dataDB['store_paypal_id'] = $this->input->get('paypal_id');
                    $dataDB['store_address'] = $this->input->get('street');
                    $dataDB['store_city'] = $this->input->get('city');
                    $dataDB['store_state'] = ($this->input->get('country') == "US" ? $this->input->get('state') : $this->input->get('other_state'));
                    $dataDB['store_zip'] = $this->input->get('zip');
                    $dataDB['store_longitude'] = ($this->input->get('store_longitude') ? $this->input->get('store_longitude') : "");
                    $dataDB['store_latitude'] = ($this->input->get('store_latitude') ? $this->input->get('store_latitude') : "");
                    $dataDB['store_country'] = $this->input->get('country');
                    $dataDB['store_number'] = $this->input->get('phone');
                    $dataDB['store_description'] = $this->input->get('store_descriotion');
                    $dataDB['store_facebook_url'] = $this->input->get('fb_url');
                    $dataDB['store_twitter_url'] = $this->input->get('twitter_url');
                    $dataDB['store_instagram_url'] = $this->input->get('instagram_url');
                    $dataDB['store_pintrest_url'] = $this->input->get('pinterest_url');
                    if($status=='add'){
                        $dataDB['store_created_date'] = date('Y-m-d h:i:s');
                    }

                    $dataDB['store_end_date'] = $end_date;
                    if($this->input->get('logo_name')!=''){
                        $dataDB['store_logo_image'] = str_replace('"', "", $this->input->get('logo_name'));
                    }

                    if($this->input->get('banner_name')!=''){
                        $dataDB['store_banner_image'] = str_replace('"', "", $this->input->get('banner_name'));
                    }

                    if($status=='add'){
                        $this->common_model->commonSave('tbl_stores', $dataDB);
                        $store_id = $insert_id = $this->db->insert_id();
                        $user_data['user_type'] = 'Seller';
                        $this->common_model->commonUpdate('tbl_user', $user_data, 'user_id', $dataDB['user_id']);
                    }else{
                        $this->common_model->commonUpdate('tbl_stores', $dataDB, 'user_id', $dataDB['user_id']);
                    }

                    if($status=='add'){
                        $subscription_data['sub_tier_id'] = $dataDB['store_tier_id'];
                        $subscription_data['sub_store_id'] = $store_id;
                        $subscription_data['sub_start'] = date('Y-m-d h:i:s');
                        $subscription_data['sub_ends'] = $end_date;
                        $subscription_data['next_payment'] = date('d', strtotime('+1 months'));
                        if(date('d')!=$subscription_data['next_payment']){
                            $subscription_data['next_payment'] = date('Y-m-d h:i:s', strtotime('last day of next month'));
                        }else{
                            $subscription_data['next_payment'] = date('Y-m-d h:i:s', strtotime('+1 months'));
                        }
                        $subscription_data['tier_price'] = $tier['tier_data']->tier_amount;
                        $subscription_id = $this->common_model->commonSave('tbl_user_store_subscription', $subscription_data);
                    }

                    $return['status'] = 200;
                    $return['message'] = "Store created successfully.";
                } else {
                    $name = $this->suggest_name($store_name);
                    $return['status'] = 201;
                    $return['message'] = "Store name available  try this ".$name;
                }
            }else{
                $return['status'] = 202;
                $return['message'] = "Your Paypal ID should be unique.";
            }
        }

        echo json_encode($return);
    }

    public function paypal_id($paypal_id){
        $user_data =  $this->db->query("
                            SELECT * FROM tbl_stores store
                            INNER JOIN tbl_user user ON store.user_id = user.user_id
                            WHERE store.store_paypal_id = '".$paypal_id."' AND user.user_is_delete = 0")->row();
        if(count($user_data)>0)
        {
            return 0;
        }else{
            return 1;
        }
    }

    public function get_compare_list()
    {
        $products = explode(',',$this->input->get('prods'));
        $data['comp_list'] = "";

        foreach ($products as $i => $prod){
            $prod_details = $this->getproduct_details($prod);
            $data['comp_list'] .= '<div class="compare-blk" id="row-'.$prod_details->prod_id.'"><a href="javascript:void" data-id="'.$prod_details->prod_id.'" class="comp-delete">X</a><img src="'.base_url().'resources/prod_images/'.$prod_details->img_name.'" alt=""><p>'.$prod_details->prod_title.'</p></div>';
            $data['title'] = $prod_details->prod_title;
        }

        if(count($products)>=2){
            $data['comp_list'] .= '<div class="compare-btn sign-in-btns"><input class="Compare submit start-compare" type="submit" value="Compare"></div>';
        }
                
        $data['comp_list'] .= '<div class="clear-all"><a href="javascript:void" id="comp-clear">Clear List</a></div>';

        echo json_encode($data);
    }

    function getproduct_details($prod_id) {

        $query = "SELECT 
                    prod.*,
                    category.cat_url AS catURL,
                    subCat.cat_url AS subCatURL,
                    subSubCat.cat_url AS subSubCatURL,
                    users.user_fname,
                    users.user_lname,
                    store.store_url,
                    store.store_name,
                    prod_images.img_name,
                    prod.qty
                  FROM
                    tbl_products prod 
                    INNER JOIN tbl_product_category prodCat 
                      ON prod.prod_id = prodCat.prod_id 
                    INNER JOIN tbl_categories category 
                      ON prodCat.category_id = category.cat_id 
                    LEFT JOIN tbl_categories subCat 
                      ON prodCat.sub_category_id = subCat.cat_id 
                    LEFT JOIN tbl_categories subSubCat 
                      ON prodCat.sub_sub_category_id = subSubCat.cat_id 
                      INNER JOIN tbl_user users
                      ON users.user_id = prod.prod_user_id
                      INNER JOIN tbl_stores store
                      ON store.user_id = users.user_id
                      INNER JOIN tbl_product_images prod_images
                      ON prod_images.img_prod_id = prod.prod_id
                  WHERE prod_is_delete = 0 
                    AND prod.prod_status = 1
                    AND prod.qty > 0
                    AND store.store_is_hide = 0
                    AND prod.prod_id = " . $prod_id . "
                    GROUP BY prod.prod_id
                    ORDER BY prod.prod_id DESC";
        return $this->db->query($query)->row();
    }

    public function get_compare_details()
    {
        $products = explode(',',$this->input->get('prods'));
        $data['images'] = "";
        $data['name'] = "<tr>";
        $data['desc'] = "<tr>";
        $data['color'] = "<tr>";
        $data['con'] = "<tr>";
        $data['size'] = "<tr>";
        $data['price'] = "";

        $data['body'] = '<tr><td colspan="3"><div class="specification">Specifications</div></td></tr>';

        foreach ($products as $i => $prod){
            $prod_details = $this->getproduct_details($prod);
            $prod_filters = getProduct1lvlFilter($prod, "Color");
            $filtersvalue = array();

            foreach ($prod_filters as $filterpush) {
                array_push($filtersvalue, $filterpush['filter_value']);
            }
            $color = "";
            if (empty($filtersvalue)) {
                $color = "N/A";
            } else {
                $color = implode(",", $filtersvalue);
            }

            $prod_filters = getProduct1lvlFilter($prod, "Size");
            $filtersvalue = array();
            foreach ($prod_filters as $filterpush) {
                array_push($filtersvalue, $filterpush['filter_value']);
            }
            $size = "";
            if (empty($filtersvalue)) {
                $size = "N/A";
            } else {
                $size = implode(",", $filtersvalue);
            }

            $price = "";
            if($prod_details->prod_onsale==1){
                $price = '<span class="clr-red">On Sale:<br/> $'.$prod_details->sale_price.'</span>';
            }else{
                $price = '<span class="clr-red">$'.$prod_details->prod_price.'</span>';
            }

            $data['name'] .= '<td><h3>Name</h3><p>'.$prod_details->prod_title.'</p></td>';
            $data['desc'] .= '<td><h3>Description</h3><p>'.clean_string($prod_details->prod_description).'</p></td>';
            $data['color'] .= '<td><h3>Color</h3><p>'.$color.'</p></td>';
            $data['con'] .= '<td><h3>Condition</h3><p>'.$prod_details->condition.'</p></td>';
            $data['size'] .= '<td><h3>Size</h3><p>'.$size.'</p></td>';

            $data['images'] .= '<th class="img-crs"><img src="'.base_url().'resources/prod_images/'.$prod_details->img_name.'" alt="" title="" class="th-img"/><a href="javascript:void" class="comp-delete" data-id="'.$prod_details->prod_id.'"><span class="cross-btn"><i class="fa fa-times"></i></span></a></th>';
            /*$data['details'] .= "<td><h3>Name</h3><p>".$prod_details->prod_title."</p><h3>Description</h3><p>".clean_string($prod_details->prod_description)."</p>";
            $data['details'] .= "<h3>Color</h3><p>".$color."</p><h3>Condition</h3><p>".$prod_details->condition."</p><h3>Size</h3><p>".$size."</p></td>";*/
            $data['price'] .= '<td class="text-center">'.$price.'<button class="ad-crt-btn">add to cart</button></td>';
        }

        $data['name'] .= '</tr>';
        $data['desc'] .= '</tr>';
        $data['color'] .= '</tr>';
        $data['con'] .= '</tr>';
        $data['size'] .= '</tr>';

        $data['body'] .= $data['name'];
        $data['body'] .= $data['desc'];
        $data['body'] .= $data['color'];
        $data['body'] .= $data['con'];
        $data['body'] .= $data['size'];

        $data['details'] = $data['body'];

        echo json_encode($data);
        /*echo "<pre>";
        print_r($prod);
        exit;*/
    }

    public function get_all_favourite_shops()
    {
        $store_id = $this->input->get('store_id');
        $user_id = $this->input->get('user_id');
        $per_page = $this->input->get('per_page');
        $counter = $this->input->get('counter') * $per_page;
        
        $data['pagedata'] = $this->db->query("SELECT *
                                              FROM tbl_stores store
                                              INNER JOIN tbl_user user  ON store.user_id = user.user_id
                                              WHERE store.store_is_hide = 0 AND store.store_id = '".$store_id."' AND user.user_is_delete = 0 AND user.user_status = 1")->row();

        $data['total_stores'] = count($this->seller_model->get_favourite_shops($data['pagedata']->user_id, 'DESC', 0, 0, 0)->result_array());

        $data['stores'] = $this->seller_model->get_favourite_shops($data['pagedata']->user_id, 'DESC', $counter, $per_page, 1)->result_array();

        $data['html'] = "<h2>Favorite Shops</h2>";

        foreach ($data['stores'] as $i => $store) {

            if($store['store_banner_image']!=''){
                $image = base_url()."resources/seller_account_image/banner/".$store['store_banner_image'];
            }else{
                $image = base_url()."resources/no_banner/no_banner.png";
            }

            $data['html'] .= '<a href="javascript:void" data-id="'.$store['store_id'].'" class="store_click"><div class="shp-lst-blk"><img src="'.$image.'"></a>';
            $data['html'] .= '<a href="javascript:void" data-id="'.$store['store_id'].'" class="store_click"><div class="fav-shp"><img src="'.base_url()."resources/seller_account_image/logo/".$store['store_logo_image'].'"></a><a href="javascript:void" data-id="'.$store['store_id'].'" class="store_click"><div class="fav-shp-rgt"><p>'.$store['store_name'].'</p><span>'.$store['store_city'].", ".$store['store_state'].'</span></div></a></div>';
            if($user_id==$data['pagedata']->user_id){
                $data['html'] .= '<div class="fav-chk"><input type="checkbox" data-id="'.$store['store_id'].'" id="c'.$store['store_id'].'" name="ulist1[]"><label for="c'.$store['store_id'].'"><span></span></label></div></div>';
            }
            $data['html'] .= '</div>';
        }

        if(count($data['stores'])==0){
            $data['html'] = "<h2>Favorite Shops</h2>";
            $data['html'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No data found.</div>";
        }

        $data['unfav_id'] = $data['pagedata']->user_id;

        $data['counter'] = $this->input->get('counter') + $per_page;

        echo json_encode($data);
    }

    public function delete_favourite_shops() {
        $user_id = $this->input->get('user_id');
        $data['stores_id'] = explode(',', $this->input->get('val'));

        $return['status'] = 201;
        $return['message'] = "There was a problem.";

        if (count($data['stores_id']) > 0) {            

            foreach ($data['stores_id'] as $store) {
                $this->common_model->commonDelete2('tbl_fans', 'fan_id', $user_id, 'store_id', $store);
            }
            $return['status'] = 200;
            $return['message'] = "success.";
        }else{
            $return['status'] = 202;
            $return['message'] = "Please select at least one Store";
        }

        echo json_encode($return);
    }

    public function get_cat_filter()
    {
        $cat_url = $this->input->get('cat_url');

        $return['status'] = 201;
        $return['message'] = "There was a problem.";

        if($cat_url!=null && $cat_url!=''){

            $multiple = $this->db->query("SELECT * 
                                        FROM tbl_cat_filter_title filter
                                        INNER JOIN `tbl_categories` cat ON cat.`cat_id` = filter.`filter_cat_id`
                                        WHERE cat.`cat_url` = '$cat_url' AND ( filter.`cat_filter_is_conditional` = 0 
                                        OR filter.`cat_filter_is_conditional` IS NULL)
                                        GROUP BY filter.`filter_title` ORDER BY filter_title_id ASC")->result_array();

            $return['html'] = "";
            foreach ($multiple as $i => $filter){
                $return['html'] .= '<div class="panel-con"><button class="accordion">'.$filter['filter_title'].'</button><div class="panel">';
                $values = explode(',', $filter['cat_filter_values']);
                foreach ($values as $i => $name) {
                    $return['html'] .= '<div class="catrg-chkbx"><div class="catrg-chkbx-lft"><input type="checkbox" class="filter_click" data-id="0" id="'.$filter['filter_slug'].'-'.$i.'" name="'.$filter['filter_slug'].'"  value="'.$name.'"/><label for="'.$filter['filter_slug'].'-'.$i.'"><span></span><p>'.$name.'</p></label></div>';
                    $return['html'] .= '<div class="catrg-chkbx-rgt"><span class="nm-flt">('.get_filter_product_count($cat_url,$name,$filter['filter_slug']).')</span></div></div>';
                }
                $return['html'] .= '</div></div>';
            }

            /*echo "<pre>";
            print_r($this->db->last_query());
            exit;*/

            $conditional = $this->db->query("SELECT * 
                                        FROM `tbl_cat_filter_title` filter
                                        INNER JOIN `tbl_categories` cat ON cat.`cat_id` = filter.`filter_cat_id`
                                        WHERE cat.`cat_url` = '$cat_url' AND filter.`cat_filter_is_conditional` = 1 
                                        GROUP BY filter.`filter_title` ORDER BY filter_title_id ASC")->result_array();

            foreach ($conditional as $i => $con) {
                //<div class="signed-in-checkbox">
                $return['html'] .= '<div class="panel-con"><button class="accordion">'.$con['filter_title'].'</button><div class="panel">';
                $return['html'] .= '<div class="multi-box-color-blk" style="width: 100%;">';
                $return['html'] .= '<div class="multi-box" style="position: relative;">';

                $con_sub = $this->db->query("SELECT * 
                                        FROM `tbl_cat_filter_detail` 
                                        WHERE filter_detail_title_id = ".$con['filter_title_id']."")->result_array();

                foreach ($con_sub as $i => $filter){
                    $return['html'] .= '<div class="catrg-chkbx"><div class="catrg-chkbx-lft"><input type="checkbox" class="filter_click" id="c'.$filter['filter_detail_id'].'"  data-id="1" name="'.$con['filter_slug'].'" class="mycheckbox" value="'.$filter['filter_slug'].'"><label class="exp-clr" for=c'.$filter['filter_detail_id'].'><span></span>'.$filter['filter_title'].'</label></div><div class="catrg-chkbx-rgt"><span class="nm-flt">('.get_con_main_filter_product_count($cat_url,$con['filter_slug'],$filter['filter_slug']).')</span></div></div>';
                    $return['html'] .= '<div class="multi-sub-box">';
                    $values = explode(',', $filter['filter_detail']);
                    foreach ($values as $i => $values){
                        $return['html'] .= '<div class="catrg-chkbx"><div class="catrg-chkbx-lft"><input type="checkbox" class="filter_click" id="c'.$filter['filter_detail_id'].'-'.$values.'"  data-id="2" name="'.$filter['filter_slug'].'" class="mycheckbox" value="'.$values.'"><label class="exp-clr" for="c'.$filter['filter_detail_id'].'-'.$values.'"><span></span>'.$values.'</label></div><div class="catrg-chkbx-rgt"><span class="nm-flt">('.get_con_second_filter_product_count($cat_url,$filter['filter_slug'],$values).')</span></div></div>';
                    }
                    $return['html'] .= '</div>';
                }
                /*echo "<pre>";
                print_r($this->db->last_query());
                exit;*/
                $return['html'] .= '</div></div>';
                $return['html'] .= '</div></div>';
            }

            /*echo "<pre>";
            print_r($conditional);
            exit;*/
            $return['status'] = 200;
            $return['message'] = "Category filters retreived successfully.";
        }else{
            $return['status'] = 201;
            $return['message'] = "Category not found.";
        }

        echo json_encode($return);
    }

    public function get_slider_price()
    {
        $query = '  SELECT MIN(p.prod_price) as min,MAX(p.prod_price) as max
                    FROM tbl_products p
                    INNER JOIN tbl_user u ON p.prod_user_id = u.user_id
                    INNER JOIN tbl_stores s ON s.user_id = u.user_id
                    INNER JOIN tbl_product_images i ON i.img_prod_id = p.prod_id 
                    INNER JOIN tbl_categories cat ON cat.cat_id = p.prod_cat_id
                    INNER JOIN tbl_product_filters f ON p.prod_id = f.prod_id 
                    WHERE 
                    p.prod_status = 1 AND
                    p.prod_is_delete = 0 AND
                    s.store_is_hide = 0 AND
                    u.user_is_delete = 0 AND
                    u.user_status = 1 
                    ';

        $result = $this->db->query($query)->row();

        echo json_encode($result);
    }
}
