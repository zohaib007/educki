<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['title'] = "Comming Soon";
		$this->form_validation->set_rules('sub_email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_message('required', 'This field is required.');
		$this->form_validation->set_message('valid_email', 'Please enter a valid email address.');
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view("index",$data);
		}else{
			$subEmail = $this->input->post('sub_email');
			$config = array(
						'protocol' => 'smtp',
						//'smtp_host'=>'smtp.mailgun.org',
						'smtp_host' => 'smtp.gmail.com',
						'smtp_port'=>465,
						'_smtp_auth'=>true, 
						'smtp_user'=>'mikesmith1166@gmail.com',
						'smtp_pass'=>'S1234mike',  
						'smtp_crypto'=>'ssl',
						'mailtype'=>'html', 
						'charset'=>'utf-8',
						'validate'=>true
					);
			
			$message = "Email: ".$subEmail."";
			$this->email->initialize($config);
			$this->email->set_newline("\r\n");
			$this->email->from('mikesmith1166@gmail.com');
			$this->email->to('educki.notify@gmail.com');
			$this->email->set_mailtype("html");
			$this->email->subject('You have a new subscriber!');
			$this->email->message($message);
			$this->email->send();
			//echo $this->email->print_debugger();
			//exit;
			
			$this->session->set_flashdata('sendmailOk',"Thanks");
			redirect(base_url());
			exit;
			/*
			if($this->email->send()){
				echo 1;
			}else{
				echo $this->email->print_debugger(); exit;
			}
			*/
		}
	}
}

?>