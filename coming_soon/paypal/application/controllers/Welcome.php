<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();
		$this->load->library('Paypal');
	}
	private $token_type;
	private $access_token;
	private $order_id;
	public function index()
	{


		echo '<pre>';
		$var = $this->paypal->GetToken();
		$var = json_decode($var);
		$this->access_token = $var->access_token;
		echo '<br/>';
		$this->token_type = $var->token_type;
		echo '<br/>';

		$this->session->set_userdata('access_token',$this->access_token);
		$this->session->set_userdata('token_type',$this->token_type);
		
		//echo 'CREATE ORDER API: <br/>';
		$order_api = $this->paypal->CreateOrder($this->token_type, $this->access_token);
		$order_api = json_decode($order_api);
		print_r($order_api);
		$this->order_id = $order_api->id;
		$this->session->set_userdata('order_id',$this->order_id);
		print_r($order_api->links);		
		//echo '<br/><a href="'.$order_api->links[1]->href.'" target="_blank">Pay $4.50 For Order.</a>';
	}

	public function pay_order(){
		echo '<pre>PAP FOR ORDER API: <br/>';
		
		$pay_api = $this->paypal->PayForOrder($this->session->userdata('token_type'), $this->session->userdata('access_token'), $this->session->userdata('order_id'));
		$pay_api = json_decode($pay_api);
		print_r($pay_api);
		echo '</hr>';
		print_r($this->session->all_userdata());
	}

	public function capture_order(){
		ini_set('display_errors', 0);
		echo '<pre> CAPTURE ORDER API: <br/>' ;
		$capture_api = $this->paypal->CaptureOrder($this->session->userdata('token_type'), $this->session->userdata('access_token'), $this->session->userdata('capture_id'));
		print_r(json_decode($capture_api));
		echo '</hr>';
		print_r($this->session->all_userdata());
	}

	public function order_detail(){
		echo '<pre>ORDER DETAIL API: <br/>' ;
		$capture_api = $this->paypal->GetOrderDetail($this->session->userdata('order_id'),$this->session->userdata('token_type'), $this->session->userdata('access_token'));
		$capture_api = json_decode($capture_api);
		//print_r($capture_api);
		$this->session->set_userdata('capture_id',@$capture_api->purchase_units[0]->payment_summary->captures[0]->id);
		$this->session->set_userdata('payment_id',@$capture_api->payment_details->payment_id);
		print_r(@$capture_api);
		echo '</hr>';
		print_r($this->session->all_userdata());
	}

	public function disburse_funds(){
		echo '<pre>Disburse Funds: <br/>' ;
		print_r($this->session->all_userdata());
		echo '</hr>';
		$disburse_api = $this->paypal->DisburseFunds($this->session->userdata('token_type'), $this->session->userdata('access_token'), $this->session->userdata('capture_id'));
		print_r($disburse_api);
	}
	public function return_url(){
		echo '<pre>';
		$get = $_GET;
		// print_r($get);
		// echo '</hr>';
		print_r($this->session->all_userdata());
		echo '</hr>';
		
		echo "<br/><br/>1. Pay Order API:  <a href='".base_url('pay-order')."' target='_blank'>Pay Order.</a>";
		echo "<br/><br/>2. Order Detail API:  <a href='".base_url('order-detail')."' target='_blank'>Order Detail</a>";
		echo "<br/><br/>3. Capture Order API:  <a href='".base_url('capture-order')."' target='_blank'>Capture Order</a>";
		echo "<br/><br/>4. Disburse Funds API:  <a href='".base_url('disburse-funds')."' target='_blank'>Disburse Funds.</a>";
		echo "<br/><br/>X. Create Webhookd API:  <a href='".base_url('create-webhooks')."' target='_blank'>Create Webhookd.</a>";

	}
	public function cancel_url(){
		$this->session->sess_destroy();
		echo "Return url. Go back: ".base_url();
	}

	public function remove_all(){
		$this->session->sess_destroy();
		echo "Return url. Go back: ".base_url();
	}

	public function getMerchantIdOfSeller(){
		$disburse_api = $this->paypal->getMerchantId($this->session->userdata('token_type'), $this->session->userdata('access_token'));
		print_r($disburse_api);
	}

	public function return_webhooks(){
		echo 'return-webhooks';
		
        $message = "Paypal Webhooks";
        $subject = "Webhooks";
        $from    = "info@paktra.com";
        $to      = "mikesmith1166@gmail.com,johnsmith1992311@gmail.com";
        
        $config  = array(
            'protocol'    => 'smtp',
            'smtp_host'=>'smtp.zoho.com',
            // 'smtp_host'   => 'smtp.gmail.com',
            'smtp_port'   => 465,
            '_smtp_auth'  => true,
			'smtp_user'   => 'info@paktra.com',
			'smtp_pass'   => 'Dotlogics123!!@@',
            'smtp_crypto' => 'ssl',
            'mailtype'    => 'html',
            'charset'     => 'utf-8',
            'validate'    => true,
        );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($from);
        $this->email->to($to);
        $this->email->set_mailtype("html");
        $this->email->subject($subject);
        $this->email->message($message);
        $is_send = $this->email->send();
        echo $this->email->print_debugger();
	}

	public function webhooks_create(){
		$created_webhooks = $this->paypal->createWebhooks($this->session->userdata('token_type'), $this->session->userdata('access_token'));
		print_r($created_webhooks);
	}
}
