<?php 
/**
- Seller Cloud SOAP API
- Code By Haseeb
- 06 Feb 2018
- Libary For Codeigniter
*/
Class Seller { 

	public function GetProductList(){
		// References: http://it.ws.sellercloud.com/scservice.asmx?op=LoadProducts
 		$soapUrl = "http://it.ws.sellercloud.com/scservice.asmx?op=LoadProducts"; // asmx URL of WSDL
        $soapUser = "Hamda@dotlogics.com";  //  username
        $soapPassword = "dotlogics"; // password
        // xml post structure

        $xml_post_string = '
        	<?xml version="1.0" encoding="utf-8"?>
			<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
			  <soap:Header>
			    <AuthHeader xmlns="http://api.sellercloud.com/">
			      <ApplicationName>newApp</ApplicationName>
			      <ApplicationVersion>10.22</ApplicationVersion>
			      <DeviceID>devID</DeviceID>
			      <ValidateDeviceID>0</ValidateDeviceID>
			      <UserName>Hamda@dotlogics.com</UserName>
			      <Password>dotlogics</Password>
			    </AuthHeader>
			    <ServiceOptions xmlns="http://api.sellercloud.com/">
			      <AlwaysRecalculateWeight>1</AlwaysRecalculateWeight>
			      <CalculateWeightIgnoreZeroWeightProducts>0</CalculateWeightIgnoreZeroWeightProducts>
			      <AllowAnyProductShippingMethods>0</AllowAnyProductShippingMethods>
			      <FetchUserDefinedColumnsForProducts>0</FetchUserDefinedColumnsForProducts>
			      <FetchUserDefinedColumnsForOrder>0</FetchUserDefinedColumnsForOrder>
			      <SkipBundleItemQtyUpdating>0</SkipBundleItemQtyUpdating>
			      <DoNotGetClientUser>0</DoNotGetClientUser>
			      <SkipCWAShippingRules>0</SkipCWAShippingRules>
			      <DontIncludePORMAImages>0</DontIncludePORMAImages>
			      <IncludeClientUserAddressBook>0</IncludeClientUserAddressBook>
			      <SaveOrderPackageDimensions>0</SaveOrderPackageDimensions>
			      <IncludeClientUserDocuments>0</IncludeClientUserDocuments>
			      <IncludeOrderTargetInfo>0</IncludeOrderTargetInfo>
			      <IncludeWalmartOrderSpecificData>0</IncludeWalmartOrderSpecificData>
			      <SkipProductMetadata>0</SkipProductMetadata>
			      <CreateNewProducts>0</CreateNewProducts>
			      <SplitItems>0</SplitItems>
			      <PaymentNotNeeded>0</PaymentNotNeeded>
			      <DebugInfo>string</DebugInfo>
			      <DoNotDownloadImageData>0</DoNotDownloadImageData>
			      <BulkWipeRelationships>0</BulkWipeRelationships>
			      <BulkDeleteShadows>0</BulkDeleteShadows>
			      <BulkResetOffsetQty>0</BulkResetOffsetQty>
			      <UseCache>0</UseCache>
			      <DontNeedCompanyProfile>0</DontNeedCompanyProfile>
			    </ServiceOptions>
			  </soap:Header>
			  <soap:Body>
			    <LoadProducts xmlns="http://api.sellercloud.com/">
			      <ProductIDs>
			        <string>string</string>
			        <string>string</string>
			      </ProductIDs>
			    </LoadProducts>
			  </soap:Body>
			</soap:Envelope>';
        $headers = array(
        				"POST /scservice.asmx HTTP/1.1",
                        "Content-Type: text/xml; charset=utf-8",
                        "Host: it.ws.sellercloud.com",
                        "SOAPAction: http://api.sellercloud.com/LoadProducts", 
                        "Content-length: ".strlen($xml_post_string),
                    );

        //$url = "http://api.sellercloud.com/LoadProducts";

        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_URL, $soapUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, "$soapUser:$soapPassword"); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // converting
        $response = curl_exec($ch); 
        curl_close($ch);

        var_dump($response);
        /*// converting
        $response1 = str_replace("<soap:Body>","",$response);
        $response2 = str_replace("</soap:Body>","",$response1);

        // convertingc to XML
        $parser = simplexml_load_string($response2);
        // user $parser to get your data out of XML response and to display it.*/
		
	}

}

?>