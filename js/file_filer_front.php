<script>
$(document).ready(function() {

    //Example 2
    $("#filer_input2").filer({
        //alert(1);
        limit: 9,
        maxSize: null,
        required: true,
        extensions: ['jpg', 'jpeg', 'png'],
        changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Drag&Drop files here</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn blue">Browse Files</a></div></div>',
        showThumbs: false,
        theme: "dragdropbox",
        dragDrop: {
            dragEnter: null,
            dragLeave: null,
            drop: null,
            dragContainer: null,
        },
        uploadFile: {
            url: base_url + "uploadProdImg",
            data: { unique_str: $('.unique_str').val(), <?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>' },
            type: 'POST',
            
            enctype: 'multipart/form-data',
            synchron: true,
            beforeSend: function() {},
            success: function(data, itemEl, listEl, boxEl, newInputEl, inputEl, id) {
                var parent = itemEl.find(".jFiler-jProgressBar").parent(),
                    new_file_name = JSON.parse(data),

                    filerKit = inputEl.prop("jFiler");

                var msg = "return confirm('Are you sure you want to delete selected item(s)?');";
                //var html = '<div class="rem_'+new_file_name.image_id+'"><img src="'+root_url+'resources/prod_images/thumb/'+new_file_name.name+'" alt="'+new_file_name.name+'" /><a title="Delete" href="javascript:void(0);" class="deleteProdImg" data-imgid = "'+new_file_name.image_id+'" onclick="'+msg+'"  >x</a></div>';
                var html = '<div><input type="hidden" name="img_name[]" value="' + new_file_name.name + '" /><img src="' + root_url + 'resources/prod_images/thumb/' + new_file_name.name + '" alt="' + new_file_name.name + '" /><a title="Delete" data-del-file="' + new_file_name.name + '" href="javascript:void(0);" class="deleteProdImg fileuploader-action fileuploader-action-remove" onclick="' + msg + '"  >x</a></div>';
                $('#imagesdata').append(html);

                /*$.each($('.cus-element'),function(i,v){
                		if($('.cus-element').length == 1){
                			$(this).find('input[type="radio"]').prop('checked',true);
                		}
                		$(this).find('input[type="radio"]').attr('id','recheck'+parseInt(parseInt(i)+1));
                		$(this).find('label').attr('for','recheck'+parseInt(parseInt(i)+1));
                	});*/
                filerKit.files_list[id].name = new_file_name;

                itemEl.find(".jFiler-jProgressBar").fadeOut("slow", function() {
                    $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Success</div>").hide().appendTo(parent).fadeIn("slow");
                });
            },
            error: function(el) {
                var parent = el.find(".jFiler-jProgressBar").parent();
                el.find(".jFiler-jProgressBar").fadeOut("slow", function() {
                    $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
                });
            },
            statusCode: null,
            onProgress: null,
            onComplete: null
        },
        files: null,
        addMore: false,
        allowDuplicates: true,
        clipBoardPaste: true,
        excludeName: null,
        beforeRender: null,
        afterRender: null,
        beforeShow: null,
        beforeSelect: null,
        onSelect: null,
        afterShow: null,
        onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl) {
            alert('remove');
            var filerKit = inputEl.prop("jFiler"),
                file_name = filerKit.files_list[id].name;

            //$.post('./php/ajax_remove_file.php', {file: file_name});
        },
        onEmpty: null,
        options: null,
        dialogs: {
            alert: function(text) {
                return alert(text);
            },
            confirm: function(text, callback) {
                confirm(text) ? callback() : null;
            }
        },
        captions: {
            button: "Choose Files",
            feedback: "Choose files To Upload",
            feedback2: "files were chosen",
            drop: "Drop file here to Upload",
            removeConfirmation: "Are you sure you want to remove this file?",
            errors: {
                filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
                filesType: "Only Images are allowed to be uploaded.",
                filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-maxSize}} MB.",
                filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB."
            }
        }
    });

});

function delete_fun() {}
</script>