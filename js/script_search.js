/*
* Author:      Marco Kuiper (http://www.marcofolio.net/)
*/
//google.load("jquery", "1.3.1");
google.setOnLoadCallback(function()
{
	// Safely inject CSS3 and give the search results a shadow
	var cssObj = { 'box-shadow' : '#888 5px 10px 10px', // Added when CSS3 is standard
		'-webkit-box-shadow' : '#888 5px 10px 10px', // Safari
		'-moz-box-shadow' : '#888 5px 10px 10px'}; // Firefox 3.5+
	//$("#suggestions").css(cssObj);
	
	// Fade out the suggestions box when not active
	 $("input").blur(function(){
	 	$('#suggestions').fadeOut();
		$('#suggestions2').fadeOut();
	 });
});

function lookup(inputString,val2) { 
	if(inputString.length == 0) {
		$('#suggestions').fadeOut(); // Hide the suggestions box
		$('#suggestions2').fadeOut(); // Hide the suggestions box
	} else { //alert(base_url);
		$.post(base_url+"search", {queryString: ""+inputString+""}, function(data) { //alert('1'); // Do an AJAX call
			if(val2==2) 
			{
				$('#suggestions2').fadeIn();			 
				$('#suggestions2').html(data); 
			}
			else
			{
				$('#suggestions').fadeIn();			 
				$('#suggestions').html(data);
			}
			 
		});
	}
}