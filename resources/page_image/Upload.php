<?php
class Upload extends CI_Controller
{

    public function __construct()
    {
        $this->load->library('upload');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    }

    public function index()
    {
        echo '<pre>';
        //print_r($_FILES);
        
        $myFiles1 = $_FILES['image'];
        $fileDP1 = array();
        $config = array(
            'allowed_types' => 'jpg|jpeg|gif|png',
            'upload_path' => FCPATH . '/resources/page_image/',
            'file_name' => 'TEST_' . date('m_d_Y') . '_' . time(),
        );

        $this->upload->initialize($config);
        //$this->upload->do_upload('file');
        echo $this->upload->do_upload('image');
        echo $this->upload->display_errors();
        // if (!$this->upload->do_upload('image')) {
        //     echo $this->upload->display_errors();
        //     exit;
        // } else {
        //     echo "Image Upload Name: ".$config['file_name'];
        //     exit;
        //     //////uplaod thumbnail image
        //     // $dataDP = $this->upload->data();
        //     // $fileDP1 = $dataDP['file_name'];
        //     // $this->load->library('image_lib');
        //     // $configt['image_library'] = 'gd2';
        //     // $configt['source_image'] = $dataDP['full_path'];
        //     // $configt['new_image'] = FCPATH . "/resources/page_image/thumb/";
        //     // $configt['maintain_ratio'] = false;
        //     // $configt['width'] = 80;
        //     // $configt['height'] = 80;
        //     // $this->image_lib->initialize($configt);
        //     // $this->image_lib->resize();
        // }
    }
}
