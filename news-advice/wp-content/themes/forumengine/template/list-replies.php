<?php
	/**
	 * List replies
	 */
global $replies_query;
?>
	<div id="replies_list">
		<div itemscope itemtype="http://schema.org/ItemList">
		<meta itemprop="itemListOrder" content="Descending" />
		<?php
			global $et_repliesData;
			$et_repliesData = array();
			if ( $replies_query->have_posts() ){
				while ( $replies_query->have_posts() ) {
					global $post;
					$replies_query->the_post();
					$reply 				= FE_Replies::convert($post);
					$et_repliesData[] 	= $reply;
					get_template_part( 'template/reply', 'item' );
				}// end while
			} //end if
		?>
		</div>
	</div>
	<!-- end items replies -->