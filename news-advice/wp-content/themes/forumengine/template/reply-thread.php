<?php
global $post, $thread;
?>
<!-- EDITOR -->
<div id="form_thread" class="post-edit thread-form edit-thread collapse">
	<form class="form-post-edit" action="" method="post">
		<input type="hidden" name="fe_nonce" value="<?php echo wp_create_nonce( 'edit_thread' ) ?>">
		<div class="text-search">
			<div class="input-container">
				<input class="inp-title" name="post_title" maxlength="90" type="text" value="<?php echo $post->post_title ?>" placeholder="<?php _e('Click here to start your new topic' , ET_DOMAIN) ?>">
			</div>
			<input type="hidden" name="ID" value="<?php echo $post->ID ?>">
			<div class="btn-group cat-dropdown dropdown category-search-items">
				<span class="line"></span>
				<button class="btn dropdown-toggle" data-toggle="dropdown">
					<span class="text-select"></span>
					<span class="caret"></span>
				</button>
				<?php $current_cat = empty($thread->thread_category[0]) ? false : $thread->thread_category[0]->term_id ?>
				<?php
				$categories = FE_ThreadCategory::get_categories();//FE_Threads::get_categories(array('hide_empty'=>false));
				?>
				<select class="collapse" name="thread_category" id="thread_category">
					<option value=""><?php _e('Please select' , ET_DOMAIN) ?></option>
					<?php et_the_cat_select($categories, $current_cat); ?>
				</select>
			</div>
	  	</div>
		<div class="form-detail">
			<div id="wp-<?php echo 'edit_post_content' . $post->ID ?>-editor-container" class="wp-editor-container">
				<textarea name="post_content" id="<?php echo 'edit_post_content' . $post->ID?>"><?php echo $post->post_content ?></textarea>
			</div>
			<?php do_action( 'fe_custom_fields_form' ); ?>
			<div class="row line-bottom">
				<div class="col-md-6 col-sm-6">
					<div class="button-event">
						<input type="submit" value="<?php _e('Update', ET_DOMAIN) ?>" data-loading-text="<?php _e("Loading...", ET_DOMAIN); ?>" class="btn">
						<a href="#" class="cancel control-edit-cancel"><span class="btn-cancel"><span class="icon" data-icon="D"></span><?php _e('Cancel', ET_DOMAIN) ?></span></a>
					</div>
				</div>
			</div>
		</div>
	</form> <!-- end EDITOR -->
</div> <!-- end form -->