/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.20-0ubuntu0.16.04.1 : Database - educki
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `tbl_admin` */

DROP TABLE IF EXISTS `tbl_admin`;

CREATE TABLE `tbl_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_fname` varchar(200) NOT NULL,
  `admin_lname` varchar(200) DEFAULT NULL,
  `admin_username` varchar(150) DEFAULT NULL,
  `admin_email` varchar(200) NOT NULL,
  `admin_password` varchar(100) NOT NULL,
  `admin_password_md5` varchar(150) NOT NULL,
  `admin_status` tinyint(1) NOT NULL COMMENT '0=Inactive; 1=Active',
  `admin_is_superadmin` tinyint(1) NOT NULL DEFAULT '0',
  `admin_date` datetime DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_admin` */

insert  into `tbl_admin`(`admin_id`,`admin_fname`,`admin_lname`,`admin_username`,`admin_email`,`admin_password`,`admin_password_md5`,`admin_status`,`admin_is_superadmin`,`admin_date`) values (1,'Mike','Smith','admin','mikesmith1166@gmail.com','admin','21232f297a57a5a743894a0e4a801fc3',1,1,'2017-10-11 10:51:02');

/*Table structure for table `tbl_blog` */

DROP TABLE IF EXISTS `tbl_blog`;

CREATE TABLE `tbl_blog` (
  `blog_id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_name` text,
  `blog_url` text,
  `blog_image` varchar(100) DEFAULT NULL,
  `blog_description` longtext,
  `blog_author` varchar(200) DEFAULT NULL,
  `blog_status` int(1) DEFAULT '0',
  `blog_date` datetime DEFAULT NULL,
  `blog_created_date` datetime DEFAULT NULL,
  `blog_admin_id` int(11) DEFAULT NULL,
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_keywords` varchar(200) DEFAULT NULL,
  `meta_description` varchar(200) DEFAULT NULL,
  `blog_category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`blog_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_blog` */

insert  into `tbl_blog`(`blog_id`,`blog_name`,`blog_url`,`blog_image`,`blog_description`,`blog_author`,`blog_status`,`blog_date`,`blog_created_date`,`blog_admin_id`,`meta_title`,`meta_keywords`,`meta_description`,`blog_category_id`) values (1,'Sample blog Name here.','-2',NULL,'Sample text here','Dotlogics',1,NULL,'2017-10-17 02:50:55',NULL,'meta title','meta keyword','Meta description',1),(2,'New Blog Name','-1','img_2017_10_16_12_49_07.jpg','Sample text here','eDucki',1,NULL,'2017-10-16 07:00:19',NULL,'1','2','3',1),(3,'new Blog','new-blog','img_2017_10_16_07_01_47.jpg','Sample blog text here.sample blog text here.','Test Name',1,NULL,'2017-10-16 07:01:47',1,'369','963','693',1);

/*Table structure for table `tbl_blog_categories` */

DROP TABLE IF EXISTS `tbl_blog_categories`;

CREATE TABLE `tbl_blog_categories` (
  `bcat_id` int(11) NOT NULL AUTO_INCREMENT,
  `bcat_blog_id` int(11) DEFAULT NULL,
  `bcat_blog_cat_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`bcat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_blog_categories` */

/*Table structure for table `tbl_blog_category` */

DROP TABLE IF EXISTS `tbl_blog_category`;

CREATE TABLE `tbl_blog_category` (
  `blog_cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_cat_name` varchar(200) DEFAULT NULL,
  `blog_cat_url` varchar(200) DEFAULT NULL,
  `blog_cat_status` tinyint(1) DEFAULT NULL,
  `blog_cat_date` datetime DEFAULT NULL,
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_keywords` varchar(200) DEFAULT NULL,
  `meta_description` varchar(200) DEFAULT NULL,
  `blog_cat_admin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`blog_cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_blog_category` */

insert  into `tbl_blog_category`(`blog_cat_id`,`blog_cat_name`,`blog_cat_url`,`blog_cat_status`,`blog_cat_date`,`meta_title`,`meta_keywords`,`meta_description`,`blog_cat_admin_id`) values (1,'Test Blog Category','test-blog-category',0,'2017-10-16 06:41:59','111','22','334',1);

/*Table structure for table `tbl_career` */

DROP TABLE IF EXISTS `tbl_career`;

CREATE TABLE `tbl_career` (
  `career_id` int(10) NOT NULL AUTO_INCREMENT,
  `career_admin_id` int(10) NOT NULL,
  `career_title` varchar(100) NOT NULL,
  `meta_title` varchar(200) DEFAULT NULL,
  `career_image` varchar(100) DEFAULT NULL,
  `career_image_thumb` varchar(100) DEFAULT NULL,
  `meta_keywords` varchar(200) DEFAULT NULL,
  `meta_description` varchar(200) DEFAULT NULL,
  `career_area` varchar(100) DEFAULT NULL,
  `career_description` varchar(100) DEFAULT NULL,
  `career_city` varchar(100) DEFAULT NULL,
  `career_state` varchar(100) DEFAULT NULL,
  `career_created_date` datetime NOT NULL,
  `career_updated_date` datetime DEFAULT NULL,
  `career_end_date` datetime DEFAULT NULL,
  `career_is_deleted` tinyint(1) DEFAULT '0',
  `career_is_active` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`career_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_career` */

insert  into `tbl_career`(`career_id`,`career_admin_id`,`career_title`,`meta_title`,`career_image`,`career_image_thumb`,`meta_keywords`,`meta_description`,`career_area`,`career_description`,`career_city`,`career_state`,`career_created_date`,`career_updated_date`,`career_end_date`,`career_is_deleted`,`career_is_active`) values (2,1,'Front End Developer','new','img_3571508319650.png','img_1491508319650.png','bew','newe','testing','Thixxs is the description of new carrier','lahore','VA','2017-10-24 08:17:57','2017-10-17 05:21:54',NULL,0,1),(4,1,'new test','dcsd','img_7411508239997.png','img_7681508239997.png','dcs','csdsd','test area','ascsdcsd','','','2017-10-17 07:33:17',NULL,NULL,0,0),(5,1,'Senior Web Developer','career title',NULL,NULL,'career keyword','career Description','it','dcssc','Lahore','DE','2017-10-24 08:16:56',NULL,NULL,0,1),(6,1,'Web Developer (PHP)','','img_1281508481645.png','img_5021508481645.png','','this is a test decthis is a test decthis is a test decthis is a test decthis is a test dec','New','this is a test decthis is a test decthis is a test decthis is a test decthis is a test dec','bfgfbfgbfgbf','CO','2017-10-24 08:13:53',NULL,'2017-10-31 00:00:00',0,1),(7,1,'Php Developer','',NULL,NULL,'','this is a test decthis is a test decthis is a test decthis is a test decthis is a test dec','New','this is a test decthis is a test decthis is a test decthis is a test decthis is a test dec','bfgfbfgbfgbf','HI','2017-10-24 08:14:09',NULL,NULL,0,1),(8,1,'new 2',NULL,NULL,NULL,NULL,'this is a test decthis is a test decthis is a test decthis is a test decthis is a test dec','New','this is a test decthis is a test decthis is a test decthis is a test decthis is a test dec','bfgfbfgbfgbf','AL','2017-10-19 00:00:00',NULL,NULL,0,0),(9,1,'Front End Developer','',NULL,NULL,'','this is a test decthis is a test decthis is a test decthis is a test decthis is a test dec','New','this is a test decthis is a test decthis is a test decthis is a test decthis is a test dec','bfgfbfgbfgbf','DC','2017-10-24 08:14:30',NULL,NULL,0,1),(10,1,'new 2',NULL,NULL,NULL,NULL,'this is a test decthis is a test decthis is a test decthis is a test decthis is a test dec','New','this is a test decthis is a test decthis is a test decthis is a test decthis is a test dec','bfgfbfgbfgbf','AL','2017-10-19 00:00:00',NULL,NULL,0,0),(11,1,'Senior Web Developer','',NULL,NULL,'','this is a test decthis is a test decthis is a test decthis is a test decthis is a test dec','New','this is a test decthis is a test decthis is a test decthis is a test decthis is a test dec','bfgfbfgbfgbf','LA','2017-10-24 08:14:54',NULL,NULL,0,1),(12,1,'new 2',NULL,NULL,NULL,NULL,'this is a test decthis is a test decthis is a test decthis is a test decthis is a test dec','New','this is a test decthis is a test decthis is a test decthis is a test decthis is a test dec','bfgfbfgbfgbf','AL','2017-10-19 00:00:00',NULL,NULL,0,0),(13,1,'Senior Web Developer (wordpress)','',NULL,NULL,'','this is a test decthis is a test decthis is a test decthis is a test decthis is a test dec','New','this is a test decthis is a test decthis is a test decthis is a test decthis is a test dec','bfgfbfgbfgbf','WA','2017-10-24 08:15:17',NULL,NULL,0,1);

/*Table structure for table `tbl_career_page` */

DROP TABLE IF EXISTS `tbl_career_page`;

CREATE TABLE `tbl_career_page` (
  `carrier_page_id` int(10) NOT NULL AUTO_INCREMENT,
  `career_page_title` varchar(50) DEFAULT NULL,
  `career_page_description` varchar(300) DEFAULT NULL,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_description` varchar(300) DEFAULT NULL,
  `meta_keyword` varchar(100) DEFAULT NULL,
  `career_page_banner_image` varchar(50) DEFAULT NULL,
  `career_page_banner_title` varchar(50) DEFAULT NULL,
  `career_page_title_first` varchar(50) DEFAULT NULL,
  `career_page_description_first` varchar(300) DEFAULT NULL,
  `career_page_image_first` varchar(50) DEFAULT NULL,
  `career_page_title_second` varchar(50) DEFAULT NULL,
  `career_page_description_second` varchar(300) DEFAULT NULL,
  `career_page_image_second` varchar(50) DEFAULT NULL,
  `career_page_title_third` varchar(50) DEFAULT NULL,
  `career_page_description_third` varchar(300) DEFAULT NULL,
  `career_page_image_third` varchar(50) DEFAULT NULL,
  `career_is_active` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`carrier_page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_career_page` */

insert  into `tbl_career_page`(`carrier_page_id`,`career_page_title`,`career_page_description`,`meta_title`,`meta_description`,`meta_keyword`,`career_page_banner_image`,`career_page_banner_title`,`career_page_title_first`,`career_page_description_first`,`career_page_image_first`,`career_page_title_second`,`career_page_description_second`,`career_page_image_second`,`career_page_title_third`,`career_page_description_third`,`career_page_image_third`,`career_is_active`) values (1,'career','Career page Description','meta title','met description','meta keyword','image_2017_10_31_08_32_28.jpg','Career','This is a First Image','This is the first image description This is the fi','image_2017_10_31_08_32_281.jpg','second image title','This is the second image description This is the s','image_2017_10_31_08_32_282.jpg','Third Image Title','This is the Third image description This is the Th','image_2017_10_31_08_32_283.jpg',0);

/*Table structure for table `tbl_careers_applied_user` */

DROP TABLE IF EXISTS `tbl_careers_applied_user`;

CREATE TABLE `tbl_careers_applied_user` (
  `career_user_id` int(10) NOT NULL AUTO_INCREMENT,
  `career_id` int(10) NOT NULL,
  `career_user_first_name` varchar(100) NOT NULL,
  `career_user_last_name` varchar(100) NOT NULL,
  `career_user_email` varchar(100) NOT NULL,
  `career_user_phone` varchar(50) NOT NULL,
  `career_user_website` varchar(100) DEFAULT NULL,
  `career_user_linkedin_profile` varchar(100) DEFAULT NULL,
  `career_user_resume` varchar(100) DEFAULT NULL,
  `career_user_cover` varchar(100) DEFAULT NULL,
  `career_user_hear_from` varchar(100) DEFAULT NULL,
  `career_user_status` int(5) NOT NULL DEFAULT '1' COMMENT '1=delivered,2=seen,3=shortlisted',
  `career_user_applied_date` datetime NOT NULL,
  `career_user_is_delete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`career_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_careers_applied_user` */

insert  into `tbl_careers_applied_user`(`career_user_id`,`career_id`,`career_user_first_name`,`career_user_last_name`,`career_user_email`,`career_user_phone`,`career_user_website`,`career_user_linkedin_profile`,`career_user_resume`,`career_user_cover`,`career_user_hear_from`,`career_user_status`,`career_user_applied_date`,`career_user_is_delete`) values (4,4,'new','last','test@test.com','6543128','http://www.test.com','http://linkedin.com','new.docx','new.docx','facebook',2,'2017-10-20 00:00:00',0),(5,4,'new','last','test@test.com','6543128','http://www.test.com','http://linkedin.com','new.docx','new.docx','facebook',1,'2017-10-20 00:00:00',0),(6,0,'waleed r','raza','waleed@test.com','6549879','http://www.website.com','http://linkedin.com',NULL,NULL,'social media',1,'2017-10-25 00:00:00',0),(7,5,'waleed','raza','mikesmith1166@gmail.com','987654321','http://www.website.com','http://linkedin.com','resume_2017_10_25_04_08_39.docx','cover_2017_10_25_04_08_39.txt','facebook',2,'2017-10-25 04:08:39',0),(8,5,'waleed','raza','mikesmith1166@gmail.com','987654321','http://www.website.com','http://linkedin.com','resume_2017_10_25_04_10_24.docx','cover_2017_10_25_04_10_24.txt','facebook',1,'2017-10-25 04:10:24',0),(9,5,'waleed','raza','mikesmith1166@gmail.com','987654321','http://www.website.com','http://linkedin.com','resume_2017_10_25_05_13_06.docx','cover_2017_10_25_05_13_06.txt','facebook',1,'2017-10-25 05:13:06',0),(10,5,'waleed','raza','mikesmith1166@gmail.com','987654321','http://www.website.com','http://linkedin.com','resume_2017_10_25_05_13_33.docx','cover_2017_10_25_05_13_33.txt','facebook',1,'2017-10-25 05:13:33',0),(18,2,'waleed','raza','waleed@test.com','654789','http://www.website.com','http://linkedin.com','resume_2017_10_26_01_07_47.docx','cover_2017_10_26_01_07_47.docx','facebook',2,'2017-10-26 01:07:47',0);

/*Table structure for table `tbl_cart` */

DROP TABLE IF EXISTS `tbl_cart`;

CREATE TABLE `tbl_cart` (
  `cart_id` int(11) NOT NULL AUTO_INCREMENT,
  `cart_user_id` int(11) DEFAULT NULL,
  `cart_prod_id` int(11) DEFAULT NULL,
  `cart_qty` int(11) DEFAULT NULL,
  `cart_color_id` int(11) DEFAULT NULL,
  `cart_size_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`cart_id`),
  KEY `cart_detail_prod_id` (`cart_prod_id`),
  KEY `cart_detail_color_id` (`cart_color_id`),
  KEY `cart_detail_size_id` (`cart_size_id`),
  KEY `tbl_cart_detail_ibfk_1` (`cart_user_id`),
  CONSTRAINT `tbl_cart_ibfk_1` FOREIGN KEY (`cart_user_id`) REFERENCES `tbl_user` (`user_id`),
  CONSTRAINT `tbl_cart_ibfk_2` FOREIGN KEY (`cart_prod_id`) REFERENCES `tbl_products` (`prod_id`),
  CONSTRAINT `tbl_cart_ibfk_3` FOREIGN KEY (`cart_color_id`) REFERENCES `tbl_product_color` (`color_id`),
  CONSTRAINT `tbl_cart_ibfk_4` FOREIGN KEY (`cart_size_id`) REFERENCES `tbl_product_size` (`size_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_cart` */

/*Table structure for table `tbl_cat_filter_detail` */

DROP TABLE IF EXISTS `tbl_cat_filter_detail`;

CREATE TABLE `tbl_cat_filter_detail` (
  `filter_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_detail_title_id` int(11) NOT NULL,
  `filter_detail_cat_id` int(11) NOT NULL,
  `filter_title` varchar(255) NOT NULL,
  `filter_detail` text NOT NULL,
  PRIMARY KEY (`filter_detail_id`),
  KEY `filter_detail_id` (`filter_detail_id`,`filter_detail_title_id`,`filter_detail_cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_cat_filter_detail` */

/*Table structure for table `tbl_cat_filter_title` */

DROP TABLE IF EXISTS `tbl_cat_filter_title`;

CREATE TABLE `tbl_cat_filter_title` (
  `filter_title_id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_cat_id` int(11) NOT NULL,
  `filter_title` varchar(150) NOT NULL,
  `cat_filter_values` text NOT NULL,
  `cat_filter_is_conditional` tinyint(4) NOT NULL,
  `is_required` tinyint(4) NOT NULL,
  PRIMARY KEY (`filter_title_id`),
  KEY `filter_title_id` (`filter_title_id`,`filter_cat_id`,`filter_title`),
  KEY `filter_title` (`filter_title`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_cat_filter_title` */

/*Table structure for table `tbl_categories` */

DROP TABLE IF EXISTS `tbl_categories`;

CREATE TABLE `tbl_categories` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_parent_id` int(11) NOT NULL DEFAULT '0',
  `cat_level` tinyint(2) DEFAULT '1',
  `cat_name` varchar(200) NOT NULL,
  `cat_desc` longtext NOT NULL,
  `cat_image` varchar(100) DEFAULT NULL,
  `cat_image_thumb` varchar(50) DEFAULT NULL,
  `cat_url` varchar(255) NOT NULL,
  `cat_status` tinyint(1) NOT NULL DEFAULT '0',
  `meta_title` varchar(200) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_desc` varchar(255) NOT NULL,
  `cat_is_delete` tinyint(1) NOT NULL,
  `cat_sort` int(11) NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `cat_id` (`cat_id`,`cat_name`,`cat_url`,`cat_status`,`meta_title`,`meta_keyword`,`meta_desc`),
  KEY `cat_is_delete` (`cat_is_delete`),
  KEY `cat_parent_id` (`cat_parent_id`),
  KEY `cat_name` (`cat_name`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_categories` */

insert  into `tbl_categories`(`cat_id`,`cat_parent_id`,`cat_level`,`cat_name`,`cat_desc`,`cat_image`,`cat_image_thumb`,`cat_url`,`cat_status`,`meta_title`,`meta_keyword`,`meta_desc`,`cat_is_delete`,`cat_sort`) values (24,0,1,'Test Category','Sample text here.','img_3931504873071.png','img_4571504873071.png','test-category',0,'meta title','meta keyword','meta description',0,2),(25,24,2,'test sub category','Sample text here. Sample text here. sample text here. Sample text here.1','img_4701506666486.png','img_5001506666486.png','test-sub-category-3',1,'Subcategory meta titles1','subcategory meta keyword1','subcategory meta description1',0,1),(26,24,2,'test sub category 2','Sample text here. Sample text here.','img_1091506666670.png','img_6931506666670.png','test-sub-category-2',1,'sample sub category meta titles','subcategory meta key words 2','subcategory meta description 2.',0,2),(27,25,3,'test sub sub category','Sample text here. Sample text here.','img_1091506666670.png','img_6931506666670.png','test-sub-sub-category',1,'sample sub category meta titles','subcategory meta key words 2','subcategory meta description 2.',0,1),(28,0,1,'Test Main Category','Sample text here. Sample text here.','img_1551507281925.png','img_4501507281925.png','test-main-category',1,'Test meta main category','test meta keywords main category','sample text here.',0,1),(29,28,2,'main cat sub','Sample text here','img_2951507282088.png','img_8191507282088.png','main-cat-sub',1,'meta title','meta keyword','meta description',0,5),(30,29,3,'main sub sub category','Sample text here.','img_6741507282149.png','img_2751507282149.png','main-sub-sub-category',1,'meta title','stasg','meta description',0,6);

/*Table structure for table `tbl_category_filters` */

DROP TABLE IF EXISTS `tbl_category_filters`;

CREATE TABLE `tbl_category_filters` (
  `filter_id` int(11) NOT NULL AUTO_INCREMENT,
  `filter_name` varchar(250) DEFAULT NULL,
  `filter_type` varchar(100) DEFAULT NULL,
  `filter_value` text,
  `filter_category_id` int(11) DEFAULT NULL,
  `filter_is_required` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`filter_id`),
  KEY `filter_category_id` (`filter_category_id`),
  CONSTRAINT `tbl_category_filters_ibfk_1` FOREIGN KEY (`filter_category_id`) REFERENCES `tbl_categories` (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_category_filters` */

/*Table structure for table `tbl_contact_form` */

DROP TABLE IF EXISTS `tbl_contact_form`;

CREATE TABLE `tbl_contact_form` (
  `c_id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(150) NOT NULL,
  `email` varchar(200) NOT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `cat_id` int(50) DEFAULT NULL,
  `cat_name` varchar(100) DEFAULT NULL,
  `comment` text NOT NULL,
  `date_created` datetime NOT NULL,
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `is_resolved` tinyint(4) NOT NULL DEFAULT '0',
  `resolved_date` datetime DEFAULT NULL,
  `filename` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_contact_form` */

insert  into `tbl_contact_form`(`c_id`,`fname`,`email`,`subject`,`cat_id`,`cat_name`,`comment`,`date_created`,`is_delete`,`is_resolved`,`resolved_date`,`filename`) values (45,'Zohaib','zohaib@topdot.pk','',0,'','order','2017-10-30 03:16:48',0,0,'0000-00-00 00:00:00',''),(46,'Zohaib','zohaib@topdot.com','order',8,'Problem with order','order','2017-10-30 03:19:40',0,0,'0000-00-00 00:00:00',''),(47,'Ali','Ali@topdot.pk','asasasasa',4,'Other Product Details','order','2017-10-30 03:22:58',0,0,'0000-00-00 00:00:00',''),(48,'zohaib','zohaib@topdot.pk','cancel',6,'Cancel an item or order','cancel order','2017-10-30 03:24:48',0,0,'0000-00-00 00:00:00','file_2017_10_30_03_2'),(49,'Zohaib','zohaib@topdot.pk','Order Problem',8,'Problem with order','I did not received my order which was due today.','2017-10-31 08:37:38',0,0,NULL,NULL);

/*Table structure for table `tbl_country` */

DROP TABLE IF EXISTS `tbl_country`;

CREATE TABLE `tbl_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=240 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_country` */

insert  into `tbl_country`(`id`,`iso`,`name`,`nicename`,`iso3`,`numcode`,`phonecode`) values (1,'AF','AFGHANISTAN','Afghanistan','AFG',4,93),(2,'AL','ALBANIA','Albania','ALB',8,355),(3,'DZ','ALGERIA','Algeria','DZA',12,213),(4,'AS','AMERICAN SAMOA','American Samoa','ASM',16,1684),(5,'AD','ANDORRA','Andorra','AND',20,376),(6,'AO','ANGOLA','Angola','AGO',24,244),(7,'AI','ANGUILLA','Anguilla','AIA',660,1264),(8,'AQ','ANTARCTICA','Antarctica',NULL,NULL,0),(9,'AG','ANTIGUA AND BARBUDA','Antigua and Barbuda','ATG',28,1268),(10,'AR','ARGENTINA','Argentina','ARG',32,54),(11,'AM','ARMENIA','Armenia','ARM',51,374),(12,'AW','ARUBA','Aruba','ABW',533,297),(13,'AU','AUSTRALIA','Australia','AUS',36,61),(14,'AT','AUSTRIA','Austria','AUT',40,43),(15,'AZ','AZERBAIJAN','Azerbaijan','AZE',31,994),(16,'BS','BAHAMAS','Bahamas','BHS',44,1242),(17,'BH','BAHRAIN','Bahrain','BHR',48,973),(18,'BD','BANGLADESH','Bangladesh','BGD',50,880),(19,'BB','BARBADOS','Barbados','BRB',52,1246),(20,'BY','BELARUS','Belarus','BLR',112,375),(21,'BE','BELGIUM','Belgium','BEL',56,32),(22,'BZ','BELIZE','Belize','BLZ',84,501),(23,'BJ','BENIN','Benin','BEN',204,229),(24,'BM','BERMUDA','Bermuda','BMU',60,1441),(25,'BT','BHUTAN','Bhutan','BTN',64,975),(26,'BO','BOLIVIA','Bolivia','BOL',68,591),(27,'BA','BOSNIA AND HERZEGOVINA','Bosnia and Herzegovina','BIH',70,387),(28,'BW','BOTSWANA','Botswana','BWA',72,267),(29,'BV','BOUVET ISLAND','Bouvet Island',NULL,NULL,0),(30,'BR','BRAZIL','Brazil','BRA',76,55),(31,'IO','BRITISH INDIAN OCEAN TERRITORY','British Indian Ocean Territory',NULL,NULL,246),(32,'BN','BRUNEI DARUSSALAM','Brunei Darussalam','BRN',96,673),(33,'BG','BULGARIA','Bulgaria','BGR',100,359),(34,'BF','BURKINA FASO','Burkina Faso','BFA',854,226),(35,'BI','BURUNDI','Burundi','BDI',108,257),(36,'KH','CAMBODIA','Cambodia','KHM',116,855),(37,'CM','CAMEROON','Cameroon','CMR',120,237),(38,'CA','CANADA','Canada','CAN',124,1),(39,'CV','CAPE VERDE','Cape Verde','CPV',132,238),(40,'KY','CAYMAN ISLANDS','Cayman Islands','CYM',136,1345),(41,'CF','CENTRAL AFRICAN REPUBLIC','Central African Republic','CAF',140,236),(42,'TD','CHAD','Chad','TCD',148,235),(43,'CL','CHILE','Chile','CHL',152,56),(44,'CN','CHINA','China','CHN',156,86),(45,'CX','CHRISTMAS ISLAND','Christmas Island',NULL,NULL,61),(46,'CC','COCOS (KEELING) ISLANDS','Cocos (Keeling) Islands',NULL,NULL,672),(47,'CO','COLOMBIA','Colombia','COL',170,57),(48,'KM','COMOROS','Comoros','COM',174,269),(49,'CG','CONGO','Congo','COG',178,242),(50,'CD','CONGO, THE DEMOCRATIC REPUBLIC OF THE','Congo, the Democratic Republic of the','COD',180,242),(51,'CK','COOK ISLANDS','Cook Islands','COK',184,682),(52,'CR','COSTA RICA','Costa Rica','CRI',188,506),(53,'CI','COTE D\'IVOIRE','Cote D\'Ivoire','CIV',384,225),(54,'HR','CROATIA','Croatia','HRV',191,385),(55,'CU','CUBA','Cuba','CUB',192,53),(56,'CY','CYPRUS','Cyprus','CYP',196,357),(57,'CZ','CZECH REPUBLIC','Czech Republic','CZE',203,420),(58,'DK','DENMARK','Denmark','DNK',208,45),(59,'DJ','DJIBOUTI','Djibouti','DJI',262,253),(60,'DM','DOMINICA','Dominica','DMA',212,1767),(61,'DO','DOMINICAN REPUBLIC','Dominican Republic','DOM',214,1809),(62,'EC','ECUADOR','Ecuador','ECU',218,593),(63,'EG','EGYPT','Egypt','EGY',818,20),(64,'SV','EL SALVADOR','El Salvador','SLV',222,503),(65,'GQ','EQUATORIAL GUINEA','Equatorial Guinea','GNQ',226,240),(66,'ER','ERITREA','Eritrea','ERI',232,291),(67,'EE','ESTONIA','Estonia','EST',233,372),(68,'ET','ETHIOPIA','Ethiopia','ETH',231,251),(69,'FK','FALKLAND ISLANDS (MALVINAS)','Falkland Islands (Malvinas)','FLK',238,500),(70,'FO','FAROE ISLANDS','Faroe Islands','FRO',234,298),(71,'FJ','FIJI','Fiji','FJI',242,679),(72,'FI','FINLAND','Finland','FIN',246,358),(73,'FR','FRANCE','France','FRA',250,33),(74,'GF','FRENCH GUIANA','French Guiana','GUF',254,594),(75,'PF','FRENCH POLYNESIA','French Polynesia','PYF',258,689),(76,'TF','FRENCH SOUTHERN TERRITORIES','French Southern Territories',NULL,NULL,0),(77,'GA','GABON','Gabon','GAB',266,241),(78,'GM','GAMBIA','Gambia','GMB',270,220),(79,'GE','GEORGIA','Georgia','GEO',268,995),(80,'DE','GERMANY','Germany','DEU',276,49),(81,'GH','GHANA','Ghana','GHA',288,233),(82,'GI','GIBRALTAR','Gibraltar','GIB',292,350),(83,'GR','GREECE','Greece','GRC',300,30),(84,'GL','GREENLAND','Greenland','GRL',304,299),(85,'GD','GRENADA','Grenada','GRD',308,1473),(86,'GP','GUADELOUPE','Guadeloupe','GLP',312,590),(87,'GU','GUAM','Guam','GUM',316,1671),(88,'GT','GUATEMALA','Guatemala','GTM',320,502),(89,'GN','GUINEA','Guinea','GIN',324,224),(90,'GW','GUINEA-BISSAU','Guinea-Bissau','GNB',624,245),(91,'GY','GUYANA','Guyana','GUY',328,592),(92,'HT','HAITI','Haiti','HTI',332,509),(93,'HM','HEARD ISLAND AND MCDONALD ISLANDS','Heard Island and Mcdonald Islands',NULL,NULL,0),(94,'VA','HOLY SEE (VATICAN CITY STATE)','Holy See (Vatican City State)','VAT',336,39),(95,'HN','HONDURAS','Honduras','HND',340,504),(96,'HK','HONG KONG','Hong Kong','HKG',344,852),(97,'HU','HUNGARY','Hungary','HUN',348,36),(98,'IS','ICELAND','Iceland','ISL',352,354),(99,'IN','INDIA','India','IND',356,91),(100,'ID','INDONESIA','Indonesia','IDN',360,62),(101,'IR','IRAN, ISLAMIC REPUBLIC OF','Iran, Islamic Republic of','IRN',364,98),(102,'IQ','IRAQ','Iraq','IRQ',368,964),(103,'IE','IRELAND','Ireland','IRL',372,353),(104,'IL','ISRAEL','Israel','ISR',376,972),(105,'IT','ITALY','Italy','ITA',380,39),(106,'JM','JAMAICA','Jamaica','JAM',388,1876),(107,'JP','JAPAN','Japan','JPN',392,81),(108,'JO','JORDAN','Jordan','JOR',400,962),(109,'KZ','KAZAKHSTAN','Kazakhstan','KAZ',398,7),(110,'KE','KENYA','Kenya','KEN',404,254),(111,'KI','KIRIBATI','Kiribati','KIR',296,686),(112,'KP','KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF','Korea, Democratic People\'s Republic of','PRK',408,850),(113,'KR','KOREA, REPUBLIC OF','Korea, Republic of','KOR',410,82),(114,'KW','KUWAIT','Kuwait','KWT',414,965),(115,'KG','KYRGYZSTAN','Kyrgyzstan','KGZ',417,996),(116,'LA','LAO PEOPLE\'S DEMOCRATIC REPUBLIC','Lao People\'s Democratic Republic','LAO',418,856),(117,'LV','LATVIA','Latvia','LVA',428,371),(118,'LB','LEBANON','Lebanon','LBN',422,961),(119,'LS','LESOTHO','Lesotho','LSO',426,266),(120,'LR','LIBERIA','Liberia','LBR',430,231),(121,'LY','LIBYAN ARAB JAMAHIRIYA','Libyan Arab Jamahiriya','LBY',434,218),(122,'LI','LIECHTENSTEIN','Liechtenstein','LIE',438,423),(123,'LT','LITHUANIA','Lithuania','LTU',440,370),(124,'LU','LUXEMBOURG','Luxembourg','LUX',442,352),(125,'MO','MACAO','Macao','MAC',446,853),(126,'MK','MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','Macedonia, the Former Yugoslav Republic of','MKD',807,389),(127,'MG','MADAGASCAR','Madagascar','MDG',450,261),(128,'MW','MALAWI','Malawi','MWI',454,265),(129,'MY','MALAYSIA','Malaysia','MYS',458,60),(130,'MV','MALDIVES','Maldives','MDV',462,960),(131,'ML','MALI','Mali','MLI',466,223),(132,'MT','MALTA','Malta','MLT',470,356),(133,'MH','MARSHALL ISLANDS','Marshall Islands','MHL',584,692),(134,'MQ','MARTINIQUE','Martinique','MTQ',474,596),(135,'MR','MAURITANIA','Mauritania','MRT',478,222),(136,'MU','MAURITIUS','Mauritius','MUS',480,230),(137,'YT','MAYOTTE','Mayotte',NULL,NULL,269),(138,'MX','MEXICO','Mexico','MEX',484,52),(139,'FM','MICRONESIA, FEDERATED STATES OF','Micronesia, Federated States of','FSM',583,691),(140,'MD','MOLDOVA, REPUBLIC OF','Moldova, Republic of','MDA',498,373),(141,'MC','MONACO','Monaco','MCO',492,377),(142,'MN','MONGOLIA','Mongolia','MNG',496,976),(143,'MS','MONTSERRAT','Montserrat','MSR',500,1664),(144,'MA','MOROCCO','Morocco','MAR',504,212),(145,'MZ','MOZAMBIQUE','Mozambique','MOZ',508,258),(146,'MM','MYANMAR','Myanmar','MMR',104,95),(147,'NA','NAMIBIA','Namibia','NAM',516,264),(148,'NR','NAURU','Nauru','NRU',520,674),(149,'NP','NEPAL','Nepal','NPL',524,977),(150,'NL','NETHERLANDS','Netherlands','NLD',528,31),(151,'AN','NETHERLANDS ANTILLES','Netherlands Antilles','ANT',530,599),(152,'NC','NEW CALEDONIA','New Caledonia','NCL',540,687),(153,'NZ','NEW ZEALAND','New Zealand','NZL',554,64),(154,'NI','NICARAGUA','Nicaragua','NIC',558,505),(155,'NE','NIGER','Niger','NER',562,227),(156,'NG','NIGERIA','Nigeria','NGA',566,234),(157,'NU','NIUE','Niue','NIU',570,683),(158,'NF','NORFOLK ISLAND','Norfolk Island','NFK',574,672),(159,'MP','NORTHERN MARIANA ISLANDS','Northern Mariana Islands','MNP',580,1670),(160,'NO','NORWAY','Norway','NOR',578,47),(161,'OM','OMAN','Oman','OMN',512,968),(162,'PK','PAKISTAN','Pakistan','PAK',586,92),(163,'PW','PALAU','Palau','PLW',585,680),(164,'PS','PALESTINIAN TERRITORY, OCCUPIED','Palestinian Territory, Occupied',NULL,NULL,970),(165,'PA','PANAMA','Panama','PAN',591,507),(166,'PG','PAPUA NEW GUINEA','Papua New Guinea','PNG',598,675),(167,'PY','PARAGUAY','Paraguay','PRY',600,595),(168,'PE','PERU','Peru','PER',604,51),(169,'PH','PHILIPPINES','Philippines','PHL',608,63),(170,'PN','PITCAIRN','Pitcairn','PCN',612,0),(171,'PL','POLAND','Poland','POL',616,48),(172,'PT','PORTUGAL','Portugal','PRT',620,351),(173,'PR','PUERTO RICO','Puerto Rico','PRI',630,1787),(174,'QA','QATAR','Qatar','QAT',634,974),(175,'RE','REUNION','Reunion','REU',638,262),(176,'RO','ROMANIA','Romania','ROM',642,40),(177,'RU','RUSSIAN FEDERATION','Russian Federation','RUS',643,70),(178,'RW','RWANDA','Rwanda','RWA',646,250),(179,'SH','SAINT HELENA','Saint Helena','SHN',654,290),(180,'KN','SAINT KITTS AND NEVIS','Saint Kitts and Nevis','KNA',659,1869),(181,'LC','SAINT LUCIA','Saint Lucia','LCA',662,1758),(182,'PM','SAINT PIERRE AND MIQUELON','Saint Pierre and Miquelon','SPM',666,508),(183,'VC','SAINT VINCENT AND THE GRENADINES','Saint Vincent and the Grenadines','VCT',670,1784),(184,'WS','SAMOA','Samoa','WSM',882,684),(185,'SM','SAN MARINO','San Marino','SMR',674,378),(186,'ST','SAO TOME AND PRINCIPE','Sao Tome and Principe','STP',678,239),(187,'SA','SAUDI ARABIA','Saudi Arabia','SAU',682,966),(188,'SN','SENEGAL','Senegal','SEN',686,221),(189,'CS','SERBIA AND MONTENEGRO','Serbia and Montenegro',NULL,NULL,381),(190,'SC','SEYCHELLES','Seychelles','SYC',690,248),(191,'SL','SIERRA LEONE','Sierra Leone','SLE',694,232),(192,'SG','SINGAPORE','Singapore','SGP',702,65),(193,'SK','SLOVAKIA','Slovakia','SVK',703,421),(194,'SI','SLOVENIA','Slovenia','SVN',705,386),(195,'SB','SOLOMON ISLANDS','Solomon Islands','SLB',90,677),(196,'SO','SOMALIA','Somalia','SOM',706,252),(197,'ZA','SOUTH AFRICA','South Africa','ZAF',710,27),(198,'GS','SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','South Georgia and the South Sandwich Islands',NULL,NULL,0),(199,'ES','SPAIN','Spain','ESP',724,34),(200,'LK','SRI LANKA','Sri Lanka','LKA',144,94),(201,'SD','SUDAN','Sudan','SDN',736,249),(202,'SR','SURINAME','Suriname','SUR',740,597),(203,'SJ','SVALBARD AND JAN MAYEN','Svalbard and Jan Mayen','SJM',744,47),(204,'SZ','SWAZILAND','Swaziland','SWZ',748,268),(205,'SE','SWEDEN','Sweden','SWE',752,46),(206,'CH','SWITZERLAND','Switzerland','CHE',756,41),(207,'SY','SYRIAN ARAB REPUBLIC','Syrian Arab Republic','SYR',760,963),(208,'TW','TAIWAN, PROVINCE OF CHINA','Taiwan, Province of China','TWN',158,886),(209,'TJ','TAJIKISTAN','Tajikistan','TJK',762,992),(210,'TZ','TANZANIA, UNITED REPUBLIC OF','Tanzania, United Republic of','TZA',834,255),(211,'TH','THAILAND','Thailand','THA',764,66),(212,'TL','TIMOR-LESTE','Timor-Leste',NULL,NULL,670),(213,'TG','TOGO','Togo','TGO',768,228),(214,'TK','TOKELAU','Tokelau','TKL',772,690),(215,'TO','TONGA','Tonga','TON',776,676),(216,'TT','TRINIDAD AND TOBAGO','Trinidad and Tobago','TTO',780,1868),(217,'TN','TUNISIA','Tunisia','TUN',788,216),(218,'TR','TURKEY','Turkey','TUR',792,90),(219,'TM','TURKMENISTAN','Turkmenistan','TKM',795,7370),(220,'TC','TURKS AND CAICOS ISLANDS','Turks and Caicos Islands','TCA',796,1649),(221,'TV','TUVALU','Tuvalu','TUV',798,688),(222,'UG','UGANDA','Uganda','UGA',800,256),(223,'UA','UKRAINE','Ukraine','UKR',804,380),(224,'AE','UNITED ARAB EMIRATES','United Arab Emirates','ARE',784,971),(225,'GB','UNITED KINGDOM','United Kingdom','GBR',826,44),(226,'US','UNITED STATES','United States','USA',840,1),(227,'UM','UNITED STATES MINOR OUTLYING ISLANDS','United States Minor Outlying Islands',NULL,NULL,1),(228,'UY','URUGUAY','Uruguay','URY',858,598),(229,'UZ','UZBEKISTAN','Uzbekistan','UZB',860,998),(230,'VU','VANUATU','Vanuatu','VUT',548,678),(231,'VE','VENEZUELA','Venezuela','VEN',862,58),(232,'VN','VIET NAM','Viet Nam','VNM',704,84),(233,'VG','VIRGIN ISLANDS, BRITISH','Virgin Islands, British','VGB',92,1284),(234,'VI','VIRGIN ISLANDS, U.S.','Virgin Islands, U.s.','VIR',850,1340),(235,'WF','WALLIS AND FUTUNA','Wallis and Futuna','WLF',876,681),(236,'EH','WESTERN SAHARA','Western Sahara','ESH',732,212),(237,'YE','YEMEN','Yemen','YEM',887,967),(238,'ZM','ZAMBIA','Zambia','ZMB',894,260),(239,'ZW','ZIMBABWE','Zimbabwe','ZWE',716,263);

/*Table structure for table `tbl_coupons` */

DROP TABLE IF EXISTS `tbl_coupons`;

CREATE TABLE `tbl_coupons` (
  `coup_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_code` varchar(50) DEFAULT NULL,
  `coupon_date` datetime DEFAULT NULL,
  `coupon_start_date` datetime DEFAULT NULL,
  `coupon_end_date` datetime DEFAULT NULL,
  `coupon_prod_id` int(11) DEFAULT NULL,
  `coupon_cat_id` int(11) DEFAULT NULL,
  `coupon_type` tinyint(1) DEFAULT '0' COMMENT '0 for price, 1 for %, 2 for free shipping',
  `coupon_discount` float DEFAULT NULL,
  `coupon_status` tinyint(1) DEFAULT '0' COMMENT '0 for Inactive, 1 for active',
  `coupon_num_uses` int(11) DEFAULT NULL,
  `coupon_max_uses` int(11) DEFAULT NULL,
  PRIMARY KEY (`coup_id`),
  KEY `coupon_prod_id` (`coupon_prod_id`),
  KEY `coupon_cat_id` (`coupon_cat_id`),
  CONSTRAINT `tbl_coupons_ibfk_1` FOREIGN KEY (`coupon_prod_id`) REFERENCES `tbl_products` (`prod_id`),
  CONSTRAINT `tbl_coupons_ibfk_2` FOREIGN KEY (`coupon_cat_id`) REFERENCES `tbl_categories` (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_coupons` */

/*Table structure for table `tbl_email_temp` */

DROP TABLE IF EXISTS `tbl_email_temp`;

CREATE TABLE `tbl_email_temp` (
  `e_email_id` int(11) NOT NULL AUTO_INCREMENT,
  `e_email_slug` varchar(222) NOT NULL,
  `e_email_title` varchar(255) DEFAULT NULL,
  `e_email_subject` varchar(255) DEFAULT NULL,
  `e_email_text` text,
  `date_created` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  KEY `e_email_id` (`e_email_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_email_temp` */

insert  into `tbl_email_temp`(`e_email_id`,`e_email_slug`,`e_email_title`,`e_email_subject`,`e_email_text`,`date_created`,`is_active`) values (1,'billing','Billing','Billing','You have been Referred Billing','2015-10-13 12:07:41',1),(2,'profile-flags','Profile Flags','Profile Flags','<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\r\n<title>Thank you for Contacting Us‏</title>\r\n<link rel=\"shortcut icon\" href=\"images/favicon.ico\" type=\"image/x-icon\">\r\n<link rel=\"icon\" href=\"images/favicon.ico\" type=\"image/x-icon\">\r\n<link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300\" rel=\"stylesheet\" type=\"text/css\">\r\n\r\n<style>*{padding:0px;} .hdhov{color:#808080;} .hdhov:hover{color:#808080;}</style>Profile Flags','2015-10-13 12:07:40',1),(3,'product-flags','Product Flags','Product Flags','<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\r\n<title>Thank you for Contacting Us‏</title>\r\n<link rel=\"shortcut icon\" href=\"images/favicon.ico\" type=\"image/x-icon\">\r\n<link rel=\"icon\" href=\"images/favicon.ico\" type=\"image/x-icon\">\r\n<link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300\" rel=\"stylesheet\" type=\"text/css\">\r\n\r\n<style>*{padding:0px;} .hdhov{color:#808080;} .hdhov:hover{color:#808080;}</style>Product Flags','2015-10-13 12:07:39',1),(5,'updates','Updates','Updates','<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\r\n<title>Thank you for Contacting Us‏</title>\r\n<link rel=\"shortcut icon\" href=\"images/favicon.ico\" type=\"image/x-icon\">\r\n<link rel=\"icon\" href=\"images/favicon.ico\" type=\"image/x-icon\">\r\n<link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300\" rel=\"stylesheet\" type=\"text/css\">\r\n\r\n<style>*{padding:0px;} .hdhov{color:#808080;} .hdhov:hover{color:#808080;}</style>Updates','2015-10-13 12:07:37',1),(6,'policy-changes','Policy Changes','Policy Changes','<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\r\n<title>Thank You for Registering</title>\r\n<link rel=\"shortcut icon\" href=\"images/favicon.ico\" type=\"image/x-icon\">\r\n<link rel=\"icon\" href=\"images/favicon.ico\" type=\"image/x-icon\">\r\n<link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300\" rel=\"stylesheet\" type=\"text/css\">\r\n\r\n<style>*{padding:0px;} .hdhov{color:#808080;} .hdhov:hover{color:#808080;}</style>Policy Changes\r\n\r\n','2015-10-13 12:07:36',1),(8,'password-reset','Password Reset','Password Reset','<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\r\n<title>Password Retrieval‏</title>\r\n<link rel=\"shortcut icon\" href=\"images/favicon.ico\" type=\"image/x-icon\">\r\n<link rel=\"icon\" href=\"images/favicon.ico\" type=\"image/x-icon\">\r\n<link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300\" rel=\"stylesheet\" type=\"text/css\">\r\n\r\n<style>*{padding:0px;} .hdhov{color:#808080;} .hdhov:hover{color:#808080;}</style>Password Reset\r\n','2015-10-13 12:07:34',1),(4,'refunds','Refunds','Refunds','<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\r\n<title>Thank you for Contacting Us‏</title>\r\n<link rel=\"shortcut icon\" href=\"images/favicon.ico\" type=\"image/x-icon\">\r\n<link rel=\"icon\" href=\"images/favicon.ico\" type=\"image/x-icon\">\r\n<link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300\" rel=\"stylesheet\" type=\"text/css\">\r\n\r\n<style>*{padding:0px;} .hdhov{color:#808080;} .hdhov:hover{color:#808080;}</style>Refunds','2015-10-13 12:07:38',1),(7,'promotions','Promotions','Promotions','<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\r\n<title>Thank You for Registering</title>\r\n<link rel=\"shortcut icon\" href=\"images/favicon.ico\" type=\"image/x-icon\">\r\n<link rel=\"icon\" href=\"images/favicon.ico\" type=\"image/x-icon\">\r\n<link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300\" rel=\"stylesheet\" type=\"text/css\">\r\n\r\n<style>*{padding:0px;} .hdhov{color:#808080;} .hdhov:hover{color:#808080;}</style>Promotions\r\n\r\n','2015-10-13 12:07:35',1);

/*Table structure for table `tbl_homepage` */

DROP TABLE IF EXISTS `tbl_homepage`;

CREATE TABLE `tbl_homepage` (
  `home_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_title` varchar(200) DEFAULT NULL,
  `feaSeller` varchar(200) DEFAULT NULL,
  `searchtitle` varchar(200) DEFAULT NULL,
  `searchtext` text,
  `searchButton` varchar(200) DEFAULT NULL,
  `gettingStartTitle` varchar(200) DEFAULT NULL,
  `startTitle1` varchar(200) DEFAULT NULL,
  `startTitle2` varchar(200) DEFAULT NULL,
  `startTitle3` varchar(200) DEFAULT NULL,
  `membertitle` varchar(200) DEFAULT NULL,
  `start_image1` varchar(100) DEFAULT NULL,
  `start_image2` varchar(100) DEFAULT NULL,
  `start_image3` varchar(100) DEFAULT NULL,
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_keywords` varchar(200) DEFAULT NULL,
  `meta_description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`home_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_homepage` */

insert  into `tbl_homepage`(`home_id`,`category_title`,`feaSeller`,`searchtitle`,`searchtext`,`searchButton`,`gettingStartTitle`,`startTitle1`,`startTitle2`,`startTitle3`,`membertitle`,`start_image1`,`start_image2`,`start_image3`,`meta_title`,`meta_keywords`,`meta_description`) values (1,'Shop By Categories','Featured Sellers','Search Items in your Area','<div style=\"text-align: center;\">Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here</div>\r\n<div style=\"text-align: center;\">City, State, Zip</div>','Search','Getting Started is as Easy as 1-2-3.','<h5 style=\"text-align: center;\">Add Listing</h5>\r\n<p style=\"text-align: center;\">Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>','test','test','Save an Average of 40% Buying from Members Like You','img_3251509346689.png','img_4701509347184.png','img_7251509347184.png','Meta Title','meta keyword','meta Description');

/*Table structure for table `tbl_homepage_banner` */

DROP TABLE IF EXISTS `tbl_homepage_banner`;

CREATE TABLE `tbl_homepage_banner` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_image` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_homepage_banner` */

/*Table structure for table `tbl_homepage_member` */

DROP TABLE IF EXISTS `tbl_homepage_member`;

CREATE TABLE `tbl_homepage_member` (
  `mem_id` int(11) NOT NULL AUTO_INCREMENT,
  `mem_image` varchar(200) DEFAULT NULL,
  `mem_description` text,
  PRIMARY KEY (`mem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_homepage_member` */

insert  into `tbl_homepage_member`(`mem_id`,`mem_image`,`mem_description`) values (1,'member_7781509427635.png','Make extra cash for your kids items taking up space');

/*Table structure for table `tbl_how_work_page` */

DROP TABLE IF EXISTS `tbl_how_work_page`;

CREATE TABLE `tbl_how_work_page` (
  `work_id` int(50) NOT NULL AUTO_INCREMENT,
  `work_title` varchar(100) DEFAULT NULL,
  `work_page_title` varchar(100) DEFAULT NULL,
  `work_desc_1` longtext,
  `work_image_1` varchar(100) DEFAULT NULL,
  `work_desc_2` longtext,
  `work_image_2` varchar(100) DEFAULT NULL,
  `work_desc_3` longtext,
  `work_image_3` varchar(100) DEFAULT NULL,
  `work_desc_4` longtext,
  `work_image_4` varchar(100) DEFAULT NULL,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_desc` longtext,
  `meta_keywords` varchar(200) DEFAULT NULL,
  `work_banner` varchar(100) DEFAULT NULL,
  KEY `work_id` (`work_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_how_work_page` */

insert  into `tbl_how_work_page`(`work_id`,`work_title`,`work_page_title`,`work_desc_1`,`work_image_1`,`work_desc_2`,`work_image_2`,`work_desc_3`,`work_image_3`,`work_desc_4`,`work_image_4`,`meta_title`,`meta_desc`,`meta_keywords`,`work_banner`) values (1,'How Does eDucki Work?','What is eDucki','<div class=\"cvr-upr\">\r\n<h5>Create a Covershot</h5>\r\n<a href=\"../../../what-is-educki\">With just a few clicks</a></div>\r\n<div class=\"cvr-bttom\">\r\n<p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>\r\n</div>','img_9721509425627.jpg','<div class=\"cvr-upr\">\r\n<h5>Introducing eDucki Protect</h5>\r\n<a href=\"../../../what-is-educki\">Buy with confidence</a></div>\r\n<div class=\"cvr-bttom\">\r\n<p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>\r\n</div>','img_191509425299.jpg','<div class=\"cvr-upr\">\r\n<h5>Enjoy One-Stop Shopping</h5>\r\n<a href=\"../../../what-is-educki\">New and pre-loved fashion</a></div>\r\n<div class=\"cvr-bttom\">\r\n<p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample</p>\r\n</div>','img_4781509425299.jpg','<div class=\"cvr-upr\">\r\n<h5>Discover the eDucki Community</h5>\r\n<a href=\"https://www.facebook.com/\" target=\"_blank\">Connect and share</a></div>\r\n<div class=\"cvr-bttom\">\r\n<p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample</p>\r\n</div>','img_9171509430305.jpg','meta title','meta description','meta keywords','img_2721509436348.jpg');

/*Table structure for table `tbl_inbox` */

DROP TABLE IF EXISTS `tbl_inbox`;

CREATE TABLE `tbl_inbox` (
  `inbox_id` int(11) NOT NULL AUTO_INCREMENT,
  `inbox_subject` text,
  `inbox_message` text,
  `inbox_to` text,
  `inbox_from` text,
  `inbox_date` datetime DEFAULT NULL,
  `inbox_delete` tinyint(1) DEFAULT '0',
  `inbox_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`inbox_id`),
  KEY `inbox_user_id` (`inbox_user_id`),
  CONSTRAINT `tbl_inbox_ibfk_1` FOREIGN KEY (`inbox_user_id`) REFERENCES `tbl_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_inbox` */

/*Table structure for table `tbl_inbox_attach` */

DROP TABLE IF EXISTS `tbl_inbox_attach`;

CREATE TABLE `tbl_inbox_attach` (
  `in_att_id` int(11) NOT NULL AUTO_INCREMENT,
  `in_att_name` varchar(250) DEFAULT NULL,
  `in_att_inbox_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`in_att_id`),
  KEY `in_att_inbox_id` (`in_att_inbox_id`),
  CONSTRAINT `tbl_inbox_attach_ibfk_1` FOREIGN KEY (`in_att_inbox_id`) REFERENCES `tbl_inbox` (`inbox_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_inbox_attach` */

/*Table structure for table `tbl_job_applies` */

DROP TABLE IF EXISTS `tbl_job_applies`;

CREATE TABLE `tbl_job_applies` (
  `apply_id` int(11) NOT NULL AUTO_INCREMENT,
  `apply_job_id` int(11) DEFAULT NULL,
  `apply_fname` varchar(150) DEFAULT NULL,
  `apply_lname` varchar(150) DEFAULT NULL,
  `apply_email` varchar(150) DEFAULT NULL,
  `apply_cv` text,
  `apply_cover` text,
  `apply_linkedin` text,
  `apply_website` text,
  `apply_here_about` text,
  `apply_date` datetime DEFAULT NULL,
  PRIMARY KEY (`apply_id`),
  KEY `apply_job_id` (`apply_job_id`),
  CONSTRAINT `tbl_job_applies_ibfk_1` FOREIGN KEY (`apply_job_id`) REFERENCES `tbl_jobs` (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_job_applies` */

/*Table structure for table `tbl_jobs` */

DROP TABLE IF EXISTS `tbl_jobs`;

CREATE TABLE `tbl_jobs` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_title` varchar(200) DEFAULT NULL,
  `job_description` text,
  `job_status` tinyint(1) DEFAULT '0',
  `job_start` datetime DEFAULT NULL,
  `job_expire` datetime DEFAULT NULL,
  `job_date` datetime DEFAULT NULL,
  `job_admin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`job_id`),
  KEY `job_admin_id` (`job_admin_id`),
  CONSTRAINT `tbl_jobs_ibfk_1` FOREIGN KEY (`job_admin_id`) REFERENCES `tbl_admin` (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_jobs` */

/*Table structure for table `tbl_message` */

DROP TABLE IF EXISTS `tbl_message`;

CREATE TABLE `tbl_message` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `message_token` varchar(150) DEFAULT NULL,
  `message_token_date` datetime DEFAULT NULL,
  `message_sender_id` int(11) DEFAULT NULL,
  `message_receiver_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`message_id`),
  KEY `message_sender_id` (`message_sender_id`),
  KEY `message_receiver_id` (`message_receiver_id`),
  CONSTRAINT `tbl_message_ibfk_1` FOREIGN KEY (`message_sender_id`) REFERENCES `tbl_user` (`user_id`),
  CONSTRAINT `tbl_message_ibfk_2` FOREIGN KEY (`message_receiver_id`) REFERENCES `tbl_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_message` */

/*Table structure for table `tbl_message_detail` */

DROP TABLE IF EXISTS `tbl_message_detail`;

CREATE TABLE `tbl_message_detail` (
  `msg_detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `msg_detail_msg_id` int(11) DEFAULT NULL,
  `msg_detail` text,
  `msg_detail_date` datetime DEFAULT NULL,
  `is_sender_read` tinyint(4) DEFAULT '0',
  `is_receiver_read` tinyint(4) DEFAULT '0',
  `msg_detail_sender` tinyint(4) DEFAULT '0' COMMENT '1 for sender and 2 for reciver',
  PRIMARY KEY (`msg_detail_id`),
  KEY `msg_detail_msg_id` (`msg_detail_msg_id`),
  CONSTRAINT `tbl_message_detail_ibfk_4` FOREIGN KEY (`msg_detail_msg_id`) REFERENCES `tbl_message` (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_message_detail` */

/*Table structure for table `tbl_newsletter` */

DROP TABLE IF EXISTS `tbl_newsletter`;

CREATE TABLE `tbl_newsletter` (
  `nl_id` int(11) NOT NULL,
  `nl_from` varchar(255) DEFAULT NULL,
  `nl_subject` varchar(255) DEFAULT NULL,
  `nl_text` text,
  `date_created` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_delete` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_newsletter` */

insert  into `tbl_newsletter`(`nl_id`,`nl_from`,`nl_subject`,`nl_text`,`date_created`,`is_active`,`is_delete`) values (2,NULL,NULL,NULL,NULL,NULL,NULL),(3,NULL,NULL,NULL,NULL,NULL,NULL),(4,'asdf','asdfas','<p>dfasfas<br></p>','2015-03-07 00:05:50',1,0),(5,'tdydrty','ern y ty','<p>dcfg  sdf sdg<br></p>','2015-03-31 22:49:38',1,0);

/*Table structure for table `tbl_order_address` */

DROP TABLE IF EXISTS `tbl_order_address`;

CREATE TABLE `tbl_order_address` (
  `order_add_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_product_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `order_bill_address` text,
  `order_ship_address` text,
  PRIMARY KEY (`order_add_id`),
  KEY `order_product_id` (`order_product_id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `tbl_order_address_ibfk_1` FOREIGN KEY (`order_product_id`) REFERENCES `tbl_order_products` (`order_products_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_order_address` */

/*Table structure for table `tbl_order_products` */

DROP TABLE IF EXISTS `tbl_order_products`;

CREATE TABLE `tbl_order_products` (
  `order_products_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `order_prod_id` int(11) DEFAULT NULL,
  `order_prod_name` varchar(250) DEFAULT NULL,
  `order_prod_qty` int(11) DEFAULT NULL,
  `order_prod_price` float DEFAULT NULL,
  PRIMARY KEY (`order_products_id`),
  KEY `order_id` (`order_id`),
  CONSTRAINT `tbl_order_products_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `tbl_orders` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_order_products` */

/*Table structure for table `tbl_orders` */

DROP TABLE IF EXISTS `tbl_orders`;

CREATE TABLE `tbl_orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `buyer_name` varchar(150) DEFAULT NULL,
  `seller_name` varchar(150) DEFAULT NULL,
  `sale_amount` float DEFAULT NULL,
  `shipping_type` varchar(150) DEFAULT NULL,
  `traking_number` varchar(100) DEFAULT NULL,
  `saller_store_name` varchar(150) DEFAULT NULL,
  `order_status` varchar(100) DEFAULT NULL,
  `order_date` datetime DEFAULT NULL,
  `ship_date` datetime DEFAULT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `buyer_id` int(11) DEFAULT NULL,
  `order_sub_total` float DEFAULT NULL,
  `order_grand_total` float DEFAULT NULL,
  `order_tax` float DEFAULT NULL,
  `order_shipping_price` float DEFAULT NULL,
  `order_discount` float DEFAULT NULL,
  `order_coupon_id` int(11) DEFAULT NULL,
  `order_coupon` varchar(100) DEFAULT NULL,
  `order_coupon_discount` float DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `buyer_id` (`buyer_id`),
  CONSTRAINT `tbl_orders_ibfk_1` FOREIGN KEY (`buyer_id`) REFERENCES `tbl_user` (`user_id`),
  CONSTRAINT `tbl_orders_ibfk_2` FOREIGN KEY (`buyer_id`) REFERENCES `tbl_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_orders` */

/*Table structure for table `tbl_pages` */

DROP TABLE IF EXISTS `tbl_pages`;

CREATE TABLE `tbl_pages` (
  `pg_id` int(11) NOT NULL AUTO_INCREMENT,
  `pg_title` varchar(255) NOT NULL,
  `pg_url` varchar(255) NOT NULL,
  `pg_description` longtext NOT NULL,
  `pg_image` varchar(150) DEFAULT NULL,
  `pg_status` tinyint(1) NOT NULL DEFAULT '0',
  `pg_is_listing` tinyint(4) DEFAULT '0',
  `pg_is_pages` tinyint(4) DEFAULT '0',
  `pg_is_editable` tinyint(4) DEFAULT '1',
  `pg_is_deleteable` tinyint(4) DEFAULT '0',
  `pg_admin_id` int(11) DEFAULT '1',
  `is_delete` tinyint(1) DEFAULT '0',
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_keywords` varchar(200) DEFAULT NULL,
  `meta_description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`pg_id`),
  KEY `pg_admin_id` (`pg_admin_id`),
  CONSTRAINT `tbl_pages_ibfk_1` FOREIGN KEY (`pg_admin_id`) REFERENCES `tbl_admin` (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_pages` */

insert  into `tbl_pages`(`pg_id`,`pg_title`,`pg_url`,`pg_description`,`pg_image`,`pg_status`,`pg_is_listing`,`pg_is_pages`,`pg_is_editable`,`pg_is_deleteable`,`pg_admin_id`,`is_delete`,`meta_title`,`meta_keywords`,`meta_description`) values (1,'Homepage','home-page','Sample text here.',NULL,1,1,0,1,0,1,0,'sample meta title here','sample meta keyword here','sample'),(3,'test page','test-ccncn','<h1>WHAT EDUCKI IS FOR</h1>\r\n<h3>Fashion and Accessories</h3>\r\n<br />\r\n<p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>\r\n<h3>Be Friendly and Kind</h3>\r\n<p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>\r\n<h3>Be Friendly and Kind</h3>\r\n<p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>\r\n<h3>Be Yourself, Be Real</h3>\r\n<p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>\r\n<h3>Talk to Us</h3>\r\n<p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>\r\n<p>Sample Text Here Sample:&nbsp;Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>\r\n<p>Sample Text Here Sample:&nbsp;Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>\r\n<p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>',NULL,1,0,0,1,1,1,0,'','',''),(4,'About Us','about-us','about us',NULL,1,1,0,1,1,1,0,'','',''),(6,'test','test','est',NULL,1,1,0,1,1,1,0,'','',''),(7,'new about page','new-about-page','This is a new about page&nbsp;This is a new about page&nbsp;This is a new about page&nbsp;This is a new about page&nbsp;This is a new about page&nbsp;This is a new about page&nbsp;This is a new about page&nbsp;This is a new about page&nbsp;This is a new about page&nbsp;This is a new about page&nbsp;This is a new about page&nbsp;This is a new about page&nbsp;This is a new about page&nbsp;This is a new about page&nbsp;This is a new about page&nbsp;This is a new about page&nbsp;This is a new about page&nbsp;',NULL,1,1,0,1,1,1,0,'new about page','new about keyword','new description');

/*Table structure for table `tbl_pages_listing` */

DROP TABLE IF EXISTS `tbl_pages_listing`;

CREATE TABLE `tbl_pages_listing` (
  `listing_id` int(11) NOT NULL AUTO_INCREMENT,
  `pg_listing_id` int(11) DEFAULT NULL,
  `lisintg_image` varchar(100) DEFAULT NULL,
  `listing_description` longtext,
  `listing_url` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`listing_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_pages_listing` */

/*Table structure for table `tbl_pages_sub` */

DROP TABLE IF EXISTS `tbl_pages_sub`;

CREATE TABLE `tbl_pages_sub` (
  `sub_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_pg_id` int(11) DEFAULT NULL,
  `sub_pg_title` varchar(255) NOT NULL,
  `sub_pg_url` varchar(255) NOT NULL,
  `sub_pg_description` longtext NOT NULL,
  `sub_pg_image` varchar(150) DEFAULT NULL,
  `sub_pg_is_listing` tinyint(1) DEFAULT '0',
  `sub_pg_is_pages` tinyint(4) DEFAULT '0',
  `sub_pg_status` tinyint(1) NOT NULL DEFAULT '0',
  `sub_pg_admin_id` int(11) DEFAULT '1',
  `sub_is_delete` tinyint(1) DEFAULT '0',
  `sub_is_deletable` tinyint(4) DEFAULT '0',
  `sub_is_editable` tinyint(4) DEFAULT '0',
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_keywords` varchar(200) DEFAULT NULL,
  `meta_description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`sub_id`),
  KEY `pg_admin_id` (`sub_pg_admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_pages_sub` */

insert  into `tbl_pages_sub`(`sub_id`,`sub_pg_id`,`sub_pg_title`,`sub_pg_url`,`sub_pg_description`,`sub_pg_image`,`sub_pg_is_listing`,`sub_pg_is_pages`,`sub_pg_status`,`sub_pg_admin_id`,`sub_is_delete`,`sub_is_deletable`,`sub_is_editable`,`meta_title`,`meta_keywords`,`meta_description`) values (1,1,'Sub page sample title','sub-page-sample-title','Sample text here','sub_pg_img_10_02_2017_1506929390.png',1,0,1,1,0,0,0,'sample meta title here','sample meta keyword here','sample meta description'),(5,4,'How Does eDucki Work?','what-is-educki','How Does eDucki Work?',NULL,0,0,1,1,0,0,0,'','',''),(6,4,'FAQ\'S','faq','These are the description of the faqs&nbsp;These are the description of the faqs&nbsp;These are the description of the faqs&nbsp;These are the description of the faqs&nbsp;These are the description of the faqs&nbsp;These are the description of the faqs',NULL,1,0,1,1,0,0,0,'faqs','faqs questions answers','this is a meta description of the faqs'),(7,4,'test','tes','csd',NULL,1,0,1,1,0,0,0,'','',''),(8,6,'test6','test6','test',NULL,1,0,1,1,0,0,0,'','',''),(9,4,'Community Guidelines','community-guidelines','<h1>WHAT EDUCKI IS FOR</h1>\r\n<h3>Fashion and Accessories</h3>\r\n<br />\r\n<p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>\r\n<h3>Be Friendly and Kind</h3>\r\n<p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>\r\n<h3>Be Friendly and Kind</h3>\r\n<p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>\r\n<h3>Be Yourself, Be Real</h3>\r\n<p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>\r\n<h3>Talk to Us</h3>\r\n<p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>\r\n<p>Sample Text Here Sample:&nbsp;Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>\r\n<p>Sample Text Here Sample:&nbsp;Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</p>\r\n<div style=\"text-align: justify;\">Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.<br /><br />Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.</div>','sub_pg_img_10_31_2017_1509435214.jpg',0,0,1,1,0,0,0,'meta title','meta keyword','meta description'),(10,4,'Guide to eDucki','guide-to-educki','Guide to eDucki',NULL,1,0,1,1,0,0,0,'','','');

/*Table structure for table `tbl_pages_sub_sub` */

DROP TABLE IF EXISTS `tbl_pages_sub_sub`;

CREATE TABLE `tbl_pages_sub_sub` (
  `sub_sub_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_pg_id` int(11) DEFAULT NULL,
  `sub_sub_pg_title` varchar(255) NOT NULL,
  `sub_sub_pg_url` varchar(255) NOT NULL,
  `sub_sub_pg_description` longtext NOT NULL,
  `sub_sub_pg_image` varchar(150) DEFAULT NULL,
  `sub_sub_pg_status` tinyint(1) NOT NULL DEFAULT '0',
  `sub_sub_is_listing` tinyint(4) DEFAULT '0',
  `sub_sub_is_pages` tinyint(4) DEFAULT '0',
  `sub_sub_is_deletable` tinyint(4) DEFAULT '0',
  `sub_sub_pg_admin_id` int(11) DEFAULT '1',
  `sub_sub_is_delete` tinyint(1) DEFAULT '0',
  `sub_sub_is_editable` tinyint(4) DEFAULT '0',
  `meta_title` varchar(200) DEFAULT NULL,
  `meta_keywords` varchar(200) DEFAULT NULL,
  `meta_description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`sub_sub_id`),
  KEY `pg_admin_id` (`sub_sub_pg_admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_pages_sub_sub` */

insert  into `tbl_pages_sub_sub`(`sub_sub_id`,`sub_pg_id`,`sub_sub_pg_title`,`sub_sub_pg_url`,`sub_sub_pg_description`,`sub_sub_pg_image`,`sub_sub_pg_status`,`sub_sub_is_listing`,`sub_sub_is_pages`,`sub_sub_is_deletable`,`sub_sub_pg_admin_id`,`sub_sub_is_delete`,`sub_sub_is_editable`,`meta_title`,`meta_keywords`,`meta_description`) values (1,1,'Sub page sample title','sub-page-sample-title','Sample text here','sub_pg_img_10_02_2017_1506929390.png',1,0,0,0,1,0,0,'sample meta title here','sample meta keyword here','sample meta description'),(2,1,'Sub Sub page title 1','sub-sub-page-title-1','Sample text here. Sample text here. edit 1','sub_pg_img_10_03_2017_1507015071.png',1,0,0,0,1,0,0,'sub sub pages meta title','sub sub meta keywords','sub sub meta description'),(3,6,'How do i start on educki?','question1','THis is the main process of the educki and in you this you will see how to get started the awsome experience of life',NULL,1,0,0,0,1,0,0,'educki started','started kids shopping educki','This is a educki description of the how should a user start educki'),(4,6,'How Does eDucki Work?','question','This is the description&nbsp;sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.sample text here.',NULL,1,0,0,0,1,0,0,'','',''),(5,6,'What is eDucki?','educki-question','This is the basic description of the educki&nbsp;This is the basic description of the educki&nbsp;This is the basic description of the educki&nbsp;This is the basic description of the educki&nbsp;This is the basic description of the educki&nbsp;This is the basic description of the educki&nbsp;This is the basic description of the educki&nbsp;This is the basic description of the educki&nbsp;This is the basic description of the educki&nbsp;This is the basic description of the educki&nbsp;This is the basic description of the educki',NULL,1,0,0,0,1,0,0,'what is Educki','educki kids shopping','educki is a meta description of the');

/*Table structure for table `tbl_product_category` */

DROP TABLE IF EXISTS `tbl_product_category`;

CREATE TABLE `tbl_product_category` (
  `prod_cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_cat_navigation_id` int(11) NOT NULL,
  `prod_cat_category_id` int(11) NOT NULL,
  `prod_cat_subcategory_id` int(11) DEFAULT NULL,
  `prod_cat_prod_id` int(11) NOT NULL,
  PRIMARY KEY (`prod_cat_id`),
  KEY `prod_cat_prod_id` (`prod_cat_prod_id`),
  CONSTRAINT `tbl_product_category_ibfk_1` FOREIGN KEY (`prod_cat_prod_id`) REFERENCES `tbl_products` (`prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=255 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_product_category` */

insert  into `tbl_product_category`(`prod_cat_id`,`prod_cat_navigation_id`,`prod_cat_category_id`,`prod_cat_subcategory_id`,`prod_cat_prod_id`) values (4,1,11,NULL,2),(17,2,11,NULL,4),(19,2,11,NULL,6),(40,2,19,NULL,12),(69,2,11,NULL,1),(70,2,11,NULL,7),(90,2,11,NULL,25),(109,2,11,NULL,19),(183,2,11,NULL,42),(184,1,46,NULL,42),(185,2,18,NULL,41),(186,1,46,NULL,41),(187,2,11,NULL,40),(188,1,46,NULL,40),(189,2,11,NULL,39),(190,1,46,NULL,39),(191,1,47,NULL,39),(192,2,19,NULL,38),(193,1,46,NULL,38),(194,1,49,NULL,38),(195,2,11,NULL,37),(196,1,46,NULL,37),(197,1,48,NULL,37),(198,1,49,NULL,37),(199,1,47,NULL,37),(200,2,11,NULL,36),(201,1,46,NULL,36),(202,1,52,NULL,36),(203,2,11,NULL,35),(204,1,46,NULL,35),(205,2,11,NULL,34),(206,1,46,NULL,34),(207,1,48,NULL,34),(208,2,11,NULL,33),(209,1,46,NULL,33),(210,1,49,NULL,32),(211,1,52,NULL,32),(212,1,46,NULL,32),(213,2,11,NULL,30),(214,1,46,NULL,30),(215,1,49,NULL,30),(216,2,11,NULL,31),(217,1,46,NULL,31),(218,1,47,NULL,31),(219,2,11,NULL,29),(220,1,46,NULL,29),(221,1,48,NULL,29),(222,2,11,NULL,28),(223,1,46,NULL,28),(224,2,11,NULL,27),(225,1,46,NULL,27),(226,2,11,NULL,26),(227,1,46,NULL,26),(228,2,11,NULL,24),(229,1,46,NULL,24),(230,2,11,NULL,23),(231,1,46,NULL,23),(232,2,11,NULL,22),(233,1,46,NULL,22),(234,2,18,NULL,21),(235,1,47,NULL,21),(236,2,11,NULL,18),(237,1,47,NULL,18),(238,2,11,NULL,17),(239,1,47,NULL,17),(240,1,46,NULL,16),(241,1,47,NULL,16),(242,2,11,NULL,15),(243,1,48,NULL,15),(244,2,11,NULL,8),(245,1,46,NULL,8),(246,1,47,NULL,8),(247,2,11,NULL,10),(248,1,47,NULL,10),(249,2,11,NULL,14),(250,1,47,NULL,14),(251,1,46,NULL,14),(252,1,48,NULL,14),(253,1,49,NULL,14),(254,1,52,NULL,14);

/*Table structure for table `tbl_product_color` */

DROP TABLE IF EXISTS `tbl_product_color`;

CREATE TABLE `tbl_product_color` (
  `color_id` int(11) NOT NULL AUTO_INCREMENT,
  `color_name` varchar(200) DEFAULT NULL,
  `color_prod_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`color_id`),
  KEY `color_prod_id` (`color_prod_id`),
  CONSTRAINT `tbl_product_color_ibfk_1` FOREIGN KEY (`color_prod_id`) REFERENCES `tbl_products` (`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_product_color` */

/*Table structure for table `tbl_product_comments` */

DROP TABLE IF EXISTS `tbl_product_comments`;

CREATE TABLE `tbl_product_comments` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `review_prod_id` int(11) NOT NULL,
  `review_user_id` int(11) NOT NULL,
  `review_user_name` text,
  `review_comment` text CHARACTER SET utf8 NOT NULL,
  `review_rating` tinyint(4) NOT NULL,
  `review_date_time` datetime NOT NULL,
  `review_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 -> Pending, 1 -> Approved, 2 -> Disapproved',
  PRIMARY KEY (`review_id`),
  KEY `review_prod_id` (`review_prod_id`),
  CONSTRAINT `tbl_product_comments_ibfk_1` FOREIGN KEY (`review_prod_id`) REFERENCES `tbl_products` (`prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_product_comments` */

insert  into `tbl_product_comments`(`review_id`,`review_prod_id`,`review_user_id`,`review_user_name`,`review_comment`,`review_rating`,`review_date_time`,`review_status`) values (1,1,1,'teeest test test','test',4,'2017-01-03 00:00:00',1);

/*Table structure for table `tbl_product_filters` */

DROP TABLE IF EXISTS `tbl_product_filters`;

CREATE TABLE `tbl_product_filters` (
  `prod_filter_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_filter_filter_id` int(11) DEFAULT NULL,
  `prod_filter_prod_id` int(11) DEFAULT NULL,
  `prid_filter_cat_id` int(11) DEFAULT NULL,
  `prod_filter_name` varchar(250) DEFAULT NULL,
  `prod_filter_value` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`prod_filter_id`),
  KEY `filter_prod_id` (`prod_filter_prod_id`),
  KEY `filter_cat_id` (`prid_filter_cat_id`),
  KEY `prod_filter_filter_id` (`prod_filter_filter_id`),
  CONSTRAINT `tbl_product_filters_ibfk_1` FOREIGN KEY (`prod_filter_prod_id`) REFERENCES `tbl_products` (`prod_id`),
  CONSTRAINT `tbl_product_filters_ibfk_2` FOREIGN KEY (`prid_filter_cat_id`) REFERENCES `tbl_categories` (`cat_id`),
  CONSTRAINT `tbl_product_filters_ibfk_3` FOREIGN KEY (`prod_filter_filter_id`) REFERENCES `tbl_category_filters` (`filter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_product_filters` */

/*Table structure for table `tbl_product_images` */

DROP TABLE IF EXISTS `tbl_product_images`;

CREATE TABLE `tbl_product_images` (
  `img_id` int(11) NOT NULL AUTO_INCREMENT,
  `img_name` varchar(150) DEFAULT NULL,
  `img_prod_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`img_id`),
  KEY `img_prod_id` (`img_prod_id`),
  CONSTRAINT `tbl_product_images_ibfk_1` FOREIGN KEY (`img_prod_id`) REFERENCES `tbl_products` (`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_product_images` */

/*Table structure for table `tbl_product_size` */

DROP TABLE IF EXISTS `tbl_product_size`;

CREATE TABLE `tbl_product_size` (
  `size_id` int(11) NOT NULL AUTO_INCREMENT,
  `size_name` varchar(200) DEFAULT NULL,
  `size_prod_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`size_id`),
  KEY `size_prod_id` (`size_prod_id`),
  CONSTRAINT `tbl_product_size_ibfk_1` FOREIGN KEY (`size_prod_id`) REFERENCES `tbl_products` (`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_product_size` */

/*Table structure for table `tbl_products` */

DROP TABLE IF EXISTS `tbl_products`;

CREATE TABLE `tbl_products` (
  `prod_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_sku` varchar(50) CHARACTER SET utf32 COLLATE utf32_bin NOT NULL,
  `prod_name` varchar(255) CHARACTER SET utf32 COLLATE utf32_bin DEFAULT NULL,
  `prod_url` varchar(270) NOT NULL,
  `prod_short_description` varchar(200) DEFAULT NULL,
  `prod_description` text CHARACTER SET utf32 COLLATE utf32_bin,
  `prod_vedio_url` varchar(250) DEFAULT NULL,
  `prod_price` float NOT NULL,
  `prod_onsale` tinyint(1) NOT NULL DEFAULT '0',
  `prod_sale_price` float NOT NULL,
  `prod_on_pro` tinyint(1) DEFAULT '0',
  `prod_pro_price` float NOT NULL,
  `prod_pro_start` datetime DEFAULT NULL,
  `prod_pro_end` datetime DEFAULT NULL,
  `prod_my_profit` varchar(255) DEFAULT NULL,
  `prod_is_return` tinyint(1) DEFAULT '0',
  `prod_is_feature` tinyint(1) DEFAULT '0',
  `prod_quantity` int(5) NOT NULL DEFAULT '0' COMMENT 'Product Inventory ',
  `prod_status` tinyint(1) NOT NULL,
  `prod_status2` varchar(50) DEFAULT NULL,
  `prod_shipping` varchar(100) DEFAULT NULL,
  `prod_shipping_price` float DEFAULT NULL,
  `prod_shipping_days` int(11) DEFAULT NULL,
  `mata_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `prod_is_delete` tinyint(1) DEFAULT '0',
  `prod_created_date` datetime NOT NULL,
  `prod_update_date` date NOT NULL,
  `prod_update_by` int(11) DEFAULT NULL COMMENT 'Who update product',
  `prod_store_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`prod_id`),
  KEY `prod_update_by` (`prod_update_by`),
  KEY `tbl_products_ibfk_1` (`prod_store_id`),
  CONSTRAINT `tbl_products_ibfk_1` FOREIGN KEY (`prod_store_id`) REFERENCES `tbl_user_store` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_products` */

insert  into `tbl_products`(`prod_id`,`prod_sku`,`prod_name`,`prod_url`,`prod_short_description`,`prod_description`,`prod_vedio_url`,`prod_price`,`prod_onsale`,`prod_sale_price`,`prod_on_pro`,`prod_pro_price`,`prod_pro_start`,`prod_pro_end`,`prod_my_profit`,`prod_is_return`,`prod_is_feature`,`prod_quantity`,`prod_status`,`prod_status2`,`prod_shipping`,`prod_shipping_price`,`prod_shipping_days`,`mata_title`,`meta_keyword`,`meta_description`,`prod_is_delete`,`prod_created_date`,`prod_update_date`,`prod_update_by`,`prod_store_id`) values (1,'test-121','New test product','new-test-product','img_8321456123348.jpg','<p>test</p>','img_4911456123348.jpg',25,0,0,0,0,NULL,NULL,NULL,0,0,66,1,NULL,NULL,NULL,NULL,'','','',1,'2016-02-22 12:06:13','2016-03-18',1,NULL),(2,'sdfsdf','sdfsdf sdf','sdfsdf-sdf','img_9231456118749.jpg','<p>sdf sdf ds f</p>',NULL,45,1,30,0,0,NULL,NULL,NULL,0,0,2,1,NULL,NULL,NULL,NULL,'','','',1,'2016-02-22 12:25:49','0000-00-00',NULL,NULL),(3,'Prod-id-123','test product 123','test-product-123','img_7991456135382.JPG','<p>test</p>','',10,1,8,0,0,NULL,NULL,NULL,0,0,100,0,NULL,NULL,NULL,NULL,'','','',1,'2016-02-22 05:03:02','2016-02-22',1,NULL),(4,'TGS','test product 123','test-product-123-1','img_881456136818.JPG','<p>test</p>','',10,0,0,0,0,NULL,NULL,NULL,0,0,10,0,NULL,NULL,NULL,NULL,'','','test',1,'2016-02-22 05:26:58','2016-02-24',1,NULL),(5,'DCN','Deluxe Festive Gift Tower','deluxe-festive-gift-tower','img_7151456315482.png','<p>test</p>',NULL,15,1,10,0,0,NULL,NULL,NULL,0,0,1,1,NULL,NULL,NULL,NULL,'','','',1,'2016-02-24 07:04:42','0000-00-00',NULL,NULL),(6,'test-color-prod','color_product','colorproduct','img_5501456386111.jpg','<p>test test test test test test</p>',NULL,25,1,20,0,0,NULL,NULL,NULL,0,0,21,1,NULL,NULL,NULL,NULL,'','','',1,'2016-02-25 02:41:51','0000-00-00',NULL,NULL),(7,'test-color-prod12','color_product','colorproduct-1','img_4531456386222.jpg','<p>test test test test test test</p>','',25,1,20,0,0,NULL,NULL,NULL,0,0,21,1,NULL,NULL,NULL,NULL,'','','',1,'2016-02-25 02:43:42','2016-03-18',1,NULL),(8,'Prod-id-100','Bags','bags','img_5661458285403.png','<p>This is a sample product description. This is a sample product description. This is a sample product description. This is a sample product description. This is a sample product description. This is a sample product description. This is a sample product description. This is a sample product description. This is a sample product description.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>This is a sample product description.</p>\r\n\r\n<p>This is a sample product description.</p>\r\n\r\n<p>This is a sample product description.</p>\r\n\r\n<p>This is a sample product description.</p>','img_2831458284181.jpg',100,1,80,0,0,NULL,NULL,NULL,0,0,4,1,NULL,NULL,NULL,NULL,'','','',0,'2016-02-26 06:36:50','2016-04-19',1,NULL),(9,'dfdf','dfdf','dfdf','img_9181456486685.jpg','<p>dfdfdfdf</p>',NULL,34,1,23,0,0,NULL,NULL,NULL,0,0,34,1,NULL,NULL,NULL,NULL,'','','',1,'2016-02-26 06:38:05','0000-00-00',NULL,NULL),(10,'Prod-lad-bag','Ladies Bag','ladies-bag','img_4621458298335.jpg','<p>Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>\r\n\r\n<p>Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>','img_8441458298336.jpg',19.95,1,12.95,0,0,NULL,NULL,NULL,0,0,100,1,NULL,NULL,NULL,NULL,'','','',0,'2016-02-28 11:44:56','2016-04-19',1,NULL),(11,'Prod-id-Caddate','Test & Product','test-product-1','img_2011456722800.jpg','<p>test test test</p>',NULL,10,1,5,0,0,NULL,NULL,NULL,0,0,100,1,NULL,NULL,NULL,NULL,'','','',1,'2016-02-29 12:13:20','0000-00-00',NULL,NULL),(12,'prod-12344','Test Product 1234','test-product-1234','img_7071456725699.jpg','<p>test test test</p>',NULL,10,0,0,0,0,NULL,NULL,NULL,0,0,100,1,NULL,NULL,NULL,NULL,'','','',1,'2016-02-29 01:01:39','0000-00-00',NULL,NULL),(13,'prod-12345','test product 123','test-product-123-2','img_2471458298395.jpg','<p>test test test</p>','img_6691458298395.jpg',10,1,8,0,0,NULL,NULL,NULL,0,0,100,1,NULL,NULL,NULL,NULL,'','','',1,'2016-02-29 01:06:01','2016-03-18',1,NULL),(14,'testid-12345','Test Product','test-product-2','img_501458307106.jpg','<p>sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here.</p>','img_5351458307106.jpg',20,0,0,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-18 09:12:30','2016-04-19',1,NULL),(15,'Test-123','New Bag','new-bag','img_7161458306829.jpg','<p>sample text here</p>\r\n\r\n<p>sample text here</p>\r\n\r\n<p>sample text hereSample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>\r\n\r\n<p>sample text here</p>','',10,0,0,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-18 09:13:49','2016-04-19',1,NULL),(16,'test-featured123','Featured_product123 Featured_product123456','featuredproduct123-featuredproduct123456','img_8701458307016.jpg','<p>Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>','img_7951458307016.jpg',20,0,0,0,0,NULL,NULL,NULL,0,0,100,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-18 09:16:56','2016-04-19',1,NULL),(17,'Test-1234','test product 123','test-product-123-3','img_8351458534404.jpg','<p>sample text hereSample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>','',10,0,0,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 12:26:44','2016-04-19',1,NULL),(18,'test-prod2','test-prod2','test-prod2','img_5571458534643.jpg','<p>sample text hereSample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>','',15,1,12.95,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 12:30:43','2016-04-19',1,NULL),(19,'test-prod3','test-prod3','test-prod3','img_7991458535296.jpg','<p>sample text here</p>','',20,0,0,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 12:41:36','2016-03-21',1,NULL),(20,'test-sale','saleproduct','saleproduct','img_7531458537977.jpg','<p>sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here</p>\n\n<p>sample text here</p>\n\n<p>sample text here</p>\n\n<p>sample text here</p>','img_6631458537977.jpg',10,0,0,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 01:26:17','2016-03-21',1,NULL),(21,'Phn-case1','Samsung Galaxy Cases','samsung-galaxy-cases','img_7751458539432.jpg','<p>sample text hereSample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>','img_5141458539433.jpg',10,0,0,0,0,NULL,NULL,NULL,0,0,9,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 01:50:32','2016-04-19',1,NULL),(22,'test-prod4','test-prod4','test-prod4','img_1611458539744.jpg','<p>sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here</p>\r\n\r\n<p>sample text here</p>\r\n\r\n<p>sample text here</p>\r\n\r\n<p>Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>','',10,0,0,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 01:55:44','2016-04-19',1,NULL),(23,'test-prod5','test-prod5','test-prod5','img_5781458539952.jpg','<p>Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>','',10,0,0,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 01:59:12','2016-04-19',1,NULL),(24,'test-prod6','test-prod6','test-prod6','img_9141458540299.jpg','<p>Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>','',10,0,0,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 02:04:59','2016-04-19',1,NULL),(25,'test-prod7','test-prod7','test-prod7','img_2841458540985.jpg','<p>test-prod7</p>',NULL,10,0,0,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 02:16:25','0000-00-00',NULL,NULL),(26,'test-prod8','test-prod8','test-prod8','img_5671458541091.jpg','<p>Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>','',10,0,0,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'meta description','Meta Keyword','Meta Title',0,'2016-03-21 02:18:11','2016-04-19',1,NULL),(27,'test-prod9','test-prod9','test-prod9','img_61458541132.jpg','<p>Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>','',10,0,0,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 02:18:52','2016-04-19',1,NULL),(28,'test-prod10','test-prod10','test-prod10','img_5031458541178.jpg','<p>Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>','',10,0,0,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 02:19:38','2016-04-19',1,NULL),(29,'test-prod11','test-prod11','test-prod11','img_4051458541254.jpg','<p>Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>','',10,1,8,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 02:20:54','2016-04-19',1,NULL),(30,'test-prod12','test-prod12','test-prod12','img_101458541316.jpg','<p>Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>','img_2011458541317.jpg',10,0,0,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 02:21:56','2016-04-19',1,NULL),(31,'test-prod13','test-prod13','test-prod13','img_1671458541389.jpg','<p>Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>','',5,0,0,0,0,NULL,NULL,NULL,0,0,4,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 02:23:09','2016-04-19',1,NULL),(32,'test-prod14','test-prod14','test-prod14','img_2781458541428.jpg','<p>Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>','',10,0,0,0,0,NULL,NULL,NULL,0,0,5,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 02:23:48','2016-04-19',1,NULL),(33,'test-prod15','test-prod15','test-prod15','img_4561458541518.jpg','<p>Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>','',5,0,0,0,0,NULL,NULL,NULL,0,0,8,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 02:25:18','2016-04-19',1,NULL),(34,'test-prod16','test-prod16','test-prod16','img_4561458541564.jpg','<p>Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>','',10,0,0,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 02:26:04','2016-04-19',1,NULL),(35,'test-prod17','test-prod17','test-prod17','img_201458541615.jpg','<p>Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here. Sample text here.</p>','',10,0,0,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 02:26:55','2016-04-19',1,NULL),(36,'test-prod18','test-prod18','test-prod18','img_8311458541669.jpg','<p>test-prod18</p>','',5,0,0,0,0,NULL,NULL,NULL,0,0,9,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 02:27:49','2016-04-19',1,NULL),(37,'test-prod-multicategory','Test Multicategory Product','test-multicategory-product','img_8131458542509.jpg','<p>sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here sample text here</p>','img_2641458542509.jpg',10,0,0,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 02:41:49','2016-04-19',1,NULL),(38,'test-prod123','test-prod123','test-prod123','img_1181458552773.jpg','<p>test-prod123</p>','',10,0,0,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 05:32:53','2016-04-19',1,NULL),(39,'test-prod1235','test-prod1235','test-prod1235','img_5461458553050.jpg','<p>test-prod1235</p>','',10,0,0,0,0,NULL,NULL,NULL,0,0,1,1,NULL,NULL,NULL,NULL,'Meta Description','Meta Keyword','Meta Title',0,'2016-03-21 05:37:30','2016-04-19',1,NULL),(40,'test-prod12345','test-prod12345','test-prod12345','img_8511458555750.jpg','<p>test-prod12345</p>','img_2251459225662.JPG',10,0,0,0,0,NULL,NULL,NULL,0,0,79,1,NULL,NULL,NULL,NULL,'','','',0,'2016-03-21 06:22:30','2016-04-19',1,NULL),(41,'1232','test','test-1','img_2801458657598.jpg','<p>test 123</p>','',1232,0,0,0,0,NULL,NULL,NULL,0,0,12,1,NULL,NULL,NULL,NULL,'test','test','test',0,'2016-03-22 10:39:58','2016-04-19',1,NULL),(42,'Prod-shiptest','Product Ship Test','product-ship-test','img_2291459509616.jpg','<p>sample text here</p>','img_1891459509616.jpg',10,1,8,0,0,NULL,NULL,NULL,0,0,10,1,NULL,NULL,NULL,NULL,'META DESCRIPTION','Meta Keyword','Meta Title',0,'2016-04-01 07:20:16','2016-04-19',1,NULL);

/*Table structure for table `tbl_promocode` */

DROP TABLE IF EXISTS `tbl_promocode`;

CREATE TABLE `tbl_promocode` (
  `promo_id` int(11) NOT NULL AUTO_INCREMENT,
  `promo_code` varchar(100) NOT NULL,
  `promo_type` int(11) NOT NULL COMMENT '0=For All Site; 1= For Specific Product only',
  `promo_category_id` int(11) NOT NULL,
  `promo_sub_cat_id` int(11) NOT NULL,
  `promo_product_id` int(11) NOT NULL,
  `promo_discount_type` tinyint(4) NOT NULL COMMENT '0=price wise; 1=percentage wise',
  `promo_price_amount` float NOT NULL,
  `promo_start_date` date NOT NULL,
  `promo_end_date` date NOT NULL,
  `promo_status` tinyint(4) NOT NULL DEFAULT '1',
  `promo_is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `promo_created_date` date NOT NULL,
  PRIMARY KEY (`promo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_promocode` */

insert  into `tbl_promocode`(`promo_id`,`promo_code`,`promo_type`,`promo_category_id`,`promo_sub_cat_id`,`promo_product_id`,`promo_discount_type`,`promo_price_amount`,`promo_start_date`,`promo_end_date`,`promo_status`,`promo_is_delete`,`promo_created_date`) values (28,'9EMBNI',1,2,11,40,0,5,'2016-03-31','2016-04-07',0,0,'2016-03-31'),(29,'TQ9BT3',0,0,0,0,0,5,'2016-03-31','2016-04-10',0,0,'2016-03-31'),(30,'5V9O1K',1,2,11,39,0,5,'2016-03-31','2016-04-21',0,0,'2016-03-31');

/*Table structure for table `tbl_search_prod` */

DROP TABLE IF EXISTS `tbl_search_prod`;

CREATE TABLE `tbl_search_prod` (
  `ser_id` int(11) NOT NULL AUTO_INCREMENT,
  `ser_prod_id` int(11) DEFAULT NULL,
  `ser_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`ser_id`),
  KEY `ser_prod_id` (`ser_prod_id`),
  CONSTRAINT `tbl_search_prod_ibfk_1` FOREIGN KEY (`ser_prod_id`) REFERENCES `tbl_products` (`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_search_prod` */

/*Table structure for table `tbl_send` */

DROP TABLE IF EXISTS `tbl_send`;

CREATE TABLE `tbl_send` (
  `send_id` int(11) NOT NULL AUTO_INCREMENT,
  `send_subject` text,
  `send_message` text,
  `send_to` text,
  `send_from` text,
  `send_date` datetime DEFAULT NULL,
  `send_delete` tinyint(1) DEFAULT '0',
  `send_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`send_id`),
  KEY `send_user_id` (`send_user_id`),
  CONSTRAINT `tbl_send_ibfk_1` FOREIGN KEY (`send_user_id`) REFERENCES `tbl_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_send` */

/*Table structure for table `tbl_send_attach` */

DROP TABLE IF EXISTS `tbl_send_attach`;

CREATE TABLE `tbl_send_attach` (
  `se_att_id` int(11) NOT NULL AUTO_INCREMENT,
  `se_att_name` varchar(250) DEFAULT NULL,
  `se_att_send_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`se_att_id`),
  KEY `se_att_inbox_id` (`se_att_send_id`),
  CONSTRAINT `tbl_send_attach_ibfk_1` FOREIGN KEY (`se_att_send_id`) REFERENCES `tbl_send` (`send_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_send_attach` */

/*Table structure for table `tbl_sessions` */

DROP TABLE IF EXISTS `tbl_sessions`;

CREATE TABLE `tbl_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_sessions` */

/*Table structure for table `tbl_site_setting` */

DROP TABLE IF EXISTS `tbl_site_setting`;

CREATE TABLE `tbl_site_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(222) NOT NULL,
  `site_email` varchar(222) NOT NULL,
  `mailing_address` text NOT NULL,
  `site_phone` varchar(100) NOT NULL,
  `site_fax` varchar(255) NOT NULL,
  `facebook` varchar(222) NOT NULL,
  `google_plus` varchar(222) NOT NULL,
  `twitter` varchar(222) NOT NULL,
  `instagram` varchar(222) NOT NULL,
  `linkedin` varchar(200) NOT NULL,
  `youtube` varchar(200) NOT NULL,
  `pinterest` varchar(222) NOT NULL,
  `site_logo` varchar(222) NOT NULL,
  `tax` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_site_setting` */

insert  into `tbl_site_setting`(`id`,`name`,`site_email`,`mailing_address`,`site_phone`,`site_fax`,`facebook`,`google_plus`,`twitter`,`instagram`,`linkedin`,`youtube`,`pinterest`,`site_logo`,`tax`) values (1,'EDucki','info@educki.com','sample text here.','(888) 441-1234','(888) 441-1234','https://www.facebook.com','https://plus.google.com/112490552119918470126/posts','https://twitter.com/','https://www.instagram.com/','https://linkedin.com/','https://www.youtube.com/','https://www.pinterest.com/','logo.png',10);

/*Table structure for table `tbl_states` */

DROP TABLE IF EXISTS `tbl_states`;

CREATE TABLE `tbl_states` (
  `stat_id` varchar(22) NOT NULL,
  `stat_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_states` */

insert  into `tbl_states`(`stat_id`,`stat_name`) values ('AK','Alaska'),('AL','Alabama'),('AR','Arkansas'),('AZ','Arizona'),('CA','California'),('CO','Colorado'),('CT','Connecticut'),('DC','District of Columbia'),('DE','Delaware'),('FL','Florida'),('GA','Georgia'),('HI','Hawaii'),('IA','Iowa'),('ID','Idaho'),('IL','Illinois'),('IN','Indiana'),('KS','Kansas'),('KY','Kentucky'),('LA','Louisiana'),('MA','Massachusetts'),('MD','Maryland'),('ME','Maine'),('MI','Michigan'),('MN','Minnesota'),('MO','Missouri'),('MS','Mississippi'),('MT','Montana'),('NC','North Carolina'),('ND','North Dakota'),('NE','Nebraska'),('NH','New Hampshire'),('NJ','New Jersey'),('NM','New Mexico'),('NV','Nevada'),('NY','New York'),('OH','Ohio'),('OK','Oklahoma'),('OR','Oregon'),('PA','Pennsylvania'),('RI','Rhode Island'),('SC','South Carolina'),('SD','South Dakota'),('TN','Tennessee'),('TX','Texas'),('UT','Utah'),('VA','Virginia'),('VT','Vermont'),('WA','Washington'),('WI','Wisconsin'),('WV','West Virginia'),('WY','Wyoming');

/*Table structure for table `tbl_subscriber` */

DROP TABLE IF EXISTS `tbl_subscriber`;

CREATE TABLE `tbl_subscriber` (
  `sub_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_email` varchar(200) NOT NULL,
  `sub_name` varchar(200) NOT NULL,
  `sub_created_date` date NOT NULL,
  `sub_is_active` tinyint(1) NOT NULL DEFAULT '1',
  `sub_is_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sub_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_subscriber` */

insert  into `tbl_subscriber`(`sub_id`,`sub_email`,`sub_name`,`sub_created_date`,`sub_is_active`,`sub_is_delete`) values (1,'mikesmith1166@gmail.com','','2016-03-02',1,0),(2,'mike@gmail.com','','2016-03-02',1,0),(3,'test@test.test','','2016-03-07',1,0);

/*Table structure for table `tbl_user` */

DROP TABLE IF EXISTS `tbl_user`;

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_fname` varchar(255) DEFAULT NULL,
  `user_lname` varchar(200) DEFAULT NULL,
  `user_sex` int(5) DEFAULT NULL COMMENT '1=male,2=female,3=other',
  `user_other_sex` varchar(50) DEFAULT NULL,
  `user_age` varchar(50) DEFAULT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_password_encryp` varchar(255) DEFAULT NULL COMMENT 'md5',
  `user_email` varchar(255) DEFAULT NULL,
  `user_type` varchar(255) DEFAULT 'Buyer' COMMENT 'Buyer or Seller user',
  `user_interests` varchar(50) DEFAULT NULL,
  `user_other_interest` varchar(50) DEFAULT NULL,
  `user_last_active` datetime DEFAULT NULL,
  `user_phone` varchar(255) DEFAULT NULL,
  `user_zip` varchar(255) DEFAULT NULL,
  `user_state` varchar(255) DEFAULT NULL,
  `user_country` varchar(255) DEFAULT NULL,
  `user_city` varchar(255) DEFAULT NULL,
  `user_gross_sales` varchar(255) DEFAULT NULL,
  `user_fee` float DEFAULT NULL,
  `user_status` tinyint(4) DEFAULT '0' COMMENT '0 for inactive , 1 for active , 2 for vacation mode and 3 for deleted account',
  `user_flagged_status` tinyint(4) DEFAULT '0' COMMENT '0 for unflagged user and 1 for flagged user',
  `user_offense` varchar(255) DEFAULT NULL,
  `user_register_date` date DEFAULT NULL,
  `subscription_type` varchar(255) DEFAULT NULL,
  `subscription_revenue` int(11) DEFAULT NULL,
  `subscription_count` int(100) DEFAULT NULL,
  `subscription_date` datetime DEFAULT NULL,
  `user_business_owner` tinyint(1) DEFAULT '0',
  `user_renewal` tinyint(1) DEFAULT '0' COMMENT '0 first time/1=renewal',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_user` */

insert  into `tbl_user`(`user_id`,`user_fname`,`user_lname`,`user_sex`,`user_other_sex`,`user_age`,`user_password`,`user_password_encryp`,`user_email`,`user_type`,`user_interests`,`user_other_interest`,`user_last_active`,`user_phone`,`user_zip`,`user_state`,`user_country`,`user_city`,`user_gross_sales`,`user_fee`,`user_status`,`user_flagged_status`,`user_offense`,`user_register_date`,`subscription_type`,`subscription_revenue`,`subscription_count`,`subscription_date`,`user_business_owner`,`user_renewal`) values (1,'johnsmith1992',NULL,1,NULL,NULL,'123456','7c4a8d09ca3762af61e59520943dc26494f8941b','johnsmith1166@gmail.com','Seller','fertility',NULL,NULL,'4168484641684649','15100','NY','US','New York','',0,2,0,'','2017-10-02','',0,NULL,NULL,0,0),(2,'Test User',NULL,1,NULL,NULL,'123456','7c4a8d09ca3762af61e59520943dc26494f8941b','test.user7756@gmail.com','Buyer','family',NULL,NULL,'4168484641684649','12345','ID','Canada','ABC','',51,1,1,'','2017-10-03','',0,NULL,NULL,0,0),(3,'Test Mike',NULL,2,NULL,NULL,'123456','7c4a8d09ca3762af61e59520943dc26494f8941b','test1555@gmail.com','Seller',NULL,NULL,NULL,'4168484641684649','12345','HI','US','ASD','',10,1,1,'','2017-10-03','',0,NULL,NULL,0,0),(5,'new','test',3,'test','35-44','123456','7c4a8d09ca3762af61e59520943dc26494f8941b','test155s5@gmail.com','Buyer','family','','2017-10-18 00:00:00','4168484641684649','12345','HI','US','ASD',NULL,10,1,1,NULL,'2017-10-20',NULL,0,NULL,'2017-10-22 00:00:00',1,0);

/*Table structure for table `tbl_user_addreses` */

DROP TABLE IF EXISTS `tbl_user_addreses`;

CREATE TABLE `tbl_user_addreses` (
  `add_id` int(11) NOT NULL AUTO_INCREMENT,
  `add_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`add_id`),
  KEY `add_user_id` (`add_user_id`),
  CONSTRAINT `tbl_user_addreses_ibfk_1` FOREIGN KEY (`add_user_id`) REFERENCES `tbl_user` (`user_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_user_addreses` */

/*Table structure for table `tbl_user_flag_history` */

DROP TABLE IF EXISTS `tbl_user_flag_history`;

CREATE TABLE `tbl_user_flag_history` (
  `flag_id` int(11) NOT NULL AUTO_INCREMENT,
  `flag_user_id` int(11) DEFAULT NULL,
  `flag_date` datetime DEFAULT NULL,
  `flag_by_admin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`flag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_user_flag_history` */

insert  into `tbl_user_flag_history`(`flag_id`,`flag_user_id`,`flag_date`,`flag_by_admin_id`) values (1,5,'2017-10-20 09:38:40',1);

/*Table structure for table `tbl_user_login` */

DROP TABLE IF EXISTS `tbl_user_login`;

CREATE TABLE `tbl_user_login` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `login_user_id` int(11) DEFAULT NULL,
  `login_date` datetime DEFAULT NULL,
  PRIMARY KEY (`login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_user_login` */

/*Table structure for table `tbl_user_store` */

DROP TABLE IF EXISTS `tbl_user_store`;

CREATE TABLE `tbl_user_store` (
  `store_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_name` varchar(200) DEFAULT NULL,
  `store_logo` varchar(100) DEFAULT NULL,
  `store_banner_img` varchar(100) DEFAULT NULL,
  `store_user_id` int(100) DEFAULT NULL,
  `store_language` varchar(100) DEFAULT NULL,
  `store_country` varchar(100) DEFAULT NULL,
  `store_currency` varchar(50) DEFAULT NULL,
  `store_best_desc` varchar(200) DEFAULT NULL,
  `store_subscription_counter` int(11) DEFAULT '0',
  `store_city` varchar(150) DEFAULT NULL,
  `store_state` varchar(150) DEFAULT NULL,
  `store_zip` varchar(50) DEFAULT NULL,
  `store_street` varchar(250) DEFAULT NULL,
  `store_phone` varchar(100) DEFAULT NULL,
  `store_facebook` varchar(255) DEFAULT NULL,
  `store_twitter` varchar(255) DEFAULT NULL,
  `store_instagram` varchar(255) DEFAULT NULL,
  `store_pinterest` varchar(255) DEFAULT NULL,
  `store_youtube` varchar(255) DEFAULT NULL,
  `store_longitude` varchar(100) DEFAULT NULL,
  `store_latitude` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`store_id`),
  KEY `store_user_id` (`store_user_id`),
  CONSTRAINT `tbl_user_store_ibfk_1` FOREIGN KEY (`store_user_id`) REFERENCES `tbl_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_user_store` */

/*Table structure for table `tbl_user_store_subscription` */

DROP TABLE IF EXISTS `tbl_user_store_subscription`;

CREATE TABLE `tbl_user_store_subscription` (
  `sub_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_tiers` varchar(150) DEFAULT NULL,
  `sub_start` datetime DEFAULT NULL,
  `sub_ends` datetime DEFAULT NULL,
  `sub_store_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`sub_id`),
  KEY `sub_store_id` (`sub_store_id`),
  CONSTRAINT `tbl_user_store_subscription_ibfk_1` FOREIGN KEY (`sub_store_id`) REFERENCES `tbl_user_store` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_user_store_subscription` */

/*Table structure for table `tbl_watchlist` */

DROP TABLE IF EXISTS `tbl_watchlist`;

CREATE TABLE `tbl_watchlist` (
  `watchId` int(11) NOT NULL AUTO_INCREMENT,
  `watchProdId` int(11) NOT NULL,
  `watchUserId` int(11) NOT NULL,
  PRIMARY KEY (`watchId`),
  KEY `watchId` (`watchId`,`watchProdId`,`watchUserId`),
  KEY `watchUserId` (`watchUserId`),
  KEY `watchProdId` (`watchProdId`),
  CONSTRAINT `tbl_watchlist_ibfk_1` FOREIGN KEY (`watchUserId`) REFERENCES `tbl_user` (`user_id`),
  CONSTRAINT `tbl_watchlist_ibfk_2` FOREIGN KEY (`watchProdId`) REFERENCES `tbl_products` (`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_watchlist` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
