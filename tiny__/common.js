

$(function(){
	setTimeout(function(){
		$('.message').fadeOut()	
	},10000);
	$(document).on('click','.message',function(){
		$(this).fadeOut();		
	});
	$(document).on('keyup','.makeurl',function(){
		
		var forbiddenChars = new RegExp("[^a-zA-Z0-9\]", 'g');
		//var forbiddenChars = new RegExp("[^a-zA-Z0-9\-]", 'g');
		if (forbiddenChars.test($(this).val())) {
			$(this).val($(this).val().replace(forbiddenChars, '-'));
		} 	
	});
	var tm_fonts 	= "Wander Lust Bold=wanderlustbold;"+
                    "Open Sans Regular=open_sansregular;"+
                    "Open Sans Semibold=open_sanssemibold;"+
                    "open Sans Light=open_sanslight;"+
                    "Open Sansbold=open_sansbold;";
                    
	tinymce.init({
		selector: '.myeditor',
		theme: 'modern',
		width: '100%',
		height: '400px',
		force_br_newlines : false,
      	force_p_newlines : false,
      	forced_root_block : 0,
		paste_as_text: false,
		paste_text_sticky: true,
		paste_auto_cleanup_on_paste : true,
		paste_remove_styles: true,
		paste_remove_styles_if_webkit: true,
		paste_strip_class_attributes: true,
		apply_source_formatting : false,
		/*oninit: function (ed) {
			ed.pasteAsPlainText = true;
		},*/
		plugins: [
		  'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
		  'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
		  'save table contextmenu directionality emoticons template paste textcolor imagetools'
		],
		content_css : base_url+"tiny/tinymce/js/tinymce/css/web_fonts/fonts.css",
		external_plugins: {
			'advlist': 'plugins/advlist/plugin.min.js',
			'autolink': 'plugins/autolink/plugin.min.js',
			'link': 'plugins/link/plugin.min.js',
			'image': 'plugins/image/plugin.min.js',
			'lists': 'plugins/lists/plugin.min.js',
			'charmap': 'plugins/charmap/plugin.min.js',
			'print': 'plugins/print/plugin.min.js',
			'preview': 'plugins/preview/plugin.min.js',
			'hr': 'plugins/hr/plugin.min.js',
			'anchor': 'plugins/anchor/plugin.min.js',
			'pagebreak': 'plugins/pagebreak/plugin.min.js',
			'spellchecker': 'plugins/spellchecker/plugin.min.js',
			'searchreplace': 'plugins/searchreplace/plugin.min.js',
			'wordcount': 'plugins/wordcount/plugin.min.js',
			'visualblocks': 'plugins/visualblocks/plugin.min.js',
			'visualchars': 'plugins/visualchars/plugin.min.js',
			'code': 'plugins/code/plugin.min.js',
			'fullscreen': 'plugins/fullscreen/plugin.min.js',
			'insertdatetime': 'plugins/insertdatetime/plugin.min.js',
			'media': 'plugins/media/plugin.min.js',
			'nonbreaking': 'plugins/nonbreaking/plugin.min.js',
			'save': 'plugins/save/plugin.min.js',
			'table': 'plugins/table/plugin.min.js',
			'contextmenu': 'plugins/contextmenu/plugin.min.js',
			'directionality': 'plugins/directionality/plugin.min.js',
			'emoticons': 'plugins/emoticons/plugin.min.js',
			'template': 'plugins/template/plugin.min.js',
			'paste': 'plugins/paste/plugin.min.js',
			'textcolor': 'plugins/textcolor/plugin.min.js',
			'imagetools': 'plugins/imagetools/plugin.min.js',
		},
		toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | bold italic sizeselect fontselect fontsizeselect',
		
		 font_formats : "Andale Mono=andale mono,times;"+
                "Arial=arial,helvetica,sans-serif;"+
                "Arial Black=arial black,avant garde;"+
                "Book Antiqua=book antiqua,palatino;"+
                "Comic Sans MS=comic sans ms,sans-serif;"+
                "Courier New=courier new,courier;"+
                "Georgia=georgia,palatino;"+
                "Helvetica=helvetica;"+
                "Impact=impact,chicago;"+
                "Symbol=symbol;"+
                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
                "Terminal=terminal,monaco;"+
                "Times New Roman=times new roman,times;"+
                "Trebuchet MS=trebuchet ms,geneva;"+
                "Verdana=verdana,geneva;"+
                "Webdings=webdings;"+
                "Wingdings=wingdings,zapf dingbats;"+
				"Wander Lust Bold=wanderlustbold;"+
				"Open Sans Regular=open_sansregular;"+
				"Open Sans Semibold=open_sanssemibold;"+
				"open Sans Light=open_sanslight;"+
				 "Open Sansbold=open_sansbold",
	   images_upload_url : base_url+'tiny/upload.php',
	   images_upload_base_path: ser,
	   images_upload_credentials: true,
	   //imagetools_cors_hosts: ['http://localhost/tiny/'],
	   imagetools_toolbar: "rotateleft rotateright | flipv fliph | editimage imageoptions",
	   file_browser_callback:RoxyFileBrowser,
	   file_browser_callback_types: 'file image media',
  	});
	function RoxyFileBrowser(field_name, url, type, win) {
		  var roxyFileman = base_url+'tiny/fileman/index.html';
		  if (roxyFileman.indexOf("?") < 0) {     
			roxyFileman += "?type=" + type;   
		  }
		  else {
			roxyFileman += "&type=" + type;
		  }
		  roxyFileman += '&input=' + field_name + '&value=' + win.document.getElementById(field_name).value;
		  if(tinyMCE.activeEditor.settings.language){
			roxyFileman += '&langCode=' + tinyMCE.activeEditor.settings.language;
		  }
		  tinyMCE.activeEditor.windowManager.open({
			 file: roxyFileman,
			 title: 'File Manager',
			 width: 850, 
			 height: 650,
			 resizable: "yes",
			 plugins: "media",
			 inline: "yes",
			 close_previous: "no"  
		  }, {     window: win,     input: field_name    });
		  return false; 
	}
	
	
});