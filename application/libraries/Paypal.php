<?php 
/**
- Connected Path Payment API of PayPal
- 31 Jan 2018
- Lib For Codeigniter
*/
Class Paypal { 

	public $apiUrl = 'https://api.paypal.com/';
	private $clientId = "Af38u4pihZE4XCfm3A5hMrufld3whOPc0Yj8NpQgOlkioz3WVIhRaaeSJvRftobvREUctUhrJ8ZfSG0W";
	private $secret = "EEDBmsw98FawdGlWvWy3okWanXiV_0MxVfxc4XTxOnIu8tP42epU5HqvpGA6PV5RDvj9jGF6Ix_nyDvY";
	private $payerID = "Z4AFQSBNUS2VL";
	private $bn_code = "eDucki_SP";
	private $account = "1618576636223598928";
	private $commisionEmail = 'educki.help@gmail.com';

	public function GetToken(){
		$url = $this->apiUrl.'v1/oauth2/token';
		$header = array(
						'Accept: application/json',
						'Accept-Language: en_US',
					);
		$values = array(
			'grant_type' => 'client_credentials',
		);
		$params = http_build_query($values);

		try{
			$ch = curl_init();
			curl_setopt($ch,CURLOPT_URL,$url);
			//
			curl_setopt($ch, CURLOPT_HEADER, TRUE); 
			curl_setopt($ch,CURLOPT_HTTPHEADER, $header);
			//
			curl_setopt($ch, CURLOPT_USERPWD, "$this->clientId:$this->secret");
			//
			curl_setopt($ch,CURLOPT_POST,true);
			curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
			//
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			
			$result = curl_exec($ch);
			curl_close($ch);

			$expStr = explode('Content-Type: application/json',$result);
			/*echo "<pre>";
			print_r($expStr[1]);
			exit;*/
			return $expStr[1];
		}catch(Exception $e){
			return $e->getMessage();
		}

	}

	public function CreateOrder($access_type, $access_token, $cartId='', $shipping_address_info){ 
		if($cartId == ''){
			return 0;
		}
		$url = $this->apiUrl."v1/checkout/orders";

		// setting CURL header
		$header = array(
						"Accept: application/json",
						"Accept-Language: en_US",
						"PayPal-Client-Metadata-Id: Client_ID".date('mdyHis'),
						"PayPal-Request-Id: sample-request-id-".date('mdyHis'),
						"PayPal-Partner-Attribution-Id: $this->bn_code",
						"Authorization: $access_type $access_token",
						"Content-Type: application/json",
					);
		
		// SETTING CURL Parameters
		//"merchant_id": "D948KD8XUAWCY"
		//"merchant_id": "ZUSKJXPQJXSCY"

		$seller_total = getSellerTotal($cartId);
		
		$shippingAddress = json_decode($shipping_address_info);
		
		$sellerParm ='';
		foreach ($seller_total as $key => $seller) {
			$storeSubTotal = number_format(($seller['store_total']+$seller['store_commission_total']), 2, '.', '')-number_format($seller['store_shipping_total'], 2, '.', '');
			$sellerParm .= '
				{
			        "reference_id": "'.$seller['store_name'].$seller['store_user_id'].date('m-d-Y-H-i-s').'",
			        "description": "'.$seller['store_name'].'",
			        "amount": {
			            "currency": "USD",
			            "details": {
			                "subtotal": '.$storeSubTotal.',
			                "shipping": '.number_format($seller['store_shipping_total'], 2, '.', '').',
			                "tax": "0.00"
			            },
			            "total": '.number_format(($seller['store_total']+$seller['store_commission_total']), 2, '.', '').'
		        	},
			        "payee": {
			            
			            "email": "'.$seller['store_email'].'"
			        },
			        "items": [],
			        "shipping_address": {
			            "recipient_name": "'.$shippingAddress->shipping_first_name.' '.$shippingAddress->shipping_last_name.'",
			            "default_address": false,
			            "preferred_address": false,
			            "primary_address": false,
			            "disable_for_transaction": false,
			            "line1": "'.$shippingAddress->shipping_street.'",
			            "line2": "N/A",
			            "city": "'.$shippingAddress->shipping_city.'",
			            "country_code": "'.$shippingAddress->shipping_country.'",
			            "postal_code": "'.$shippingAddress->shipping_zip.'",
			            "state": "'.($shippingAddress->shipping_state1=''?$shippingAddress->shipping_state:$shippingAddress->shipping_state_other).'"			            
			        },
		        	"shipping_method": "",
		        	"partner_fee_details": {
			            "receiver": {
			                "email": "'.$this->commisionEmail.'"
						},
			            "amount": {
			                "value": '.number_format($seller['store_commission_total'], 2, '.', '').',
			                "currency": "USD"
			            }
		        	},
			        "payment_linked_group": 1,
			        "custom": "oren",
			        "invoice_number": "invoice_'.date('m_d_Y_H_i_s').'",
			        "payment_descriptor": "Connected Path Marketplace eDucki"
		    	},';
        }
        $sellerParm = rtrim($sellerParm, ',');
		$params = '
			{ 
			    "purchase_units": 
			    [
			    	'.$sellerParm.'
			    ],
			    
			    "redirect_urls": {
			        "return_url": "'.base_url('processing-order').'",
			        "cancel_url": "'.base_url('cart-confirm-order').'"
			    }
			}';

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		//
		curl_setopt($ch, CURLOPT_HEADER, TRUE); 
		curl_setopt($ch,CURLOPT_HTTPHEADER, $header);
		//
		curl_setopt($ch,CURLOPT_POST,true);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
		//
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$result = curl_exec($ch);
		curl_close($ch);
		$expStr = explode('Content-Type: application/json',$result);
		return $expStr[1];
		//return $result;
		
	}
	//"phone": "'.$shippingAddress->shipping_phone.'"


	public function PayForOrder($access_type, $access_token, $order_id){


		$url = $this->apiUrl."v1/checkout/orders/".$order_id."/pay";

		// setting CURL header
		$header = array(
						// "PayPal-Client-Metadata-Id: Client_ID".rand(0,9999),
						"PayPal-Request-Id: sample-request-id-".rand(0,9999),
						"PayPal-Partner-Attribution-Id: $this->bn_code",
						"Authorization: $access_type $access_token",
						"Content-Type: application/json",
					);
		
		// SETTING CURL Parameters
		$params = '{
		    "disbursement_mode": "DELAYED"
		}';

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		//
		curl_setopt($ch, CURLOPT_HEADER, TRUE); 
		curl_setopt($ch,CURLOPT_HTTPHEADER, $header);
		//
		curl_setopt($ch,CURLOPT_POST,true);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
		//
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$result = curl_exec($ch);
		/*echo "<pre>";
		print_r($result);
		exit;*/
		curl_close($ch);
		$expStr = explode('Content-Type: application/json',$result);
		return $expStr[1];
		//return $result;

	}

	public function CaptureOrder($access_type, $access_token, $capture_id){
		$url = $this->apiUrl."v1/payments/capture/".$capture_id;

		// setting CURL header
		$header = array(
						"PayPal-Request-Id: sample-request-id-".rand(0,9999),
						"PayPal-Partner-Attribution-Id: $this->bn_code",
						"Authorization: $access_type $access_token",
						"Content-Type: application/json",
						"Accept: application/json"
					);
		
		// SETTING CURL Parameters
		$params = array();

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		//
		curl_setopt($ch, CURLOPT_HEADER, TRUE); 
		curl_setopt($ch,CURLOPT_HTTPHEADER, $header);
		//
		//curl_setopt($ch,CURLOPT_POST,false);
		//curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
		//
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$result = curl_exec($ch);
		curl_close($ch);
		$expStr = explode('Content-Type: application/json',$result);
		return $expStr[1];
	}

	public function GetOrderDetail($order_id,$access_type, $access_token){
		$url = $this->apiUrl."v1/checkout/orders/".$order_id;

		// setting CURL header
		$header = array(
						"PayPal-Client-Metadata-Id: Client_ID".rand(0,9999),
						"PayPal-Request-Id: sample-request-id-".rand(0,9999),
						"PayPal-Partner-Attribution-Id: $this->bn_code",
						"Authorization: $access_type $access_token",
						"Content-Type: application/json",
					);
		
		// SETTING CURL Parameters
		$params = array();

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		//
		curl_setopt($ch, CURLOPT_HEADER, TRUE); 
		curl_setopt($ch,CURLOPT_HTTPHEADER, $header);
		
		// curl_setopt($ch,CURLOPT_POST,0);
		// curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$result = curl_exec($ch);
		//$err = curl_error($curl);
		curl_close($ch);
		//echo $result;
		$expStr = explode('Content-Type: application/json',$result);
		return $expStr[1];
	}


	public function DisburseFunds($access_type, $access_token, $capture_id){
		$url = $this->apiUrl."v1/payments/referenced-payouts-items";
		//https://api.sandbox.paypal.com/v1/payments/referenced-payouts-items


		$curl = curl_init();

		curl_setopt_array($curl, array(
					CURLOPT_URL => $url,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_POSTFIELDS => "{\n\t\"reference_type\": \"TRANSACTION_ID\",\n\t\"reference_id\": \"$capture_id\"\n}",
					CURLOPT_HTTPHEADER => array(
											"Accept: application/json",
											"Authorization: $access_type $access_token",
											"Content-Type: application/json",
											"PayPal-Partner-Attribution-Id: $this->bn_code"
											),
				)
		);

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}
	}

	/**
	- Seller On Boarding Functions
	-	 
	-   |  |
	-   |  |
	-   |  |_ _   
	-	|	    |
		|  [ ]  |
		|_ _ _ _|
	*/

	public function CreatePartnerReferral($access_type, $access_token){ // (with prefill)

		$url = $this->apiUrl.'v1/customer/partner-referrals';

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "$url",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => '{
				"customer_data": {
					"customer_type": "MERCHANT",
					"person_details": {
						"email_address": "gopayments@camerashop.example.com",
						"date_of_birth": {
							"event_type": "BIRTH",
							"event_date": "1986-2-3T23:59:59.999Z"
						},
						"nationality_country_code": "US",
						"name": {
							"given_name":"Adam",
							"surname": "Hansel"
						}
					},
					"business_details": {
						"phone_contacts": [
							{
								"phone_number_details": {
									"country_code": "1",
									"national_number": "6025550128"
								},
								"phone_type": "FAX"
							}
						],
						"business_address": {
							"line1": "9999 N 90th Street",
							"state": "AZ",
							"city": "Scottsdale",
							"country_code": "US",
							"postal_code": "85258"
						},
						"business_type": "PROPRIETORSHIP",
						"category": "1008",
						"sub_category": "2076",
						"names": [
						  {
						    "type": "LEGAL",
						    "name": "Camera Shop"
						  }
						],
						"business_description": "Physical goods from the Camera Shop",
						"event_dates": [
						  {
						    "event_type": "ESTABLISHED",
						    "event_date": "2009-01-01T13:59:45Z"
						  }
						],
						"website_urls": [
						  "https://camerashop.example.com"
						],
						"annual_sales_volume_range": {
						  "minimum_amount": {
						    "currency": "USD",
						    "value": "60000"
						  },
						  "maximum_amount": {
						    "currency": "USD",
						    "value": "300000"
						  }
						},
						"average_monthly_volume_range": {
						  "minimum_amount": {
						    "currency": "USD",
						    "value": "5000"
						  },
						  "maximum_amount": {
						    "currency": "USD",
						    "value": "24999"
						  }
						},
						"email_contacts": [
						  {
						    "email_address": "support@camerashop.example.com",
						    "role": "CUSTOMER_SERVICE"
						  }
						]
					},
						"preferred_language_code": "en_US",
						"primary_currency_code": "USD",
						"partner_specific_identifiers": [
							{
						  "type": "TRACKING_ID",
						  "value": "1506721845"
						}
					]
				},
					"requested_capabilities": [
							{
								"capability": "API_INTEGRATION",
								"api_integration_preference": {
									"partner_id": "BSD55M2TSQT72",
									"rest_api_integration": {
									"integration_method": "PAYPAL",
									"integration_type": "THIRD_PARTY"
								},
							"rest_third_party_details": {
							  "partner_client_id": "AViNcnTmPaYZ3VltsmWEN3UmogFcZnjKsnqaitDo2cHrEEl1Rlns4GSz36CSUl69q9eADJwEItR0Rq7M",
							  "feature_list": [
							    "PAYMENT",
							    "REFUND",
							    "PARTNER_FEE",
							    "DELAY_FUNDS_DISBURSEMENT"
							  ]
							}
						}
					}
				],
				"web_experience_preference": {
						"partner_logo_url": "{{connected_partner_logo_url}}",
					"return_url": "{{connected_return_url}}",
					"action_renewal_url": "{{connected_action_renewal_url}}"
				},
					"collected_consents": [
						{
							"type": "SHARE_DATA_CONSENT",
						"granted": true
					}
				],
					"products": [
							"EXPRESS_CHECKOUT"
						]
					}',
			CURLOPT_HTTPHEADER => array(
				"Authorization: $access_type $access_token",
				"Content-Type: application/json"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		echo "cURL Error #:" . $err;
		} else {
		echo $response;
		}
	}
	public function getMerchantId($access_type, $access_token){


		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.paypal.com/v1/customer/partners/$this->payerID/merchant-integrations?tracking_id=1506721845",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Authorization: $access_type $access_token",
		    "Content-Type: application/json"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  echo $response;
		}
	}


}






	/*public function CreateOrderMultipleSeller($access_type, $access_token){
		$url = $this->apiUrl."v1/checkout/orders";

		$curl = curl_init();

		curl_setopt_array($curl, array(
				CURLOPT_URL => "$url",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => '
						{
							"purchase_units": 
							[
								{
									"reference_id": "camera_shop_seller_{{$timestamp}}",
									"description": "Camera Shop",
									"amount": {
										"currency": "USD",
										"details": {
											"subtotal": "1.09",
											"shipping": "0.02",
											"tax": "0.33"
										},
										"total": "1.44"
									},
									"payee": {

										"merchant_id": "FSMRBANCV8PSG"
									},
									"items": [
										{
											"name": "DSLRCameraLensCap",
											"sku": "sku03",
											"price": "0.54",
											"currency": "USD",
											"quantity": "1",
											"category": "PHYSICAL"
										},
										{
											"name": "DSLRCameraLensCleaner",
											"sku": "sku04",
											"price": "0.55",
											"currency": "USD",
											"quantity": "1",
											"category": "PHYSICAL"
										}
									],
									"shipping_address": {
										"recipient_name": "Pat Brandt",
										"line1": "2211 North First Street",
										"line2": "Building 17",
										"city": "San Jose",
										"country_code": "US",
										"postal_code": "95131",
										"state": "CA",
										"phone": "408-555-1901"
									},

									"shipping_method": "United Postal Service",
									"payment_linked_group": 1,
									"custom": "custom_value_{{$timestamp}}",
									"invoice_number": "invoice_number_{{$timestamp}}",
									"payment_descriptor": "Payment Camera Shop"
								},
								{
									"reference_id": "watch_shop_seller_{{$timestamp}}",
									"description": "Watch Shop",
									"amount": {
										"currency": "USD",
										"details": {
											"subtotal": "1.09",
											"shipping": "0.02",
											"tax": "0.33"
										},
									"total": "1.44"
									},
									"payee": {
										"merchant_id": "3KFWC7NA0WBY8"
									},
									"items": [
										{
											"name": "WatchCleaner",
											"sku": "sku06",
											"price": "0.54",
											"currency": "USD",
											"quantity": "1",
											"category": "PHYSICAL"
										},
										{
											"name": "WatchCase",
											"sku": "sku08",
											"price": "0.55",
											"currency": "USD",
											"quantity": "1",
											"category": "PHYSICAL"
										}
									],
									"shipping_address": {
										"recipient_name": "Pat Brandt",
										"line1": "2211 North First Street",
										"line2": "Building 17",
										"city": "San Jose",
										"country_code": "US",
										"postal_code": "95131",
										"state": "CA",
										"phone": "408-555-1901"
									},
									"shipping_method": "United Postal Service",
									"payment_linked_group": 1,
									"custom": "custom_value_{{$timestamp}}",
									"invoice_number": "invoice_number_{{$timestamp}}",
									"payment_descriptor": "Payment Camera Shop"
								}
							],
							"redirect_urls": {
								"return_url": "https://example.com?r",
								"cancel_url": "https://example.com?c"
							}
						}',
				CURLOPT_HTTPHEADER => array(
					"Accept: application/json",
					"Accept-Language: en_US",
					"Authorization: $access_type $access_token",
					"Content-Type: application/json",
					"PayPal-Partner-Attribution-Id: $this->bn_code",
					"PayPal-Request-Id: $timestamp",
					"Paypal-Client-Metadata-Id: 1234567890"
				),
			)
		);

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		echo "cURL Error #:" . $err;
		} else {
		echo $response;
		}
	}*/
?>