<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Stripe
{
    private $process_url = 'https://api.stripe.com/v1';
    private $api_key = 'sk_live_0LCxhw41WzTGHHH4Uf6FebBR';

    /*
    |--------------------------------------------
    | STRIPE | CONSTRUCT
    | (In case you need access to the CI Instance
    |--------------------------------------------
    */


    /*
    |-------------------------------------------
    | STRIPE | CREATE CUSTOMER
    |-------------------------------------------
    */
    public function create_customer($request) {

        $process_url = $this->process_url.'/customers';
        $res = $this->post_request($process_url, $request);
        
        return json_decode($res)->id;
    }

    /*
    |-------------------------------------------
    | STRIPE | GET CUSTOMER
    |-------------------------------------------
    */
    public function get_customer($cus_id) {
        $process_url = $this->process_url.'/customers/'.$cus_id;
        $customer_obj = $this->post_request($process_url);
        $customer_arr = json_decode($customer_obj);
        return $customer_arr;
    }

    /*
    |-------------------------------------------
    | STRIPE | GET CUSTOMER
    |-------------------------------------------
    */
    public function get_default_source($cus_id) {
        $process_url = $this->process_url.'/customers/'.$cus_id;
        $customer_obj = $this->post_request($process_url);
        $customer_arr = json_decode($customer_obj);
        $card_id = $customer_arr->default_source;

        return $card_id;

    }

    /*
    |-------------------------------------------
    | STRIPE | GET CARD
    |-------------------------------------------
    */
    public function get_card($cus_id,$card_id) {
        $process_url = $this->process_url.'/customers/'.$cus_id.'/cards/'.$card_id;
        $card_obj = $this->post_request($process_url);

        return $this->decode_response($card_obj);

    }

    /*
    |-----------------------------------------------
    | STRIPE | ADD CARD
    | Params: Stripge Customer ID, Card Data Object
    |-----------------------------------------------
    */
    public function add_card($cus_id,$card_data) {
        $request = array(
            'source' => $card_data
        );
        $process_url = $this->process_url.'/customers/'.$cus_id.'/sources';
        $card_json = $this->post_request($process_url,$request);
        $card_obj = json_decode($card_json);

        // successful?
        if(isset($card_obj->id)) {
            $ret_arr = array(
                'status' => true,
                'response' => $card_obj
            );
        } else {
            $ret_arr = array(
                'status' => false,
                'error' => $card_obj->error->message
            );
        }

        return $ret_arr;

    }

    /*
    |-------------------------------------------
    | STRIPE | ADD CARD
    |-------------------------------------------
    */
    public function subscribe_customer($cus_id,$plan_id, $source) {
        $request['plan'] = $plan_id;
        if($source) { $request['source'] = $source; }
        $process_url = $this->process_url.'/customers/'.$cus_id.'/subscriptions';
        $plan_json = $this->post_request($process_url,$request);
        $plan_obj = json_decode($plan_json);

        // successful?
        if(isset($plan_obj->id)) {
            $ret_arr = array(
                'status' => true,
                'response' => $plan_obj
            );
        } else {
            $ret_arr = array(
                'status' => false,
                'error' => $plan_obj->error->message
            );
        }

        return $ret_arr;

    }

    /*
    |-------------------------------------------
    | STRIPE | DECODE JSON RESPONSE
    |-------------------------------------------
    */
    public function decode_response($response) {
        return json_decode($response);
    }


    /*
    |-------------------------------------------
    | STRIPE | POST REQUESTS
    |-------------------------------------------
    */
    public function post_request($url, $data = NULL)
    {
        $ch = curl_init($url);

        if (is_array($data))
        {
            $data = http_build_query($data);
        }

        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        return $this->do_curl_request($ch);
    }

    /*
    |-------------------------------------------
    | STRIPE | CURL REQUEST
    |-------------------------------------------
    */
    private function do_curl_request($ch)
    {
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_USERPWD, $this->api_key);

        $response = curl_exec($ch);
        $error = curl_error($ch);

        if($response) {

        } else {
            var_dump($error);
        }

        curl_close($ch);
        return $response;
    }

}