<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

class Custom_calendar {  
     

	/********************* PROPERTY ********************/  
    private $dayLabels = array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");
	//private $dayLabels = array("Mon","Tue","Wed","Thu","Fri","Sat","Sun");
     
    private $currentYear = 0;
     
    private $currentMonth = 0;
    
    private $currentDay = 0;
     
    private $currentDate = null;
     
    private $daysInMonth = 0;
     
    private $naviHref = null;
	
	private $CI;
	
	private $region = null;
	
	private $all_weekends = null;
	
	private $all_holiday_dates = null;
	
	private $cutoff_exp = null;
	
	private $cutoff_std = null;
	
	private $is_meltable = null;
	
	private $exp_display = 0;
	
	private $prod_region_price = null;
	
	private $prod_data = null;
	
	private $expedite_dates = 0;
	
	/*private $is_loop = 1;
	
	private $exp_day = 1;*/
     
    
	/********************* PUBLIC **********************/  
    
	/**
	* Constructor
	*/
	public function __construct()
	{
		$this->CI =& get_instance();
		
		$this->CI->load->helper('date');
		
		//$this->naviHref = base_url('etc/test/calender');//htmlentities($_SERVER['PHP_SELF']);
		$this->naviHref = base_url('ajax/calender');
		
		//exit;
		//$human = unix_to_human(now());
		
		//days_in_month(06, 2005);
		
    }
	    
    /**
    *	Print out the calendar
    */
    public function show($cal_array = array(), $customer_array = array())
	{
		$year  = null;
        $month = null;
        
		if(null==$year && isset($cal_array['year']) && !empty($cal_array['year']))
		{
			$year = $cal_array['year'];
        }
		else if(null==$year)
		{
			$year = date("Y",time());
        }
		
        if(null==$month && isset($cal_array['month']) && !empty($cal_array['month']))
		{
			$month = $cal_array['month'];
         
        }
		else if(null==$month)
		{
			$month = date("m",time());
        }               
         
        $this->currentYear = $year;
         
        $this->currentMonth = $month;
         
        $this->daysInMonth = $this->_daysInMonth($month,$year);
		
		$this->prod_data = $this->_get_product_data($customer_array['prod_id']);
		
		$this->is_meltable = $this->prod_data['prod_is_metable'];
		
		if($customer_array['country']=="US")
		{
			$this->region = $this->_get_region_no($customer_array['zip']);
		}
		else
		{
			$this->region = 7;
		}
		
		$this->prod_region_price = $this->_get_product_zip_region_price($customer_array);		
		
		$this->all_weekends = $this->_get_4month_holidays();
		
		$this->all_holiday_dates = $this->_get_All_HoliDays();
		
		$cutoff_array = $this->_get_cutoff_time();
		$this->cutoff_std = date("Y-m-d ".$cutoff_array['standard_cut_off']);
		$this->cutoff_exp = date("Y-m-d ".$cutoff_array['expedite_cut_off']);
		
				
		$this->expedite_dates = $this->_get_expedited_date();
		
		$this->all_working_dates = $this->_get_all_working_dates();
		
						
		if(date('n', $this->currentMonth) == date('n') &&  $this->CI->session->userdata('exp_display1') <= 0)
		{
			$this->CI->session->set_userdata('exp_display1', 0);
			$this->CI->session->set_userdata('exp_display2', 0);
			$this->CI->session->set_userdata('exp_display3', 0);
			$this->CI->session->set_userdata('exp_display4', 0);
		}
		else if( $this->currentMonth == date('n', strtotime('+1 month')) )
		{
			$this->exp_display = $this->CI->session->userdata('exp_display1');
		}
		else if( $this->currentMonth == date('n', strtotime('+2 month')))
		{
			$this->exp_display = $this->CI->session->userdata('exp_display2');
		}
		else if ( $this->currentMonth == date('n', strtotime('+3 month')))
		{
			$this->exp_display = $this->CI->session->userdata('exp_display3');
		}
		
		// Print Calendar data.
		$content='<div id="calendar">';
			$content.='<div class="box">'.
				$this->_createNavi().
			'</div>'.
			'<div class="box-content">'.
				'<ul class="label">'.$this->_createLabels().'</ul>';
					$content.='<div class="clear"></div>';
					$content.='<ul class="dates">';
					
					$weeksInMonth = $this->_weeksInMonth($month,$year);
					// Create weeks in a month
					for($i=0; $i<$weeksInMonth; $i++)
					{
						//Create days in a week
						for($j=0; $j<=6; $j++)
						{
							//echo $i*7+$j.'<br>';
							$content.=$this->_showDay($i*7+$j);
						}
					}
					
					$content.='</ul>';
				$content.='<div class="clear"></div>';
			$content.='</div>';
			
		$content.='</div><br>';
		
		return $content;
		
    }
     
    /********************* PRIVATE **********************/ 
    /**
    * create the li element for ul
    */
    private function _showDay($cellNumber)
	{
		if($this->currentDay==0)
		{
            $firstDayOfTheWeek = date('w',strtotime($this->currentYear.'-'.$this->currentMonth.'-01'));
			
            if(intval($cellNumber) == intval($firstDayOfTheWeek))
			{                 
                $this->currentDay = 1;
            }
        }
		
		if(($this->currentDay!=0) && ($this->currentDay<=$this->daysInMonth))
		{
			$this->currentDate = date('Y-m-d',strtotime($this->currentYear.'-'.$this->currentMonth.'-'.($this->currentDay)));
            
            $cellContent = $this->currentDay;
             
            $this->currentDay++;
			//$this->currentDay--;
             
        }
		else
		{
            $this->currentDate = null;
            $cellContent = null;
			/*$this->currentDay = 1;
			$this->currentDate = date('Y-m-d',strtotime($this->currentYear.'-'.($this->currentMonth + 1).'-'.($this->currentDay)));
			$cellContent = $this->currentDay;*/
			
        }
		
		$expedited_date = $this->expedite_dates;
		
		//echo '<pre>';print_r($expedited_date);exit;
		$date_day = date('D', strtotime($this->currentDate));
		
		
		if($this->prod_data['prod_is_metable'] == 0)
		{
			if( strtotime($this->currentDate) == strtotime(date('Y-m-d'))  )
			{
				return '<li id="li-'.strtotime($this->currentDate).'" style="cursor:default" class=" color-02 '.($cellNumber%7==0?' start ':($cellNumber%7==6?' end ':' ')).
							($cellContent==null?'mask':'').'">'.$cellContent.'<p>Today</p></li>';
			}
			else if( strtotime($this->currentDate) > strtotime(date('Y-m-d')) && !(in_array($this->currentDate, @$this->all_weekends)) && $date_day != 'Sat' && $date_day != 'Sun' )
			{
				//return "not weekends";
				if(is_array($this->all_holiday_dates) && !empty($this->all_holiday_dates) && in_array($this->currentDate, @$this->all_holiday_dates))
				{
					$holiday_data = $this->_get_holiday_color($this->currentDate);
					
					return '<li id="li-'.strtotime($this->currentDate).'" style="background-color:'.$holiday_data['cal_color'].';cursor:default"  class="'.($cellNumber%7==0?' start ':($cellNumber%7==6?' end ':' ')).
						($cellContent==null?'mask':'').'">'.$cellContent.'<p>'.$holiday_data['cal_name'].'</p></li>';
				}
				else if(is_array($this->expedite_dates) && in_array($this->currentDate, @$this->expedite_dates))
				{
					if($this->exp_display > 2)
					{
						$this->exp_display = 2;
					}
					$onclick_fun = "onclick='get_expedite_date(".strtotime($this->currentDate).",".$this->exp_display.",".$this->prod_region_price[$this->exp_display].")'";
					$exp_price = $this->prod_region_price[$this->exp_display];
					$this->exp_display = $this->exp_display + 1;
					
					/*Set session for used in next month*/
					if($this->currentMonth == date('n'))
					{
						#echo 1;
						$this->CI->session->set_userdata('exp_display1', $this->exp_display);
					}
					else if( $this->currentDate == date('n', strtotime('+1 month')) )
					{
						#echo 2;
						$this->CI->session->set_userdata('exp_display2', $this->exp_display);
					}
					else if( $this->currentDate == date('n', strtotime('+2 month')))
					{
						#echo 3;
						$this->CI->session->set_userdata('exp_display3', $this->exp_display);
					}
					else if ( $this->currentDate == date('n', strtotime('+3 month')))
					{
						#echo 4;
						$this->CI->session->set_userdata('exp_display4', $this->exp_display);
					}
					/*Set session for used in next month*/
					
					return '<li id="li-'.strtotime($this->currentDate).'" '.$onclick_fun.' class=" color-04'.($cellNumber%7==0?' start ':($cellNumber%7==6?' end ':' ')).
							($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery + $'.number_format($exp_price,2,'.','').'</p></li>';
				}
				else if (!empty($this->expedite_dates) && $this->currentDate < $expedited_date[0])
				{
					return '<li id="li-'.strtotime($this->currentDate).' "style="cursor:default" class="'.($cellNumber%7==0?' start ':($cellNumber%7==6?' end ':' ')).
						($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
				}
				else
				{
					$onclick_fun = "onclick='get_date(".strtotime($this->currentDate).")'";
					//'.$onclick_fun.'
					return '<li id="li-'.strtotime($this->currentDate).'" '.$onclick_fun.' class=" color-03'.($cellNumber%7==0?' start ':($cellNumber%7==6?' end ':' ')).
						($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery</p></li>'; //$'.$prod_data['prod_stand_ship_price_us'].'
				}
			}
			else
			{
				return '<li id="li-'.$this->currentDate.' "style="cursor:default" class="'.($cellNumber%7==0?' start ':($cellNumber%7==6?' end ':' ')).
					($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
			}
		}
		else
		{
			
			if( strtotime($this->currentDate) == strtotime(date('Y-m-d'))  )
			{
				return '<li id="li-'.$this->currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==0?' start ':($cellNumber%7==6?' end ':' ')).
							($cellContent==null?'mask':'').'">'.$cellContent.'<p>Today</p></li>';
			}
			else if( strtotime($this->currentDate) > strtotime(date('Y-m-d')) && !(in_array($this->currentDate, @$this->all_weekends))  && $date_day != 'Sat' && $date_day != 'Sun' && $date_day != 'Mon'  )
			{
				
				//return "not weekends";
				if(is_array($this->all_holiday_dates) && !empty($this->all_holiday_dates) && in_array($this->currentDate, @$this->all_holiday_dates))
				{
					$holiday_data = $this->_get_holiday_color($this->currentDate);
					
					return '<li id="li-'.$this->currentDate.'" style="background-color:'.$holiday_data['cal_color'].';cursor:default"  class="'.($cellNumber%7==0?' start ':($cellNumber%7==6?' end ':' ')).
						($cellContent==null?'mask':'').'">'.$cellContent.'<p>'.$holiday_data['cal_name'].'</p></li>';
				}
				else if(is_array($this->expedite_dates) && in_array($this->currentDate, @$this->expedite_dates) &&  $date_day != 'Sat' && $date_day != 'Sun' && $date_day != 'Mon')
				{
					/*Set session for used in next month*/
					if($this->currentMonth == date('n'))
					{
						#echo 1;
						$this->CI->session->set_userdata('exp_display1', $this->exp_display);
					}
					else
					{
						#echo 4;
						$this->exp_display = 1;
					}
					/*Set session for used in next month*/
					
					if($this->exp_display == 0 )
					{
						$onclick_fun = "onclick='get_expedite_date(".strtotime($this->currentDate).",0,".$this->prod_region_price[0].")'";
						$exp_price = $this->prod_region_price[0];
						$this->exp_display = $this->exp_display + 1;
						
						return '<li id="li-'.strtotime($this->currentDate).'" '.$onclick_fun.' class=" color-04'.($cellNumber%7==0?' start ':($cellNumber%7==6?' end ':' ')).
							($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery + $'.number_format($exp_price,2,'.','').'</p></li>';
						
					}
					else if($date_day != "Tue")
					{
						if($this->exp_display > 1)
						{
							$this->exp_display = 1;
						}
						$onclick_fun = "onclick='get_expedite_date(".strtotime($this->currentDate).",".$this->exp_display.",".$this->prod_region_price[$this->exp_display].")'";
						$exp_price = $this->prod_region_price[$this->exp_display];
						$this->exp_display = $this->exp_display + 1;
						
						return '<li id="li-'.strtotime($this->currentDate).'" '.$onclick_fun.' class=" color-04'.($cellNumber%7==0?' start ':($cellNumber%7==6?' end ':' ')).
							($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery + $'.number_format($exp_price,2,'.','').'</p></li>';
						
					}
					else if ( $date_day == "Tue" && $this->exp_display > 0)
					{
						if($this->exp_display > 1)
						{
							$this->exp_display = 1;
						}
						$onclick_fun = "onclick='get_expedite_date(".strtotime($this->currentDate).", 0 ,".$this->prod_region_price[0].")'";
						$exp_price = $this->prod_region_price[0];
						$this->exp_display = $this->exp_display + 1;
						
						return '<li id="li-'.strtotime($this->currentDate).'" '.$onclick_fun.' class=" color-04'.($cellNumber%7==0?' start ':($cellNumber%7==6?' end ':' ')).
							($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery + $'.number_format($exp_price,2,'.','').'</p></li>';
						
					}
					/*else
					{
						return '<li id="li-'.$this->currentDate.' "style="cursor:default" class="'.($cellNumber%7==0?' start ':($cellNumber%7==6?' end ':' ')).
							($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
					}*/
				}
				else if (!empty($this->expedite_dates) && $this->currentDate < $expedited_date[0])
				{
					return '<li id="li-'.strtotime($this->currentDate).' "style="cursor:default" class="'.($cellNumber%7==0?' start ':($cellNumber%7==6?' end ':' ')).
						($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
				}
				else if ($date_day == 'Sat' || $date_day == 'Sun' || $date_day == 'Mon')
				{
					return '<li id="li-'.$this->currentDate.' "style="cursor:default" class="'.($cellNumber%7==0?' start ':($cellNumber%7==6?' end ':' ')).
						($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
				}
			}
			else
			{
				return '<li id="li-'.$this->currentDate.' "style="cursor:default" class="'.($cellNumber%7==0?' start ':($cellNumber%7==6?' end ':' ')).
					($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
			}
		}
         
        /*return '<li id="li-'.$this->currentDate.'" class="'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
                ($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery + $2 + $8</p></li>';*/
    }
     
    /**
    *	Create navigation
    */
    private function _createNavi()
	{
         $nextMonth = $this->currentMonth==12?1:intval($this->currentMonth)+1;
         
         $nextYear = $this->currentMonth==12?intval($this->currentYear)+1:$this->currentYear;
         
         $preMonth = $this->currentMonth==1?12:intval($this->currentMonth)-1;
         
         $preYear = $this->currentMonth==1?intval($this->currentYear)-1:$this->currentYear;
        
		$currYear = $this->currentYear;
		$currMonth = $this->currentMonth;
		
		#date($currYear.'-'.$currMonth.'-01');
		#date('Y-n-01');
		
		if(strtotime(date($currYear.'-'.$currMonth.'-01')) == strtotime(date('Y-n-01')))
		{
			return
				'<div class="header">'.
					'<a class="prev" title="Previous" href="javascript:void(0);"></a>'.
						'<span class="title">'.date('F Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
					'<a onclick = "month_changer('.$nextMonth.','.$nextYear.')" class="next" title="Next" href="javascript:void(0);"></a>'.
				'</div>';
		}
		else if (strtotime(date($currYear.'-'.$currMonth.'-01')) > strtotime(date('Y-n-01')) && strtotime(date($currYear.'-'.$currMonth.'-01')) < strtotime(date("Y-n-01", strtotime('+3 month'))) )
		{
			return
				'<div class="header">'.
					'<a onclick = "month_changer('.$preMonth.','.$preYear.')" class="prev" title="Previous" href="javascript:void(0);"></a>'.
						'<span class="title">'.date('F Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
					'<a onclick = "month_changer('.$nextMonth.','.$nextYear.')" class="next" title="Next" href="javascript:void(0);"></a>'.
				'</div>';
			
		}
		else if(strtotime(date($currYear.'-'.$currMonth.'-01')) >= strtotime(date('Y-n-01')) )
		{
			return
				'<div class="header">'.
					'<a onclick = "month_changer('.$preMonth.','.$preYear.')" class="prev" title="Previous" href="javascript:void(0);"></a>'.
						'<span class="title">'.date('F Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
					'<a class="next" title="Next" href="javascript:void(0);"></a>'.
				'</div>';
		}
		/*else
		{
			return
				'<div class="header">'.
					'<a onclick = "month_changer('.$preMonth.','.$preYear.')" class="prev" title="Previous" href="javascript:void(0);"></a>'.
						'<span class="title">'.date('M Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
					'<a class="next" title="Next" href="javascript:void(0);"></a>'.
				'</div>';
		}*/
		
		
		/* 
        return
            '<div class="header">'.
                '<a class="prev" title="Previous" href="'.$this->naviHref.'?month='.sprintf('%02d',$preMonth).'&year='.$preYear.'"></a>'.
                    '<span class="title">'.date('M Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
                '<a class="next" title="Next" href="'.$this->naviHref.'?month='.sprintf("%02d", $nextMonth).'&year='.$nextYear.'"></a>'.
            '</div>';
			
			*/
    }
         
    /**
    *	Create calendar week labels
    */
    private function _createLabels()
	{
		$content='';
        foreach($this->dayLabels as $index=>$label)
		{
			$content.='<li class="'.($label==6?'end title':'start title').' title">'.$label.'</li>';
        }
		//echo $content;exit;
        return $content;
    }
	
    /**
    *	Calculate number of weeks in a particular month
    */
    private function _weeksInMonth($month=null,$year=null)
	{
		if(null==($year))
		{
			$year =  date("Y",time()); 
        }
        if(null==($month))
		{
			$month = date("m",time());
        }
		
        // find number of days in this month
		
		$daysInMonths = $this->_daysInMonth($month,$year);
		//echo intval($daysInMonths/7);
        $numOfweeks = ($daysInMonths%7==0?0:1) + intval($daysInMonths/7);
		
		$monthEndingDay= date('N',strtotime($year.'-'.$month.'-'.$daysInMonths));
		
        $monthStartDay = date('N',strtotime($year.'-'.$month.'-01'));
		
        if(($monthStartDay >= 4 && $monthStartDay < 7 && $daysInMonths==31) && $monthEndingDay < $monthStartDay)
		{
			$numOfweeks++;
        }
		//echo $numOfweeks;
		return $numOfweeks;
    }
 
    /**
    * calculate number of days in a particular month
    */
    private function _daysInMonth($month=null, $year=null)
	{
		if(null==($year))
		{
			$year =  date("Y",time());
		}
		
		if(null==($month))
		{
			$month = date("m",time());
		}
		//echo $month.'<br>';
		//echo date('t',strtotime($year.'-'.$month.'-01')).'<br>';
		return date('t',strtotime($year.'-'.$month.'-01'));
    }
	
    /**
    * Get expedited Date
    */
	private function _get_expedited_date()
	{		
		if($this->is_meltable)
		{
			#echo 1;
			$all_expedite_dates = $this->_get_all_working_dates();
		}
		else
		{
			#echo 2;
			$all_expedite_dates = $this->_get_expedited_date_region_wise();
		}
		
		return $all_expedite_dates;
		
	}
	
	/**
	** Return meltable expedited dates region wise
	*/
	private function _get_meltable_expedite_date_region_wise()
	{
		$expedite_days = null;
		$expedite_dates = array();
		$cut_off = 0;
		
		if($this->region==1)
		{			
			if(strtotime(date("Y-m-d H:i:s")) > strtotime($this->cutoff_std))
			{
				#$cut_off = $cut_off + 1;
				$expedite_days = 1;
			}
			
			if(strtotime(date("Y-m-d H:i:s")) > strtotime($this->cutoff_exp))
			{
				$cut_off = $cut_off + 1;
				$expedite_days = 1;
			}
						
			if(strtotime(date("Y-m-d H:i:s")) < strtotime($this->cutoff_std) && strtotime(date("Y-m-d H:i:s")) < strtotime($this->cutoff_exp))
			{
				$expedite_days = 0;
			}
		}
		else
		{
			if(strtotime(date("Y-m-d H:i:s")) > strtotime($this->cutoff_std))
			{
				#$cut_off = $cut_off + 1;
				$expedite_days = ($this->region - 1);
			}
			
			if(strtotime(date("Y-m-d H:i:s")) > strtotime($this->cutoff_exp))
			{
				$cut_off = $cut_off + 1;
				$expedite_days = ($this->region - 1);
			}
			
			if(strtotime(date("Y-m-d H:i:s")) < strtotime($this->cutoff_std) && strtotime(date("Y-m-d H:i:s")) < strtotime($this->cutoff_exp))
			{
				$expedite_days = ($this->region - 1);
			}
		}
		
		for($i=0; $i < $expedite_days; $i++)
		{
			$days = $i + 1;
			$new_date = date("Y-m-d", strtotime(date("Y-m-d")." +".$cut_off." day"));
			$expedite_dates[] = $this->_get_next_working_day($days, $new_date);
		}
				
		return $expedite_dates;
	}
	
	/**
	** Return epedited dates region wise
	*/
	private function _get_expedited_date_region_wise()
	{
		$expedite_days = null;
		$expedite_dates = array();
		$cut_off = 0;
		
		if($this->region==1)
		{			
			if(strtotime(date("Y-m-d H:i:s")) > strtotime($this->cutoff_std))
			{
				#$cut_off = $cut_off + 1;
				$expedite_days = 1;
			}
			
			if(strtotime(date("Y-m-d H:i:s")) > strtotime($this->cutoff_exp))
			{
				$cut_off = $cut_off + 1;
				$expedite_days = 1;
			}
						
			if(strtotime(date("Y-m-d H:i:s")) < strtotime($this->cutoff_std) && strtotime(date("Y-m-d H:i:s")) < strtotime($this->cutoff_exp))
			{
				$expedite_days = 0;
			}
		}
		else
		{
			if(strtotime(date("Y-m-d H:i:s")) > strtotime($this->cutoff_std))
			{
				#$cut_off = $cut_off + 1;
				$expedite_days = ($this->region - 1);
			}
			
			if(strtotime(date("Y-m-d H:i:s")) > strtotime($this->cutoff_exp))
			{
				$cut_off = $cut_off + 1;
				$expedite_days = ($this->region - 1);
			}
			
			if(strtotime(date("Y-m-d H:i:s")) < strtotime($this->cutoff_std) && strtotime(date("Y-m-d H:i:s")) < strtotime($this->cutoff_exp))
			{
				$expedite_days = ($this->region - 1);
			}
		}
		
		for($i=0; $i < $expedite_days; $i++)
		{
			
			$days = $i + 1;
			//echo $cut_off;exit;
			$new_date = date("Y-m-d", strtotime(date("Y-m-d")." +".$cut_off." day"));
			$expedite_dates[] = $this->_get_next_working_day($days, $new_date);
		}
				
		return $expedite_dates;
	}
		
	/**
	** Return working days
	*/
	private function _get_next_working_day($number_of_days=1, $start_date='today')
	{
		// get the start date as a string to time
		$result = strtotime($start_date);
		
		// now keep adding to today's date until number of working days is 0 and we land on a working day
		while ($number_of_days > 0)
		{
			// add one day to the start date
			$result = strtotime(date('Y-m-d',$result) . " + 1 day");
			
			// this day counts if it's a weekend and not a holiday, or if we choose to ignore holidays
			if(!in_array(date('Y-m-d',$result), $this->all_holiday_dates))
			{
				$number_of_days--;
			}
		}
		// when my $number of working days is exausted I have my final date
		return(date('Y-m-d',$result));
	}
	
	
	
	
	/**
	** Return All working dates
	*/
	private function _get_all_working_dates()
	{
		return array_diff($this->_get_next_4month_days(), $this->all_holiday_dates);
	}
	
	
	/**
    * Get Single Month Holiday dates
    */
	private function _get_month_working_days($year, $month)
	{
		$count = 0;
		$weekdays = array();
		$weekdays1 = array();
		$counter = mktime(0, 0, 0, $month, 1, $year);
		
		while(date("n", $counter) == $month)
		{
			if(in_array(date("Y-m-d", $counter), $this->all_holiday_dates) == FALSE && $counter > strtotime(date("Y-m-d")))
			{
				$weekdays[] = date('Y-m-d', mktime(0, 0, 0, $month, $count, $year));
			}
			else
			{
				$weekdays1[] = date('Y-m-d', mktime(0, 0, 0, $month, $count, $year));
			}
			$count++;
			$counter = strtotime("+1 day", $counter);
		}
		
		/*echo '<pre>';
		print_r($weekdays);
		print_r($weekdays1);
		exit;*/
		return $weekdays;
	}
	
	
	
	/**
	** get cutoff time from database
	*/
	private function _get_cutoff_time()
	{
		$result = $this->CI->db->query("
										SELECT standard_cut_off, expedite_cut_off FROM tbl_site_setting
									")->row_array();
		return $result;
		
	}
	
	
	/**
	** Return All holidays dates array including weekends and user defined holidays
	*/
	private function _get_All_HoliDays()
	{
		$holiday_listing = $this->_getHoliday();
		$holiday_date = array();
		
		if(!empty($holiday_listing))
		{
			foreach($holiday_listing as $key => $value)
			{
				$holiday_date[] = $value['cal_date'];
			}
		}
		
		$total_holidays = array_merge($holiday_date,$this->all_weekends);
		
		return $this->_bubble_Sort_Improved($total_holidays);
	}
	
	/**
    * Get all 4 Month Holiday dates (weekend only).
    */
	private function _get_4month_holidays()
	{
		$current_month_holiday = $this->_get_month_holidays(date("Y"), date("m"));
		
		$next_month_1 = date('m', strtotime('+1 month', strtotime(date('Y-m'))));
		$next_year_1 = date('Y', strtotime('+1 month', strtotime(date('Y-m'))));
		
		$number_of_holidays_1 = $this->_get_month_holidays($next_year_1, $next_month_1);
		
		$next_month_2 = date('m', strtotime('+2 month', strtotime(date('Y-m'))));
		$next_year_2 = date('Y', strtotime('+2 month', strtotime(date('Y-m'))));
		
		$number_of_holidays_2 = $this->_get_month_holidays($next_year_2, $next_month_2);
		
		$next_month_3 = date('m', strtotime('+3 month', strtotime(date('Y-m'))));
		$next_year_3 = date('Y', strtotime('+3 month', strtotime(date('Y-m'))));
		
		$number_of_holidays_3 = $this->_get_month_holidays($next_year_3, $next_month_3);
		
		
		$current_with_1 = array_merge($current_month_holiday, $number_of_holidays_1);
		
		$first_with_second = array_merge($current_with_1, $number_of_holidays_2);
		
		$final_holiday_array = array_merge($first_with_second, $number_of_holidays_3);
		
		return $final_holiday_array;
		
	}
	
	/**
    * Get Single Month Holiday dates
    */
	private function _get_month_holidays($year, $month)
	{
		$count = 0;
		$weekend_holiday = array();
		$counter = mktime(0, 0, 0, $month, 1, $year);
		
		if($this->is_meltable)
		{
			$holidays = array(0,1,2);
		}
		else
		{
			$holidays = array(0,1);
		}
		
		while(date("n", $counter) == $month)
		{
			if (in_array(date("w", $counter), $holidays) == TRUE)
			{
				$weekend_holiday[] = date('Y-m-d',mktime(0, 0, 0, $month, $count, $year));
			}
			$count++;
			$counter = strtotime("+1 day", $counter);
		}
		
		$weekend_array = array();
		
		if(date("m") == $month)
		{
			foreach($weekend_holiday as $value)
			{
				if($value > date("Y-m-d"))
				{
					$weekend_array[] = $value;
				}
			}
		}
		else
		{
			$weekend_array = $weekend_holiday;
		}
				
		return $weekend_array;
	}
	
	
	/**
    * Get Next 4 Month dates
    */
	private function _get_next_4month_days()
	{
		$cur_mon_rem_days = date('t') - date('j');
		
		$next_month_1 = date('m', strtotime('+1 month', strtotime(date('Y-m'))));
		$next_year_1 = date('Y', strtotime('+1 month', strtotime(date('Y-m'))));
		
		$number_of_days_1 = $this->_daysInMonth($next_month_1, $next_year_1);
		
		$next_month_2 = date('m', strtotime('+2 month', strtotime(date('Y-m'))));
		$next_year_2 = date('Y', strtotime('+2 month', strtotime(date('Y-m'))));
		
		$number_of_days_2 = $this->_daysInMonth($next_month_2, $next_year_2);
		
		$next_month_3 = date('m', strtotime('+3 month', strtotime(date('Y-m'))));
		$next_year_3 = date('Y', strtotime('+3 month', strtotime(date('Y-m'))));
		
		$number_of_days_3 = $this->_daysInMonth($next_month_3, $next_year_3);
		
		$total_num_of_days = $cur_mon_rem_days + $number_of_days_1 + $number_of_days_2 + $number_of_days_3;
		
		return $this->_dates_Of_NextWeek($total_num_of_days);
	}
	
	/**
    * Get next week dates
    */	
	private function _dates_Of_NextWeek($number_of_days)
	{
		$dates = array();
		$date = time();									// get current date.
		while(date('w', $date += 86400) == 1);			// find the next Monday.
		for($i = 0; $i < $number_of_days; $i++)			// get the 7 dates from it.
		{
			$dates[] = date('Y-m-d', $date + $i * 86400);
		}
		return $dates;
	}
	
	/**
    * Get holiday array (user defined array)
    */
	private function _getHoliday()
	{
		$query = $this->CI->db->query("
										SELECT * FROM tbl_calender
										WHERE
										is_active = 1
										AND
										cal_date >= DATE_FORMAT(CURDATE(), '%Y-%m-%d')
										AND
										cal_date <= DATE_FORMAT(CURDATE(), '%Y-%m-01') + INTERVAL 4 MONTH
									");
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
	/**
    * Array Bubble Sort function
    /
	private function array_sort($arr)
	{
		$size = count($arr);
		for ($i=0; $i<$size; $i++) {
			for ($j=0; $j<$size-1-$i; $j++) {
				if ($arr[$j+1] < $arr[$j]) {
					$this->array_index_swap($arr, $j, $j+1);
				}
			}
		}
		return $arr;
	}
	
	/**
    * Array Swap function
    /
	private function array_index_swap(&$arr, $a, $b)
	{
		$tmp = $arr[$a];
		$arr[$a] = $arr[$b];
		$arr[$b] = $tmp;
	}*/
	
	/**
    * Array Bubble Sort improved function
    */
	private function _bubble_Sort_Improved(array $arr)
	{
		$n = sizeof($arr);    
		for($i = 1; $i < $n; $i++)
		{
			$flag = false;
			for($j = $n - 1; $j >= $i; $j--)
			{
				if($arr[$j-1] > $arr[$j])
				{
					$tmp = $arr[$j - 1];
					$arr[$j - 1] = $arr[$j];
					$arr[$j] = $tmp;
					$flag = true;
				}
			}
			if(!$flag)
			{
				break;
			}
		}
	
		return $arr;
	}
	
	/**
    * Get Region No
    */
	private function _get_region_no($zip)
	{
		$this->CI->db->select('*')->from('tbl_region_zip')->where(array('rp_zipcode' => $zip));
		$zip_region = $this->CI->db->get()->row_array();
		
		if(count($zip_region) > 0)
		{
			return $zip_region['rp_region'];
		}
		else
		{
			return 7;
		}
		
	}
	
	
	private function _get_holiday_color($holiday)
	{
		$this->CI->db->select('*')->from('tbl_calender')->where(array('cal_date'=>$holiday,'is_active'=>'1'));
		return $this->CI->db->get()->row_array();
	}
	
	private function _get_product_zip_region_price($data = array())
	{
		
		$region_no = $this->region;
		
		if($region_no == 1)
		{
			$this->CI->db->select('prod_region_one')->from('tbl_product_region_prices')->where(array('prod_region_prod_id'=>$data['prod_id']));
			$result_price = $this->CI->db->get()->result_array();
			$price[0] = $result_price[0]['prod_region_one'];
			$price[1] = $result_price[1]['prod_region_one'];
			$price[2] = $result_price[2]['prod_region_one'];
			return $price;
		}
		else if($region_no == 2)
		{
			$this->CI->db->select('prod_region_two, prod_region_type')->from('tbl_product_region_prices')->where(array('prod_region_prod_id'=>$data['prod_id']));
			$result_price = $this->CI->db->get()->result_array();
			$price[0] = $result_price[0]['prod_region_two'];
			$price[1] = $result_price[1]['prod_region_two'];
			$price[2] = $result_price[2]['prod_region_two'];
			return $price;

		}
		else if($region_no == 3)
		{
			$this->CI->db->select('prod_region_three, prod_region_type')->from('tbl_product_region_prices')->where(array('prod_region_prod_id'=>$data['prod_id']));
			$result_price = $this->CI->db->get()->result_array();
			$price[0] = $result_price[0]['prod_region_three'];
			$price[1] = $result_price[1]['prod_region_three'];
			$price[2] = $result_price[2]['prod_region_three'];
			return $price;
		}
		else if($region_no == 4)
		{
			$this->CI->db->select('prod_region_four, prod_region_type')->from('tbl_product_region_prices')->where(array('prod_region_prod_id'=>$data['prod_id']));
			$result_price = $this->CI->db->get()->result_array();
			$price[0] = $result_price[0]['prod_region_four'];
			$price[1] = $result_price[1]['prod_region_four'];
			$price[2] = $result_price[2]['prod_region_four'];
			return $price;
		}
		else if($region_no == 5)
		{
			$this->CI->db->select('prod_region_five, prod_region_type')->from('tbl_product_region_prices')->where(array('prod_region_prod_id'=>$data['prod_id']));
			$result_price = $this->CI->db->get()->result_array();
			$price[0] = $result_price[0]['prod_region_five'];
			$price[1] = $result_price[1]['prod_region_five'];
			$price[2] = $result_price[2]['prod_region_five'];
			return $price;
		}
		else if($region_no == 6)
		{
			$this->CI->db->select('prod_region_six, prod_region_type')->from('tbl_product_region_prices')->where(array('prod_region_prod_id'=>$data['prod_id']));
			$result_price = $this->CI->db->get()->result_array();
			$price[0] = $result_price[0]['prod_region_six'];
			$price[1] = $result_price[1]['prod_region_six'];
			$price[2] = $result_price[2]['prod_region_six'];
			return $price;
		}
		elseif($region_no == 7)
		{
			$this->CI->db->select('prod_region_seven, prod_region_type')->from('tbl_product_region_prices')->where(array('prod_region_prod_id'=>$data['prod_id']));
			$result_price = $this->CI->db->get()->result_array();
			
			$price[0] = $result_price[0]['prod_region_seven'];
			$price[1] = $result_price[1]['prod_region_seven'];
			$price[2] = $result_price[2]['prod_region_seven'];
			return $price;
		}
		
		
	}
	
	private function _get_product_data($prod_id)
	{
		$this->CI->db->select('*')->from('tbl_product')->where('prod_id = '.$prod_id);
		return $this->CI->db->get()->row_array();
	}
	
	
	
	
}
