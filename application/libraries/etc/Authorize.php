<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

// autoload composer 
require 'vendor/autoload.php';


class Authorize {
	
	/********************* PROPERTY ********************/  
	private $CI;
	
	private $auth;
	
	private $mode;
	
	private $refId;
	
	private $api_mode;
	
	
	/********************* PUBLIC **********************/  
	
	/**
	* Constructor
	*/
	public function __construct()
	{
		$this->CI =& get_instance();
		
		// create or update log file
		define("AUTHORIZENET_LOG_FILE", "authorizenet_log");
		
		// Common setup for API credentials (merchant)
		$this->auth = new AnetAPI\MerchantAuthenticationType();
		
		//5KP3u95bQpv
		//4Ktq966gC55GAX7S
		
		//login=5KP3u95bQpv
		//Transaction-key=4Ktq966gC55GAX7S
		//=================================
		//login=7uKQ68g7kdF7
		//Transaction-key=2PAL4Pz63yF5x8CA
		//=================================
		//login=7uKQ68g7kdF7
		//Transaction-key=36b4SD5wKhmx9G9F
		//=================================
		//login=7Mw9V9bY
		//Transaction-key=7WGk945Lk6WE5Cer
		//=================================
		// Live Credentials Clients
		//=================================
		//Login=65Fw7bfLjs
		//Transaction-key=42dB9e6qvVx9y7Rb
		//=================================
		//=================================
		
		//$this->auth->setName("7Mw9V9bY");
		//$this->auth->setTransactionKey("7WGk945Lk6WE5Cer");
		
		// authorize validation process mode type //testMode // liveMode
		$this->mode = "testMode";
		
		// reference id
		$this->refId = 'ref'.time(microtime().microtime());
		
		if($this->mode=="liveMode")
		{
			$this->auth->setName("65Fw7bfLjs");
			$this->auth->setTransactionKey("42dB9e6qvVx9y7Rb");
			
			$this->api_mode = \net\authorize\api\constants\ANetEnvironment::PRODUCTION;
		}
		else
		{
			$this->auth->setName("7Mw9V9bY");
			$this->auth->setTransactionKey("7WGk945Lk6WE5Cer");
			
			$this->api_mode = \net\authorize\api\constants\ANetEnvironment::SANDBOX;
		}
		
	}
	
	/**
	* Create Payment Profile 
	*/
	public function create_payment_profile($data)
	{
		//Use an existing profile id
		$existingcustomerprofileid = $data['customer_id'];
		
		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($data['card_no']);
		$creditCard->setExpirationDate($data['card_exp']);
		$creditCard->setCardCode($data['card_cvc']);
		
		$paymentCreditCard = new AnetAPI\PaymentType();
		$paymentCreditCard->setCreditCard($creditCard);
		
		// Create a new Customer Payment Profile
		$paymentprofile = new AnetAPI\CustomerPaymentProfileType();
		$paymentprofile->setCustomerType('individual');
		$paymentprofile->setPayment($paymentCreditCard);
		
		$paymentprofiles[] = $paymentprofile;
		
		// Submit a CreateCustomerPaymentProfileRequest to create a new Customer Payment Profile
		$paymentprofilerequest = new AnetAPI\CreateCustomerPaymentProfileRequest();
		$paymentprofilerequest->setMerchantAuthentication($this->auth);
		$paymentprofilerequest->setCustomerProfileId( $existingcustomerprofileid );
		$paymentprofilerequest->setPaymentProfile( $paymentprofile );
		$paymentprofilerequest->setValidationMode($this->mode);
		
		$controller = new AnetController\CreateCustomerPaymentProfileController($paymentprofilerequest);
		$response = $controller->executeWithApiResponse($this->api_mode);
				
		if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") )
		{
			return $response->getCustomerPaymentProfileId();
		}
		else
		{
			return $response->getMessages()->getMessage();
			//echo "Create Customer Payment Profile: ERROR Invalid response\n";
			//echo "Response : " . $response->getMessages()->getMessage()[0]->getCode() . "  " .$response->getMessages()->getMessage()[0]->getText() . "\n";
		}
		
		return false;

	}
	
	
	/**
	* Create Customer Profile with payment profile 
	*/
	public function create_customer_profile($data)
	{
		
		// Create the payment data for a credit card
		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($data['card_no']);
		$creditCard->setExpirationDate($data['card_exp']);
		$creditCard->setCardCode($data['card_cvc']);
		
		$paymentCreditCard = new AnetAPI\PaymentType();
		$paymentCreditCard->setCreditCard($creditCard);
		
		// Create a Customer Profile Request
		//  1. create a Payment Profile
		//  2. create a Customer Profile   
		//  3. Submit a CreateCustomerProfile Request
		//  4. Validate Profile ID returned
		
		$paymentprofile = new AnetAPI\CustomerPaymentProfileType();
		$paymentprofile->setCustomerType('individual');
		$paymentprofile->setPayment($paymentCreditCard);
		
		$paymentprofiles[] = $paymentprofile;
		
		$customerprofile = new AnetAPI\CustomerProfileType();
		$customerprofile->setDescription("");
		$merchantCustomerId = time().rand(1,150);
		$customerprofile->setMerchantCustomerId($merchantCustomerId);
		$customerprofile->setEmail($data['email']);
		$customerprofile->setPaymentProfiles($paymentprofiles);
				
		/*$paymentprofilerequest = new AnetAPI\CreateCustomerPaymentProfileRequest();
		$paymentprofilerequest->setMerchantAuthentication($this->auth);
		$paymentprofilerequest->setCustomerProfileId($existingcustomerprofileid);
		$paymentprofilerequest->setPaymentProfile($paymentprofile);
		$paymentprofilerequest->setValidationMode($this->mode);*/
		
		
		$request = new AnetAPI\CreateCustomerProfileRequest();
		$request->setMerchantAuthentication($this->auth);
		$request->setRefId($this->refId);
		$request->setProfile($customerprofile);
		
		$controller = new AnetController\CreateCustomerProfileController($request);
		$response = $controller->executeWithApiResponse($this->api_mode);
		
		if(($response != null) && ($response->getMessages()->getResultCode() == "Ok"))
		{
			$return_data = array(
									'cutomer_profile_id' => $response->getCustomerProfileId(),
								);
			$payment_profile = $response->getCustomerPaymentProfileIdList();
			$return_data['payment_profile_id'] = $payment_profile[0];
			
			return $return_data;
			
			//echo "SUCCESS: PROFILE ID : " . $response->getCustomerProfileId() . "\n";
			//echo "SUCCESS: PAYMENT PROFILE ID : " . $response->getCustomerPaymentProfileIdList()[0] . "\n";
		}
		else
		{
			return $response->getMessages()->getMessage();
			
			//echo "ERROR :  Invalid response\n";
			//echo "Response : " . $response->getMessages()->getMessage()[0]->getCode() . "  " .$response->getMessages()->getMessage()[0]->getText() . "\n";
		  
		}
		//return $response;
	}

	/**
	*	Validate payment profile
	*/
	public function validate_payment_profile()
	{
		// An existing payment profile ID for this Merchant name and Transaction key
		//
		$customerprofileid = "37680862";
		$customerpaymentprofileid = "34249159";
		
		$request = new AnetAPI\ValidateCustomerPaymentProfileRequest();
		$request->setMerchantAuthentication($this->auth);
		$request->setCustomerProfileId($customerprofileid);
		$request->setCustomerPaymentProfileId($customerpaymentprofileid);
		$request->setValidationMode($this->mode);
		
		$controller = new AnetController\ValidateCustomerPaymentProfileController($request);
		$response = $controller->executeWithApiResponse($this->api_mode);
		
		echo '<pre>';
		print_r($response);
		exit;
		
		if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") )
		{
			//echo $response->getMessages()->getMessage()[0]->getText();
		}
		else
		{
			echo "ERROR :  Validate Customer Payment Profile: Invalid response\n";
			//echo "Response : " . $response->getMessages()->getMessage()[0]->getCode() . "  " .$response->getMessages()->getMessage()[0]->getText() . "\n";
		}
	}
	
	/**
	* Get Customer Payment profiles
	*/
	public function get_customer_profile_listing($cProfileId = "36731856", $cPaymentProId = "33211899")
	{
		$data_array = array();
		
		if(!is_numeric($cProfileId) || !is_numeric($cPaymentProId))
		{
			return false;
		}
		
		//Setting the paging
		$paging = new AnetAPI\PagingType();
		$paging->setLimit("1000");
		$paging->setOffset("1");
		
		//Setting the sorting
		$sorting = new AnetApi\CustomerPaymentProfileSortingType();
		$sorting->setOrderBy("id");
		$sorting->setOrderDescending("false");
		
		//Creating the request with the required parameters
		$request = new AnetAPI\GetCustomerPaymentProfileListRequest();
		$request->setMerchantAuthentication($this->auth);
		$request->setRefId($this->refId);
		$request->setPaging($paging);
		$request->setSorting($sorting);
		$request->setSearchType("cardsExpiringInMonth");
		$request->setMonth("2020-12");
		
		// Controller
		$controller = new AnetController\GetCustomerPaymentProfileListController($request);
		// Getting the response
		$response = $controller->executeWithApiResponse($this->api_mode);
		
		if(($response != null))
		{
			if($response->getMessages()->getResultCode() == "Ok")
			{
				// Success
				//echo "GetCustomerPaymentProfileList SUCCESS: " . "\n";
				//echo "Message Code: " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
				//echo "Message Text: " . $response->getMessages()->getMessage()[0]->getText() . "\n";
				//echo "Total number of Results in the result set" . $response->getTotalNumInResultSet() . "\n";
				// Displaying the customer payment profile list 
				foreach($response->getPaymentProfiles() as $paymentProfile)
				{
					echo "\nCustomer Profile id: " . $paymentProfile->getCustomerProfileId() . "\n";
					echo "Payment profile id: " . $paymentProfile->getCustomerPaymentProfileId() . "\n";
					echo "Credit Card Number: " . $paymentProfile->getPayment()->getCreditCard()->getCardNumber() . "\n";
					if($paymentProfile->getBillTo() != null)
					{
						echo "First Name in Billing Address: " . $paymentProfile->getBillTo()->getFirstName() . "\n";
					}
				}
			}
			else
			{
				// Error
				//echo "GetCustomerPaymentProfileList ERROR :  Invalid response\n";
				//echo "Response : " . $response->getMessages()->getMessage()[0]->getCode() . "  " .$response->getMessages()->getMessage()[0]->getText() . "\n";
			}
		}
		else
		{
			// Failed to get the response
			echo "Failed to get Response Error";
		}
		return $response;
		
	}
	
	
	/**
	* Get Customer Profile
	*/
	public function get_customer_profile($cProfileId = "36731856")
	{
		$data_array = array();
		
		if(!is_numeric($cProfileId))
		{
			return false;
		}
		
		//request requires customerProfileId
		$request = new AnetAPI\GetCustomerProfileRequest();
		$request->setMerchantAuthentication($this->auth);
		$request->setCustomerProfileId($cProfileId);
		
		$controller = new AnetController\GetCustomerProfileController($request);
		$response = $controller->executeWithApiResponse($this->api_mode);
		
		$return_array = array();
		
		if(($response != null))
		{
			if($response->getMessages()->getResultCode() == "Ok")
			{
				$res_pro = $response->getprofile()->getpaymentProfiles();
				
				for($i = 0; $i <= count($res_pro); $i++)
				{
					$value = $res_pro[$i];
					$return_array[$i]['payment_profile_id'] = $value->getcustomerPaymentProfileId();
					$min_len = strlen($value->getpayment()->getcreditCard()->getcardNumber())-4;
					$return_array[$i]['card_number'] = subtr($value->getpayment()->getcreditCard()->getcardNumber(), $min_len);
				}
				
				return $return_array;
				//echo "GetCustomerPaymentProfile SUCCESS: " . "\n";
				//echo "Customer Payment Profile Id: " . $response->getPaymentProfile()->getCustomerPaymentProfileId() . "\n";
			}
			else
			{
				return $response->getMessages()->getMessage();
				//echo "GetCustomerPaymentProfile ERROR :  Invalid response\n";
				//echo "Response : " . $response->getMessages()->getMessage()[0]->getCode() . "  " .$response->getMessages()->getMessage()[0]->getText() . "\n";
			}
		}
		else
		{
			return "Failed to get Response Error";
		}
		
	}
	
	
	public function delete_payment_profile($data)
	{
		// An existing payment profile ID for this Merchant name and Transaction key
		//
		$customerprofileid = $data['customer_pro_id'];
		$customerpaymentprofileid = $data['cus_payment_pro_id'];
		
		$request = new AnetAPI\DeleteCustomerPaymentProfileRequest();
		$request->setMerchantAuthentication($this->auth);
		$request->setCustomerProfileId($customerprofileid);
		$request->setCustomerPaymentProfileId($customerpaymentprofileid);
		$controller = new AnetController\DeleteCustomerPaymentProfileController($request);
		$response = $controller->executeWithApiResponse($this->api_mode);
		/*echo '<pre>';
		print_r($response);
		exit;*/
		if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") )
		{
			return "SUCCESS";
		}
		else
		{
			$msg = $response->getMessages()->getMessage();
			return $msg[0]->getText();
			
		}		
	}
	
	
	/*
	*	Charged through customer profile
	*/
	public function charge_customer_profile($data)
	{
		
		$customerprofileid = $data['customer_pro_id'];
		$customerpaymentprofileid = $data['cus_payment_pro_id'];
		
		$profileToCharge = new AnetAPI\CustomerProfilePaymentType();
		$profileToCharge->setCustomerProfileId($customerprofileid);
		$paymentProfile = new AnetAPI\PaymentProfileType();
		$paymentProfile->setPaymentProfileId($customerpaymentprofileid);
		$profileToCharge->setPaymentProfile($paymentProfile);
		
		$transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType( "authCaptureTransaction"); 
		$transactionRequestType->setAmount($data['total_amount']);
		$transactionRequestType->setProfile($profileToCharge);
		
		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($this->auth);
		$request->setRefId($this->refId);
		$request->setTransactionRequest($transactionRequestType);
		
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse($this->api_mode);
		
		/*echo '<pre>';
		print_r($response);
		exit;*/
		
		if($response != null)
		{
			$tresponse = $response->getTransactionResponse();
			if(($tresponse != null) && ($tresponse->getResponseCode()=="1") )   
			{
				$response_array = array();
				$response_array['auth_code'] = $tresponse->getAuthCode();
				$response_array['auth_transaction_id'] = $tresponse->getTransId();
				return $response_array;
			}
			else
			{
				$message = $response->getMessages()->getMessage();
				return $message[0]->getText();
			}
			/*elseif(($tresponse != null) && ($tresponse->getResponseCode()=="2") )
			{
				return "Your Request is Declined.";
			}
			elseif(($tresponse != null) && ($tresponse->getResponseCode()=="4") )
			{
				return "Your Request is held for Reveiw.";
			}
			else
			{
				$message = $response->getMessages()->getMessage();
				return $message[0]->getText();
			}*/
		}
		else
		{
			return "No response returned.";
		}
		
	}
	
	
	public function charge_credit_card($data)
	{
		
		// Create the payment data for a credit card
		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($data['card_no']);
		$creditCard->setExpirationDate($data['card_exp']);
		$creditCard->setCardCode($data['card_cvc']);
		
		$paymentCreditCard = new AnetAPI\PaymentType();
		$paymentCreditCard->setCreditCard($creditCard);
		
		// Customer info 
		$customer = new AnetAPI\CustomerDataType();
		$customer->setId($data['user_id']);
		$customer->setEmail($data['email']);
		
		//create a transaction
		$transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType( "authCaptureTransaction"); 
		$transactionRequestType->setAmount($data['amount']);
		$transactionRequestType->setPayment($paymentCreditCard);
		
		$transactionRequestType->setCustomer($customer);
		
		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($this->auth);
		$request->setRefId($this->refId);
		$request->setTransactionRequest($transactionRequestType);
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse($this->api_mode);
		
		if ($response != null)
		{
			$tresponse = $response->getTransactionResponse();
			
			if (($tresponse != null) && ($tresponse->getResponseCode()=="1") )   
			{
				$response_array = array();
				$response_array['auth_code'] = $tresponse->getAuthCode();
				$response_array['auth_transaction_id'] = $tresponse->getTransId();
				return $response_array;
			}
			else
			{
				$message = $response->getMessages()->getMessage();
				return $message[0]->getText();
			}
		
		}
		else
		{
			return "Charge Credit card Null response returned";
		}

	}
	
	
	public function authorize_card($data)
	{
		// Create the payment data for a credit card
		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($data['card_no']);
		$creditCard->setExpirationDate($data['card_exp']);
		$creditCard->setCardCode($data['card_cvc']);
		
		$paymentCreditCard = new AnetAPI\PaymentType();
		$paymentCreditCard->setCreditCard($creditCard);
		
		//create a transaction
		$transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType("authOnlyTransaction"); 
		$transactionRequestType->setAmount($data['amount']);
		$transactionRequestType->setPayment($paymentCreditCard);
		
		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($this->auth);
		$request->setRefId($this->refId);
		$request->setTransactionRequest($transactionRequestType);
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse($this->api_mode);
		
		/*echo '<pre>';
		print_r($response);
		exit;*/
		
		if ($response != null)
		{
			$tresponse = $response->getTransactionResponse();
			
			if (($tresponse != null) && ($tresponse->getResponseCode()=="1") )   
			{
				$response_array = array();
				$response_array['auth_code'] = $tresponse->getAuthCode();
				$response_array['auth_transaction_id'] = $tresponse->getTransId();
				return $response_array;
			}
			else
			{
				$errors = $tresponse->geterrors();
				
				if(is_array($errors) && !empty($errors))
				{
					return $errors[0]->geterrorText();
				}
				else
				{
					$message = $response->getMessages()->getMessage();
					return $message[0]->getText();
				}
			}
			
		}
		else
		{
			return "Charge Credit card Null response returned";
		}
	}
	
	
	public function capture_authorize_card($data)
	{
		// Create the payment data for a credit card
		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($data['card_no']);
		$creditCard->setExpirationDate($data['card_exp']);
		$creditCard->setCardCode($data['card_cvc']);
		
		$paymentCreditCard = new AnetAPI\PaymentType();
		$paymentCreditCard->setCreditCard($creditCard);
		
		//  First Authorize the amount
		$transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType("priorAuthCaptureTransaction"); 
		$transactionRequestType->setAmount($data['amount']);
		$transactionRequestType->setPayment($paymentCreditCard);
		
		
		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($this->auth);
		$request->setTransactionRequest($transactionRequestType);
		
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse($this->api_mode);
		
		$authTransId = "0";
		$response_array = array();
		
		if($response != null)
		{
			$tresponse = $response->getTransactionResponse();
			$authTransId = $tresponse->getTransId();
			
			if(($tresponse != null) && ($tresponse->getResponseCode()=="1") )
			{
				$response_array['auth_code'] = $tresponse->getAuthCode();
				$authTransId;
			}
			else
			{
				return "Authorization : Invalid response";
			}
		}
		else
		{
			return "Authorization NULL Response Error";
		}
		
		// Now capture the previously authorized  amount
		//echo "Capturing the Authorization with transaction ID : " . $authTransId . "\n";
		$transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType("priorAuthCaptureTransaction");
		$transactionRequestType->setAmount($data['amount']);
		$transactionRequestType->setRefTransId($authTransId);
		
		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($this->auth);
		$request->setTransactionRequest($transactionRequestType);
		
		$controller = new AnetController\CreateTransactionController($request);
		$response = $controller->executeWithApiResponse($this->api_mode);
		
		if ($response != null)
		{
			$tresponse = $response->getTransactionResponse();
			if (($tresponse != null) && ($tresponse->getResponseCode()=="1") )
			{
				$response_array['auth_transaction_id'] = $tresponse->getTransId();
				return $response_array;
			}
			else
			{
				return " Capture Previously Authorized Amount: Invalid response";
			}
		}
		else
		{
			return "Capture Previously Authorized Amount NULL Response Error";
		}	
	}
	
	
	
}





