<?php
// autoload composer
require 'vendor/autoload.php';

use Ctct\ConstantContact;
use Ctct\Components\Contacts\Contact;
use Ctct\Components\Contacts\ContactList;
use Ctct\Components\Contacts\EmailAddress;
use Ctct\Exceptions\CtctException;

class Constant_contact {
	
	public function __construct()
	{
		
	}
	
	public function add_user_data($data = array())
	{
		/*define("APIKEY", "vnkn3fgsmd3v48nyb727y4ak");
		define("ACCESS_TOKEN", "245c7949-d9c1-40a2-b94a-be70525c3016");*/
		
		/*define("APIKEY", "6gxmcp4868mx2866jmmymgqb");
		define("ACCESS_TOKEN", "9642bc8a-ccc9-409e-98d9-5adba4a2c44c");*/
		
		// Live Client Account
		define("APIKEY", "j6v4s36nk86qtbd9tybya5bd");
		define("ACCESS_TOKEN", "260d47b8-d94b-4757-bdd4-1cc4e95e2fde");
		// live account ends
		
		$cc = new ConstantContact(APIKEY);
		
		//$firstName = $data['fname'];
		//$lastName = $data['lname'];
		$emailAddress = $data['email'];
		
		#$response = $cc->contactService->getContacts(ACCESS_TOKEN);
		#echo'<pre>';
		#print_r($response);
		#echo '<hr>';
		
		
		$lists = $cc->listService->getLists(ACCESS_TOKEN);
		#print_r($lists);
		#echo '<hr>';
		
		#Get the list id in which contact will add.
		#In this case we will add contact in first list.
		$listId = $lists[0]->id; 
		
		
		$get_data = $cc->contactService->getContacts(ACCESS_TOKEN, array("email" => $emailAddress));
		#print_r($get_data);
		#echo '<hr>';
		
		if(empty($get_data->results))
		{
			$action = "Creating Contact";
			
			$contact = new Contact();
			#$contact->first_name = $firstName;
            #$contact->last_name = $lastName;
			$contact->addEmail($emailAddress);
			$contact->addList($listId);
            
            $returnContact = $cc->contactService->addContact(ACCESS_TOKEN, $contact, false);
			
			if($returnContact)
			{
				#echo "Contact Added. This is your newly created contact's information<br /><pre>";
				#print_r($returnContact);
				return $returnContact->email_addresses['0']->id;
				#echo "</pre>";
			}
		}
		else
		{
    		#echo "Contact already exist.";
			return false;
		}

	}
}