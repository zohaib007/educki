<?php
ini_set('memory_limit', '2560M');

class Calendar {  

    /**
     * Constructor
     */
	
	/********************* PROPERTY ********************/  
    private $dayLabels = array("Mon","Tue","Wed","Thu","Fri","Sat","Sun");
	     
    private $currentYear = 0;
		   
    private $currentMonth = 0;
		 
    private $currentDay = 0;
	
    private $currentDate = null;
		
    private $daysInMonth = 0;
		 
    private $naviHref = null;
		
	private $CI;
	
	private $call_one = 0;
	
	private $is_loop = 1;
	
	private $exp_day = 1;
		
    /********************* PUBLIC **********************/  
	
	public function __construct()
	{
		
		$this->CI =& get_instance();
		
		$this->naviHref = base_url('etc/test/calender');//htmlentities($_SERVER['PHP_SELF']);
		
		
		$this->CI->load->helper('date');
		
		$human = unix_to_human(now());
		days_in_month(06, 2005);
		
		
    }
    /**
    * print out the calendar
    */
    public function show($data = array(), $data2 = array())
	{
		if($data['nloop'] != '')
		{
			$this->is_loop = $data['nloop'];
		}
		if($data['month'] == date('n') )
		{
			$this->is_loop = 1;
		}
		
		$region_no = $this->get_region_no($data2);
		
		if(empty($region_no))
		{
			return false;
		}
		
		$year  = null;
        $month = null;
        
		if(null==$year && isset($data['year']) && !empty($data['year']))
		{
			$year = $data['year'];
        }
		else if(null==$year)
		{
			$year = date("Y",time());
        }
		
        if(null==$month && isset($data['month']) && !empty($data['month']))
		{
			$month = $data['month'];
         
        }
		else if(null==$month)
		{
			$month = date("m",time());
        }
         
        $this->currentYear = $year;
		
		$this->currentMonth = $month;
		
        $this->daysInMonth = $this->_daysInMonth($month,$year);
        
		$holiday_listing = $this->_getHoliday();
		
		
		$content = 
					'<div class="box-content">'.
						'<ul class="label">'.$this->_createLabels().'</ul>';   
						$content.='<div class="clear"></div>';     
						$content.='<ul class="dates">';    
						 
						$weeksInMonth = $this->_weeksInMonth($month,$year);
						
						// Create weeks in a month
						for( $i=0; $i<$weeksInMonth; $i++ ){
							//Create days in a week
							for($j=1;$j<=7;$j++){
								$content.=$this->_showDay($i*7+$j, $holiday_listing, $j, $data2 );
							}
						}
						$content.='</ul>';                                 
			$content.='<div class="clear"></div>';     
	
		$content.='</div>';
		
		
		//var_dump($this->is_loop);
		
		if($region_no == 1)
		{
			$top_nav = $this->_createNavi_1($region_no);
		}
		else if($region_no == 2)
		{
			$top_nav = $this->_createNavi_2($region_no);
		}
		else if($region_no == 3)
		{
			$top_nav = $this->_createNavi_3($region_no);
		}
		else
		{
			echo "other region : ".$region_no;
		}
		$content_exp = '<div class="box">'.	$top_nav. '</div>'. $content;
 		
        return $content_exp;   
    }
     
    /********************* PRIVATE **********************/ 
    /**
    * create the li element for ul
    */
    private function _showDay($cellNumber, $holiday_listing = '', $off_cell, $data2)
	{
		$prod_region_price = $this->get_product_zip_region_price($data2);
		$region_no = $this->get_region_no($data2);
		$prod_data = $this->get_product_data($data2['prod_id']);
		$cut_off = $this->get_cutoff_time();
		
		echo $this->currentDay.'<br/>';
		//echo $cellNumber;
		if($this->currentDay == 0)
		{
			$firstDayOfTheWeek = date('N',strtotime($this->currentYear.'-'.$this->currentMonth.'-01'));
            
			if(intval($cellNumber) == intval($firstDayOfTheWeek))
			{
				$this->currentDay = 1;
            }
        }
		
        if(($this->currentDay!=0)&&($this->currentDay<=$this->daysInMonth))
		{
			$this->currentDate = date('Y-m-d',strtotime($this->currentYear.'-'.$this->currentMonth.'-'.($this->currentDay)));
            $cellContent = $this->currentDay;
            $this->currentDay++;   
             
        }
		else
		{
			$this->currentDate = null;
            $cellContent = null;
        }
		
		$holiday_date = array();
		
		if(!empty($holiday_listing))
		{
			foreach($holiday_listing as $key => $value)
			{
				$holiday_date[] = $value['cal_date'];
			}
		}
		
		//echo 'cut off date : '.$cut_off_date;
		//echo $this->currentDate.' : '.$off_day.'<br/><br/>';
		//echo date('Y-m-d', strtotime('+1 day')).'<br>';
		//echo  date('H:i:s', time()).'<br>'.$cut_off['standard_cut_off'].'<br>';
		//echo $this->currentDate.'<br>';
		/*echo '<pre>';
		echo  date('H:i:s', time());
		exit;*/
				
		# Region Two Cutoff Time START
		$stnd_cut_off_date = '';
		$expd_cut_off_date = '';
		if($this->call_one == 0 && $this->currentDate != '' && $this->currentDate > date('Y-m-d') )
		{
			//echo 'Before Function call : '.$this->currentDate.' ';
			$stnd_cut_off_date = $this->get_standard_cut_off_date($this->currentDate, $off_cell, $holiday_date, $cut_off);
			$expd_cut_off_date = $this->get_expedite_cut_off_date($this->currentDate, $off_cell, $holiday_date, $cut_off, $stnd_cut_off_date);
			//echo "<br>Exp value: ". $expd_cut_off_date;
			$this->call_one = 1;
		}
		# Region Two Cutoff Time End
		
		
		if($prod_data['prod_is_metable'] == 0)
		{
			// If product is non-meltable.
			if($data2['country'] == 'US')
			{
				if($region_no == '1')
				{
					# Region One Cutoff Time START
					$cut_off_date = '';
					if($this->call_one == 0 && $this->currentDate != '' && strtotime($this->currentDate) > strtotime(date('Y-m-d')) )
					{
						//echo 'Before Function call : '.$this->currentDate.' ';
						$cut_off_date = $this->get_standard_cut_off_date($this->currentDate, $off_cell, $holiday_date, $cut_off);
						$this->call_one = 1;
					}
					# Region One Cutoff Time End

					return $this->display_non_meltable_calendar_r1($this->currentDate, $off_cell, $holiday_date, $cellNumber, $cellContent, $cut_off_date, $region_no, $prod_region_price, $prod_data);
				}
				else if($region_no == '2')
				{
					return $this->display_non_meltable_calendar_r2($this->currentDate, $off_cell, $holiday_date, $cellNumber, $cellContent, $stnd_cut_off_date, $expd_cut_off_date, $region_no, $prod_region_price, $prod_data);
				}
				else if($region_no == '3')
				{
					
					return $this->display_non_meltable_calendar_r3($this->currentDate, $off_cell, $holiday_date, $cellNumber, $cellContent, $stnd_cut_off_date, $expd_cut_off_date, $region_no, $prod_region_price, $prod_data);
				}
				else if($region_no == '4')
				{
					return 4;
				}
				else if($region_no == '5')
				{
					return 5;
				}
				else if($region_no == '6')
				{
					return 6;
				}
				else
				{
					return false;
				}
			}
			else if ($data2['country'] == 'CA')
			{
				return 7;
			}
		}
		else if ($prod_data['prod_is_metable'] == 1)
		{
			// If product is meltable.
			
			return "asasas";
			//$this->display_meltable_calendar($this->currentDate, $off_cell, $holiday_date, $cellNumber, $cellContent);
		}
	}
	
	#########################################################
	private function _get_expedited_date($current_date, $region_no, $holiday_list, $cut_off_time)
	{
		if($country == 'US')
		{
			if($region_no=='1')
			{
				date('Y-m-d', strtotime($stop_date . ' +1 day'));
			}
		}
	}
		
	#########################################################
	
	private function display_non_meltable_calendar_r1($currentDate, $off_cell, $holiday_date, $cellNumber, $cellContent, $cut_off_date, $region_no, $prod_region_price, $prod_data)
	{
		$onclick_fun = "onclick='get_date(".strtotime($this->currentDate).")'";
		$cut_off = $this->get_cutoff_time();
		if( strtotime($currentDate) == strtotime(date('Y-m-d')) )
		{
			return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
						($cellContent==null?'mask':'').'">'.$cellContent.'<p>Today</p></li>';
		}
		else if(strtotime($currentDate) > strtotime(date('Y-m-d')) && $off_cell != 6 && $off_cell != 7 )
		{
			if(is_array($holiday_date) && !empty($holiday_date) && in_array($currentDate, @$holiday_date))
			{
				$holiday_data = $this->get_holiday_color($currentDate);
				
				return '<li id="li-'.$currentDate.'" style="background-color:'.$holiday_data['cal_color'].';cursor:default"  class="'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
					($cellContent==null?'mask':'').'">'.$cellContent.'<p>'.$holiday_data['cal_name'].'</p></li>';
			}
			else if($cut_off['expedite_cut_off'] > date('H:i:s', time()) && $cut_off['standard_cut_off'] < date('H:i:s', time()) &&  $this->is_loop <= $region_no )
			{
				if( (!(in_array(date('Y-m-d', strtotime($currentDate. ' -3 day')), @$holiday_date)) && $off_cell == 1) || ( !(in_array($currentDate, @$holiday_date))) )
				{
					$onclick_fun = "onclick='get_expedite_date(".strtotime($this->currentDate).",".$this->is_loop.",".$prod_region_price[($this->is_loop - 1)]['prod_region_one'].")'";
					$exp_price = $prod_region_price[($this->is_loop - 1)]['prod_region_one'];
					$this->is_loop = $this->is_loop + 1;
					return '<li id="li-'.$currentDate.'" '.$onclick_fun.' class=" color-04'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
							($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery $'.number_format($prod_data['prod_stand_ship_price_us'], 2, '.' , '' ).' + '.number_format($exp_price,2,'.','').'</p></li>';
				}
				else 
				{
					return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
						($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
				}
			}
			else
			{
				if( ( !(in_array(date('Y-m-d', strtotime($currentDate. ' -3 day')), @$holiday_date)) && $off_cell == 1)
				 || ( !(in_array(date('Y-m-d', strtotime($currentDate. ' -1 day')), @$holiday_date)) && $off_cell != 1) )
				{
					return '<li id="li-'.$currentDate.'" '.$onclick_fun.' class=" color-03'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
								($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery</p></li>'; //$'.$prod_data['prod_stand_ship_price_us'].'
				}
				else
				{
					return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
							($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
				}
			}
		}
		
		else
		{
			return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
						($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
		}
	}
	
	
	
	private function display_non_meltable_calendar_r2($currentDate, $off_cell, $holiday_date, $cellNumber, $cellContent, $stnd_cut_off_date, $expd_cut_off_date, $region_no, $prod_region_price, $prod_data)
	{
		//echo ($this->is_loop) + 1;
		if( $this->is_loop <= $region_no)
		{
			
			$cut_off = $this->get_cutoff_time();
			while($this->is_loop <= $region_no)
			{
				//echo $currentDate.' : '.date('Y-m-d', strtotime($currentDate. ' -3 day')).'<br>';
				//echo '<br>loop : '.$currentDate.' : '.$this->is_loop.'<br>';
				//$onclick_fun = "onclick='get_expedite_date(".strtotime($this->currentDate).",".$this->is_loop.")'";
			
				if(strtotime($currentDate) > strtotime(date('Y-m-d')) && $off_cell != 6 && $off_cell != 7 && $expd_cut_off_date !=  $currentDate )
				{
					if(is_array($holiday_date) && !empty($holiday_date) && in_array($currentDate, @$holiday_date))
					{
						//$onclick_fun = "onclick='get_date()'";
						$holiday_data = $this->get_holiday_color($currentDate);
						if($this->is_loop > 1)
						{
							$this->is_loop = $this->is_loop + 1;
						}
						return '<li id="li-'.$currentDate.'" style="background-color:'.$holiday_data['cal_color'].';cursor:default"  class="'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
							($cellContent==null?'mask':'').'">'.$cellContent.'<p>'.$holiday_data['cal_name'].'</p></li>';
					}
					else if($cut_off['expedite_cut_off'] > date('H:i:s', time()) && $cut_off['standard_cut_off'] > date('H:i:s', time()) && $this->is_loop > 1  )
					{
						if( ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -3 day')), @$holiday_date)) && $off_cell == 1)
						 || ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -1 day')), @$holiday_date)) && $off_cell != 1) )
						{
							$onclick_fun = "onclick='get_date(".strtotime($this->currentDate).")'";
							//$exp_price = $prod_region_price[($this->is_loop - 1)]['prod_region_two'];
							$this->is_loop = $this->is_loop + 1;
							return '<li id="li-'.$currentDate.'" '.$onclick_fun.' class=" color-03'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery </p></li>'; //$'.number_format($prod_data['prod_stand_ship_price_us'], 2, '.' , '' ).'
						}
						else
						{
							return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
						}
					}
					else if($cut_off['expedite_cut_off'] < date('H:i:s', time()) && $cut_off['standard_cut_off'] < date('H:i:s', time()) && $this->is_loop > 1 )
					{
						if( ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -3 day')), @$holiday_date)) && $off_cell == 1)
						 || ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -1 day')), @$holiday_date)) && $off_cell != 1) )
						{
							$onclick_fun = "onclick='get_date(".strtotime($this->currentDate).")'";
							//$exp_price = $prod_region_price[($this->is_loop - 1)]['prod_region_two'];
							$this->is_loop = $this->is_loop + 1;
							return '<li id="li-'.$currentDate.'" '.$onclick_fun.' class=" color-03'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery </p></li>';//$'.number_format($prod_data['prod_stand_ship_price_us'], 2, '.' , '' ).'
						}
						else
						{
							return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
						}
					}
					else if($cut_off['expedite_cut_off'] < date('H:i:s', time()) && $cut_off['standard_cut_off'] > date('H:i:s', time()) && $this->is_loop > 1)
					{
						if( ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -3 day')), @$holiday_date)) && $off_cell == 1)
						 || ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -1 day')), @$holiday_date)) && $off_cell != 1) )
						{
							$this->is_loop = $this->is_loop + 1;
							return '<li id="li-'.$currentDate.'" class=" color-03'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery </p></li>'; //$'.$prod_data['prod_stand_ship_price_us'].'
						}
						else
						{
							return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
						}
					}
					else if($cut_off['expedite_cut_off'] > date('H:i:s', time()) && $cut_off['standard_cut_off'] < date('H:i:s', time()) && $this->is_loop > 1 )
					{
						if( ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -3 day')), @$holiday_date)) && $off_cell == 1)
						 || ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -1 day')), @$holiday_date)) && $off_cell != 1) )
						{
							$onclick_fun = "onclick='get_expedite_date(".strtotime($this->currentDate).",".$this->is_loop.",".$prod_region_price[($this->is_loop - 1)]['prod_region_two'].")'";
							$exp_price = $prod_region_price[($this->is_loop - 1)]['prod_region_two'];
							$this->is_loop = $this->is_loop + 1;
							return '<li id="li-'.$currentDate.'" '.$onclick_fun.' class=" color-04'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery $'.number_format($prod_data['prod_stand_ship_price_us'], 2, '.' , '' ).' + $ '.number_format($exp_price,2,'.','').'</p></li>';
						}
						else
						{
							return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
						}
					}
					else
					{
						if( ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -3 day')), @$holiday_date)) && $off_cell == 1)
						 || ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -1 day')), @$holiday_date)) && $off_cell != 1) )
						{
							$onclick_fun = "onclick='get_expedite_date(".strtotime($this->currentDate).",".$this->is_loop.",".$prod_region_price[($this->is_loop - 1)]['prod_region_two'].")'";
							$exp_price = $prod_region_price[($this->is_loop - 1)]['prod_region_two'];
							$this->is_loop = $this->is_loop + 1;
							return '<li id="li-'.$currentDate.'" '.$onclick_fun.' class=" color-04'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
										($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery + $'.number_format($prod_data['prod_stand_ship_price_us'], 2, '.' , '' ).' + $ '.number_format($exp_price,2,'.','').'</p></li>';
						}
						else
						{
							return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
						}
					}
				}
				else
				{
					if($this->is_loop > 1)
					{
						$this->is_loop = $this->is_loop + 1;
					}
					return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
								($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
				}
			}
		}
		else
		{
			$onclick_fun = "onclick='get_date(".strtotime($this->currentDate).")'";
			
			if(strtotime($currentDate) > strtotime(date('Y-m-d')) && $off_cell != 6 && $off_cell != 7 && $stnd_cut_off_date !=  $currentDate)
			{
				if(is_array($holiday_date) && !empty($holiday_date) && in_array($currentDate, @$holiday_date))
				{
					//$onclick_fun = "onclick='get_date()'";
					$holiday_data = $this->get_holiday_color($currentDate);
					return '<li id="li-'.$currentDate.'" style="background-color:'.$holiday_data['cal_color'].';cursor:default"  class="'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
						($cellContent==null?'mask':'').'">'.$cellContent.'<p>'.$holiday_data['cal_name'].'</p></li>';
				}
				else
				{
					if( ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -3 day')), @$holiday_date)) && $off_cell == 1)
						 || ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -1 day')), @$holiday_date)) && $off_cell != 1) )
					{
						return '<li id="li-'.$currentDate.'" '.$onclick_fun.' class=" color-03'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery </p></li>';//$'.$prod_data['prod_stand_ship_price_us'].'
					}
					else
					{
						return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
								($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
					}
				}
			}
			else
			{
				return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
							($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
			}
		}
	}
	
	
	private function display_non_meltable_calendar_r3($currentDate, $off_cell, $holiday_date, $cellNumber, $cellContent, $stnd_cut_off_date, $expd_cut_off_date, $region_no, $prod_region_price, $prod_data)
	{
		$cut_off = $this->get_cutoff_time();
		//echo ($this->is_loop) + 1;
		if( ($cut_off['expedite_cut_off'] > date('H:i:s', time()) && $cut_off['standard_cut_off'] < date('H:i:s', time()) )
			|| ($cut_off['expedite_cut_off'] < date('H:i:s', time()) && $cut_off['standard_cut_off'] < date('H:i:s', time()) ) )
		{
			$region = $region_no + 1;
		}
		else
		{
			$region = $region_no;
		}
		if( $this->is_loop <= $region)
		{
			//echo 'isloop : '.$this->is_loop.'region_no :'.($region_no+1).'<br>';
			
			while($this->is_loop <= $region)
			{
				//echo $currentDate.' : '.date('Y-m-d', strtotime($currentDate. ' -3 day')).'<br>';
				//echo '<br>loop : '.$currentDate.' : '.$this->is_loop.'<br>';
				//$onclick_fun = "onclick='get_expedite_date(".strtotime($this->currentDate).",".$this->is_loop.")'";
			
				if(strtotime($currentDate) > strtotime(date('Y-m-d')) && $off_cell != 6 && $off_cell != 7  )
				{
					if(is_array($holiday_date) && !empty($holiday_date) && in_array($currentDate, @$holiday_date))
					{
						//$onclick_fun = "onclick='get_date()'";
						$holiday_data = $this->get_holiday_color($currentDate);
						if($this->is_loop > 1)
						{
							$this->is_loop = $this->is_loop + 1;
						}
						return '<li id="li-'.$currentDate.'" style="background-color:'.$holiday_data['cal_color'].';cursor:default"  class="'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
							($cellContent==null?'mask':'').'">'.$cellContent.'<p>'.$holiday_data['cal_name'].'</p></li>';
					}
					else if($cut_off['expedite_cut_off'] > date('H:i:s', time()) && $cut_off['standard_cut_off'] > date('H:i:s', time()) )
					{
						if( ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -3 day')), @$holiday_date)) && $off_cell == 1)
						 || ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -1 day')), @$holiday_date)) && $off_cell != 1) )
						{
							$onclick_fun = "onclick='get_expedite_date(".strtotime($this->currentDate).",".$this->is_loop.",".$prod_region_price[($this->is_loop - 1)]['prod_region_three'].")'";
							$exp_price = $prod_region_price[($this->is_loop - 1)]['prod_region_three'];
							$this->is_loop = $this->is_loop + 1;
							return '<li id="li-'.$currentDate.'" '.$onclick_fun.' class=" color-04'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery $'.number_format($prod_data['prod_stand_ship_price_us'], 2, '.' , '' ).' + $ '.number_format($exp_price,2,'.','').'</p></li>';
						}
						else
						{
							return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
						}
					}
					else if($cut_off['expedite_cut_off'] > date('H:i:s', time()) && $cut_off['standard_cut_off'] < date('H:i:s', time()) )
					{
						
						if( ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -3 day')), @$holiday_date)) && $off_cell == 1)
						 || ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -1 day')), @$holiday_date)) && $off_cell != 1) )
						{
							
							if($this->is_loop == 4)
							{
								$onclick_fun = "onclick='get_expedite_date(".strtotime($this->currentDate).",".$this->is_loop.",".$prod_region_price[($this->is_loop - 2)]['prod_region_three'].")'";
								$exp_price = $prod_region_price[($this->is_loop - 2)]['prod_region_three'];
							}
							else
							{
								$onclick_fun = "onclick='get_expedite_date(".strtotime($this->currentDate).",".$this->is_loop.",".$prod_region_price[($this->is_loop - 1)]['prod_region_three'].")'";
								$exp_price = $prod_region_price[($this->is_loop - 1)]['prod_region_three'];
							}
							$this->is_loop = $this->is_loop + 1;
							return '<li id="li-'.$currentDate.'" '.$onclick_fun.' class=" color-04'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery $'.number_format($prod_data['prod_stand_ship_price_us'], 2, '.' , '' ).' + $ '.number_format($exp_price,2,'.','').'</p></li>';
						}
						else
						{
							return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
						}
					}
					else if($cut_off['expedite_cut_off'] < date('H:i:s', time()) && $cut_off['standard_cut_off'] < date('H:i:s', time()) )
					{
						if($this->is_loop == 1)
						{
							$this->is_loop = $this->is_loop + 1;
							return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'</li>';//$'.number_format($prod_data['prod_stand_ship_price_us'], 2, '.' , '' ).'
						}
						else
						{
							if( ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -3 day')), @$holiday_date)) && $off_cell == 1)
							 || ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -1 day')), @$holiday_date)) && $off_cell != 1) )
							{
								$onclick_fun = "onclick='get_expedite_date(".strtotime($this->currentDate).",".$this->is_loop.",".$prod_region_price[($this->is_loop - 2)]['prod_region_three'].")'";
								$exp_price = $prod_region_price[($this->is_loop - 2)]['prod_region_three'];
								$this->is_loop = $this->is_loop + 1;
								return '<li id="li-'.$currentDate.'" '.$onclick_fun.' class=" color-04'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
										($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery $'.number_format($prod_data['prod_stand_ship_price_us'], 2, '.' , '' ).' + $ '.number_format($exp_price,2,'.','').'</p></li>';
							}
							else
							{
								return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
										($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
							}
						}
					}
					else if($cut_off['expedite_cut_off'] < date('H:i:s', time()) && $cut_off['standard_cut_off'] > date('H:i:s', time()))
					{
						if($this->is_loop == 1)
						{
							echo $this->is_loop = $this->is_loop + 1;
							return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'</li>';//$'.number_format($prod_data['prod_stand_ship_price_us'], 2, '.' , '' ).'
						}
						else
						{
							$onclick_fun = "onclick='get_expedite_date(".strtotime($this->currentDate).",".$this->is_loop.",".$prod_region_price[($this->is_loop - 1)]['prod_region_three'].")'";
							$exp_price = $prod_region_price[($this->is_loop - 1)]['prod_region_three'];
							$this->is_loop = $this->is_loop + 1;
							return '<li id="li-'.$currentDate.'" '.$onclick_fun.' class=" color-04'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery $'.number_format($prod_data['prod_stand_ship_price_us'], 2, '.' , '' ).' + $ '.number_format($exp_price,2,'.','').'</p></li>';
							
						}
					}
					/*else
					{
						if( ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -3 day')), @$holiday_date)) && $off_cell == 1)
						 || ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -1 day')), @$holiday_date)) && $off_cell != 1) )
						{
							$onclick_fun = "onclick='get_expedite_date(".strtotime($this->currentDate).",".$this->is_loop.",".$prod_region_price[($this->is_loop - 1)]['prod_region_three'].")'";
							$exp_price = $prod_region_price[($this->is_loop - 1)]['prod_region_three'];
							$this->is_loop = $this->is_loop + 1;
							return '<li id="li-'.$currentDate.'" '.$onclick_fun.' class=" color-04'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
										($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery + $'.number_format($prod_data['prod_stand_ship_price_us'], 2, '.' , '' ).' + $ '.number_format($exp_price,2,'.','').'</p></li>';
						}
						else
						{
							return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
						}
					}*/
				}
				else
				{
					if($this->is_loop > 1)
					{
						$this->is_loop = $this->is_loop + 1;
					}
					return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
								($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
				}
			}
		}
		else
		{
			$onclick_fun = "onclick='get_date(".strtotime($this->currentDate).")'";
			
			if(strtotime($currentDate) > strtotime(date('Y-m-d')) && $off_cell != 6 && $off_cell != 7 && $stnd_cut_off_date !=  $currentDate)
			{
				if(is_array($holiday_date) && !empty($holiday_date) && in_array($currentDate, @$holiday_date))
				{
					//$onclick_fun = "onclick='get_date()'";
					$holiday_data = $this->get_holiday_color($currentDate);
					return '<li id="li-'.$currentDate.'" style="background-color:'.$holiday_data['cal_color'].';cursor:default"  class="'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
						($cellContent==null?'mask':'').'">'.$cellContent.'<p>'.$holiday_data['cal_name'].'</p></li>';
				}
				else
				{
					if( ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -3 day')), @$holiday_date)) && $off_cell == 1)
						 || ( !(in_array(date('Y-n-d', strtotime($currentDate. ' -1 day')), @$holiday_date)) && $off_cell != 1) )
					{
						return '<li id="li-'.$currentDate.'" '.$onclick_fun.' class=" color-03'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
									($cellContent==null?'mask':'').'">'.$cellContent.'<p>Standard Delivery </p></li>';//$'.$prod_data['prod_stand_ship_price_us'].'
					}
					else
					{
						return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
								($cellContent==null?'mask':'').'">'.$cellContent.'cvc</li>';
					}
				}
			}
			else
			{
				return '<li id="li-'.$currentDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
							($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
			}
		}
		
	}
	
     
    /**
    * create navigation
    */
    
	private function _createNavi_1($region_no)
	{
		$nextMonth = $this->currentMonth==12?1:intval($this->currentMonth)+1;
		$nextYear = $this->currentMonth==12?intval($this->currentYear)+1:$this->currentYear;
        $preMonth = $this->currentMonth==1?12:intval($this->currentMonth)-1;
        $preYear = $this->currentMonth==1?intval($this->currentYear)-1:$this->currentYear;
		
		$currMonth = $this->currentMonth;
		$currYear = $this->currentYear;
		$pre_loop = 0;
		//echo 'Is_loop : '.$this->is_loop.' Pre loop'.$pre_loop;//.' : '.$this->currentMonth.' : '.date('n'); 
		
		/*if($currMonth == date('n')) { echo "Current : ".date('n'); }
		else*/ if($currMonth == date('n', strtotime('+1 month')) && $this->is_loop > 1 ) { $pre_loop = $this->is_loop -1; /* echo "Next 1st :".date('n', strtotime('+1 month'));*/ }
		else if($currMonth == date('n', strtotime('+2 month')) && $this->is_loop == 2 && $pre_loop == 0 ) {  $pre_loop = 1; /*echo "Next 2nd :".date('n', strtotime('+2 month'));*/ }
		else if($currMonth == date('n', strtotime('+3 month')) && $this->is_loop > 1 ) { $pre_loop = 2;  /*echo "Next 3rd :".date('n', strtotime('+3 month')); */}
		else { $pre_loop = 2; }
		
		//echo ' Pre_loop : '.$pre_loop;
		if(date($currYear.'-'.$currMonth.'-01') == date('Y-n-01'))
		{
			return
				'<div class="header">'.
					'<a class="prev" title="Previous" href="javascript:void(0);"></a>'.
						'<span class="title">'.date('M Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
					'<a onclick = "month_changer('.$nextMonth.','.$nextYear.','.$this->is_loop.')" class="next" title="Next" href="javascript:void(0);"></a>'.
				'</div>';
		}
		else if (date($currYear.'-'.$currMonth.'-01') > date('Y-n-01') && date($currYear.'-'.$currMonth.'-01') < date("Y-n-01", strtotime('+3 month')) )
		{
			return
				'<div class="header">'.
					'<a onclick = "month_changer('.$preMonth.','.$preYear.','.$pre_loop.')" class="prev" title="Previous" href="javascript:void(0);"></a>'.
						'<span class="title">'.date('M Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
					'<a onclick = "month_changer('.$nextMonth.','.$nextYear.','.$this->is_loop.')" class="next" title="Next" href="javascript:void(0);"></a>'.
				'</div>';
			
		}
		else if(date($currYear.'-'.$currMonth.'-01') >= date('Y-n-01'))
		{
			return
				'<div class="header">'.
					'<a onclick = "month_changer('.$preMonth.','.$preYear.','.$pre_loop.')" class="prev" title="Previous" href="javascript:void(0);"></a>'.
						'<span class="title">'.date('M Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
					'<a class="next" title="Next" href="javascript:void(0);"></a>'.
				'</div>';
		}
		
    }
	
	
	private function _createNavi_2($region_no)
	{
		$nextMonth = $this->currentMonth==12?1:intval($this->currentMonth)+1;
		$nextYear = $this->currentMonth==12?intval($this->currentYear)+1:$this->currentYear;
        $preMonth = $this->currentMonth==1?12:intval($this->currentMonth)-1;
        $preYear = $this->currentMonth==1?intval($this->currentYear)-1:$this->currentYear;
		
		$currMonth = $this->currentMonth;
		$currYear = $this->currentYear;
		$pre_loop = $this->is_loop;
		#echo '<br>Is_loop : '.$this->is_loop.'<br>Pre loop : '.$pre_loop;//.' : '.$this->currentMonth.' : '.date('n'); 
		if($currMonth == date('n'))
		{
			 $this->CI->session->set_userdata('pre_mon', $this->is_loop);
		}
		else if($currMonth == date('n', strtotime('+1 month')) && $this->is_loop > 1  )
		{
			
			$pre_loop = $this->is_loop -1;
			#echo "<br>Next 1st :".date('n', strtotime('+1 month'));
			#echo '<br>session : ' .$this->CI->session->userdata('pre_mon');
		}
		else if($currMonth == date('n', strtotime('+2 month')) && $this->is_loop > 1 )
		{
			if($this->CI->session->userdata('pre_mon') != '' && $this->CI->session->userdata('pre_mon') != 3 )
			{
				#echo '<br>session : ' .$this->CI->session->userdata('pre_mon');
				$pre_loop = $this->CI->session->userdata('pre_mon');
			}
			else
			{
				$pre_loop = 1;
			}
			#echo "<br>Next 2nd :".date('n', strtotime('+2 month'));
		}
		#else if($currMonth == date('n', strtotime('+3 month')) && $this->is_loop > 1 && $this->is_loop != 3) { $pre_loop = 2;  echo "<br>Next 3rd :".date('n', strtotime('+3 month')); }
		else
		{
			$pre_loop = 3;
			#echo "<br>Invalid";
		}
		
		#echo '<br>Pre_loop after: '.$pre_loop;
		if(date($currYear.'-'.$currMonth.'-01') == date('Y-n-01'))
		{
			return
				'<div class="header">'.
					'<a class="prev" title="Previous" href="javascript:void(0);"></a>'.
						'<span class="title">'.date('M Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
					'<a onclick = "month_changer('.$nextMonth.','.$nextYear.','.$this->is_loop.')" class="next" title="Next" href="javascript:void(0);"></a>'.
				'</div>';
		}
		else if (date($currYear.'-'.$currMonth.'-01') > date('Y-n-01') && date($currYear.'-'.$currMonth.'-01') < date("Y-n-01", strtotime('+3 month')) )
		{
			
			return
				'<div class="header">'.
					'<a onclick = "month_changer('.$preMonth.','.$preYear.','.$pre_loop.')" class="prev" title="Previous" href="javascript:void(0);"></a>'.
						'<span class="title">'.date('M Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
					'<a onclick = "month_changer('.$nextMonth.','.$nextYear.','.$this->is_loop.')" class="next" title="Next" href="javascript:void(0);"></a>'.
				'</div>';
			
		}
		else if(date($currYear.'-'.$currMonth.'-01') >= date('Y-n-01'))
		{
			
			
			return
				'<div class="header">'.
					'<a onclick = "month_changer('.$preMonth.','.$preYear.','.$pre_loop.')" class="prev" title="Previous" href="javascript:void(0);"></a>'.
						'<span class="title">'.date('M Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
					'<a class="next" title="Next" href="javascript:void(0);"></a>'.
				'</div>';
		}
		
    }
	
	
	private function _createNavi_3($region_no)
	{
		$nextMonth = $this->currentMonth==12?1:intval($this->currentMonth)+1;
		$nextYear = $this->currentMonth==12?intval($this->currentYear)+1:$this->currentYear;
        $preMonth = $this->currentMonth==1?12:intval($this->currentMonth)-1;
        $preYear = $this->currentMonth==1?intval($this->currentYear)-1:$this->currentYear;
		
		$currMonth = $this->currentMonth;
		$currYear = $this->currentYear;
		$pre_loop = $this->is_loop;
		echo '<br>Is_loop : '.$this->is_loop.'<br>Pre loop : '.$pre_loop;//.' : '.$this->currentMonth.' : '.date('n'); 
		if($currMonth == date('n'))
		{
			 $this->CI->session->set_userdata('pre_mon', $this->is_loop);
		}
		else if($currMonth == date('n', strtotime('+1 month')) && $this->is_loop > 1  )
		{
			
			$pre_loop = $this->is_loop -1;
			echo "<br>Next 1st :".date('n', strtotime('+1 month'));
			echo '<br>session : ' .$this->CI->session->userdata('pre_mon');
		}
		else if($currMonth == date('n', strtotime('+2 month')) && $this->is_loop > 1 )
		{
			if($this->CI->session->userdata('pre_mon') != '' && $this->CI->session->userdata('pre_mon') != 3 )
			{
				echo '<br>session : ' .$this->CI->session->userdata('pre_mon');
				$pre_loop = $this->CI->session->userdata('pre_mon');
			}
			else
			{
				$pre_loop = 1;
			}
			echo "<br>Next 2nd :".date('n', strtotime('+2 month'));
		}
		else
		{
			$pre_loop = 3;
			echo "<br>Invalid";
		}
		
		echo '<br>Pre_loop after: '.$pre_loop;
		if(date($currYear.'-'.$currMonth.'-01') == date('Y-n-01'))
		{
			return
				'<div class="header">'.
					'<a class="prev" title="Previous" href="javascript:void(0);"></a>'.
						'<span class="title">'.date('M Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
					'<a onclick = "month_changer('.$nextMonth.','.$nextYear.','.$this->is_loop.')" class="next" title="Next" href="javascript:void(0);"></a>'.
				'</div>';
		}
		else if (date($currYear.'-'.$currMonth.'-01') > date('Y-n-01') && date($currYear.'-'.$currMonth.'-01') < date("Y-n-01", strtotime('+3 month')) )
		{
			
			return
				'<div class="header">'.
					'<a onclick = "month_changer('.$preMonth.','.$preYear.','.$pre_loop.')" class="prev" title="Previous" href="javascript:void(0);"></a>'.
						'<span class="title">'.date('M Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
					'<a onclick = "month_changer('.$nextMonth.','.$nextYear.','.$this->is_loop.')" class="next" title="Next" href="javascript:void(0);"></a>'.
				'</div>';
			
		}
		else if(date($currYear.'-'.$currMonth.'-01') >= date('Y-n-01'))
		{
			
			
			return
				'<div class="header">'.
					'<a onclick = "month_changer('.$preMonth.','.$preYear.','.$pre_loop.')" class="prev" title="Previous" href="javascript:void(0);"></a>'.
						'<span class="title">'.date('M Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
					'<a class="next" title="Next" href="javascript:void(0);"></a>'.
				'</div>';
		}
		
    }
	
	
         
    /**
    * create calendar week labels
    */
    private function _createLabels(){  
                 
        $content='';
         
        foreach($this->dayLabels as $index=>$label)
		{
            $content.='<li class="'.($label==6?'end title':'start title').' title">'.$label.'</li>'; 
        }
		
        return $content;
    }
     
     
     
    /**
    * calculate number of weeks in a particular month
    */
    private function _weeksInMonth($month=null,$year=null)
	{
		if( null==($year) )
		{
			$year =  date("Y",time()); 
        }
         
        if(null==($month))
		{
			$month = date("m",time());
        }
         
        // find number of days in this month
        $daysInMonths = $this->_daysInMonth($month,$year);
		
        $numOfweeks = ($daysInMonths%7==0?0:1) + intval($daysInMonths/7);
         
        $monthEndingDay= date('N',strtotime($year.'-'.$month.'-'.$daysInMonths));
         
        $monthStartDay = date('N',strtotime($year.'-'.$month.'-01'));
         
        if($monthEndingDay<$monthStartDay)
		{
			$numOfweeks++;
        }
        
		return $numOfweeks;
    }
 
    /**
    * calculate number of days in a particular month
    */
    private function _daysInMonth($month=null,$year=null)
	{
		if(null==($year))
		{
			$year =  date("Y",time());
		}
		
        if(null==($month))
		{
			$month = date("m",time());
		}
		
		return date('t',strtotime($year.'-'.$month.'-01'));
    }
	
	private function _getHoliday()
	{
		$query = $this->CI->db->query("
										SELECT * FROM tbl_calender
										WHERE
										is_active = 1
										AND
										cal_date >= DATE_FORMAT(CURDATE(), '%Y-%m-%d')
										AND
										cal_date <= DATE_FORMAT(CURDATE(), '%Y-%m-01') + INTERVAL 4 MONTH
									");
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
	
	private function get_region_no($data = array())
	{
		$this->CI->db->select('*')->from('tbl_region_zip')->where(array('rp_zipcode' => $data['zip']));
		$zip_region = $this->CI->db->get()->row_array();
		
		if(count($zip_region) > 0)
		{
			return $zip_region['rp_region'];
		}
		else
		{
			return false;
		}
		
	}
	
	private function get_product_zip_region_price($data = array())
	{		
		$region_no = $this->get_region_no($data);
		
		if($data['country'] == 'US')
		{
			if(!empty($region_no))
			{
				if($region_no == '1')
				{
					$this->CI->db->select('prod_region_one, prod_region_type')->from('tbl_product_region_prices')->where(array('prod_region_prod_id'=>$data['prod_id']));
					return $this->CI->db->get()->result_array();
				}
				else if($region_no == '2')
				{
					$this->CI->db->select('prod_region_two, prod_region_type')->from('tbl_product_region_prices')->where(array('prod_region_prod_id'=>$data['prod_id']));
					return $this->CI->db->get()->result_array();
	
				}
				else if($region_no == '3')
				{
					$this->CI->db->select('prod_region_three, prod_region_type')->from('tbl_product_region_prices')->where(array('prod_region_prod_id'=>$data['prod_id']));
					return $this->CI->db->get()->result_array();
				}
				else if($region_no == '4')
				{
					$this->CI->db->select('prod_region_four, prod_region_type')->from('tbl_product_region_prices')->where(array('prod_region_prod_id'=>$data['prod_id']));
					return $this->CI->db->get()->result_array();
				}
				else if($region_no == '5')
				{
					$this->CI->db->select('prod_region_five, prod_region_type')->from('tbl_product_region_prices')->where(array('prod_region_prod_id'=>$data['prod_id']));
					return $this->CI->db->get()->result_array();
				}
				else if($region_no == '6')
				{
					$this->CI->db->select('prod_region_six, prod_region_type')->from('tbl_product_region_prices')->where(array('prod_region_prod_id'=>$data['prod_id']));
					return $this->CI->db->get()->result_array();
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
			
		}
		else if ($data['country'] == 'CA')
		{
			$this->CI->db->select('prod_region_seven, prod_region_type')->from('tbl_product_region_prices')->where(array('prod_region_prod_id'=>$data['prod_id']));
			return $this->CI->db->get()->result_array();
		}
		
		/*else if($region_no == 1)
		{
		}*/
	}
	
	
	
	
	private function get_expedite_cut_off_date($currentDate, $off_cell, $holiday_date, $cut_off, $stnd_cut_off_date)
	{
		//echo $cut_off['standard_cut_off'] .' : '. date('H:i:s', time() );
		//&& (strtotime(date('Y-m-d'))- $currentDate) == 0
		//echo strtotime($currentDate) - strtotime($stnd_cut_off_date);
		//exit;
		//echo '<br>'.(strtotime($stnd_cut_off_date)-strtotime($currentDate));
		if($cut_off['expedite_cut_off'] < date('H:i:s', time()) )//&& $cut_off['expedite_cut_off'] >= $cut_off['standard_cut_off']
		{
			if(is_array($holiday_date) && !empty($holiday_date) && in_array($currentDate, @$holiday_date))
			{
				return $this->get_expedite_cut_off_date(date('Y-m-d', strtotime($currentDate .' +1 day')), ($off_cell + 1), $holiday_date, $cut_off, $stnd_cut_off_date);
			}
			/*else if( (strtotime($stnd_cut_off_date)-strtotime($currentDate)) == 0 )
			{
				return $this->get_expedite_cut_off_date(date('Y-m-d', strtotime($currentDate .' +1 day')), ($off_cell + 1), $holiday_date, $cut_off, $stnd_cut_off_date);
			}*/
			else
			{
				//echo '<br>return value: '.$currentDate;
				return $currentDate;
			}
		}
		else
		{
			return date('Y-m-d', strtotime($currentDate .' -1 day'));
		}
	}
	
	private function get_standard_cut_off_date($currentDate, $off_cell, $holiday_date, $cut_off)
	{
		//echo 'Curr Date: '.$currentDate;
		//echo 'next day'.date('Y-m-d', strtotime($currentDate .' +1 day'));
		//echo $cut_off['standard_cut_off'] .' : '. date('H:i:s', time() );
		//&& (strtotime(date('Y-m-d'))- $currentDate) == 0
		if($cut_off['standard_cut_off'] < date('H:i:s', time()) )
		{
			if(is_array($holiday_date) && !empty($holiday_date) && in_array($currentDate, @$holiday_date))
			{
				return $this->get_standard_cut_off_date(date('Y-m-d', strtotime($currentDate .' +1 day')), ($off_cell + 1), $holiday_date, $cut_off);
			}
			else
			{
				if($off_cell == 5 || $off_cell == 6 || $off_cell == 7 )
				{
					return $this->get_standard_cut_off_date(date('Y-m-d', strtotime($currentDate .' +1 day')), ($off_cell + 1), $holiday_date, $cut_off);
				}
				else
				{
					//echo '<br>return value: '.$currentDate;
					//echo 'Output : '.$currentDate. ' -> '.date($currentDate, strtotime('+1 day')).' ';
					return $currentDate;
				}
			}
		}
		else
		{
			return date('Y-m-d', strtotime($currentDate .' -1 day'));//return false;//echo 'empty';
		}
	}
	
	private function get_product_data($prod_id)
	{
		$this->CI->db->select('*')->from('tbl_product')->where('prod_id = '.$prod_id);
		return $this->CI->db->get()->row_array();
	}
	
	private function get_cutoff_time()
	{
		$this->CI->db->select('standard_cut_off, expedite_cut_off')->from('tbl_site_setting');
		return $this->CI->db->get()->row_array();
		
		
	}
	private function get_holiday_color($holiday)
	{
		$this->CI->db->select('*')->from('tbl_calender')->where(array('cal_date'=>$holiday,'is_active'=>'1'));
		return $this->CI->db->get()->row_array();
	}
	
	
	
	
	/*private function display_non_meltable_calendar($currendDate, $off_cell, $holiday_date, $cellNumber, $cellContent, $cut_off, $prod_region_price)
	{
		$onclick_fun = "onclick='get_date(".strtotime($this->currentDate).")'";
		
		if(strtotime($currendDate) > strtotime(date('Y-m-d')) && $off_cell != 6 && $off_cell != 7)
		{
			if(is_array($holiday_date) && !empty($holiday_date) && in_array($currendDate, @$holiday_date))
			{
					$onclick_fun = "onclick='get_date()'";
					$holiday_data = $this->get_holiday_color($currendDate);
					
					return '<li id="li-'.$currendDate.'" style="background-color:'.$holiday_data['cal_color'].';cursor:default" '.$onclick_fun.' class="'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
						($cellContent==null?'mask':'').'">'.$cellContent.'<p>'.$holiday_data['cal_name'].'</p></li>';
			}
			else if($cut_off['standard_cut_off'])
			{
				//if there is no holiday add from admin side
				//Cut Off time is Not end for Standard Delivery
				return '<li id="li-'.$currendDate.'" '.$onclick_fun.' class=" color-03 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
							($cellContent==null?'mask':'').'">'.$cellContent.'<p>2Standard Delivery + $2 + $8</p></li>';
			}
			else
			{
				return '<li id="li-'.$currendDate.'" style="cursor:default" class=" color-02'.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
							($cellContent==null?'mask':'').'">'.$cellContent.'<p>2Standard Delivery + $2 + $8</p></li>';
			}
		}
		else
		{
			return '<li id="li-'.$currendDate.'" style="cursor:default" class=" color-02 '.($cellNumber%7==1?' start ':($cellNumber%7==0?' end ':' ')).
						($cellContent==null?'mask':'').'">'.$cellContent.'</li>';
		}
	}*/
	
	
	
	/*
	private function _createNavi($region_no)
	{
		#echo 'Arg : '.$loop_var;
		#echo 'class : '.$this->is_loop;
		#exit;
		//echo '<pre>';
		//print_r($region_no);
		//echo '</pre>';
		
        $nextMonth = $this->currentMonth==12?1:intval($this->currentMonth)+1;
		$nextYear = $this->currentMonth==12?intval($this->currentYear)+1:$this->currentYear;
        $preMonth = $this->currentMonth==1?12:intval($this->currentMonth)-1;
        $preYear = $this->currentMonth==1?intval($this->currentYear)-1:$this->currentYear;
		
		$currMonth = $this->currentMonth;
		$currYear = $this->currentYear;
		
		echo $this->is_loop.' : '.$this->currentMonth.' : '.date('n', strtotime('+1 month')); 
       # return
       #     '<div class="header">'. 
       #         '<a  class="prev" title="Previous" href="'.$this->naviHref.'/'.$preMonth.'/'.$preYear.'"></a>'.
       #             '<span class="title">'.date('M Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
       #         '<a class="next" title="Next" href="'.$this->naviHref.'/'.$nextMonth.'/'.$nextYear.'"></a>'.
       #     '</div>';
		
		
		
		//echo "Previous Date : ".date($preYear.'-'.$preMonth.'-01').'<br/>Today date : '.date('Y-m-01').'<br>Next Date : '.date($nextYear.'-'.$nextMonth.'-01').'<br/>';
		//echo '<br/>Current Date'.date($currYear.'-'.$currMonth.'-01');
		//echo "Next 4 months : ". $next4Month =  date("Y-n-01", strtotime('+3 month')).'<br/>';
		if(date($currYear.'-'.$currMonth.'-01') == date('Y-n-01'))
		{
			return
				'<div class="header">'.
					'<a class="prev" title="Previous" href="javascript:void(0);"></a>'.
						'<span class="title">'.date('M Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
					'<a onclick = "month_changer('.$nextMonth.','.$nextYear.','.$this->is_loop.')" class="next" title="Next" href="javascript:void(0);"></a>'.
				'</div>';
		}
		else if (date($currYear.'-'.$currMonth.'-01') > date('Y-n-01') && date($currYear.'-'.$currMonth.'-01') < date("Y-n-01", strtotime('+3 month')) )
		{
			$pre_loop = 0;
			if($this->is_loop > 1 && $this->is_loop > $region_no && $currMonth >= date('n', strtotime('+1 month')) )
			{
				$pre_loop = $this->is_loop - 1;
			}
			return
				'<div class="header">'.
					'<a onclick = "month_changer('.$preMonth.','.$preYear.','.$pre_loop.')" class="prev" title="Previous" href="javascript:void(0);"></a>'.
						'<span class="title">'.date('M Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
					'<a onclick = "month_changer('.$nextMonth.','.$nextYear.','.$this->is_loop.')" class="next" title="Next" href="javascript:void(0);"></a>'.
				'</div>';
			
		}
		else if(date($currYear.'-'.$currMonth.'-01') >= date('Y-n-01'))
		{
			$pre_loop = 0;
			if($this->is_loop > 1 && $this->is_loop > $region_no && $currMonth >= date('n', strtotime('+1 month')) )
			{
				$pre_loop = $this->is_loop - 1;
			}
			return
				'<div class="header">'.
					'<a onclick = "month_changer('.$preMonth.','.$preYear.','.$pre_loop.')" class="prev" title="Previous" href="javascript:void(0);"></a>'.
						'<span class="title">'.date('M Y',strtotime($this->currentYear.'-'.$this->currentMonth.'-1')).'</span>'.
					'<a class="next" title="Next" href="javascript:void(0);"></a>'.
				'</div>';
		}
		
    }*/
	
	
	
	
	
	
     
}
