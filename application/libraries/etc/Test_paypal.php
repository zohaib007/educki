<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');

//use net\authorize\api\contract\v1 as AnetAPI;
//use net\authorize\api\controller as AnetController;

// autoload composer 
//require 'vendor/autoload.php';
require 'vendor/paypal/rest-api-sdk-php/sample/bootstrap.php';

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;


class Test_paypal {
	
	private $CI;
	
	public function __construct()
	{
		
		//$this->ci = & get_instance();
	}
	
	
	public function new_fun()
	{
		/*$payer = new Payer();
		$payer->setPayment_method("paypal");
		
		$item1 = new Item();
		$item1->setName('Ground Coffee 40 oz')->setCurrency('USD')->setQuantity(1)->setSku("123123")->setPrice(7.5);
		$item2 = new Item();
		$item2->setName('Granola bars')->setCurrency('USD')->setQuantity(5)->setSku("321321")->setPrice(2);
		$itemList = new ItemList(); $itemList->setItems(array($item1, $item2));
		
		$details = new Details();
		$details->setShipping(1.2)->setTax(1.3)->setSubtotal(17.50);
		
		$amount = new Amount();
		$amount->setCurrency("USD")->setTotal(20)->setDetails($details);
		
		$transaction = new Transaction();
		$transaction->setAmount($amount)->setItemList($itemList)->setDescription("Payment description")->setInvoiceNumber(uniqid());
		
		$baseUrl = getBaseUrl();
		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl("$baseUrl/ExecutePayment.php?success=true")->setCancelUrl("$baseUrl/ExecutePayment.php?success=false");
		
		$payment = new Payment();
		$payment->setIntent("sale")->setPayer($payer)->setRedirectUrls($redirectUrls)->setTransactions(array($transaction));
		
		$request = clone $payment;
		
		try{
			$payment->create($apiContext);
		}catch(Exception $ex){
			ResultPrinter::printError("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", null, $request, $ex);
			exit(1);
		}
		
		$approvalUrl = $payment->getApprovalLink();
		
		ResultPrinter::printResult("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", "<a href='$approvalUrl' >$approvalUrl</a>", $request, $payment);
		
		
		echo '<pre>';
		print_r($payment);
		exit;
		return $payment;*/
		
		//$apiContext = new ApiContext(new OAuthTokenCredential(
		//"EBWKjlELKMYqRNQ6sYvFo64FtaRLRR5BdHEESmha49TM", "EO422dn3gQLgDbuwqTjzrFgFtaRLRR5BdHEESmha49TM"));
				
		
		
		$payer = new Payer();
		$payer->setPayment_method('paypal');
				
		$amount = new Amount();
		$amount->setCurrency('USD');
		$amount->setTotal('7.47');
		
		
		$transaction = new Transaction();
		$transaction->setAmount($amount);
		$transaction->setDescription('This is the payment transaction description.');
		
		$payment = new Payment();
		$payment->setIntent('sale');
		$payment->setPayer($payer);
		$payment->setTransactions(array($transaction));
		
		
		
	}
	
	
	
	public function test_check()
	{
		
		//require_once(APPPATH.'/third_party/adaptivepayments-sdk-php-master/samples/PPBootStrap.php');
		//require_once(APPPATH.'/third_party/adaptivepayments-sdk-php-master/samples/Common/Constants.php');
		
		require 'vendor/autoload.php';
		
		define("DEFAULT_SELECT", "- Select -");
		///////////////////////////////parameters send to paypal account/////////
		$returnUrl =  "http://top.com/broadwaybasketeers/cart/paypal_entry";
		$cancelUrl =  "http://top.com/broadwaybasketeers/cart/checkout";
		$actionType = "PAY";
		$currencyCode = "USD";
		$reciver_email = "michael.smith1177-facilitator@gmail.com";
		$id = "PAYMENTINFO_0_TRANSACTIONID";
		$total_amount = number_format('100');
		
		$receiver = array();
		/*
		* A receiver's email address 
		*/
		
		$receiver = new Receiver();
		$receiver->email = $reciver_email;
		/*
		*  	Amount to be credited to the receiver's account 
		*/
		//$receiver->amount = $_POST['receiverAmount'];
		$receiver->amount = $total_amount;
		
		$receiverList = new ReceiverList($receiver);
		
		/*
		* The action for this request. Possible values are:
		
		PAY � Use this option if you are not using the Pay request in combination with ExecutePayment.
		CREATE � Use this option to set up the payment instructions with SetPaymentOptions and then execute the payment at a later time with the ExecutePayment.
		PAY_PRIMARY � For chained payments only, specify this value to delay payments to the secondary receivers; only the payment to the primary receiver is processed.
		
		*/
		/*
		* The code for the currency in which the payment is made; you can specify only one currency, regardless of the number of receivers 
		*/
		/*
		* URL to redirect the sender's browser to after canceling the approval for a payment; it is always required but only used for payments that require approval (explicit payments) 
		*/
		/*
		* URL to redirect the sender's browser to after the sender has logged into PayPal and approved a payment; it is always required but only used if a payment requires explicit approval 
		*/
		$payRequest = new PayRequest(new RequestEnvelope("en_US"), $actionType, $cancelUrl, $currencyCode, $receiverList, $returnUrl, $id);
		
		/*
		* 	 ## Creating service wrapper object
		Creating service wrapper object to make API call and loading
		Configuration::getAcctAndConfig() returns array that contains credential and config parameters
		*/
		$service = new AdaptivePaymentsService(Configuration::getAcctAndConfig());
		try
		{
			/* wrap API method calls on the service object with a try catch */
			$response = $service->Pay($payRequest);
		} 
		catch(Exception $ex)
		{
			require_once(APPPATH.'/third_party/adaptivepayments-sdk-php-master/samples/Common/Error.php');
			exit;
		}
		
		$ack = strtoupper($response->responseEnvelope->ack);
		if($ack != "SUCCESS") 
		{
			echo "<b>Error</b>";
			echo "<pre>";
			echo "</pre>";
		} 
		else 
		{
			$payKey = $response->payKey;
			$payPalURL = PAYPAL_REDIRECT_URL . '_ap-payment&paykey=' . $payKey;
			#redirect($payPalURL);
			#exit;
			redirect($payPalURL);
			exit;
			/*echo "<table>";
			echo "<tr><td>Ack :</td><td><div id='Ack'>$ack</div> </td></tr>";
			echo "<tr><td>PayKey :</td><td><div id='PayKey'>$payKey</div> </td></tr>";
			echo "<tr><td><a href=$payPalURL><b>Redirect URL to Complete Payment </b></a></td></tr>";
			echo "</table>";
			echo "<pre>";
			print_r($response);
			echo "</pre>";	
			exit;*/
		}
		require_once(APPPATH.'/third_party/adaptivepayments-sdk-php-master/samples/Common/Response.php');
		/* Make the call to PayPal to get the Pay token
		If the API call succeded, then redirect the buyer to PayPal
		to begin to authorize payment.  If an error occured, show the
		resulting errors */
		//$this->load->view('SimplePay');	
	
	}
	
}
	