<?php (defined('BASEPATH')) OR exit('No direct script access allowed');


class MY_Cart extends CI_Cart {
	
	public function __construct($params = array())
	{
		// Set the super object to a local variable for use later
		$this->CI =& get_instance();
		
		// Are any config settings being passed manually?  If so, set them
		$config = array();
		if(count($params) > 0)
		{
			foreach($params as $key => $val)
			{
				$config[$key] = $val;
			}
		}
				
		// Load the Sessions class
		$this->CI->load->library('session', $config);
		
		// Grab the shopping cart array from the session table, if it exists
		if ($this->CI->session->userdata('cart_contents') !== FALSE)
		{
			$this->_cart_contents = $this->CI->session->userdata('cart_contents');
		}
		else
		{
			// No cart exists so we'll set some base values
			$this->_cart_contents['cart_total'] = 0;
			$this->_cart_contents['total_items'] = 0;
			$this->_cart_contents['cart_total_discount'] = 0;
			$this->_cart_contents['cart_total_no_discount'] = 0;
		}
		
		
		log_message('debug', "Cart Class Initialized");
	}
	
	public function insert($items = array())
	{
		// Was any cart data passed? No? Bah...
		if ( ! is_array($items) OR count($items) == 0)
		{
			log_message('error', 'The insert method must be passed an array containing data.');
			return FALSE;
		}

		// You can either insert a single product using a one-dimensional array,
		// or multiple products using a multi-dimensional one. The way we
		// determine the array type is by looking for a required array key named "id"
		// at the top level. If it's not found, we will assume it's a multi-dimensional array.

		$save_cart = FALSE;
		if (isset($items['id']))
		{
			if (($rowid = $this->_insert($items)))
			{
				$save_cart = TRUE;
			}
		}
		else
		{
			foreach ($items as $val)
			{
				if (is_array($val) AND isset($val['id']))
				{
					if ($this->_insert($val))
					{
						$save_cart = TRUE;
					}
				}
			}
		}

		// Save the cart data if the insert was successful
		if ($save_cart == TRUE)
		{
			$this->_save_cart();
			return isset($rowid) ? $rowid : TRUE;
		}

		return FALSE;
	}
	
	public function _insert($items = array())
	{
		// Was any cart data passed? No? Bah...
		if ( ! is_array($items) OR count($items) == 0)
		{
			log_message('error', 'The insert method must be passed an array containing data.');
			return FALSE;
		}

		// --------------------------------------------------------------------

		// Does the $items array contain an id, quantity, price, and name?  These are required
		if ( ! isset($items['id']) OR ! isset($items['qty']) OR ! isset($items['price']) OR ! isset($items['name']))
		{
			log_message('error', 'The cart array must contain a product ID, quantity, price, and name.');
			return FALSE;
		}

		// --------------------------------------------------------------------

		// Prep the quantity. It can only be a number.  Duh...
		$items['qty'] = trim(preg_replace('/([^0-9])/i', '', $items['qty']));
		// Trim any leading zeros
		$items['qty'] = trim(preg_replace('/(^[0]+)/i', '', $items['qty']));

		// If the quantity is zero or blank there's nothing for us to do
		if ( ! is_numeric($items['qty']) OR $items['qty'] == 0)
		{
			return FALSE;
		}

		// --------------------------------------------------------------------

		// Validate the product ID. It can only be alpha-numeric, dashes, underscores or periods
		// Not totally sure we should impose this rule, but it seems prudent to standardize IDs.
		// Note: These can be user-specified by setting the $this->product_id_rules variable.
		if ( ! preg_match("/^[".$this->product_id_rules."]+$/i", $items['id']))
		{
			log_message('error', 'Invalid product ID.  The product ID can only contain alpha-numeric characters, dashes, and underscores');
			return FALSE;
		}

		// --------------------------------------------------------------------

		// Validate the product name. It can only be alpha-numeric, dashes, underscores, colons or periods.
		// Note: These can be user-specified by setting the $this->product_name_rules variable.
		if ( ! preg_match("/^[".$this->product_name_rules."]+$/i", $items['name']))
		{
			log_message('error', 'An invalid name was submitted as the product name: '.$items['name'].' The name can only contain alpha-numeric characters, dashes, underscores, colons, and spaces');
			return FALSE;
		}

		// --------------------------------------------------------------------

		// Prep the price.  Remove anything that isn't a number or decimal point.
		$items['price'] = trim(preg_replace('/([^0-9\.])/i', '', $items['price']));
		// Trim any leading zeros
		$items['price'] = trim(preg_replace('/(^[0]+)/i', '', $items['price']));

		// Is the price a valid number?
		if ( ! is_numeric($items['price']))
		{
			log_message('error', 'An invalid price was submitted for product ID: '.$items['id']);
			return FALSE;
		}

		// --------------------------------------------------------------------

		// We now need to create a unique identifier for the item being inserted into the cart.
		// Every time something is added to the cart it is stored in the master cart array.
		// Each row in the cart array, however, must have a unique index that identifies not only
		// a particular product, but makes it possible to store identical products with different options.
		// For example, what if someone buys two identical t-shirts (same product ID), but in
		// different sizes?  The product ID (and other attributes, like the name) will be identical for
		// both sizes because it's the same shirt. The only difference will be the size.
		// Internally, we need to treat identical submissions, but with different options, as a unique product.
		// Our solution is to convert the options array to a string and MD5 it along with the product ID.
		// This becomes the unique "row ID"
		if (isset($items['options']) AND count($items['options']) > 0)
		{
			$rowid = md5($items['id'].implode('', $items['options']));
		}
		else
		{
			// No options were submitted so we simply MD5 the product ID.
			// Technically, we don't need to MD5 the ID in this case, but it makes
			// sense to standardize the format of array indexes for both conditions
			$rowid = md5($items['id']);
		}

		// --------------------------------------------------------------------

		// Now that we have our unique "row ID", we'll add our cart items to the master array

		// let's unset this first, just to make sure our index contains only the data from this submission
		unset($this->_cart_contents[$rowid]);

		// Create a new index with our new row ID
		$this->_cart_contents[$rowid]['rowid'] = $rowid;

		// And add the new items to the cart array
		foreach ($items as $key => $val)
		{
			$this->_cart_contents[$rowid][$key] = $val;
		}

		// Woot!
		return $rowid;
	}

	public function update($items = array())
	{
		// Was any cart data passed?
		if ( ! is_array($items) OR count($items) == 0)
		{
			return FALSE;
		}

		// You can either update a single product using a one-dimensional array,
		// or multiple products using a multi-dimensional one.  The way we
		// determine the array type is by looking for a required array key named "id".
		// If it's not found we assume it's a multi-dimensional array
		$save_cart = FALSE;
		if (isset($items['rowid']) AND isset($items['qty']))
		{
			if ($this->_update($items) == TRUE)
			{
				$save_cart = TRUE;
			}
		}
		else
		{
			foreach ($items as $val)
			{
				if (is_array($val) AND isset($val['rowid']) AND isset($val['qty']))
				{
					if ($this->_update($val) == TRUE)
					{
						$save_cart = TRUE;
					}
				}
			}
		}

		// Save the cart data if the insert was successful
		if ($save_cart == TRUE)
		{
			$this->_save_cart();
			return TRUE;
		}

		return FALSE;
	}	
	
	public function _update($items = array())
	{
		// Without these array indexes there is nothing we can do
		if ( ! isset($items['qty']) OR ! isset($items['rowid']) OR ! isset($this->_cart_contents[$items['rowid']]))
		{
			return FALSE;
		}
		// create new rowid
		if (isset($items['options']) AND count($items['options']) > 0)
		{
			$rowid = md5(@$items['id'].implode('', $items['options']));
		}
		else
		{
			if(isset($items['id']))
			{
				$rowid = md5(@$items['id']);
			}
		}
		
		if(isset($rowid))
		{
			// save current cart item
			$current_cart_item = $this->_cart_contents[$items['rowid']];
			// save current order
			$current_order = $this->_preserve_cart_order($this->_cart_contents);
			// remove current cart with old rowid
			unset($this->_cart_contents[$items['rowid']]);
			// create new array with new rowid
			$this->_cart_contents[$rowid] = $current_cart_item;
			// return order key with old rowid
			$order_key = array_search($items['rowid'], $current_order);
			// replace old rowid with new rowid in order array
			$current_order[$order_key] = $rowid;
			// reorder cart
			$this->_cart_contents = $this->_restore_cart_order($this->_cart_contents, $current_order);
		}
		else
		{
			$rowid = $items['rowid'];
		}
		
		if(isset($items['price']))
		{
			// Prep the price.  Remove anything that isn't a number or decimal point.
			$items['price'] = trim(preg_replace('/([^0-9\.])/i', '', $items['price']));
			// Trim any leading zeros
			$items['price'] = trim(preg_replace('/(^[0]+)/i', '', $items['price']));
			// Is the price a valid number?
			if ( ! is_numeric($items['price']))
			{
				log_message('error', 'An invalid price was submitted for product ID: '.$items['id']);
				return FALSE;
			}
			else
			{
				$this->_cart_contents[$rowid]['price'] = $items['price'];
			}
		}
		
		// Prep the quantity
		$items['qty'] = preg_replace('/([^0-9])/i', '', $items['qty']);
		// Is the quantity a number?
		if ( ! is_numeric($items['qty']))
		{
			return FALSE;
		}
		// Is the quantity zero?  If so we will remove the item from the cart.
		// If the quantity is greater than zero we are updating
		if ($items['qty'] == 0)
		{
			unset($this->_cart_contents[$rowid]);
		}
		else
		{
			$this->_cart_contents[$rowid]['qty'] = $items['qty'];
		}
		
		if(isset($items['options']) AND count($items['options']) > 0)
		{
			$this->_cart_contents[$rowid]['options'] = $items['options'];
		}
		// reset rowid
		$this->_cart_contents[$rowid]['rowid'] = $rowid;
		return TRUE;
	}
	
	// end of _update function
	public function _preserve_cart_order($cart)
	{
		$cart_order = array();
		foreach($cart as $key=>$index)
		{
			$cart_order[] = $key;
		}
		return $cart_order;
	}
	
	private function _restore_cart_order($cart,$cart_order)
	{
		$ordered = array();
		foreach($cart_order as $key)
		{
			if(array_key_exists($key,$cart))
			{
				$ordered[$key] = $cart[$key];
				unset($cart[$key]);
			}
		}
		return $ordered + $cart;
	}
	
	
	public function contents($newest_first = FALSE)
	{
		$cart = $this->_cart_contents;
		
		// Remove these so they don't create a problem when showing the cart table
		unset($cart['total_items']);
		unset($cart['cart_total']);
		unset($cart['cart_total_discount']);
		unset($cart['cart_total_no_discount']);
		
		return $cart;
	}
	
	public function destroy()
	{
		unset($this->_cart_contents);
		
		$this->_cart_contents['cart_total'] = 0;
		$this->_cart_contents['total_items'] = 0;
		$this->_cart_contents['cart_total_discount'] = 0;
		$this->_cart_contents['cart_total_no_discount'] = 0;
		
		$this->CI->session->unset_userdata('cart_contents');
	}
	
	public function total_discount()
	{
		return $this->_cart_contents['cart_total_discount'];
	}
	
	public function total_no_discount()
	{
		return $this->_cart_contents['cart_total_no_discount'];
	}
	
	public function _save_cart()
	{
		// Unset these so our total can be calculated correctly below
		unset($this->_cart_contents['total_items']);
		unset($this->_cart_contents['cart_total']);
		
		unset($this->_cart_contents['cart_total_no_discount']);
		unset($this->_cart_contents['cart_total_discount']);

		// Lets add up the individual prices and set the cart sub-total
		$total = 0;
		$total_discount = 0;
		$items = 0;
		foreach ($this->_cart_contents as $key => $val)
		{
			// We make sure the array contains the proper indexes
			if ( ! is_array($val) OR ! isset($val['price']) OR ! isset($val['qty']))
			{
				continue;
			}

			$total += ($val['price'] * $val['qty']);
			$items += $val['qty'];

			// Set the subtotal
			$this->_cart_contents[$key]['subtotal'] = ($this->_cart_contents[$key]['price'] * $this->_cart_contents[$key]['qty']);
		}

		// Set the cart total and total items.
		$this->_cart_contents['total_items'] = $items;
		$this->_cart_contents['cart_total'] = $total;

		// Is our cart empty?  If so we delete it from the session
		if (count($this->_cart_contents) <= 2)
		{
			$this->CI->session->unset_userdata('cart_contents');

			// Nothing more to do... coffee time!
			return FALSE;
		}

		// If we made it this far it means that our cart has data.
		// Let's pass it to the Session class so it can be stored
		$this->CI->session->set_userdata(array('cart_contents' => $this->_cart_contents));

		// Woot!
		return TRUE;
	}
	
	
	/*public function _save_cart()
	{
		// Unset these so our total can be calculated correctly below
		unset($this->_cart_contents['total_items']);
		unset($this->_cart_contents['cart_total']);
		unset($this->_cart_contents['cart_total_no_discount']);
		unset($this->_cart_contents['cart_total_discount']);
		
		// Lets add up the individual prices and set the cart sub-total
		$total = 0;
		$total_discount = 0;
		$items = 0;
		foreach ($this->_cart_contents as $key => $val)
		{
			// We make sure the array contains the proper indexes
			if ( ! is_array($val) OR ! isset($val['price']) OR ! isset($val['qty']))
			{
				continue;
			}
			
			#Lets Get the actual product stored in the database
			$this->CI->db->where('prod_id', $val['id']);
			$get_product = $this->CI->db->get('tbl_product');
			
			#Extract the Price of the product
			foreach($get_product->result() as $gp)
			{
				$product_price = $gp->price;
			}
			
			$items += $val['qty'];
			
			#Discount Multiplier
			$discount_mult = 100 - $this->CI->config->item('discount_multi_percent');
			$discount = $discount_mult / 100;
			$price = $product_price * $discount;
			
			#Tax
			$tax_value = $this->CI->config->item('hst_tax') / 100;
			$tax_hst_discount = $price * $tax_value;
			$tax_hst = $product_price * $tax_value;
			
			if($items >= $this->CI->config->item('discount_multiple_items'))
			{
				$total += ($product_price * $val['qty'] + $tax_hst * $val['qty']);
				$total_discount += ($price * $val['qty'] + $tax_hst_discount * $val['qty']);
				$this->_cart_contents[$key]['price'] = $product_price;
				$this->_cart_contents[$key]['price_discount'] = $price;
				// Set the subtotal
				$this->_cart_contents[$key]['subtotal'] = ($this->_cart_contents[$key]['price'] * $this->_cart_contents[$key]['qty'] + $tax_hst * $this->_cart_contents[$key]['qty']);
				$this->_cart_contents[$key]['subtotal_discount'] = ($this->_cart_contents[$key]['price_discount'] * $this->_cart_contents[$key]['qty'] + $tax_hst_discount * $this->_cart_contents[$key]['qty']);
			}
			else
			{ 
				$total += ($product_price * $val['qty'] + $tax_hst * $val['qty']);
				$total_discount += ($price * $val['qty'] + $tax_hst_discount * $val['qty']);
				$this->_cart_contents[$key]['price'] = $product_price;
				$this->_cart_contents[$key]['price_discount'] = $price;
				// Set the subtotal
				$this->_cart_contents[$key]['subtotal'] = ($this->_cart_contents[$key]['price'] * $this->_cart_contents[$key]['qty'] + $tax_hst * $this->_cart_contents[$key]['qty']);
				$this->_cart_contents[$key]['subtotal_discount'] = ($this->_cart_contents[$key]['price_discount'] * $this->_cart_contents[$key]['qty'] + $tax_hst_discount * $this->_cart_contents[$key]['qty']);
			}
		}
		
		// Set the cart total and total items.
		$this->_cart_contents['total_items'] = $items;
		$this->_cart_contents['cart_total_no_discount'] = $total;
		$this->_cart_contents['cart_total_discount'] = $total_discount;
		$this->_cart_contents['cart_total'] = $total;
		
		// Is our cart empty?  If so we delete it from the session
		if (count($this->_cart_contents) <= 2)
		{
			$this->CI->session->unset_userdata('cart_contents');
			
			// Nothing more to do... coffee time!
			return FALSE;
		}
		
		// If we made it this far it means that our cart has data.
		// Let's pass it to the Session class so it can be stored
		$this->CI->session->set_userdata(array('cart_contents' => $this->_cart_contents));
		
		// Woot!
		return TRUE;
	}*/
	
}
