<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

// HFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHF
function hasTags($str) {
    return !(strcmp($str, strip_tags($str)) == 0);
}

function getConditionalFilter($id) {
    $CIH = &get_instance();
    $result = $CIH->db->query('
            SELECT *
            FROM tbl_cat_filter_detail
            WHERE filter_detail_title_id = ' . $id . '
        ');

    return $result;
}

function getConditionalFilterTitle($id) {
    $CIH = &get_instance();
    $result = $CIH->db->query('
            SELECT *
            FROM tbl_cat_filter_detail
            WHERE filter_detail_id = ' . $id . '
        ');
    $result = $result->row();
    return $result->filter_title;
}

function SEF_URL($text, $replace = array(), $delimiter = '-') {
    // replace non letter or digits by -
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

    // trim
    $text = trim($text, '-');

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // lowercase
    $text = strtolower($text);

    // remove unwanted characters
    // $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $text = preg_replace('~[^-\w]+~', '', $text);
    //$text = preg_replace('/[0-9]+/', '', $text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}

function check_URL($text, $replace = array(), $delimiter = '-') {
    // replace non letter or digits by -
    //$text = preg_replace('~[^\\pL\d]+~u', '-', $text);
    // trim
    $text = trim($text, '-');

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // lowercase
    $text = strtolower($text);

    // remove unwanted characters
    // $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $text = preg_replace('~[^-_0-9\w ]+~', '', $text);
    // $text = preg_replace('/[0-9_]+/', '', $text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}

function limited_text($value, $limit) {
    if (empty($value)) {
        return false;
    }
    if (empty($limit)) {
        return $value;
    }

    $desc = strip_tags($value);
    if (strlen($desc) > $limit) {
        return substr($desc, 0, $limit) . "...";
    }
    return $value;
}

function prod_img_check($image = '', $n = "") {
    $CI = &get_instance();

    $prod_img_path = base_url() . 'resources/prod_images/';

    /*
      0 = Orignal Image;
      1 = Thumb 1 image;
      2 = Thumb 2 image;
      3 = Thumb 3 image;
      4 = Thumb 4 image;
     */

    if ($image != '') {
        if (file_exists('resources/prod_images/' . $image) && $n != 0) {
            $prod_img_thumb_path = $prod_img_path . 'thumb/';

            if ($n == 1) {
                if (file_exists('resources/prod_images/thumb/' . 'thumb1_' . $image)) {
                    return $prod_img_thumb_path . "thumb1_" . $image;
                } else {
                    return $prod_img_thumb_path . "thumb1_no-image.jpg";
                }
            }

            if ($n == 2) {
                if (file_exists('resources/prod_images/thumb/' . 'thumb2_' . $image)) {
                    return $prod_img_thumb_path . "thumb2_" . $image;
                } else {
                    return $prod_img_thumb_path . "thumb2_no-image.jpg";
                }
            }

            if ($n == 3) {
                if (file_exists('resources/prod_images/thumb/' . 'thumb3_' . $image)) {
                    return $prod_img_thumb_path . "thumb3_" . $image;
                } else {
                    return $prod_img_thumb_path . "thumb3_no-image.jpg";
                }
            }

            if ($n == 4) {
                if (file_exists('resources/prod_images/thumb/' . 'thumb4_' . $image)) {
                    return $prod_img_thumb_path . "thumb4_" . $image;
                } else {
                    return $prod_img_thumb_path . "thumb4_no-image.jpg";
                }
            }
        } else {
            return $prod_img_path . $image;
        }
    } else {
        if ($n == 0) {
            $prod_img_thumb_path = $prod_img_path . 'thumb/';
            return $prod_img_thumb_path . 'thumb3_no-image.jpg';
        }
        if ($n == 1) {
            $prod_img_thumb_path = $prod_img_path . 'thumb/';
            return $prod_img_thumb_path . 'thumb1_no-image.jpg';
        }
        if ($n == 2) {
            $prod_img_thumb_path = $prod_img_path . 'thumb/';
            return $prod_img_thumb_path . 'thumb2_no-image.jpg';
        }
        if ($n == 3) {
            $prod_img_thumb_path = $prod_img_path . 'thumb/';
            return $prod_img_thumb_path . 'thumb3_no-image.jpg';
        }
        if ($n == 4) {
            $prod_img_thumb_path = $prod_img_path . 'thumb/';
            return $prod_img_thumb_path . 'thumb4_no-image.jpg';
        }
    }
}

function get_conversation($tablename, $col_name, $value, $order_by = '', $order = "DESC")
{
    $CI = &get_instance();
    $CI->db->select('*');
    $CI->db->from($tablename);
    $CI->db->where($col_name, $value);
    if ($order_by != '') {
        $CI->db->order_by($order_by, $order);
    }
    $query = $CI->db->get();
    return $query->row();
}

function generateFields($strLabel, $name, $value, $ntype = "", $options = "") {
    if ($ntype == "TextArea") {
        //echo $strLabel."-*-".$name."-*-".$value."-*-".$ntype."-*-".$options;    exit;
        $str = ' <div class="add-frm-wrp">
                        <label>' . $strLabel . '</label>
                        <div class="frm-s-editor" id="section1">
                             <textarea  class="" cols="110" rows="20" name="id' . $name . '" id="id' . $name . '">' . $value . '</textarea>
                                  <script>  CKEDITOR.replace( "id' . $name . '", {
                toolbar: [
                    [ "Bold", "Italic", "-", "NumberedList", "BulletedList"]
                ]
            } );</script>
                        </div>
                    </div>';
    }

    /*
      CKEDITOR.replace( 'editor1', {
      uiColor: '#14B8C4',
      toolbar: [
      [ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
      [ 'FontSize', 'TextColor', 'BGColor' ]
      ]
      });

     */ elseif ($ntype == "SelectBox") {
        $options = explode(",", $options);
        $str = '<div class="add-frm-wrp">
                      <label>' . $strLabel . '</label>
                              <div class="frm-s-sel">
                                <select name="id' . $name . '" class="category" data-name="category">';

        $str .= '<option value="">Select</option>';
        foreach ($options as $optValue) {
            $selected = "";
            if ($value == $optValue) {
                $selected = "selected='selected'";
            }

            $str .= '<option value="' . $optValue . '" ' . $selected . '>' . $optValue . '</option>';
        }
        $str .= ' </select>
                                </div>
                            </div> ';
    } else {
        //if($ntype=="TextField")
        $str = '<div class="f-s-ad-j nrow">
                            <label style="word-break:normal;">' . $strLabel . '</label>
                            <div class="frm-s-sel">
                                <input type="text" name="id' . $name . '"   value="' . $value . '" />
                            </div>
                    </div> ';
    }

    return $str;
}

function generateFieldsFront($strLabel, $name, $value, $ntype = "", $options = "") {
    if ($ntype == "TextArea") {
        //echo $strLabel."-*-".$name."-*-".$value."-*-".$ntype."-*-".$options;    exit;
        $str = ' <div class="mu-flds">
                        <label>' . $strLabel . '</label>
                        <div class="mu-fldInTxt" id="section1">
                             <textarea  class="" cols="110" rows="20" name="id' . $name . '" id="id' . $name . '">' . $value . '</textarea>
                                  <script>  CKEDITOR.replace( "id' . $name . '", {
                toolbar: [
                    [ "Bold", "Italic", "-", "NumberedList", "BulletedList"]
                ]
            } );</script>
                        </div>
                    </div>';
    }

    /*
      CKEDITOR.replace( 'editor1', {
      uiColor: '#14B8C4',
      toolbar: [
      [ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
      [ 'FontSize', 'TextColor', 'BGColor' ]
      ]
      });

     */ elseif ($ntype == "SelectBox") {
        $options = explode(",", $options);
        $str = '<div class="mu-flds">
                      <label>' . $strLabel . '</label>
                              <div class="mu-fldIn">
                                <select name="id' . $name . '" class="category" data-name="category">';

        $str .= '<option value="">Select</option>';
        foreach ($options as $optValue) {
            $selected = "";
            if ($value == $optValue) {
                $selected = "selected='selected'";
            }

            $str .= '<option value="' . $optValue . '" ' . $selected . '>' . $optValue . '</option>';
        }
        $str .= ' </select>
                                </div>
                            </div> ';
    } else {
        //if($ntype=="TextField")
        $str = '<div class="mu-flds">
                            <label style="word-break:normal;">' . $strLabel . '</label>
                            <div class="mu-fldIn">
                                <input type="text" name="id' . $name . '"   value="' . $value . '" />
                            </div>
                    </div> ';
    }

    return $str;
}

function UrlRferer() {
    $CI = &get_instance();

    $uri_string = uri_string();

    $arr['login'] = "login";
    /* $arr['restpassword']="restpassword";
      $arr['loginout']="loginout";
      $arr['loginout']="forgotpassword"; */

    if (!in_array($uri_string, $arr)) {
        $CI->session->set_userdata('refererUrl', @$_SERVER['HTTP_REFERER']);

        //$uri_string=uri_string(@$_SERVER['HTTP_REFERER']);
        //if($uri_string!="login" && $uri_string!="restpassword" && $uri_string!="loginout")
        {

            //$CI->session->set_userdata('refererUrl',@$_SERVER['HTTP_REFERER']);
            //$CI->session->userdata('refererUrl');
        }
    }

    /* $mystring = @$_SERVER['HTTP_REFERER'];
      $findmeArr[]  = base_url().'login';
      $findmeArr[]  = base_url().'restpassword';

      foreach($findmeArr as $findme)
      {
      $pos = strpos($mystring, $findme);
      if ($pos === false)
      {
      $bCheck=1;
      $this->session->set_userdata('refererUrl',@$_SERVER['HTTP_REFERER']);
      }
      } */
}

function clean($string) {
    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}

function clean_special_character($string) {
    return preg_replace('/[^(\x20-\x7F)]*/', '', $string);
}

function clean2($string) {
    //$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    return $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    //preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}

function clean3($string) {
    //$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
    return $string = preg_replace('/[^A-Za-z0-9\-]/', ' ', $string); // Removes special chars.
    //preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}

function clean4($string) {
    $string = str_replace('&nbsp;', '-', $string); // Replaces all spaces with hyphens.
    return $string = preg_replace('/[^A-Za-z0-9\-]/', ' ', $string); // Removes special chars.
    //preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
}

function dbQuery($sql) {
    $CI = &get_instance();
    return $query = $CI->db->query($sql);
}

function space($n = 0) {
    $str = "";
    for ($n1 = 0; $n1 <= $n; $n1++) {
        $str .= "&nbsp;";
    }

    return $str;
}

function xml2array($contents, $get_attributes = 1, $priority = 'tag') {
    if (!$contents) {
        return array();
    }

    if (!function_exists('xml_parser_create')) {
        //print "'xml_parser_create()' function not found!";
        return array();
    }

    //Get the XML parser of PHP - PHP must have this module for the parser to work
    $parser = xml_parser_create('');
    xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
    xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
    xml_parse_into_struct($parser, trim($contents), $xml_values);
    xml_parser_free($parser);

    if (!$xml_values) {
        return;
    }
//Hmm...
    //Initializations
    $xml_array = array();
    $parents = array();
    $opened_tags = array();
    $arr = array();

    $current = &$xml_array; //Refference
    //Go through the tags.
    $repeated_tag_index = array(); //Multiple tags with same name will be turned into an array
    foreach ($xml_values as $data) {
        unset($attributes, $value); //Remove existing values, or there will be trouble
        //This command will extract these variables into the foreach scope
        // tag(string), type(string), level(int), attributes(array).
        extract($data); //We could use the array by itself, but this cooler.

        $result = array();
        $attributes_data = array();

        if (isset($value)) {
            if ($priority == 'tag') {
                $result = $value;
            } else {
                $result['value'] = $value;
            }
            //Put the value in a assoc array if we are in the 'Attribute' mode
        }

        //Set the attributes too.
        if (isset($attributes) and $get_attributes) {
            foreach ($attributes as $attr => $val) {
                if ($priority == 'tag') {
                    $attributes_data[$attr] = $val;
                } else {
                    $result['attr'][$attr] = $val;
                }
                //Set all the attributes in a array called 'attr'
            }
        }

        //See tag status and do the needed.
        if ($type == "open") {
//The starting of the tag '<tag>'
            $parent[$level - 1] = &$current;
            if (!is_array($current) or ( !in_array($tag, array_keys($current)))) {
                //Insert New tag
                $current[$tag] = $result;
                if ($attributes_data) {
                    $current[$tag . '_attr'] = $attributes_data;
                }

                $repeated_tag_index[$tag . '_' . $level] = 1;

                $current = &$current[$tag];
            } else {
                //There was another element with the same tag name

                if (isset($current[$tag][0])) {
//If there is a 0th element it is already an array
                    $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;
                    $repeated_tag_index[$tag . '_' . $level] ++;
                } else {
//This section will make the value an array if multiple tags with the same name appear together
                    $current[$tag] = array($current[$tag], $result); //This will combine the existing item and the new item together to make an array
                    $repeated_tag_index[$tag . '_' . $level] = 2;

                    if (isset($current[$tag . '_attr'])) {
                        //The attribute of the last(0th) tag must be moved as well
                        $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                        unset($current[$tag . '_attr']);
                    }
                }
                $last_item_index = $repeated_tag_index[$tag . '_' . $level] - 1;
                $current = &$current[$tag][$last_item_index];
            }
        } elseif ($type == "complete") {
            //Tags that ends in 1 line '<tag />'
            //See if the key is already taken.
            if (!isset($current[$tag])) {
                //New Key
                $current[$tag] = $result;
                $repeated_tag_index[$tag . '_' . $level] = 1;
                if ($priority == 'tag' and $attributes_data) {
                    $current[$tag . '_attr'] = $attributes_data;
                }
            } else {
                //If taken, put all things inside a list(array)
                if (isset($current[$tag][0]) and is_array($current[$tag])) {
                    //If it is already an array...
                    // ...push the new element into that array.
                    $current[$tag][$repeated_tag_index[$tag . '_' . $level]] = $result;

                    if ($priority == 'tag' and $get_attributes and $attributes_data) {
                        $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                    }
                    $repeated_tag_index[$tag . '_' . $level] ++;
                } else {
                    //If it is not an array...
                    $current[$tag] = array($current[$tag], $result); //...Make it an array using using the existing value and the new value
                    $repeated_tag_index[$tag . '_' . $level] = 1;
                    if ($priority == 'tag' and $get_attributes) {
                        if (isset($current[$tag . '_attr'])) {
                            //The attribute of the last(0th) tag must be moved as well

                            $current[$tag]['0_attr'] = $current[$tag . '_attr'];
                            unset($current[$tag . '_attr']);
                        }

                        if ($attributes_data) {
                            $current[$tag][$repeated_tag_index[$tag . '_' . $level] . '_attr'] = $attributes_data;
                        }
                    }
                    $repeated_tag_index[$tag . '_' . $level] ++; //0 and 1 index is already taken
                }
            }
        } elseif ($type == 'close') {
            //End of tag '</tag>'
            $current = &$parent[$level - 1];
        }
    }

    return ($xml_array);
}

function auth() {
    $CIH = &get_instance();

    if ( !empty($_GET) ) $_GET = array_stripslash($_GET);
    if ( !empty($_POST) ) $_POST = array_stripslash($_POST);

    $admin_id = $CIH->session->userdata('admin_id');
    if ($admin_id == '') {

        header("Location: " . base_url() . "admin/login");
        exit;
    }
}

function read_only_user() {
    $CIH = &get_instance();
    $admin_id = $CIH->session->userdata('admin_type');
    if ($admin_id == 0) {

        header("Location: " . base_url() . "admin/login");
        exit;
    }
}

function auth_admin_type($path) {
    $CIH = &get_instance();
    $admin_type = $CIH->session->userdata('admin_type');
    if ($admin_type != '1') {

        header("Location: " . base_url() . $path);
        exit;
    }
}

function authFront() {
    delete_cookie('img_counter');
    $CIH = &get_instance();

    $userid = $CIH->session->userdata('userid');

    if ( !empty($_GET) ) $_GET = array_stripslash($_GET);
    if ( !empty($_POST) ) $_POST = array_stripslash($_POST);

    if ($userid == '') {
        header("Location: " . base_url());
        exit;
    } else {
        $is_user = $CIH->db->query("
                                SELECT *
                                FROM tbl_user
                                WHERE
                                user_id = " . $userid . "
                                AND
                                user_status = 1 AND user_is_delete = 0
                                ");
        if ($is_user->num_rows() <= 0) {
            header("Location: " . base_url("logout"));
            exit;
        }else{
            $_SESSION['usertype'] = $is_user->row()->user_type;
        }
    }
}

function sellerFront() {
    $CIH = &get_instance();
    $userid = $CIH->session->userdata('userid');
    if ($userid == '') {
        header("Location: " . base_url());
        exit;
    } else {
        $is_user = $CIH->db->query("
                    SELECT *
                                    FROM tbl_user
                                    WHERE
                                    user_id = " . $userid . "
                                    AND
                                    user_status = 1
                                    AND user_type = 'Buyer'
                                    ");
        if ($is_user->num_rows() <= 0) {
            header("Location: " . base_url("seller-dashboard"));
        }
    }
}

/* function authcart()
  {
  $CIH = & get_instance();
  $userid = $CIH->session->userdata('userid');
  $guestid = $CIH->session->userdata('guest_checkout');
  if($userid == '' )
  {
  header("Location: ".base_url("login"));
  exit;
  }
  else
  {
  $is_user = $CIH->db->query("
  SELECT *
  FROM tbl_user
  WHERE
  user_id = ".$userid."
  AND
  user_status = 1
  ");
  if($is_user->num_rows() <= 0)
  {
  $CIH->load->library('facebook');
  $CIH->facebook->destroySession();

  $sessiondata = array(
  'userid'  => '',
  'useremail' => '',
  'userfname' => '',
  'facebookid' => '',
  'usertype' => '',
  'is_user_loggedin' => false,
  'logged_in' => false,
  'user_status' => '',
  );
  $CIH->session->unset_userdata($sessiondata);
  $CIH->cart->destroy();
  header("Location: ".base_url("login"));
  exit;
  }
  }
  }

  /*$guest_session_data = array(

  'useremail' => $this->input->post('guest_email'),
  'guest_checkout' => TRUE,
  'thank_pass_check' => TRUE
  ); */

function authForSite() {
    $CIH = &get_instance();

    if ($CIH->agent->is_referral()) {
        $CIH->load->library('uri');
        $CIH->config->load('test_config');

        $filter_array = $CIH->config->item('referal_url');

        if (in_array($CIH->uri->segment(1), $filter_array) == true) {
            $url_val = $CIH->config->item('base_url') . $CIH->uri->uri_string();
        } else if ($CIH->uri->uri_string() == '') {
            $url_val = $CIH->config->item('base_url') . 'accountinfo';
        }
        //echo $url_val;

        if (isset($url_val)) {
            $CIH->session->set_userdata('referrer', $url_val);
        }
    }

    if ($CIH->session->userdata('is_user_loggedin') == false) {
        header("Location: " . base_url('/comingsoon'));
        exit;
    }
}

function saveDate($strdate) {
    $arr = explode('-', $strdate);
    return $strdate = @$arr[2] . "-" . @$arr[0] . "-" . @$arr[1];
}

function showDate($strdate) {
    $arr = explode("-", $strdate);
    return @$arr[1] . "-" . @$arr[2] . "-" . @$arr[0];
}

function showDateField($strdate) {
    $arr = explode("-", $strdate);
    $date = @$arr[1] . "-" . @$arr[2] . "-" . @$arr[0];
    if ($date == "//") {
        $date = "";
    }

    return $date;
}

function showTime($strTime) {
    $arr = explode(":", $strTime);
    $strMints = "";
    if (@$arr[1] > 0) {
        $strMints = ":" . $arr[1];
    } else {
        $strMints = ":00";
    }

    if (@$arr[0] > 12) {

        $arr[0] = $arr[0] - 12;

        if (intval($arr[0]) < 10) {
            $arr[0] = "0" . $arr[0];
        }

        return $arr[0] . $strMints . " PM";
    } else {
        if (@$arr[0] > 0 && @$arr[0] < 12) {
            return $arr[0] . $strMints . " AM";
        }

        if (@$arr[0] == 12) {
            return $arr[0] . $strMints . " PM";
        } else {
            return "";
        }
    }
}

function sendMessageEmailAlert($toEmail, $name = "") {
    $CIH = &get_instance();
    $nResult = $CIH->common_model->query("SELECT *  FROM  tbl_emailtemplate WHERE e_id = 2  AND  e_on_off = 'On' ");
    if ($nResult->num_rows() > 0) {
        $rstRow = $nResult->row();
        $data_save = null;
        $subject = $rstRow->e_title;
        $message = $rstRow->e_desc;
        $from_name = GetSiteParameter(1);
        $from_email = GetSiteParameter(3);
        $CIH->load->library('email');
        $CIH->email->set_mailtype('html');
        $CIH->email->from($from_email, $from_name);
        $CIH->email->to($toEmail);
        $CIH->email->subject($name . " " . $subject);
        $CIH->email->message($message);
        $CIH->email->send();
        //echo $CIH->email->print_debugger();
    }
}

function sendEmailAlert($data) {
    $CIH = &get_instance();
    $nResult = $CIH->common_model->query("SELECT *  FROM  tbl_emailtemplate WHERE e_id = 2  AND  e_on_off = 'On' ");
    if ($nResult->num_rows() > 0) {
        $rstRow = $nResult->row();
        $data_save = null;
        $subject = $rstRow->e_title;
        $message = $rstRow->e_desc;
        $from_name = GetSiteParameter(1);
        $from_email = GetSiteParameter(3);
        $CIH->load->library('email');
        $CIH->email->set_mailtype('html');
        $CIH->email->from($from_email, $from_name);
        $CIH->email->to($toEmail);
        $CIH->email->subject($name . " " . $subject);
        $CIH->email->message($message);
        $CIH->email->send();
        //echo $CIH->email->print_debugger();
    }
}

function master_auth() {
    $CIH = &get_instance();
    $admin_id = $CIH->session->userdata('admin_id');
    $is_master = $CIH->session->userdata('is_master');
    if ($is_master != 'yes') {
        $CIH->session->set_userdata('msg', "Access Denied.");
        header("Location: " . base_url() . "admin/home");
        exit;
    }
}

function user_auth() {
    $CIH = &get_instance();
    $user_id = $CIH->session->userdata('user_id');
    if ($user_id == '') {

        $CIH->session->set_flashdata('msg', 'Please Login First');
        header("Location: " . base_url() . "home");
        exit;
    }
}

// HFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHF
function SEF_URLS($str, $replace = array(), $delimiter = '-') {
    $str = trim($str);
    if (!empty($replace)) {
        $str = str_replace((array) $replace, ' ', $str);
    }

    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
    return $clean;
}

// HFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHF
function page_sefurl($pg_id) {
    $CI = &get_instance();
    $query = $CI->db->query("select pg_sefurl from tbl_pages where pg_id=" . (int) $pg_id);
    if ($query->num_rows() > 0) {
        $row = $query->row();
        return $row->pg_sefurl;
    }
}

// HFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHF
function GetSiteImage($image_id) {
    $CI = &get_instance();
    $query = $CI->db->query("select simage_file from tbl_site_images where simage_id=" . (int) $image_id);
    if ($query->num_rows() > 0) {
        $row = $query->row();
        return stripslashes($row->simage_file);
    }
}

function GetSiteParameter($param_id) {
    $CI = &get_instance();
    $query = $CI->db->query("select value as param_value from tbl_parameters where id=" . (int) $param_id);
    if ($query->num_rows() > 0) {
        $row = $query->row();
        return stripslashes($row->param_value);
    }
}

// HFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHF
function GetImageThumb($image = '', $n = "") {
    if ($image != '') {
        $array = explode(".", $image);
        if ($n == 1) {
            return $array[0] . "_thumb1" . "." . $array[1];
        } else {
            return $array[0] . "_thumb" . "." . $array[1];
        }
    }
}

// HFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHF
function splitText($text, $maxLength) {
    /* Make sure that the string will not be longer
      than $maxLength.
     */
    if (strlen($text) > $maxLength) {
        /* Trim the text to $maxLength characters */
        $text = substr($text, 0, $maxLength - 1);

        /* Split words only at boundaries. This will be
          accomplished by moving back each character from
          the end of the split string until a space is found.
         */
        while (substr($text, -1) != ' ') {
            $text = substr($text, 0, -1);
        }

        /* Remove the whitespace at the end. */
        $text = rtrim($text);
    }
    return $text;
}

// HFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHF

function randomPrefix($length) {
    $random = "";
    srand((double) microtime() * 1000000);
    $data = "abcdefghijklmn123opq45rs67tuv89wxyz";

    for ($i = 0; $i < $length; $i++) {
        $random .= substr($data, (rand() % (strlen($data))), 1);
    }

    return $random;
}

// HFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHFHF
function calcDateDiff($iStartDate, $iEndDate) {

    /* if (!is_int($iStartDate) || !is_int($iEndDate))
      {
      trigger_error('Invalid arguments for ' . __FUNCTION__);
      return false;
      } */

    $aResult = array('year' => &$year, 'month' => &$month, 'day' => &$day);
    $year = abs(date('Y', $iStartDate) - date('Y', $iEndDate));

    // Fetch the month.  If its < 0 difference, add 12 months and decrement the year
    if (0 > ($month = abs(date('m', $iStartDate) - date('m', $iEndDate)))) {
        $month += 12;
        --$year;
    }

    $dateDiff = $iEndDate - mktime(0, 0, 0, date('m', $iStartDate) + $month, date('d', $iStartDate), date('Y', $iStartDate) + $year);
    $day = date('z', abs($dateDiff));

    return $aResult;
}

function ckeditor($strId) {
    return "<script type='text/javascript'>
                    CKEDITOR.replace( '" . $strId . "',
                     {
                        filebrowserBrowseUrl : '" . base_url() . "ckfinder/ckfinder.html',
                        filebrowserImageBrowseUrl : '" . base_url() . "ckfinder/ckfinder.html?Type=Images',
                        filebrowserFlashBrowseUrl : '" . base_url() . "ckfinder/ckfinder.html?Type=Flash',
                        filebrowserUploadUrl : '" . base_url() . "ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                        filebrowserImageUploadUrl : '" . base_url() . "ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                        filebrowserFlashUploadUrl : '" . base_url() . "ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                    });
            </script>";
}

function recordEdits($nId, $rstRowBefore) {
    $arrFields = $this->config->item('diamondHistroy');
    $rstRowAfter = $this->common_model->commonselect("tbl_users", "user_id", $nId)->result_array();
    $rstGetStater = $this->common_model->commonselect("tblgetstarted", "get_user_id", (int) $nId)->row();
    $arrResultAfter = array_diff_assoc($rstRowAfter[0], $rstRowBefore[0]);
    $arrResultBefore = array_diff_assoc($rstRowBefore[0], $rstRowAfter[0]);
    // print_r($arrResultAfter);
    //echo "<br><br><br>";
    // print_r($arrResult2);
    foreach ($arrResultAfter as $key => $value) {
        //echo $key => $value;

        $array = "";
        $array['his_field'] = @$arrFields[$key];
        $array['his_before'] = $arrResultBefore[$key];
        $array['his_after'] = $arrResultAfter[$key];
        $array['his_user_id'] = $this->session->userdata("user_id");
        $array['his_prod_id'] = $rstGetStater->get_id;
        $array['his_url'] = current_url();
        $array['his_date_time'] = date("Y-m-d H:i:s");
        $this->common_model->commonSave('tbl_history_edits', $array);
    }
}

############################################################################################
##############################  Image Resize function  #####################################
############################################################################################
if (!function_exists('ratio_image_resize')) {

    function ratio_image_resize($image_path, $new_path, $file_name, $width, $height) {
        $CI = &get_instance();

        $image_data = getimagesize($image_path);
        $img_width = $image_data[0];
        $img_height = $image_data[1];

        if ($width == '' && $height == '') {
            return false;
        } else if ($width != '' && $height != '') {
            $reqwidth = $width;
            $reqheight = $height;
        } else {
            if ($width != '' && $height == '') {
                $nwidth = $img_width - $width;
                $pwidth = ($nwidth / $img_width) * 100;
                $pheight = ($pwidth / 100) * $img_height;
                $nheight = $img_height - $pheight;

                if ($img_height <= $nheight) {
                    $reqwidth = $width;
                    $reqheight = $img_height;
                }
                if ($img_height >= $nheight) {
                    $reqwidth = $width;
                    $reqheight = $nheight;
                }
            } else if ($height != '' && $width == '') {
                $nheight = $img_height - $height;
                $pheight = ($nheight / $img_height) * 100;
                $pwidth = ($pheight / 100) * $img_width;
                $nwidth = $img_width - $pwidth;

                if ($img_width >= $nwidth) {
                    $reqwidth = $nwidth;
                    $reqheight = $height;
                }
                if ($img_width <= $nwidth) {
                    $reqwidth = $img_width;
                    $reqheight = $height;
                }
            }
        }

        $config['width'] = $reqwidth;
        $config['height'] = $reqheight;
        $config['source_image'] = $image_path;
        $config['new_image'] = $new_path . $file_name;
        //$config['thumb_marker'] = "_thumb";
        $config['image_library'] = 'gd2';
        $config['create_thumb'] = false;
        $config['maintain_ratio'] = false;
        $config['quality'] = '100%';

        $CI->load->library('image_lib', $config);
        $CI->image_lib->initialize($config);

        if (!$CI->image_lib->resize()) {
            $error = array('error' => $CI->image_lib->display_errors());
            return $error;
        } else {
            return $file_name;
        }
    }

}

function get_social_media_links() {
    $ci = &get_instance();
    $ci->db->select('*');
    $ci->db->from('tbl_site_setting');
    $query = $ci->db->get();
    $result = $query->row();
    return $result;
}

function check_unique_url($pg_url) {
    $url = array('forgot-password', 'admin', 'customers_details', 'customers_add', 'customers_delete', 'customers_orders', 'customers_address', 'check-register-email', 'register-data', 'my-dashboard', 'logout', 'facebook-login', 'login-user', 'messages', 'seller', 'Career', 'retrive-password', 'reset-password');

    if (in_array($pg_url, $url)) {
        return true;
    } else {
        return false;
    }
}

/*function check_unique_cat_url($pg_url) {
    $ci = &get_instance();
    $query = "SELECT *
            FROM $table
            WHERE $column = $pg_id";
    $results = $ci->db->query($query);

    if (in_array($pg_url, $url)) {
        return true;
    } else {
        return false;
    }
}*/

function get_page_url($pg_id = '', $table = '', $column = '') {
    $ci = &get_instance();
    $query = "SELECT *
            FROM $table
            WHERE $column = $pg_id";
    $results = $ci->db->query($query);
    return $results->row();
}

function get_user_data($user_id = '') {
    $ci = &get_instance();
    $query = "SELECT *
            FROM tbl_stores
            WHERE user_id = $user_id";

    $data['results'] = $ci->db->query($query)->row();
    if (count($data['results']) != 0) {
        $data['store'] = 1;

        $query = "SELECT *
            FROM tbl_user
            WHERE user_id = $user_id";

        $data['store_user'] = $ci->db->query($query)->row();

        return $data;
    } else {
        $query = "SELECT *
            FROM tbl_user
            WHERE user_id = $user_id";

        $data['results'] = $ci->db->query($query)->row();

        $data['store'] = 0;
        return $data;
    }
}

function check_extension($filename = '') {
    $path_info = pathinfo($filename);
    if ($filename != '') {
        if ($path_info['extension'] == 'doc' || $path_info['extension'] == 'docx') {
            return "doc.png";
        } elseif ($path_info['extension'] == 'pdf') {
            return "pdf_ico.png";
        } elseif ($path_info['extension'] == 'txt') {
            return "txt-ico.png";
        } elseif ($path_info['extension'] == 'png' || $path_info['extension'] == 'jpg' || $path_info['extension'] == 'jpeg') {
            return "image-ico.png";
        } elseif ($path_info['extension'] == 'ppt') {
            return "ppt-ico.png";
        } else {
            return "file_thumbnail.jpg";
        }
    } else {
        return "file_thumbnail.jpg";
    }
}

function send_email($to = '', $from = '', $subject = '', $message = '') {
    $ci = get_instance();
    $config = array(
        'protocol' => 'smtp',
        //'smtp_host'=>'smtp.mailgun.org',
        'smtp_host' => 'mail.educki.com',
        'smtp_port' => 465,
        '_smtp_auth' => true,
        'smtp_user' => 'noreply@educki.com',
        'smtp_pass' => 'nFAy_$G8Nv$J',
        'smtp_crypto' => 'ssl',
        'mailtype' => 'html',
        'charset' => 'utf-8',
        'validate' => true,
    );

    $msg = str_replace('../../../', base_url(), $message);
    $msg = str_replace('../../', base_url(), $msg);
    $ci->load->library('email');
    $ci->email->initialize($config);
    $ci->email->set_newline("\r\n");
    $ci->email->from($from);
    $ci->email->to($to);
    $ci->email->set_mailtype("html");
    $ci->email->subject($subject);
    $ci->email->message($msg);
    $is_send = $ci->email->send();
    return $is_send;
}

function send_email_2($to = '', $from = '', $subject = '', $message = '', $files = '') {
    $ci = get_instance();
    $config = array(
        'protocol' => 'smtp',
        //'smtp_host'=>'smtp.mailgun.org',
        'smtp_host' => 'mail.educki.com',
        'smtp_port' => 465,
        '_smtp_auth' => true,
        'smtp_user' => 'noreply@educki.com',
        'smtp_pass' => 'nFAy_$G8Nv$J',
        'smtp_crypto' => 'ssl',
        'mailtype' => 'html',
        'charset' => 'utf-8',
        'validate' => true,
    );
    $ci->load->library('email');
    $ci->email->initialize($config);
    $ci->email->set_newline("\r\n");
    $ci->email->from($from);
    $ci->email->to($to);
    $ci->email->set_mailtype("html");
    $ci->email->subject($subject);
    $ci->email->message($message);
    if ($files != '') {
        $ci->email->attach($files);
    }
    $is_send = $ci->email->send();
    return $is_send;
}

function check_wishlist_item($prod_id = '') {
    $ci = &get_instance();
    $user_id = $ci->session->userdata('userid');
    $query = "SELECT *
            FROM tbl_wish_list
            WHERE product_id = $prod_id AND user_id = $user_id";
    $results = $ci->db->query($query);
    if (count($results->row()) > 0) {
        return true;
    } else {
        return false;
    }
}

function get_store_user_id($store_url = '') {
    $ci = &get_instance();
    $query = "SELECT *
            FROM tbl_stores
            WHERE store_url = '$store_url'";
    $results = $ci->db->query($query);
    return $results->row();
}

function totalCartItems() {
    $CI = & get_instance();
    $CI->load->model('front/cart/cart_model');
    $cartId = $CI->session->userdata('cartId');
     if ($cartId == '') {
        $cartId = @$_COOKIE['educki_cartId'];
    }
    $data['cart_product_details'] = getProductPrices();
    
    foreach ($data['cart_product_details'] as $product) {
        $product_details = getproduct_details_2($product->cart_prod_id);
        
        if ($product_details == '') {
            $CI->cart_model->removeCartItem($cartId, $product->cart_prod_id, $product->cart_detail_id);
        }
    }

//    var_dump($cartId);die;
    return $CI->db->query("SELECT SUM(cart_prod_qnty) AS total_cart_items FROM tbl_cart_detail cd
            JOIN tbl_products p ON cd.cart_prod_id = p.prod_id
            JOIN tbl_cart c ON cd.cart_id = c.cart_id
            WHERE c.cart_id = '" . $cartId . "' ")->row('total_cart_items');
}

function getProductPrices() {
    $CI = & get_instance();
    $cartId = $CI->session->userdata('cartId');
    if ($cartId == '') {
            $cartId = @$_COOKIE['educki_cartId'];
        }   
    return $CI->db->query("SELECT cd.cart_prod_id,cd.cart_detail_id,cd.cart_details,cd.cart_id, cd.cart_prod_qnty, 
                (
                CASE
                 WHEN p.prod_onsale = 1 && p.sale_start_date <= '" . date('Y-m-d') . "' && p.sale_end_date >= '" . date('Y-m-d') . "' THEN p.sale_price
                 WHEN p.prod_onsale = 1 && p.sale_start_date = '' && p.sale_end_date = '' THEN p.sale_price
                 ELSE p.prod_price
                END
                ) AS unit_prod_price,
                (
                CASE
                 WHEN p.prod_onsale = 1 && p.sale_start_date <= '" . date('Y-m-d') . "' && p.sale_end_date >= '" . date('Y-m-d') . "' THEN p.sale_price * cd.cart_prod_qnty
                 WHEN p.prod_onsale = 1 && p.sale_start_date = '' && p.sale_end_date = '' THEN p.sale_price * cd.cart_prod_qnty
                 ELSE p.prod_price * cd.cart_prod_qnty
                END
                ) AS total_prod_price  FROM tbl_cart_detail cd
                JOIN tbl_products p ON cd.cart_prod_id = p.prod_id
                JOIN tbl_cart c ON cd.cart_id = c.cart_id
                WHERE c.cart_id = '" . $cartId . "' ")->result();
}

function getCartSubTotal() {
    $CI = & get_instance();
    $cartId = $CI->session->userdata('cartId');
    if ($cartId == '') {
        $cartId = @$_COOKIE['educki_cartId'];
    }

    $query = $CI->db->query("SELECT 
                SUM(
                CASE
                 WHEN p.prod_onsale = 1 && p.sale_start_date <= '" . date('Y-m-d') . "' && p.sale_end_date >= '" . date('Y-m-d') . "' THEN p.sale_price * cd.cart_prod_qnty
                 WHEN p.prod_onsale = 1 && p.sale_start_date = '' && p.sale_end_date = '' THEN p.sale_price * cd.cart_prod_qnty
                 ELSE p.prod_price * cd.cart_prod_qnty
                END
                ) AS cart_sub_total  FROM tbl_cart_detail cd
                JOIN tbl_products p ON cd.cart_prod_id = p.prod_id
                JOIN tbl_cart c ON cd.cart_id = c.cart_id
                WHERE c.cart_id = '" . $cartId . "' ")->row();
    return number_format($query->cart_sub_total,2,'.','');
}

function getSiteLogo() {
    $CI = & get_instance();
    $query = $CI->db->query("select site_logo from tbl_site_setting where id=1");
    if ($query->num_rows() > 0) {
        $row = $query->row();
        return $row->site_logo;
    }
}

function getSiteSettings($column_name) {
    $CI = & get_instance();
    $query = $CI->db->query("select " . $column_name . " as column_name from tbl_site_setting where id=1");
    if ($query->num_rows() > 0) {
        $row = $query->row();
        return $row->column_name;
    }
}

function getproduct_details($prod_id) {
    $CI = & get_instance();
    $query = "SELECT 
                prod.*,
                category.cat_url AS catURL,
                subCat.cat_url AS subCatURL,
                subSubCat.cat_url AS subSubCatURL,
                users.user_fname,
                users.user_lname,
                store.store_url,
                store.store_name,
                store.store_id,
                prod_images.img_name,
                prod.qty
              FROM
                tbl_products prod 
                INNER JOIN tbl_product_category prodCat 
                  ON prod.prod_id = prodCat.prod_id 
                INNER JOIN tbl_categories category 
                  ON prodCat.category_id = category.cat_id 
                LEFT JOIN tbl_categories subCat 
                  ON prodCat.sub_category_id = subCat.cat_id 
                LEFT JOIN tbl_categories subSubCat 
                  ON prodCat.sub_sub_category_id = subSubCat.cat_id 
                  INNER JOIN tbl_user users
                  ON users.user_id = prod.prod_user_id
                  INNER JOIN tbl_stores store
                  ON store.user_id = users.user_id
                  INNER JOIN tbl_product_images prod_images
                  ON prod_images.img_prod_id = prod.prod_id
              WHERE prod_is_delete = 0 
                AND prod.prod_status = 1
                AND prod.qty > 0
                AND store.store_is_hide = 0
                AND prod.prod_id = " . $prod_id . "
                GROUP BY prod.prod_id
                ORDER BY prod.prod_id DESC";
    return $CI->db->query($query)->row();
}

function getProdName($prod_id) {
    $CI = & get_instance();
    $query = "SELECT 
                prod_title
                FROM
                tbl_products
              WHERE prod_id = " . $prod_id . "
            ";
    return $CI->db->query($query)->row();
}

function getproduct_details_2($prod_id) {
    $CI = & get_instance();
    $query = "SELECT 
                prod.*,
                category.cat_url AS catURL,
                subCat.cat_url AS subCatURL,
                subSubCat.cat_url AS subSubCatURL,
                users.user_fname,
                users.user_lname,
                store.store_url,
                store.store_name,
                prod_images.img_name,
                prod.qty
              FROM
                tbl_products prod 
                INNER JOIN tbl_product_category prodCat 
                  ON prod.prod_id = prodCat.prod_id 
                INNER JOIN tbl_categories category 
                  ON prodCat.category_id = category.cat_id 
                LEFT JOIN tbl_categories subCat 
                  ON prodCat.sub_category_id = subCat.cat_id 
                LEFT JOIN tbl_categories subSubCat 
                  ON prodCat.sub_sub_category_id = subSubCat.cat_id 
                  INNER JOIN tbl_user users
                  ON users.user_id = prod.prod_user_id
                  INNER JOIN tbl_stores store
                  ON store.user_id = users.user_id
                  INNER JOIN tbl_product_images prod_images
                  ON prod_images.img_prod_id = prod.prod_id
              WHERE users.user_is_delete = 0
              AND users.user_status = 1
                AND prod.prod_status = 1
                AND prod.prod_is_delete = 0
                AND prod.qty > 0
                AND store.store_is_hide = 0
                AND prod.prod_id = " . $prod_id . "
                GROUP BY prod.prod_id
                ORDER BY prod.prod_id DESC";
    return $CI->db->query($query)->row();
}

function cgetproduct_details_2($prod_id) {
    $CI = & get_instance();
    $query = "SELECT 
                prod.*,
                category.cat_url AS catURL,
                subCat.cat_url AS subCatURL,
                subSubCat.cat_url AS subSubCatURL,
                users.user_fname,
                users.user_lname,
                store.store_url,
                store.store_name,
                prod_images.img_name,
                prod.qty
              FROM
                tbl_products prod 
                INNER JOIN tbl_product_category prodCat 
                  ON prod.prod_id = prodCat.prod_id 
                INNER JOIN tbl_categories category 
                  ON prodCat.category_id = category.cat_id 
                LEFT JOIN tbl_categories subCat 
                  ON prodCat.sub_category_id = subCat.cat_id 
                LEFT JOIN tbl_categories subSubCat 
                  ON prodCat.sub_sub_category_id = subSubCat.cat_id 
                  INNER JOIN tbl_user users
                  ON users.user_id = prod.prod_user_id
                  INNER JOIN tbl_stores store
                  ON store.user_id = users.user_id
                  INNER JOIN tbl_product_images prod_images
                  ON prod_images.img_prod_id = prod.prod_id
              WHERE users.user_is_delete = 0
              AND users.user_status = 1
                AND prod.prod_status = 1
                AND prod.prod_is_delete = 0
                AND prod.qty > 0
                AND store.store_is_hide = 0
                AND prod.prod_id = " . $prod_id . "
                GROUP BY prod.prod_id
                ORDER BY prod.prod_id DESC";
    return $CI->db->query($query)->result();
}

function get_shipment_sub_total() {

    $CI = & get_instance();
    $cartId = $CI->session->userdata('cartId');
    if ($cartId == '') {
            $cartId = @$_COOKIE['educki_cartId'];
        }   
    return $CI->db->query("SELECT  SUM(
            CASE
            WHEN p.prod_status = 1 && p.shipping = 'Charge for Shipping'
            THEN p.ship_price
            ELSE 0
            END
                  ) AS shipping_sub_total
          FROM
            tbl_cart_detail cd 
            JOIN tbl_products p 
              ON cd.cart_prod_id = p.prod_id
            JOIN tbl_cart c 
              ON cd.cart_id = c.cart_id 
          WHERE c.cart_id =  '" . $cartId . "' ")->row('shipping_sub_total');
}

function productDetailUrl($prod_id = '') {
    $CI = & get_instance();
    $query = "
                SELECT 
                prod.prod_url, category.cat_url as catURL, subCat.cat_url as subCatURL, subSubCat.cat_url as subSubCatURL
                FROM tbl_products prod
                INNER JOIN tbl_product_category prodCat ON prod.prod_id = prodCat.prod_id
                INNER JOIN tbl_categories category ON prodCat.category_id = category.cat_id
                LEFT JOIN tbl_categories subCat ON prodCat.sub_category_id = subCat.cat_id
                LEFT JOIN tbl_categories subSubCat ON prodCat.sub_sub_category_id = subSubCat.cat_id
                WHERE
                prod_is_delete = 0
                AND
                prod_status = 1
                AND
                qty > 0
                AND
                prod.prod_id = ".$prod_id."
                GROUP BY prod.prod_id
                ";
    $result = $CI->db->query($query)->row_array();
    if(!empty($result)){
        $produrl = base_url($result['catURL']);
        if ($result['subCatURL'] != '') {
            $produrl .= '/' . $result['subCatURL'];
        }
        if ($result['subSubCatURL'] != '') {
            $produrl .= '/' . $result['subSubCatURL'];
        }
        return $produrl;
    }else{
        return base_url();
    }
}

function calculateStoreDiscount($cart_id = '') {
    $CI = &get_instance();
    $query = "
            SELECT 
            store.store_id, store.store_discount, store.store_discount_item, SUM(cartD.cart_prod_qnty) as totalQty
            FROM tbl_cart_detail cartD
            INNER JOIN tbl_products prod ON cartD.cart_prod_id = prod.prod_id
            INNER JOIN tbl_stores store ON prod.prod_store_id = store.store_id
            WHERE
            prod.prod_is_delete = 0
            AND
            prod.prod_status = 1
            AND
            store.store_is_hide = 0
            AND
            cartD.cart_id = $cart_id
            GROUP BY store.store_id
            ";
    $result = $CI->db->query($query)->result();

    foreach ($result as $res) {

        $store_sub_total = getSubTotalByStore($res->store_id, $cart_id);

        if ($res->totalQty > $res->store_discount_item && $res->store_discount_item != "") {

            $discount_price = $store_sub_total * ($res->store_discount / 100);
            $store_sub_total = $store_sub_total - $discount_price;
        }

        $new_sub_total += $store_sub_total;
    }

    return number_format($new_sub_total, 2, '.', '');
}

function getSubTotalByStore($store_id, $cartId) {
    $CI = & get_instance();
    //$cartId = 2;//$CI->session->userdata('cartId');
    $query = "
            SELECT
            SUM(
            CASE
             WHEN p.prod_onsale = 1 && p.sale_start_date <= '" . date('Y-m-d') . "' && p.sale_end_date >= '" . date('Y-m-d') . "' THEN p.sale_price * cd.cart_prod_qnty
             WHEN p.prod_onsale = 1 && p.sale_start_date = '' && p.sale_end_date = '' THEN p.sale_price * cd.cart_prod_qnty
             ELSE p.prod_price * cd.cart_prod_qnty
            END
            ) AS cart_sub_total  FROM tbl_cart_detail cd
            JOIN tbl_products p ON cd.cart_prod_id = p.prod_id
            JOIN tbl_cart c ON cd.cart_id = c.cart_id
            WHERE c.cart_id = " . $cartId . " AND p.prod_store_id = " . $store_id . "
            GROUP BY p.`prod_store_id`
            ";
    $result = $CI->db->query($query)->row('cart_sub_total');
    return $result;
}

function getTierNameBySellerId($seller_id) {
    $CI = & get_instance();
    //$cartId = 2;//$CI->session->userdata('cartId');
    $query = "
            SELECT t.`tier_name` as tier_name
            FROM tbl_user u 
            INNER JOIN tbl_stores us ON us.user_id = u.`user_id`
            INNER JOIN tbl_user_store_subscription uss ON uss.`sub_store_id` = us.`store_id`
            INNER JOIN tbl_tier_list t ON t.`tier_id` = uss.`sub_tier_id`
            WHERE u.`user_id` = " . $seller_id . "
            GROUP BY u.`user_id`
            ORDER BY uss.`sub_id` DESC
            ";
    $result = $CI->db->query($query)->row('tier_name');
    return $result;
}

function getTierName($seller_id) {
    $CI = & get_instance();
    //$cartId = 2;//$CI->session->userdata('cartId');
    $query = "
            SELECT t.`tier_name` as tier_name
            FROM tbl_tier_list t
            INNER JOIN tbl_stores us ON us.store_tier_id = t.`tier_id`
            WHERE us.`user_id` = " . $seller_id . "
            ";
    $result = $CI->db->query($query)->row('tier_name');
    return $result;
}

function getTierInfoBySellerId($seller_id) {
    $CI = & get_instance();
    //$cartId = 2;//$CI->session->userdata('cartId');
    $query = "
            SELECT t.`tier_name` as tier_name, uss.`sub_start`, uss.`sub_ends`, us.`store_paypal_id`
            FROM tbl_user u 
            INNER JOIN tbl_stores us ON us.user_id = u.`user_id`
            INNER JOIN tbl_user_store_subscription uss ON uss.`sub_store_id` = us.`store_id`
            INNER JOIN tbl_tier_list t ON t.`tier_id` = uss.`sub_tier_id`
            WHERE u.`user_id` = " . $seller_id . "
            GROUP BY u.`user_id`
            ORDER BY uss.`sub_id` DESC
            ";
    $result = $CI->db->query($query)->row();
    return $result;
}

function dateDifference($date_1, $date_2, $differenceFormat = '%a') {
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);

    $interval = date_diff($datetime1, $datetime2);

    return $interval->format($differenceFormat);
}

function getStoreDiscount($cart_id = '') {
    $CI = & get_instance();
    if ($cart_id == '') {
        $cart_id = @$_COOKIE['educki_cartId'];
    } 
   
    $query = "
            SELECT 
            store.store_id, store.store_discount, store.store_discount_item, SUM(cartD.cart_prod_qnty) as totalQty
            FROM tbl_cart_detail cartD
            INNER JOIN tbl_products prod ON cartD.cart_prod_id = prod.prod_id
            INNER JOIN tbl_stores store ON prod.prod_store_id = store.store_id
            WHERE
            prod.prod_is_delete = 0
            AND
            prod.prod_status = 1
            AND
            store.store_is_hide = 0
            AND
            cartD.cart_id = $cart_id
            AND store_discount_item > 0 AND store_discount > 0
            GROUP BY store.store_id
            ";
    $result = $CI->db->query($query)->result();
    $discount_price = 0;
    foreach ($result as $res) {

        $store_sub_total = getSubTotalByStore($res->store_id, $cart_id);

        if ($res->totalQty >= $res->store_discount_item && $res->store_discount_item != "") {

            $discount_price += $store_sub_total * ($res->store_discount / 100);
//            $store_sub_total = $store_sub_total - $discount_price;
        }

//        $new_sub_total += $store_sub_total;
    }

    //return number_format($discount_price, 2, '.', '');
    //echo '<pre>';
    //print_r($discount_price);
    // exit;
    return $discount_price;
}

function getTotalCommision($cart_id = '') {
    //$cartId = 2;//$CI->session->userdata('cartId');
    $CI = & get_instance();
    $query = "
            SELECT 
            store.store_id, store.store_discount, store.store_discount_item, SUM(cartD.cart_prod_qnty) as totalQty, tier.tier_fee
            FROM tbl_cart_detail cartD
            INNER JOIN tbl_products prod ON cartD.cart_prod_id = prod.prod_id
            INNER JOIN tbl_stores store ON prod.prod_store_id = store.store_id
            INNER JOIN tbl_tier_list tier ON store.store_tier_id = tier.tier_id
            WHERE
            prod.prod_is_delete = 0
            AND
            prod.prod_status = 1
            AND
            store.store_is_hide = 0
            AND
            cartD.cart_id = " . $cart_id . "
            GROUP BY store.store_id
            ";
    $result = $CI->db->query($query)->result();

    foreach ($result as $res) {

        $store_sub_total = getSubTotalByStore($res->store_id, $cart_id);

        //if($res->totalQty > $res->store_discount_item && $res->store_discount_item != ""){

        $discount_price = $store_sub_total * ($res->tier_fee / 100);
        //echo '<pre>';
        //print_r($discount_price);
        //$store_sub_total = $store_sub_total - $discount_price;
        //  echo '<pre>';
        // print_r($store_sub_total);
        //}

        $total_fee += $discount_price;
    }
    //echo '<pre>';
    //        print_r($total_fee);
    return number_format($total_fee, 2, '.', '');
}

function getStoreInfoByUserId($user_id) {
    $CI = & get_instance();
    //$cartId = 2;//$CI->session->userdata('cartId');
    $query = "
            SELECT *
            FROM tbl_stores s 
            WHERE s.`user_id` = " . $user_id . "
            ";
    $result = $CI->db->query($query)->row();
    return $result;
}

function getTotalProductsByUserId($user_id) {
    $CI = & get_instance();
    //$cartId = 2;//$CI->session->userdata('cartId');
    $query = "
            SELECT COUNT(prod.prod_id) AS total_prods
            FROM tbl_products prod
            INNER JOIN tbl_stores s ON prod.prod_store_id = s.store_id
            INNER JOIN tbl_user  u ON s.user_id = u.user_id
            WHERE prod.prod_is_delete = 0 AND u.user_is_delete = 0 AND s.user_id = " . $user_id . "
            GROUP BY prod.prod_store_id
            ";
    $result = $CI->db->query($query)->row('total_prods');

    return $result;
}

function get_country_name($value = '') {
    $CI = & get_instance();
    $query = "
            SELECT name
            FROM tbl_country
            WHERE iso = '" . $value . "'";
    $result = $CI->db->query($query)->row();

    return $result->name;
}

function get_state_name($value = '') {
    $CI = & get_instance();
    $query = "
            SELECT stat_name
            FROM tbl_states
            WHERE stat_id = '" . $value . "'";
    $result = $CI->db->query($query)->row();

    return $result->stat_name;
}

function getSellerTotal($cart_id = '') {
    $CI = & get_instance();
    $query = "
            SELECT 
            store.store_id, store.store_tier_id, store.store_discount, store.store_discount_item, 
            SUM(cartD.cart_prod_qnty) as totalQty, tier.tier_fee, prod.prod_user_id
            FROM tbl_cart_detail cartD
            INNER JOIN tbl_products prod ON cartD.cart_prod_id = prod.prod_id
            INNER JOIN tbl_stores store ON prod.prod_store_id = store.store_id
            INNER JOIN tbl_tier_list tier ON store.store_tier_id = tier.tier_id
            WHERE
            prod.prod_is_delete = 0
            AND
            prod.prod_status = 1
            AND
            store.store_is_hide = 0
            AND
            cartD.cart_id = $cart_id
            GROUP BY store.store_id
            ";
    $result = $CI->db->query($query)->result();

    foreach ($result as $res) {
        // echo '<br>--------------------------------------<br>';
        $storeData[$res->store_id]['store_user_id'] = $res->store_id;
        $storeData[$res->store_id]['store_id'] = $res->store_id;
        $storeData[$res->store_id]['store_tier_id'] = $res->store_tier_id;
        $store_sub_total = getTotalSumByStore($res->store_id, $cart_id)->cart_sub_total;
        // echo '<pre>';
        //print_r("Store Sub Total : ".$store_sub_total);
        $shipping_sub_total = getTotalSumByStore($res->store_id, $cart_id)->shipping_sub_total;
        //echo '<pre>';
        //print_r("Store Shipping Total : ".$shipping_sub_total);
        $storeData[$res->store_id]['store_shipping_total'] = $shipping_sub_total;

        $commission_price = $store_sub_total * ($res->tier_fee / 100);
        //echo '<pre>';
        //print_r("Commission Total : ".$commission_price);
        $storeData[$res->store_id]['store_commission_total'] = $commission_price;

        $store_sub_total1 = $store_sub_total - $commission_price;
        ///echo '<pre>';
        //print_r("Store Sub Total Without Commission : ".$store_sub_total1);

        if ($res->totalQty > $res->store_discount_item && $res->store_discount_item != "" && $res->store_discount_item > 0) {

            $discount_price = $store_sub_total * ($res->store_discount / 100);
            //   echo '<pre>';
            //   print_r("Discount Price : ".$discount_price);
            $storeData[$res->store_id]['store_discount_price'] = $discount_price;
            $store_sub_total = $store_sub_total - $commission_price - $discount_price + $shipping_sub_total;
            //  echo '<pre>';
            // print_r("Store Total : ".$store_sub_total);
            $storeData[$res->store_id]['store_total'] = $store_sub_total;
            $store_name = getStoreNameById($res->store_id)->store_name;
            // echo '<pre>';
            // print_r("Store Name : ".$store_name);
            $storeData[$res->store_id]['store_name'] = $store_name;
            $store_email = getStoreNameById($res->store_id)->store_paypal_id;
            //echo '<pre>';
            //print_r("Store Paypal Id : ".$store_email);
            $storeData[$res->store_id]['store_email'] = $store_email;
        } else {
            $store_sub_total = $store_sub_total - $commission_price + $shipping_sub_total;
            //echo '<pre>';
            //print_r("Store Total : ".$store_sub_total);
            $storeData[$res->store_id]['store_total'] = $store_sub_total;
            $store_name = getStoreNameById($res->store_id)->store_name;
            //echo '<pre>';
            //print_r("Store Name : ".$store_name);
            $storeData[$res->store_id]['store_name'] = $store_name;
            $store_email = getStoreNameById($res->store_id)->store_paypal_id;
            //echo '<pre>';
            //print_r("Store Paypal Id : ".$store_email);
            $storeData[$res->store_id]['store_email'] = $store_email;
        }        
        //echo '<br>--------------------------------------<br>';
        $new_sub_total += $store_sub_total;
    }
    //echo '<pre>';
    //print_r("Final Store Total : ".$new_sub_total);
    //echo '<br>--------------------------------------<br>';
    //return number_format($new_sub_total, 2, '.', '');
    ///return $new_sub_total;
    //echo '<pre>';
    //print_r($storeData);
    return $storeData;
}

function getTotalSumByStore($store_id, $cartId) {
    $CI = & get_instance();
    //$cartId = 2;//$CI->session->userdata('cartId');
    //echo '<pre>';
    //print_r("Store Id : ".$store_id);
    $query = "
            SELECT
            SUM(
            CASE
            WHEN p.prod_status = 1 && p.shipping = 'Charge for Shipping'
            THEN p.ship_price
            ELSE p.ship_price
            END
              ) AS shipping_sub_total,
            SUM(
            CASE
            WHEN p.prod_onsale = 1 && p.sale_start_date <= '" . date('Y-m-d') . "' && p.sale_end_date >= '" . date('Y-m-d') . "' THEN p.sale_price * cd.cart_prod_qnty
            WHEN p.prod_onsale = 1 && p.sale_start_date = '' && p.sale_end_date = '' THEN p.sale_price * cd.cart_prod_qnty
            ELSE p.prod_price * cd.cart_prod_qnty
            END
            ) AS cart_sub_total  FROM tbl_cart_detail cd
            JOIN tbl_products p ON cd.cart_prod_id = p.prod_id
            JOIN tbl_cart c ON cd.cart_id = c.cart_id
            WHERE c.cart_id = " . $cartId . " AND p.prod_store_id = " . $store_id . "
            ";
    $result = $CI->db->query($query)->row();
    return $result;
}

function getStoreNameById($store_id) {
    $CI = & get_instance();
    //$cartId = 2;//$CI->session->userdata('cartId');
    //echo '<pre>';
    //print_r("Store Id : ".$store_id);
    $query = "SELECT * FROM tbl_stores WHERE store_id = " . $store_id . "";

    $result = $CI->db->query($query)->row();
    return $result;
}

function getOrderProductByOrderId($order_id) {
    $CI = & get_instance();
    //$cartId = 2;//$CI->session->userdata('cartId');
    //echo '<pre>';
    //print_r("Store Id : ".$store_id);
    $query = "SELECT * FROM tbl_order_products WHERE order_id = " . $order_id . " ORDER BY order_prod_id DESC";

    $result = $CI->db->query($query)->row();
//    echo $CI->db->last_query();
    return $result;
}

function getDealOfDay() {
    $CI = & get_instance();

    $query = "SELECT pv.product_id, COUNT(pv.view_id) AS total_views, p.*
                FROM tbl_product_views pv
                INNER JOIN tbl_products p ON p.prod_id = pv.product_id
                INNER JOIN tbl_user u ON p.prod_user_id = u.user_id 
                INNER JOIN tbl_stores s ON s.user_id = u.user_id 
                WHERE pv.view_date  >=  '" . date('Y-m-d 00:00:00') . "' AND pv.view_date <= '" . date('Y-m-d 23:59:59') . "' AND p.`prod_onsale` = 1 
                AND u.user_is_delete = 0 AND prod.prod_status = '1'
                AND prod.prod_is_delete = '0' AND u.user_status = 1 AND store_is_hide = 0 
                GROUP BY pv.product_id
                ORDER BY total_views DESC
                LIMIT 0,1";

    $result = $CI->db->query($query)->row();
    return $result;
}

function getMostViewedDeals() {
    $CI = & get_instance();

    $query = "SELECT images.img_name,
                COUNT(views.product_id) AS prod_occurrence,
                prod.prod_title,prod.prod_onsale,
                prod.sale_price,prod.prod_id,
                prod.prod_url,
                prod.prod_price 
              FROM
                tbl_product_views views 
                JOIN tbl_products prod 
                  ON prod.prod_id = views.product_id 
                JOIN tbl_product_images images
                  ON images.img_prod_id = prod.prod_id 
              INNER JOIN tbl_user u ON prod.prod_user_id = u.user_id 
              INNER JOIN tbl_stores s ON s.user_id = prod.prod_user_id 
              WHERE views.view_date  >=  '" . date('Y-m-d 00:00:00') . "' AND views.view_date <= '" . date('Y-m-d 23:59:59') . "' 
              AND prod.prod_status = '1' 
                AND prod.prod_is_delete = '0' 
                AND prod.prod_status = '1' 
                AND u.user_is_delete = 0 AND u.user_status = 1 AND store_is_hide = 0 AND prod.prod_onsale = 1 
              GROUP BY views.product_id 
              ORDER BY prod_occurrence DESC 
              LIMIT 3 ";
    $result = $CI->db->query($query)->result_array();
    return $result;
//     
}

function getOrderCountByUserId($buyer_id) {
    $CI = & get_instance();

    $query = "SELECT COUNT(order_id) AS total_orders
                FROM tbl_orders
                WHERE buyer_id = " . $buyer_id . "
                GROUP BY buyer_id";

    $result = $CI->db->query($query)->row('total_orders');
    return $result;
}

function store_amount_received($store_id) {
    $CI = & get_instance();
    // $query = "SELECT 
    //         SUM(tbl_order_seller_info.store_total - tbl_order_seller_info.store_commission_total) AS amount_received
    //         FROM
    //           tbl_order_products
    //           JOIN tbl_orders
    //             ON tbl_order_products.order_id = tbl_orders.order_id 
    //             INNER JOIN tbl_order_seller_info
    //             ON tbl_order_seller_info.order_id = tbl_orders.order_id 
    //         WHERE tbl_order_products.order_prod_store_id =" . $store_id . "
    //           AND tbl_order_products.order_prod_status = 'completed' 
    //         GROUP BY tbl_order_products.order_id";

    //- QUERY BY HASEEB
    /*$query = "
            SELECT (SUM(op.order_prod_total_price)+(SELECT 
                SUM(store_shipping_total) 
                FROM `tbl_order_seller_info`
                WHERE store_id = $store_id
                )-(SELECT 
                SUM(store_commission_total) 
                FROM `tbl_order_seller_info`
                WHERE store_id = $store_id
                ) ) AS amount_received
FROM tbl_order_products op 
WHERE op.order_prod_store_id = $store_id AND

            op.order_prod_status NOT IN (
                'In Process',
                'Awaiting Pickup',
                'Awaiting Tracking'
              )  
            ";

    $result = $CI->db->query($query)->row('amount_received');
    return $result;
*/
    $query = "
            SELECT 
            (os.store_total) AS amount_received, 
            op.order_id, os.info_id,os.store_total FROM tbl_order_seller_info os 
            INNER JOIN tbl_order_products op ON op.order_seller_info_id = os.info_id
            INNER JOIN tbl_orders o ON o.order_id = os.order_id
            WHERE os.store_id = $store_id AND
            op.order_prod_status NOT IN ('In Process' , 'Awaiting Pickup' , 'Awaiting Tracking') 
            GROUP BY os.store_id, os.info_id
            ";
    $result = $CI->db->query($query)->result();
    $total = 0;
    if(!empty($result)){
        foreach($result as $res){
            $total +=$res->amount_received;
        }
    }
    return $total;

}

function store_amount_pending($store_id) {
    $CI = & get_instance();
    // $query = "SELECT 
    //             SUM(tbl_order_seller_info.store_total - tbl_order_seller_info.store_commission_total) AS amount_pending 
    //             FROM
    //               tbl_order_products 
    //               JOIN tbl_orders 
    //                 ON tbl_order_products.order_id = tbl_orders.order_id
    //               INNER JOIN tbl_order_seller_info
    //                 ON tbl_order_seller_info.order_id = tbl_orders.order_id 
    //             WHERE tbl_order_products.order_prod_store_id = " . $store_id . " 
    //               AND tbl_orders.traking_number IS NULL 
    //               AND tbl_order_products.order_prod_status IN ('In Process', 'Awaiting Pickup','Awaiting Tracking') 
    //             GROUP BY tbl_order_products.order_id";

/*    $query = '';
    $count = $CI->db->query("SELECT count('info_id') as infoCount FROM tbl_order_seller_info WHERE store_id = $store_id" )->row();
    $count = $count->infoCount;
    //- Query By Haseeb
    if($count <=1 ){
    $query = "
            SELECT (SUM(op.order_prod_total_price)+(SELECT 
                SUM(store_shipping_total) 
                FROM `tbl_order_seller_info`
                WHERE store_id = $store_id
                )-(SELECT 
                SUM(store_commission_total) 
                FROM `tbl_order_seller_info`
                WHERE store_id = $store_id
                ) ) AS amount_pending
FROM tbl_order_products op 
WHERE op.order_prod_store_id = $store_id AND

            op.order_prod_status IN (
                'In Process',
                'Awaiting Pickup',
                'Awaiting Tracking'
                
              )  
            ";
            //op.order_prod_tracking_number IS NULL AND
}else{
    //-- op.order_prod_tracking_number IS NULL AND
    $query = "SELECT SUM(os.store_total) AS amount_pending
            FROM  tbl_order_products op
            INNER JOIN tbl_orders o ON op.order_id = o.order_id 
            INNER JOIN tbl_order_seller_info os ON os.info_id = op.order_seller_info_id
            WHERE 
            op.order_prod_store_id  = $store_id AND 
            
            op.order_prod_status IN (
                'In Process',
                'Awaiting Pickup',
                'Awaiting Tracking'
                
              ) 
            GROUP BY op.order_id";
        }
*/
    $query = "
            SELECT 
            (os.store_total) AS amount_pending, 
            op.order_id, os.info_id,os.store_total FROM tbl_order_seller_info os 
            INNER JOIN tbl_order_products op ON op.order_seller_info_id = os.info_id
            INNER JOIN tbl_orders o ON o.order_id = os.order_id
            WHERE os.store_id = $store_id AND
            op.order_prod_status IN ('In Process' , 'Awaiting Pickup' , 'Awaiting Tracking') 
            GROUP BY os.store_id, os.info_id
            ";
    $result = $CI->db->query($query)->result();
    $total = 0;
    if(!empty($result)){
        foreach($result as $res){
            $total +=$res->amount_pending;
        }
    }
    return $total;
}

function getTotalSalesByStore($store_id){
    $CI = & get_instance();
    $query = "
            SELECT 
            (os.store_total-os.store_shipping_total) AS amount_pending, 
            op.order_id, os.info_id,os.store_total FROM tbl_order_seller_info os 
            INNER JOIN tbl_order_products op ON op.order_seller_info_id = os.info_id
            INNER JOIN tbl_orders o ON o.order_id = os.order_id
            WHERE os.store_id = $store_id AND
            op.order_prod_status NOT IN ('In Process' , 'Awaiting Pickup' , 'Awaiting Tracking' , 'cancel') 
            AND DATE(o.order_date) = DATE('".date('Y-m-d')."')
            GROUP BY os.store_id, os.info_id
            ";
    $result = $CI->db->query($query)->result();
    $total = 0;
    if(!empty($result)){
        foreach($result as $res){
            $total +=$res->amount_pending;
        }
    }
    return $total;
}

function calculateSellerProfit($store_id, $order_id){
    $CI = & get_instance();
    //- QUERY BY HASEEB
    $query = "
            SELECT 
            SUM(store_total) as totalProfit, SUM(store_shipping_total) as shipping_total, SUM(store_discount_price) as store_discount_price  
            FROM
            tbl_order_seller_info os 
            WHERE 
            os.store_id = $store_id AND 
            os.order_id = $order_id
            GROUP BY os.order_id 
            ";
    $result = $CI->db->query($query)->row();
    
    return $result;
}

function getCompareList(){
    $CI =& get_instance();

    $prods = explode(',',$_SESSION['compare_prods']);
    //$prods = $_SESSION['compare_prods'];
        $prods = str_replace("'",'',$prods);
        //echo '<pre>';
        //print_r($prods);
        //exit;
        foreach ($prods as $key => $prod_id) {
            $prod_details = getproduct_details($prod_id);
            $return['html'] .= '<div class="compre-pop-bx '.($key == 2 ? 'mrg-rgt-0' : '').' prod-td-'.trim($prod_id).'"><div class="compre-pop-bx-img"><img src="'.base_url().'resources/prod_images/'.$prod_details->img_name.'" alt="" title="" class="img-responsive"/></div><div class="compre-pop-bx-right"><a href="'.productDetailUrl($prod_id).'/'.$prod_details->prod_url.'">'.$prod_details->prod_title.'</a></div><button class="removeslctitem" onclick="removeslctitem('.$prod_id.')"></button></div>';
        }

    return $return['html'];
}


function educki_kmli(){

    
    $CI =& get_instance();
    //$CI->load->helper('cookie');
    $check_cookie = @$_COOKIE['educki_kmli'];
//    echo '<pre>';
//    print_r(@$_COOKIE['educki_kmli']);
//    exit;
    if($check_cookie){
        $email    = @$_COOKIE['educki_kmli'];
        
        $query = "SELECT * FROM tbl_user WHERE user_email = '" . $email . "' AND user_status = 1";

        $regData  = $CI->db->query($query)->row();

        $creds                  = array();
        $creds['user_login']    = $regData->user_email;
        $creds['user_password'] = $regData->user_password;
        $creds['remember']      = true;
        $user                   = wp_signon($creds, false);
        if (!empty($regData) ) {
            $sessiondata = array(
                'userid'    => $regData->user_id,
                'useremail' => $regData->user_email,
                'userfname' => $regData->user_fname,
                'userlname' => $regData->user_lname,
                'usertype' => $regData->user_type,
                'logged_in' => true,
            );

            $CI->session->set_userdata($sessiondata);
            $loginData['login_user_id']    = $regData->user_id;
            $loginData['login_date']       = date('Y-m-d');
            $lastlogin['user_last_active'] = date('Y-m-d');
            $CI->common_model->commonSave('tbl_user_login', $loginData);
            $CI->common_model->commonUpdate('tbl_user', $lastlogin, 'user_id', $regData->user_id);
        }
    }else{
        // $CI->facebook->destroy_session();
        // $CI->session->unset_userdata('userid');
        // $CI->session->unset_userdata('useremail');
        // $CI->session->unset_userdata('userfname');
        // $CI->session->unset_userdata('userlname');
        // $CI->session->unset_userdata('logged_in');
        // //$CI->session->unset_userdata('cartId');
        // $CI->session->unset_userdata('isCart');
        if(isset($_COOKIE['educki_kmli'])){
            delete_cookie('educki_kmli');
        }
        //delete_cookie('educki_isCart', "", time() - 3600);
        //delete_cookie('educki_cartId', "", time() - 3600);
        //delete_cookie('educki_userId', "", time() - 3600);
        
        // $expires = time() - 50;
        // setcookie('isCart',"FALSE",$expires,$this->data['url'],'/','educki_');
        // setcookie('cartId',0,$expires,$this->data['url'],'/','educki_');
        // setcookie('userId',0,$expires,$this->data['url'],'/','educki_');
        // setcookie('educki_isCart', "FALSE", $expires, '/');
        // setcookie('educki_cartId', 0, $expires, '/');
        // setcookie('educki_userId', 0, $expires, '/');
        
        //wp_logout();
    } 
}


function getProductPriceById($prod_id){
    $CI =& get_instance();
    return $CI->db->query("SELECT p.`prod_id`, 
        (
        CASE
        WHEN p.prod_onsale = 1 && p.sale_start_date <= '" . date('Y-m-d') . "' && p.sale_end_date >= '" . date('Y-m-d') . "' THEN p.sale_price
        WHEN p.prod_onsale = 1 && p.sale_start_date = '' && p.sale_end_date = '' THEN p.sale_price
        ELSE p.prod_price
        END
        ) AS unit_prod_price,
        (
        CASE
        WHEN p.prod_onsale = 1 && p.sale_start_date <= '" . date('Y-m-d') . "' && p.sale_end_date >= '" . date('Y-m-d') . "' THEN 'On Sale:'
        WHEN p.prod_onsale = 1 && p.sale_start_date = '' && p.sale_end_date = '' THEN 'On Sale:'
        ELSE 'Price:'
        END
        ) AS unit_prod_sale
        FROM tbl_products p 
        WHERE p.prod_id = '" . $prod_id . "' ")->row();
}



function getProduct1lvlFilter($prod_id, $filter_title){
    $CI =& get_instance();
//    $query="SELECT pf.*,cft.`filter_title` FROM tbl_product_filters pf      
//        INNER JOIN tbl_cat_filter_title cft ON cft.`filter_title_id` = pf.`filter_id`
//        WHERE pf.`prod_id` = ".$prod_id." AND cft.filter_title LIKE '%".$filter_title."%'";
//    echo $query;
    return $CI->db->query("SELECT pf.*,cft.`filter_title` FROM tbl_product_filters pf      
        INNER JOIN tbl_cat_filter_title cft ON cft.`filter_title_id` = pf.`filter_id`
        WHERE pf.`prod_id` = ".$prod_id." AND cft.filter_title LIKE '%".$filter_title."%'")->result_array();
}

function my_remove_array_item( $array, $item ) {
    $index = array_search($item, $array);
    if ( $index !== false ) {
        unset( $array[$index] );
    }

    return $array;
}

function getCompareList2(){

    if(!empty($_SESSION['compare_prods'])){
        $prods = explode(',',$_SESSION['compare_prods']);
        //$prods = $_SESSION['compare_prods'];
            $prods = str_replace("'",'',$prods);
            //echo '<pre>';
            //print_r($prods);
            //exit;
            foreach ($prods as $key => $prod_id) {
                $prod_details = getproduct_details($prod_id);
                $return['html'] .= '<div class="col-md-3 prod-td-'.trim($prod_id).'"><div class="compare-main-bx"><div class="compare-main-bx-lft"><img src="'.base_url().'resources/prod_images/'.$prod_details->img_name.'" alt="" title="" class="img-responsive"/></div><div class="compare-main-bx-rgt"><a href="'.productDetailUrl($prod_id).'/'.$prod_details->prod_url.'">'.$prod_details->prod_title.'</a></div><button class="removeslctitem" onclick="removeslctitem('.$prod_id.')"></button></div></div>';
            }

        return $return['html'];
    }else{
        return 0;
    }
}

function getCartTotalQty($prodId, $cart_id){
    $CI =& get_instance();
    $total = $CI->db->query("SELECT SUM(cart_prod_qnty) AS totalCartQty FROM tbl_cart_detail WHERE cart_prod_id = ".$prodId." AND cart_id = ".$cart_id."")->row();
    return (int)$total->totalCartQty;
}

/// Distance Calculator
/// echo distance(32.9697, -96.80322, 29.46786, -98.53506, "M") . " Miles<br>";
/// echo distance(32.9697, -96.80322, 29.46786, -98.53506, "K") . " Kilometers<br>";
/// echo distance(32.9697, -96.80322, 29.46786, -98.53506, "N") . " Nautical Miles<br>";
function distance($lat1, $lon1, $lat2, $lon2, $unit) {

    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
        return ($miles * 1.609344);
    } else if ($unit == "N") {
        return ($miles * 0.8684);
    } else {
        return $miles;
    }
}


function getTopSellingsDeals() {
    $CI = & get_instance();

    $query = "SELECT images.img_name,
                COUNT(prod.prod_id) AS prod_occurrence,
                prod.prod_title,prod.prod_onsale,
                prod.sale_price,prod.prod_id,
                prod.prod_url,
                prod.prod_price 
                FROM
                tbl_products prod 
                JOIN tbl_product_images images
                ON images.img_prod_id = prod.prod_id 
                INNER JOIN tbl_user u ON prod.prod_user_id = u.user_id 
                INNER JOIN tbl_stores s ON prod.prod_user_id = s.user_id 
                WHERE prod.`sale_start_date` <= CURDATE() AND prod.`sale_end_date` >= CURDATE()
                AND prod.`prod_onsale` = 1 AND prod.`sale_price` != ''
                AND prod.prod_status = '1'
                AND prod.prod_is_delete = '0' AND u.user_is_delete = 0 AND u.user_status = 1
                GROUP BY prod.prod_id 
                ORDER BY prod.`sale_price` DESC 
                LIMIT 3";
    $result = $CI->db->query($query)->result_array();
    return $result;
//     
}


function getDealOfTheDay() {
    $CI = & get_instance();

    $query = "SELECT images.img_name,
                COUNT(prod.prod_id) AS prod_occurrence,
                prod.prod_title,prod.prod_onsale,
                prod.sale_price,prod.prod_id,
                prod.prod_url,
                prod.sale_start_date,
                prod.sale_end_date,
                prod.prod_price 
                FROM
                tbl_products prod 
                JOIN tbl_product_images images
                ON images.img_prod_id = prod.prod_id
                INNER JOIN tbl_user u ON prod.prod_user_id = u.user_id 
                INNER JOIN tbl_stores s ON s.user_id = u.user_id 
                WHERE
                CASE WHEN prod.`sale_start_date` != '' THEN prod.`sale_start_date` <= CURDATE() ELSE prod_onsale = 1 END
                AND 
                CASE WHEN prod.`sale_end_date` != '' THEN prod.`sale_end_date` >= CURDATE() ELSE prod_onsale = 1 END
                AND prod.`prod_onsale` = 1
                AND prod.`sale_price` != ''
                AND prod.prod_status = '1'
                AND prod.prod_is_delete = '0' 
                AND u.user_is_delete = 0 AND u.user_status = 1 AND s.store_is_hide = 0 
                GROUP BY prod.prod_id 
                ORDER BY  CASE WHEN prod_onsale THEN prod_price-sale_price END DESC
                LIMIT 1";
    $result = $CI->db->query($query)->result_array();
    //echo $CI->db->last_query();
    return $result;
//     
}

function get_latest_message($con_id){
    $CI =& get_instance();
    $query = $CI->db->query("SELECT message FROM tbl_messages WHERE conversation_id ='".$con_id."' order by id DESC LIMIT 1")->row();
    return $query->message;
}

function get_email($user_id){
    $CI =& get_instance();
    $query = $CI->db->query("SELECT user_email FROM tbl_user WHERE user_id ='".$user_id."'")->row();
    return $query->user_email;
}

function calculateGMV($value='')
{
    $CI =& get_instance();
    $start = date('Y-m-d', strtotime('first day of january this year'));
    $end = date('Y-m-d');
    $GMV = $CI->db->query("SELECT SUM(store_total) as sum 
                                    FROM tbl_order_seller_info s 
                                    INNER JOIN tbl_stores i ON i.store_id = s.store_id 
                                    INNER JOIN tbl_orders o ON o.order_id = s.order_id
                                    WHERE i.user_id = ".$value." AND DATE(o.order_completion_date) >= DATE('" . $start . "') AND DATE('" . $end . "') >= DATE(o.order_completion_date) AND o.order_status = 'completed';")->row()->sum;
    //echo $CI->db->last_query();
    return $GMV;
}

function calculateYTD($value='')
{
    $CI =& get_instance();
    $start = date('Y-m-d', strtotime('first day of january this year'));
    $end = date('Y-m-d');
    $GMV = $CI->db->query("SELECT SUM(s.store_commission_total) as sum 
                                    FROM tbl_order_seller_info s 
                                    INNER JOIN tbl_stores i ON i.store_id = s.store_id 
                                    INNER JOIN tbl_orders o ON o.order_id = s.order_id
                                    WHERE i.user_id = ".$value." AND DATE(o.order_completion_date) >= DATE('" . $start . "') AND DATE('" . $end . "') >= DATE(o.order_completion_date) AND o.order_status = 'completed';")->row()->sum;
    //echo $CI->db->last_query();
    return $GMV;
}


function prod_reported($id='')
{
    $CI =& get_instance();
    $item_reported = $CI->db->query("SELECT COUNT(*) AS COUNT
                                            FROM `tbl_item_flag_history` flag
                                            INNER JOIN tbl_products prod ON prod.prod_id = flag.flag_prod_id
                                            INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
                                            INNER JOIN tbl_user users ON users.user_id = store.user_id
                                            WHERE users.user_is_delete = 0 AND store.user_id = ".$id."
                                            ")->row()->COUNT;
    return $item_reported;
}

function getTierMonths($tier_id) {
    $CI = & get_instance();
    $query = "
            SELECT *
            FROM tbl_tier_list t
            WHERE tier_id = " . $tier_id . "
            ";
    $result = $CI->db->query($query)->row('tier_months');
    return $result;
}

function getTierAmount($tier_id) {
    $CI = & get_instance();
    $query = "
            SELECT *
            FROM tbl_tier_list t
            WHERE tier_id = " . $tier_id . "
            ";
    $result = $CI->db->query($query)->row('tier_amount');
    return $result;
}

function getBuyerInfoById($user_id) {
    $CI = & get_instance();
    $query = "
            SELECT *
            FROM tbl_user u 
            WHERE user_id = " . $user_id . "
            ";
    $result = $CI->db->query($query)->row();
    return $result;
}

function getrodImage($prod_id) {
    $CI = & get_instance();
    $query = "
            SELECT img_name 
            FROM tbl_product_images 
            WHERE img_prod_id = " . $prod_id . "
            ";
    $result = $CI->db->query($query)->row('img_name');
    return $result;
}

function get_user_email($user_id) {
    $CI = & get_instance();
    $query = "
            SELECT user_email
            FROM tbl_user
            WHERE user_id = ".$user_id." AND user_is_delete = 0
            ";
    $result = $CI->db->query($query)->row('user_email');
    return $result;
}

function get_total_msg()
{
    $CI = & get_instance();
    $CI->load->model('Message_model');
    $Allconversation = $CI->Message_model->fetchAllConversations($CI->session->userdata('userid'));
    $sum = 0;
    foreach ($Allconversation as $i => $con) {
        if ($con->buyer != $CI->session->userdata('userid')){
            if($con->seller_unread>0){
                $sum += $con->seller_unread;
            }
        }else{
            if($con->buyer_unread>0){
                $sum += $con->buyer_unread;
            }
        }
    }

    return $sum;
}

function get_filter_product_count($cat_url = '',$filter_name='',$filter_slug='')
{
    $CI = & get_instance();
    $result = 0;
    if($cat_url !='' && $filter_name !=''){
        $query = '  SELECT *

                    FROM tbl_products p
                    INNER JOIN tbl_user u ON p.prod_user_id = u.user_id
                    INNER JOIN tbl_stores s ON s.user_id = u.user_id
                    INNER JOIN tbl_product_images i ON i.img_prod_id = p.prod_id 
                    INNER JOIN tbl_categories cat ON cat.cat_id = p.prod_cat_id
                    INNER JOIN tbl_product_filters f ON p.prod_id = f.prod_id 
                    WHERE 
                    p.prod_status = 1 AND
                    p.prod_is_delete = 0 AND
                    cat.cat_url = "'.$cat_url.'" AND
                    s.store_is_hide = 0 AND
                    u.user_is_delete = 0 AND
                    u.user_status = 1 AND 
                    f.filter_value = "'.$filter_name.'" AND f.filter_slug = "'.$filter_slug.'" 
                    GROUP BY p.prod_id
                    ';

        $result = count($CI->db->query($query)->result_array());
    }
    return $result;
}

function get_con_main_filter_product_count($cat_url = '',$filter_name='',$filter_slug='')
{
    $CI = & get_instance();
    $result = 0;
    if($cat_url !='' && $filter_name !=''){
        $query = '  SELECT *

                    FROM tbl_products p
                    INNER JOIN tbl_user u ON p.prod_user_id = u.user_id
                    INNER JOIN tbl_stores s ON s.user_id = u.user_id
                    INNER JOIN tbl_product_images i ON i.img_prod_id = p.prod_id 
                    INNER JOIN tbl_categories cat ON cat.cat_id = p.prod_cat_id
                    LEFT JOIN tbl_product_filters f ON p.prod_id = f.prod_id 
                    LEFT JOIN tbl_product_filters_detail fd ON f.id = fd.prod_filter_id 
                    WHERE 
                    p.prod_status = 1 AND
                    p.prod_is_delete = 0 AND
                    cat.cat_url = "'.$cat_url.'" AND
                    s.store_is_hide = 0 AND
                    u.user_is_delete = 0 AND
                    u.user_status = 1 AND 
                    f.filter_slug = "'.$filter_name.'" AND fd.filter_slug = "'.$filter_slug.'" 
                    GROUP BY p.prod_id
                    ';

        $result = count($CI->db->query($query)->result_array());
    }
    return $result;
}

function get_con_second_filter_product_count($cat_url = '',$filter_name='',$filter_slug='')
{
    $CI = & get_instance();
    $result = 0;
    if($cat_url !='' && $filter_name !=''){
        $query = '  SELECT *

                    FROM tbl_products p
                    INNER JOIN tbl_user u ON p.prod_user_id = u.user_id
                    INNER JOIN tbl_stores s ON s.user_id = u.user_id
                    INNER JOIN tbl_product_images i ON i.img_prod_id = p.prod_id 
                    INNER JOIN tbl_categories cat ON cat.cat_id = p.prod_cat_id
                    LEFT JOIN tbl_product_filters f ON p.prod_id = f.prod_id 
                    LEFT JOIN tbl_product_filters_detail fd ON f.id = fd.prod_filter_id 
                    WHERE 
                    p.prod_status = 1 AND
                    p.prod_is_delete = 0 AND
                    cat.cat_url = "'.$cat_url.'" AND
                    s.store_is_hide = 0 AND
                    u.user_is_delete = 0 AND
                    u.user_status = 1 AND 
                    fd.filter_slug = "'.$filter_name.'" AND fd.filter_value = "'.$filter_slug.'" 
                    GROUP BY p.prod_id
                    ';

        $result = count($CI->db->query($query)->result_array());
    }
    return $result;
}

function clean_string($string='')
{   
    $string = str_replace("../","",$string);
    $string = str_replace('img src="','img src="'.base_url(),$string);
    return $string;
}

function removeOnSale(){
    $CI = & get_instance();
    $prod = $CI->db->select('*')->from('tbl_products')->where('prod_is_delete', 0)->where('prod_onsale', 1)->get();
    $prod = $prod->result();
    foreach($prod as $p){
        $sDate = date('Y-m-d', strtotime($p->sale_start_date));
        $eDate = date('Y-m-d', strtotime($p->sale_end_date));
        $tDate = date('Y-m-d');
        if ($p->sale_end_date != '') {
            if( $tDate > $eDate){
                $udata['prod_onsale'] = 0;
                $udata['sale_price'] = NULL;
                $CI->common_model->commonUpdate('tbl_products', $udata, 'prod_id', $p->prod_id );
            }
        }
    }
}

function countWishListItem($userId){
    $CI = & get_instance();
    $query = "
            SELECT 
            prod.*, wish.id, store.store_name, store.store_url, images.img_name
            FROM tbl_wish_list wish
            INNER JOIN tbl_products prod ON prod.prod_id = wish.product_id
            INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
            INNER JOIN tbl_product_images images ON prod.prod_id = images.img_prod_id
            INNER JOIN tbl_product_category prodCat ON prod.prod_id = prodCat.prod_id
            INNER JOIN tbl_categories category ON prodCat.category_id = category.cat_id
            LEFT JOIN tbl_categories subCat ON prodCat.sub_category_id = subCat.cat_id
            LEFT JOIN tbl_categories subSubCat ON prodCat.sub_sub_category_id = subSubCat.cat_id
            WHERE
            prod_is_delete = 0
            AND
            prod_status = 1
            AND
            wish.user_id = ".$userId."
            AND prod.prod_is_delete = 0
            AND prod.qty > 0
            GROUP BY prod.prod_id
            ";
    $result = $CI->db->query($query)->result_array();
    // echo '<pre>';
    // print_r($result);
    // echo '</pre>';
    return count(@$result);
}

function user_reported($id='')
{
    $CI =& get_instance();
    $user_reported = $CI->db->query("SELECT *
                                            FROM `tbl_user` user
                                            INNER JOIN tbl_stores store ON store.user_id = user.user_id
                                            WHERE user.user_is_delete = 0 AND user.user_id = ".$id."
                                            ")->row()->store_reported;
    return $user_reported;
}

function get_user_email_by_store_id($store_id) {
    $CI = & get_instance();
    $query = "
            SELECT *
            FROM tbl_stores 
            INNER JOIN tbl_user ON tbl_user.user_id = tbl_stores.user_id 
            WHERE tbl_stores.store_id = ".$store_id." AND user_is_delete = 0
            ";
    $result = $CI->db->query($query)->row();
    if($result){
        return $result;
    }else{
        $query = "
            SELECT *
            FROM tbl_user 
            WHERE user_id = ".$store_id." AND user_is_delete = 0
            ";
        $result = $CI->db->query($query)->row();
        return $result;
    }
}

function isStorePer($user_id, $is_pickupORis_fb_store){
    $CI = & get_instance();
    $tier_id = $CI->db->query("
                    SELECT store_tier_id
                    FROM tbl_stores
                    WHERE user_id = $user_id
                   ")->row();
    $store_tier_id = $tier_id->store_tier_id;
    /*echo $store_tier_id;
    exit();*/
    $tier_info = $CI->db->query("
                    SELECT $is_pickupORis_fb_store
                    FROM tbl_tier_list
                    WHERE tier_id = $store_tier_id
                   ")->row();
    return $tier_info->$is_pickupORis_fb_store;
}