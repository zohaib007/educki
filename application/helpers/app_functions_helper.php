<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function getProductPricesApp($cartId = 0) {
    $CI = & get_instance();
      
    return $CI->db->query("SELECT cd.cart_prod_id,cd.cart_detail_id,cd.cart_details,cd.cart_id, cd.cart_prod_qnty, 
                (
                CASE
                 WHEN p.prod_onsale = 1 && p.sale_start_date <= '" . date('Y-m-d') . "' && p.sale_end_date >= '" . date('Y-m-d') . "' THEN p.sale_price
                 WHEN p.prod_onsale = 1 && p.sale_start_date = '' && p.sale_end_date = '' THEN p.sale_price
                 ELSE p.prod_price
                END
                ) AS unit_prod_price,
                (
                CASE
                 WHEN p.prod_onsale = 1 && p.sale_start_date <= '" . date('Y-m-d') . "' && p.sale_end_date >= '" . date('Y-m-d') . "' THEN p.sale_price * cd.cart_prod_qnty
                 WHEN p.prod_onsale = 1 && p.sale_start_date = '' && p.sale_end_date = '' THEN p.sale_price * cd.cart_prod_qnty
                 ELSE p.prod_price * cd.cart_prod_qnty
                END
                ) AS total_prod_price  FROM tbl_cart_detail cd
                JOIN tbl_products p ON cd.cart_prod_id = p.prod_id
                JOIN tbl_cart c ON cd.cart_id = c.cart_id
                WHERE c.cart_id = '" . $cartId . "' ")->result();
}

function getShipmentSubTotalApp($cartId = 0) {

    $CI = & get_instance();
    return $CI->db->query("SELECT  SUM(
            CASE
            WHEN p.prod_status = 1 && p.shipping = 'Charge for Shipping'
            THEN p.ship_price
            ELSE 0
            END
                  ) AS shipping_sub_total
          FROM
            tbl_cart_detail cd 
            JOIN tbl_products p 
              ON cd.cart_prod_id = p.prod_id
            JOIN tbl_cart c 
              ON cd.cart_id = c.cart_id 
          WHERE c.cart_id =  '" . $cartId . "' ")->row('shipping_sub_total');
}

function getCartSubTotalApp($cartId = 0) {
    $CI = & get_instance();
    
    $query = $CI->db->query("SELECT 
                SUM(
                CASE
                 WHEN p.prod_onsale = 1 && p.sale_start_date <= '" . date('Y-m-d') . "' && p.sale_end_date >= '" . date('Y-m-d') . "' THEN p.sale_price * cd.cart_prod_qnty
                 WHEN p.prod_onsale = 1 && p.sale_start_date = '' && p.sale_end_date = '' THEN p.sale_price * cd.cart_prod_qnty
                 ELSE p.prod_price * cd.cart_prod_qnty
                END
                ) AS cart_sub_total  FROM tbl_cart_detail cd
                JOIN tbl_products p ON cd.cart_prod_id = p.prod_id
                JOIN tbl_cart c ON cd.cart_id = c.cart_id
                WHERE c.cart_id = '" . $cartId . "' ")->row();
    return number_format($query->cart_sub_total,2,'.','');
}


function totalCartItemsApp($cartId = 0) {
    $CI = & get_instance();
    $CI->load->model('front/cart/cart_model');
    
    $data['cart_product_details'] = getProductPricesApp($cartId);
    
    foreach ($data['cart_product_details'] as $product) {
        $product_details = getproduct_details_2($product->cart_prod_id);
        
        if ($product_details == '') {
            $CI->cart_model->removeCartItem($cartId, $product->cart_prod_id, $product->cart_detail_id);
        }
    }

    return $CI->db->query("SELECT SUM(cart_prod_qnty) AS total_cart_items FROM tbl_cart_detail cd
            JOIN tbl_products p ON cd.cart_prod_id = p.prod_id
            JOIN tbl_cart c ON cd.cart_id = c.cart_id
            WHERE c.cart_id = '" . $cartId . "' ")->row('total_cart_items');
}

function productDetailUrlApp($prod_id = '') {
    $CI = & get_instance();
    $query = "
                SELECT 
                prod.prod_url, category.cat_url as catURL, subCat.cat_url as subCatURL, subSubCat.cat_url as subSubCatURL
                FROM tbl_products prod
                INNER JOIN tbl_product_category prodCat ON prod.prod_id = prodCat.prod_id
                INNER JOIN tbl_categories category ON prodCat.category_id = category.cat_id
                LEFT JOIN tbl_categories subCat ON prodCat.sub_category_id = subCat.cat_id
                LEFT JOIN tbl_categories subSubCat ON prodCat.sub_sub_category_id = subSubCat.cat_id
                WHERE
                prod_is_delete = 0
                AND
                prod_status = 1
                AND
                prod.prod_id = ".$prod_id."
                GROUP BY prod.prod_id
                ";
    $result = $CI->db->query($query)->row_array();
    if(!empty($result)){
        return 1;
    }else{
        return 0;
    }
}

function get_total_msg_app($user_id)
{
    $CI = & get_instance();
    $CI->load->model('Message_model');
    $Allconversation = $CI->Message_model->fetchAllConversations($user_id);
    $sum = 0;
    foreach ($Allconversation as $i => $con) {
        if ($con->buyer != $user_id){
            if($con->seller_unread>0){
                $sum += $con->seller_unread;
            }
        }else{
            if($con->buyer_unread>0){
                $sum += $con->buyer_unread;
            }
        }
    }

    return $sum;
}