<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function footer_about_menu() {

    $CIH = &get_instance();
    $query = "SELECT
            subpages.sub_pg_name,subpages.sub_pg_url
            FROM
            tbl_pages_sub subpages
            WHERE subpages.sub_pg_status = 1 
            AND subpages.sub_is_delete = 0
            AND subpages.sub_pg_id = 4
            AND subpages.pg_footer = 'Y'
            ORDER BY sub_pg_order ASC";
    $results = $CIH->db->query($query);
    return $results->result_array();
}

function footer_parent_menu(){
    $CIH = &get_instance();
    $query = "SELECT
                pg_title,pg_url
                FROM
                tbl_pages pages
                WHERE pages.pg_status = 1 
                AND pages.is_delete = 0
                AND pages.pg_footer = 'Y'
                AND pages.pg_header != 'Y' 
                ORDER BY pg_order ASC";
    $results = $CIH->db->query($query);
    return $results->result_array();
}


function header_menu() {
    $CIH = &get_instance();
    $query = "SELECT
                pg_title,pg_url
                FROM
                tbl_pages pages
                WHERE pages.pg_status = 1 
                AND pages.is_delete = 0
                AND pages.pg_header = 'Y'";
    $results = $CIH->db->query($query);
    return $results->result_array();
}

function about_side_menu(){
    $CIH = &get_instance();
    $query = "SELECT
            subpages.sub_pg_name,subpages.sub_id,subpages.sub_pg_url,subpages.sub_pg_title
            FROM
            tbl_pages_sub subpages
            WHERE subpages.sub_pg_status = 1 
            AND subpages.sub_is_delete = 0
            AND subpages.sub_pg_id = 4
            ORDER BY side_menu_order ASC";
    $results = $CIH->db->query($query);
    return $results->result_array();
}
function about_side_menu_active(){
    $CIH = &get_instance();
    $query = "SELECT
            subpages.sub_pg_name,subpages.sub_id,subpages.sub_pg_url,subpages.sub_pg_title
            FROM
            tbl_pages_sub subpages
            WHERE subpages.sub_pg_status = 1 
            AND subpages.sub_is_delete = 0
            AND subpages.sub_pg_id = 4
            ORDER BY side_menu_order ASC LIMIT 1";
    $results = $CIH->db->query($query);
    return $results->row();
}
