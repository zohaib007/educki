<?php

class Gallery_model extends CI_Model {

    public function __construct() {

        $this->load->database();
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN
    function add_gall_detail($data) {

        $query = $this->db->insert('tbl_gallery', $data);

        $sql = $this->db->insert_id();
        return $sql;
    }

    function gallery_listing() {
        $query = $this->db->get_where('tbl_gallery',array('is_delete'=>0));
        return $query->result_array();
    }
       function cat_name($id)
   {
      
       $query = $this->db->get_where('tbl_gall_cat_name', array('gall_cat_id' => $id));

        $query->result();
        if ($query->num_rows > 0) {
            foreach ($query->result() as $row) {

                $firstimage = $row->gall_cat_name;
            }
            return $firstimage;
        }
      
   }

    function gall_cat_check($vender) {

        $query = $this->db->get_where('tbl_gall_cat_name', array('gall_cat_name' => $vender));
        $data = $query->result_array();
        $result = count($data);
        if ($result == 0) {
            $sql = $result;
        } elseif ($result == 1) {
            $sql = $data[0]['gall_cat_id'];
        }


        return $sql;
    }

    function add_gall_cat($data) {
        $array = array('gall_cat_name' => $data);
        $sq = $this->db->insert('tbl_gall_cat_name', $array);
        $sql = $this->db->insert_id();
        return $sql;
    }

    function gall_cat() {

        $query = $this->db->get('tbl_gall_cat_name');

        return $query->result();
    }
      function add_gallery_img($id, $image) {
        $array = array('gall_cat_id' => $id, 'gall_img_path' => $image);
        $this->db->insert('tbl_gall_image_path', $array);
        $sql = $this->db->insert_id();
        return $sql;
    }
       function getfirst_image($id) {

        // echo $id;
        $this->db->limit(1);
        $query = $this->db->get_where('tbl_gall_image_path', array('gall_cat_id' => $id));

        $query->result();
        if ($query->num_rows > 0) {
            foreach ($query->result() as $row) {

                $firstimage = $row->gall_img_path;
            }
            return $firstimage;
        }
    }
    
    function edit_gall_data($id)
    {
       
       
        $this->db->select('*');
        $this->db->from('tbl_gall_cat_name');
        $this->db->join('tbl_gallery', 'tbl_gall_cat_name.gall_cat_id = tbl_gallery.gall_cat_id', 'left outer');     
        $this->db->where('tbl_gallery.gall_id', $id);
        $q = $this->db->get();
       
        return $q->result_array();
        
    }
     function gall_edit_image($id) {
        $query = $this->db->get_where('tbl_gall_image_path', array('gall_cat_id' => $id));
        return $query->result();
    }


  function image_delete($id) {
      
					$select = "SELECT `gall_img_path` FROM `tbl_gall_image_path` WHERE `gall_img_id`=".$id."";
					$image  =$this->db->query($select);
					
					$rootpath=$_SERVER['DOCUMENT_ROOT'].'/customadmin/images/gallery_images/';
					$rootpaththumb=$_SERVER['DOCUMENT_ROOT'].'/customadmin/images/gallery_images/thumb/';
					
					
					if ($image->num_rows > 0) {
					foreach ($image->result() as $row) {
					
					$image = $row->gall_img_path;
					$rootpath.$image;
					
					@unlink($rootpath.$image);
					@unlink($rootpaththumb.$image);
					// $this->db->delete('tbl_gall_image_path', array('tbl_cat_id' => $val)); 
					}
					
					}
					
					
					$this->db->where('gall_img_id', $id);
					$this->db->delete('tbl_gall_image_path');
					return "ok";
    }
    function edit_gall_detail($table, $col,$id, $val) {

     
            $this->db->where($col, $id);
            $this->db->update($table, $val);
            $sql = $this->db->insert_id();
           return $sql;
        
      
 }
     function commonDelete($table,$col,$val)

    {
                     
$select = "SELECT `gall_img_path` FROM `tbl_gall_image_path` WHERE `gall_cat_id`=".$val."";
   $image  =$this->db->query($select);
   
  $rootpath=$_SERVER['DOCUMENT_ROOT'].'/customadmin/images/gallery_images/';
  $rootpaththumb=$_SERVER['DOCUMENT_ROOT'].'/customadmin/images/gallery_images/thumb/';

 
    if ($image->num_rows > 0) {
            foreach ($image->result() as $row) {

               $image = $row->gall_img_path;
                $rootpath.$image;
              
                @unlink($rootpath.$image);
                @unlink($rootpaththumb.$image);
              // $this->db->delete('tbl_gall_image_path', array('tbl_cat_id' => $val)); 
            }
           
        }


	  $this->db->where($col,$val);

	     $this->db->update($table, array('is_delete'=>1));

	  return "done";
    }
 
 
 
 
 


    function commonUpdate($table, $col, $val) {

        $this->db->where($col, $val);
        $result = $this->db->get($table);
        $data = $result->result();

        foreach ($data as $value) {
            $value->is_active;
        }

        if ($value->is_active == 0) {
            $this->db->where($col, $val);
            $this->db->update($table, array('is_active' => 1));
            $status = 1;
        } else {
            $this->db->where($col, $val);
            $this->db->update($table, array('is_active' => 0));
            $status = 0;
        }

        return $status;
    }


    function edit_news($id) {
        $query = $this->db->get_where('newsletter', array('gall_id' => $id));
        //print_r($query->result_array());
        return $query->result_array();
    }

    function submit_editnew($data, $id) {
//          print_r($data);
//          exit;
        $this->db->where('gall_id', $id);
        $this->db->update('newsletter', $data);
    }

    //////////////////////////////////////////////////////////////

    function listing_sub() {
        $query = $this->db->get_where('subscriber', array('is_delete' => 0));
        return $query->result_array();
    }

    function add_sub($data) {
//         print_r($data);
//         exit;

        $query = $this->db->insert('subscriber', $data);

        $sql = $this->db->insert_id();
        return $sql;
    }

    function commonselect($tablename, $col_name, $value, $order_by = '', $order = "ASC") {

        $this->db->select('*');

        $this->db->from($tablename);

        $this->db->where($col_name, $value);

        if ($order_by != '') {

            $this->db->order_by($order_by, $order);
        }

        $query = $this->db->get();
        return $query;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    function commonselect2($tablename, $col_name, $value, $col_name2, $value2) {

        $this->db->select('*');

        $this->db->from($tablename);

        $this->db->where($col_name, $value);

        $this->db->where($col_name2, $value2);

        $query = $this->db->get();

        return $query;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    function getCombox($tablename) {

        $this->db->select('*');

        $this->db->from($tablename);

        //$this->db->order_by("com_name", "asc"); 
        $query = $this->db->get();

        return $query;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN


    function getCombox2($tablename) {

        $this->db->select('*');

        $this->db->from($tablename);

        $this->db->order_by("com_name", "asc");
        $query = $this->db->get();

        return $query;
    }

    function getComboxuser($tablename, $where) {

        $this->db->select('*');

        $this->db->from($tablename);

        $this->db->where('user_id', $where);

        $query = $this->db->get();

        return $query;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    function getComboxpages($tablename, $where) {

        $this->db->select('*');

        $this->db->from($tablename);

        $this->db->where('page_sefurl', $where);

        $query = $this->db->get();

        return $query;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    function commonSave($table, $data) {

        $this->db->insert($table, $data);

        return $this->db->insert_id();
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN
// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    function commonUpdate2($table, $data, $col, $val, $col2, $val2) {

        $this->db->where($col, $val);

        $this->db->where($col2, $val2);

        $this->db->update($table, $data);
    }

    // MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    function commonUpdate3($table, $data, $col, $val, $col2, $val2, $col3, $val3) {

        $this->db->where($col, $val);

        $this->db->where($col2, $val2);
        $this->db->where($col3, $val3);

        $this->db->update($table, $data);
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN



    function commonDelete2($table, $col, $val, $col2, $val2) {

        $this->db->where($col, $val);

        $this->db->where($col2, $val2);

        $this->db->delete($table);
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    function getCommon($table) {

        $this->db->select('*');

        $this->db->from($table);

        $query = $this->db->get();

        return $query;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    function recordExistCommon($table, $field_name, $field_value) {



        if (trim($field_value) != '') {

            $this->db->select($field_name);

            $this->db->from($table);

            $this->db->where($field_name, $field_value);

            $query = $this->db->get();

            if ($query->num_rows() > 0) {



                return true;
            } else {

                return false;
            }
        } else {

            return false;
        }
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    function recordExistCommon2($table, $field_name, $field_value, $field_name2, $field_value2) {



        if (trim($field_value) != '') {

            $this->db->select($field_name);

            $this->db->from($table);

            $this->db->where($field_name, $field_value);

            $this->db->where($field_name2, $field_value2);

            $query = $this->db->get();

            if ($query->num_rows() > 0) {



                return true;
            } else {

                return false;
            }
        } else {

            return false;
        }
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    function getSingleValue($table, $where, $fieldname) {

        $q = "select $fieldname from $table where $where";



        $query = $this->db->query($q);

        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row)
                return $row->$fieldname; //$row[$fieldname];
        }

        else
            return 'N/A';
    }

// function close getSingleValue
    //////update/////////
    function update($tableName, $arr, $where) {
        $str = $this->db->update_string($tableName, $arr, $where);
        $rs = $this->db->query($str);
        return $rs;
    }

//////////////////update///////////////
// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    function common_get_colum_value($col, $table, $condition_str) {

        $res = '';

        $this->db->select($col);

        $this->db->from($table);

        $this->db->where($condition_str);

        $result = $this->db->get();

        if ($result->num_rows() > 0) {

            foreach ($result->result_array() as $row) {

                return $res = $row[$col];
            }
        }

        return $res;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    function query($query) {

        return $result = $this->db->query($query);
    }

    function common_where($tbl, $where, $orderBy = "", $orderType = "") {
        $this->db->where($where);
        if ($orderBy != "")
            $this->db->order_by($orderBy, $orderType);

        return $this->db->get($tbl);
    }

    function countRecord($tbl, $where = "") {
        return $this->db->query("SELECT COUNT(*) AS Num FROM  " . $tbl . " " . $where)->row()->Num;
    }

//MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    function countQuery($table, $select, $where = "") {

        $this->db->select($select);
        $this->db->from($table);
        if ($where != "") {
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function strip_word_html($text, $allowed_tags = '') {
        mb_regex_encoding('UTF-8');
        $search = array('/&lsquo;/u', '/&rsquo;/u', '/&ldquo;/u', '/&rdquo;/u', '/&mdash;/u');
        $replace = array('\'', '\'', '"', '"', '-');
        $text = preg_replace($search, $replace, $text);

        $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');

        if (mb_stripos($text, '/*') !== FALSE) {
            $text = mb_eregi_replace('#/\*.*?\*/#s', '', $text, 'm');
        }

        $text = preg_replace(array('/<([0-9]+)/'), array('< $1'), $text);
        $text = strip_tags($text, $allowed_tags);
        $text = preg_replace(array('/^\s\s+/', '/\s\s+$/', '/\s\s+/u'), array('', '', ' '), $text);
        $search = array('#<(strong|b)[^>]*>(.*?)</(strong|b)>#isu', '#<(em|i)[^>]*>(.*?)</(em|i)>#isu', '#<u[^>]*>(.*?)</u>#isu');
        $replace = array('<b>$2</b>', '<i>$2</i>', '<u>$1</u>');
        $text = preg_replace($search, $replace, $text);
        $num_matches = preg_match_all("/\<!--/u", $text, $matches);
        if ($num_matches) {
            $text = preg_replace('/\<!--(.)*--\>/isu', '', $text);
        }
        return $text;
    }

}

?>