<?php
 class Categories_model extends CI_Model{
    public function __construct()
 	{
 		$this->load->database();
 	}
	  function check_url($slug)
    {
        $query="SELECT * FROM tbl_categories WHERE cat_url LIKE '$slug%'";
        $data=$this->db->query($query);
        return $data->result_array();
    }
	
	function  FeaturedArticles($nId)
	{
		$query=" SELECT COUNT(*) AS Num FROM  tbl_artical INNER JOIN tbl_categories ON  `art_cat_id` = cat_id  WHERE   art_cat_id  = ".$nId." and  art_feature  = 1 and  art_status  = 1 ";	
		$query = $this->db->query($query);
		 if ($query->num_rows() > 0) return $query->result_array();
				return NULL;
	}
	
	function  FeaturedProducts($strWhere)
	{
 		$query=" SELECT COUNT( prod_id ) AS Num FROM  `tbl_products`
				INNER JOIN tbl_categories AS channel ON prod_cat_id_channel =  channel.cat_id 
				INNER JOIN tbl_categories AS category ON prod_cat_id  =  category.cat_id 				 
				INNER JOIN tbl_categories AS subCategory ON prod_sub_cat_id  =  subCategory.cat_id
				 INNER JOIN  tbl_company ON com_id = prod_com_id
		 WHERE  `prod_status` =0  AND  `prod_type` =1  ".$strWhere;	
		$query = $this->db->query($query);
		 if ($query->num_rows() > 0) return $query->result_array();
				return NULL;
	}
	
	function  CategorySpec($nId)
	{
		$query="SELECT   DISTINCT `cat_spec_table_no` , cat_spec_heading    FROM tblcategory_spec  WHERE  cat_spec_cat_id = ".$nId."";	
		$query = $this->db->query($query);
		 if ($query->num_rows() > 0) return $query->result_array();
				return NULL;
	}
        
	
	function get_categories($strWhere="") //"ASC"
	{
		$query = "
				SELECT * 
				FROM tbl_categories
				WHERE
				cat_id > 0 
				".$strWhere."
				ORDER BY cat_name ASC
				";
		
		$query = $this->db->query($query);
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
        	return NULL;
		}
	}
	
	
	
	
	
	
	
	function getCategories($strWhere="", $recordperpage=20, $page=0,$nRecordCount=0,$orderby="") //"ASC"
	{

		$query ="SELECT * FROM  tbl_categories WHERE  	cat_id > 0  ".$strWhere;	
		if($nRecordCount>0)
		{
			$query .= " ORDER BY cat_name ASC LIMIT ".(int)$page.", ".(int)$recordperpage;		
		}	 
		
		$query.=$orderby;
	
			$query = $this->db->query($query);
			 if ($query->num_rows() > 0)
                             
                             return $query->result_array();
                         
					return NULL;
	}
        

	function getmainCategories($strWhere="", $recordperpage=20, $page=0,$nRecordCount=0) //"ASC"
	{

		 $query =" select * from ( 
										SELECT cat_id AS categoryId ,cat_status AS categorystatus,  cat_name AS categoryName,cat_order AS categoryorder, cat_cat_id  from  tbl_categories WHERE  cat_id > 0    ".$strWhere."
									)
									 tblcategory inner join   tbl_categories AS tblchannel ON tblcategory.cat_cat_id = tblchannel.cat_id  ORDER BY   categoryId DESC
 ";	
		if($nRecordCount>0)
		{
			$query .= "  LIMIT ".(int)$page.", ".(int)$recordperpage;		
		}	 
	
			$query = $this->db->query($query);
			 if ($query->num_rows() > 0) return $query->result_array();
					return NULL;

	}
	
	function getcompany($strWhere="", $recordperpage=20, $page=0,$nRecordCount=0) //"ASC"
	{

		$query ="SELECT * FROM  tbl_company WHERE  	com_id > 0 ".$strWhere;	
		if($nRecordCount>0)
		{
			$query .= " ORDER BY  com_name ASC LIMIT ".(int)$page.", ".(int)$recordperpage;		
		}	 
	
			$query = $this->db->query($query);
			 if ($query->num_rows() > 0) return $query->result_array();
					return NULL;

	}
	
	
	
	function getarticles($strWhere="", $recordperpage=20, $page=0,$nRecordCount=0) //"ASC"
	{

		$query ="SELECT tbl_categories.*,tbl_artical.* FROM  tbl_artical inner join tbl_categories  tbl_categories ON tbl_categories.cat_id = tbl_artical.art_cat_id  WHERE  	art_id > 0 ".$strWhere ;	
		if($nRecordCount>0)
		{
			$query .= " ORDER BY  cat_name  ASC LIMIT ".(int)$page.", ".(int)$recordperpage;		
		}	 
	
			$query = $this->db->query($query);
			 if ($query->num_rows() > 0) return $query->result_array();
					return NULL;
	}


	


	function getsubmenu($strWhere="", $recordperpage=20, $page=0,$nRecordCount=0) //"ASC"
	{

		 $query =" select * from ( 
										SELECT cat_id AS categoryId ,cat_status AS categorystatus,  cat_name AS categoryName,cat_order AS categoryorder, cat_cat_id  from  tbl_categories WHERE  cat_id > 0    ".$strWhere."
									)
									 tblcategory inner join   tbl_categories AS tblchannel ON tblcategory.cat_cat_id = tblchannel.cat_id 
 ";	
		if($nRecordCount>0)
		{
			$query .= " ORDER BY   cat_id DESC LIMIT ".(int)$page.", ".(int)$recordperpage;		
		}	 
	
			$query = $this->db->query($query);
			 if ($query->num_rows() > 0) return $query->result_array();
					return NULL;

	}




	public function getFilterById($id){
		$result = $this->db->query('
			SELECT filter_title AS title,cat_filter_values AS value
			FROM tbl_cat_filter_title
			WHERE filter_title_id = '.$id.'
		
		');
		
		return $result;
	}
	
	public function getFilterDetailById($id){
		$result = $this->db->query('
			SELECT *
			FROM tbl_cat_filter_detail
			WHERE filter_detail_id = '.$id.'
		
		');
		
		return $result;
	}


}



?>