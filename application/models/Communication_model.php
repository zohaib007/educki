<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class communication_model extends CI_Model{

	public function __construct()
	{
	
		$this->load->database();
	
	}
	
	public function get_dropship_data($table ,$limit ="" ,$start ="" )
	{
		$query ="SELECT * from ".$table.""; 
		if($limit>0)
		{
			$query .= " order by drop_id DESC LIMIT ".(int)$start.", ".(int)$limit;		
		}
        
		$query = $this->db->query($query);
        return $query;
		
	}
	
	
	////////////////////////////email trigger functions///////////////////////////////////////////////
	public function getEmailMessages($strWhere="") //"ASC"
	{
		$query ="SELECT * FROM tbl_email_temp WHERE  e_email_id > 0 ORDER BY date_created DESC ".$strWhere;	
		$query = $this->db->query($query);
		if ($query->num_rows() > 0) return $query->result_array();
		return NULL;
	}
	///////MMMMMMMMMMMMM  ADD NEWSLETTER MESSAGES NOT USE IN BITGLASS /////////////////
	public function add_emailtrigger($data)
	{
		$query = $this->db->insert('tbl_email_temp', $data);
      
        $sql = $this->db->insert_id();
        return $sql;
    }
	
	//MMMMMMMMMMMMMMMMMMM  EDIT NEWS  MMMMMMMMMMMMMMMMM  
	
	function edit_emailtrigger($id)
    {
    	$query = $this->db->get_where('tbl_email_temp', array('e_email_id' => $id));
        return $query;
    }
	
	//MMMMMMMMMMMMMMMMMMM  EDIT NEWS NOT USE IN BITGLASS MMMMMMMMMMMMMMMMM  
	
	 function update_emailtrigger($data, $id) 
	 {
        $this->db->where('e_email_id', $id);
        $this->db->update('tbl_email_temp', $data);
    }
	
	
	function add_news($data)
	{
		$query = $this->db->insert('newsletter', $data);
		$sql = $this->db->insert_id();
		return $sql;
	
	}
	function listing_news()
	{
		$query = $this->db->get_where('newsletter', array('is_delete' =>0));
		return $query->result_array();
	}
	function commonUpdate($table,$col,$val)
	{
	
		$this->db->where($col,$val);
		$result = $this->db->get($table);
		$data=$result->result();
		foreach ($data as $value) {
			$value->is_active;
		
		}
		if($value->is_active==0)
		{
			$this->db->where($col,$val);
			$this->db->update($table, array('is_active'=>1));
			$status = 1;
		
		}
		else
		{
			$this->db->where($col,$val);
			$this->db->update($table, array('is_active'=>0));
			$status = 0;
		}
		return $status;
	}
	function commonDelete($table,$col,$val)
	{	
		$this->db->where($col,$val);
		$this->db->update($table, array('sub_is_delete'=>1));
		return "done";
	}
	function edit_news($id)
	{
		$query = $this->db->get_where('newsletter', array('nl_id' => $id));
		//print_r($query->result_array());
		return $query->result_array();
	}
	
	function submit_editnew($data, $id)
	{
		$this->db->where('nl_id', $id);
		$this->db->update('newsletter', $data);
	}
	//////////////////////////////////////////////////////////////
	
	function listing_sub()
	{
		$query="SELECT * FROM tbl_subscriber where sub_is_delete = 0 ORDER BY sub_id DESC";	
		$query = $this->db->query($query);
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return NULL;
		}
	}
	
	function export()
	{
	
		$query="SELECT sub_created_date,sub_email FROM tbl_subscriber where sub_is_delete = 0 ORDER BY sub_id DESC";	
		$query = $this->db->query($query);
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return NULL;
		}
	}
	function add_sub($data)
	{

		$query = $this->db->insert('tbl_subscriber', $data);
		$sql = $this->db->insert_id();
		return $sql;
	
	}
/////////////////////////////////end subscriber/////////////////////////////////////


	public function get_bulk_order($perPage = 50 , $page = 0, $ncount = 0)
	{
		$query = "
				SELECT *
				
				FROM tbl_bulk_order
				
				WHERE
				bulk_order_is_delete = 0
				
				ORDER BY bulk_order_id DESC
				";
				
		if($ncount > 0)
		{
			$query .= " LIMIT ".(int)$page.", ".(int)$perPage;
		}
				
		$query = $this->db->query($query);
		
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return NULL;
		}
	}
	
}	 

?>