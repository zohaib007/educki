<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Content_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function banner_upload($Files1, $file_name, $path, $pg_id, $page_name)
    {
        $filename = array();
        if ($Files1['name'] !== "") {
            $fconfig = array(
                'allowed_types' => 'jpg|jpeg|png|ppt|txt|pdf|gif',
                'upload_path'   => FCPATH . $path,
                'file_name'     => 'content_' . date('Y_m_d_h_i_s'),
            );
            $this->load->library('upload', $fconfig);
            if (!$this->upload->do_upload($file_name)) {
                //Print message if file is not uploaded
                echo $this->upload->display_errors();
                exit;
                $this->session->set_flashdata('msg', $this->upload->display_errors());
                header("Location: " . base_url() . "admin/pages/" . $page_name);
                exit();
            } else {
                $dataDP   = $this->upload->data();
                $filename = $dataDP['file_name'];
                $this->load->library('image_lib');
                $configt['image_library']  = 'gd2';
                $configt['source_image']   = $dataDP['full_path'];
                $configt['new_image']      = FCPATH . $path . "/thumb/";
                $configt['maintain_ratio'] = false;
                $configt['width']          = 80;
                $configt['height']         = 80;
                $this->image_lib->initialize($configt);
                $this->image_lib->resize();

                return $filename;
            }
        }
    }

}
