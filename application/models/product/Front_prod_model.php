<?php

class Front_prod_model extends CI_Model{

   public function __construct()
	{

		$this->load->database();

	}
	public function get_front_prod($sub_cat_id , $sort="")
	{
		$query = "
					SELECT * 
					FROM tbl_product
					INNER JOIN tbl_product_category ON tbl_product.prod_id = tbl_product_category.prod_cat_prod_id
					
					INNER JOIN tbl_categories AS navigation ON prod_cat_navigation_id = navigation.cat_id 
					INNER JOIN tbl_categories AS category ON prod_cat_category_id = category.cat_id 				 
					INNER JOIN tbl_categories AS subCategory ON prod_cat_sub_cat_id = subCategory.cat_id
					
					WHERE
					prod_is_delete = 0
					AND
					prod_status = 1
					AND
					prod_cat_sub_cat_id = ".$sub_cat_id." ORDER BY prod_price ".$sort." 
				";
		
		$result = $this->db->query($query);
		return $result;
		
	}
	
	
	public function get_related_prod($id)
	{

		$query = $this->db->query("
									SELECT *
									FROM tbl_product
									
									INNER JOIN tbl_product_category ON prod_cat_prod_id = prod_id
									
									INNER JOIN tbl_categories AS navigation ON prod_cat_navigation_id = navigation.cat_id 
									INNER JOIN tbl_categories AS category ON prod_cat_category_id = category.cat_id 				 
									INNER JOIN tbl_categories AS subCategory ON prod_cat_sub_cat_id = subCategory.cat_id
					
									WHERE
									
									tbl_product.prod_id IN (SELECT relp_prod_one_id
															FROM tbl_related_products
															WHERE prod_status = 1
															AND
															prod_is_delete = 0
															AND relp_prod_id = ".$id.")
									OR
									tbl_product.prod_id IN (SELECT relp_prod_two_id 
															FROM tbl_related_products
															WHERE prod_status = 1
															AND
															prod_is_delete = 0
															AND relp_prod_id = ".$id.")
									OR
									tbl_product.prod_id IN (SELECT relp_prod_three_id
															FROM tbl_related_products
															WHERE prod_status = 1
															AND
															prod_is_delete = 0
															AND relp_prod_id = ".$id.")
									OR
									tbl_product.prod_id IN (SELECT relp_prod_four_id
															FROM tbl_related_products
															WHERE prod_status = 1
															AND
															prod_is_delete = 0
															AND relp_prod_id = ".$id.")
									OR
									tbl_product.prod_id IN (SELECT relp_prod_five_id
															FROM tbl_related_products
															WHERE prod_status = 1
															AND
															prod_is_delete = 0
															AND relp_prod_id = ".$id.")
									OR
									tbl_product.prod_id IN (SELECT relp_prod_six_id
															FROM tbl_related_products
															WHERE prod_status = 1
															AND
															prod_is_delete = 0
															AND relp_prod_id = ".$id.")
									
									GROUP BY tbl_product.prod_id 
									
								");
		
/*		$query = "SELECT * FROM tbl_related_products WHERE relp_prod_id = ".$id."";
		$result = $this->db->query($query)->result_array();
		$query1 = "SELECT * FROM tbl_product 
		LEFT JOIN tbl_related_products  AS prod_rel_one
		ON tbl_product.prod_id = prod_rel_one.relp_prod_one_id
		
		LEFT JOIN tbl_related_products AS prod_rel_two
		ON tbl_product.prod_id = prod_rel_two.relp_prod_two_id
		
		LEFT JOIN tbl_related_products AS prod_rel_three
		ON tbl_product.prod_id = prod_rel_three.relp_prod_three_id
		
		LEFT JOIN tbl_related_products AS prod_rel_four
		ON tbl_product.prod_id = prod_rel_four.relp_prod_four_id
		
		LEFT JOIN tbl_related_products AS prod_rel_five
		ON tbl_product.prod_id = prod_rel_five.relp_prod_five_id
		
		LEFT JOIN tbl_related_products AS prod_rel_six
		ON tbl_product.prod_id = prod_rel_six.relp_prod_six_id
		
		
		
		WHERE 
		prod_rel_one.relp_id = ".$result[0]['relp_id']."
		
		";
		$result = $this->db->query($query1); */
		echo "<pre>";
		print_r($query->result_array());	
		exit; 
		
		return $query; 
	}
	public function get_related_prod_catname($id)
	{
	
		$query = $this->db->query("SELECT cat_url
									FROM  tbl_categories 
									JOIN tbl_product_category
									ON tbl_categories.cat_id = tbl_product_category.prod_cat_sub_cat_id
									WHERE prod_cat_prod_id =".$id."");
					
		return $query->result_array();
		
	}
	
	
	public function get_prod_review($prod_id)
	{
		if($prod_id == '')
		{
			return NULL;
		}
		$query = "
					SELECT
					tbl_reviews.*
					
					#, tbl_user.user_fname, tbl_user.user_lname, user_email
					
					FROM tbl_reviews
					
					#INNER JOIN tbl_user ON review_user_id = user_id
					
					INNER JOIN tbl_product ON review_prod_id = prod_id
					
					WHERE
					review_prod_id = ".$prod_id."
					AND
					review_status = 1
					AND
					prod_is_delete = 0
					
					ORDER BY tbl_reviews.review_id DESC
				";
				 
		
		$query = $this->db->query($query);
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return NULL;
		}
	}
	
	public function get_avg_review($prod_id)
	{
		if($prod_id == '')
		{
			return NULL;
		}
		$query = "
					SELECT count(*) as total_review, AVG(review_rating) as avg_rating
					
					FROM tbl_reviews
					
					INNER JOIN tbl_product ON prod_id = review_prod_id
					#INNER JOIN tbl_user ON review_user_id = user_id
					
					WHERE
					review_prod_id = ".$prod_id."
					AND
					review_status = 1
					AND
					prod_is_delete = 0
					ORDER BY  tbl_reviews.review_id DESC
				";
				 
		
		$query = $this->db->query($query);
		if ($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return NULL;
		}
	}
	
	
	public function get_product_detail($prod_where)
	{
		$query = $this->db->query("
									SELECT
									
									tbl_product.*,
									navigation.cat_name AS nav_name, navigation.cat_url AS nav_url, navigation.cat_id AS nav_id,
									category.cat_name AS cat_name, category.cat_url AS cat_url, category.cat_id AS cat_id,
									subCategory.cat_name AS sub_cat_name, subCategory.cat_url AS sub_cat_url, subCategory.cat_id AS sub_cat_id
									
									FROM tbl_product
									
									INNER JOIN tbl_product_category ON prod_cat_prod_id = prod_id
				
									INNER JOIN tbl_categories AS navigation ON prod_cat_navigation_id = navigation.cat_id 
									INNER JOIN tbl_categories AS category ON prod_cat_category_id = category.cat_id 				 
									INNER JOIN tbl_categories AS subCategory ON prod_cat_sub_cat_id = subCategory.cat_id
									
									WHERE
									
									prod_status = 1
									AND
									prod_is_delete = 0
									
									".$prod_where."
									
									GROUP BY prod_id
								");
		
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
	
	
	public function get_product_related($prod_id)
	{
		if($prod_id == '')
		{
			return NULL;
		}
		$query = $this->db->query("
									SELECT * FROM tbl_related_products
									WHERE
									relp_prod_id = '".$prod_id."'
								");
		
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
		
	}
	
	public function get_product_related_list($prod_id)
	{
		if($prod_id == '')
		{
			return NULL;
		}
		$list_products = $this->get_product_related($prod_id);
		
		/*echo '<pre>';
		print_r($list_products);
		exit;*/
		
				
		if($list_products['relp_prod_one_id'] > 0)
		{
			$related_product_array[] = $this->get_product_rel_detail(" AND prod_id = ".$list_products['relp_prod_one_id']);
		}
		
		if($list_products['relp_prod_two_id'] > 0)
		{
			$related_product_array[] = $this->get_product_rel_detail(" AND prod_id = ".$list_products['relp_prod_two_id']);
		}
		
		if($list_products['relp_prod_three_id'] > 0)
		{
			$related_product_array[] = $this->get_product_rel_detail(" AND prod_id = ".$list_products['relp_prod_three_id']);
		}
		
		if($list_products['relp_prod_four_id'] > 0)
		{
			$related_product_array[] = $this->get_product_rel_detail(" AND prod_id = ".$list_products['relp_prod_four_id']);
		}
		
		if($list_products['relp_prod_five_id'] > 0)
		{
			$related_product_array[] = $this->get_product_rel_detail(" AND prod_id = ".$list_products['relp_prod_five_id']);
		}
		
		if($list_products['relp_prod_six_id'] > 0)
		{
			$related_product_array[] = $this->get_product_rel_detail(" AND prod_id = ".$list_products['relp_prod_six_id']);
		}
		if(count(@$related_product_array) > 0)
		{
		
		return $related_product_array;
		}
		else
		{
			return false;
		}
	}
	
	public function get_product_rel_detail($prod_where)
	{
		$query = $this->db->query("
									SELECT
									
									tbl_product.*,
									navigation.cat_name AS nav_name, navigation.cat_url AS nav_url, navigation.cat_id AS nav_id,
									category.cat_name AS cat_name, category.cat_url AS cat_url, category.cat_id AS cat_id,
									subCategory.cat_name AS sub_cat_name, subCategory.cat_url AS sub_cat_url, subCategory.cat_id AS sub_cat_id
									
									FROM tbl_product
									
									INNER JOIN tbl_product_category ON prod_cat_prod_id = prod_id
				
									INNER JOIN tbl_categories AS navigation ON prod_cat_navigation_id = navigation.cat_id 
									INNER JOIN tbl_categories AS category ON prod_cat_category_id = category.cat_id 				 
									INNER JOIN tbl_categories AS subCategory ON prod_cat_sub_cat_id = subCategory.cat_id
									
									WHERE
									
									prod_status = 1
									AND
									prod_is_delete = 0
									
									".$prod_where."
									
									GROUP BY prod_id
								");
		
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}
	
	## Get prod price by given prod id
	## This Price sale price that will apply in cart and also check if prod is on sale or not
	public function get_product_price($prod_id)
	{
		$query = $this->db->query("
									SELECT * 
									FROM tbl_product
									INNER JOIN tbl_product_category
									ON prod_id = prod_cat_prod_id
									WHERE
									prod_status = 1
									AND
									prod_id = ''
								");
	
	}
	
	public function most_popular($sub_cat_id )
	{
		
		$query = $this->db->query("
									SELECT  tbl_product.*,
									SUM(tbl_order_detail.order_qty) AS QTY 
									FROM tbl_product
									INNER JOIN tbl_product_category ON tbl_product.prod_id = tbl_product_category.prod_cat_prod_id
									
									INNER JOIN tbl_categories AS navigation ON prod_cat_navigation_id = navigation.cat_id 
									INNER JOIN tbl_categories AS category ON prod_cat_category_id = category.cat_id 				 
									INNER JOIN tbl_categories AS subCategory ON prod_cat_sub_cat_id = subCategory.cat_id
									
									INNER JOIN tbl_order_detail  ON  tbl_product.prod_id = tbl_order_detail.order_prod_id
									
									WHERE
									prod_is_delete = 0
									AND
									prod_status = 1
									AND
									prod_cat_sub_cat_id = ".$sub_cat_id."
									GROUP BY prod_id
									ORDER BY QTY DESC 
									
								");
		return $query;
	
	}

}	 
