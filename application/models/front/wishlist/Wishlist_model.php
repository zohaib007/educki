<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wishlist_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_wish_list($user_id, $sort='', $to=0, $from = '', $is_limit = 0)
	{
		$query = "
				SELECT 
				prod.*, wish.id, store.store_name, store.store_url, images.img_name, category.cat_url as catURL, subCat.cat_url as subCatURL, subSubCat.cat_url as subSubCatURL
				FROM tbl_wish_list wish
				INNER JOIN tbl_products prod ON prod.prod_id = wish.product_id
				INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
				INNER JOIN tbl_product_images images ON prod.prod_id = images.img_prod_id
				INNER JOIN tbl_product_category prodCat ON prod.prod_id = prodCat.prod_id
				INNER JOIN tbl_categories category ON prodCat.category_id = category.cat_id
				LEFT JOIN tbl_categories subCat ON prodCat.sub_category_id = subCat.cat_id
				LEFT JOIN tbl_categories subSubCat ON prodCat.sub_sub_category_id = subSubCat.cat_id
				WHERE
				prod_is_delete = 0
				AND
				prod_status = 1
				AND
				wish.user_id = ".$user_id."
                                AND prod.prod_is_delete = 0
                                AND prod.qty > 0
				GROUP BY prod.prod_id
				ORDER BY wish.id $sort
				";
				#ORDER BY prod.prod_price ".$sort."
				
		if($is_limit == 1)
		{
			$query .= " LIMIT ".$to. "," .$from;
		}
		$result = $this->db->query($query);
		return $result;		
	}

}

?>