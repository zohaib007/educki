<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shop_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_product_data($prod_cat, $order = "DESC") {
        $query = "
                    SELECT *
                    FROM tbl_products prod
                    JOIN tbl_product_category cat ON prod.prod_id = cat.prod_id 
                    INNER JOIN tbl_categories AS main_cat ON cat.category_id = main_cat.cat_id
                    INNER JOIN tbl_categories AS sub_cat ON sub_cat.cat_parent_id = cat.sub_category_id
                    INNER JOIN tbl_categories AS sub_sub_cat ON sub_sub_cat.cat_parent_id = cat.category_id

                    WHERE prod.prod_is_delete = 0
                    AND qty > 0
                    AND prod_status = 1
                    AND prod.prod_id = " . $prod_cat->prod_id . "
                    GROUP BY prod.prod_id
                   
                    ORDER BY (CASE
                        WHEN prod.prod_onsale = 1 THEN sale_price
                        ELSE prod.prod_price
                        END)  ".$order."
                ";
                    // ORDER BY prod.prod_price " . $order . "";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
    }

    public function get_product_data_lvl3($prod_cat, $order = "DESC") {
        $query = "SELECT *
                    FROM tbl_products prod
                    JOIN tbl_product_category cat ON prod.prod_id = cat.prod_id 
                    INNER JOIN tbl_categories AS main_cat ON cat.category_id = main_cat.cat_id
                    WHERE
                    prod.prod_is_delete = 0 AND 
                    qty > 0 AND 
                    prod_status = 1 AND 
                    prod.prod_id = " . $prod_cat->prod_id . "
                    ORDER BY (CASE
                        WHEN prod.prod_onsale = 1 THEN sale_price
                        ELSE prod.prod_price
                        END)  ".$order."";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
    }

    public function get_product_images($id, $order = "DESC") {
        $query = "SELECT 
                    * 
                  FROM
                  tbl_product_images 
                  WHERE img_prod_id = " . $id . "
                  order by img_id " . $order . "";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function get_seller_details($product_id) {
        $user_id = $this->common_model->commonselect('tbl_products', 'prod_id', $product_id)->row();
        $query = "SELECT 
                    * 
                  FROM
                  tbl_user 
                  INNER JOIN tbl_stores 
                  ON tbl_stores.user_id = tbl_user.user_id 
                  WHERE tbl_user.user_id =" . $user_id->prod_user_id . "";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
    }

    public function get_seller_other_products($product_id) {
        $user_id = $this->common_model->commonselect('tbl_products', 'prod_id', $product_id)->row();
        $where = "prod.prod_store_id =" . $user_id->prod_store_id . " AND prod.prod_id !=" . $product_id . " AND store.store_is_hide = 0";
        $this->db->select('prod.*,category.cat_url AS catURL,category.cat_name AS catName,subCat.cat_url AS subCatURL,subSubCat.cat_url AS subSubCatURL,subSubCat.cat_name AS subSubCatName,
            users.user_fname,
            users.user_lname,store.store_name,
            store.store_url,prod_images.img_name');
        $this->db->from('tbl_products prod');
        $this->db->join('tbl_product_category prodCat', 'prod.prod_id = prodCat.prod_id', 'inner');
        $this->db->join('tbl_categories category', 'prodCat.category_id = category.cat_id', 'inner');
        $this->db->join('tbl_categories subCat', 'prodCat.sub_category_id = subCat.cat_id', 'left');
        $this->db->join('tbl_categories subSubCat', 'prodCat.sub_sub_category_id = subSubCat.cat_id', 'left');
        $this->db->join('tbl_user users', 'users.user_id = prod.prod_user_id', 'inner');
        $this->db->join('tbl_stores store', 'store.user_id = users.user_id', 'inner');
        $this->db->join('tbl_product_images prod_images', 'prod_images.img_prod_id = prod.prod_id', 'inner');
        $this->db->where('prod_is_delete', '0');
        $this->db->where('prod.prod_status', '1');
        $this->db->where('store.store_is_hide', '0');
        $this->db->where('prod.qty >', '0');
        $this->db->where($where);
        $this->db->group_by('prod.prod_id');
        $this->db->order_by("prod.prod_id", 'DESC');
        $this->db->limit("9");
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function product_listing_by_category($category_id, $sort_val, $limit, $start) {

        if (@$this->input->get('rangeOne') != '') {
            $rangeOne = @$this->input->get('rangeOne');
        }
        if (@$this->input->get('rangeTwo') != '') {
            $rangeTwo = @$this->input->get('rangeTwo');
        }
        $this->db->select('prod.*,category.cat_url AS catURL,category.cat_name AS catName,subCat.cat_url AS subCatURL,subSubCat.cat_url AS subSubCatURL,subSubCat.cat_name AS subSubCatName,
            users.user_fname,
            users.user_lname,store.store_name,
            store.store_url,prod_images.img_name');
        $this->db->from('tbl_products prod');
        $this->db->join('tbl_product_category prodCat', 'prod.prod_id = prodCat.prod_id', 'inner');
        $this->db->join('tbl_categories category', 'prodCat.category_id = category.cat_id', 'inner');
        $this->db->join('tbl_categories subCat', 'prodCat.sub_category_id = subCat.cat_id', 'left');
        $this->db->join('tbl_categories subSubCat', 'prodCat.sub_sub_category_id = subSubCat.cat_id', 'left');
        $this->db->join('tbl_user users', 'users.user_id = prod.prod_user_id', 'inner');
        $this->db->join('tbl_stores store', 'store.user_id = users.user_id', 'inner');
        $this->db->join('tbl_product_images prod_images', 'prod_images.img_prod_id = prod.prod_id', 'inner');
        $this->db->where('prod_is_delete', '0');
        $this->db->where('users.user_is_delete', '0');
        $this->db->where('prod.prod_status', '1');
        $this->db->where('store.store_is_hide', '0');
        $this->db->where('prod.qty >', '0');
        if ($rangeOne != '' && $rangeTwo != '') {
            $this->db->where("prod.prod_price BETWEEN " . $rangeOne . " AND " . $rangeTwo);
        }
        if (@$this->input->get('location') != '' && $this->input->get('radius') != "") {
            $location = $this->location();
            if ($location != '') {
                $this->db->where("$location");
            }
        }
        $this->db->where_in('prod.prod_cat_id', $category_id);
        $this->db->group_by('prod.prod_id');
        if ($sort_val == 2) {
            //$this->db->where('prod.prod_feature', '1');
            $this->db->order_by("prod.prod_feature", 'DESC');
        } elseif ($sort_val == 3) {
            $this->db->order_by("(CASE
                        WHEN prod.prod_onsale = 1 THEN sale_price
                        ELSE prod.prod_price
                        END) ", 'ASC');
        } elseif ($sort_val == 4) {
            $this->db->order_by("(CASE
                        WHEN prod.prod_onsale = 1 THEN sale_price
                        ELSE prod.prod_price
                        END) ", 'DESC');
        } else {
            $this->db->order_by("prod.prod_id", 'DESC');
        }
        $this->db->limit($limit, $start);
        $result = $this->db->get();
        /*echo $this->db->last_query();*/
        // echo '<pre>';
        // print_r($this->db->last_query());
        // exit;
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function product_listing_by_filters($category_id, $sort_val, $limit, $start, $singlefilter, $multiple) {
        /*echo "<pre>";
        print_r($multiple);
        print_r($singlefilter);
        exit;*/
        if (@$this->input->get('rangeOne') != '') {
            $rangeOne = @$this->input->get('rangeOne');
        }
        if (@$this->input->get('rangeTwo') != '') {
            $rangeTwo = @$this->input->get('rangeTwo');
        }
        $this->db->select('prod.*,category.cat_url AS catURL,category.cat_name AS catName,subCat.cat_url AS subCatURL,subSubCat.cat_url AS subSubCatURL,subSubCat.cat_name AS subSubCatName,
            users.user_fname,
            users.user_lname,store.store_name,
            store.store_url,prod_images.img_name');
        $this->db->from('tbl_products prod');
        $this->db->join('tbl_product_category prodCat', 'prod.prod_id = prodCat.prod_id', 'inner');
        $this->db->join('tbl_categories category', 'prodCat.category_id = category.cat_id', 'inner');
        $this->db->join('tbl_categories subCat', 'prodCat.sub_category_id = subCat.cat_id', 'left');
        $this->db->join('tbl_categories subSubCat', 'prodCat.sub_sub_category_id = subSubCat.cat_id', 'left');
        $this->db->join('tbl_user users', 'users.user_id = prod.prod_user_id', 'inner');
        $this->db->join('tbl_stores store', 'store.user_id = users.user_id', 'inner');
        $this->db->join('tbl_product_images prod_images', 'prod_images.img_prod_id = prod.prod_id', 'inner');
        $this->db->join('tbl_product_filters pf', 'pf.prod_id = prod.prod_id', 'left');
        $this->db->join('tbl_product_filters_detail pd', 'pd.prod_filter_id = pf.id', 'left');
        $this->db->where('prod_is_delete', '0');
        $this->db->where('prod.prod_status', '1');
        $this->db->where('store.store_is_hide', '0');
        $this->db->where('prod.qty >', '0');
        if ($rangeOne != '' && $rangeTwo != '') {
            $this->db->where("prod.prod_price BETWEEN " . $rangeOne . " AND " . $rangeTwo);
        }
        if (@$this->input->get('location') != '' && $this->input->get('radius') != "") {
            $location = $this->location();
            if ($location != '') {
            //echo $location;
                $this->db->where($location);
            }
        }
        if ($singlefilter) {
            $this->db->where("$singlefilter");
        }
        if ($multiple) {
            $this->db->where("$multiple");
        }
        $this->db->where('prod.prod_cat_id', $category_id);
        $this->db->group_by('prod.prod_id');
        if ($sort_val == 2) {
            //$this->db->where('prod.prod_feature', '1');
            $this->db->order_by("prod.prod_feature", 'DESC');
        } elseif ($sort_val == 3) {
            $this->db->order_by("(CASE
                        WHEN prod.prod_onsale = 1 THEN sale_price
                        ELSE prod.prod_price
                        END) ", 'ASC');
        } elseif ($sort_val == 4) {
            $this->db->order_by("(CASE
                        WHEN prod.prod_onsale = 1 THEN sale_price
                        ELSE prod.prod_price
                        END) ", 'DESC');
        } else {
            $this->db->order_by("prod.prod_id", 'DESC');
        }
        $this->db->limit($limit, $start);
        //    echo '<pre>';
        //   echo $this->db->_compile_select();
        //  exit;
        $result = $this->db->get();

        // echo '<pre>';
        // print_r($this->db->last_query());
        // exit;

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function product_price($category_id, $sort_val) {
        $this->db->select('prod.*,category.cat_url AS catURL,category.cat_name AS catName,subCat.cat_url AS subCatURL,subSubCat.cat_url AS subSubCatURL,subSubCat.cat_name AS subSubCatName,
            users.user_fname,
            users.user_lname,store.store_name,
            store.store_url,prod_images.img_name');
        $this->db->from('tbl_products prod');
        $this->db->join('tbl_product_category prodCat', 'prod.prod_id = prodCat.prod_id', 'inner');
        $this->db->join('tbl_categories category', 'prodCat.category_id = category.cat_id', 'inner');
        $this->db->join('tbl_categories subCat', 'prodCat.sub_category_id = subCat.cat_id', 'left');
        $this->db->join('tbl_categories subSubCat', 'prodCat.sub_sub_category_id = subSubCat.cat_id', 'left');
        $this->db->join('tbl_user users', 'users.user_id = prod.prod_user_id', 'inner');
        $this->db->join('tbl_stores store', 'store.user_id = users.user_id', 'inner');
        $this->db->join('tbl_product_images prod_images', 'prod_images.img_prod_id = prod.prod_id', 'inner');
        $this->db->where('prod_is_delete', '0');
        $this->db->where('prod.prod_status', '1');
        $this->db->where('store.store_is_hide', '0');
        $this->db->where('prod.qty >', '0');
        $this->db->where_in('prod.prod_cat_id', $category_id);
        $this->db->group_by('prod.prod_id');
        if ($sort_val == 2) {
            $this->db->order_by("prod.prod_id", 'DESC');
        } elseif ($sort_val == 3) {
            $this->db->order_by("(CASE
                        WHEN prod.prod_onsale = 1 THEN sale_price
                        ELSE prod.prod_price
                        END) ", 'ASC');
        } elseif ($sort_val == 4) {
            $this->db->order_by("(CASE
                        WHEN prod.prod_onsale = 1 THEN sale_price
                        ELSE prod.prod_price
                        END) ", 'DESC');
        } else {
            $this->db->order_by("prod.prod_id", 'DESC');
        }
        $this->db->get();
        $query = $this->db->last_query();
        $new_query = "SELECT MIN(price.prod_price) AS price_minimum,
                         MAX(price.prod_price) AS price_maximum,COUNT(price.prod_id) AS product_count
                         FROM (" . $query . ") AS price";
//        var_dump($new_query);die;
        $result = $this->db->query($new_query);
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
    }

    public function getbreadcrumbs_category_data($breadcrumbs_data) {
        $query = "SELECT 
                category.cat_url AS catURL,
                category.cat_name AS catName,
                subCat.cat_url AS subCatURL,
                subCat.cat_name AS subCatName,
                subSubCat.cat_url AS subSubCatURL,
                subSubCat.cat_name AS subSubCatName
              FROM
                tbl_products prod 
                INNER JOIN tbl_product_category prodCat 
                  ON prod.prod_id = prodCat.prod_id 
                INNER JOIN tbl_categories category 
                  ON prodCat.category_id = category.cat_id 
                LEFT JOIN tbl_categories subCat 
                  ON prodCat.sub_category_id = subCat.cat_id 
                LEFT JOIN tbl_categories subSubCat 
                  ON prodCat.sub_sub_category_id = subSubCat.cat_id
                  WHERE category.cat_url ='" . $breadcrumbs_data['segment1'] . "'";
        if ($breadcrumbs_data['segment2'] != '') {
            $query .= " AND subCat.cat_url = '" . $breadcrumbs_data['segment2'] . "'";
        }
        if ($breadcrumbs_data['segment3'] != '') {
            $query .= " AND subSubCat.cat_url = '" . $breadcrumbs_data['segment3'] . "'";
        }
        $result = $this->db->query($query);
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
    }

    public function getbreadcrumbs_product_data($breadcrumbs_data) {

        $query = "SELECT 
                category.cat_url AS catURL,
                category.cat_name AS catName,
                subCat.cat_url AS subCatURL,
                subCat.cat_name AS subCatName,
                subSubCat.cat_url AS subSubCatURL,
                subSubCat.cat_name AS subSubCatName,
                prod.prod_url AS prodURL,
                prod.prod_title AS prodTitle
              FROM
                tbl_products prod 
                INNER JOIN tbl_product_category prodCat 
                  ON prod.prod_id = prodCat.prod_id 
                INNER JOIN tbl_categories category 
                  ON prodCat.category_id = category.cat_id 
                LEFT JOIN tbl_categories subCat 
                  ON prodCat.sub_category_id = subCat.cat_id 
                LEFT JOIN tbl_categories subSubCat 
                  ON prodCat.sub_sub_category_id = subSubCat.cat_id
                  WHERE category.cat_url ='" . $breadcrumbs_data['segment1'] . "'";
        if ($breadcrumbs_data['segment2'] != '') {
            if ($breadcrumbs_data['prod_level'] == 2) {
                $query .= " AND prod.prod_url = '" . $breadcrumbs_data['segment2'] . "'";
            } else {
                $query .= " AND subCat.cat_url = '" . $breadcrumbs_data['segment2'] . "'";
            }
        }
        if ($breadcrumbs_data['segment3'] != '') {
            if ($breadcrumbs_data['prod_level'] == 3) {
                $query .= " AND prod.prod_url = '" . $breadcrumbs_data['segment3'] . "'";
            } else {
                $query .= " AND subSubCat.cat_url = '" . $breadcrumbs_data['segment3'] . "'";
            }
        }
        if ($breadcrumbs_data['prod_level'] == 4) {
            $query .= " AND prod.prod_url = '" . $breadcrumbs_data['segment4'] . "'";
        }
//        echo $query;die;
        $result = $this->db->query($query);
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
    }

    public function view_product_listing_by_category($category_id, $sort_val) {
        $rangeOne = '';
$rangeTwo = '';
        if (@$this->input->get('rangeOne') != '') {
            $rangeOne = @$this->input->get('rangeOne');
        }
        if (@$this->input->get('rangeTwo') != '') {
            $rangeTwo = @$this->input->get('rangeTwo');
        }
        $this->db->select('prod.*,
                            category.cat_url AS catURL,category.cat_name AS catName,subCat.cat_url AS subCatURL,
                            subSubCat.cat_url AS subSubCatURL,subSubCat.cat_name AS subSubCatName,
                            users.user_fname,
                            users.user_lname,store.store_name,
                            store.store_url,prod_images.img_name');
        $this->db->from('tbl_products prod');
        $this->db->join('tbl_product_category prodCat', 'prod.prod_id = prodCat.prod_id', 'inner');
        $this->db->join('tbl_categories category', 'prodCat.category_id = category.cat_id', 'inner');
        $this->db->join('tbl_categories subCat', 'prodCat.sub_category_id = subCat.cat_id', 'left');
        $this->db->join('tbl_categories subSubCat', 'prodCat.sub_sub_category_id = subSubCat.cat_id', 'left');
        $this->db->join('tbl_user users', 'users.user_id = prod.prod_user_id', 'inner');
        $this->db->join('tbl_stores store', 'store.user_id = users.user_id', 'inner');
        $this->db->join('tbl_product_images prod_images', 'prod_images.img_prod_id = prod.prod_id', 'inner');
        $this->db->where('prod_is_delete', '0');
        $this->db->where('users.user_is_delete', '0');
        $this->db->where('prod.prod_status', '1');
        $this->db->where('store.store_is_hide', '0');
        $this->db->where('prod.qty >', '0');
        if ($rangeOne != '' && $rangeTwo != '') {
            $this->db->where("prod.prod_price BETWEEN " . $rangeOne . " AND " . $rangeTwo);
//            $range = "AND prod.prod_price BETWEEN " . $rangeOne . " AND " . $rangeTwo;
        }
        if (@$this->input->get('location') != '' && $this->input->get('radius') != "") {
            $location = $this->location();
            if ($location != '') {
                $this->db->where("$location");
            }
        }
        $this->db->where_in('prod.prod_cat_id', $category_id);
        $this->db->group_by('prod.prod_id');
        if ($sort_val == 2) {
            //$this->db->where('prod.prod_feature', '1');
            $this->db->order_by("prod.prod_feature", 'DESC');
        } elseif ($sort_val == 3) {
            $this->db->order_by("(CASE
                        WHEN prod.prod_onsale = 1 THEN sale_price
                        ELSE prod.prod_price
                        END) ", 'ASC');
        } elseif ($sort_val == 4) {
            $this->db->order_by("(CASE
                        WHEN prod.prod_onsale = 1 THEN sale_price
                        ELSE prod.prod_price
                        END) ", 'DESC');
        } else {
            $this->db->order_by("prod.prod_id", 'DESC');
        }
        $result = $this->db->get();
        // echo "<pre>";
        // print_r($this->db->last_query());
        // exit;
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function view_product_listing_by_filters($category_id, $sort_val, $singlefilter, $multiple) {

        /*echo "<pre>";
        print_r($singlefilter);
        print_r($multiple);
        exit;*/
        $rangeOne = '';
        $rangeTwo = '';
        if (@$this->input->get('rangeOne') != '') {
            $rangeOne = @$this->input->get('rangeOne');
        }
        if (@$this->input->get('rangeTwo') != '') {
            $rangeTwo = @$this->input->get('rangeTwo');
        }
        $this->db->select('prod.*,category.cat_url AS catURL,category.cat_name AS catName,subCat.cat_url AS subCatURL,subSubCat.cat_url AS subSubCatURL,subSubCat.cat_name AS subSubCatName,
            users.user_fname,
            users.user_lname,store.store_name,
            store.store_url,prod_images.img_name');
        $this->db->from('tbl_products prod');
        $this->db->join('tbl_product_category prodCat', 'prod.prod_id = prodCat.prod_id', 'inner');
        $this->db->join('tbl_categories category', 'prodCat.category_id = category.cat_id', 'inner');
        $this->db->join('tbl_categories subCat', 'prodCat.sub_category_id = subCat.cat_id', 'left');
        $this->db->join('tbl_categories subSubCat', 'prodCat.sub_sub_category_id = subSubCat.cat_id', 'left');
        $this->db->join('tbl_user users', 'users.user_id = prod.prod_user_id', 'inner');
        $this->db->join('tbl_stores store', 'store.user_id = users.user_id', 'inner');
        $this->db->join('tbl_product_images prod_images', 'prod_images.img_prod_id = prod.prod_id', 'inner');
        $this->db->join('tbl_product_filters pf', 'pf.prod_id = prod.prod_id', 'left');
        $this->db->join('tbl_product_filters_detail pd', 'pd.prod_id = prod.prod_id', 'left');
        $this->db->where('prod_is_delete', '0');
        $this->db->where('prod.prod_status', '1');
        $this->db->where('store.store_is_hide', '0');
        $this->db->where('prod.qty >', '0');

        if ($rangeOne != '' && $rangeTwo != '') {
            $this->db->where("prod.prod_price BETWEEN " . $rangeOne . " AND " . $rangeTwo);
        }
        if (@$this->input->get('location') != '' && $this->input->get('radius') != "") {
            $location = $this->location();
            if ($location != '') {
                $this->db->where("$location");
            }
        }
        if ($singlefilter) {
            $this->db->where("$singlefilter");
        }
        if ($multiple) {
            $this->db->where("$multiple");
        }
        $this->db->where('prod.prod_cat_id', $category_id);
        $this->db->group_by('prod.prod_id');
        if ($sort_val == 2) {
            //$this->db->where('prod.prod_feature', '1');
            $this->db->order_by("prod.prod_feature", 'DESC');
        } elseif ($sort_val == 3) {
            $this->db->order_by("(CASE
                        WHEN prod.prod_onsale = 1 THEN sale_price
                        ELSE prod.prod_price
                        END) ", 'ASC');
        } elseif ($sort_val == 4) {
            $this->db->order_by("(CASE
                        WHEN prod.prod_onsale = 1 THEN sale_price
                        ELSE prod.prod_price
                        END) ", 'DESC');
        } else {
            $this->db->order_by("prod.prod_id", 'DESC');
        }

       // $result->result_array();
        
//        echo '<pre>';
//        echo $this->db->get_compiled_select();
//        exit;
        $result = $this->db->get();

        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function location($page_data = '') {
        $storeurl = "";
        if (@$this->input->get('location') != '' && $this->input->get('radius') != "") {

            $location = explode("|", $this->input->get('location'));
            $radius = $this->input->get('radius');

            $lat = $location[0];
            $lng = $location[1];

            $stores = array();


            $storeflag = true;

            //$data['store'] = $this->common_model->query("SELECT s.* FROM `tbl_stores` s JOIN `tbl_user` u ON s.user_id=u.user_id WHERE s.store_is_hide = 0 AND u.user_status = 1 AND user_is_delete = 0")->result();
            $query = "
                    SELECT *
                    FROM tbl_stores store
                    INNER JOIN tbl_user user ON store.user_id = user.user_id
                    WHERE
                    store_is_hide = 0 AND user.user_is_delete = 0 AND user.user_status = 1
                    ";
            $data['store'] = $this->db->query($query)->result();

            // echo '<pre>';
            // print_r($data['store']);

            foreach ($data['store'] as $store) {

                if ($store->store_longitude != '' && $store->store_latitude != '') {
                    if (distance($lat, $lng, $store->store_latitude, $store->store_longitude, "M") <= $radius) {
                        if ($storeflag) {
                            $storeurl = $storeurl . "`prod_store_id` = " . $store->store_id;
                            array_push($stores, $store);
                            $storeflag = false;
                        } else {
                            $storeurl = $storeurl . " OR `prod_store_id` = " . $store->store_id;
                        }
                    }
                }
            }
        }

        if($storeurl != ''){
            return "($storeurl)";
        }else {
            return "(prod_store_id = 0)";
        }
    }

    public function getProduct1lvlRequiredFilter($prod_id) {
        $result_array = array();
        $query = "SELECT 
                pf.*, pf.is_unique as uniqueFilter, cft.*
                FROM
                tbl_product_filters pf 
                INNER JOIN tbl_cat_filter_title cft ON cft.filter_title_id = pf.filter_id 
                WHERE pf.prod_id = " . $prod_id . " 
                AND cft.is_required = 1";

                //AND pf.is_unique = 0    
        $result = $this->db->query($query);
        // echo $this->db->last_query();
        // exit;
        if ($result->num_rows() > 0) {
            foreach ($result->result_array() as $key => $label) {
                $result_array[$key]['id'] = $label['id'];
                $result_array[$key]['prod_id'] = $label['prod_id'];
                $result_array[$key]['filter_id'] = $label['filter_id'];
                $result_array[$key]['filter_slug'] = $label['filter_slug'];
                $result_array[$key]['filter_value'] = $label['filter_value'];
                $result_array[$key]['category_id'] = $label['category_id'];
                $result_array[$key]['is_unique'] = $label['is_unique'];
                $result_array[$key]['filter_title_id'] = $label['filter_title_id'];
                $result_array[$key]['filter_cat_id'] = $label['filter_cat_id'];
                $result_array[$key]['filter_title'] = $label['filter_title'];
                $result_array[$key]['cat_filter_values'] = $label['cat_filter_values'];
                $result_array[$key]['cat_filter_is_conditional'] = $label['cat_filter_is_conditional'];
                $result_array[$key]['is_required'] = $label['is_required'];
                $result_array[$key]['uniqueFilter'] = $label['uniqueFilter'];

            }
            
            return $result_array;
        } else {
            return false;
        }
    }


    public function getProduct1lvlAddFilter($prod_id) {
        $result_array = array();
        $query = "SELECT 
                * 
                FROM
                tbl_product_filters pf 
                INNER JOIN tbl_cat_filter_title cft 
                ON cft.filter_title_id = pf.filter_id 
                WHERE pf.prod_id = " . $prod_id . " 
                AND cft.is_required = 0";
        
        $result = $this->db->query($query);
        // echo $this->db->last_query();
        // exit;
        if ($result->num_rows() > 0) {
            foreach ($result->result_array() as $key => $label) {
                $result_array[$key]['id'] = $label['id'];
                $result_array[$key]['prod_id'] = $label['prod_id'];
                $result_array[$key]['filter_id'] = $label['filter_id'];
                $result_array[$key]['filter_slug'] = $label['filter_slug'];
                $result_array[$key]['filter_value'] = $label['filter_value'];
                $result_array[$key]['category_id'] = $label['category_id'];
                $result_array[$key]['is_unique'] = $label['is_unique'];
                $result_array[$key]['filter_title_id'] = $label['filter_title_id'];
                $result_array[$key]['filter_cat_id'] = $label['filter_cat_id'];
                $result_array[$key]['filter_title'] = $label['filter_title'];
                $result_array[$key]['cat_filter_values'] = $label['cat_filter_values'];
                $result_array[$key]['cat_filter_is_conditional'] = $label['cat_filter_is_conditional'];
                $result_array[$key]['is_required'] = $label['is_required'];
            }
            
            return $result_array;
        } else {
            return false;
        }
    }

    

}

?>