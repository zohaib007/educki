<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Guidetoeducki_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    function view_list($listing_id) {
        $query = "SELECT 
                  listing.listing_title,
                  listing.listing_url,
                  listing.lisintg_image,listing.lisintg_image2,listing.listing_id 
                  FROM
                  tbl_pages_sub pages_sub 
                  INNER JOIN tbl_pages page 
                  ON page.pg_id = pages_sub.sub_pg_id 
                  INNER JOIN tbl_pages_listing listing 
                  ON listing.pg_listing_id = pages_sub.sub_id 
                  WHERE listing.pg_listing_id =" . $listing_id . "
                  AND listing.listing_status = 1 ORDER BY listing_id desc";
        $results = $this->db->query($query);
        return $results->result_array();
    }

    function listing_pages($url) {
        
        $query = "SELECT 
                    * 
                  FROM
                  tbl_pages_listing listing 
                  INNER JOIN tbl_pages_sub_listing sub_listing 
                  ON sub_listing.list_listing_id = listing.listing_id 
                  WHERE listing.listing_url ='" . $url ."'
                  AND sub_listing.listing_status = 1";
        
        $results = $this->db->query($query);
        return $results->result_array();
    }
}

?>