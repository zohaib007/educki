<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Address_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function add_address() {
//echo "<pre>";print_r($this->input->post());die;
        $data_save['add_user_id'] = $this->input->post('user_id');
        $data_save['nick_name'] = $this->input->post('nick_name');
        $data_save['first_name'] = $this->input->post('first_name');
        $data_save['last_name'] = $this->input->post('last_name');
        $data_save['zip'] = $this->input->post('zip_code');
        $data_save['street'] = $this->input->post('street');
        $data_save['city'] = $this->input->post('city');
        $data_save['created_date'] = date('Y-m-d h:i:s');
        $data_save['country'] = $this->input->post('country');
        $data_save['state'] = ($this->input->post('country') == "US" ? $this->input->post('state') : "");
        $data_save['state_other'] = ($this->input->post('country') != "US" ? $this->input->post('other_state') : "");
        $data_save['phone'] = $this->input->post('phone');
        $billing = $this->input->post('default_billing');
        $shipping = $this->input->post('default_shipping');
        $data_save['default_billing_check'] = 'N';
        $data_save['default_shipping_check'] = 'N';

        if (!empty($billing)) {
            $data_save['default_billing_check'] = 'Y';
            $default_billing['default_billing_check'] = 'N';
            $this->common_model->commonUpdate('tbl_user_addreses', $default_billing, "add_user_id", $data_save['add_user_id']);
        }
        if (!empty($shipping)) {
            $data_save['default_shipping_check'] = 'Y';
            $default_shipping['default_shipping_check'] = 'N';
            $this->common_model->commonUpdate('tbl_user_addreses', $default_shipping, "add_user_id", $data_save['add_user_id']);
        }

        $inserted_id = $this->common_model->commonSave('tbl_user_addreses', $data_save);
        if ($inserted_id != null) {
            return '1';
        } else {
            return '0';
        }
    }

    public function cart_add_address() {
        $data_save['add_user_id'] = $this->input->post('user_id');
        $data_save['nick_name'] = $this->input->post('nick_name');
        $data_save['first_name'] = $this->input->post('first_name');
        $data_save['last_name'] = $this->input->post('last_name');
        $data_save['zip'] = $this->input->post('zip_code');
        $data_save['street'] = $this->input->post('street');
        $data_save['city'] = $this->input->post('city');
        $data_save['created_date'] = date('Y-m-d h:i:s');
        $data_save['country'] = $this->input->post('country');
        $data_save['state'] = ($this->input->post('country') == "US" ? $this->input->post('state') : "");
        $data_save['state_other'] = ($this->input->post('country') != "US" ? $this->input->post('other_state') : "");
        $data_save['phone'] = $this->input->post('phone');
        $data_save['default_billing_check'] = 'N';
        $data_save['default_shipping_check'] = 'N';

        $inserted_id = $this->common_model->commonSave('tbl_user_addreses', $data_save);
        if ($inserted_id != null) {
            return '1';
        } else {
            return '0';
        }
    }

    public function edit_address() {
        $add_id = $this->input->post('add_id');
        $data_save['add_user_id'] = $this->input->post('user_id');
        $data_save['nick_name'] = $this->input->post('nick_name');
        $data_save['first_name'] = $this->input->post('first_name');
        $data_save['last_name'] = $this->input->post('last_name');
        $data_save['zip'] = $this->input->post('zip_code');
        $data_save['street'] = $this->input->post('street');
        $data_save['city'] = $this->input->post('city');
        $data_save['country'] = $this->input->post('country');
        $data_save['state'] = ($this->input->post('country') == "US" ? $this->input->post('state') : "");
        $data_save['state_other'] = ($this->input->post('country') != "US" ? $this->input->post('other_state') : "");
        $data_save['phone'] = $this->input->post('phone');

        $billing = $this->input->post('default_billing');
        $shipping = $this->input->post('default_shipping');
        $data_save['default_billing_check'] = 'N';
        $data_save['default_shipping_check'] = 'N';

        if (!empty($billing)) {
            $data_save['default_billing_check'] = 'Y';
            $default_billing['default_billing_check'] = 'N';
            $this->common_model->commonUpdate('tbl_user_addreses', $default_billing, "add_user_id", $data_save['add_user_id']);
        }
        if (!empty($shipping)) {
            $data_save['default_shipping_check'] = 'Y';
            $default_shipping['default_shipping_check'] = 'N';
            $this->common_model->commonUpdate('tbl_user_addreses', $default_shipping, "add_user_id", $data_save['add_user_id']);
        }

        /* echo "<pre>";
          echo "Billing check : ".$this->input->post('default_billing')."<br>";
          echo "shippinging check : ".$this->input->post('default_shipping')."<br>";
          print_r($data_save);
          exit; */

        $response = $this->common_model->commonUpdate('tbl_user_addreses', $data_save, 'add_id', $add_id);
        if ($response != null) {
            return '1';
        } else {
            return '0';
        }
    }

    public function address_list($user_id) {
        $query = "SELECT
                    country.nicename,
                    address.*
                    FROM
                      tbl_user_addreses address
                      INNER JOIN tbl_country country
                        ON country.iso = address.country
                    WHERE add_user_id = " . $user_id . " AND
                        default_billing_check != 'Y'
                        AND default_shipping_check != 'Y'
                      ";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return false;
        }
    }

    public function getuseraddress($user_id) {
        $query = "SELECT
                    *
                    FROM
                    tbl_user_addreses address
                    INNER JOIN tbl_country country
                    ON country.iso = address.country
                    WHERE add_user_id = " . $user_id . "";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return false;
        }
    }

    public function getuseraddress_2($user_id,$type){
        $where = '';
        if($type==1){
            $where = "AND default_billing_check != 'Y'";
        }elseif($type==2){
            $where = "AND default_shipping_check != 'Y'";
        }else{
            $where = "";
        }
        $query = "SELECT
                    *
                    FROM
                    tbl_user_addreses address
                    INNER JOIN tbl_country country
                    ON country.iso = address.country
                    WHERE add_user_id = ".$user_id." ".$where."";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return false;
        }
    }

    public function cart_address_json() {
        $sessiondata = array(            
            ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_add_user_id' => true,
        );
        $this->session->set_userdata($sessiondata);
        /*echo "<pre>";
        echo "Test 1";
        print_r($this->session->userdata('shipping_address_info'));
        print_r($address['default_shipping_address']);
        print_r($this->session->userdata());
        die;*/
        if ($this->session->userdata('same_as_shipping') == TRUE) {
            $address['default_shipping_address'] = json_decode($this->session->userdata('shipping_address_info'));            
            $data = array(
                "billing_add_user_id" => $address['default_shipping_address']->shipping_add_user_id,
                "billing_nick_name" => $address['default_shipping_address']->shipping_nick_name,
                "billing_first_name" => $address['default_shipping_address']->shipping_first_name,
                "billing_last_name" => $address['default_shipping_address']->shipping_last_name,
                "billing_zip" => $address['default_shipping_address']->shipping_zip,
                "billing_street" => $address['default_shipping_address']->shipping_street,
                "billing_city" => $address['default_shipping_address']->shipping_city,
                "billing_country" => $address['default_shipping_address']->shipping_country,
                "billing_state" => $address['default_shipping_address']->shipping_state,
                "billing_state_other" => $address['default_shipping_address']->shipping_state_other,
                "billing_phone" => $address['default_shipping_address']->shipping_phone,
            );
        } elseif ($this->input->post('us') == '1') {
            $address['default_shipping_address'] = $this->common_model->default_billing($this->session->userdata('userid'));            
            $data = array(
                "billing_add_user_id" => $address['default_shipping_address']->add_user_id,
                "billing_nick_name" => $address['default_shipping_address']->nick_name,
                "billing_first_name" => $address['default_shipping_address']->first_name,
                "billing_last_name" => $address['default_shipping_address']->last_name,
                "billing_zip" => $address['default_shipping_address']->zip,
                "billing_street" => $address['default_shipping_address']->street,
                "billing_city" => $address['default_shipping_address']->city,
                "billing_country" => $address['default_shipping_address']->country,
                "billing_state" => ($address['default_shipping_address']->country == "US" ? $address['default_shipping_address']->state : ""),
                "billing_state_other" => ($address['default_shipping_address']->country != "US" ? $address['default_shipping_address']->state_other : ""),
                "billing_phone" => $address['default_shipping_address']->phone,
            );
            /*echo "<pre>";
            print_r($data);
            exit;*/
        } else {
            /*echo "<pre>";echo "Test 2";print_r($address['default_shipping_address']);die;*/
            $data = array(
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_add_user_id' => $this->input->post('user_id'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_nick_name' => $this->input->post('nick_name'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_first_name' => $this->input->post('first_name'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_last_name' => $this->input->post('last_name'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_zip' => $this->input->post('zip_code'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_street' => $this->input->post('street'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_city' => $this->input->post('city'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_country' => $this->input->post('country'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_state' => ($this->input->post('country') == "US" ? $this->input->post('state') : ""),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_state_other' => ($this->input->post('country') != "US" ? $this->input->post('other_state') : ""),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_phone' => $this->input->post('phone'),
            );
        }
        return json_encode($data);
    }

}
