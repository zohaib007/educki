<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function view_order_details($OrderId, $store_id) {
        $this->db->select('tbl_order_products.*, tbl_order_seller_info.*');
        $this->db->from('tbl_order_products');
        $this->db->join('tbl_orders', 'tbl_order_products.order_id = tbl_orders.order_id');
        $this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');
        $this->db->where('tbl_orders.order_id', $OrderId);
        $this->db->where('tbl_order_products.order_prod_store_id', $store_id);
        //$this->db->where('tbl_order_products.order_prod_shipping !=', 'Local Pick Up');
//        $this->db->where('tbl_stores.store_is_hide', '0');
//        $this->db->group_by('tbl_order_products.order_id');

        $this->db->group_by('tbl_order_products.order_prod_id');
        $this->db->group_by('tbl_order_products.order_prod_filters');

        $this->db->order_by('tbl_order_products.order_id', 'DESC');
        $result = $this->db->get();
//                echo $this->db->last_query();die;
//        var_dump($result->num_rows());die;
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function view_order_details_print($OrderId, $store_id) {
        $this->db->select('tbl_order_products.*, tbl_order_seller_info.*');
        $this->db->from('tbl_order_products');
        $this->db->join('tbl_orders', 'tbl_order_products.order_id = tbl_orders.order_id');
        $this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');
        $this->db->where('tbl_orders.order_id', $OrderId);
        $this->db->where('tbl_order_products.order_prod_store_id', $store_id);
//        $this->db->where('tbl_stores.store_is_hide', '0');
//        $this->db->group_by('tbl_order_products.order_id');

        $this->db->group_by('tbl_order_products.order_prod_id');
        $this->db->group_by('tbl_order_products.order_prod_filters');

        $this->db->order_by('tbl_order_products.order_id', 'DESC');
        $result = $this->db->get();
//                echo $this->db->last_query();die;
//        var_dump($result->num_rows());die;
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function view_order($OrderId, $store_id) {
        $this->db->select('tbl_orders.*, tbl_order_seller_info.*');
        $this->db->from('tbl_order_products');
        $this->db->join('tbl_orders', 'tbl_order_products.order_id = tbl_orders.order_id');
        $this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');
        $this->db->where('tbl_orders.order_id', $OrderId);
        $this->db->where('tbl_order_products.order_prod_store_id', $store_id);
//        $this->db->where('tbl_stores.store_is_hide', '0');
        $result = $this->db->get();
//                echo $this->db->last_query();die;
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
    }

    public function get_user_orders($store_id, $number, $order_status, $limit = '', $start = ''){
        $this->db->select('tbl_order_products.*,tbl_orders.*,tbl_order_seller_info.*,TIMEDIFF(CURRENT_TIMESTAMP,tbl_orders.order_date) AS time_diff,DATEDIFF(CURDATE(),tbl_orders.order_date) AS DaysDiff');
        $this->db->from('tbl_order_products');
        $this->db->join('tbl_orders', 'tbl_order_products.order_id = tbl_orders.order_id');
        $this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');
        $this->db->where('tbl_order_products.order_prod_store_id', $store_id);
        if ($number == 1) {
            $this->db->where('tbl_order_products.order_prod_tracking_number', NULL);
            $this->db->where_in('tbl_order_products.order_prod_status', $order_status);
            $this->db->where('tbl_order_products.order_prod_shipping !=', "Local Pick Up");
        } else if ($number == 2) {
            $this->db->where('tbl_order_products.order_prod_tracking_number !=', NULL);
            $this->db->where_in('tbl_order_products.order_prod_status', $order_status);
        } else if ($number == 3) {
            $this->db->where('tbl_order_products.order_prod_tracking_number', NULL);
            $this->db->where('tbl_order_products.order_prod_status', $order_status);
            $this->db->where('tbl_order_products.order_prod_shipping', "Local Pick Up");
        } else {
            /*$this->db->where('tbl_order_products.order_prod_tracking_number !=', NULL);*/
            $this->db->where_in('tbl_order_products.order_prod_status', $order_status);
        }
        $this->db->group_by('tbl_order_products.order_id');
        $this->db->order_by('tbl_order_products.order_id', 'DESC');
        if($limit != ''){
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        //echo $this->db->last_query();
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function get_user_orders_app($store_id, $number, $order_status, $limit = '', $start = ''){
        $text = $this->input->get('utext');
        $type = $this->input->get('utype');
        $this->db->select('tbl_order_products.*,tbl_orders.*,tbl_order_seller_info.*,TIMEDIFF(CURRENT_TIMESTAMP,tbl_orders.order_date) AS time_diff,DATEDIFF(CURDATE(),tbl_orders.order_date) AS DaysDiff');
        $this->db->from('tbl_order_products');
        $this->db->join('tbl_orders', 'tbl_order_products.order_id = tbl_orders.order_id');
        $this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');
        $this->db->where('tbl_order_products.order_prod_store_id', $store_id);
        if ($number == 1) {
            $this->db->where('tbl_order_products.order_prod_tracking_number', NULL);
            $this->db->where_in('tbl_order_products.order_prod_status', $order_status);
            $this->db->where('tbl_order_products.order_prod_shipping !=', "Local Pick Up");
        } else if ($number == 2) {
            $this->db->where('tbl_order_products.order_prod_tracking_number !=', NULL);
            $this->db->where_in('tbl_order_products.order_prod_status', $order_status);
        } else if ($number == 3) {
            $this->db->where('tbl_order_products.order_prod_tracking_number', NULL);
            $this->db->where('tbl_order_products.order_prod_status', $order_status);
            $this->db->where('tbl_order_products.order_prod_shipping', "Local Pick Up");
        } else {
            /*$this->db->where('tbl_order_products.order_prod_tracking_number !=', NULL);*/
            $this->db->where_in('tbl_order_products.order_prod_status', $order_status);
        }
        if($text!=''){
            if($type==1){
                $this->db->where('tbl_orders.order_id',$text);
            }elseif($type==2){
                $this->db->like('tbl_order_products.order_prod_name',$text);
            }
        }
        $this->db->group_by('tbl_order_products.order_id');
        $this->db->order_by('tbl_order_products.order_id', 'DESC');
        if($limit != ''){
            $this->db->limit($limit, $start);
        }
        $result = $this->db->get();
        //echo $this->db->last_query();
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function get_my_orders_app($buyer_id, $limit='', $start='') {
    	$text = $this->input->get('utext');
        $type = $this->input->get('utype');
        $this->db->select('*');
        $this->db->from('tbl_orders');
        $this->db->join('tbl_order_products', 'tbl_order_products.order_id = tbl_orders.order_id');
        //$this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');
        $this->db->where('tbl_orders.buyer_id', $buyer_id);
        if($text!=''){
            if($type==1){
                $this->db->where('tbl_orders.order_id',$text);
            }elseif($type==2){
                $this->db->like('tbl_order_products.order_prod_name',$text);
            }
        }
        //$this->db->group_by('tbl_order_products.order_id');
        $this->db->order_by('tbl_orders.order_id', 'DESC');

        if($limit != ''){
            $this->db->limit($limit, $start);
        }
        
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function view_user_order($store_id, $number, $order_status) {
        $this->db->select('tbl_orders.*');
        $this->db->from('tbl_order_products');
        $this->db->join('tbl_orders', 'tbl_order_products.order_id = tbl_orders.order_id');
        $this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');
        $this->db->where('tbl_order_products.order_prod_store_id', $store_id);
        if ($number == 1) {
            $this->db->where('tbl_order_products.order_prod_tracking_number', NULL);
            $this->db->where_in('tbl_order_products.order_prod_status', $order_status);
            $this->db->where('tbl_order_products.order_prod_shipping !=', "Local Pick Up");
        } else if ($number == 2) {
            $this->db->where('tbl_order_products.order_prod_tracking_number !=', NULL);
            $this->db->where_in('tbl_order_products.order_prod_status', $order_status);
        } else if ($number == 3) {
            $this->db->where('tbl_order_products.order_prod_tracking_number', NULL);
            $this->db->where('tbl_order_products.order_prod_status', $order_status);
            $this->db->where('tbl_order_products.order_prod_shipping', "Local Pick Up");
        } else {
            $this->db->where('tbl_order_products.order_prod_tracking_number !=', NULL);
            $this->db->where_in('tbl_order_products.order_prod_status', $order_status);
        }
//        $this->db->group_by('tbl_order_products.order_id');
        $this->db->group_by('tbl_order_products.order_id');
        $this->db->order_by('tbl_order_products.order_id', 'DESC');
        $result = $this->db->get();
//        echo $this->db->last_query();die;
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function search_order($store_id, $limit, $start, $is_limit = 0, $step, $order_status, $search = '', $select_value='') {

        if($search == ''){
            $search = $this->input->post('search_value');
        }

        if($select_value == ''){
            $select_value = $this->input->post('select_search');
        }

        $this->db->select('*');
        $this->db->from('tbl_order_products');
        $this->db->join('tbl_orders', 'tbl_order_products.order_id = tbl_orders.order_id');
        $this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');

        if ($select_value == '1') {
            $this->db->where('tbl_orders.order_id', $search);
        }elseif ($select_value == '4') {
            $this->db->like('tbl_order_products.order_prod_name', $search);
//            $this->db->like('LOWER(tbl_order_products.order_prod_name)', strtolower($this->input->post('search_value')));
        }
        $this->db->where('tbl_order_products.order_prod_store_id', $store_id);
        if ($step == 1) {
            $this->db->where('tbl_order_products.order_prod_tracking_number', NULL);
            $this->db->where('tbl_order_products.order_prod_status', $order_status);
        } else if ($step == 2) {
            $this->db->where('tbl_order_products.order_prod_tracking_number !=', NULL);
            $this->db->where_in('tbl_order_products.order_prod_status', $order_status);
        } else if ($step == 3) {
            $this->db->where('tbl_order_products.order_prod_tracking_number', NULL);
            // removed $this->db->where('tbl_order_products.order_prod_status', $order_status);
            $this->db->where('tbl_order_products.order_prod_shipping', "Local Pick Up");
        }else {
            /*$this->db->where('tbl_order_products.order_prod_tracking_number !=', NULL);*/
            $this->db->where_in('tbl_order_products.order_prod_status', $order_status);
        }
        
        // if ($is_limit == 1) {
        //     $this->db->limit($limit, $start);
        // }
        if ($is_limit == 1) {
            if($start == 0){
                $this->db->limit( $limit,$start);
            }else{
                $this->db->limit( $limit,$start);
            }
        }
        
        $this->db->group_by('tbl_order_products.order_id');
        $result = $this->db->get();

        /*echo "<pre>";
        print_r($this->db->last_query());
        exit;*/
        
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function update_tracking() {
        $order_id = $this->input->post('order_id');
        $data['userid'] = $this->session->userdata('userid');
        $data['store_info'] = $this->common_model->commonselect('tbl_stores', 'user_id', $data['userid'])->row();
        $store_id = $data['store_info']->store_id;
        $data['store_id'] = $data['store_info']->store_id;
        $this->db->select('*');
        $this->db->from('tbl_order_products');
        $this->db->join('tbl_orders', 'tbl_order_products.order_id = tbl_orders.order_id');
        $this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');
        $this->db->where('tbl_orders.order_id', $order_id);
        $this->db->where('tbl_order_products.order_prod_store_id', $store_id);
        $result = $this->db->get();
        /*echo "<pre>";
        print_r($result->result());
        exit;*/
        $buyer_email = '';
        $buyer_name = '';
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
                $buyer_email = $row->buyer_email;
                $buyer_name = $row->buyer_name;
                if($row->order_prod_shipping!='Local Pick Up'){
                    $update_data = array(
                        'order_prod_tracking_number' => $this->input->post('tracking_number'),
                        'order_prod_status' => 'In Process',
                        'order_prod_shipping_date' => date('Y-m-d h:i:s'),
                    );
                    $this->common_model->commonUpdate2('tbl_order_products', $update_data, 'order_id', $order_id, 'order_prod_id', $row->order_prod_id);
                }                
            }
        }
        $message = "<p>Dear ".$buyer_name.",</p><br>";
        $message .= "<p>We’ve updated your order ".$order_id." to: In Process</p><br>";
        $message .= "<p>Your order status is: In Process</p><br>";
        $is_send = send_email_2($buyer_email, 'noreply@educki.com', 'Order Status Update',$message);
        return 0;
    }

    public function update_tracking_app($order_id,$user_id,$tracking_number){
        $data['userid'] = $user_id;
        $data['store_info'] = $this->common_model->commonselect('tbl_stores', 'user_id', $data['userid'])->row();
        $store_id = $data['store_info']->store_id;
        $data['store_id'] = $data['store_info']->store_id;
        $this->db->select('*');
        $this->db->from('tbl_order_products');
        $this->db->join('tbl_orders', 'tbl_order_products.order_id = tbl_orders.order_id');
        $this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');
        $this->db->where('tbl_orders.order_id', $order_id);
        $this->db->where('tbl_order_products.order_prod_store_id', $store_id);
        $result = $this->db->get();
        /*echo "<pre>";
        print_r($result->result());
        exit;*/
        $buyer_email = '';
        $buyer_name = '';
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
                $buyer_email = $row->buyer_email;
                $buyer_name = $row->buyer_name;
                if($row->order_prod_shipping!='Local Pick Up'){
                    $update_data = array(
                        'order_prod_tracking_number' => $tracking_number,
                        'order_prod_status' => 'In Process'
                    );
                    $this->common_model->commonUpdate2('tbl_order_products', $update_data, 'order_id', $order_id, 'order_prod_id', $row->order_prod_id);
                }                
            }
        }
        $message = "<p>Dear ".$buyer_name.",</p><br>";
        $message .= "<p>We’ve updated your order ".$order_id." to: In Process</p><br>";
        $message .= "<p>Your order status is: In Process</p><br>";
        $is_send = send_email_2($buyer_email, 'noreply@educki.com', 'Order Status Update',$message);
        return 0;
    }

    public function get_my_orders($buyer_id, $limit, $start) {
        $this->db->select('*');
        $this->db->from('tbl_orders');
        $this->db->join('tbl_order_products', 'tbl_order_products.order_id = tbl_orders.order_id','inner');
        //$this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');
        $this->db->where('tbl_orders.buyer_id', $buyer_id);
        $this->db->order_by('tbl_orders.order_id', 'DESC');
        $this->db->limit($limit, $start);
        $result = $this->db->get();
        /*echo "<pre>";
        print_r($result->result());
        exit;*/
        //echo $this->db->last_query(); die;
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function view_my_order($buyer_id) {
        $this->db->select('*');
        $this->db->from('tbl_orders');
        $this->db->join('tbl_order_products', 'tbl_order_products.order_id = tbl_orders.order_id');
        $this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');
        $this->db->where('tbl_orders.buyer_id', $buyer_id);
        //$this->db->group_by('tbl_order_products.order_id');
        $this->db->order_by('tbl_orders.order_id', 'DESC');
        $result = $this->db->get();
//        echo $this->db->last_query();die;
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function search_my_order($buyer_id, $limit, $start, $is_limit = 0, $search = '', $select_value='' ) {
        //$select_value = $this->input->post('select_search');
//        $user_id = $this->session->userdata('userid');

        if($search == ''){
            $search = $this->input->post('search_value');
        }

        if($select_value == ''){
            $select_value = $this->input->post('select_search');
        }


        $this->db->select('*');
        $this->db->from('tbl_orders');
        $this->db->join('tbl_order_products', 'tbl_order_products.order_id = tbl_orders.order_id');
        //$this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');
//        if ($select_value == '1') {
//            $this->db->like('order_id', $this->input->post('search_value'));
//        } elseif ($select_value == '2') {
//            $this->db->like('traking_number', $this->input->post('search_value'));
//        }
        /*if ($select_value == '3') {
            $this->db->like('tbl_order_products.order_prod_id', $this->input->post('search_value'));
        }*/
        if ($select_value == '1') {
            $this->db->like('tbl_orders.order_id', $search);
        } elseif ($select_value == '4') {
            $this->db->like('tbl_order_products.order_prod_name', $search);
//            $this->db->like('LOWER(tbl_order_products.order_prod_name)', strtolower($this->input->post('search_value')));
        }
        $this->db->where('buyer_id', $buyer_id);
        //$this->db->group_by('tbl_order_products.order_id');
        $this->db->order_by('tbl_orders.order_id', 'DESC');
        
        // if ($is_limit == 1) {
        //     $this->db->limit($limit, $start);
        // }

        if ($is_limit == 1) {
            if($start == 0){
                $this->db->limit( $limit,$start);
            }else{
                $this->db->limit( $limit,$start);
            }
        }
        $result = $this->db->get();
//      echo $this->db->last_query();die;
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function confirm_order() {
        $order_id = $this->input->post('order_id');
        $prod_id = $this->input->post('Prod_id');
        $data['userid'] = $this->session->userdata('userid');
        $data['store_info'] = $this->common_model->commonselect('tbl_stores', 'user_id', $data['userid'])->row();
        $data['prod_info'] = $this->common_model->commonselect2('tbl_order_products', 'order_prod_id', $prod_id,'order_id',$order_id)->row();
        $store_id = $data['store_info']->store_id;
        $data['store_id'] = $data['store_info']->store_id;
        $this->db->select('*');
        $this->db->from('tbl_order_products');
        $this->db->join('tbl_orders', 'tbl_order_products.order_id = tbl_orders.order_id');
        $this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');
        $this->db->where('tbl_orders.order_id', $order_id);
        $this->db->where('tbl_order_products.order_prod_store_id', $store_id);
        if($data['prod_info']->order_prod_shipping!='Local Pick Up'){
            $this->db->where('tbl_order_products.order_prod_shipping !=', 'Local Pick Up');
        }else{
            $this->db->where('tbl_order_products.order_prod_shipping', 'Local Pick Up');
        }
        /*$this->db->where('tbl_order_products.order_prod_id', $prod_id);*/
        $result = $this->db->get();
        //echo "<pre>";echo$this->db->last_query();print_r($result->result());die;
        if ($result->num_rows() > 0) {
            $response = $result->row();
        }
        $update_data = array(
            'order_prod_status' => 'completed',
            'order_prod_completion_date' => date('Y-m-d h:i:s'),
        );

        if($data['prod_info']->order_prod_shipping!='Local Pick Up'){
            $response = $this->common_model->commonUpdate3('tbl_order_products', $update_data, 'order_id', $order_id, 'order_prod_store_id', $store_id,'order_prod_shipping !=','Local Pick Up');
        }else{
            $response = $this->common_model->commonUpdate3('tbl_order_products', $update_data, 'order_id', $order_id, 'order_prod_store_id', $store_id,'order_prod_shipping','Local Pick Up');
        }

        if ($response == TRUE) {
            return 0;
        } else {
            return 1;
        }
    }

    public function view_order_pickup_details($OrderId, $store_id) {
        $this->db->select('*');
        $this->db->from('tbl_order_products');
        $this->db->join('tbl_orders', 'tbl_order_products.order_id = tbl_orders.order_id');
        $this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');
        $this->db->where('tbl_orders.order_id', $OrderId);
        $this->db->where('tbl_order_products.order_prod_store_id', $store_id);
        $this->db->where('tbl_order_products.order_prod_shipping', 'Local Pick Up');
        $this->db->where('tbl_order_products.order_prod_status !=', 'completed');
//        $this->db->where('tbl_stores.store_is_hide', '0');
//        $this->db->group_by('tbl_order_products.order_id');
        $this->db->order_by('tbl_order_products.order_id', 'DESC');
        $this->db->group_by('tbl_order_products.order_products_id');
      
        
        $result = $this->db->get();
//                echo $this->db->last_query();die;
//        var_dump($result->num_rows());die;
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function view_order_history_details($OrderId, $store_id) {
        $this->db->select('*');
        $this->db->from('tbl_order_products');
        $this->db->join('tbl_orders', 'tbl_order_products.order_id = tbl_orders.order_id');
        $this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');
        $this->db->where('tbl_orders.order_id', $OrderId);
        $this->db->where('tbl_order_products.order_prod_store_id', $store_id);
        $this->db->where('tbl_order_products.order_prod_status', 'completed');
        $this->db->group_by('tbl_order_products.order_products_id');
        $this->db->order_by('tbl_order_products.order_id', 'DESC');
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            return $result->result_array();
        } else {
            return false;
        }
    }

    public function cancel_order($order_id) {
        $this->db->select('*');
        $this->db->from('tbl_order_products');
        $this->db->join('tbl_orders', 'tbl_order_products.order_id = tbl_orders.order_id');
        $this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');
        $this->db->where('tbl_orders.order_id', $order_id);
        $result = $this->db->get();
        if ($result->num_rows() > 0) {
            foreach ($result->result_array() as $prod) {
                $data_save['order_prod_status'] = 'cancel';
                $this->common_model->commonUpdate('tbl_order_products', $data_save, 'order_id', $order_id);
            }
            $data_order['order_status'] = 'cancel';
            $this->common_model->commonUpdate('tbl_orders', $data_order, 'order_id', $order_id);
        }
    }
   
    public function get_order($OrderId) {
        $this->db->select('*');
        $this->db->from('tbl_order_products');
        $this->db->join('tbl_orders', 'tbl_order_products.order_id = tbl_orders.order_id');
        $this->db->join('tbl_order_seller_info', 'tbl_order_seller_info.order_id = tbl_orders.order_id');
        $this->db->where('tbl_orders.order_id', $OrderId);
        $this->db->group_by('tbl_order_products.order_products_id');
        $this->db->order_by('tbl_order_products.order_id', 'DESC');
        $result = $this->db->get();
               // echo $this->db->last_query();die;
//        var_dump($result->num_rows());die;
        return $result->result_array();
        if ($result->num_rows() > 0) {
            // echo 121212;
        } else {
            // echo 'sdsdsds';
            return false;
        }
    }

}
