<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    function order_list($date = '') {
        $count= 0;
        if ($date!='') {
            $count++;
            $where = 'WHERE ';
            $expload_date = explode("to", $date);
            @$date1 = $expload_date[0];
            @$date2 = $expload_date[1];
            $where .= "(tbl_orders.order_date between '$date1' and '$date2')";
        }
        if ($this->input->get('srchOp') == 'status') {
            $count++;
            if($count==1){
                $where = 'WHERE ';
            }else{
                $where .= ' AND ';
            }
            $where .= " tbl_orders.order_status LIKE '%" . $this->input->get('searchText') . "%'";
        } elseif ($this->input->get('srchOp') == 'id') {
            $count++;
            if($count==1){
                $where = 'WHERE ';
            }else{
                $where .= ' AND ';
            }
            $where .= " tbl_orders.order_id LIKE '%" . $this->input->get('searchText') . "%'";
        }

        $query="SELECT order_id,buyer_name,order_status,order_date,order_completion_date,order_grand_total
                FROM tbl_orders 
                ".$where."
                ORDER BY order_id DESC
                 "; 
        /*echo "<pre>";
        print_r($this->db->last_query());
        print_r($this->db->query($query)->result());
        exit;*/
        /*$jointable = 'tbl_orders';
        $joincondition = 'order_prod.order_id = tbl_orders.order_id';
        $jointype = 'inner';
        $groupby = 'order_prod.order_id';
        $results = $this->order_listing("tbl_order_products order_prod", $where, $jointable, $joincondition, $jointype, '', '', $is_count = 0,$orderby = 'tbl_orders.order_id', $order = 'DESC', $groupby)->result_array();*/
        return $this->db->query($query)->result_array();
    }

    function order_listing($tabel_name, $where = '', $jointable = '', $joincondition = '', $jointype = '', $page = 0, $per_page = 0, $is_count = 0, $orderby = '', $order = 'DESC', $groupby) {

        $query = "SELECT * 
                  FROM " . $tabel_name . "
                 ";
        if ($jointable != '') {

            $query .= $jointype . " JOIN " . $jointable . " ON " . $joincondition;
        }
        if ($where != '') {

            $query .= " WHERE " . $where;
        }

        if ($groupby != '') {
            $query .= " GROUP BY " . $groupby;
        }

        if ($orderby != '') {
            $query .= " ORDER BY " . $orderby . " " . $order;
        }        
        if ($is_count > 0) {
            $query .= " LIMIT " . (int) $page . ", " . (int) $per_page;
        }

        $result = $this->db->query($query);
        /*echo "<pre>";
        print_r($this->db->last_query());
        exit;*/
        return $result;
    }

    function order_details($order_id) {
        $results = $this->db->query("
                            SELECT * FROM tbl_orders o
                            WHERE o.`order_id` = " . $order_id . "")->row();
        return $results;
    }

    function order_products($order_id) {
        $results = $this->db->query("SELECT * FROM tbl_order_products o WHERE o.`order_id` = " . $order_id . "")->result();
        return $results;
        
    }

    function order_sellers($order_id) {
        $results = $this->db->query("SELECT * FROM tbl_order_seller_info o WHERE o.`order_id` = " . $order_id . "")->result();
        return $results;
        
    }
    
    function getTotalcommission(){
        $results = $this->db->query("SELECT  SUM(orders.order_total_commission) AS educki_commission
                                    FROM tbl_orders orders
                                    WHERE CURDATE() = DATE(orders.order_completion_date)")->row();
        return $results;
    }

}

?>