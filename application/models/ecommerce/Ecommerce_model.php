<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ecommerce_model extends CI_Model {
	
	public function get_ecommerce_prod_report()
	{
		$query = $this->db->query("
									SELECT 
									order_prod_id, order_prod_name, order_prod_sku,
									SUM(order_qty) AS Qty, SUM(order_price * order_qty) AS prod_price
									FROM tbl_order_detail
									WHERE
									order_prod_id > 0
									GROUP BY order_prod_id
								");
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	public function sort_prod_report($where)
	{
		$query = $this->db->query("
									SELECT 
									order_prod_id, order_prod_name, order_prod_sku,
									SUM(order_qty) AS Qty, SUM(order_price * order_qty) AS prod_price
									FROM tbl_order_detail
									INNER JOIN tbl_order ON tbl_order.order_id = tbl_order_detail.order_detail_id
														
									WHERE
									order_prod_id > 0
									AND 
									".$where."
									GROUP BY order_prod_id
								");
							
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
	
	public function top_five_prod()
	{
		$query = $this->db->query("
									SELECT 
									order_prod_id, order_prod_name, order_prod_sku,
									SUM(order_qty) AS Qty, SUM(order_price * order_qty) AS prod_price
									FROM tbl_order_detail
									WHERE
									order_prod_id > 0
									GROUP BY order_prod_id
									ORDER BY Qty DESC 
									LIMIT 5
								");
							
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
	public function get_vendor_user_data($nOrdId)
	{
		$query = $this->db->query("
									SELECT
									tbl_user.*,
									tbl_order.*
									FROM
									tbl_order
									INNER JOIN tbl_user
									ON tbl_user.user_id = tbl_order.order_user_id
									WHERE
									tbl_order.order_id = ".$nOrdId."
								");
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
	
	public function get_vendor_order_details($nOrdId)
	{
		$query = $this->db->query("
									SELECT
									tbl_order.*,
									tbl_orderdetail.*,
									tbl_products.prod_id,
									tbl_products.prod_name,
									tbl_company.com_name,
									tbl_company.com_id
									FROM
									tbl_order
									JOIN tbl_orderdetail
									ON tbl_orderdetail.orderd_order_id = tbl_order.order_id
									JOIN tbl_products
									ON tbl_orderdetail.orderd_prod_id = tbl_products.prod_id
									JOIN tbl_company
									ON tbl_company.com_id = tbl_products.prod_com_id
									WHERE
									tbl_order.order_id = ".$nOrdId."
								");
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
	
	public function get_report_by_product()
	{
		$query = $this->db->query("
									SELECT
									prod_id,
									prod_img1,
									prod_name,
									prod_desc,
									SUM(orderd_qty) AS Qty
									FROM
									`tbl_products`
									INNER JOIN tbl_orderdetail
									ON prod_id = orderd_prod_id
									GROUP BY prod_id, prod_img1, prod_name 
								");
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
		
	}
	
	public function get_product_purchaser_details($nProdId)
	{
		$query = $this->db->query("
									SELECT
									user_fname,
									user_lname,
									user_email,
									user_phone,
									order_id,
									order_datetime
									FROM
									`tbl_order`
									INNER JOIN tbl_user
									ON user_id = order_user_id
									WHERE
									order_id IN (	SELECT
													DISTINCT orderd_order_id
													FROM `tbl_products`
													INNER JOIN tbl_orderdetail
													ON prod_id = orderd_prod_id
													WHERE
													prod_id = ".$nProdId." )
								");
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
	
}