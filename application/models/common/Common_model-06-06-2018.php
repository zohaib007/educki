<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Common_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function create_slug($title, $table_name, $field_name, $nId_name = '', $nId = '', $count = 0)
    {
        //echo "Product Id = ".$nId."<br>";
        $this->load->helper('url');
        $slug = url_title(clean(strtolower(trim($title))), 'dash', true);
        //echo "count = ".$count;
        if ($count == 0) {
            $slug = $slug;
        } else {
            $slug = $slug . '-' . $count;
        }
        //echo '<pre>';
        //echo $slug;

        $query = "
                SELECT " . $field_name . "
                FROM " . $table_name . "
                WHERE
                (" . $field_name . " LIKE '" . $slug . "-%'
                OR
                " . $field_name . " LIKE '" . $slug . "')";

        if ($nId_name != '' && $nId != '') {
            $query .= " AND " . $nId_name . " != " . $nId;
        }

        $query1 = $this->db->query($query)->result_array();
        //echo $this->db->last_query();
        $total_records = count($query1);
        //print_r($query1);

        if ($total_records) {
            //echo $this->create_slug($title, $table_name, $field_name, $nId_name, $nId, ++$count);
            return $this->create_slug($title, $table_name, $field_name, $nId_name, $nId, ++$count);
        } else {
            //echo $slug;
            return $slug;
        }
    }

    public function create_filter_slug($title, $table_name, $field_name, $nId_name = '', $nId = '', $count = 0)
    {
        //echo "Product Id = ".$nId."<br>";
        $this->load->helper('url');
        $slug = url_title(clean(strtolower(trim($title))), 'underscore', true);
        //echo "count = ".$count;
        if ($count == 0) {
            $slug = $slug;
        } else {
            $slug = $slug . '_' . $count;
        }
        //echo '<pre>';
        //echo $slug;

        $query = "
                SELECT " . $field_name . "
                FROM " . $table_name . "
                WHERE
                (" . $field_name . " LIKE '" . $slug . "-%'
                OR
                " . $field_name . " LIKE '" . $slug . "')";

        if ($nId_name != '' && $nId != '') {
            $query .= " AND " . $nId_name . " != " . $nId;
        }

        $query1 = $this->db->query($query)->result_array();
        //echo $this->db->last_query();
        $total_records = count($query1);
        //print_r($query1);

        if ($total_records) {
            //echo $this->create_slug($title, $table_name, $field_name, $nId_name, $nId, ++$count);
            return $this->create_slug($title, $table_name, $field_name, $nId_name, $nId, ++$count);
        } else {
            //echo $slug;
            return str_replace('-', '_', $slug);
        }
    }

    public function get_data_page($tabel_name, $where = '', $page = 0, $per_page = 0, $is_count = 0, $orderby = '', $order = 'ASC')
    {

        $query = "SELECT *
                  FROM " . $tabel_name . "
                  " . $where . "
                  ";

        if ($orderby != '') {
            $query .= "ORDER BY " . $orderby . " " . $order;
        }
        if ($is_count > 0) {
            $query .= " LIMIT " . (int) $page . ", " . (int) $per_page;
        }

        $result = $this->db->query($query);
        return $result;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    public function commonselect($tablename, $col_name, $value, $order_by = '', $order = "DESC")
    {

        $this->db->select('*');
        $this->db->from($tablename);
        $this->db->where($col_name, $value);
        if ($order_by != '') {
            $this->db->order_by($order_by, $order);
        }
        $query = $this->db->get();
        return $query;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN Code By Umair / add $where clause for searching and sorting

    public function commonselect_3($tablename, $col_name, $value, $order_by = '', $order = "DESC", $where = false)
    {

        $this->db->select('*');
        $this->db->from($tablename);
        $this->db->where($col_name, $value);
        if ($where) {
            $this->db->where($where);
        }

        if ($order_by != '') {
            $this->db->order_by($order_by, $order);
        }
        $query = $this->db->get();
        return $query;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    public function commonselect_array($tablename, $data, $order_by = '', $order = "DESC")
    {
        $this->db->where($data);
        if ($order_by != '') {
            $this->db->order_by($order_by, $order);
        }
        $query = $this->db->get($tablename);
//        echo $this->db->last_query();
        return $query;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    public function commonselect2($tablename, $col_name, $value, $col_name2, $value2, $orderby = '', $order = "DESC")
    {
        $this->db->select('*');
        $this->db->from($tablename);
        $this->db->where($col_name, $value);
        $this->db->where($col_name2, $value2);
        if ($orderby != '') {
            $this->db->order_by($orderby, $order);
        }
        $query = $this->db->get();
        return $query;
    }

    public function commonselect3($tablename, $col_name, $value, $col_name2, $value2, $col_name3, $value3, $orderby = '', $order = "DESC")
    {
        $this->db->select('*');
        $this->db->from($tablename);
        $this->db->where($col_name, $value);
        $this->db->where($col_name2, $value2);
        $this->db->where($col_name3, $value3);
        if ($orderby != '') {
            $this->db->order_by($orderby, $order);
        }
        $query = $this->db->get();
        return $query;
    }

    public function commonselect4($tablename, $col_name, $value, $col_name2, $value2, $col_name3, $value3, $col_name4, $value4, $orderby = '', $order = "DESC")
    {
        $this->db->select('*');
        $this->db->from($tablename);
        $this->db->where($col_name, $value);
        $this->db->where($col_name2, $value2);
        $this->db->where($col_name3, $value3);
        $this->db->where($col_name4, $value4);
        if ($orderby != '') {
            $this->db->order_by($orderby, $order);
        }
        $query = $this->db->get();
        return $query;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    public function getCombox($tablename, $orderby = '', $order = '')
    {

        $this->db->select('*');
        $this->db->from($tablename);
        if ($orderby != '') {
            $this->db->order_by($orderby, $order);
        }
        $query = $this->db->get();
        return $query;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    public function getCombox2($tablename)
    {

        $this->db->select('*');

        $this->db->from($tablename);

        $this->db->order_by("com_name", "DESC");
        $query = $this->db->get();

        return $query;
    }

    public function getComboxuser($tablename, $where)
    {

        $this->db->select('*');

        $this->db->from($tablename);

        $this->db->where('user_id', $where);

        $query = $this->db->get();

        return $query;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    public function getComboxpages($tablename, $where)
    {

        $this->db->select('*');

        $this->db->from($tablename);

        $this->db->where('page_sefurl', $where);

        $query = $this->db->get();

        return $query;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    public function commonSave($table, $data)
    {

        $this->db->insert($table, $data);

        $user_id = $this->db->insert_id();

        return $user_id;
    }

    public function commonSave1($table, $data)
    {
        $this->db->insert($table, $data);
        $user_id = $this->db->insert_id();
        //$this->registerInCommunity($data);        // By Burhan
        return $user_id;
    }

    // By Burhan
    public function registerInCommunity($data)
    {

        $param = array(
            "mode"          => "register",
            "security_hash" => $this->config->item('community_security_hash'),
        );

        echo $this->curlPost($this->config->item('community_url') . "_communicator.php", array_merge($data, $param));
    }

    // By Burhan
    public function loginInCommunity($user_email, $user_password)
    {

        $data = array(
            "mode"          => "login",
            "security_hash" => $this->config->item('community_security_hash'),
            "user_email"    => $user_email,
            "user_password" => $user_password,
        );

        $response = $this->curlPost($this->config->item('community_url') . "_communicator.php", $data);
        $cookies  = $this->parseCookies($response);

        foreach ($cookies as $name => $val) {
            setcookie($name, $val, time() + 360000);
        }
    }

    // By Burhan
    public function curlPost($url, $data)
    {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $cookiestore = tempnam(sys_get_temp_dir(), '_cookiejar_');

        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiestore); // set cookie file to given file
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiestore); // set same file as cookie jar
        curl_setopt($ch, CURLOPT_COOKIE, $cookiestore);

        $server_output = curl_exec($ch);

        curl_close($ch);

        return $server_output;
    }

    public function parseCookies($result)
    {
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $matches);
        $cookies = array();
        foreach ($matches[1] as $item) {
            parse_str($item, $cookie);
            $cookies = array_merge($cookies, $cookie);
        }
        return $cookies;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    public function commonUpdate($table, $data, $col, $val)
    {

        $this->db->where($col, $val);

        $result = $this->db->update($table, $data);
        return $result;
    }

    public function commonUpdatesimple($table, $data)
    {

        $result = $this->db->update($table, $data);
        return $result;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    public function commonUpdate2($table, $data, $col, $val, $col2, $val2)
    {

        $this->db->where($col, $val);

        $this->db->where($col2, $val2);

        $result = $this->db->update($table, $data);
        return $result;
    }

    // MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    public function commonUpdate3($table, $data, $col, $val, $col2, $val2, $col3, $val3)
    {

        $this->db->where($col, $val);

        $this->db->where($col2, $val2);
        $this->db->where($col3, $val3);

        $result = $this->db->update($table, $data);
        return $result;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    public function commonDelete($table, $col, $val)
    {

        $this->db->where($col, $val);

        $result = $this->db->delete($table);

        return $result;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    public function commonDelete2($table, $col, $val, $col2, $val2)
    {

        $this->db->where($col, $val);

        $this->db->where($col2, $val2);

        $this->db->delete($table);
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    public function getCommon($table, $orderBy, $order = 'DESC')
    {

        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by($orderBy, $order);
        $query = $this->db->get();
        return $query;
    }

    public function common_select($table)
    {
        $this->db->select('*');
        $this->db->from($table);
        $query = $this->db->get();
        return $query;
    }

    public function getCommon_pagination($table, $limit = "", $start = "")
    {

        $query = "SELECT * from " . $table . "";

        if ($limit > 0) {
            $query .= " order by date_created DESC LIMIT " . (int) $start . ", " . (int) $limit;
        }

        $query = $this->db->query($query);
        return $query;
    }

    ///////////////////contact communication listing/////////////////
    public function contact_communication_listing($table, $limit = "", $start = "")
    {
        $query = "SELECT * from " . $table . "";

        if ($limit > 0) {
            $query .= " order by date_created DESC LIMIT " . (int) $start . ", " . (int) $limit;
        }

        $query = $this->db->query($query);
        return $query;
    }

    ////////////////////////////////////////////////////////////////
    // MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    public function recordExistCommon($table, $field_name, $field_value)
    {

        if (trim($field_value) != '') {

            $this->db->select($field_name);

            $this->db->from($table);

            $this->db->where($field_name, $field_value);

            $query = $this->db->get();

            if ($query->num_rows() > 0) {

                return true;
            } else {

                return false;
            }
        } else {

            return false;
        }
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    public function recordExistCommon2($table, $field_name, $field_value, $field_name2, $field_value2)
    {

        if (trim($field_value) != '') {

            $this->db->select($field_name);

            $this->db->from($table);

            $this->db->where($field_name, $field_value);

            $this->db->where($field_name2, $field_value2);

            $query = $this->db->get();

            if ($query->num_rows() > 0) {

                return true;
            } else {

                return false;
            }
        } else {

            return false;
        }
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    public function getSingleValue($table, $where, $fieldname)
    {

        $q = "select $fieldname from $table where $where";

        $query = $this->db->query($q);

        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {
                return $row->$fieldname;
            }
            //$row[$fieldname];
        } else {
            return 'N/A';
        }

    }

// function close getSingleValue
    //////update/////////
    public function update($tableName, $arr, $where)
    {
        $str = $this->db->update_string($tableName, $arr, $where);
        $rs  = $this->db->query($str);
        return $rs;
    }

//////////////////update///////////////
    // MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    public function common_get_colum_value($col, $table, $condition_str)
    {

        $res = '';

        $this->db->select($col);

        $this->db->from($table);

        $this->db->where($condition_str);

        $result = $this->db->get();

        if ($result->num_rows() > 0) {

            foreach ($result->result_array() as $row) {

                return $res = $row[$col];
            }
        }

        return $res;
    }

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

    public function query($query)
    {

        return $result = $this->db->query($query);
    }

    public function common_where($tbl, $where, $orderBy = "", $orderType = "")
    {
        $this->db->where($where);
        if ($orderBy != "") {
            $this->db->order_by($orderBy, "DESC");
        }

        return $this->db->get($tbl);
    }

    public function countRecord($tbl, $where = "")
    {
        return $this->db->query("SELECT COUNT(*) AS Num FROM  " . $tbl . " " . $where)->row()->Num;
    }

//MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
    public function countQuery($table, $select, $where = "")
    {

        $this->db->select($select);
        $this->db->from($table);
        if ($where != "") {
            $this->db->where($where);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function strip_word_html($text, $allowed_tags = '')
    {
        mb_regex_encoding('UTF-8');
        $search  = array('/&lsquo;/u', '/&rsquo;/u', '/&ldquo;/u', '/&rdquo;/u', '/&mdash;/u');
        $replace = array('\'', '\'', '"', '"', '-');
        $text    = preg_replace($search, $replace, $text);

        $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');

        if (mb_stripos($text, '/*') !== false) {
            $text = mb_eregi_replace('#/\*.*?\*/#s', '', $text, 'm');
        }

        $text        = preg_replace(array('/<([0-9]+)/'), array('< $1'), $text);
        $text        = strip_tags($text, $allowed_tags);
        $text        = preg_replace(array('/^\s\s+/', '/\s\s+$/', '/\s\s+/u'), array('', '', ' '), $text);
        $search      = array('#<(strong|b)[^>]*>(.*?)</(strong|b)>#isu', '#<(em|i)[^>]*>(.*?)</(em|i)>#isu', '#<u[^>]*>(.*?)</u>#isu');
        $replace     = array('<b>$2</b>', '<i>$2</i>', '<u>$1</u>');
        $text        = preg_replace($search, $replace, $text);
        $num_matches = preg_match_all("/\<!--/u", $text, $matches);
        if ($num_matches) {
            $text = preg_replace('/\<!--(.)*--\>/isu', '', $text);
        }
        return $text;
    }

    public function check_front_user($array)
    {
        $query = $this->db->query("
                                    SELECT *
                                    FROM tbl_user
                                    WHERE
                                    user_email = '" . $array['user_email'] . "'
                                    AND
                                    user_password = '" . $array['user_password'] . "'
                                    AND
                                    user_status = 1
                                ");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    ##############################################################################################################################
    ################################################# header cart data ###########################################################

    public function header_order_listing()
    {
        $data = array();
        $i    = 0;
        if (count($this->cart->contents()) > 0 && count($this->cart->total()) > 0) {
            foreach ($this->cart->contents() as $key => $items) {
                if ($this->cart->has_options($items['rowid'])) {
                    $data[$i]['rowid']             = $items['rowid'];
                    $data[$i]['id']                = $items['id'];
                    $data[$i]['qty']               = $items['qty'];
                    $data[$i]['price']             = $items['price'];
                    $data[$i]['name']              = $items['name'];
                    $data[$i]['subtotal']          = $items['subtotal'];
                    $data[$i]['order_prod_image']  = $items['options']['order_prod_image'];
                    $data[$i]['order_ship_detail'] = json_decode($items['options']['order_detail']);
                    if (isset($items['options']['order_ribbon_detail'])) {
                        $data[$i]['order_ribbon_detail_header'] = json_decode($items['options']['order_ribbon_detail']);
                    }

                    #$data['order_ship_detail'][$i] = json_decode($items['options']['order_detail']);
                    #$data['order_ribbon_detail'][$i] = json_decode($items['options']['order_ribbon_detail']);
                    $i++;
                }
            }

            /* echo "<pre>";
            print_r($data);
            exit; */
            return $data;
        } else {
            return null;
        }
    }

    ##############################################################################################################################
    ################################################# header cart data ###########################################################

    public function getActiveSellers()
    {
        $query = $this->db->query('
                                    SELECT COUNT(u.user_id) as ActiveSellers
                                    FROM tbl_user u 
                                    INNER JOIN tbl_stores s ON s.user_id = u.user_id 
                                    WHERE u.user_status = 1 AND u.user_is_delete = 0 AND u.user_type = "Seller" AND s.store_is_hide = 0
                                ');
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function getSellersOnVacation()
    {
        $query = $this->db->query("
                                    SELECT COUNT(store_id) as SellersOnVacation
                                    FROM tbl_stores s
                                    INNER JOIN tbl_user u ON u.user_id = s.user_id 
                                    WHERE store_is_hide = 1 AND user_status = 1 AND user_is_delete = 0 AND 
                                    user_type = 'Seller'
                                ");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function getTotalSellers()
    {
        $query = $this->db->query('
                                    SELECT COUNT(user_id) as TotalSellers
                                    FROM tbl_user
                                    WHERE user_status = 1 AND user_is_delete = 0 AND user_type = "Seller"
                                ');
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function getTotalBuyers()
    {
        $query = $this->db->query('
                                    SELECT COUNT(user_id) as TotalBuyers
                                    FROM tbl_user
                                    WHERE user_status = 1 AND user_is_delete = 0 AND user_type = "Buyer"
                                ');
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function getTotalFlaggedUsers()
    {
        $query = $this->db->query('
                                    SELECT COUNT(user_id) as TotalFlaggedUsers
                                    FROM tbl_user
                                    WHERE user_flagged_status = 1 AND user_status = 1 AND user_is_delete = 0
                                ');
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function getTotalRegisteredUsers()
    {
        $query = $this->db->query('
                                    SELECT COUNT(user_id) as TotalRegisteredUsers
                                    FROM tbl_user
                                    WHERE user_status = 1 AND user_is_delete = 0
                                ');
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function getFlagStatusById($user_id)
    {
        $query = $this->db->query("
                                    SELECT user_flagged_status, user_flagged_counter
                                    FROM tbl_user
                                    WHERE
                                    user_id = " . $user_id . "
                                ");
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return false;
        }
    }

    public function getTierStatistics($tier_id)
    {
        $row['existing'] = $this->db->query("SELECT COUNT(*) as total
                                            FROM tbl_stores store
                                            INNER JOIN tbl_user user ON store.user_id = user.user_id
                                            WHERE store.store_tier_id = ".$tier_id." AND user_is_delete = 0 AND store_is_hide = 0 AND user_status = 1
                                            ")->row()->total;
        $row['Renewal'] = $this->db->query("SELECT COUNT(*) as total
                                            FROM tbl_user_store_subscription
                                            WHERE sub_tier_id = ".$tier_id." AND sub_is_renewal = 1
                                            ")->row()->total;
        $row['churn'] = $this->db->query("SELECT COUNT(*) as total
                                            FROM tbl_user_store_subscription
                                            WHERE sub_tier_id = ".$tier_id." AND sub_is_churn = 1
                                            ")->row()->total;
        $row['New']   = $this->db->query("SELECT COUNT(*) as total
                                            FROM tbl_user_store_subscription
                                            WHERE sub_id IN (
                                            SELECT MAX(sub_id)
                                            FROM tbl_user_store_subscription
                                            GROUP BY sub_store_id
                                            ) AND sub_tier_id = ".$tier_id." AND sub_is_churn = 0 AND sub_is_renewal = 0
                                            ")->row()->total;
        return $row;
    }

    public function getTierStatistics_2($tier_id)
    {
        $curr_date = date('Y-m-d');
        $query     = $this->db->query("
                                    SELECT DISTINCT sub_store_id
                                    FROM tbl_user_store_subscription
                                    WHERE
                                    sub_tier_id = " . $tier_id . "
                                ");
        $data_stat['total_sub'] = count($query->result());
        $query                  = $this->db->query("
                                    SELECT sub_id, sub_tier_id, sub_store_id,sub_start,sub_ends
                                    FROM tbl_user_store_subscription
                                    WHERE sub_id IN (
                                        SELECT MAX(sub_id)
                                        FROM tbl_user_store_subscription
                                        GROUP BY sub_store_id
                                    ) AND sub_tier_id = " . $tier_id . " AND sub_start < '" . $curr_date . "' AND sub_ends > '" . $curr_date . "'
                                ");
        $data_stat['existing'] = count($query->result());
        $data_stat['data']          = $this->getLatestsubscription($tier_id);
        $data_stat['churn'] = $this->db->query("SELECT COUNT(*) chun
                                                FROM tbl_user_store_subscription
                                                WHERE sub_id IN (
                                                  SELECT MAX(sub_id)
                                                  FROM tbl_user_store_subscription
                                                  GROUP BY sub_store_id
                                                  ) AND sub_tier_id = ".$tier_id." AND DATE(sub_ends) < DATE(".$curr_date.")
                                                ")->row();
        echo "<pre>";
        print_r($data_stat);
        exit;
        return $data_stat;
    }

    public function getLatestsubscription($tier_id)
    {
        $data = array();
        $renewCount  = $newCount = 0;
        $query  = $this->db->query("
                                    SELECT sub_id, sub_tier_id, sub_store_id
                                    FROM tbl_user_store_subscription
                                    WHERE sub_id IN (
                                        SELECT MAX(sub_id)
                                        FROM tbl_user_store_subscription
                                        GROUP BY sub_store_id
                                    ) AND sub_tier_id = " . $tier_id . "
                                ");
        foreach ($query->result() as $val) {
            $oldSub = $this->getSecondLast($val->sub_store_id);
            if($oldSub > 0){
                if ($oldSub == $val->sub_tier_id) {
                    $renewCount++;
                }
            }else {
                $newCount++;
            }
            
        }

        $data['Renewal'] = $renewCount;
        $data['New'] = $newCount;

        return $data;
    }

    public function getSecondLast($store_id = '')
    {
        $query = $this->db->query("
                                    SELECT sub_tier_id
                                    FROM tbl_user_store_subscription
                                    WHERE sub_store_id = " . $store_id . "
                                    ORDER BY sub_id DESC
                                    LIMIT 1,1
                                ");
        $query = $query->row();
        if(!empty($query) ){
            return $query->sub_tier_id;
        }else{
            return 0;
        }
    }

    public function getChurnUsers($tier_id = '')
    {
        $query = $this->db->query("
                                    SELECT count(sub_id) as Total, sub_store_id
                                    FROM tbl_user_store_subscription
                                    WHERE sub_tier_id = " . $tier_id . "
                                    GROUP BY sub_store_id
                                ");
        //return $query->result();
        echo "<pre>";
        print_r($query->result());
        exit;
    }

    public function send_email($data)
    {

        $this->load->library('email');
        $config              = array();
        $config['useragent'] = "CodeIgniter";
        $config['mailpath']  = "/usr/sbin/sendmail"; // or "/usr/sbin/sendmail"
        $config['protocol']  = "smtp";
        $config['smtp_host'] = $_SERVER['HTTP_HOST']; //"192.185.173.5";
        $config['smtp_port'] = "25";
        $this->email->initialize($config);
        $this->email->set_mailtype('html');
        $this->email->from($data['from']);
        $this->email->to($data['to']);
        $this->email->subject($data['subject']);
        $this->email->message($data['message']);
        /*
        if(@$data['attachement'] != '')
        {
        $this->email->attach($data['attachement']);
        }
         */
        if ($this->email->send()) {
            $this->email->clear(true);
            return true;
        }
        return false;
    }

    public function get_table_listing($tabel_name, $fields, $where = '', $jointable = '', $joincondition = '', $jointype = '', $page = 0, $per_page = 0, $is_count = 0, $orderby = '', $order = 'ASC')
    {

        $query = "SELECT " . $fields . "
                  FROM " . $tabel_name . "
                 ";
        if ($jointable != '') {

            $query .= $jointype . " JOIN " . $jointable . " ON " . $joincondition;
        }
        if ($where != '') {

            $query .= " WHERE " . $where;
        }

        if ($orderby != '') {
            $query .= " ORDER BY " . $orderby . " " . $order;
        }
        if ($is_count > 0) {
            $query .= " LIMIT " . (int) $page . ", " . (int) $per_page;
        }
//        var_dump($query);die;
        $result = $this->db->query($query);
        return $result;
    }

    public function content_upload($Files1, $file_name, $path, $pg_id, $page_name = '')
    {

        $filename = array();
        if ($Files1['name'] !== "") {
            $config = array(
                'allowed_types' => 'jpg|jpeg|png|ppt|txt|pdf|gif',
                'upload_path'   => FCPATH . $path,
                'file_name'     => 'content_' . date('Y_m_d_h_i_s'),
            );
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('' . $file_name . '')) {
                //Print message if file is not uploaded
                $this->session->set_flashdata('msg', $this->upload->display_errors());
                header("Location: " . base_url() . "admin/pages/" . $page_name);
                exit();
            } else {
                $dataDP   = $this->upload->data();
                $filename = $dataDP['file_name'];
                $this->load->library('image_lib');
                $configt['image_library']  = 'gd2';
                $configt['source_image']   = $dataDP['full_path'];
                $configt['new_image']      = FCPATH . $path . "/thumb/";
                $configt['maintain_ratio'] = false;
                $configt['width']          = 80;
                $configt['height']         = 80;
                $this->image_lib->initialize($configt);
                $this->image_lib->resize();

                return $filename;
            }
        }
    }

    public function default_shipping($user_id)
    {
        $query = "SELECT
                    country.nicename,address.*
                    FROM
                    tbl_user_addreses address
                    INNER JOIN tbl_country country
                    ON country.iso = address.country
                    WHERE add_user_id = " . $user_id . "
                    AND default_shipping_check = 'Y'
                    ORDER BY add_id DESC
                    LIMIT 1 ";
//        echo $query;
        $result = $this->db->query($query);
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
    }

    public function default_billing($user_id)
    {
        $query = "SELECT
                    country.nicename,address.*
                    FROM
                    tbl_user_addreses address
                    INNER JOIN tbl_country country
                    ON country.iso = address.country
                    WHERE add_user_id = " . $user_id . "
                    AND default_billing_check = 'Y'
                    ORDER BY add_id DESC
                    LIMIT 1 ";
//        echo $query;C
        $result = $this->db->query($query);
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
    }
   

    function getSellerInfoById($user_id) {
        $query = "SELECT 
                    * 
                    FROM
                    tbl_user u 
                    INNER JOIN tbl_stores s ON u.user_id = s.user_id 
                    WHERE u.user_id = " . $user_id . "  
                    ORDER BY u.user_id DESC 
                    LIMIT 1 ";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
    }
    public function commonDeletearray($table, $col, $val)
    {

        $this->db->where_in($col, $val);
        $result = $this->db->delete($table);
        echo $this->db->last_query();
        return $result;
    }
    
    public function get_order_details($order_id){
        $query = "SELECT 
                    * 
                    FROM
                    tbl_orders
                    WHERE order_id = " . $order_id . " 
                    LIMIT 1 ";
        $result = $this->db->query($query);
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return false;
        }
    }
    
    public function getProduct1lvlRequiredFilter($prod_id) {
        $result_array = array();
        $query = "SELECT 
                * 
                FROM
                tbl_product_filters pf 
                INNER JOIN tbl_cat_filter_title cft 
                ON cft.filter_title_id = pf.filter_id 
                WHERE pf.prod_id = " . $prod_id . " 
                AND pf.is_unique = 0    
                AND cft.is_required = 1 ";
//        HAVING COUNT(DISTINCT pf.filter_id) > 1";
        $result = $this->db->query($query);
//        echo $this->db->last_query();
        if ($result->num_rows() > 0) {
            foreach ($result->result_array() as $key => $label) {
                $result_array[$key]['id'] = $label['id'];
                $result_array[$key]['prod_id'] = $label['prod_id'];
                $result_array[$key]['filter_id'] = $label['filter_id'];
                $result_array[$key]['filter_slug'] = $label['filter_slug'];
                $result_array[$key]['filter_value'] = $label['filter_value'];
                $result_array[$key]['category_id'] = $label['category_id'];
                $result_array[$key]['is_unique'] = $label['is_unique'];
                $result_array[$key]['filter_title_id'] = $label['filter_title_id'];
                $result_array[$key]['filter_cat_id'] = $label['filter_cat_id'];
                $result_array[$key]['filter_title'] = $label['filter_title'];
                $result_array[$key]['cat_filter_values'] = $label['cat_filter_values'];
                $result_array[$key]['cat_filter_is_conditional'] = $label['cat_filter_is_conditional'];
                $result_array[$key]['is_required'] = $label['is_required'];
            }
            return $result_array;
        } else {
            return false;
        }
    }
    
    public function unique_multidimentional_array($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach ($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

    public function get_categories_html()
    {
        $html['cat_html'] = "";
        $html['cat_html'] .= "<ul id='tree'>";
        if(count(getMainCategories()>0)){
            foreach(getMainCategories() as $cat) {
                $cat_prod = get_main_count($cat['cat_id']);
                $html['cat_html'] .= '<li> <span><a href="javascript:void" class="cat_main_click" data-name="'.$cat["cat_name"].'" data-url="'.$cat["cat_url"].'">'.$cat["cat_name"].'</a><h5>('.(($cat_prod>0)?$cat_prod:'0').')</h5></span>';                
                $sub_category = getSubCategories($cat['cat_id']);
                if (count($sub_category) > 0) {
                    $html['cat_html'] .= "<ul>";
                    foreach ($sub_category as $sub_cat) {                        
                        $sub_sub_category = getSubSubCategories($sub_cat['cat_id']);
                        $sub_cat_prod = get_sub_count($sub_cat['cat_id']);
                        $html['cat_html'] .= '<li> <span><a href="javascript:void" class="cat_sub_click" data-parent-name="'.$cat['cat_name'].'" data-parent-url="'.$cat['cat_url'].'" data-name="'.$sub_cat['cat_name'].'" data-url="'.$sub_cat['cat_url'].'">'.$sub_cat['cat_name'].'</a> <h5>('.(($sub_cat_prod>0)?$sub_cat_prod:'0').')</h5></span>';
                        if ($sub_sub_category > 0) {
                            $html['cat_html'] .= "<ul>";
                            foreach ($sub_sub_category as $sub_sub_cat) {
                                $sub_sub_cat_prod = getproductcount($sub_sub_cat['cat_id']);
                                $html['cat_html'] .= '<li><a href="javascript:void" class="cat_sub_sub_click" data-grand-parent-name="'.$cat['cat_name'].'" data-grand-parent-url="'.$cat['cat_url'].'" data-parent-name="'.$sub_cat['cat_name'].'" data-parent-url="'.$sub_cat['cat_url'].'" data-name="'.$sub_sub_cat['cat_name'].'" data-url="'.$sub_sub_cat['cat_url'].'">'.$sub_sub_cat['cat_name'].'</a> <h5>('.$sub_sub_cat_prod->prod_count.')</h5></li>';
                            }
                            $html['cat_html'] .= "</ul>";
                        }
                        $html['cat_html'] .= "</li>";
                    }
                    $html['cat_html'] .= "</ul>";
                }             
                $html['cat_html'] .= "</li>";
            }
        }
        $html['cat_html'] .= "</ul>";

        return $html['cat_html'];
    }
}
