<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model{
	 
    public function __construct()
 	{
 		$this->load->database();
 	}
	
	function getuser($strWhere="", $recordperpage=20, $page=0,$nRecordCount=0) //"ASC"
	{
		$query ="SELECT * FROM  tbl_user WHERE  user_id > 0 ".$strWhere;	
		if($nRecordCount>0)
		{
			$query .= " ORDER BY  user_id DESC LIMIT ".(int)$page.", ".(int)$recordperpage;		
		}	 
		
		$query = $this->db->query($query);
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return NULL;
		}

	}
	
	function get_user($strWhere="")//"ASC"
	{
		$query ="SELECT *, DATEDIFF(CURDATE(), USER.`user_last_active`) AS last_active_difference FROM  tbl_user  USER WHERE  user_id > 0 ".$strWhere;
                $query = $this->db->query($query);
		
		if ($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		else
		{
			return NULL;
		}

	}
	
	
	function getusershort($strWhere="", $recordperpage=20, $page=0,$nRecordCount=0) //"ASC"
	{
		$sort_by =  $this->session->userdata('sort_by');
		$order_by = $this->session->userdata('sort_order');
		if($sort_by == '')
		{
			$sort_by = 'user_id';
			$_sortdata = array('sort_by'  => 'user_register_date');
			$_sortdata = array('sort_order' => 'DESC');
		
			$this->session->set_userdata($_sortdata);		

		}
		if($order_by == '')
		{
			$order_by = 'DESC';
			
		}
		
				//DATEDIFF(CURDATE(), user.user_last_active) AS last_active_difference
		$query ="SELECT (SELECT store.store_is_hide FROM `tbl_stores` store WHERE store.user_id = user.user_id) AS store_vacations,(SELECT store.store_reported FROM `tbl_stores` store WHERE store.user_id = user.user_id) AS store_reported,user.user_id,user.user_fname,user.user_lname,user.user_email,user.user_type,user.user_register_date,user.user_status,user.user_last_active,user.user_flagged_status
				 FROM tbl_user user
				 WHERE user.user_id > 0 AND user.user_is_delete = 0 AND user.user_email != '' ".$strWhere;
				
		if($nRecordCount > 0)
		{
			$query .= " ORDER BY  ".$sort_by." ".$order_by." LIMIT ".(int)$page.", ".(int)$recordperpage;
		}
	
		$query = $this->db->query($query);
                
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return NULL;
		}

	}
	
	function  getUserExport($strWere="",$nId="")
	{
	
		$query="SELECT user_id,user_register_date,user_fname,user_lname,user_email,user_type,user_last_active,user_status FROM tbl_user WHERE user_is_delete = 0 ".$strWere." ORDER BY user_id DESC
				 ";	
	 $query = $this->db->query($query);
  
		 if ($query->num_rows() > 0)
		 
		 {
		  return $query->result_array();
		 }
		 else
		 {
				return NULL;
		 }
	}
	
	public function get_default_user_address_details($id){
		$query = $this->db->query("
									SELECT
									tbl_user.*,
									tblstates.*,
									tblcountries.*
									FROM `tbl_user`
									LEFT JOIN tblstates
									ON tblstates.stat_id = tbl_user.user_state
									LEFT JOIN tblcountries
									ON tblcountries.coun_id = tbl_user.user_country
									WHERE
									tbl_user.user_id = '".$id."'
								");
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_user_address_details($id){
		$query = $this->db->query("
									SELECT
									tbl_address.*,
									tblstates.*,
									tblcountries.*
									FROM `tbl_address`
									LEFT JOIN tblstates
									ON tblstates.stat_id = tbl_address.add_state
									LEFT JOIN tblcountries
									ON tblcountries.coun_id = tbl_address.add_country
									WHERE
									tbl_address.add_user_id = '".$id."'
									ORDER BY tbl_address.add_id DESC
								");
		if($query->num_rows()>0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function user_sort($value ,$date)
	{
		$order_by = "";
		$where = "";
		
		if($value == 1)
		{
			
			$order_by = "user_fname , user_lname ASC";
			
		}
		if($value == 2)
		{
			
			$order_by = "user_fname DESC";
			
		}
		if($value == 3)
		{
			$order_by = "user_register_date DESC";
			
		}
		if($value == 4)
		{
			$order_by = "user_register_date ASC";
			
		}
		if($date)
		{
			$expload_date = explode("to", $date);
			@$date1 = $expload_date[0];
			@$date2 = $expload_date[1];
			$where = " WHERE (user_register_date between '$date1' and '$date2')";
		
			$order_by = "user_id DESC";
		}
		$query =  $this->db->query("SELECT * from tbl_user ".$where." ORDER BY ".$order_by."");
			
			if($query->num_rows()>0){
			return $query->result_array();
			}
		
	}

	public function validate_new_account($key)
	{
		$query = $this->db->query("
									SELECT user_id 
									FROM tbl_user
									WHERE
									user_activation_code_password = '".$key."'
								");
		if($query->num_rows()>0)
		{
			return $query->row_array();
		}
		else
		{
			return false;
		}
	}
	
	public function update_validate_status($id)
	{
		$this->db->where('user_id',$id);
		$result = $this->db->update('tbl_user',array('user_status' => 1));
		
		if($result)
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	
	
	
}