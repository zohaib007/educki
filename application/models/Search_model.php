<?php

class Search_model extends CI_Model{

   public function __construct()
	{
		$this->load->database();
	}
	public function get_search_data($where = '', $sort_by = '')
	{
		$query = "
					SELECT tbl_product.*, tbl_product_category.*,
					navigation.cat_id AS nnavigation_id, navigation.cat_name AS navigation_name, navigation.cat_url AS navigation_url,
					category.cat_id AS category_id, category.cat_name AS category_name, category.cat_url AS category_url,
					subCategory.cat_id AS subCategory_id, subCategory.cat_name AS subCategory_name, subCategory.cat_url AS subCategory_url
					
					FROM tbl_product
					
					INNER JOIN tbl_product_category ON tbl_product.prod_id = tbl_product_category.prod_cat_prod_id
					
					
					INNER JOIN tbl_categories AS navigation ON prod_cat_navigation_id = navigation.cat_id 
					INNER JOIN tbl_categories AS category ON prod_cat_category_id = category.cat_id 				 
					INNER JOIN tbl_categories AS subCategory ON prod_cat_sub_cat_id = subCategory.cat_id
					
					WHERE
					prod_is_delete = 0
					AND
					prod_status = 1
					".$where."
					
					
					GROUP BY prod_id
				";
		if($sort_by != '')
		{
			$query .= "ORDER BY prod_price ".$sort_by."";
		}
		
		$query = $this->db->query($query);
		
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return NULL;
		}
		
	}
	
	
	public function search_related_products($search_text)
	{
		$search_text = strtolower($search_text);
		$query = "
				SELECT
				tbl_product.prod_name
				
				FROM tbl_product
				
				INNER JOIN tbl_product_category ON prod_cat_prod_id = prod_id
				
				INNER JOIN tbl_categories AS navigation ON prod_cat_navigation_id = navigation.cat_id 
				INNER JOIN tbl_categories AS category ON prod_cat_category_id = category.cat_id 				 
				INNER JOIN tbl_categories AS subCategory ON prod_cat_sub_cat_id = subCategory.cat_id
				
				WHERE		
				(
				LOWER(prod_name) LIKE '%".$search_text."%'
				OR
				LOWER(prod_sku) LIKE '%".$search_text."%' 
				)
				AND
				prod_is_delete = 0
				AND
				prod_status = 1
				
				GROUP BY prod_id
				ORDER BY prod_name ASC
				";
				
		$query = $this->db->query($query);
		
		
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
	
	public function most_popular( $str_where )
	{
		
		
		$query = "
				SELECT  tbl_product.*, tbl_product_category.*,
				
				navigation.cat_id AS nnavigation_id, navigation.cat_name AS navigation_name, navigation.cat_url AS navigation_url,
				category.cat_id AS category_id, category.cat_name AS category_name, category.cat_url AS category_url,
				subCategory.cat_id AS subCategory_id, subCategory.cat_name AS subCategory_name, subCategory.cat_url AS subCategory_url,
				
				SUM(tbl_order_detail.order_qty) AS QTY 
				
				FROM tbl_product
				
				INNER JOIN tbl_product_category ON tbl_product.prod_id = tbl_product_category.prod_cat_prod_id
				
				INNER JOIN tbl_categories AS navigation ON prod_cat_navigation_id = navigation.cat_id 
				INNER JOIN tbl_categories AS category ON prod_cat_category_id = category.cat_id 				 
				INNER JOIN tbl_categories AS subCategory ON prod_cat_sub_cat_id = subCategory.cat_id
				
				INNER JOIN tbl_order_detail ON tbl_product.prod_id = tbl_order_detail.order_prod_id
				
				WHERE
				prod_is_delete = 0
				AND
				prod_status = 1
				
				".$str_where."
				
				GROUP BY prod_id
				ORDER BY QTY DESC 
					
				";
								
		$query = $this->db->query($query);
		
		
		if($query->num_rows()>0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	
	}
	
	
	public function smartSearch($title){
		$result = $this->db->query('
			SELECT pp.prod_id AS id , pp.prod_title AS title, pp.prod_url AS url ,"Products" as  "tbl"
			FROM tbl_products pp
			INNER JOIN tbl_categories cat ON pp.prod_cat_id  = cat.cat_id
			INNER JOIN tbl_stores s ON pp.prod_store_id = s.store_id
			INNER JOIN tbl_user u ON s.user_id = u.user_id
			WHERE pp.prod_status = 1 AND pp.prod_is_delete = 0 AND pp.qty > 0 AND UPPER(pp.prod_title) LIKE UPPER("%'.$title.'%")  AND cat.cat_status = 1 AND cat.cat_is_delete = 0 AND s.store_is_hide = 0 AND u.user_is_delete = 0 AND u.user_status = 1			
			UNION
			SELECT ss.store_id AS id , ss.store_name AS title, ss.store_url AS url ,"Stores" as  "tbl"
			FROM tbl_stores ss
			INNER JOIN tbl_user u ON ss.user_id = u.user_id
			WHERE ss.store_is_hide = 0 AND UPPER(ss.store_name) LIKE UPPER("%'.$title.'%")  AND u.user_is_delete = 0 AND u.user_status = 1
			UNION
			SELECT bb.blog_id AS id ,  bb.blog_name AS title, bb.blog_url AS url ,"Blogs" as  "tbl"
			FROM tbl_blog bb
			WHERE bb.blog_status = 1 AND UPPER(bb.blog_name) LIKE UPPER("%'.$title.'%")');
		return $result;
	}
	

}	 
