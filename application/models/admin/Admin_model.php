<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model{

	public function __construct()
	{
		$this->load->database();
	}
	
	public function GetAllSurvey($recordperpage, $page,$nRecordCount=0,$nId=0)
	{
		$strWhere = "";
		$query = " SELECT * FROM  tblsurvey INNER JOIN tbl_users ON user_id = sur_user_id WHERE sur_id > 0 ";
		
		if($nId>0)
		{
			$strWhere = " AND sur_id = '".$nId."'";
		}
		
		$query .= $strWhere;
		
		if($nRecordCount>0)
		{
			$query .= " ORDER BY  sur_id DESC LIMIT ".(int)$page.", ".(int)$recordperpage;		
		}	 
	
		$result = $this->db->query($query);
		return $result;

	}

	public function getEmailTemplate($order, $recordperpage, $page,$nRecordCount=0) //"ASC"
	{
		$query ="SELECT * FROM  tbl_emailtemplate WHERE  	e_id > 0 ";
	
		if($nRecordCount>0)
		{
			$query .= " ORDER BY  e_id DESC LIMIT ".(int)$page.", ".(int)$recordperpage;		
		}	 

		$result = $this->db->query($query);
		return $result;

	}
	
	public function GetAllAdmins($page,$recordperpage,$num=0)
	{
		$this->db->select('*');
		$this->db->from('tbl_users');
		//$this->db->where('user_admin_type' , 'no');
		$this->db->where('user_admin' , 'admin');
		
		if($num>0)
		{
			$result = $this->db->get();
			return $result->num_rows();
		}
		else
		{
			$this->db->order_by("user_id", "DESC");
			$this->db->limit($recordperpage, $page);
			$result = $this->db->get();
			return $result;	
		}
	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function check_admin($post_array)
	{
		
		$result = $this->db->query("
									SELECT *
									FROM tbl_admin
									WHERE
									(admin_username = '".$post_array['user_name']."'
									OR 
									admin_email = '".$post_array['user_name']."')
									AND
									admin_password_md5 = '".$post_array['password']."'
									AND
									admin_status = 1
								");
		
		if($result->num_rows() > 0)
		{
			return $result->row_array();
		}
		else
		{
			return false;
		}
		
	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function forget_password($admin_email)
	{
		$this->db->select('*');
		$this->db->from('tbl_admin');
		$this->db->where('admin_email',$admin_email);
		$result=$this->db->get();
		return $result;		
	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function validate_email($a_email)
	{
		$this->db->select('*');
		$this->db->from('tbl_admin');
		$this->db->where('admin_email', $a_email);			
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}	
	}
	
// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

	public function validate_password($a_password)
	{
		$this->db->select('*');
		$this->db->from('tbl_admin');
		$this->db->where('admin_password', $a_password);			
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}	
	}
	
// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function get_site_settings()
	{
		$result = $this->db->query('SELECT * FROM tbl_parameters where id = 3');
		return $result;	
	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function get_site_settings2()
	{
		$result = $this->db->query('SELECT * FROM tbl_parameters where id = 1');
		return $result;	
	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function checkold($old)
	{

		$id= $this->session->userdata('admin_id');
		$this->db->select('*');
		$this->db->from('tbl_users');
		$this->db->where('user_id', $id);
		$this->db->where('user_password_md5', $old);			
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}	
	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function change_password($data,$adminid,$oldpass)
	{
 		$this->db->where('user_id', $adminid);
		$this->db->where('user_password_md5', $oldpass);
		$this->db->where('user_admin', "admin");
        return $this->db->update('tbl_users', $data);
	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function getPages_number()
	{
		$this->db->select('*');
		$this->db->from('tbl_pages');
		$result = $this->db->get();
		$numRows = $result->num_rows();
		return $numRows;
	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function getPages_header()
	{
		$result = $this->db->query('SELECT * from tbl_pages where pg_show = 1 OR pg_show = 3 order by header_sort');
		return $result;
	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function getPages_footer()
	{
		$result = $this->db->query('SELECT * from tbl_pages where pg_show = 2 OR pg_show = 3 order by footer_sort');
		return $result;
	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function getPages($category,$page="",$recordperpage="",$sortfield="",$sortorder="")
	{
		$this->db->select('*');
		$this->db->from('tbl_pages');
		if($category!=0)
		{
			$this->db->where('pg_parent', $category);	
		}
		else
		{
			$this->db->limit($recordperpage, $page);
		}

		$result = $this->db->get();
		return $result;
	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function getFAQs($order, $recordperpage, $page) //"ASC"
	{
		$this->db->select('*');
		$this->db->from('tblfaq');
		$this->db->limit($recordperpage,$page);
		$this->db->order_by("faq_id",$order);
		$result = $this->db->get();
		return $result;
	}
	
	public function getStarted($order, $recordperpage, $page,$nRecordCount=0) //"ASC"
	{
		$query ="SELECT * FROM  tblgetstarted WHERE get_email !='' ";
	
		if($nRecordCount>0)
		{
			$query .= " ORDER BY  get_id DESC LIMIT ".(int)$page.", ".(int)$recordperpage;		
		}	 
		$result = $this->db->query($query);
		return $result;
	}

	public function getGlo($order, $recordperpage, $page) //"ASC"
	{
		$this->db->select('*');
		$this->db->from('tblglossary');
		$this->db->limit($recordperpage,$page);
		$this->db->order_by("glo_alpha","");
		$this->db->order_by("glo_term",$order);
		$result = $this->db->get();
		return $result;
	}

	// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function getParentPage($nId="",$order="ASC") //"ASC"
	{
		$this->db->select('*');
		$this->db->from('tbltabs');
		$this->db->order_by("tabs_id",$order);
		$result = $this->db->get();
		return $result;
	}


	public function getLeftnav($nId ,$order="ASC") //"ASC"
	{
		$this->db->select('*');
		$this->db->from('tblleftnav');
	 	$this->db->where('nav_type' , $nId);
		$this->db->order_by("nav_id",$order);
		$result = $this->db->get();
		return $result;
	}

	// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function getTabs($order, $recordperpage, $page) //"ASC"
	{
		$this->db->select('*');
		$this->db->from('tbltabs');
		$this->db->limit($recordperpage,$page);
		$this->db->order_by("tabs_id",$order);
		$result = $this->db->get();
		return $result;
	}

	public function getLeftNavShortDesc()
	{
		$result = $this->db->query('SELECT * FROM `tblleftnav`');
		return $result;	
	}

	// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function getFormParameters()
	{
		$this->db->select('*');
		$this->db->from('tbl_parameters');
		$result=$this->db->get();
		return $result;	
	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function cleanValue($value)
	{
		$value = mysql_real_escape_string($value);
		return ($value);
	}

	public function GetAllUsers($page,$recordperpage,$num=0,$alpha="",$admin=0,$strSearch="",$status="",$orderBy=" ORDER BY  user_id DESC",$adminStatus="")
	{
		$arr = explode("_",$alpha);
		if(@$arr[0]!="")
		{
			$alpha=$arr[0];
		}
		
		$strWhere = "";
		$joinQuer = "";
		if($strSearch!="")
		{
			$strWhere.=" AND (user_fname LIKE '%".$strSearch."%' OR user_lname LIKE '%".$strSearch."%' OR user_email LIKE '%".$strSearch."%' )";	 
		}
		
		if($status!="")
		{
			//$strWhere.="   ";
			$strWhere.=" AND  user_id IN (SELECT info_user_id FROM tbl_camilla_info   WHERE  	 info_status = '".$status."'  AND info_type = 1   AND info_latest_staus = 1 )";//OR info_status_text LIKE
		}
		
		if($adminStatus!="")
		{
			$strWhere .= " AND  user_id IN (SELECT info_user_id FROM tbl_camilla_info   WHERE  	 info_status  LIKE '%".$status."%'     AND info_type = 14 )";//OR info_status_text LIKE
		}
		
		if($alpha!=""&& count($arr)<=1)
		{
			$strWhere=" AND user_fname LIKE '".$alpha."%'";
		}
		elseif($alpha!=""&& count($arr)>1)
		{
			$strWhere=" AND user_id = '".$alpha."'";	
		}
			
		if($alpha==1)
		{
			$strWhere.=" AND user_admin =  'admin'";
		}
		else
		{
			$strWhere.=" AND user_admin !=  'admin'";
		}
			
		$query = "SELECT DISTINCT tbl_users.* FROM tbl_users ".$joinQuer." WHERE user_id > 0  ".$strWhere;
		
		if($num>0)
		{
			$result = $this->db->query($query);
			return $result->num_rows();
		}
		else
		{
			$query.=$orderBy."   LIMIT ".$page." , ".$recordperpage."" ;			 
			$result = $this->db->query($query);
			return $result;
		}
	}

//##############################################################################	

	public function save_admin($table,$data)
	{
		$this->db->insert($table,$data); 
		return $this->db->insert_id();
	}
	
	public function get_adminlist()
	{
		$data = $this->db->get("tbl_admin");
		return $data->result_array();
	}
	
	public function get_admin($id)
	{
		$data = $this->db->get_where("tbl_admin" , array('admin_id' => $id));
		return $data->result_array();
	}
	
	public function save_admin_edit($table,$data,$col ,$val)
	{
		$this->db->where($col,$val);
		$result = $this->db->update($table,$data);
		return $result;
		return TRUE;
	}
	
	public function del_admin($table,$col,$val)
    {
		$this->db->where($col,$val);
	 	$result = $this->db->delete($table);
		return $result;
	}



}
