<?php
class Get_main_cat extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('front/guidetoeducki/Guidetoeducki_model');
        $this->load->model('front/faq/faq_model');
        $this->load->model('front/contact_model');
        $this->load->library('upload');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    }

    public function index()
    {
        $mainCat = $this->common_model->commonselect3('tbl_categories', 'cat_parent_id', 0, 'cat_status', 1, 'cat_is_delete', 0, 'cat_sort', 'ASC')->result();
        $html['data'] = '';
        if(!empty($mainCat)){
            foreach($mainCat as $cat){
                $html['data'] .= "
                                <li style='display:none;' >
                                    <a class='catClick $cat->cat_url' href='javascript:void' data-name='$cat->cat_name' data-url='$cat->cat_url'>$cat->cat_name</a>
                                </li>
                                ";
            }
        }
        echo json_encode($html);
    }

    public function listAllMainCategory(){
        $mainCat = $this->common_model->commonselect3('tbl_categories', 'cat_parent_id', 0, 'cat_status', 1, 'cat_is_delete', 0, 'cat_sort', 'ASC')->result();
        $html['data'] = '';
        if(!empty($mainCat)){
            foreach($mainCat as $cat){
                $html['data'] .= '
                                <div class="cate-block">
                                    <a href="javascript:void" class="catClick '.$cat->cat_url.'" data-name="'.$cat->cat_name.'" data-url="'.$cat->cat_url.'" >
                                        <img  src="'.base_url('resources/cat_image/'.$cat->cat_image).'" alt="'.$cat->cat_name.'" />
                                        <div class="description">
                                            '.$cat->cat_name.'
                                        </div>     
                                    </a>            
                                </div>
                                ';
            }
        }else{
            /*$html['data'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No Data Found</div>";*/
        }
        echo json_encode($html);
    }

    public function listAllSubCategory(){
        $mainCatUrl = $this->input->get('mainCatUrl');
        $html['data'] = '';
        $mainCat = $this->common_model->commonselect4('tbl_categories','cat_url', $mainCatUrl, 'cat_parent_id', 0, 'cat_status', 1, 'cat_is_delete', 0, 'cat_sort', 'ASC')->row();
        if(!empty($mainCat)){
            $subCat = $this->common_model->commonselect3('tbl_categories', 'cat_parent_id', $mainCat->cat_id, 'cat_status', 1, 'cat_is_delete', 0, 'cat_sort', 'ASC')->result();
            if(!empty($subCat)){
                foreach($subCat as $cat){
                    $html['data'] .= '
                                    <div class="cate-block">
                                       <a href="javascript:void" class="subcatClick '.$cat->cat_url.'" data-name="'.$cat->cat_name.'" data-url="'.$cat->cat_url.'" >
                                            <img  src="'.base_url('resources/cat_image/'.$cat->cat_image).'" alt="'.$cat->cat_name.'" />
                                            <div class="description">
                                                '.$cat->cat_name.'
                                            </div>     
                                        </a>            
                                    </div>
                                    ';

                }
            }else{
                /*$html['data'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No Data Found</div>";*/
                $html['re'] = 1;
            }
        }else{
            /*$html['data'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No Data Found</div>";*/
            $html['re'] = 2;
        }
        echo json_encode($html);
    }


    public function listAllSubSubCategory(){
        $mainCatUrl = $this->input->get('mainCatUrl');
        $subCatUrl = $this->input->get('subCatUrl');
        $html['data'] = '';
        $mainCat = $this->common_model->commonselect4('tbl_categories','cat_url', $mainCatUrl, 'cat_parent_id', 0, 'cat_status', 1, 'cat_is_delete', 0, 'cat_sort', 'ASC')->row();
        if(!empty($mainCat)){
            $subCat = $this->common_model->commonselect4('tbl_categories', 'cat_parent_id', $mainCat->cat_id, 'cat_url', $subCatUrl, 'cat_status', 1, 'cat_is_delete', 0, 'cat_sort', 'ASC')->row();
            if(!empty($subCat)){
                $subSubCat = $this->common_model->commonselect3('tbl_categories', 'cat_parent_id', $subCat->cat_id, 'cat_status', 1, 'cat_is_delete', 0, 'cat_sort', 'ASC')->result();
                if(!empty($subSubCat)){
                    foreach($subSubCat as $cat){
                        $html['data'] .= '
                                        <div class="pro-blk">
                                            <a href="javascript:void" class="subSubCatClick '.$cat->cat_url.'" data-name="'.$cat->cat_name.'" data-url="'.$cat->cat_url.'" >
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <span>'.$cat->cat_name.'</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <img  src="'.base_url('resources/cat_image/'.$cat->cat_image).'" alt="'.$cat->cat_name.'" /> 
                                            </a>
                                        </div>
                                        ';
                    }
                }else{
                    /*$html['data'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No Data Found</div>";*/
                    $html['re'] = 1;
                }
            }else{
                /*$html['data'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No Data Found</div>";*/
                $html['re'] = 2; // no sub cat exist
            }
        }else{
            /*$html['data'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No Data Found</div>";*/
            $html['re'] = 3; // No main cat exist
        }
        echo json_encode($html);
    }

   
}