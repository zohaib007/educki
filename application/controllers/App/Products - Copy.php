<?php
class Cart extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('common/common_model');
        $this->load->helper(array('form', 'url'));
        $this->load->model('front/guidetoeducki/Guidetoeducki_model');
        $this->load->model('front/faq/faq_model');
        $this->load->model('front/contact_model');
        $this->data['url'] = base_url();
        $this->load->model('front/cart/cart_model');
        $this->load->model('front/address/address_model');
        if ( !empty($_GET) ) $_GET = array_stripslash($_GET);
        if ( !empty($_POST) ) $_POST = array_stripslash($_POST);
        date_default_timezone_set('America/New_York');
        $this->load->library('upload');
        $this->output->set_header('Access-Control-Allow-Origin: *');
        $this->output->set_header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    }

    public function index()
    {
        echo 'Code.';
    }

    public function displayCart(){
        $html = array();
        $cartId = $this->input->get('cartId');
        $data['userId'] = $this->input->get('userId');
        if($cartId != ''){
            $isCartExist = $this->db->query('SELECT * FROM tbl_cart WHERE cart_id = '.$cartId)->row();

            if(!empty($isCartExist) > 0){
                $this->setCartSession($cartId);
            }else{
                $this->cart_model->removeCart($cartId);
            }
        }

        if ($cartId != '') {
            $data['cart_product_details'] = getProductPrices();
            //echo $this->db->last_query();
            foreach ($data['cart_product_details'] as $product) {
                $product_details = getproduct_details_2($product->cart_prod_id);
                if ($product_details == '') {
                    $this->cart_model->removeCartItem($cartId, $product->cart_prod_id);
                }else{
                    $getCartTotalQty = $this->db->query("SELECT 
                                                SUM(cart_prod_qnty) as total 
                                                FROM tbl_cart_detail 
                                                WHERE cart_id=$cartId 
                                                AND cart_prod_id=$product->cart_prod_id")->row();
                    if($product_details->qty < $getCartTotalQty->total){
                        $this->cart_model->removeCartItem($cartId, $product->cart_prod_id);
                    }
                }
            }
            $data['cart_product_details'] = getProductPrices();
            $data['shipment_sub_total'] = get_shipment_sub_total();
            $data['cart_sub_total'] = getCartSubTotal();
            $data['discount_amount'] = getStoreDiscount($cartId);
            $data['total_shipment_amount'] = $data['shipment_sub_total'] - $data['discount_amount'] + $data['cart_sub_total'];
            $data['main_category_url'] = getFirstMainCategory()->cat_url;
            $data['title'] = "Shopping Cart";
            $html['cart_html'] = $this->load->view("app/cart", $data, true);
            $html['isOk'] = 1;
        } else {
            $html['cart_html'] = '';
            $html['isOk'] = 0;
        }

        $html['session'] = $this->session->all_userdata();
        echo json_encode($html);
    }

    public function getCartCookie(){
        $cartId = get_cookie('educki_cartId');
        if($cartId !='' ){
            $data['cartId'] = $cartId;
            $data['ok'] = 1;
        }else{
            $data['cartId'] = '';
            $data['ok'] = 0;
        }
        echo json_encode($data);
    }

     public function getUserCookie(){
        $userId = get_cookie('educki_userId');
        if($cartId !='' ){
            $data['user'] = $userId;
            $data['ok'] = 1;
        }else{
            $data['user'] = '';
            $data['ok'] = 0;
        }
        echo json_encode($data);
    }

    

    public function add_to_cart_ajax() {
        //session_destroy();
        $product_id = $this->input->post('prod_id');
        $filters = stripslashes($this->input->post('filter_data'));
        /*echo "<pre>";
        print_r($filters);
        exit;*/
        $is_cart = $this->session->userdata('isCart');
        $cartId = $this->session->userdata('cartId');
        if($cartId == ''){
            $cartId = get_cookie('educki_cartId');
        }

        
        if($cartId == ''){
            $expires = time() - 500;
            // setcookie('isCart',"FALSE",$expires,$this->data['url'],'/','educki_');
            // setcookie('cartId',0,$expires,$this->data['url'],'/','educki_');
            // setcookie('educki_isCart', "FALSE", $expires, '/');
            // setcookie('educki_cartId', 0, $expires, '/');

            delete_cookie('isCart');
            delete_cookie('cartId');
            delete_cookie('educki_isCart');
            delete_cookie('educki_cartId');
            delete_cookie('educki_userId');
            $this->session->unset_userdata('cartId');
            $this->session->unset_userdata('isCart');
        }


        
        $is_cart = $this->session->userdata('isCart');
        $cartId = $this->session->userdata('cartId');
        if($cartId == ''){
            $cartId = get_cookie('educki_cartId');
        }
        $userId = $this->session->userdata('userid');
        $resp = array();
        if($cartId <= 0){ // If cart ID Not exist.
            if($userId > 0){ // If user Login.
                $cart_data = $this->db->query("SELECT cart_id FROM tbl_cart WHERE cart_user_id = $userId")->row();
                if(count($cart_data) <= 0){ // If not in cart.
                    $this->db->insert('tbl_cart', array('cart_user_id'=>$userId));
                    $cartId = $this->db->insert_id();
                    $this->session->set_userdata('cartId', $cartId);
                }else{ // if cart exist.
                    $cartId = $cart_data->cart_id;
                    $this->session->set_userdata('cartId', $cartId);
                }
                $this->setCartSession($cartId);
            }else{ // If the user is not login.
                $this->load->helper('cookie');
                $this->load->helper('string');
                $cartCookie = get_cookie('educki_isCart');
                if ($cartCookie) {
                    $cartId = get_cookie('educki_cartId');
                    $isCartExist = $this->db->query('SELECT * FROM tbl_cart WHERE cart_id = '.$cartId)->row();
                    if(count($isCartExist) > 0){
                        $this->setCartSession($cartId);
                    }else{
                        $this->cart_model->removeCart($cartId);

                        $user_id = random_string('alnum', 16); // Make the random string as user ID
                        // Add UserID to DB
                        $this->db->insert('tbl_cart', array('cart_user_id'=>$user_id));
                        $cartId = $this->db->insert_id();
                        $this->session->set_userdata('cartId', $cartId);
                        $this->setCartSession($cartId, true, $user_id);
                    }
                }else{
                    $user_id = random_string('alnum', 16); // Make the random string as user ID
                    // Add UserID to DB
                    $this->db->insert('tbl_cart', array('cart_user_id'=>$user_id));
                    $cartId = $this->db->insert_id();
                    $this->session->set_userdata('cartId', $cartId);
                    $this->setCartSession($cartId, true, $user_id);
                }
            }
        }else {

            $cartId = $this->session->userdata('cartId');
        }

        // Prod Reorder Case
        $order_id = $this->input->post('order_id');
        if ($order_id) { // Reorder product
            $this->load->model('front/order/order_model');
            $this->data['order_info'] = $this->order_model->get_order($order_id);
            
            if (count($this->data['order_info']) > 0) {

                foreach ($this->data['order_info'] as $key => $prds) {
                    $prod_ids = $prds['order_prod_id'];
                    $r_inventory = $this->cart_model->getProduct($prod_ids)->result();
                    $r_inventory = cgetproduct_details_2($prod_ids);
                    
                    

                    if ($r_inventory[0]->qty > 0) {
                        if ($r_inventory[0]->qty >= $prds['order_prod_qty']) {
                            $qty = $prds['order_prod_qty'];
                        } else {
                            $qty = $r_inventory[0]->qty;
                        }
                        $getCartTotalQty = $this->db->query("SELECT 
                                                SUM(cart_prod_qnty) as total 
                                                FROM tbl_cart_detail 
                                                WHERE cart_id=$cartId 
                                                AND cart_prod_id=$prod_ids")->row();
                        if( ($getCartTotalQty->total+($qty)) > $r_inventory[0]->qty ){
                            //Not Update
                            $isUpdate= 0;
                            $resp['message'] = "Not added";
                            $resp['cart_items'] = totalCartItems();
                            $resp['cart_sub_total'] = number_format(getCartSubTotal(), 2, '.', '');
                            $resp['status'] = 'limit-ok';

                        }else{
                             //update
                            $isUpdate= 1;
                            $qty = $getCartTotalQty->total+($qty);
                            $stat = $this->cart_model->addreorderProduct($cartId, $prod_ids, $qty, $prds['order_prod_filters']);

                            //var_dump($stat);die;


                            // echo $this->db->last_query();
                            $resp['message'] = "ProductId " . $prod_ids . " is added to cartId " . $cartId;
                            $resp['cart_items'] = totalCartItems();
                            $resp['cart_sub_total'] = number_format(getCartSubTotal(), 2, '.', '');
                            if($stat == 1){
                                $resp['status'] = 'ok';
                            }else{
                                $resp['status'] = 'ready-ok';
                            }
                        }
                    }else{
                        $resp['status'] = 'limit-ok';
                        
                    }
                }
            }else {
                $resp['status'] = 'not-ok';
            }

            $data['cart_product_details'] = getProductPrices();
            //echo $this->db->last_query();
            foreach ($data['cart_product_details'] as $product) {
                $product_details = getproduct_details_2($product->cart_prod_id);
                if ($product_details == '') {
                    $this->cart_model->removeCartItem($cartId, $product->cart_prod_id);
                }
            }
            $resp['cartId'] = $cartId;
            $resp['user_id'] = $user_id;
            return print_r(json_encode($resp));
        } else { // add product into the cart
           
            $data['productData'] = $this->common_model->commonselect('tbl_products', 'prod_id', $product_id);
            if ($data['productData']->num_rows() > 0) {
                
                $row = $data['productData']->row();
                if ($row->qty > 0) {
                    
                    $qty = 1;
                    $data['qtyAdded'] = $qty;
                    
                    $returnType = $this->cart_model->addProduct($cartId, $product_id, $qty);
                    // echo $returnType;
                    $resp['cart_items'] = totalCartItems();
                    $resp['cart_sub_total'] = number_format(getCartSubTotal(), 2, '.', '');
                    //echo $returnType;
                    if($returnType == 1){
                        $resp['status'] = 'ok';
                    }else if($returnType == 2){
                        
                        $resp['status'] = 'ready-ok';
                    }else{
                        $resp['status'] = 'limit-ok';
                    }
                } else {
                    
                    $resp['status'] = 'not-ok';
                }
            }
            
            $data['cart_product_details'] = getProductPrices();
            //echo $this->db->last_query();
            foreach ($data['cart_product_details'] as $product) {
                $product_details = getproduct_details_2($product->cart_prod_id);
                if ($product_details == '') {
                    $this->cart_model->removeCartItem($cartId, $product->cart_prod_id);
                }
            }
            $resp['cartId'] = $cartId;

            print_r(json_encode($resp));
        }
    }



  
    function setCartSession($cartId, $cookieAlso = false, $userId = false) {
        $this->session->set_userdata('isCart', TRUE);
        $this->session->set_userdata('cartId', $cartId);
        $expires = time() + (5400000);
        if ($cookieAlso) {
            $cookie1 = array(
                'name' => 'isCart',
                'value' => TRUE,
                'expire' => $expires,
                'domain' => $this->data['url'],
                'path' => '/',
                'prefix' => 'educki_',
            );
            $cookie2 = array(
                'name' => 'cartId',
                'value' => $cartId,
                'expire' => $expires,
                'domain' => $this->data['url'],
                'path' => '/',
                'prefix' => 'educki_',
            );
            $cookie3 = array(
                'name' => 'userId',
                'value' => $userId,
                'expire' => $expires,
                'domain' => $this->data['url'],
                'path' => '/',
                'prefix' => 'educki_',
            );
            
//            set_cookie($cookie1);
//            set_cookie($cookie2);
//            set_cookie($cookie3);

            // delete_cookie('isCart');
            // delete_cookie('cartId');
            // delete_cookie('userId');
            // delete_cookie('educki_isCart');
            // delete_cookie('educki_cartId');
            // delete_cookie('educki_userId');

            setcookie('isCart',"TRUE",$expires,$this->data['url'],'/','educki_');
            setcookie('cartId',$cartId,$expires,$this->data['url'],'/','educki_');
            setcookie('userId',$userId,$expires,$this->data['url'],'/','educki_');
            setcookie('educki_isCart', "TRUE", $expires, '/');
            setcookie('educki_cartId', $cartId, $expires, '/');
            setcookie('educki_userId', $userId, $expires, '/');
        }
    }
   
}