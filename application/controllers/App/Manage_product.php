<?php

/**
 * - Code by Haseeb
 * - Code for EDucki
 * - Code for add product and filters in front-end.
 */
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Manage_product extends CI_Controller {

    public $storeInfo = '';

    public function __construct() {

        parent::__construct();
        $this->load->model('front/product/product_model');
        $this->load->library('pagination'); // load Pagination library
        $this->load->helper('url');  // load URL helper
        $this->load->library('upload');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    }

    public function index() {

        $data = array();
        $user_id = $this->input->get('userid');
        $data['user_id'] = $user_id;
        $data['store_info'] = $this->common_model->commonselect('tbl_stores', 'user_id', $data['user_id'])->row();
        $data['store_id'] = $data['store_info']->store_id;

        $limit_per_page = 10;

        $page = $this->input->get('page');

        $data['page'] = $page;
        $data['counter'] = ($page * $limit_per_page) + $limit_per_page;

        $data['store_amount_received'] = number_format(store_amount_received($data['store_id']), 2, '.', '');
        $data['store_amount_pending'] = number_format(store_amount_pending($data['store_id']), 2, '.', '');

        $page = $this->input->get('page');
        $data['total_records'] = count($this->product_model->view_product_detail($user_id));
        
        $data['results'] = $this->product_model->get_product_detail($user_id, $limit_per_page, $page * $limit_per_page);

        $products = '';
        
        if (isset($data['results'])){

        foreach ($data['results'] as $product) {
            $prod_image = $this->common_model->commonselect("tbl_product_images", "img_prod_id", $product->prod_id)->row();



                                    $productPrice = 0.0;
                                    $sDate = date('Y-m-d', strtotime($product->sale_start_date));
                                    $eDate = date('Y-m-d', strtotime($product->sale_end_date));
                                    $tDate = date('Y-m-d');

                                    $price = '';
                                    if ($product->prod_onsale == 1) {
                                        if ($product->sale_start_date != '' && $product->sale_end_date != '') {
                                            if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                                                $productPrice = number_format($product->prod_price, 2, '.', '');
                                                $price = '<p  class="prc-ext">$' . number_format($product->sale_price, 2, '.', '') . '</p> 
                                                <p class="prc-cut">$' . number_format($product->prod_price, 2, '.', '') . '</p>';
                                            } else {
                                                $productPrice = number_format($product->prod_price, 2, '.', '');
                                                $price = '<p>$' . number_format($product->prod_price, 2, '.', '') . '</p>';
                                            }
                                        } else if ($product->sale_start_date != '') {
                                            if ($tDate >= $sDate) {
                                                $productPrice = number_format($product->prod_price, 2, '.', '');
                                                $price = '<p  class="prc-ext">$' . number_format($product->sale_price, 2, '.', '') . '</p> <p  class="prc-cut">$' . number_format($product->prod_price, 2, '.', '') . '</p>';
                                            } else {
                                                $productPrice = number_format($product->prod_price, 2, '.', '');
                                                $price = '<p>$' . number_format($product->prod_price, 2, '.', '') . '</p>';
                                            }
                                        } else if ($product->sale_end_date != '') {
                                            if ($tDate <= $eDate) {
                                                $price = '<p  class="prc-ext">$' . number_format($product->sale_price, 2, '.', '') . '</p> <p  class="prc-cut">$' . number_format($product->prod_price, 2, '.', '') . '</p>';
                                            } else {
                                                $price = '<p  class="prc-ext">$' . number_format($product->prod_price, 2, '.', '') . '</p>';
                                            }
                                        } else {
                                            $productPrice = number_format($product->prod_price, 2, '.', '');
                                            $price = '<p  class="prc-ext">$' . number_format($product->sale_price, 2, '.', '') . '</p> <p  class="prc-cut">$' . number_format($product->prod_price, 2, '.', '') . '</p>';
                                        }
                                    } else {
                                        $productPrice = number_format($product->prod_price, 2, '.', '');
                                        $price = '$' . number_format($product->prod_price, 2, '.', '');
                                    }
                                                       
         $shipPrice =($product->shipping == "Charge for Shipping")?number_format($product->ship_price, 2, '.', ''):"00.00";
          $products = $products . "<div class=\"crt-order-frst-rw\">\n" .
                "\t\t\t\t\t\t\t\t\t<div class=\"signed-in-checkbox\">\n" .
                "\t\t\t\t\t\t\t\t\t\t<label>\n" .
                "\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"c1\" value='" . $product->prod_id . "' name=\"c1\" class=\"mycheckbox\">\n" .
                "\t\t\t\t\t\t\t\t\t\t\t<span></span>\n" .
                "\t\t\t\t\t\t\t\t\t\t</label>\n" .
                "\t\t\t\t\t\t\t\t\t</div>\n" .
                "\t                                <div class=\"crt-order-frst-bx\">\n" .
                "\t                                \t<a ".( ($product->qty>0 && $product->prod_status == 1 && $prod_image->img_name != '')?'class="prod_click"':'') ."  data-id='".$product->prod_id."' href='javascript:void(0);'><img  src=\"" . base_url()  . "resources/prod_images/thumb/" . $prod_image->img_name . "\" alt=\"\" title=\"\" class=\"img-responsive\"></a>\n" .
                "\n" .
                "\t                                \t<div class=\"dlt-edt\">\n" .
                "\t                                \t\t<a class=\"twitter_share\" onClick='share($(this));' data-type='tw' data-title='".$product->prod_title."' data-image='".base_url()."resources/prod_images/thumb/".$prod_image->img_name."' data-url='".productDetailUrl($product->prod_id)."/".$product->prod_url."'><img src=\"images/tweet.png\" alt=\"twitter Share\"></a>\n" .
                "\t                                \t\t<a class=\"fb_share\" onClick='share($(this));' data-type='fb' data-title='".$product->prod_title."' data-image='".base_url()."resources/prod_images/thumb/".$prod_image->img_name."' data-url='".productDetailUrl($product->prod_id)."/".$product->prod_url."'><img src=\"images/fb.png\" alt=\"Facebook Share\"></a>\n" .
                "\t                                \t\t<a href=\"javascript:void(0);\" data-id='". $product->prod_id ."' class=\"editProduct\"><img src=\"images/edit.png\" alt=\"Edit\"></a>\n" .
                "\t                                \t\t<a class=\"single_delete\"  data-url='". $product->prod_url ."'><img src=\"images/delete.png\" alt=\"Delete\"></a>\n" .
                "\t                                \t</div>\n" .
                "\t                                </div>\n" .
                "\t                                \n" .
                "\t                                <div class=\"crt-order-frst-bx2\">\n" .
                "\t                                \t<div class=\"crt-order-frst-bx2-rw\">\n" .
                "\t                                    \t<div class=\"crt-order-frst-bx2-rw-left\">\n" .
                "\t                                        \t<h5>ID:</h5>\n" .
                "\t                                        </div>\n" .
                "\t                                        \n" .

                "\t                                        <div class=\"crt-order-frst-bx2-rw-right\">\n" .
                "\t                                        \t<p>". $product->prod_id ."</p>\n" .
                "\t                                        </div>\n" .
                "\t                                    </div>\n" .
                "\t                                    \n" .
                "\t                                    <div class=\"crt-order-frst-bx2-rw\">\n" .
                "\t                                    \t<div class=\"crt-order-frst-bx2-rw-left\">\n" .
                "\t                                        \t<h5>Product Title:</h5>\n" .
                "\t                                        </div>\n" .
                "\t                                        \n" .
                "\t                                        <div class=\"crt-order-frst-bx2-rw-right\">\n" .
                "\t                                        \t<p><a ".( ($product->qty>0 && $product->prod_status == 1 && $prod_image->img_name != '')?'class="prod_click"':'') ."  data-id='".$product->prod_id."' href='javascript:void(0);'>" . $product->prod_title . "</a></p>\n" .
                "\t                                        </div>\n" .
                "\t                                    </div>\n" .
                "\t                                    \n" .
                "\t                                    <div class=\"crt-order-frst-bx2-rw\">\n" .
                "\t                                    \t<div class=\"crt-order-frst-bx2-rw-left\">\n" .
                "\t                                        \t<h5>Price:</h5>\n" .
                "\t                                        </div>\n" .
                "\t                                        \n" .
                "\t                                        <div class=\"crt-order-frst-bx2-rw-right\">\n" .
                "\t                                        " . $price .
                "\t                                        </div>\n" .
                "\t                                    </div>\n" .
                "\t                                    \n" .
                "\t                                    <div class=\"crt-order-frst-bx2-rw\">\n" .
                "\t                                    \t<div class=\"crt-order-frst-bx2-rw-left\">\n" .
                "\t                                        \t<h5>Shipping:</h5>\n" .
                "\t                                        </div>\n" .
                "\t                                        \n" .
                "\t                                        <div class=\"crt-order-frst-bx2-rw-right\">\n" .
                "\t                                        \t<p>$" . $shipPrice  . "</p>\n" .
                "\t                                        </div>\n" .
                "\t                                    </div>\n" .
                "\t                                    \n" .
                "\t                                    <div class=\"crt-order-frst-bx2-rw\">\n" .
                "\t                                    \t<div class=\"crt-order-frst-bx2-rw-left\">\n" .
                "\t                                        \t<h5>Qty:</h5>\n" .
                "\t                                        </div>\n" .
                "\t                                        \n" .
                "\t                                        <div class=\"crt-order-frst-bx2-rw-right\">\n" .
                "\t                                        \t<p>" . $product->qty ."</p>\n" .
                "\t                                        </div>\n" .
                "\t                                    </div>\n" .
                "\t                                    \n" .
                "\t                                    <div class=\"crt-order-frst-bx2-rw\">\n" .
                "\t                                    \t<div class=\"crt-order-frst-bx2-rw-left\">\n" .
                "\t                                        \t<h5>Status:</h5>\n" .
                "\t                                        </div>\n" .
                "\t                                        \n" .
                "\t                                        <div class=\"crt-order-frst-bx2-rw-right\">\n" .
                "\t                                        \t<p>" . (($product->prod_status) == '1' ? "Active" : "Inactive") . "</p>\n" .
                "\t                                        </div>\n".
                "\t                                    </div>\n" .
                "\t                                \n" .
                "\t                                </div>\n" .
                "\t                                \n" .
                "\t                            </div>";
        }


        }

        $data['results'] = $products;


   //     $this->load->view("front/custom_template", $data);
       echo  json_encode($data);
    }

    public function search_product() {
        $data = array();
        $user_id = $this->input->post('userid');
        $data['user_id'] = $user_id;
        $data['search_term'] = $this->input->post('select_search');
        $data['user_id'] = $user_id;
        $data['store_info'] = $this->common_model->commonselect('tbl_stores', 'user_id', $data['user_id'])->row();

        $data['store_id'] = $data['store_info']->store_id;

        $limit_per_page = 10;

        $page = $this->input->post('page');

        $data['counter'] = ($page * $limit_per_page) + $limit_per_page;

        $data['total_records'] = count($this->product_model->search_product($user_id, $to = 0, $from = '', $is_limit = 0));
        
        $data['results'] = $this->product_model->search_product($user_id, $limit_per_page, $page * $limit_per_page, 1);

        $products = '';

        if (isset($data['results'])){

        foreach ($data['results'] as $product) {
            $prod_image = $this->common_model->commonselect("tbl_product_images", "img_prod_id", $product->prod_id)->row();



                                    $productPrice = 0.0;
                                    $sDate = date('Y-m-d', strtotime($product->sale_start_date));
                                    $eDate = date('Y-m-d', strtotime($product->sale_end_date));
                                    $tDate = date('Y-m-d');

                                    $price = '';
                                    if ($product->prod_onsale == 1) {
                                        if ($product->sale_start_date != '' && $product->sale_end_date != '') {
                                            if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                                                $productPrice = number_format($product->prod_price, 2, '.', '');
                                                $price = '<p  class="prc-ext">$' . number_format($product->sale_price, 2, '.', '') . '</p> 
                                                <p class="prc-cut">$' . number_format($product->prod_price, 2, '.', '') . '</p>';
                                            } else {
                                                $productPrice = number_format($product->prod_price, 2, '.', '');
                                                $price = '<p>$' . number_format($product->prod_price, 2, '.', '') . '</p>';
                                            }
                                        } else if ($product->sale_start_date != '') {
                                            if ($tDate >= $sDate) {
                                                $productPrice = number_format($product->prod_price, 2, '.', '');
                                                $price = '<p  class="prc-ext">$' . number_format($product->sale_price, 2, '.', '') . '</p> <p  class="prc-cut">$' . number_format($product->prod_price, 2, '.', '') . '</p>';
                                            } else {
                                                $productPrice = number_format($product->prod_price, 2, '.', '');
                                                $price = '<p>$' . number_format($product->prod_price, 2, '.', '') . '</p>';
                                            }
                                        } else if ($product->sale_end_date != '') {
                                            if ($tDate <= $eDate) {
                                                $price = '<p  class="prc-ext">$' . number_format($product->sale_price, 2, '.', '') . '</p> <p  class="prc-cut">$' . number_format($product->prod_price, 2, '.', '') . '</p>';
                                            } else {
                                                $price = '<p  class="prc-ext">$' . number_format($product->prod_price, 2, '.', '') . '</p>';
                                            }
                                        } else {
                                            $productPrice = number_format($product->prod_price, 2, '.', '');
                                            $price = '<p  class="prc-ext">$' . number_format($product->sale_price, 2, '.', '') . '</p> <p  class="prc-cut">$' . number_format($product->prod_price, 2, '.', '') . '</p>';
                                        }
                                    } else {
                                        $productPrice = number_format($product->prod_price, 2, '.', '');
                                        $price = '$' . number_format($product->prod_price, 2, '.', '');
                                    }
                                                       
         $shipPrice =($product->shipping == "Charge for Shipping")?number_format($product->ship_price, 2, '.', ''):"00.00";
          $products = $products . "<div class=\"crt-order-frst-rw\">\n" .
                "\t\t\t\t\t\t\t\t\t<div class=\"signed-in-checkbox\">\n" .
                "\t\t\t\t\t\t\t\t\t\t<label>\n" .
                "\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"c1\" value='" . $product->prod_id . "' name=\"c1\" class=\"mycheckbox\">\n" .
                "\t\t\t\t\t\t\t\t\t\t\t<span></span>\n" .
                "\t\t\t\t\t\t\t\t\t\t</label>\n" .
                "\t\t\t\t\t\t\t\t\t</div>\n" .
                "\t                                <div class=\"crt-order-frst-bx\">\n" .
                "\t                                \t<img  src=\"" . base_url()  . "resources/prod_images/thumb/" . $prod_image->img_name . "\" alt=\"\" title=\"\" class=\"img-responsive\">\n" .
                "\n" .
                "\t                                \t<div class=\"dlt-edt\">\n" .
                "\t                                \t\t<a href=\"javascript:void\" class=\"editProduct\" data-id=\"$product->prod_id\"><img src=\"images/edit.png\" alt=\"Edit\"></a>\n" .
                "\t                                \t\t<a  class=\"single_delete\" data-url='". $product->prod_url ."' ><img src=\"images/delete.png\" alt=\"Delete\"></a>\n" .
                "\t                                \t</div>\n" .
                "\t                                </div>\n" .
                "\t                                \n" .
                "\t                                <div class=\"crt-order-frst-bx2\">\n" .
                "\t                                \t<div class=\"crt-order-frst-bx2-rw\">\n" .
                "\t                                    \t<div class=\"crt-order-frst-bx2-rw-left\">\n" .
                "\t                                        \t<h5>ID:</h5>\n" .
                "\t                                        </div>\n" .
                "\t                                        \n" .

                "\t                                        <div class=\"crt-order-frst-bx2-rw-right\">\n" .
                "\t                                        \t<p>". $product->prod_id ."</p>\n" .
                "\t                                        </div>\n" .
                "\t                                    </div>\n" .
                "\t                                    \n" .
                "\t                                    <div class=\"crt-order-frst-bx2-rw\">\n" .
                "\t                                    \t<div class=\"crt-order-frst-bx2-rw-left\">\n" .
                "\t                                        \t<h5>Product Title:</h5>\n" .
                "\t                                        </div>\n" .
                "\t                                        \n" .
                "\t                                        <div class=\"crt-order-frst-bx2-rw-right\">\n" .
                "\t                                        \t<p><a ".( ($product->qty>0 && $product->prod_status == 1 && $prod_image->img_name != '')?'class="prod_click"':'') ."  data-id='".$product->prod_id."' href='javascript:void(0);'>" . $product->prod_title . "</a></p>\n" .
                "\t                                        </div>\n" .
                "\t                                    </div>\n" .
                "\t                                    \n" .
                "\t                                    <div class=\"crt-order-frst-bx2-rw\">\n" .
                "\t                                    \t<div class=\"crt-order-frst-bx2-rw-left\">\n" .
                "\t                                        \t<h5>Price:</h5>\n" .
                "\t                                        </div>\n" .
                "\t                                        \n" .
                "\t                                        <div class=\"crt-order-frst-bx2-rw-right\">\n" .
                "\t                                        " . $price .
                "\t                                        </div>\n" .
                "\t                                    </div>\n" .
                "\t                                    \n" .
                "\t                                    <div class=\"crt-order-frst-bx2-rw\">\n" .
                "\t                                    \t<div class=\"crt-order-frst-bx2-rw-left\">\n" .
                "\t                                        \t<h5>Shipping:</h5>\n" .
                "\t                                        </div>\n" .
                "\t                                        \n" .
                "\t                                        <div class=\"crt-order-frst-bx2-rw-right\">\n" .
                "\t                                        \t<p>$" . $shipPrice  . "</p>\n" .
                "\t                                        </div>\n" .
                "\t                                    </div>\n" .
                "\t                                    \n" .
                "\t                                    <div class=\"crt-order-frst-bx2-rw\">\n" .
                "\t                                    \t<div class=\"crt-order-frst-bx2-rw-left\">\n" .
                "\t                                        \t<h5>Qty:</h5>\n" .
                "\t                                        </div>\n" .
                "\t                                        \n" .
                "\t                                        <div class=\"crt-order-frst-bx2-rw-right\">\n" .
                "\t                                        \t<p>" . $product->qty ."</p>\n" .
                "\t                                        </div>\n" .
                "\t                                    </div>\n" .
                "\t                                    \n" .
                "\t                                    <div class=\"crt-order-frst-bx2-rw\">\n" .
                "\t                                    \t<div class=\"crt-order-frst-bx2-rw-left\">\n" .
                "\t                                        \t<h5>Status:</h5>\n" .
                "\t                                        </div>\n" .
                "\t                                        \n" .
                "\t                                        <div class=\"crt-order-frst-bx2-rw-right\">\n" .
                "\t                                        \t<p>" . (($product->prod_status) == '1' ? "Active" : "Inactive") . "</p>\n" .
                "\t                                        </div>\n".
                "\t                                    </div>\n" .
                "\t                                \n" .
                "\t                                </div>\n" .
                "\t                                \n" .
                "\t                            </div>";
        }


        }


        if(sizeof($data['results'])==0){
            $data['results'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No data found.</div>";
        }else{
            $data['results'] = $products;
        }

         echo  json_encode($data);
    }


     public function product_delete($slug) {

        $data['title'] = "Manage Products";
        $data['content_view'] = "products/manage-product";
        $array['prod_is_delete'] = 1;
        $nId = $this->product_model->product_delete($slug);
        $this->common_model->commonUpdate('tbl_products', $array, "prod_id", $nId);
        $this->common_model->commonDelete('tbl_wish_list', "product_id", $nId);
        

         echo  json_encode("Item deleted successfully.");
       
    }

    public function viewNewProduct() {

        $user_id = $this->input->get('userid');

        $this->storeInfo = $this->common_model->commonselect('tbl_stores', 'user_id',  $user_id)->row();
       
        $data['title'] = "Edit Product";
       
        $data['mainCat'] = $this->db->query("
                            SELECT *
                            FROM tbl_categories
                            WHERE cat_parent_id = 0 AND cat_status = 1 AND cat_is_delete = 0
                        ")->result();
        
        $data['content_view'] = "products/add-new-product";
            //Haseeb Code
            $data['TireInfo'] = $this->common_model->commonselect('tbl_tier_list', 'tier_id', $this->storeInfo->store_tier_id)->row();

            // Mehmood Code
            $data['products'] = $this->common_model->commonselect2('tbl_products', 'prod_store_id', $this->storeInfo->store_id, 'prod_is_delete', 0)->result();
             // echo "<pre>";
              echo json_encode($data);
              exit; 
            

            
            }

    public function upload_prodout_image()
    {
        sleep(1);
        $ext = explode(".",urldecode($_FILES["file"]["name"]));
        $ext = $ext[count($ext)-1];
        $new_image_name = 'img_'. date('Y_m_d_h_i_s').'.'.$ext;
        $source_image = $_SERVER['DOCUMENT_ROOT'] . '/resources/prod_images/'.str_replace('"',"",$new_image_name);
        $test = move_uploaded_file($_FILES["file"]["tmp_name"],$source_image);
        if($test){
            chmod($source_image, 0755);
            $this->load->library('image_lib');
            $configt['image_library'] = 'gd2';
            $configt['source_image'] = $source_image;
            $configt['new_image'] = $_SERVER['DOCUMENT_ROOT'] . '/resources/prod_images/thumb/';
            $configt['maintain_ratio'] = true;
            $configt['width'] = 80;
            $configt['height'] = 60;
            $this->image_lib->initialize($configt);
            $this->image_lib->resize();
        }
        echo json_encode(str_replace('"','',$new_image_name));
    }




    public function addNewProduct(){
       
        $data['title'] = "Create New Product";
        $user_id = $this->input->get('userid');

        $this->storeInfo = $this->common_model->commonselect('tbl_stores', 'user_id',  $user_id)->row();

        if(!empty($this->storeInfo)){

            $data['mainCat'] = $this->db->query("
                            SELECT *
                            FROM tbl_categories
                            WHERE cat_parent_id = 0 AND cat_status = 1 AND cat_is_delete = 0
                        ")->result();
   


            $prodData['looking_for'] = $this->input->get('looking_for');
            $prodData['condition'] = $this->input->get('condition');
            $prodData['qty'] = $this->input->get('qty');
            $prodData['prod_title'] = $this->input->get('prod_title');
            $prodData['prod_url'] = $this->common_model->create_slug($prodData['prod_title'], 'tbl_categories', 'cat_url');
            $prodData['prod_url'] = $this->common_model->create_slug($prodData['prod_url'], 'tbl_products', 'prod_url');
            $prodData['prod_description'] = $this->input->get('prod_description');
            $prodData['youtube_links'] = $this->input->get('youtube_links');
            $prodData['prod_price'] = number_format($this->input->get('prod_price'), 2, '.', '');
            $prodData['prod_onsale'] = $this->input->get('is_onsale');
            if ($prodData['prod_onsale'] == 1) {
                $prodData['sale_price'] = number_format($this->input->get('sale_price'), 2, '.', '');
                $prodData['sale_start_date'] = '';
                $prodData['sale_end_date'] = '';
                if ($this->input->get('sale_start_date') != '') {
                    $prodData['sale_start_date'] = date('Y-m-d', strtotime(str_replace('-','/',$this->input->get('sale_start_date') ) ) );
                }
                if ($this->input->get('sale_start_date') != '') {
                    $prodData['sale_end_date'] = date('Y-m-d', strtotime(str_replace('-','/',$this->input->get('sale_end_date') ) ) );
                }
            }
            $prodData['shipping'] = $this->input->get('shipping');
            $prodData['ship_price'] = 0;
            if ($prodData['shipping'] == "Charge for Shipping") {
                $prodData['ship_price'] = $this->input->get('ship_price');
                $prodData['ship_days'] = $this->input->get('ship_days');
            } else if ($prodData['shipping'] == "Offer free Shipping") {
                $prodData['free_ship_days'] = $this->input->get('free_ship_days');
            }
            $prodData['prod_return'] = $this->input->get('prod_return');

            $prodData['prod_cat_id'] = $this->input->get('mainCat');
            if ($this->input->get('sub_sub_cat') != '') {
                $prodData['prod_cat_id'] = $this->input->get('sub_sub_cat');
            } else if ($this->input->get('sub_cat') != '') {
                $prodData['prod_cat_id'] = $this->input->get('sub_cat');
            }
            $prodData['prod_created_date'] = date('Y-m-d H:i:s');
            $prodData['prod_shipping_methods'] = $this->input->get('shipping_method');
            $prodData['prod_store_id'] = $this->input->get('sub_cat');
            $prodData['prod_user_id'] = $this->input->get('userid');

            $prodData['prod_store_id'] = $this->storeInfo->store_id;
            $prodData['is_unique_filter'] = $this->input->get('is_unique');
            if ($this->input->get('prod_status') == 1) {
                $prodData['prod_status'] = 1;
            } else {
                $prodData['prod_status'] = 0;
            }
            
            $userInfo = $this->common_model->commonselect('tbl_user', 'user_id',$user_id)->row();

            if($userInfo->user_is_featured==1){
                $prodData['prod_feature'] = 1;
            }


            // Insert Product Data
            $prod_id = $this->common_model->commonSave('tbl_products', $prodData);

            $catData['category_id'] = $this->input->get('mainCat');
            $catData['sub_category_id'] = $this->input->get('sub_cat');
            $catData['sub_sub_category_id'] = $this->input->get('sub_sub_cat');
            $catData['prod_id'] = $prod_id;


            // Insert Categories
            $prod_cat_id = $this->common_model->commonSave('tbl_product_category', $catData);

            $name = $this->input->get('img_name');
            // $label = $this->input->get('img_label');
            for ($i = 0; $i < count($this->input->get('img_name')); $i++) {
                $imgData['img_name'] = trim($name[$i],'"');
                // $imgData['img_label'] = $label[$i];
                $imgData['img_prod_id'] = $prod_id;
                $this->common_model->commonSave('tbl_product_images', $imgData);
            }

            // Insert Filters Data in the case of Unique Filter
            $is_unique = $this->input->get('is_unique'); // 1 for unique and 0 for multiple

            if ($is_unique == 1) {
                // Add unique filters
                $sub_sub_cat_id = $this->input->get('sub_sub_cat');
                if ($this->input->get('sub_sub_cat') != '') {
                    $filters = $this->common_model->commonselect('tbl_cat_filter_title', 'filter_cat_id', $sub_sub_cat_id)->result();
                    foreach ($filters as $row) {
                        if ($this->input->get($row->filter_slug)) {
                            // If value post is not empty
                            $filtersData['prod_id'] = $prod_id;
                            $filtersData['category_id'] = $sub_sub_cat_id;
                            $filtersData['filter_id'] = $row->filter_title_id;
                            $filtersData['filter_slug'] = $row->filter_slug;
                            $filtersData['filter_value'] = $this->input->get($row->filter_slug);
                            $filtersData['is_unique'] = 1;
                            $filterId = $this->common_model->commonSave('tbl_product_filters', $filtersData);

                            // If this filter is conditional get the detail value and save it.
                            $filter_dtil = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_id', $this->input->get($row->filter_slug))->row();
                            if ($row->cat_filter_is_conditional == 1 && $this->input->get($filter_dtil->filter_slug) != '') {
                                $filtersDetailData['prod_filter_id'] = $filterId;
                                $filtersDetailData['prod_id'] = $prod_id;
                                $filtersDetailData['category_id'] = $sub_sub_cat_id;
                                $filtersDetailData['filter_id'] = $row->filter_title_id;
                                $filtersDetailData['filter_detail_id'] = $this->input->get($row->filter_slug);
                                $filtersDetailData['filter_slug'] = $filter_dtil->filter_slug;
                                $filtersDetailData['filter_value'] = $this->input->get($filter_dtil->filter_slug);
                                $filtersDetailData['filter_is_unique'] = 1;
                                $filterDetailId = $this->common_model->commonSave('tbl_product_filters_detail', $filtersDetailData);
                            }
                        }
                    }
                }
            } else {
                // Add multiple filters.
                $sub_sub_cat_id = $this->input->get('sub_sub_cat');
                if ($this->input->get('sub_sub_cat') != '') {
                    $filters = $this->common_model->commonselect('tbl_cat_filter_title', 'filter_cat_id', $sub_sub_cat_id)->result();
                    foreach ($filters as $row) {
                        $filtersData['prod_id'] = $prod_id;
                        $filtersData['category_id'] = $sub_sub_cat_id;
                        $filtersData['filter_id'] = $row->filter_title_id;
                        $filtersData['filter_slug'] = $row->filter_slug;
                        //$filtersData['filter_value'] = $this->input->post($row->filter_slug);
                        $filtersData['is_unique'] = 0;
                        $fil_value = $is_multi = '';

                        if ($row->is_required == 1) {
                            $fil_value = $this->input->get('multi_' . $row->filter_slug);
                            // echo 'if: '.$row->filter_slug.' : '.($fil_value=='').'<br/>';
                            // $is_multi = 1;
                        } else {
                            $fil_value = $this->input->get($row->filter_slug);
                            if ($fil_value != '') {
                                $fil_value = (array) $fil_value;
                            } else {
                                $fil_value = array();
                            }
                            // echo 'else: '.$row->filter_slug.' : '.($fil_value=='').'<br/>';
                            // $is_multi = 0;
                        }

                        if (count($fil_value) > 0) {
                            if (!is_array($fil_value)) {
                                $fil_value = (array) $fil_value;
                            }
                            foreach ($fil_value as $filVal) {
                                $filtersData['filter_value'] = $filVal;
                                $filterId = $this->common_model->commonSave('tbl_product_filters', $filtersData);
                                // If this filter is conditional get the detail value and save it.
                                $filter_dtil = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_id', $filVal)->row();
                                if ($row->cat_filter_is_conditional == 1) {
                                    $filtersDetailData['prod_filter_id'] = $filterId;
                                    $filtersDetailData['prod_id'] = $prod_id;
                                    $filtersDetailData['category_id'] = $sub_sub_cat_id;
                                    $filtersDetailData['filter_id'] = $row->filter_title_id;
                                    $filtersDetailData['filter_slug'] = $filter_dtil->filter_slug;
                                    $filtersDetailData['filter_is_unique'] = 0;

                                    if ($row->is_required == 1) {
                                        $fil_detail_val = $this->input->get('multi_' . $filter_dtil->filter_slug);
                                    } else {
                                        $fil_detail_val = $this->input->get($filter_dtil->filter_slug);
                                    }

                                    if (is_array($fil_detail_val)) {
                                        foreach ($fil_detail_val as $fil_del_val) {
                                            $filtersDetailData['filter_detail_id'] = $filVal;
                                            $filtersDetailData['filter_value'] = $fil_del_val;
                                            $filterDetailId = $this->common_model->commonSave('tbl_product_filters_detail', $filtersDetailData);
                                        }
                                    }else{
                                        $filtersDetailData['filter_detail_id'] = $filVal;
                                        $filtersDetailData['filter_value'] = $fil_detail_val;
                                        $filterDetailId = $this->common_model->commonSave('tbl_product_filters_detail', $filtersDetailData);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $return['status'] = 200;
            $return['message'] = "Product added successfully.";
            
            $prodCat = $this->common_model->commonselect("tbl_categories", "cat_id", $prodData['prod_cat_id'])->result();
            $proCatSlug = $prodCat[0]->cat_url;
            
            $prod_link  = "<a href='".base_url($proCatSlug.'/'.$prodData['prod_url'])."'>$prodData[prod_title]</a>";
            $store_link = "<a href='".base_url('store/'.$this->storeInfo->store_url)."'>".$this->storeInfo->store_name."</a>";
            
            $message = "New product $prod_link is uploaded to store $store_link";
      
            $result = send_email(getSiteSettings('site_email'), "noreply@educki.com", "New Product Uploaded", $message);
            //$result = send_email(getSiteSettings('site_email'), "no-reply@educki.com", "New Product Uploaded", $message);

            if($result){
                $return['status'] = 200;
                $return['message'] = "Product added successfully.";
            }else{
                $return['status'] = 201;
                $return['message'] = "unable to send email.";
            }
        }else{
            $return['status'] = 202;
            $return['message'] = "There was a problem.";
        }
       
        echo json_encode($return); 
    }

    public function editProduct($prod_id = '') {

         $user_id = $this->input->get('userid');

        $this->storeInfo = $this->common_model->commonselect('tbl_stores', 'user_id',  $user_id)->row();

        if(!empty($this->storeInfo)){

            $data['prodData'] = $this->common_model->commonselect3('tbl_products', 'prod_id', $prod_id, 'prod_store_id', $this->storeInfo->store_id, 'prod_is_delete', 0)->row();
            if (empty($data['prodData'])) {
                $return['status'] = 202;
                $return['message'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No data found.</div>";
            }else{
                $data['title'] = "Edit Product";
                $data['mainCat'] = $this->db->query("
                                    SELECT *
                                    FROM tbl_categories
                                    WHERE cat_parent_id = 0 AND cat_status = 1 AND cat_is_delete = 0
                                ")->result();
                $data['prodCat'] = $this->common_model->commonselect('tbl_product_category', 'prod_id', $data['prodData']->prod_id)->row();
                $data['prodImg'] = $this->common_model->commonselect('tbl_product_images', 'img_prod_id', $data['prodData']->prod_id)->result();
                
                

                    $prodData['looking_for'] = $this->input->get('looking_for');
                    $prodData['condition'] = $this->input->get('condition');
                    $prodData['qty'] = $this->input->get('qty');
                    $prodData['prod_title'] = $this->input->get('prod_title');
                    $prodData['prod_url'] = $this->common_model->create_slug($prodData['prod_title'], 'tbl_categories', 'cat_url');
                    $prodData['prod_url'] = $this->common_model->create_slug($prodData['prod_url'], 'tbl_products', 'prod_url', 'prod_id', $data['prodData']->prod_id);
                    //$prodData['prod_sku'] = $this->input->post('prod_sku');
                    $prodData['prod_description'] = $this->input->get('prod_description');
                    $prodData['youtube_links'] = $this->input->get('youtube_links');
                    $prodData['prod_price'] = number_format($this->input->get('prod_price'), 2, '.', '');
                    $prodData['prod_onsale'] = $this->input->get('is_onsale');
                    $prodData['sale_price'] = NULL;
                    $prodData['sale_start_date'] = '';
                    $prodData['sale_end_date'] = '';
                    if ($prodData['prod_onsale'] == 1) {
                        $prodData['sale_price'] = number_format($this->input->get('sale_price'), 2, '.', '');
                        $prodData['sale_start_date'] = '';
                        $prodData['sale_end_date'] = '';
                        if ($this->input->get('sale_start_date') != '') {
                            $prodData['sale_start_date'] = date('Y-m-d', strtotime(str_replace('-','/',$this->input->get('sale_start_date') ) ) );
                        }
                        if ($this->input->get('sale_start_date') != '') {
                            $prodData['sale_end_date'] = date('Y-m-d', strtotime(str_replace('-','/',$this->input->get('sale_end_date') ) ) );
                        }
                    }
                        
                    $prodData['shipping'] = $this->input->get('shipping');
                    $prodData['ship_price'] = '0';
                    $prodData['ship_days'] = '0';
                    if ($prodData['shipping'] == "Charge for Shipping") {
                        $prodData['ship_price'] = $this->input->get('ship_price');
                        $prodData['ship_days'] = $this->input->get('ship_days');
                    } else if ($prodData['shipping'] == "Offer free Shipping") {
                        $prodData['free_ship_days'] = $this->input->get('free_ship_days');
                        $prodData['ship_price'] = 0;
                        $prodData['ship_days'] = 0;

                    }
                    $prodData['prod_return'] = $this->input->get('prod_return');
                    $prodData['prod_shipping_methods'] = $this->input->get('shipping_method');
                    $prodData['prod_cat_id'] = $this->input->get('mainCat');
                    if ($this->input->get('sub_sub_cat') != '') {
                        $prodData['prod_cat_id'] = $this->input->get('sub_sub_cat');
                    } else if ($this->input->get('sub_cat') != '') {
                        $prodData['prod_cat_id'] = $this->input->get('sub_cat');
                    }
                    $prodData['prod_created_date'] = date('Y-m-d H:i:s');
                    $prodData['prod_store_id'] = $this->input->get('sub_cat');
                    $prodData['prod_user_id'] = $this->input->get('userid');
                    $prodData['prod_store_id'] = $this->storeInfo->store_id;
                    // Insert Product Data
                    $prod_id = $data['prodData']->prod_id;
                    $prodData['is_unique_filter'] = $this->input->get('is_unique');
                    if ($this->input->get('list_items_value') == 1) {
                        $prodData['prod_status'] = 1;
                    } else {
                        $prodData['prod_status'] = 0;
                    }
                    
                    $userInfo = $this->common_model->commonselect('tbl_user', 'user_id', $user_id)->row();

                    if($userInfo->user_is_featured==1){
                        $prodData['prod_feature'] = 1;
                    }

                    $this->common_model->commonUpdate('tbl_products', $prodData, 'prod_id', $prod_id);


                    $catData['category_id'] = $this->input->get('mainCat');
                    $catData['sub_category_id'] = $this->input->get('sub_cat');
                    $catData['sub_sub_category_id'] = $this->input->get('sub_sub_cat');
                    $catData['prod_id'] = $prod_id;

                    // Insert Categories
                    $prod_cat_id = $data['prodCat']->prod_cat_id;
                    $this->common_model->commonUpdate('tbl_product_category', $catData, 'prod_id', $prod_id);

                    $this->common_model->commonDelete('tbl_product_images', 'img_prod_id', $prod_id);
                    foreach ($data['prodImg'] as $img) {
                        if (!in_array($img->img_name, $this->input->get('img_name'))) {
                            unlink(FCPATH . 'resources/prod_images/' . $img->img_name);
                            //unlink(FCPATH . 'resources/prod_images/thumb/' . $img->img_name);
                        }
                    }
                    $this->common_model->commonDelete('tbl_product_filters', 'prod_id', $prod_id);
                    $this->common_model->commonDelete('tbl_product_filters_detail', 'prod_id', $prod_id);

                    // Insert image Data
                    $name = $this->input->get('img_name');
                    // $label = $this->input->get('img_label');
                    // for ($i = 0; $i < count($this->input->get('img_name')); $i++) {
                    //     $imgData['img_name'] = $name[$i];
                    //     // $imgData['img_label'] = $label[$i];
                    //     $imgData['img_prod_id'] = $prod_id;
                    //     $this->common_model->commonSave('tbl_product_images', $imgData);
                    // }
                    foreach ($name as $img) {
                      $imgData['img_name'] = $img;
                      $imgData['img_prod_id'] = $prod_id;
                      $this->common_model->commonSave('tbl_product_images', $imgData);
                    }
                    
                    $return['images'] = $name;

                    // Insert Filters Data in the case of Unique Filter
                    $is_unique = $this->input->get('is_unique'); // 1 for unique and 0 for multiple
                    if ($is_unique == 1) {
                        // Add unique filters
                        $sub_sub_cat_id = $this->input->get('sub_sub_cat');
                        if ($this->input->get('sub_sub_cat') != '') {
                            $filters = $this->common_model->commonselect('tbl_cat_filter_title', 'filter_cat_id', $sub_sub_cat_id)->result();
                            foreach ($filters as $row) {
                                if ($this->input->get($row->filter_slug)) {
                                    // If value post is not empty
                                    $filtersData['prod_id'] = $prod_id;
                                    $filtersData['category_id'] = $sub_sub_cat_id;
                                    $filtersData['filter_id'] = $row->filter_title_id;
                                    $filtersData['filter_slug'] = $row->filter_slug;
                                    $filtersData['filter_value'] = $this->input->get($row->filter_slug);
                                    $filtersData['is_unique'] = 1;
                                    $filterId = $this->common_model->commonSave('tbl_product_filters', $filtersData);

                                    // If this filter is conditional get the detail value and save it.
                                    $filter_dtil = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_id', $this->input->get($row->filter_slug))->row();
                                    if ($row->cat_filter_is_conditional == 1 && $this->input->get($filter_dtil->filter_slug) != '') {
                                        $filtersDetailData['prod_filter_id'] = $filterId;
                                        $filtersDetailData['prod_id'] = $prod_id;
                                        $filtersDetailData['category_id'] = $sub_sub_cat_id;
                                        $filtersDetailData['filter_id'] = $row->filter_title_id;
                                        $filtersDetailData['filter_detail_id'] = $this->input->get($row->filter_slug);
                                        $filtersDetailData['filter_slug'] = $filter_dtil->filter_slug;
                                        $filtersDetailData['filter_value'] = $this->input->get($filter_dtil->filter_slug);
                                        $filtersDetailData['filter_is_unique'] = 1;
                                        $filterDetailId = $this->common_model->commonSave('tbl_product_filters_detail', $filtersDetailData);
                                    }
                                }
                            }
                        }
                    } else {
                        // Add multiple filters.
                        $sub_sub_cat_id = $this->input->get('sub_sub_cat');
                        if ($this->input->get('sub_sub_cat') != '') {
                            $filters = $this->common_model->commonselect('tbl_cat_filter_title', 'filter_cat_id', $sub_sub_cat_id)->result();
                            foreach ($filters as $row) {
                                $filtersData['prod_id'] = $prod_id;
                                $filtersData['category_id'] = $sub_sub_cat_id;
                                $filtersData['filter_id'] = $row->filter_title_id;
                                $filtersData['filter_slug'] = $row->filter_slug;
                                //$filtersData['filter_value'] = $this->input->post($row->filter_slug);
                                $filtersData['is_unique'] = 0;
                                $fil_value = $is_multi = '';

                                if ($row->is_required == 1) {
                                    $fil_value = $this->input->get('multi_' . $row->filter_slug);
                                    // echo 'if: '.$row->filter_slug.' : '.($fil_value=='').'<br/>';
                                    // $is_multi = 1;
                                } else {
                                    $fil_value = $this->input->get($row->filter_slug);
                                    if ($fil_value != '') {
                                        $fil_value = (array) $fil_value;
                                    } else {
                                        $fil_value = array();
                                    }
                                    // echo 'else: '.$row->filter_slug.' : '.($fil_value=='').'<br/>';
                                    // $is_multi = 0;
                                }

                                if (count($fil_value) > 0) {
                                    if (!is_array($fil_value)) {
                                        $fil_value = (array) $fil_value;
                                    }
                                    foreach ($fil_value as $filVal) {
                                        $filtersData['filter_value'] = $filVal;
                                        $filterId = $this->common_model->commonSave('tbl_product_filters', $filtersData);
                                        // If this filter is conditional get the detail value and save it.
                                        $filter_dtil = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_id', $filVal)->row();
                                        if ($row->cat_filter_is_conditional == 1) {
                                            $filtersDetailData['prod_filter_id'] = $filterId;
                                            $filtersDetailData['prod_id'] = $prod_id;
                                            $filtersDetailData['category_id'] = $sub_sub_cat_id;
                                            $filtersDetailData['filter_id'] = $row->filter_title_id;
                                            $filtersDetailData['filter_slug'] = $filter_dtil->filter_slug;
                                            $filtersDetailData['filter_is_unique'] = 0;

                                            if ($row->is_required == 1) {
                                                $fil_detail_val = $this->input->get('multi_' . $filter_dtil->filter_slug);
                                            } else {
                                                $fil_detail_val = $this->input->get($filter_dtil->filter_slug);
                                            }

                                            if (is_array($fil_detail_val)) {
                                                foreach ($fil_detail_val as $fil_del_val) {
                                                    $filtersDetailData['filter_detail_id'] = $filVal;
                                                    $filtersDetailData['filter_value'] = $fil_del_val;
                                                    $filterDetailId = $this->common_model->commonSave('tbl_product_filters_detail', $filtersDetailData);
                                                }
                                            }else{
                                                $filtersDetailData['filter_detail_id'] = $filVal;
                                                $filtersDetailData['filter_value'] = $fil_detail_val;
                                                $filterDetailId = $this->common_model->commonSave('tbl_product_filters_detail', $filtersDetailData);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                $return['status'] = 200;
                $return['message'] = "Product updated successfully.";
            }
        }else{
            $return['status'] = 202;
            $return['message'] = "There was a problem.";
        }
        echo json_encode($return);
    }




    public function get_sub_category() {
        $main_cat_id = $this->input->post('cat_id');
        /* $sub_cat = $this->common_model->commonselect2('tbl_categories', 'cat_parent_id', $main_cat_id, 'cat_status', 1)->result(); */
        $sub_cat = $this->db->query("
                            SELECT *
                            FROM tbl_categories
                            WHERE cat_parent_id = '" . $main_cat_id . "' AND cat_status = 1 AND cat_is_delete = 0
                        ")->result();
        if (count($sub_cat) > 0) {

           $str = '<div class="describe-plan">';
           $str .='<label>Sub Category <span class="red">*</span></label>';
                                           
            $str .= "<select name='sub_cat' class='sub-cat' id='sub_cat' >";
            $str .= "<option value=''>Select</option>";
            foreach ($sub_cat as $subCat) {
                $str .= "<option value=" . $subCat->cat_id . ">" . $subCat->cat_name . "</option>";
            }
            $str .= "</select>";
            $str .= "</div>";


            echo $str;
        } else {
            echo 0;
        }
    }

    public function get_sub_sub_category() {
        $main_cat_id = $this->input->post('cat_id');
        /* $sub_cat = $this->common_model->commonselect2('tbl_categories', 'cat_parent_id', $main_cat_id, 'cat_status', 1)->result(); */
        $sub_cat = $this->db->query("
                            SELECT *
                            FROM tbl_categories
                            WHERE cat_parent_id = '" . $main_cat_id . "' AND cat_status = 1 AND cat_is_delete = 0
                        ")->result();
        if (count($sub_cat) > 0) {

             $str = '<div class="describe-plan">';
           $str .='<label>Sub Sub Category <span class="red">*</span></label>';

            $str .= "<select name='sub_sub_cat' class='sub-sub-cat'  id='sub_sub_cat' >";
            $str .= "<option value=''>Select</option>";
            foreach ($sub_cat as $subCat) {
                $str .= "<option value=" . $subCat->cat_id . ">" . $subCat->cat_name . "</option>";
            }
            $str .= "</select>";
            $str .= "</div>";

            echo $str;
        } else {
            echo 0;
        }
    }

// Mehmood

    public function getProductDetails() {
        $id = $this->input->post('id');
        $product = $this->common_model->commonselect('tbl_products', 'prod_id', $id)->row();
        $product->prod_start_date = date('m-d-Y', strtotime($product->sale_start_date) );
        $product->prod_end_date = date('m-d-Y', strtotime($product->sale_end_date) );



        $image = $this->common_model->commonselect('tbl_product_images', 'img_prod_id', $id)->result();
        $product_cat = $this->common_model->commonselect('tbl_product_category', 'prod_id', $id)->row();
        $product_filter = $this->common_model->commonselect('tbl_product_filters', 'prod_id', $id)->result();
        $product_filters_detail = $this->common_model->commonselect('tbl_product_filters_detail', 'prod_id', $id)->result();

        if (count($image) > 0) {
            setcookie('img_counter', count($image), 0, '/');
        }

        $result['product'] = $product;
        $result['sale_start'] = date('m-d-Y', strtotime($product->sale_start_date) );
        $result['sale_end'] = date('m-d-Y', strtotime($product->sale_end_date) );
        $result['images'] = $image;
        $result['product_cat'] = $product_cat;
        $result['product_filter'] = $product_filter;
        $result['product_filters_detail'] = $product_filters_detail;


        echo json_encode($result);
        // print_r($product);
    }

    public function get_required_filter_unique() {
        $cat_id = $this->input->post('cat_id');
        $is_has = 0;
        $filterReq = $this->common_model->commonselect3('tbl_cat_filter_title', 'filter_cat_id', $cat_id, 'is_required', 1, 'cat_filter_values != ', '')->result();
        $filterReqCon = $this->db->query("
                                SELECT f.*
                                FROM tbl_cat_filter_title f
                                INNER JOIN tbl_cat_filter_detail df ON f.filter_title_id = df.filter_detail_title_id
                                WHERE f.cat_filter_is_conditional = 1 AND
                                f.filter_cat_id = $cat_id AND
                                df.filter_detail != '' AND
                                f.is_required = 1 AND 
                                df.filter_detail != ''
                                GROUP BY f.filter_title_id
                                ")->result();

        $str = '';
        if (count($filterReq) > 0) {
            $is_has = 1;
            $str = '';

            //$str .=     "<option value=''>Select</option>";
            foreach ($filterReq as $filter) {
                $str .= "<div class='unique-itm-main-row-bx describe-plan'>";
                  $str .= "<label>$filter->filter_title * </label>";
                $str .= "<select name='$filter->filter_slug'  >";
                if ($filter->cat_filter_values != '') {

                    $options = explode(",", ltrim(rtrim($filter->cat_filter_values, ','), ','));
                    foreach ($options as $op) {
                        $str .= "<option value='" . $op . "'>" . $op . "</option>";
                    }
                }
                $str .= "</select>";
                $str .= "</div>";
            }
            //echo $str;
        }
        if (count($filterReqCon) > 0) {
            $is_has = 1;
            foreach ($filterReqCon as $filter) {
                $str .= "<div class='unique-itm-main-row-bx describe-plan'>";
                $str .= "<label>$filter->filter_title * </label>";
                $str .= "<select name='$filter->filter_slug'  class='conditional-req'>";
                $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_title_id', $filter->filter_title_id)->result();
                if (count($values) > 0) {
                    foreach ($values as $op) {
                        $str .= "<option value='" . $op->filter_detail_id . "'>" . $op->filter_title . "</option>";
                    }
                }
                $str .= "</select>";
                $str .= "</div>";

                $str .= '<div class="unique-itm-main-row-bx describe-plan ' . $filter->filter_slug . '">';
                $str .= "<label>" . $values[0]->filter_title . " * </label>";
                $str .= "<select name='" . $values[0]->filter_slug . "'  >";
                $options = explode(",", ltrim(rtrim($values[0]->filter_detail, ','), ','));
                foreach ($options as $op) {
                    $str .= "<option value='" . $op . "'>" . $op . "</option>";
                }
                $str .= "</select>";
                $str .= "</div>";
            }
            //echo $str;
        }

        if ($is_has == 0) {
            echo 0;
        } else {
            echo $str;
        }
    }

    public function get_sub_conditional_req_unique() {
        $filter_id = $this->input->post('filter_id');
        $str = '';
        $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_id', $filter_id)->result();
        if (count($values) > 0) {
             $str .= '<div class="unique-itm-main-row-bx describe-plan ' . $filter->filter_slug . '">';
            $str .= "<h5>" . $values[0]->filter_title . "<span> * </span></h5>";
            $str .= "<select name='" . $values[0]->filter_slug . "'  >";
            $options = explode(",", ltrim(rtrim($values[0]->filter_detail, ','), ','));
            foreach ($options as $op) {
                $str .= "<option value='" . $op . "'>" . $op . "</option>";
            }
            $str .= "</select>";
            echo $str;
        } else {
            echo 0;
        }
    }

    public function get_additional_filter_unique() {
        $cat_id = $this->input->post('cat_id');
        
        $is_has = 0;
        $filterReq = $this->common_model->commonselect3('tbl_cat_filter_title', 'filter_cat_id', $cat_id, 'is_required', 0, 'cat_filter_values != ', '')->result();
        $filterReqCon = $this->db->query("
                                SELECT f.*
                                FROM tbl_cat_filter_title f
                                INNER JOIN tbl_cat_filter_detail df ON f.filter_title_id = df.filter_detail_title_id
                                WHERE f.cat_filter_is_conditional = 1 AND
                                f.filter_cat_id = $cat_id AND
                                df.filter_detail != '' AND
                                f.is_required = 0 AND 
                                df.filter_detail != '' 
                                GROUP BY f.filter_title_id
                                ")->result();
        $str = '';
        if (count($filterReq) > 0) {
            $is_has = 1;
            $str = '';

            //$str .=     "<option value=''>Select</option>";
            foreach ($filterReq as $filter) {
                $str .= '<div class="unique-itm-main-row-bx describe-plan">';
                $str .= "<label>".$filter->filter_title."</label>";
                $str .= "<select name='".$filter->filter_slug."'  >";
                $str .= "<option value=''>Select</option>";
                if ($filter->cat_filter_values != '') {

                    $options = explode(",", ltrim(rtrim($filter->cat_filter_values, ','), ','));
                    foreach ($options as $op) {
                        $str .= "<option value='" . $op . "'>" . $op . "</option>";
                    }
                }
                $str .= "</select>";
                $str .= "</div>";
            }
            //echo $str;
        }
        if (count($filterReqCon) > 0) {
            $is_has = 1;
            foreach ($filterReqCon as $filter) {
                $str .= '<div class="unique-itm-main-row-bx describe-plan">';
                $str .= "<label>".$filter->filter_title."</label>";
                $str .= "<select name='".$filter->filter_slug."'  class='conditional'>";
                $str .= "<option value=''>Select</option>";
                $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_title_id', $filter->filter_title_id)->result();
                if (count($values) > 0) {
                    foreach ($values as $op) {
                        $str .= "<option value='" . $op->filter_detail_id . "'>" . $op->filter_title . "</option>";
                    }
                }
                $str .= "</select>";
                $str .= "</div>";

                $str .= '<div class="unique-itm-main-row-bx ' . $filter->filter_slug . '">';
                 // $str .= "<h5>".$values[0]->filter_title."</h5>";
                 //  $str .= "<select name='".$values[0]->filter_slug."'  >";
                 //  $options = explode(",",ltrim(rtrim($values[0]->filter_detail,','),',') );
                 //  foreach($options as $op){
                 //  $str .=  "<option value='".$op."'>".$op."</option>";
                 //  }
                 //  $str .= "</select>"; 
                $str .= "</div>";
            }
            //echo $str;
        }

        if ($is_has == 0) {
            echo 0;
        } else {
            echo $str;
        }
    }

    public function get_sub_conditional_unique() {
        $filter_id = $this->input->post('filter_id');
        $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_id', $filter_id)->result();
        $str = '';
        if (count($values) > 0) {
            $str .= '<div class="describe-plan">';
            $str .= "<label>" . $values[0]->filter_title . "</label>";
            $str .= "<select name='" . $values[0]->filter_slug . "' >";
            $str .= "<option value=''>Select</option>";
            $options = explode(",", ltrim(rtrim($values[0]->filter_detail, ','), ','));
            foreach ($options as $op) {
                $str .= "<option value='" . $op . "'>" . $op . "</option>";
            }
            $str .= "</select>";
            echo $str;
        } else {
            echo 0;
        }
    }

    /*
     * ***************************************************************
     * ********************---------------------**********************
     * ****************** [ For Multiple Filters ] *******************
     * ******************** -------------------- *********************
     * ***************************************************************
     */

    public function get_required_filter_multi() {
        $cat_id = $this->input->post('cat_id');
        $is_has = 0;
        $filterReq = $this->common_model->commonselect3('tbl_cat_filter_title', 'filter_cat_id', $cat_id, 'is_required', 1, 'cat_filter_values != ', '')->result();
        $filterReqCon = $this->db->query("
                                SELECT f.*
                                FROM tbl_cat_filter_title f
                                INNER JOIN tbl_cat_filter_detail df ON f.filter_title_id = df.filter_detail_title_id
                                WHERE f.cat_filter_is_conditional = 1 AND
                                f.filter_cat_id = $cat_id AND
                                df.filter_detail != '' AND
                                f.is_required = 1 AND 
                                df.filter_detail != ''
                                GROUP BY f.filter_title_id
                                ")->result();
        $str = '';

        if (count($filterReq) > 0) {
            $is_has = 1;
            foreach ($filterReq as $filter) {
                $str .= '<div class="clr-size">';
                $str .= '<div class="panel-con">';

                $str .= "<button type='button' class='accordion'>$filter->filter_title * </button>";
                $str .= '<div class="panel">';
                $str .= '<div class="multi-box-color">';
                $str .=    '<div class="multi-box-color-row atleadst-one-catrg-bx">';
                if ($filter->cat_filter_values != '') {
                    $options = explode(",", ltrim(rtrim($filter->cat_filter_values, ','), ','));
                    $i = 1;
                    foreach ($options as $op) {
                        $str .= '<div class="multi-box-color-blk">';    
                         $str .=   '<div class="signed-in-checkbox">';
                         $str .= '<label>';
                        $str .= '<input  type="checkbox" id="' . $i . '_' . $filter->filter_title_id . '" value="' . $op . '" name="multi_' . $filter->filter_slug . '[]">';
                        $str .= '<span></span>' .  $op ;
                        $str .= '</label> ';
                        $str .= '</div>';
                        $str .= '</div>';
                        $i++;
                    }
                }
                $str .= '</div>';
                $str .= '<div class="error"></div>';
                $str .= '</div>';
                $str .= "</div>";
                $str .= "</div>";
                $str .= "</div>";
            }
            //echo $str;
        }
        if (count($filterReqCon) > 0) {
            $i = $is_has = 1;

            foreach ($filterReqCon as $filter) {
                $str .= '<div class="clr-size">';
                $str .= '<div class="panel-con">';

                $str .= "<button type='button' class='accordion'>$filter->filter_title * </button>";
                $str .= '<div class="panel">';
                 $str .= '<div class="multi-box-color">';
                $str .=    '<div class="multi-box-color-row atleadst-one-catrg-bx">';
                $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_title_id', $filter->filter_title_id)->result();
                if (count($values) > 0) {
                    foreach ($values as $op) {
                         $str .= '<div class="multi-box-color-blk">';
                         $str .= '<div class="multi-box" style="position: relative;">';    
                          $str .=  '<div class="signed-in-checkbox">';
                         $str .= '<label>';
                         $str .= '   <input class="multi-conditional"  type="checkbox" id="' . $i . '_' . $filter->filter_title_id . '" value="' . $op->filter_detail_id . '" data-name2="multi_' . $filter->filter_slug . '' . $i . '_' . $filter->filter_title_id . '" name="multi_' . $filter->filter_slug . '[]">';
                        $str .= '<span></span>' .$op->filter_title;
                        $str .= '</label> ';
                        $str .= '   <div class="multi_' . $filter->filter_slug . '' . $i . '_' . $filter->filter_title_id . ' multi-sub-box"></div>';
                        $str .= '</div>';
                        $str .= '</div>';
                        $str .= '</div>';
                        $i++;
                    }
                }
                $str .= '</div>';
                $str .= '</div>';
                $str .= "</div>";
                $str .= "</div>";
                $str .= "</div>";
            }
            //echo $str;
        }

        if ($is_has == 0) {
            echo 0;
        } else {
            echo $str;
        }
    }

    public function get_sub_conditional_req_multi() {
        $filter_id = $this->input->post('filter_id');
        $str = '';
        $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_id', $filter_id)->result();
        if (count($values) > 0) {
            $options = explode(",", ltrim(rtrim($values[0]->filter_detail, ','), ','));
            $i = 1;
            foreach ($options as $op) {
                $str .= '<div class="signed-in-checkbox">';
                $str .= '<label for="sub_' . $i . '_' . $values[0]->filter_detail_id . '" class="exp-clr">';
                $str .= '   <input class="mycheckbox"  type="checkbox" id="sub_' . $i . '_' . $values[0]->filter_detail_id . '" value="' . $op . '" name="multi_' . $values[0]->filter_slug . '[]">';
                $str .= '  <span></span>' . $op . '</label>';
                $str .= '</div>';
                $i++;
            }
            echo $str;
        } else {
            echo 0;
        }
    }

    public function valid_sku_add() {
        $sku = $this->input->post('sku');
        $prod_sku = $this->common_model->commonselect('tbl_products', 'prod_sku', $sku)->result();
        if (count($prod_sku) > 0) {
            echo 0;
        } else {
            echo 1;
        }
    }

    
    public function get_required_filter_unique_edit($prod_id = '') {
        $cat_id = $this->input->post('cat_id');
        $is_has = 0;
        $filterReq = $this->common_model->commonselect3('tbl_cat_filter_title', 'filter_cat_id', $cat_id, 'is_required', 1, 'cat_filter_values != ', '')->result();
        $filterReqCon = $this->db->query("
                                SELECT f.*
                                FROM tbl_cat_filter_title f
                                INNER JOIN tbl_cat_filter_detail df ON f.filter_title_id = df.filter_detail_title_id
                                WHERE f.cat_filter_is_conditional = 1 AND
                                f.filter_cat_id = $cat_id AND
                                df.filter_detail != '' AND
                                f.is_required = 1 AND 
                                df.filter_detail != ''
                                GROUP BY f.filter_title_id
                                ")->result();
        $str = '';
        if (count($filterReq) > 0) {
            $is_has = 1;
            $str = '';

            //$str .=     "<option value=''>Select</option>";
            foreach ($filterReq as $filter) {
                $prodFilter = $this->common_model->commonselect2('tbl_product_filters', 'filter_id', $filter->filter_title_id, 'prod_id', $prod_id)->row();
                //
                $str .= '<div class="unique-itm-main-row-bx">';
                $str .= "<h5>$filter->filter_title <span> * </span></h5>";
                $str .= "<select name='$filter->filter_slug'  >";
                if ($filter->cat_filter_values != '') {

                    $options = explode(",", ltrim(rtrim($filter->cat_filter_values, ','), ','));
                    foreach ($options as $op) {
                        $str .= "<option value='" . $op . "'" . ((@$prodFilter->filter_value == $op) ? 'selected' : '') . " >" . $op . "</option>";
                    }
                }
                $str .= "</select>";
                $str .= "</div>";
            }
            //echo $str;
        }
        if (count($filterReqCon) > 0) {
            $is_has = 1;
            foreach ($filterReqCon as $filter) {
                $prodFilter = $this->common_model->commonselect2('tbl_product_filters', 'filter_id', $filter->filter_title_id, 'prod_id', $prod_id)->row();
                $prodFilter_id = 0;
                $subFilterTitle = $subFilterSlug = '';

                $str .= '<div class="unique-itm-main-row-bx">';
                $str .= "<h5>$filter->filter_title <span> * </span></h5>";
                $str .= "<select name='$filter->filter_slug'  class='conditional-req'>";
                $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_title_id', $filter->filter_title_id)->result();
                if (count($values) > 0) {
                    foreach ($values as $op) {
                        $str .= "<option value='" . $op->filter_detail_id . "'" . ((@$prodFilter->filter_value == $op->filter_detail_id) ? 'selected' : '') . " >" . $op->filter_title . "</option>";
                        if (@$prodFilter->filter_value == $op->filter_detail_id) {
                            $prodFilter_id = $op->filter_detail_id;
                            $subFilterTitle = $op->filter_title;
                            $subFilterSlug = $op->filter_slug;
                        }
                    }
                }
                $str .= "</select>";
                $str .= "</div>";
                //
                if ($prodFilter_id > 0) {
                    $prodDeFilter = $this->common_model->commonselect('tbl_product_filters_detail', 'prod_filter_id', $prodFilter->id)->row();
                    //
                    $str .= '<div class="unique-itm-main-row-bx ' . $prodDeFilter->filter_slug . '">';
                    $str .= "<h5>" . $subFilterTitle . "<span> * </span></h5>";
                    $str .= "<select name='" . $subFilterSlug . "'  >";
                    $options = explode(",", ltrim(rtrim($values[0]->filter_detail, ','), ','));
                    foreach ($options as $op) {
                        $str .= "<option value='" . $op . "'" . ((@$prodDeFilter->filter_value == $op) ? 'selected' : '') . " >" . $op . "</option>";
                    }
                    $str .= "</select>";
                    $str .= "</div>";
                } else {
                    $str .= '<div class="unique-itm-main-row-bx ' . $filter->filter_slug . '">';
                    $str .= "<h5>" . $values[0]->filter_title . "<span> * </span></h5>";
                    $str .= "<select name='" . $values[0]->filter_slug . "'  >";
                    $options = explode(",", ltrim(rtrim($values[0]->filter_detail, ','), ','));
                    foreach ($options as $op) {
                        $str .= "<option value='" . $op . "'>" . $op . "</option>";
                    }
                    $str .= "</select>";
                    $str .= "</div>";
                }
            }
            //echo $str;
        }

        if ($is_has == 0) {
            echo 0;
        } else {
            echo $str;
        }
    }

    public function get_additional_filter_unique_edit($prod_id = '') {
        $cat_id = $this->input->post('cat_id');
        $is_has = 0;
        $filterReq = $this->common_model->commonselect3('tbl_cat_filter_title', 'filter_cat_id', $cat_id, 'is_required', 0, 'cat_filter_values != ', '')->result();
        $filterReqCon = $this->db->query("
                                SELECT f.*
                                FROM tbl_cat_filter_title f
                                INNER JOIN tbl_cat_filter_detail df ON f.filter_title_id = df.filter_detail_title_id
                                WHERE f.cat_filter_is_conditional = 1 AND
                                f.filter_cat_id = $cat_id AND
                                df.filter_detail != '' AND
                                f.is_required = 0 
                                GROUP BY f.filter_title_id
                                ")->result();
        $str = '';
        if (count($filterReq) > 0) {
            $is_has = 1;
            $str = '';

            //$str .=     "<option value=''>Select</option>";
            foreach ($filterReq as $filter) {
                $prodFilter = $this->common_model->commonselect2('tbl_product_filters', 'filter_id', $filter->filter_title_id, 'prod_id', $prod_id)->row();
                //
                $str .= '<div class="unique-itm-main-row-bx">';
                $str .= "<h5>$filter->filter_title</h5>";
                $str .= "<select name='$filter->filter_slug'  >";
                $str .= "<option value=''>Select</option>";
                if ($filter->cat_filter_values != '') {

                    $options = explode(",", ltrim(rtrim($filter->cat_filter_values, ','), ','));
                    foreach ($options as $op) {
                        $str .= "<option value='" . $op . "'" . ((@$prodFilter->filter_value == $op) ? 'selected' : '') . " >" . $op . "</option>";
                    }
                }
                $str .= "</select>";
                $str .= "</div>";
            }
            //echo $str;
        }
        if (count($filterReqCon) > 0) {
            $is_has = 1;
            foreach ($filterReqCon as $filter) {
                $prodFilter = $this->common_model->commonselect2('tbl_product_filters', 'filter_id', $filter->filter_title_id, 'prod_id', $prod_id)->row();
                $prodFilter_id = 0;
                $subFilterTitle = $subFilterSlug = '';

                $str .= '<div class="unique-itm-main-row-bx">';
                $str .= "<h5>$filter->filter_title</h5>";
                $str .= "<select name='$filter->filter_slug'  class='conditional'>";
                $str .= "<option value=''>Select</option>";
                $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_title_id', $filter->filter_title_id)->result();
                if (count($values) > 0) {
                    foreach ($values as $op) {
                        $str .= "<option value='" . $op->filter_detail_id . "'" . ((@$prodFilter->filter_value == $op->filter_detail_id) ? 'selected' : '') . " >" . $op->filter_title . "</option>";
                        if (@$prodFilter->filter_value == $op->filter_detail_id) {
                            $prodFilter_id = $op->filter_detail_id;
                            $subFilterTitle = $op->filter_title;
                            $subFilterSlug = $op->filter_slug;
                        }
                    }
                }
                $str .= "</select>";
                $str .= "</div>";
                //
                if ($prodFilter_id > 0) {
                    $prodDeFilter = $this->common_model->commonselect('tbl_product_filters_detail', 'prod_filter_id', $prodFilter->id)->row();
                    //
                    $str .= '<div class="unique-itm-main-row-bx ' . $prodDeFilter->filter_slug . '">';
                    $str .= "<h5>" . $subFilterTitle . "<span> * </span></h5>";
                    $str .= "<select name='" . $subFilterSlug . "' >";
                    $options = explode(",", ltrim(rtrim($values[0]->filter_detail, ','), ','));
                    foreach ($options as $op) {
                        $str .= "<option value='" . $op . "'" . ((@$prodDeFilter->filter_value == $op) ? 'selected' : '') . " >" . $op . "</option>";
                    }
                    $str .= "</select>";
                    $str .= "</div>";
                } else {
                    $str .= '<div class="unique-itm-main-row-bx ' . $filter->filter_slug . '">';
                    /* $str .= "<h5>".$values[0]->filter_title."</h5>";
                      $str .= "<select name='".$values[0]->filter_slug."'  >";
                      $options = explode(",",ltrim(rtrim($values[0]->filter_detail,','),',') );
                      foreach($options as $op){
                      $str .=  "<option value='".$op."'>".$op."</option>";
                      }
                      $str .= "</select>"; */
                    $str .= "</div>";
                }
            }
            //echo $str;
        }

        if ($is_has == 0) {
            echo 0;
        } else {
            echo $str;
        }
    }

    public function get_required_filter_multi_edit($prod_id = '') {
        $cat_id = $this->input->post('cat_id');
        $is_has = 0;
        $filterReq = $this->common_model->commonselect3('tbl_cat_filter_title', 'filter_cat_id', $cat_id, 'is_required', 1, 'cat_filter_values != ', '')->result();
        $filterReqCon = $this->db->query("
                                SELECT f.*
                                FROM tbl_cat_filter_title f
                                INNER JOIN tbl_cat_filter_detail df ON f.filter_title_id = df.filter_detail_title_id
                                WHERE f.cat_filter_is_conditional = 1 AND
                                f.filter_cat_id = $cat_id AND
                                df.filter_detail != '' AND
                                f.is_required = 1 AND 
                                df.filter_detail != ''
                                GROUP BY f.filter_title_id
                                ")->result();
        $str = '<h4>Select at least one of each category<span>*</span></h4>';

        if (count($filterReq) > 0) {
            $is_has = 1;
            foreach ($filterReq as $filter) {
                $prodFilter = $this->common_model->commonselect2('tbl_product_filters', 'filter_id', $filter->filter_title_id, 'prod_id', $prod_id)->result_array();

                $newarray = array();
                foreach ($prodFilter as $value) {
                    $newarray[] = $value['filter_value'];
                }


                $str .= '<div class="atleadst-one-catrg-bx">';
                $str .= "<h6>$filter->filter_title <span> * </span></h6>";
                $str .= '<div class="atleadst-one-catrg-bx-row">';
                if ($filter->cat_filter_values != '') {
                    $options = explode(",", ltrim(rtrim($filter->cat_filter_values, ','), ','));
                    $i = 1;
                    foreach ($options as $op) {
                        $str .= '<div class="catrg-bx-row-clr">';
                        $str .= '<input type="checkbox" id="' . $i . '_' . $filter->filter_title_id . '" value="' . $op . '"' . ((in_array($op, $newarray)) ? "checked" : "") . ' name="multi_' . $filter->filter_slug . '[]">';
                        $str .= '<label for="' . $i . '_' . $filter->filter_title_id . '"><span></span><p>' . $op . '</p></label>';
                        $str .= '</div>';
                        $i++;
                    }
                }
                $str .= "</div>";
                $str .= "</div>";
            }
            //echo $str;
        }
        if (count($filterReqCon) > 0) {
            $i = $is_has = 1;

            foreach ($filterReqCon as $filter) {
                $prodFilter = $this->common_model->commonselect2('tbl_product_filters', 'filter_id', $filter->filter_title_id, 'prod_id', $prod_id)->result_array();

                $newarray = array();
                foreach ($prodFilter as $value) {
                    $newarray[] = $value['filter_value'];
                }

                $str .= '<div class="atleadst-one-catrg-bx">';
                $str .= "<h6>$filter->filter_title <span> * </span></h6>";
                $str .= '   <div class="atleadst-one-catrg-bx-row">';
                $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_title_id', $filter->filter_title_id)->result();
                if (count($values) > 0) {
                    foreach ($values as $op) {
                        $str .= '<div class="catrg-bx-row-clr ">';
                        $str .= '   <input class="multi-conditional"  type="checkbox" id="' . $i . '_' . $filter->filter_title_id . '" value="' . $op->filter_detail_id . '"' . ((in_array($op->filter_detail_id, $newarray)) ? "checked" : "") . ' data-name2="multi_' . $filter->filter_slug . '' . $i . '_' . $filter->filter_title_id . '" name="multi_' . $filter->filter_slug . '[]">';
                        $str .= '   <label for="' . $i . '_' . $filter->filter_title_id . '"><span></span><p>' . $op->filter_title . '</p></label>';
                        $str .= '   <div class="multi_' . $filter->filter_slug . '' . $i . '_' . $filter->filter_title_id . '">';

                        if (in_array($op->filter_detail_id, $newarray)) {
                            $filter_id = $op->filter_detail_id;
                            //
                            $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_id', $filter_id)->result();
                            if (count($values) > 0) {
                                $prodDeFilter = $this->common_model->commonselect2('tbl_product_filters_detail', 'filter_detail_id', $filter_id, 'prod_id', $prod_id)->result_array();
                                //

                                $newarray2 = array();
                                foreach ($prodDeFilter as $value2) {
                                    $newarray2[] = $value2['filter_value'];
                                }

                                $options = explode(",", ltrim(rtrim($values[0]->filter_detail, ','), ','));
                                $j = 1;
                                foreach ($options as $op) {
                                    $str .= '<div class="catrg-bx-row-clr">';
                                    $str .= '   <input  type="checkbox" id="sub_' . $j . '_' . $values[0]->filter_detail_id . '" value="' . $op . '"' . ((in_array($op, $newarray2)) ? "checked" : "") . ' name="multi_' . $values[0]->filter_slug . '[]">';
                                    $str .= '   <label for="sub_' . $j . '_' . $values[0]->filter_detail_id . '"><span></span><p>' . $op . '</p></label>';
                                    $str .= '</div>';
                                    $j++;
                                }
                            }
                        }

                        $str .= '</div>';
                        $str .= '</div>';
                        $i++;
                    }
                }
                $str .= "   </div>";
                $str .= "</div>";
            }
            //echo $str;
        }

        if ($is_has == 0) {
            echo 0;
        } else {
            echo $str;
        }
    }

    
    public function upload_prod_image() {
        $path = $_SERVER['DOCUMENT_ROOT'] . '/front_resources/new_filer/php/class.uploader.php';
        include $path;

        $no_of_images = $_COOKIE['img_counter'];
        /* echo $no_of_images;
          exit */;
        if ($no_of_images >= 9) {
            echo '200';
        } else {
            $no_of_images = $no_of_images + 1;
            setcookie('img_counter', $no_of_images, 0, '/');
            $uploader = new Uploader();
            $data = $uploader->upload($_FILES['files'], array(
                'limit' => 10, //Maximum Limit of files. {null, Number}
                'maxSize' => 10, //Maximum Size of files {null, Number(in MB's)}
                'extensions' => array('jpeg', 'jpg', 'gif', 'png', 'JPEG', 'JPG', 'GIF', 'PNG'), //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
                'required' => false, //Minimum one file is required for upload {Boolean}
                'uploadDir' => $_SERVER['DOCUMENT_ROOT'] . '/resources/prod_images/', //Upload directory {String}
                'title' => array('auto'), //New file name {null, String, Array} *please read documentation in README.md
                'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
                'replace' => false, //Replace the file if it already exists  {Boolean}
                'perms' => null, //Uploaded file permisions {null, Number}
                'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
                'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
                'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
                'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
                'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
                'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
            ));



            if ($data['isComplete']) {
                $files = $data['data'];
                $this->load->library('image_lib');
                $configt['image_library'] = 'gd2';
                $configt['source_image'] = $files['metas'][0]['file'];
                $configt['new_image'] = $_SERVER['DOCUMENT_ROOT'] . '/resources/prod_images/thumb/';
                $configt['maintain_ratio'] = true;
                $configt['width'] = 80;
                $configt['height'] = 60;
                $this->image_lib->initialize($configt);
                $this->image_lib->resize();

                $dbData['img_name'] = $files['metas'][0]['name'];
                //$dbData['img_unique_str'] = $this->input->post('unique_str');
                /* echo "<pre>";
                  print_r($files['metas']);
                  exit; */
                //$this->db->insert('tbl_product_images', $dbData);
                //$insertId = $this->db->insert_id();
                //$files['metas'][0]['image_id'] = $insertId;
                echo json_encode($files['metas'][0]);
            }

            if ($data['hasErrors']) {
                $errors = $data['errors'];
                echo json_encode($errors);
            }
        }

        exit;
    }

    public function edit_upload_prod_image() {
        $path = $_SERVER['DOCUMENT_ROOT'] . '/front_resources/new_filer/php/class.uploader.php';
        include $path;

        $no_of_images = $_COOKIE['img_counter'];
        /* echo $no_of_images;
          exit */;
        if ($no_of_images >= 9) {
            echo '200';
        } else {
            $no_of_images = $no_of_images + 1;
            setcookie('img_counter', $no_of_images, 0, '/');
            $uploader = new Uploader();
            $data = $uploader->upload($_FILES['files'], array(
                'limit' => 10, //Maximum Limit of files. {null, Number}
                'maxSize' => 10, //Maximum Size of files {null, Number(in MB's)}
                'extensions' => array('jpeg', 'jpg', 'gif', 'png', 'JPEG', 'JPG', 'GIF', 'PNG'), //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
                'required' => false, //Minimum one file is required for upload {Boolean}
                'uploadDir' => $_SERVER['DOCUMENT_ROOT'] . '/resources/prod_images/', //Upload directory {String}
                'title' => array('auto'), //New file name {null, String, Array} *please read documentation in README.md
                'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
                'replace' => false, //Replace the file if it already exists  {Boolean}
                'perms' => null, //Uploaded file permisions {null, Number}
                'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
                'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
                'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
                'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
                'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
                'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
            ));



            if ($data['isComplete']) {
                $files = $data['data'];
                $this->load->library('image_lib');
                $configt['image_library'] = 'gd2';
                $configt['source_image'] = $files['metas'][0]['file'];
                $configt['new_image'] = $_SERVER['DOCUMENT_ROOT'] . '/resources/prod_images/thumb/';
                $configt['maintain_ratio'] = true;
                $configt['width'] = 80;
                $configt['height'] = 60;
                $this->image_lib->initialize($configt);
                $this->image_lib->resize();

                $dbData['img_name'] = $files['metas'][0]['name'];
                //$dbData['img_unique_str'] = $this->input->post('unique_str');
                /* echo "<pre>";
                  print_r($files['metas']);
                  exit; */
                //$this->db->insert('tbl_product_images', $dbData);
                //$insertId = $this->db->insert_id();
                //$files['metas'][0]['image_id'] = $insertId;
                echo json_encode($files['metas'][0]);
            }

            if ($data['hasErrors']) {
                $errors = $data['errors'];
                echo json_encode($errors);
            }
        }
        exit;
    }

    public function remove_images() {
        // $data = $this->common_model->commonselect('tbl_product_images', 'img_id', $img_unique_str)->row();
        // if(count($data) > 0){
        $no_of_images = $_COOKIE['img_counter'];
        $no_of_images = $no_of_images - 1;
        setcookie('img_counter', $no_of_images, 0, '/');
        $img_name = $_POST['file'];
        unlink(FCPATH . 'resources/prod_images/' . $img_name['name']);
        unlink(FCPATH . 'resources/prod_images/thumb/' . $img_name['name']);
        //}
        //$this->common_model->commonDelete('tbl_product_images','img_id', $img_unique_str);
        echo 1;
    }

    public function edit_remove_images() {
        $img_name = $_POST['file'];
        $no_of_images = $_COOKIE['img_counter'];
        $no_of_images = $no_of_images - 1;
        $data = $this->common_model->commonselect('tbl_product_images', 'img_name', $img_name)->row();
        if (count($data) > 0) {
            setcookie('img_counter', $no_of_images, 0, '/');
            unlink(FCPATH . 'resources/prod_images/' . $img_name);
            unlink(FCPATH . 'resources/prod_images/thumb/' . $img_name);
            $this->common_model->commonDelete('tbl_product_images', 'img_name', $img_name);
        }
        echo 1;
    }

   

    
    public function bluk_product_delete() {

        $response = $this->product_model->product_bluk_delete();
        if ($response == 1) {
            echo json_encode(array('msg' => 'Selected Products deleted successfully.'));
            die;
        }
    }

    public function download_csv() {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=user_products_' . date("m-d-Y") . '.csv');
        $output = fopen('php://output', 'w');
        fputcsv($output, array('Product ID', 'Product Title', 'Price', 'Shipping', 'Qty', 'Status'));
        $resultsProd = $this->product_model->product_download_csv();
        $res = array();
        //echo "<pre>";print_r($resultsProd);die;
        for ($n = 0; $n < count($resultsProd); $n++) {
            $res[$n]['prod_id'] = $resultsProd[$n]['prod_id'];
            $res[$n]['prod_title'] = $resultsProd[$n]['prod_title'];
            $res[$n]['prod_price'] = $resultsProd[$n]['prod_price'];
            $res[$n]['shipping'] = 0.00;
            if($resultsProd[$n]['shipping'] == 'Charge for Shipping'){
                $res[$n]['shipping'] = $resultsProd[$n]['ship_price'];
            }

            $res[$n]['qty'] = $resultsProd[$n]['qty'];
            $res[$n]['prod_status'] = ($resultsProd[$n]['prod_status'] == 1)? 'Active':'Inactive';

            
            $sDate = date('Y-m-d', strtotime($resultsProd[$n]['sale_start_date']));
            $eDate = date('Y-m-d', strtotime($resultsProd[$n]['sale_end_date']));
            $tDate = date('Y-m-d');
            if($resultsProd[$n]['prod_onsale'] == 1) {
                if($resultsProd[$n]['sale_start_date'] != '' && $resultsProd[$n]['sale_end_date'] != '') {
                    if($tDate >= $sDate && $tDate <= $eDate) {
                        $res[$n]['prod_price'] = number_format($resultsProd[$n]['sale_price'], 2,'.','');
                    } else {
                        $res[$n]['prod_price'] = number_format($resultsProd[$n]['prod_price'], 2,'.','');
                    }
                }else if($resultsProd[$n]['sale_start_date'] != '') {
                    if($tDate >= $sDate) {
                        $res[$n]['prod_price'] = number_format($resultsProd[$n]['sale_price'], 2,'.','');
                    } else {
                        $res[$n]['prod_price'] = number_format($resultsProd[$n]['prod_price'], 2,'.','');
                    }
                } else if($resultsProd[$n]['sale_end_date'] != '') {
                    if($tDate <= $eDate) {
                        $res[$n]['prod_price'] = number_format($resultsProd[$n]['sale_price'], 2,'.','');
                    } else {
                        $res[$n]['prod_price'] = number_format($resultsProd[$n]['prod_price'], 2,'.','');
                    }
                } else {
                    $res[$n]['prod_price'] = number_format($resultsProd[$n]['sale_price'], 2,'.','');
                }
            } else {
                $res[$n]['prod_price'] = number_format($resultsProd[$n]['prod_price'], 2,'.','');
            }
            
            fputcsv($output, $res[$n]);
        }
    }

    public function bluk_product_copy() {
        $response = $this->product_model->product_bluk_copy();
        if ($response == 1) {
            echo json_encode(array('msg' => 'Selected Products Copied successfully.'));
            die;
        }
    }

    public function product_report() {
        $response = $this->product_model->product_report();
        if ($response == 1) {
            echo json_encode(array('msg' => 'Selected Products reported successfully.'));
            die;
        }
    }


    public function bulk_file_validate(){

        $myFiles = $_FILES['bulkFile'];
        $name = $myFiles['name'];
        $bulkFileName = '';
        if ($name != "" && !empty($name) && $name != "Upload File") {
            $ext = explode('.', $name);
            $ext = strtolower(end($ext));
            if ($ext == "csv" || $ext == "CSV") {
                // ok
                $_FILES['bulkFile']['name']       = $myFiles['name'];
                $_FILES['bulkFile']['type']       = $myFiles['type'];
                $_FILES['bulkFile']['tmp_name']   = $myFiles['tmp_name'];
                $_FILES['bulkFile']['error']      = $myFiles['error'];                
                $_FILES['bulkFile']['size']       = $myFiles['size'];
                
                $file_name = $myFiles['name'];
                
                $file_ext = explode('.',$file_name);
                $file_extension = end($file_ext);
                
                $image_path = $myFiles['tmp_name'];
                if($myFiles['name'] != '')
                {
                    $new_image_name = "csv_file_".date('m-d-Y-h-i-s'); //rand(1,1000).time();
                    $bulkFileName = $new_image_name.'.'.$file_extension;
                    //If user selected the photo to upload .
                    $uploaddir_11               = FCPATH."resources/bulk_files/";
                    $uploaddir_11               = str_replace(" ","",$uploaddir_11);
                    $config_11['upload_path']   = $uploaddir_11;
                    $config_11['allowed_types'] = 'csv|CSV';
                    $config_11['overwrite']     = true;
                    $config_11['remove_spaces'] = true;
                    $config_11['file_name']     = $bulkFileName;
                    $this->upload->initialize($config_11);
                    if ( ! $this->upload->do_upload('bulkFile'))
                    {
                        $this->session->set_flashdata('valid_file', $this->upload->display_errors());
                        echo $this->upload->display_errors();
                        redirect(base_url()."manage-products");
                        exit;
                    }else{
                        $this->bulk_upload_file($bulkFileName);
                        $this->session->set_flashdata('valid_file', 'Item(s) added successfully.');
                        redirect(base_url()."manage-products");
                        exit;
                    }
                }else{
                    $this->session->set_flashdata('valid_file', 'This field is required.');
                    echo "This field is required.";
                    redirect(base_url()."manage-products");
                    exit;
                }
            } else {
                $this->session->set_flashdata('valid_file', 'The file type you are attempting to upload is not allowed.');
                echo "The file type you are attempting to upload is not allowed.";
                redirect(base_url()."manage-products");
                exit;
            }
        } else {
            $this->session->set_flashdata('valid_file', 'This field is required.');
            echo "This field is required.";
            redirect(base_url()."manage-products");
            exit;
        }
    }


    public function bulk_upload_file($name)
    {
        ini_set('memory_limit', '-1');
        
        $file =  $_SERVER['DOCUMENT_ROOT'].'/resources/bulk_files/'.$name;

        $handle = fopen($file, "r"); // Make all conditions to avoid errors
        $read = file_get_contents($file); //read
        $lines = explode("\n", $read);//get
        $i= 0;//initialize
        foreach($lines as $key => $value){
            $cols[$i] = explode(",", $value);
            $i++;
        }
$total = count($cols);
        
        $insert = array();
        for($i = 8; $i < $total; $i++){
            if(@$cols[$i][3] != ''){
                $insert['looking_for'] = trim($cols[$i][0]);
                $insert['condition'] = trim($cols[$i][1]);
                $insert['qty'] = (int)$cols[$i][2];
                $insert['prod_title'] = trim($cols[$i][3]);
                $insert['prod_url'] = $this->common_model->create_slug($insert['prod_title'], 'tbl_categories', 'cat_url');
                $insert['prod_url'] = $this->common_model->create_slug($insert['prod_url'], 'tbl_products', 'prod_url');
                $insert['prod_description'] = $cols[$i][4];
                $insert['youtube_links'] = trim($cols[$i][5]);
                $insert['prod_price'] = number_format($cols[$i][6], 2, '.', '');
                $insert['sale_price'] = number_format($cols[$i][7], 2, '.', '');
                if($insert['sale_price'] > 0){
                    $insert['prod_onsale'] = 1;
                    $insert['sale_start_date'] = '';
                    $insert['sale_end_date'] = '';
                    if ($cols[$i][8] != '') {
                        $insert['sale_start_date'] = date('Y-m-d', strtotime($cols[$i][8]));
                    }
                    if ($cols[$i][9] != '') {
                        $insert['sale_end_date'] = date('Y-m-d', strtotime($cols[$i][9]));
                    }
                }
                
                $insert['shipping'] = trim($cols[$i][10]);
                $insert['ship_price'] = 0;
                if ($insert['shipping'] == "Charge for Shipping") {
                    $insert['prod_shipping_methods'] = $cols[$i][11];
                    $insert['ship_price'] = $cols[$i][12];
                    $insert['ship_days'] = $cols[$i][13];
                } else if ($insert['shipping'] == "Offer free Shipping") {
                    $insert['free_ship_days'] = trim($cols[$i][13]);
                }
                $insert['prod_return'] = trim($cols[$i][14]);
                $insert['prod_cat_id'] = 1;
                $insert['prod_created_date'] = date('Y-m-d H:i:s');
                $insert['prod_user_id'] = $this->session->userdata('userid');
                $insert['prod_store_id'] = $this->storeInfo->store_id;
                $insert['is_unique_filter'] = 1;
                $insert['prod_status'] = 0;
                // Insert Product Data
                
                $prod_id = $this->common_model->commonSave('tbl_products', $insert);

                $catData['category_id'] = 1;
                $catData['sub_category_id'] = 1;
                $catData['sub_sub_category_id'] = 1;
                $catData['prod_id'] = $prod_id;
                // Insert Categories
                $prod_cat_id = $this->common_model->commonSave('tbl_product_category', $catData);

                $imgData['img_name'] = 'no-image.png';
                // $imgData['img_label'] = 'no-image';
                $imgData['img_prod_id'] = $prod_id;
                $this->common_model->commonSave('tbl_product_images', $imgData);
                
            }
        }
    }






    public function testindex()
    {
        echo '<pre>';
        //print_r($_FILES);
        
        $myFiles1 = $_FILES['image'];
        /*$fileDP1 = array();
        $config = array(
            'allowed_types' => 'jpg|jpeg|gif|png',
            'upload_path' => FCPATH . '/resources/page_image/',
            'file_name' => 'TEST_' . date('m_d_Y') . '_' . time(),
        );*/

        $ext = explode(".",urldecode($_FILES["image"]["name"]));
        $ext = end($ext);
        $new_image_name = 'TEST_'. date('Y_m_d_h_i_s').'.'.$ext;
        $source_image = $_SERVER['DOCUMENT_ROOT'] . '/resources/prod_images/'.str_replace('"',"",$new_image_name);
        $test = move_uploaded_file($_FILES["image"]["tmp_name"],$source_image);

        var_dump($test);
    }

}

?>