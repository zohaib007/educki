<?php
class Products extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('common/common_model');
        $this->load->helper(array('form', 'url'));
        $this->load->model('front/guidetoeducki/Guidetoeducki_model');
        $this->load->model('front/faq/faq_model');
        $this->load->model('front/contact_model');
        $this->load->library('upload');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    }

    public function index()
    {
        echo 'Code.';
    }

    public function getProductsListing(){
        $mainCatUrl = $this->input->get('mainCatUrl');
        $subCatUrl = $this->input->get('subCatUrl');
        $subSubCatUrl = $this->input->get('subSubCatUrl');
        $is_logged_in = $this->input->get('is_logged_in');

        $location = $this->input->get('location');
        $radius = $this->input->get('radius');

        $range = explode(',',$this->input->get('range'));

        $per_page = $this->input->get('per_page');

        $multiple = $this->input->get('multiple');

        $counter = $this->input->get('counter') * $per_page;

        $data['counter'] = $counter;
        $data['store_id'] = $store_id;

        $sort = '';

        if ($this->input->get('sort_by') != '') {
            $data['sort_by'] = @$this->input->get('sort_by');
            if ($data['sort_by'] == "new") {
                $sort = 'p.prod_id DESC';
            } else if ($data['sort_by'] == "featured") {
                $sort = 'p.prod_feature DESC';
            } else if ($data['sort_by'] == "price_lh") {
                $sort = ' (CASE WHEN p.prod_onsale = 1 THEN sale_price ELSE p.prod_price END) ASC';
            } else if ($data['sort_by'] == "pricehl") {
                $sort = ' (CASE WHEN p.prod_onsale = 1 THEN sale_price ELSE p.prod_price END) DESC';
            }
        }

        $html['cat_html'] = $this->common_model->get_categories_html();

        $html['data'] = '';
        $mainCat = $this->common_model->commonselect4('tbl_categories','cat_url', $mainCatUrl, 'cat_parent_id', 0, 'cat_status', 1, 'cat_is_delete', 0, 'cat_sort', 'ASC')->row();
        if(!empty($mainCat)){
            $subCat = $this->common_model->commonselect4('tbl_categories', 'cat_parent_id', $mainCat->cat_id, 'cat_url', $subCatUrl, 'cat_status', 1, 'cat_is_delete', 0, 'cat_sort', 'ASC')->row();
            if(!empty($subCat)){
                $subSubCat = $this->common_model->commonselect4('tbl_categories','cat_url', $subSubCatUrl, 'cat_parent_id', $subCat->cat_id, 'cat_status', 1, 'cat_is_delete', 0, 'cat_sort', 'ASC')->row();
                if(!empty($subSubCat)){

                    $html['total_rows'] = count($this->get_products($subSubCat->cat_id,$sort,$to = 0,$from='',0,$location,$radius,$range,$multiple));

                    $getAllProduct = $this->get_products($subSubCat->cat_id,$sort,$counter,$per_page,1,$location,$radius,$range,$multiple);

                    $html['tab1'] = "";
                    $html['tab2'] = "";                    

                    if(count($getAllProduct)<=0){
                        $html['tab1'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
                        $html['tab2'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
                    }

                    $html['tab1'] .= "<div class='cloths-row'>";
                    $row_start = 0;
                    foreach ($getAllProduct as $i => $wish_list){
                        $class = '';
                        $feature = '';

                        if(($i+1)%2==0 && $i>0){
                            $class = ' clths-rgt mrg-rgt-0';
                        }else{
                            $class = '';
                        }

                        if($wish_list['prod_feature']==1){
                            $feature = 'featured-absolt';
                        }else{
                            $feature = '';
                        }

                        if($row_start == 1){
                            $html['tab1'] .= "<div class='cloths-row'>";
                            $row_start = 0;
                        }

                        $price_1 = '';
                        $price_2 = '';

                        if($wish_list['prod_onsale']==1){
                            $price_1 = "Price:<span class='sp1'>$".$wish_list['sale_price']."</span><span class='sp2'>$".$wish_list['prod_price']."</span>";
                            $price_2 = "Price:<span class='sp3'>$".$wish_list['sale_price']."</span><span class='sp4'>$".$wish_list['prod_price']."</span>";
                        }else{
                            $price_1 = "Price:<span class='sp3'>$".$wish_list['prod_price']."</span>";
                            $price_2 = "Price:<span class='sp1'>$".$wish_list['prod_price']."</span>";
                        }

                        $active = '';
                        if( is_numeric( $this->input->get('user_id') ) ){
                            if($this->check_wishlist_item($wish_list['prod_id'],$this->input->get('user_id'))){
                                $active = 'active';
                            }else{
                                $active = '';
                            }
                        }else{
                            $active = '';
                        }

                        $html['tab1'] .= "<div class='cloths-bx".$class."'><div class='cloths-bx-img-sec'><a href='javascript:void' class='prod_click' data-id='".$wish_list['prod_id']."'><img src='".base_url()."resources/prod_images/".$wish_list['img_name']."' alt='' title='' class='img-responsive'/></a><span class='".$feature."'></span></div>";
                        $html['tab1'] .= "<div class='cloths-bx-hed'><a href='javascript:void' data-toggle='tooltip' data-placement='top' title='Products Detail' data-id='".$wish_list['prod_id']."' class='prod_click'>".$wish_list['prod_title']."</a></div>";
                        $html['tab1'] .= "<div class='cloths-bx-lowr1'><a href='javascript:void' data-toggle='tooltip' data-placement='top' class='store_click'  data-id='".$wish_list['prod_store_id']."'>".$wish_list['store_name']."</a><p>".$price_1."</p></div>";
                        $html['tab1'] .= "<div class='cloths-bx-lowr2'><input type='checkbox' class='compare-prod' data-id='".$wish_list['prod_id']."' id='c".$wish_list['prod_id']."' name='cc'/><label for='c".$wish_list['prod_id']."'><span></span><p>Compare</p></label><a href='javascript:void' data-id='".$wish_list['prod_id']."' class='not_logged_in wish ".$active." wish".$wish_list['prod_id']."' data-toggle='tooltip' data-placement='bottom' title='Add to Wishlist'></a></div></div>";

                        $html['tab2'] .= "<div class='cloths-bx-2'><div class='cloths-bx-2-img'><a href='javascript:void' class='prod_click' data-id='".$wish_list['prod_id']."'><img src='".base_url()."resources/prod_images/".$wish_list['img_name']."' alt='' title='' class='img-responsive'/></a><span class='".$feature."'></span></div><div class='cloths-bx-2-rgt'>";
                        $html['tab2'] .= "<div class='cloths-bx-hed2'><a href='javascript:void' data-toggle='tooltip' data-placement='top' title='Products Detail' data-id='".$wish_list['prod_id']."' class='prod_click'>".$wish_list['prod_title']."</a></div>";
                        $html['tab2'] .= "<div class='cloths-bx-lowr3'><a href='javascript:void' data-toggle='tooltip' data-placement='top' title='Seller Store'>".$wish_list['store_name']."</a><p>".$price_2."</p></div>";
                        $html['tab2'] .= "<div class='cloths-bx-lowr4'><input type='checkbox' class='compare-prod' data-id='".$wish_list['prod_id']."' id='c".$wish_list['prod_id']."' name='cc' /><label for='c".$wish_list['prod_id']."'><span></span><p>Compare</p></label><a href='javascript:void' data-id='".$wish_list['prod_id']."' class='not_logged_in wish ".$active." wish".$wish_list['prod_id']."' data-toggle='tooltip' data-placement='bottom' title='Add to Wishlist'></a></div></div></div>";

                        if(($i+1)%2==0 && $i>0){
                            $html['tab1'] .= "</div>";
                            $row_start = 1;
                        }
                    }

                    $html['counter'] = $counter + $per_page;

                }else{
                    $html['tab1'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
                    $html['tab2'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
                    $html['data'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
                    $html['re'] = 1;
                }
            }else{
                $html['tab1'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
                $html['tab2'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
                $html['data'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
                $html['re'] = 2; // no sub cat exist
            }
        }else{
            $html['tab1'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
            $html['tab2'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
            $html['data'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
            $html['re'] = 3; // No main cat exist
        }

        echo json_encode($html);
    }

    public function get_products($cat_id='',$sort='',$to='',$from='',$is_limit='',$location='',$radius='',$range='',$multiple)
    {
        if ($location != '' && $radius != "") {
            $location_query = $this->location($location,$radius)." AND";
        }
        $query = "        
                    SELECT 
                    p.prod_id, p.prod_title, p.prod_onsale, p.prod_price, p.sale_price, p.prod_url,p.prod_description, p.prod_feature,p.prod_store_id,
                    u.user_id, u.user_fname, u.user_lname, s.store_id, s.store_name, s.store_url, 
                    i.img_name, i.img_label

                    FROM tbl_products p
                    INNER JOIN tbl_user u ON p.prod_user_id = u.user_id
                    INNER JOIN tbl_stores s ON s.user_id = u.user_id
                    INNER JOIN tbl_product_images i ON i.img_prod_id = p.prod_id
                    LEFT JOIN tbl_product_filters pf ON pf.prod_id = p.prod_id 
                    LEFT JOIN tbl_product_filters_detail pfd ON pf.id = pfd.prod_filter_id 
                    WHERE $location_query $multiple 
                    p.prod_status = 1 AND
                    p.prod_is_delete = 0 AND
                    p.prod_cat_id = $cat_id AND
                    s.store_is_hide = 0 AND
                    u.user_is_delete = 0 AND
                    u.user_status = 1 AND p.prod_price >= $range[0] AND p.prod_price <= $range[1] 
                    GROUP BY p.prod_id
                    ORDER BY $sort 
                    ";

        if ($is_limit == 1) {
            $query .= " LIMIT " . $to . "," . $from;
        }

        $result = $this->db->query($query)->result_array();
        return $result;
    
    }

    function check_wishlist_item($prod_id = '',$user_id = ''){
        $query = "SELECT *
                FROM tbl_wish_list
                WHERE product_id = $prod_id AND user_id = $user_id";
        $results = $this->db->query($query);
        if (count($results->row()) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getmainProductsListing(){

        $mainCatUrl = $this->input->get('mainCatUrl');

        $is_logged_in = $this->input->get('is_logged_in');        

        $per_page = $this->input->get('per_page');

        $location = $this->input->get('location');
        $radius = $this->input->get('radius');

        $counter = $this->input->get('counter') * $per_page;

        $data['counter'] = $counter;
        $data['store_id'] = $store_id;

        $sort = '';

        if ($this->input->get('sort_by') != '') {
            $data['sort_by'] = @$this->input->get('sort_by');
            if ($data['sort_by'] == "new") {
                $sort = 'p.prod_id DESC';
            } else if ($data['sort_by'] == "featured") {
                $sort = 'p.prod_feature DESC';
            } else if ($data['sort_by'] == "price_lh") {
                $sort = ' (CASE WHEN p.prod_onsale = 1 THEN sale_price ELSE p.prod_price END) ASC';
            } else if ($data['sort_by'] == "pricehl") {
                $sort = ' (CASE WHEN p.prod_onsale = 1 THEN sale_price ELSE p.prod_price END) DESC';
            }
        }

        /*$data['mainCatUrl'] = $mainCatUrl;
        $data['subCatUrl'] = $subCatUrl;
        $data['subSubCatUrl'] = $subSubCatUrl;
        $data['per_page'] = $per_page;
        $data['counter'] = $counter;
        $data['sort'] = $sort;
        $data['radius'] = $radius;
        $data['location'] = $location;

        echo "<pre>";
        print_r($data);
        exit;*/

        $html['cat_html'] = $this->common_model->get_categories_html();

        $count = 0;

        $html['data'] = '';

        $all_cat_id = array();

        $mainCat = $this->common_model->commonselect4('tbl_categories','cat_url', $mainCatUrl, 'cat_parent_id', 0, 'cat_status', 1, 'cat_is_delete', 0, 'cat_sort', 'ASC')->row();
        if(!empty($mainCat)){

            array_push($all_cat_id,$mainCat->cat_id);
            
            $subCat = $this->common_model->commonselect3('tbl_categories', 'cat_parent_id', $mainCat->cat_id, 'cat_status', 1, 'cat_is_delete', 0, 'cat_sort', 'ASC')->result();
            
            if(!empty($subCat)){

                foreach ($subCat as $i => $cat) {

                    $subSubCat = $this->common_model->commonselect3('tbl_categories', 'cat_parent_id', $cat->cat_id, 'cat_status', 1, 'cat_is_delete', 0, 'cat_sort', 'ASC')->result();
                    array_push($all_cat_id,$cat->cat_id);
                    
                    if(!empty($subSubCat)){
                        foreach ($subSubCat as $i => $sub_cat) {
                            array_push($all_cat_id,$sub_cat->cat_id);
                        }
                    }                    
                }
            }else{
                $html['tab1'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
                $html['tab2'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
                /*$html['data'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No Data Found</div>";*/
                $html['re'] = 2; // No main cat exist
            }
        }else{
            $html['tab1'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
            $html['tab2'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
            /*$html['data'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No Data Found</div>";*/
            $html['re'] = 3; // No main cat exist
        }

        $html['total_rows'] = count($this->get_all_products($all_cat_id,$sort,$to = 0,$from='',0,$location,$radius));
        $getAllProduct = $this->get_all_products($all_cat_id,$sort,$counter,$per_page,1,$location,$radius);

        $html['tab1'] = "";
        $html['tab2'] = "";

        if(count($getAllProduct)<=0){
            $html['tab1'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
            $html['tab2'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
        }

        $html['tab1'] .= "<div class='cloths-row'>";
        $row_start = 0;

        foreach ($getAllProduct as $i => $wish_list){
            $class = '';
            $feature = '';

            if(($i+1)%2==0 && $i>0){
                $class = ' clths-rgt mrg-rgt-0';
            }else{
                $class = '';
            }

            if($wish_list['prod_feature']==1){
                $feature = 'featured-absolt';
            }else{
                $feature = '';
            }

            if($row_start == 1){
                $html['tab1'] .= "<div class='cloths-row'>";
                $row_start = 0;
            }

            $price_1 = '';
            $price_2 = '';

            if($wish_list['prod_onsale']==1){
                $price_1 = "Price:<span class='sp1'>$".$wish_list['sale_price']."</span><span class='sp2'>$".$wish_list['prod_price']."</span>";
                $price_2 = "Price:<span class='sp3'>$".$wish_list['sale_price']."</span><span class='sp4'>$".$wish_list['prod_price']."</span>";
            }else{
                $price_1 = "Price:<span class='sp3'>$".$wish_list['prod_price']."</span>";
                $price_2 = "Price:<span class='sp1'>$".$wish_list['prod_price']."</span>";
            }

            $active = '';
            if( is_numeric( $this->input->get('user_id') ) ){
                if($this->check_wishlist_item($wish_list['prod_id'],$this->input->get('user_id'))){
                    $active = 'active';
                }else{
                    $active = '';
                }
            }else{
                $active = '';
            }

            $html['tab1'] .= "<div class='cloths-bx".$class."'><div class='cloths-bx-img-sec'><a href='javascript:void' class='prod_click' data-id='".$wish_list['prod_id']."'><img src='".base_url()."resources/prod_images/".$wish_list['img_name']."' alt='' title='' class='img-responsive'/></a><span class='".$feature."'></span></div>";
            $html['tab1'] .= "<div class='cloths-bx-hed'><a href='javascript:void' data-toggle='tooltip' data-placement='top' title='Products Detail' data-id='".$wish_list['prod_id']."' class='prod_click'>".$wish_list['prod_title']."</a></div>";
            $html['tab1'] .= "<div class='cloths-bx-lowr1'><a href='javascript:void' data-toggle='tooltip' data-placement='top' class='store_click'  data-id='".$wish_list['prod_store_id']."'>".$wish_list['store_name']."</a><p>".$price_1."</p></div>";
            $html['tab1'] .= "<div class='cloths-bx-lowr2'><input type='checkbox' class='compare-prod' data-id='".$wish_list['prod_id']."' id='c".$wish_list['prod_id']."' name='cc'/><label for='c".$wish_list['prod_id']."'><span></span><p>Compare</p></label><a href='javascript:void' data-id='".$wish_list['prod_id']."' class='not_logged_in wish ".$active." wish".$wish_list['prod_id']."' data-toggle='tooltip' data-placement='bottom' title='Add to Wishlist'></a></div></div>";

            $html['tab2'] .= "<div class='cloths-bx-2'><div class='cloths-bx-2-img'><a href='javascript:void' class='prod_click' data-id='".$wish_list['prod_id']."'><img src='".base_url()."resources/prod_images/".$wish_list['img_name']."' alt='' title='' class='img-responsive'/></a><span class='".$feature."'></span></div><div class='cloths-bx-2-rgt'>";
            $html['tab2'] .= "<div class='cloths-bx-hed2'><a href='javascript:void' data-toggle='tooltip' data-placement='top' title='Products Detail' data-id='".$wish_list['prod_id']."' class='prod_click'>".$wish_list['prod_title']."</a></div>";
            $html['tab2'] .= "<div class='cloths-bx-lowr3'><a href='javascript:void' data-toggle='tooltip' data-placement='top' title='Seller Store'>".$wish_list['store_name']."</a><p>".$price_2."</p></div>";
            $html['tab2'] .= "<div class='cloths-bx-lowr4'><input type='checkbox' class='compare-prod' data-id='".$wish_list['prod_id']."' id='c".$wish_list['prod_id']."' name='cc' /><label for='c".$wish_list['prod_id']."'><span></span><p>Compare</p></label><a href='javascript:void' data-id='".$wish_list['prod_id']."' class='not_logged_in wish ".$active." wish".$wish_list['prod_id']."' data-toggle='tooltip' data-placement='bottom' title='Add to Wishlist'></a></div></div></div>";

            if(($i+1)%2==0 && $i>0){
                $html['tab1'] .= "</div>";
                $row_start = 1;
            }
        }

        $html['counter'] = $counter + $per_page;

        /*echo "<pre>";
        echo "Total = ".$html['total_rows'];
        print_r($getAllProduct);
        exit;*/
        echo json_encode($html);
    }

    public function get_all_products($all_id='',$sort='',$to='',$from='',$is_limit='',$location,$radius)
    {
        $all_id = implode(', ', $all_id);

        if ($location != '' && $radius != "") {
            $location_query = $this->location($location,$radius)." AND";
        }

        $query = "        
                    SELECT 
                    p.prod_id, p.prod_title, p.prod_onsale, p.prod_price, p.sale_price, p.prod_url,p.prod_description, p.prod_feature,p.prod_store_id,
                    u.user_id, u.user_fname, u.user_lname, s.store_id, s.store_name, s.store_url, 
                    i.img_name, i.img_label

                    FROM tbl_products p
                    INNER JOIN tbl_user u ON p.prod_user_id = u.user_id
                    INNER JOIN tbl_stores s ON s.user_id = u.user_id
                    INNER JOIN tbl_product_images i ON i.img_prod_id = p.prod_id
                    WHERE $location_query
                    p.prod_status = 1 AND
                    p.prod_is_delete = 0 AND
                    p.prod_cat_id IN ($all_id) AND
                    s.store_is_hide = 0 AND
                    u.user_is_delete = 0 AND
                    u.user_status = 1                
                    GROUP BY p.prod_id
                    ORDER BY $sort 
                    ";
            
        if ($is_limit == 1) {
            $query .= " LIMIT " . $to . "," . $from;
        }

        $result = $this->db->query($query)->result_array();
        return $result;
    
    }

    public function location($location = '',$radius='') {

        $storeurl = "";

        if ($location != '' && $radius != "") {

            $location = explode('|',$location);

            $lat = $location[0];
            $lng = $location[1];

            $stores = array();

            $storeflag = true;

            $query = "
                    SELECT *
                    FROM tbl_stores store
                    INNER JOIN tbl_user user ON store.user_id = user.user_id
                    WHERE
                    store_is_hide = 0 AND user.user_is_delete = 0 AND user.user_status = 1
                    ";
            $data['store'] = $this->db->query($query)->result();

            foreach ($data['store'] as $store) {

                if ($store->store_longitude != '' && $store->store_latitude != '') {
                    if (distance($lat, $lng, $store->store_latitude, $store->store_longitude, "M") <= $radius) {
                        if ($storeflag) {
                            $storeurl = $storeurl . "`prod_store_id` = " . $store->store_id;
                            array_push($stores, $store);
                            $storeflag = false;
                        } else {
                            $storeurl = $storeurl . " OR `prod_store_id` = " . $store->store_id;
                        }
                    }
                }
            }
        }

        if($storeurl != ''){
            return "($storeurl)";
        }else {
            return "(prod_store_id = 0)";
        }
    }

    public function getsubProductsListing(){

        $mainCatUrl = $this->input->get('mainCatUrl');
        $subCatUrl = $this->input->get('subCatUrl');

        $is_logged_in = $this->input->get('is_logged_in');        

        $per_page = $this->input->get('per_page');

        $location = $this->input->get('location');
        $radius = $this->input->get('radius');

        $counter = $this->input->get('counter') * $per_page;

        $data['counter'] = $counter;
        $data['store_id'] = $store_id;

        $sort = '';

        if ($this->input->get('sort_by') != '') {
            $data['sort_by'] = @$this->input->get('sort_by');
            if ($data['sort_by'] == "new") {
                $sort = 'p.prod_id DESC';
            } else if ($data['sort_by'] == "featured") {
                $sort = 'p.prod_feature DESC';
            } else if ($data['sort_by'] == "price_lh") {
                $sort = ' (CASE WHEN p.prod_onsale = 1 THEN sale_price ELSE p.prod_price END) ASC';
            } else if ($data['sort_by'] == "pricehl") {
                $sort = ' (CASE WHEN p.prod_onsale = 1 THEN sale_price ELSE p.prod_price END) DESC';
            }
        }

        $html['cat_html'] = $this->common_model->get_categories_html();

        $count = 0;

        $html['data'] = '';

        $all_cat_id = array();

        $mainCat = $this->common_model->commonselect4('tbl_categories','cat_url', $mainCatUrl, 'cat_parent_id', 0, 'cat_status', 1, 'cat_is_delete', 0, 'cat_sort', 'ASC')->row();
        
        if(!empty($mainCat)){
            
            $subCat = $this->common_model->commonselect4('tbl_categories', 'cat_parent_id', $mainCat->cat_id, 'cat_url', $subCatUrl, 'cat_status', 1, 'cat_is_delete', 0, 'cat_sort', 'ASC')->row();
            
            if(!empty($subCat)){

                $subSubCat = $this->common_model->commonselect3('tbl_categories', 'cat_parent_id', $subCat->cat_id, 'cat_status', 1, 'cat_is_delete', 0, 'cat_sort', 'ASC')->result();
                
                array_push($all_cat_id,$subCat->cat_id);
                
                if(!empty($subSubCat)){
                    foreach ($subSubCat as $i => $sub_cat) {
                        array_push($all_cat_id,$sub_cat->cat_id);
                    }
                }

            }else{
                $html['tab1'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
                $html['tab2'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
                /*$html['data'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No Data Found</div>";*/
                $html['re'] = 2; // No main cat exist
            }
        }else{
            $html['tab1'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
            $html['tab2'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
            /*$html['data'] = "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No Data Found</div>";*/
            $html['re'] = 3; // No main cat exist
        }

        $html['total_rows'] = count($this->get_all_products($all_cat_id,$sort,$to = 0,$from='',0,$location,$radius));
        $getAllProduct = $this->get_all_products($all_cat_id,$sort,$counter,$per_page,1,$location,$radius);

        $html['tab1'] = "";
        $html['tab2'] = "";

        if(count($getAllProduct)<=0){
            $html['tab1'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
            $html['tab2'] .= "<div class='no-data' style=\"font-size: 20px;font-family: 'source_sans_prosemibold';font-weight: normal;color: #000;float:left;width:100%;text-align: center;\">No items found.</div>";
        }

        $html['tab1'] .= "<div class='cloths-row'>";
        $row_start = 0;

        foreach ($getAllProduct as $i => $wish_list){
            $class = '';
            $feature = '';

            if(($i+1)%2==0 && $i>0){
                $class = ' clths-rgt mrg-rgt-0';
            }else{
                $class = '';
            }

            if($wish_list['prod_feature']==1){
                $feature = 'featured-absolt';
            }else{
                $feature = '';
            }

            if($row_start == 1){
                $html['tab1'] .= "<div class='cloths-row'>";
                $row_start = 0;
            }

            $price_1 = '';
            $price_2 = '';

            if($wish_list['prod_onsale']==1){
                $price_1 = "Price:<span class='sp1'>$".$wish_list['sale_price']."</span><span class='sp2'>$".$wish_list['prod_price']."</span>";
                $price_2 = "Price:<span class='sp3'>$".$wish_list['sale_price']."</span><span class='sp4'>$".$wish_list['prod_price']."</span>";
            }else{
                $price_1 = "Price:<span class='sp3'>$".$wish_list['prod_price']."</span>";
                $price_2 = "Price:<span class='sp1'>$".$wish_list['prod_price']."</span>";
            }

            $active = '';
            if( is_numeric( $this->input->get('user_id') ) ){
                if($this->check_wishlist_item($wish_list['prod_id'],$this->input->get('user_id'))){
                    $active = 'active';
                }else{
                    $active = '';
                }
            }else{
                $active = '';
            }

            $html['tab1'] .= "<div class='cloths-bx".$class."'><div class='cloths-bx-img-sec'><a href='javascript:void' class='prod_click' data-id='".$wish_list['prod_id']."'><img src='".base_url()."resources/prod_images/".$wish_list['img_name']."' alt='' title='' class='img-responsive'/></a><span class='".$feature."'></span></div>";
            $html['tab1'] .= "<div class='cloths-bx-hed'><a href='javascript:void' data-toggle='tooltip' data-placement='top' title='Products Detail' data-id='".$wish_list['prod_id']."' class='prod_click'>".$wish_list['prod_title']."</a></div>";
            $html['tab1'] .= "<div class='cloths-bx-lowr1'><a href='javascript:void' data-toggle='tooltip' data-placement='top' class='store_click'  data-id='".$wish_list['prod_store_id']."'>".$wish_list['store_name']."</a><p>".$price_1."</p></div>";
            $html['tab1'] .= "<div class='cloths-bx-lowr2'><input type='checkbox' class='compare-prod' data-id='".$wish_list['prod_id']."' id='c".$wish_list['prod_id']."' name='cc'/><label for='c".$wish_list['prod_id']."'><span></span><p>Compare</p></label><a href='javascript:void' data-id='".$wish_list['prod_id']."' class='not_logged_in wish ".$active." wish".$wish_list['prod_id']."' data-toggle='tooltip' data-placement='bottom' title='Add to Wishlist'></a></div></div>";

            $html['tab2'] .= "<div class='cloths-bx-2'><div class='cloths-bx-2-img'><a href='javascript:void' class='prod_click' data-id='".$wish_list['prod_id']."'><img src='".base_url()."resources/prod_images/".$wish_list['img_name']."' alt='' title='' class='img-responsive'/></a><span class='".$feature."'></span></div><div class='cloths-bx-2-rgt'>";
            $html['tab2'] .= "<div class='cloths-bx-hed2'><a href='javascript:void' data-toggle='tooltip' data-placement='top' title='Products Detail' data-id='".$wish_list['prod_id']."' class='prod_click'>".$wish_list['prod_title']."</a></div>";
            $html['tab2'] .= "<div class='cloths-bx-lowr3'><a href='javascript:void' data-toggle='tooltip' data-placement='top' title='Seller Store'>".$wish_list['store_name']."</a><p>".$price_2."</p></div>";
            $html['tab2'] .= "<div class='cloths-bx-lowr4'><input type='checkbox' class='compare-prod' data-id='".$wish_list['prod_id']."' id='c".$wish_list['prod_id']."' name='cc'/><label for='c".$wish_list['prod_id']."'><span></span><p>Compare</p></label><a href='javascript:void' data-id='".$wish_list['prod_id']."' class='not_logged_in wish ".$active." wish".$wish_list['prod_id']."' data-toggle='tooltip' data-placement='bottom' title='Add to Wishlist'></a></div></div></div>";

            if(($i+1)%2==0 && $i>0){
                $html['tab1'] .= "</div>";
                $row_start = 1;
            }
        }

        $html['counter'] = $counter + $per_page;

        echo json_encode($html);
    }
   
}