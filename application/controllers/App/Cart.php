<?php
class Cart extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('common/common_model');
        $this->load->helper(array('form', 'url'));
        $this->load->model('front/guidetoeducki/Guidetoeducki_model');
        $this->load->model('front/faq/faq_model');
        $this->load->model('front/contact_model');
        $this->data['url'] = base_url();
        $this->load->model('front/cart/cart_model');
        $this->load->model('front/address/address_model');
        if ( !empty($_GET) ) $_GET = array_stripslash($_GET);
        if ( !empty($_POST) ) $_POST = array_stripslash($_POST);
        date_default_timezone_set('America/New_York');
        $this->load->library('upload');
        $this->output->set_header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        $this->output->set_header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    }

    public function index()
    {
        echo 'Code.';
    }

    public function displayCart(){
        $html = array();
        $cartId = $this->input->get('cartId');
        $data['userId'] = $this->input->get('userId');
        if($cartId != ''){
            $isCartExist = $this->db->query('SELECT * FROM tbl_cart WHERE cart_id = '.$cartId)->row();

            if(!empty($isCartExist) > 0){
                $this->setCartSession($cartId);
            }else{
                $this->cart_model->removeCart($cartId);
            }
        }

        if ($cartId != '') {
            $data['cart_product_details'] = getProductPricesApp($cartId);
            //echo $this->db->last_query();
            foreach ($data['cart_product_details'] as $product) {
                $product_details = getproduct_details_2($product->cart_prod_id);
                if ($product_details == '') {
                    $this->cart_model->removeCartItem($cartId, $product->cart_prod_id);
                }else{
                    $getCartTotalQty = $this->db->query("SELECT 
                                                SUM(cart_prod_qnty) as total 
                                                FROM tbl_cart_detail 
                                                WHERE cart_id=$cartId 
                                                AND cart_prod_id=$product->cart_prod_id")->row();
                    if($product_details->qty < $getCartTotalQty->total){
                        $this->cart_model->removeCartItem($cartId, $product->cart_prod_id);
                    }
                }
            }
            $data['cart_product_details'] = getProductPricesApp($cartId);
            $data['shipment_sub_total'] = getShipmentSubTotalApp($cartId);
            $data['cart_sub_total'] = getCartSubTotalApp($cartId);
            $data['discount_amount'] = getStoreDiscount($cartId);
            $data['total_shipment_amount'] = $data['shipment_sub_total'] - $data['discount_amount'] + $data['cart_sub_total'];
            $data['main_category_url'] = getFirstMainCategory()->cat_url;
            $data['title'] = "Shopping Cart";
            $html['cart_html'] = $this->load->view("app/cart", $data, true);
            $html['isOk'] = 1;
        } else {
            $html['cart_html'] = '';
            $html['isOk'] = 0;
        }

        $html['session'] = $this->session->all_userdata();
        echo json_encode($html);
    }

    public function getCartCookie(){
        $cartId = get_cookie('educki_cartId');
        if($cartId !='' ){
            $data['cartId'] = $cartId;
            $data['ok'] = 1;
        }else{
            $data['cartId'] = '';
            $data['ok'] = 0;
        }
        echo json_encode($data);
    }

     public function getUserCookie(){
        $userId = get_cookie('educki_userId');
        if($cartId !='' ){
            $data['user'] = $userId;
            $data['ok'] = 1;
        }else{
            $data['user'] = '';
            $data['ok'] = 0;
        }
        echo json_encode($data);
    }

    

    public function add_to_cart_ajax() {
        $product_id = $this->input->post('prod_id');
        $filters = stripslashes($this->input->post('filter_data'));
        $user_id = $this->input->post('user_id');
        $userId = $this->input->post('user_id');
        $cartId = $this->input->post('cart_id');
        $is_cart = 0;
        if($cartId != ''){
            $is_cart = 1;
        }

        $resp = array();
        if($cartId <= 0){ // If cart ID Not exist.
            if($user_id > 0){ // If user Login.
                $cart_data = $this->db->query("SELECT cart_id FROM tbl_cart WHERE cart_user_id = $userId")->row();
                if(count($cart_data) <= 0){ // If not in cart.
                    $this->db->insert('tbl_cart', array('cart_user_id'=>$userId));
                    $cartId = $this->db->insert_id();
                    $this->session->set_userdata('cartId', $cartId);
                }else{ // if cart exist.
                    $cartId = $cart_data->cart_id;
                    $this->session->set_userdata('cartId', $cartId);
                }
                $this->setCartSession($cartId);
            }else{ // If the user is not login.
                
                $this->load->helper('string');
                if ($is_cart) {
                    $isCartExist = $this->db->query('SELECT * FROM tbl_cart WHERE cart_id = '.$cartId)->row();
                    if(count($isCartExist) > 0){
                        ;
                    }else{
                        $this->cart_model->removeCart($cartId);

                        $user_id = random_string('alnum', 16); // Make the random string as user ID
                        // Add UserID to DB
                        $this->db->insert('tbl_cart', array('cart_user_id'=>$user_id));
                        $cartId = $this->db->insert_id();
                        // $this->session->set_userdata('cartId', $cartId);
                        // $this->setCartSession($cartId, true, $user_id);
                    }
                }else{
                    $user_id = random_string('alnum', 16); // Make the random string as user ID
                    // Add UserID to DB
                    $this->db->insert('tbl_cart', array('cart_user_id'=>$user_id));
                    $cartId = $this->db->insert_id();
                    // $this->session->set_userdata('cartId', $cartId);
                    // $this->setCartSession($cartId, true, $user_id);
                }
            }
        }


        // Prod Reorder Case
        $order_id = $this->input->post('order_id');
        if ($order_id) { // Reorder product
            $this->load->model('front/order/order_model');
            $this->data['order_info'] = $this->order_model->get_order($order_id);
            
            if (count($this->data['order_info']) > 0) {

                foreach ($this->data['order_info'] as $key => $prds) {
                    $prod_ids = $prds['order_prod_id'];
                    $r_inventory = $this->cart_model->getProduct($prod_ids)->result();
                    $r_inventory = cgetproduct_details_2($prod_ids);
                    
                    

                    if ($r_inventory[0]->qty > 0) {
                        if ($r_inventory[0]->qty >= $prds['order_prod_qty']) {
                            $qty = $prds['order_prod_qty'];
                        } else {
                            $qty = $r_inventory[0]->qty;
                        }
                        $getCartTotalQty = $this->db->query("SELECT 
                                                SUM(cart_prod_qnty) as total 
                                                FROM tbl_cart_detail 
                                                WHERE cart_id=$cartId 
                                                AND cart_prod_id=$prod_ids")->row();
                        if( ($getCartTotalQty->total+($qty)) > $r_inventory[0]->qty ){
                            //Not Update
                            $isUpdate= 0;
                            $resp['message'] = getProdName($prod_ids)->prod_title . " out of stock.<br/>";
                            $resp['cart_items'] = totalCartItemsApp($cartId);
                            $resp['cart_sub_total'] = number_format(getCartSubTotalApp($cartId), 2, '.', '');
                            $resp['status'] = 'limit-ok';

                        }else{
                             //update
                            $isUpdate= 1;
                            $qty = $getCartTotalQty->total+($qty);
                            $stat = $this->cart_model->addreorderProduct($cartId, $prod_ids, $qty, $prds['order_prod_filters']);

                            //var_dump($stat);die;


                            // echo $this->db->last_query();
                            $resp['cart_items'] = totalCartItems();
                            $resp['cart_sub_total'] = number_format(getCartSubTotal(), 2, '.', '');
                            if($stat == 1){
                                $resp['status'] = 'ok';
                                $resp['message'] .= getProdName($prod_ids)->prod_title . " is added to cart.<br/>";
                            }else{
                                $resp['message'] .= getProdName($prod_ids)->prod_title . " is updated to cart.<br/>";
                                $resp['status'] = 'ready-ok';
                            }

                            /*$resp['message'] = "ProductId " . $prod_ids . " is added to cartId " . $cartId;
                            $resp['cart_items'] = totalCartItemsApp($cartId);
                            $resp['cart_sub_total'] = number_format(getCartSubTotalApp($cartId), 2, '.', '');
                            if($stat == 1){
                                $resp['status'] = 'ok';
                            }else{
                                $resp['status'] = 'ready-ok';
                            }*/
                        }
                    }else{
                        $resp['message'] .= getProdName($prod_ids)->prod_title . " out of stock.<br/>";
                        $resp['status'] = 'limit-ok';
                        
                    }
                }
            }else {
                $resp['status'] = 'not-ok';
            }

            $data['cart_product_details'] = getProductPricesApp($cartId);
            //echo $this->db->last_query();
            foreach ($data['cart_product_details'] as $product) {
                $product_details = getproduct_details_2($product->cart_prod_id);
                if ($product_details == '') {
                    $this->cart_model->removeCartItem($cartId, $product->cart_prod_id);
                }
            }
            $resp['cartId'] = $cartId;
            $resp['user_id'] = $user_id;
            return print_r(json_encode($resp));
        } else { // add product into the cart
           
            $data['productData'] = $this->common_model->commonselect('tbl_products', 'prod_id', $product_id);
            if ($data['productData']->num_rows() > 0) {
                
                $row = $data['productData']->row();
                if ($row->qty > 0) {
                    
                    $qty = 1;
                    $data['qtyAdded'] = $qty;
                    
                    $returnType = $this->cart_model->addProduct($cartId, $product_id, $qty);
                    // echo $returnType;
                    $resp['cart_items'] = totalCartItemsApp($cartId);
                    $resp['cart_sub_total'] = number_format(getCartSubTotalApp($cartId), 2, '.', '');
                    //echo $returnType;
                    if($returnType == 1){
                        $resp['status'] = 'ok';
                    }else if($returnType == 2){
                        
                        $resp['status'] = 'ready-ok';
                    }else{
                        $resp['status'] = 'limit-ok';
                    }
                } else {
                    
                    $resp['status'] = 'not-ok';
                }
            }
            
            $data['cart_product_details'] = getProductPricesApp($cartId);
            //echo $this->db->last_query();
            foreach ($data['cart_product_details'] as $product) {
                $product_details = getproduct_details_2($product->cart_prod_id);
                if ($product_details == '') {
                    $this->cart_model->removeCartItem($cartId, $product->cart_prod_id);
                }
            }
            $resp['cartId'] = $cartId;

            //print_r(json_encode($resp));
        }

        $resp['user_id'] = $user_id;
        
        print_r(json_encode($resp));

    }



  
    function setCartSession($cartId, $cookieAlso = false, $userId = false) {
        $this->session->set_userdata('isCart', TRUE);
        $this->session->set_userdata('cartId', $cartId);
        $expires = time() + (5400000);
        if ($cookieAlso) {
            $cookie1 = array(
                'name' => 'isCart',
                'value' => TRUE,
                'expire' => $expires,
                'domain' => $this->data['url'],
                'path' => '/',
                'prefix' => 'educki_',
            );
            $cookie2 = array(
                'name' => 'cartId',
                'value' => $cartId,
                'expire' => $expires,
                'domain' => $this->data['url'],
                'path' => '/',
                'prefix' => 'educki_',
            );
            $cookie3 = array(
                'name' => 'userId',
                'value' => $userId,
                'expire' => $expires,
                'domain' => $this->data['url'],
                'path' => '/',
                'prefix' => 'educki_',
            );
            
//            set_cookie($cookie1);
//            set_cookie($cookie2);
//            set_cookie($cookie3);

            // delete_cookie('isCart');
            // delete_cookie('cartId');
            // delete_cookie('userId');
            // delete_cookie('educki_isCart');
            // delete_cookie('educki_cartId');
            // delete_cookie('educki_userId');

            setcookie('isCart',"TRUE",$expires,$this->data['url'],'/','educki_');
            setcookie('cartId',$cartId,$expires,$this->data['url'],'/','educki_');
            setcookie('userId',$userId,$expires,$this->data['url'],'/','educki_');
            setcookie('educki_isCart', "TRUE", $expires, '/');
            setcookie('educki_cartId', $cartId, $expires, '/');
            setcookie('educki_userId', $userId, $expires, '/');
        }
    }


    public function changeitems(){
        $cardId = $this->input->post('cartId');
        $this->setCartSession($cardId);
        $cardDetailId = $this->input->post('cart_detail_id');
        $qty = $this->input->post('qty');
        $oldQty = $this->input->post('oldQty');
        $prodId = $this->input->post('prodId');
        $prod_details = getproduct_details($prodId);
        if ($prod_details == '') {
            $this->cart_model->removeCartItem($cardId, $prodId);
            echo json_encode(array('status' => 'Error', 'msg' => 'Product is Not Avaliable'));
            die;
        } else {
            $getCartTotalQty = $this->db->query("SELECT 
                                                SUM(cart_prod_qnty) as total 
                                                FROM tbl_cart_detail 
                                                WHERE cart_detail_id=$cardDetailId 
                                                AND cart_prod_id=$prodId")->row();
            $isUpdate= 1;
            if(($getCartTotalQty->total+($qty-$oldQty)) > $prod_details->qty){
                //Not Update
                 $isUpdate= 0;
            }else{
                //update
                 $isUpdate= 1;
            }
            if($isUpdate == 1){
                $prodPrice = 0.0;
                $sDate = date('Y-m-d', strtotime($prod_details->sale_start_date));
                $eDate = date('Y-m-d', strtotime($prod_details->sale_end_date));
                $tDate = date('Y-m-d');
                if ($prod_details->prod_onsale== 1) {
                    if ($prod_details->sale_start_date != '' && $prod_details->sale_end_date != '') {
                        if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                            $prodPrice = number_format($prod_details->sale_price, 2, '.', '');                        
                        } else {
                            $prodPrice = number_format($prod_details->prod_price, 2, '.', '');                        
                        }
                    } else if ($prod_details->sale_start_date != '') {
                        if ($tDate >= $sDate) {                        
                            $prodPrice = number_format($prod_details->sale_price, 2, '.', '');
                        } else {
                            $prodPrice = number_format($prod_details->prod_price, 2, '.', '');
                        }
                    } else if($prod_details->sale_end_date != '') {
                        if($tDate <= $eDate) {
                            $prodPrice = number_format($prod_details->sale_price, 2, '.', '');
                        } else {
                            $prodPrice = number_format($prod_details->prod_price, 2, '.', '');
                        }
                    }else {
                        $prodPrice = number_format($prod_details->sale_price, 2, '.', '');
                    }
                } else {
                    $prodPrice = number_format($prod_details->prod_price, 2, '.', '');
                }

                $total_price = $prodPrice * $qty;
                $response = $this->cart_model->updateCartInfo($cardDetailId, $prodId, $qty);
                $data['status'] = 'Avaliable';
                $data['total_price'] = number_format($total_price, 2, '.', '');
                $subTotal = getCartSubTotalApp($cardId);
                
                $data['cart_sub_total'] = number_format($subTotal,2,'.','');
                $data['shipment_sub_total'] = number_format(getShipmentSubTotalApp($cardId), 2, '.', '');
                $data['discount_amount'] = number_format(getStoreDiscount($cardId), 2, '.', '');
                $total_shipment_amount = ($data['shipment_sub_total'] - $data['discount_amount']) + $data['cart_sub_total'];
                $data['total_shipment_amount'] =  number_format($total_shipment_amount, 2, '.', '');
                echo json_encode($data);
            }else{
                echo json_encode(array('status' => 'Error', 'msg' => 'Product is Not Avaliable'));
            }
            
        }
    }

    public function cartShippingAddress(){
        
        //$this->load->view("app/cart", $data, true);
        if (is_numeric($this->input->post('userId') ) )
        {
            $userId = $this->input->post('userId');
            $cartId = $this->input->post('cartId');
            $data['userid'] = $userId;

            $data['current'] = json_decode($this->input->post('shipping'));

            $data['address_list'] = $this->address_model->getuseraddress_2($userId,1);
            $data['default_address_list'] = $this->common_model->default_shipping($userId);
            $data['allstate'] = $this->db->query("SELECT stat_id, stat_name FROM tbl_states")->result_array();
            $data['allcountries'] = $this->db->query("SELECT iso, name FROM tbl_country")->result_array();
            $data['cart_product_details'] = getProductPricesApp($cartId);
            $data['shipment_sub_total'] = getShipmentSubTotalApp($cartId);
            $data['cart_sub_total'] = getCartSubTotalApp($cartId);
            $data['discount_amount'] = getStoreDiscount($cartId);
            $data['total_shipment_amount'] = ($data['shipment_sub_total'] - $data['discount_amount']) + $data['cart_sub_total'];
            $data['main_category_url'] = getFirstMainCategory()->cat_url;

            $res['html'] = $this->load->view("app/cart-shipping-address", $data, true);
            $res['isOk'] = 1;
            echo json_encode($res);
            
        } else {
            $res['html'] = '';
            $res['isOk'] = 0;
            echo json_encode($res);
        }
    }


    public function cartShippingAddressSave(){
        
        $data = array(
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_add_user_id' => $this->input->post('user_id'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_nick_name' => $this->input->post('nick_name'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_first_name' => $this->input->post('first_name'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_last_name' => $this->input->post('last_name'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_zip' => $this->input->post('zip_code'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_street' => $this->input->post('street'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_city' => $this->input->post('city'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_country' => $this->input->post('country'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_state' => ($this->input->post('country') == "US" ? $this->input->post('state') : ""),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_state_other' => ($this->input->post('country') != "US" ? $this->input->post('other_state') : ""),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_phone' => $this->input->post('phone'),
            );

        if ($this->input->post('save_address') != '') {
            $data_save['add_user_id'] = $this->input->post('user_id');
            $data_save['nick_name'] = $this->input->post('nick_name');
            $data_save['first_name'] = $this->input->post('first_name');
            $data_save['last_name'] = $this->input->post('last_name');
            $data_save['zip'] = $this->input->post('zip_code');
            $data_save['street'] = $this->input->post('street');
            $data_save['city'] = $this->input->post('city');
            $data_save['created_date'] = date('Y-m-d h:i:s');
            $data_save['country'] = $this->input->post('country');
            $data_save['state'] = ($this->input->post('country') == "US" ? $this->input->post('state') : "");
            $data_save['state_other'] = ($this->input->post('country') != "US" ? $this->input->post('other_state') : "");
            $data_save['phone'] = $this->input->post('phone');
            $data_save['default_billing_check'] = 'N';
            $data_save['default_shipping_check'] = 'N';
            $inserted_id = $this->common_model->commonSave('tbl_user_addreses', $data_save);
        }
        echo json_encode($data);
        
    }



    public function cartBillingAddress(){

        if (is_numeric($this->input->post('userId') ) )
        {
            $userId = $this->input->post('userId');
            $cartId = $this->input->post('cartId');
            $data['userid'] = $userId;

            //$data['current'] = json_decode($this->input->post('billing'));
            $billing_address = json_decode($this->input->post('billing'));

            $data['current'] = '';
            if($billing_address->use_default==1){
                $data['active'] = 1;
            }elseif($billing_address->same_as_shipping==1){
                $data['active'] = 2;
            }else{
                $data['active'] = 3;
                $data['current'] = json_decode($this->input->post('billing'));
            }

            /*echo "<pre>";
            print_r($_POST);
            print_r($data);
            exit;*/

            $data['address_list'] = $this->address_model->getuseraddress_2($userId,1);
            $data['default_address_list'] = $this->common_model->default_shipping($userId);
            $data['default_billing_address'] = $this->common_model->default_billing($userId);
            $data['allstate'] = $this->db->query("SELECT stat_id, stat_name FROM tbl_states")->result_array();
            $data['allcountries'] = $this->db->query("SELECT iso, name FROM tbl_country")->result_array();
            $data['cart_product_details'] = getProductPricesApp($cartId);
            $data['shipment_sub_total'] = getShipmentSubTotalApp($cartId);
            $data['cart_sub_total'] = getCartSubTotalApp($cartId);
            $data['discount_amount'] = getStoreDiscount($cartId);
            $data['total_shipment_amount'] = ($data['shipment_sub_total'] - $data['discount_amount']) + $data['cart_sub_total'];
            $data['main_category_url'] = getFirstMainCategory()->cat_url;

            $res['html'] = $this->load->view("app/cart-billing-payment", $data, true);
            $res['isOk'] = 1;
            echo json_encode($res);
            
        } else {
            $res['html'] = '';
            $res['isOk'] = 0;
            echo json_encode($res);
        }


    }

    public function cartBillingAddressSave(){

        $userId = $this->input->post('userId');
        if ($this->input->post('cc') == '1') {
            $address['default_shipping_address'] = $this->common_model->default_billing($userId);
            $data = array(
                "billing_add_user_id" => $address['default_shipping_address']->add_user_id,
                "billing_nick_name" => $address['default_shipping_address']->nick_name,
                "billing_first_name" => $address['default_shipping_address']->first_name,
                "billing_last_name" => $address['default_shipping_address']->last_name,
                "billing_zip" => $address['default_shipping_address']->zip,
                "billing_street" => $address['default_shipping_address']->street,
                "billing_city" => $address['default_shipping_address']->city,
                "billing_country" => $address['default_shipping_address']->country,
                "billing_state" => ($address['default_shipping_address']->country == "US" ? $address['default_shipping_address']->state : ""),
                "billing_state_other" => ($address['default_shipping_address']->country != "US" ? $address['default_shipping_address']->state_other : ""),
                "billing_phone" => $address['default_shipping_address']->phone,

                'same_as_shipping' => false,
                'use_default' => True
            );
            
            echo json_encode($data);

        } else if ($this->input->post('cc') == '2') {

            $address['default_shipping_address'] = json_decode($this->input->post('shipping_address'));            
            $data = array(
                "billing_add_user_id" => $address['default_shipping_address']->shipping_add_user_id,
                "billing_nick_name" => $address['default_shipping_address']->shipping_nick_name,
                "billing_first_name" => $address['default_shipping_address']->shipping_first_name,
                "billing_last_name" => $address['default_shipping_address']->shipping_last_name,
                "billing_zip" => $address['default_shipping_address']->shipping_zip,
                "billing_street" => $address['default_shipping_address']->shipping_street,
                "billing_city" => $address['default_shipping_address']->shipping_city,
                "billing_country" => $address['default_shipping_address']->shipping_country,
                "billing_state" => $address['default_shipping_address']->shipping_state,
                "billing_state_other" => $address['default_shipping_address']->shipping_state_other,
                "billing_phone" => $address['default_shipping_address']->shipping_phone,

                'same_as_shipping' => True,
                'use_default' => false,
            );

            echo json_encode($data);
        
        } else {
            $data = array(
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_add_user_id' => $this->input->post('user_id'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_nick_name' => $this->input->post('nick_name'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_first_name' => $this->input->post('first_name'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_last_name' => $this->input->post('last_name'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_zip' => $this->input->post('zip_code'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_street' => $this->input->post('street'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_city' => $this->input->post('city'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_country' => $this->input->post('country'),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_state' => ($this->input->post('country') == "US" ? $this->input->post('state') : ""),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_state_other' => ($this->input->post('country') != "US" ? $this->input->post('other_state') : ""),
                ($this->input->post('default_shipping') == "Y" ? "shipping" : "billing") . '_phone' => $this->input->post('phone'),
                'use_default' => false,
                'same_as_shipping' => false,
            );
            echo json_encode($data);
        }
    }


    public function cartConfirm(){

        $userId = $this->input->post('userId');
        $cartId = $this->input->post('cartId');
        $shipping = $this->input->post('shipping');
        $billing = $this->input->post('billing');

        if ($userId != '' && $cartId != '') {
            $data['userid'] = $userId;
           
            $data['cart_product_details'] = getProductPricesApp($cartId);
            foreach ($data['cart_product_details'] as $product) {
                $product_details = getproduct_details($product->cart_prod_id);
                if ($product_details == '') {
                    $this->cart_model->removeCartItem($cartId, $product->cart_prod_id);
                }
            }
            $data['shipment_sub_total'] = getShipmentSubTotalApp($cartId);
            $data['cart_sub_total'] = getCartSubTotalApp($cartId);
            $data['discount_amount'] = getStoreDiscount($cartId);
            $data['total_shipment_amount'] = ($data['shipment_sub_total'] - $data['discount_amount']) + $data['cart_sub_total'];
            $data['default_shipping_address'] = json_decode($shipping);
            $data['default_billing_address'] = json_decode($billing);
            $data['title'] = "Confirm Order";
            $data['paypalInfo'] = $this->payWithPaypal($cartId, $shipping);

            /*echo "<pre>";
            echo "cartId = ".$cartId;
            echo "shipping = ".$shipping;
            print_r($data);
            exit;*/
            $res['html'] = $this->load->view("app/cart-confirm", $data, true);
            $res['isOk'] = 1;
            echo json_encode($res);

        } else {
            $res['html'] = '';
            $res['isOk'] = 0;
            echo json_encode($res);
        }
    }


    public function payWithPaypal($cartId, $shipping) {

        /*echo "<pre>";
        print_r($shipping);*/
        
        $var = $this->paypal->GetToken();
        /*echo "<pre>";
        print_r($var);*/
        $var = json_decode($var);
        /*print_r($var);
        exit;*/
        
        //if($var->access_token != ''){
            $this->session->set_userdata('access_token',$var->access_token);
            $this->session->set_userdata('token_type',$var->token_type);
            
            $order_api = $this->paypal->CreateOrder($var->token_type, $var->access_token, $cartId,$shipping);
            
            /*echo "<pre>";
            print_r($order_api);
            exit;*/

            $order_api = json_decode($order_api);

            /*echo "<pre>";
            print_r($order_api);
            exit;*/
            
            $return['paypal_order_id'] = $order_api->id;

            if(!empty($order_api->links[1]->href)){
                $return['payPalURL'] = $order_api->links[1]->href;
            }else{
                $return['message'] = $order_api->message;
            }
            
            return $return;

        // }else{
        //     $this->session->set_flashdata('paypal_error', $var);
        //     redirect(base_url('cart-payment'));
        //     exit;
        // }

    }

    public function payForOrder()
    {
        $return['status'] = 201;
        $return['message'] = "There was a problem.";

        $order_id = $this->input->get('order_id');

        $data['order_id'] = $order_id;

        if($order_id!=null && $order_id!='undefined'){

            $var = $this->paypal->GetToken();

            $var = json_decode($var);

            $pay_api = $this->paypal->PayForOrder($var->token_type, $var->access_token, $order_id);

            $return['status'] = 200;
            $return['message'] = "Placed order successfully.";
        }

        echo json_encode($return);
    }

    public function paymentSave(){

        $return['status'] = 201;
        $return['message'] = "There was a problem.";
        
        sleep(3);

        $order_id = $this->input->get('order_id');

        $var = $this->paypal->GetToken();

        $var = json_decode($var);
        
        $orderDetail = $this->paypal->GetOrderDetail($order_id,$var->token_type, $var->access_token);
        
        $cartId = $this->input->get('cart_id');
        $user_id = $this->input->get('user_id');
        
        $data['orderDetail'] = $orderDetail;

        $data['paypal_order_id'] = $order_id;
        
        $data['cartid'] = $cartId;
        $data['userid'] = $user_id;
        $data['shipment_sub_total'] = getShipmentSubTotalApp($cartId);
        $data['cart_sub_total'] = getCartSubTotalApp($cartId);
        $data['discount_amount'] = getStoreDiscount($cartId);
        $data['total_shipment_amount'] = ($data['shipment_sub_total'] - $data['discount_amount']) + $data['cart_sub_total'];

        $data['cart_product_details'] = getProductPricesApp($cartId);
        $data['default_shipping_address'] = $this->input->get('shipping_address_info');
        $data['default_billing_address'] = $this->input->get('billing_address_info');

        $data['order_total_commission'] = getTotalCommision($cartId);

        $seller_total = getSellerTotal($cartId);

        $data['order_seller_info'] = json_encode($seller_total);

        $data['order_paypal_response'] = json_encode($orderDetail);

        /*echo "<pre>";
        print_r($data);
        exit;*/

        $new_order_id = $this->cart_model->confirm_order_app($data);
       
        if ($new_order_id > 0) {

            $this->common_model->commonDelete('tbl_cart_detail', 'cart_id', $cartId);
            $this->common_model->commonDelete('tbl_cart', 'cart_id', $cartId);
            $this->common_model->commonDelete('tbl_cart', 'cart_user_id', $user_id);

            $user_data = $this->common_model->commonselect('tbl_user', 'user_id', $user_id)->row();
            $email_data           = $this->common_model->commonselect('tbl_email_temp', 'e_email_slug', 'purchase-product')->row();
            $subject              = $email_data->e_email_subject;
            $message              = $email_data->e_email_text;
            $message              = str_replace("{{username}}", $user_data->user_fname, $message);
            $message              = str_replace("{{new_order_id}}", $new_order_id, $message);
            send_email($user_data->user_email, "noreply@educki.com", $subject, $message);

            $return['order_id'] = $new_order_id;
            $return['status'] = 200;
            $return['message'] = "Order placed successfully.";
        }else{            
            $return['status'] = 201;
            $return['message'] = "There was a problem.";
        }

        echo json_encode($return);
    }
   
}