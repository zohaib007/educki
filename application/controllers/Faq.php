<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Faq extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('front/faq/faq_model');
    }

    public function index()
    {
        $data                     = array();
        $page_id                  = 6;
        $data['pagedata']         = $this->common_model->commonselect('tbl_pages_sub', 'sub_id', $page_id)->row();
        $data['pageurl']          = $this->common_model->commonSelect('tbl_pages_sub', 'sub_id', 5)->row();
        $data['title']            = $data['pagedata']->sub_pg_title;
        $data['meta_title']       = $data['pagedata']->meta_title;
        $data['meta_description'] = $data['pagedata']->meta_description;
        $data['meta_keyword']     = $data['pagedata']->meta_keywords;
        $data['content_view']     = "about/faq";
        $data['list']             = $this->faq_model->view_list($page_id);
        $this->load->view("front/custom_template", $data);
    }

    public function faq_search()
    {
        $this->session->unset_userdata('msg');
        $data                     = array();
        $page_id                  = 6;
        $data['pagedata']         = $this->common_model->commonselect('tbl_pages_sub', 'sub_id', $page_id)->row();
        $data['pageurl']          = $this->common_model->commonSelect('tbl_pages_sub', 'sub_id', 10)->row();
        $data['title']            = "FAQs";
        $data['meta_title']       = "eDucki";
        $data['meta_description'] = "eDucki";
        $data['meta_keyword']     = "eDucki";
        $data['content_view']     = "about/faq";
        $data['list']             = $this->faq_model->view_search_list(4);
        if (count($data['list']) <= 0) {
            $this->session->set_flashdata('msg', "No Data Found...");
        }
        $this->load->view("front/custom_template", $data);
    }

}
