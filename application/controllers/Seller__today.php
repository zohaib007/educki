<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Seller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('front/seller/seller_model');
        authFront();
    }

    public function index() {
//        sellerFront();
        $data['tiers'] = $this->common_model->getCommon('tbl_tier_list', 'tier_id', 'ASC')->result();
        $data['title'] = "Become a Seller";
        $data['meta_title'] = "meta_title";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";
        $data['content_view'] = "seller/create_store";
        $data['userid'] = $this->session->userdata('userid');

        if($this->session->userdata('pricing_benefits_clicked')==1){
            $data['tier_id'] = $this->session->userdata('tier_id');
        }else{
            $data['tier_id'] = 3;
        }

        $data['user_store'] = $this->db->query("SELECT * FROM tbl_stores where user_id = ".$data['userid'])->result_array();

        if(count($data['user_store'])>0){
            redirect(base_url('seller-dashboard'));
        }

        $this->load->view("front/custom_template", $data);
    }

    public function account_details($store_id) {
        $data['tiers'] = $this->common_model->getCommon('tbl_tier_list', 'tier_id', 'ASC')->result();
        $data['store_id'] = $store_id;
        $data['userid'] = $this->session->userdata('userid');
        $data['page_data'] = $this->common_model->commonSelect('tbl_stores', 'store_id', $store_id)->row();
        $data['allstate'] = $this->db->query("SELECT stat_id, stat_name FROM tbl_states")->result_array();
        $data['allcountries'] = $this->db->query("SELECT iso, name FROM tbl_country")->result_array();
        $data['title'] = "Become a Seller";
        $data['content_view'] = "seller/my_account";
        $this->load->view("front/custom_template", $data);
    }

    public function check_availability() {

        $store_name = $this->input->post('store_name');
        if($store_name){
            $response = $this->seller_model->check_availability($store_name);
            if ($response == 0) {
                echo json_encode(array('status' => 'Available'));
                die;
            } else {
                $name = $this->suggest_name($store_name);
                echo json_encode(array('status' => 'error', 'new_name' => $name));
                die;
            }
        }
        $paypal_id = $this->input->post('paypal_id');
        if($paypal_id){
            $response = $this->seller_model->paypal_availability($paypal_id);
            if ($response == 0) {
                echo json_encode(array('status' => 'Available'));
                die;
            } else {
                echo json_encode(array('status' => 'error'));
                die;
            }
        }
    }

    public function suggest_name($data) {
        $new_data = $data . mt_rand(0, 10000);
        $response = $this->seller_model->check_availability($new_data);
        if ($response == 0) {
            return $new_data;
        }
    }

    public function paypal_id($paypal_id){
        $user_data =  $this->db->query("
                            SELECT * FROM tbl_stores store
                            INNER JOIN tbl_user user ON store.user_id = user.user_id
                            WHERE store.store_paypal_id = '".$paypal_id."' AND user.user_is_delete = 0")->row();
        if(count($user_data)>0)
        {
            $this->form_validation->set_message('paypal_id', 'Your Paypal ID should be unique.');
            return false;
        }else{
            return true;
        }
    }

    public function create_store() {
        
        $data['title'] = "Become a Seller";
        $data['tiers'] = $this->common_model->getCommon('tbl_tier_list', 'tier_id', 'ASC')->result();
        $this->form_validation->set_rules('store_name', 'Store Name', 'trim|required|callback_check_name_availability');
        $this->form_validation->set_rules('paypal_id', 'paypal Id', 'trim|required|callback_paypal_id');
        $this->form_validation->set_message('is_unique', 'Your Paypal ID should be unique.');
        $this->form_validation->set_message('required', 'This field is required.');
        $data['userid'] = $this->session->userdata('userid');
        
        if($_POST['tier']!=''){
            $data['tier_id'] = $_POST['tier'];
        }else{
            $data['tier_id'] = 3;
        }

        /*echo "<pre>";
        print_r($data);
        exit;*/
        if ($this->form_validation->run() == false) {
            $data['content_view'] = "seller/create_store";
            $this->load->view("front/custom_template", $data);
        } else {
            $store_name = $this->input->post('store_name');
            $tier_id = $this->input->post('tier');
            $response = $this->seller_model->check_availability($store_name);
            if ($response == 0) {
//                if ($tier_id == 1) {
                    $store_id = $this->seller_model->create_store();
//                } else {
//                    $test = $this->checkout();
//                    exit;
//            $this->seller_model->paypal_details();
                //
//                }
                if ($store_id != '') {
                    $array_items = array('tier_id' => '');
                    $this->session->unset_userdata($array_items);
                    redirect(base_url() . "seller/account_details/" . $store_id);
                    exit;
                }
            }
        }
    }

    public function check_name_availability($store_name) {
        $where = "store_name = '" . $store_name . "'";
        $results = $this->common_model->commonselect_array('tbl_stores', $where, 'store_id')->row();
        if (count($results)>0) {
            $this->form_validation->set_message('check_name_availability', 'Store Name Not Avaliable try this '.$this->suggest_name($store_name));
            return false;
        } else {
            return true;
        }
    }

    public function myaccount() {
        $store_id = $this->seller_model->myaccount();
        $sessiondata = array(
            'storeid' => $store_id,
        );
        $this->session->set_userdata($sessiondata);
        if ($store_id != '') {
            redirect(base_url() . "seller-dashboard");
            exit;
        } else {
            
        }
    }

    public function tier_info() {
        $tier_id = $this->input->post('tier_value');
        $response = $this->seller_model->tier_info($tier_id);
        echo json_encode(array('status' => $response));
    }

    public function checkout() {

        $products = array(
            array('AMT' => $_POST["itemprice"],
                'ITEMAMT' => $_POST["itemprice"],
                'TAXAMT' => 0,
                'PAYMENTACTION' => 'Order',
                'DESC' => $_POST["itemdesc"],
                "SELLERPAYPALACCOUNTID" => "mikesmith1166@gmail.com",
                "PAYMENTREQUESTID" => 'CART286-PAYMENT1',
                "NAME0" => $_POST["itemname"],
                "NUMBER0" => $_POST["itemnumber"],
                "QTY0" => $_POST["itemQty"]),
        );
        //-------------------- prepare charges -------------------------

        $charges = [];

        //Other important variables like tax, shipping cost
        $charges['TotalTaxAmount'] = 0;  //Sum of tax for all items in this order. 
        $charges['HandalingCost'] = 0;  //Handling cost for this order.
        $charges['InsuranceCost'] = 0;  //shipping insurance cost for this order.
        $charges['ShippinDiscount'] = 0; //Shipping discount for this order. Specify this as negative number.
        $charges['ShippinCost'] = 0; //Although you may change the value later, try to pass in a shipping amount that is reasonably accurate.
        //------------------SetExpressCheckOut-------------------
        $this->session->set_userdata('purchase', $products);
        $this->session->set_userdata('charges', $charges);
        $this->load->library('mypaypal');
        $this->mypaypal->setReturnUrl(base_url() . "seller/ipn");
        $this->mypaypal->setCancelUrl(base_url());
//        var_dump($products);die;
        $this->mypaypal->SetExpressCheckout($products, $charges);
        exit;
    }

    public function ipn() {
        $this->load->library('mypaypal');
//        $this->mypaypal->GetTransactionDetails($_GET['token']);
        $products = $this->session->userdata('purchase');
        $charges = $this->session->userdata('charges');
        $this->mypaypal->DoExpressCheckoutPayment($_GET['token'], $_GET['PayerID'], $products ,$charges);
        
        echo '<pre>';
        echo "i am here return url page";
        print_r($products);
        exit();
        //        exit;
    }

    public function cancel() {
        
    }

    public function edit_account_details() {

        $data['userid'] = $this->session->userdata('userid');
        $data['page_data'] = $this->common_model->commonSelect('tbl_stores', 'user_id', $data['userid'])->row();
        if($data['page_data']->store_name!=$this->input->post('store_name')){
            $this->form_validation->set_rules('store_name', 'Store Name', 'trim|required|is_unique[tbl_stores.store_name]');
        }else{
            $this->form_validation->set_rules('store_name', 'Store Name', 'trim|required');
        }
        if($data['page_data']->store_paypal_id!=$this->input->post('paypal_id')){
            $this->form_validation->set_rules('paypal_id', 'paypal Id', 'trim|required|callback_paypal_id');
        }        
        $this->form_validation->set_rules('store_city', 'Store City', 'trim|required');
        $this->form_validation->set_rules('store_zip', 'Store zip', 'required');
        $this->form_validation->set_rules('store_country', 'Store Country', 'required');
        $this->form_validation->set_message('required', 'This field is required.');
        
        $data['allstate'] = $this->db->query("SELECT stat_id, stat_name FROM tbl_states")->result_array();
        $data['allcountries'] = $this->db->query("SELECT iso, name FROM tbl_country")->result_array();
        $data['tiers'] = $this->common_model->getCommon('tbl_tier_list', 'tier_id', 'ASC')->result();

        $data['title'] = "My Store";
        if ($this->form_validation->run() == false) {
            $data['content_view'] = "seller/edit_my_store";
            $this->load->view("front/custom_template", $data);
        } else {
//                $this->seller_model->paypal_details();
            $result = $this->seller_model->edit_store();
            $store_id = $this->seller_model->myaccount();
            if ($store_id != '') {
                redirect(base_url() . "seller-dashboard");
                exit;
            }
        }
    }

    public function edit_tiers_paypal(){
        require_once(APPPATH.'/third_party/adaptivepayments-sdk-php-master/samples/PPBootStrap.php');
        require_once(APPPATH.'/third_party/adaptivepayments-sdk-php-master/samples/Common/Constants.php');
        define("DEFAULT_SELECT", "- Select -");
        $returnUrl =  base_url('edit-store');
        $cancelUrl =  base_url('edit-store');
        $actionType = "PAY";
        #$currencyCode = "EUR"; //USD EUR GBP
        $reciver_email = "dotlogics5@gmail.com";
        $id = "PAYMENTINFO_00_TRANSACTIONID";
        $total_amount = 5.25;
        
        $receiver = array();
        /*
        * A receiver's email address 
        */
        $receiver = new Receiver();
        $receiver->email = $reciver_email;
        /** Amount to be credited to the receiver's account  **/
        //$receiver->amount = $_POST['receiverAmount'];
        $receiver->amount = $total_amount;
        $receiverList = new ReceiverList($receiver);
        /**  The action for this request. Possible values are:
        
        PAY � Use this option if you are not using the Pay request in combination with ExecutePayment.
        CREATE � Use this option to set up the payment instructions with SetPaymentOptions and then execute the payment at a later time with the ExecutePayment.
        PAY_PRIMARY � For chained payments only, specify this value to delay payments to the secondary receivers; only the payment to the primary receiver is processed.
        
        */
        /*
        * The code for the currency in which the payment is made; you can specify only one currency, regardless of the number of receivers 
        */
        /*
        * URL to redirect the sender's browser to after canceling the approval for a payment; it is always required but only used for payments that require approval (explicit payments) 
        */
        /*
        * URL to redirect the sender's browser to after the sender has logged into PayPal and approved a payment; it is always required but only used if a payment requires explicit approval 
        */
        $payRequest = new PayRequest(new RequestEnvelope("en_US"), $actionType, $cancelUrl, 'USD', $receiverList, $returnUrl, $id);
        
        /*
        *    ## Creating service wrapper object
        Creating service wrapper object to make API call and loading
        Configuration::getAcctAndConfig() returns array that contains credential and config parameters
        */
        
        $service = new AdaptivePaymentsService(Configuration::getAcctAndConfig());
        try {
            /* wrap API method calls on the service object with a try catch */
            $response = $service->Pay($payRequest);
            $payKey = $response->payKey;
            $this->session->set_userdata('pay_key',$payKey);
        } catch(Exception $ex) {
            require_once(APPPATH.'/third_party/adaptivepayments-sdk-php-master/samples/Common/Error.php');
            exit;
        }
        
        $ack = strtoupper($response->responseEnvelope->ack);
        $payKey = $response->payKey;
        
        $this->session->set_userdata('pay_key',$payKey);
        if($ack != "SUCCESS") 
        {
            $this->session->set_flashdata('paypal_error',$response->error[0]->message);
            //$this->session->set_flashdata('paypal_error',"Currently the system does not accept the YUWAN currency(CNY).");
            
            redirect(base_url('edit-store'));
            exit;
            /*
            echo "<b>Error</b>";
            echo "<pre>";
            echo "</pre>";
            */
        } 
        else 
        {
            $payKey = $response->payKey;
            $payPalURL = PAYPAL_REDIRECT_URL . '_ap-payment&paykey=' . $payKey;
            
            $payKey = $response->payKey;
            $this->session->set_userdata('pay_key',$payKey);
            
            /*redirect($payPalURL);*/
            
            echo "<table>";
            echo "<tr><td>Ack :</td><td><div id='Ack'>$ack</div> </td></tr>";
            echo "<tr><td>PayKey :</td><td><div id='PayKey'>$payKey</div> </td></tr>";
            echo "<tr><td><a href=$payPalURL><b>Redirect URL to Complete Payment </b></a></td></tr>";
            echo "</table>";
            echo "<pre>";
            print_r($response);
            echo "</pre>";  
            exit;
            
        }
        
        //require_once(APPPATH.'/third_party/adaptivepayments-sdk-php-master/samples/Common/Response.php');
        /* Make the call to PayPal to get the Pay token
        If the API call succeded, then redirect the buyer to PayPal
        to begin to authorize payment.  If an error occured, show the
        resulting errors */
        //$this->load->view('SimplePay');
    }

    public function delete_page_img() {
        if (!$this->input->is_ajax_request()) {
            
        } else {
            $id = $this->input->post('nId');
            $data[$this->input->post('fieldname')] = "";
            $this->db->where('store_id', $id);
            $this->db->update('tbl_stores', $data);
            $this->common_model->commonUpdate('tbl_stores',$data, 'store_id', $id );
            echo 1;
        }
    }

    public function ask_a_question() {
        $question = $this->input->post('question');
        $sender_id = $this->input->post('sender');
        $email_to = $this->input->post('receiver');
        $prod = $this->input->post('prod_id');
        $sender_info = $this->common_model->commonSelect('tbl_user', 'user_id', $sender_id)->row();
        $seller_info = $this->common_model->commonSelect('tbl_user', 'user_email', $email_to)->row();
        $prod_info = $this->common_model->commonSelect('tbl_products', 'prod_id', $prod)->row();
        $email_from = $sender_info->user_email;
        $email_data['subject'] = 'Ask a Question';
//        $email_msg = 'Dear ' . $name . ',<br>';
        $email_msg = 'Dear '.$seller_info->user_fname." ".$seller_info->user_lname.',<br><br>';
        $email_msg .= 'The user <b>'.$sender_info->user_fname." ".$sender_info->user_lname.'</b> has asked a question about the product <b>'.$prod_info->prod_title.'</b><br><br>';
//        $email_msg .= 'The user ' . $name . ' has asked a question about the product' . $career_name . '. The details are mention below <br>';
        $email_msg .= 'Question: ' . $question . '<br><br>';
        $email_msg .= 'Thank you';
        /*echo "<pre>";
        print_r($email_msg);
        exit;*/
        //$this->common_model->send_email($email_data);
        send_email_2($email_to,$email_from,'Question About Product on Educki',$email_msg);
    }

    public function favourite_shops($store_url = '', $page = 0) {
        if ($store_url == '') {
            redirect(base_url());
        }

        $data['title'] = "Favorite Shops";
        $data['meta_title'] = "meta_title";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";

        $store_user_id = get_store_user_id($this->uri->segment(2))->user_id;

        $data['logged_user'] = $store_user_id;
        //$user_id = $this->session->userdata('userid');
        if ($page == 0) {
            $page = 0;
        }
        $data['active'] = "wish_list";
        $sort = 'DESC';
        $data['per_page'] = 10;

        if (@$this->input->get('per_page') != '') {
            $data['per_page'] = @$this->input->get('per_page');
        }

        $data['user_data'] = $this->common_model->commonSelect('tbl_stores', 'user_id', $store_user_id)->row();

        $data['total_rows'] = count($this->seller_model->get_favourite_shops($store_user_id)->result_array());

        $mypaing['per_page'] = $data['per_page'];
        $mypaing['total_rows'] = $data['total_rows'];
        $mypaing['base_url'] = base_url() . "favorite-shops/".$store_url;
        $mypaing['uri_segment'] = 3;
        $mypaing['next_link'] = '&nbsp;';
        $mypaing['prev_link'] = '&nbsp;';
        $mypaing['prev_tag_open'] = '<li class="page-left" >';
        $mypaing['prev_tag_close'] = '</li>';
        $mypaing['next_tag_open'] = '<li class="page-right">';
        $mypaing['next_tag_close'] = '</li>';
        $mypaing['num_tag_open'] = '<li>';
        $mypaing['num_tag_close'] = '</li>';
        $mypaing['cur_tag_open'] = '<li><a class = "active">';
        $mypaing['cur_tag_close'] = '</a></li>';
        $mypaing['num_links'] = 5;
        $mypaing['first_tag_open'] = '<div style="display:none;">';
        $mypaing['first_tag_close'] = '</div>';
        $mypaing['last_tag_open'] = '<div style="display:none;">';
        $mypaing['last_tag_close'] = '</div>';

        $data['stores'] = $this->seller_model->get_favourite_shops($store_user_id, $sort, $page, $mypaing['per_page'], 1)->result_array();

        $data['to'] = $page;
        $data['from'] = $page + $mypaing['per_page'];

        $this->pagination->initialize($mypaing);
        $data['paginglink'] = $this->pagination->create_links();

        $data['content_view'] = "seller/favourite-stores";
        $this->load->view("front/custom_template", $data);
    }

    public function delete_favourite_shops() {
        $user_id = $this->session->userdata('userid');
        $data['stores_id'] = $this->input->post('val');

        //print_r($data['stores_id']);

        if (count($data['stores_id']) > 0) {

            foreach ($data['stores_id'] as $store) {
                $this->common_model->commonDelete2('tbl_fans', 'fan_id', $user_id, 'store_id', $store);
            }

            echo 1;
        }
    }

    public function edit_tier() {
        $data['userid'] = $this->session->userdata('userid');
        $data['page_data'] = $this->common_model->commonSelect('tbl_stores', 'user_id', $data['userid'])->row();

        $this->form_validation->set_rules('tiersId', 'Edit tier', 'trim|required');

        $data['tiers'] = $this->common_model->getCommon('tbl_tier_list', 'tier_id', 'ASC')->result();
        if($this->session->userdata('pricing_benefits_clicked')==1){
            $data['tier_id'] = $this->session->userdata('tier_id');
        }else{
            $data['tier_id'] = $data['page_data']->store_tier_id;
        }
        $data['tiers'] = $this->common_model->getCommon('tbl_tier_list', 'tier_id', 'ASC')->result();
        $data['title'] = "My Tier";
        if ($this->form_validation->run() == false) {
            $data['content_view'] = "seller/change_tier";
            $this->load->view("front/custom_template", $data);
        } else {
            $update_data['sub_ends'] = date('Y-m-d h:i:s');
            $this->common_model->commonUpdate('tbl_user_store_subscription', $update_data,'sub_tier_id',$data['page_data']->store_tier_id);
            $tier_id = $this->input->post('tiersId');
            $store_id = $this->input->post('store_id');
            $tier['tier_data'] = $this->common_model->commonSelect('tbl_tier_list', 'tier_id', $tier_id)->row();
            $end_date = '';
            $time = strtotime(date("Y-m-d h:i:s"));

            if ($tier_id == 1) {
                $end_date = date('Y-m-d h:i:s');
                $sub_data['sub_free_tier'] = 'Y';
            } elseif ($tier_id == 2) {
                $end_date = date('Y-m-d h:i:s', strtotime('+'.$tier['tier_data']->tier_months.' months', $time));
                $sub_data['sub_free_tier'] = 'N';
            } elseif ($tier_id == 3) {
                $end_date = date('Y-m-d h:i:s', strtotime('+'.$tier['tier_data']->tier_months.' months', $time));
                $sub_data['sub_free_tier'] = 'N';
            } else {
                $end_date = date('Y-m-d h:i:s', strtotime('+'.$tier['tier_data']->tier_months.' months', $time));
                $sub_data['sub_free_tier'] = 'N';
            }

            $dbdata['store_tier_id'] = $tier_id;
            $dbdata['store_end_date'] = $end_date;
            $this->common_model->commonUpdate('tbl_stores', $dbdata, 'store_id', $store_id);

            $sub_data['sub_tier_id'] = $tier_id;
            $sub_data['sub_start'] = date('Y-m-d h:i:s');
            $sub_data['next_payment'] = date('d', strtotime('+1 months'));
            if(date('d')!=$sub_data['next_payment']){
                $sub_data['next_payment'] = date('Y-m-d h:i:s', strtotime('last day of next month'));
            }else{
                $sub_data['next_payment'] = date('Y-m-d h:i:s', strtotime('+1 months'));
            }
            $sub_data['sub_ends'] = $end_date;
            $sub_data['sub_store_id'] = $store_id;
            $sub_data['tier_price'] = $tier['tier_data']->tier_amount;
            if($data['page_data']->store_tier_id==$tier_id){                
                $sub_data['sub_is_renewal'] = 1;
                $churn_data['sub_is_churn'] = 0;
            }else{
                $sub_data['sub_is_renewal'] = 0;
                $churn_data['sub_is_churn'] = 1;
            }
            $sub_id = $this->db->query("
                                    SELECT *
                                    FROM tbl_user_store_subscription                                    
                                    WHERE sub_store_id = ".$data['page_data']->store_id."
                                    ORDER BY sub_id DESC
                                    LIMIT 1
                                ")->row()->sub_id;
            $this->common_model->commonUpdate('tbl_user_store_subscription', $churn_data, 'sub_id', $sub_id);
            /*echo "<pre>";
            print_r($data);
            print_r($tier);
            print_r($sub_data);
            exit;*/
            $this->common_model->commonSave('tbl_user_store_subscription', $sub_data);
            $this->session->set_flashdata('msg','Tier Changed Successfully.');
            redirect(base_url().'edit-tier');
        }
    }    
}
