<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('front/shop/shop_model');
        date_default_timezone_set('America/New_York');
    }

    public function index() {
        $data['title'] = "eDucki";
        $data['meta_title'] = "eDucki";
        $data['meta_description'] = "eDucki";
        $data['meta_keyword'] = "eDucki";
        $data['content_view'] = "index";
        $this->load->view("front/custom_template", $data);
    }

    public function smart_search() {
        $this->load->model('search_model');
        $text = trim($this->db->escape_str($this->input->post('txt')));
        $result = $this->search_model->smartSearch($text);
        $return['status'] = 201;
        $return['rows'] = 0;
        $return['html'] = '';
        if ($result->num_rows() > 0) {
            $return['rows'] = $result->num_rows();
            foreach ($result->result() as $row) {
                $pos = stripos($row->title, $text);
                $len = strlen($text);
                $keyw = '<strong>' . substr($row->title, $pos, $len) . '</strong>';
                $url = '';
                if ($row->tbl == 'Products') {
                    $url = productDetailUrl($row->id)."/".$row->url;
                    $prod_id = $row->id;
                    $complete_url = base_url("save-search?term=$row->title&cat=$row->tbl&url=$url&prod_id=$prod_id");
                    $return['html'] .= "<li><a href=\"$complete_url\">".str_replace($text, $keyw, $row->title) . " in <span>" . $row->tbl . "</span></a></li>";
                } elseif ($row->tbl == 'Stores') {
                    $url = base_url('store/' . $row->url);
                    $complete_url = base_url("save-search?term=$row->title&cat=$row->tbl&url=$url");
                    $return['html'] .= "<li><a href=\"$complete_url\">" . str_replace($text, $keyw, $row->title) . " in <span>" . $row->tbl . "</span></a></li>";
                } elseif ($row->tbl == 'Blogs') {
                    $url = base_url('blog-details/' . $row->url);
                    $complete_url = base_url("save-search?term=$row->title&cat=$row->tbl&url=$url");
                    $return['html'] .= "<li><a href=\"$complete_url\">" . str_replace($text, $keyw, $row->title) . " in <span>" . $row->tbl . "</span></a></li>";
                }
            }
        } else {
            $return['status'] = 200;
        }
        echo json_encode($return);
    }

    public function save_search() {        
        $cat_id = '';
        $text = trim($this->db->escape_str($this->input->get('term')));
        $cat = trim($this->db->escape_str($this->input->get('cat')));
        $link = trim($this->db->escape_str($this->input->get('url')));
        if($cat=='Products'){
            $prod_id = trim($this->db->escape_str($this->input->get('prod_id')));
            $cat_id = $this->db->query("SELECT category_id
                                        FROM tbl_product_category WHERE prod_id = ".$prod_id."
                                        ")->row()->category_id;
            $data['cat_id'] = $cat_id;
        }        

        $data['search_term'] = $text;
        $data['search_category'] = $cat;        
        $data['search_datetime'] = date('Y-m-d H:i:s');
        $data['user_ip'] = getHostByName(php_uname('n'));        
        /*echo "<pre>";
        print_r($data);
        exit;*/
        $this->db->insert('tbl_search_terms', $data);
        redirect($link);
    }

    public function compare_prods() {
        $return['status'] = 201;
        $return['msg'] = '';
        $return['html'] = '';
        $prod_id = $this->input->post('prodId');
        //$prod_details = getproduct_details(153);
        //echo '<pre>';
        //print_r($prod_details);
        //exit;
        //$prod_id = 10;
        //$this->session->unset_userdata('compare_prods');
        $prod_details = getproduct_details($prod_id);
        if (empty($_SESSION['compare_prods'])) {
            $sessiondata = array(
                'compare_prods' => "'" . $prod_id . "'",
            );

            $this->session->set_userdata($sessiondata);
            $return['status'] = 200;
            $return['msg'] = 'Prod ID : ' . $prod_id . ' Added to Compare List';
        } else {
            $prods = explode(',', $_SESSION['compare_prods']);
            //$prods = $_SESSION['compare_prods'];
            if (count($prods) < 3) {
                $prod_list = $_SESSION['compare_prods'];
                $prodId = "'" . $prod_id . "'";
                if (array_search($prodId, $prods) !== FALSE) {
                    $return['status'] = 900;
                    $return['msg'] = 'Prod ID : ' . $prod_id . ' Already in Compare List';
                } else {
                    $_SESSION['compare_prods'] .= ',' . "'" . $prod_id . "'";
                    $return['status'] = 200;
                    $return['msg'] = 'Prod ID : ' . $prod_id . ' Added to Compare List';
                }
            } else {
                $return['status'] = 201;
                $return['msg'] = 'Compare List is Full';
            }
        }
        $return['html'] = getCompareList();
        $return['compare_list'] = $_SESSION['compare_prods'];
        $prods = explode(',', $_SESSION['compare_prods']);
        //$prods = $_SESSION['compare_prods'];
        $prods = str_replace("'", '', $prods);
        $return['total_prods'] = count($prods);
        echo json_encode($return);
//        echo '<pre>';
//        print_r('prods : '.$_SESSION['compare_prods']);
//        print_r('prods : '.count($_SESSION['compare_prods']));
//        echo '</pre>';
//        exit;
    }

    public function clear_compare_list() {
        $return['status'] = 201;
        $this->session->unset_userdata('compare_prods');
        $return['status'] = 200;
        echo json_encode($return);
    }

    public function testmodule() {
        $_SESSION['compare_prods'];
        if (!empty($_SESSION['compare_prods'])) {
            $prods = explode(',', $_SESSION['compare_prods']);
            //$prods = $_SESSION['compare_prods'];
            $prods = str_replace("'", '', $prods);
            echo '<pre>';
            print_r($prods);
            //exit;
            foreach ($prods as $key => $prod_id) {
                $prod_details = getproduct_details($prod_id);
                $return['html'] .= '<div class="compre-pop-bx ' . ($key == 2 ? 'mrg-rgt-0' : '') . ' prod-td-' . trim($prod_id) . '"><div class="compre-pop-bx-img"><img src="' . base_url() . 'resources/prod_images/' . $prod_details->img_name . '" alt="" title="" class="img-responsive"/></div><div class="compre-pop-bx-right"><a href="' . productDetailUrl($prod_id) . '/' . $prod_details->prod_url . '">' . $prod_details->prod_title . '</a></div><button class="removeslctitem" data-id="' . $prod_id . '"></button></div>';
                echo '<pre>';
                print_r($prod_details);
                echo '</pre>';
            }
            //$prod_details = getproduct_details($prod_id);
        }
        echo '<pre>';
        print_r('prods : ' . $_SESSION['compare_prods']);
        echo '</pre>';
        echo '<pre>';
        print_r('total prods : ' . count($prods));
        echo '</pre>';
        echo '<pre>';
        print_r('HTML ::: ' . $return['html']);
        echo '</pre>';
        exit;
    }

    public function compare_details() {
        if (!empty($_SESSION['compare_prods'])) {
            $data['compare_prods'] = $_SESSION['compare_prods'];
            $prods = explode(',', $_SESSION['compare_prods']);
            $prods = str_replace("'", '', $prods);
            $data['prods'] = $prods;
            $data['title'] = "Compare Products";
            $data['total_prods'] = count($prods);
            $data['title'] = 'Compare';
            //echo '<pre>';
            //print_r($data);
            //echo '</pre>';
            $data['content_view'] = "shop/compare-details";
            $this->load->view("front/custom_template", $data);
        } else {
            redirect(base_url());
        }
    }

    public function remove_compare_prod() {
        $return['status'] = 201;
        $return['msg'] = '';

        $prod_id = $this->input->post('prod_id');

        $return['compare_prods'] = $_SESSION['compare_prods'];
        $prods = explode(',', $_SESSION['compare_prods']);
        $p_id = "'" . $prod_id . "'";
        //$prods = str_replace("'",'',$prods);
        $items = my_remove_array_item($prods, $p_id);
        $_SESSION['compare_prods'] = implode(',', $items);
        $return['prods'] = $prods;
        $return['new_prods'] = $items;
        $return['lastest_compare_list'] = $_SESSION['compare_prods'];
        $return['total_prods'] = count($items);
        $return['status'] = 200;
        $return['msg'] = 'Prod ID : ' . $prod_id . ' Removed from Compare List';
        echo json_encode($return);
    }

    public function compare_prods_2() {
        $return['status'] = 201;
        $return['msg'] = '';
        $return['html'] = '';
        $prod_id = $this->input->post('prod_id');
        //$prod_details = getproduct_details(153);
        //echo '<pre>';
        //print_r($prod_details);
        //exit;
        //$prod_id = 10;
        //$this->session->unset_userdata('compare_prods');
        $prod_details = getproduct_details($prod_id);
        if (empty($_SESSION['compare_prods'])) {
            $sessiondata = array(
                'compare_prods' => "'" . $prod_id . "'",
            );

            $this->session->set_userdata($sessiondata);
            $return['status'] = 200;
            $return['msg'] = 'Prod ID : ' . $prod_id . ' Added to Compare List';
        } else {
            $prods = explode(',', $_SESSION['compare_prods']);
            //$prods = $_SESSION['compare_prods'];
            if (count($prods) < 3) {
                $prod_list = $_SESSION['compare_prods'];
                $prodId = "'" . $prod_id . "'";
                if (array_search($prodId, $prods) !== FALSE) {
                    $return['status'] = 900;
                    $return['msg'] = 'Prod ID : ' . $prod_id . ' Already in Compare List';
                    $this->remove_compare_prod();
                } else {
                    $_SESSION['compare_prods'] .= ',' . "'" . $prod_id . "'";
                    $return['status'] = 200;
                    $return['msg'] = 'Prod ID : ' . $prod_id . ' Added to Compare List';
                }
            } else {
                $return['status'] = 201;
                $return['msg'] = 'Compare List is Full';
            }
        }
        $return['html'] = getCompareList2();
        $return['compare_list'] = $_SESSION['compare_prods'];
        $prods = explode(',', $_SESSION['compare_prods']);
        //$prods = $_SESSION['compare_prods'];
        $prods = str_replace("'", '', $prods);
        $return['total_prods'] = count($prods);
        echo json_encode($return);
        /* echo '<pre>';
          print_r('prods : '.$_SESSION['compare_prods']);
          echo '</pre>';
          exit; */
    }

    public function compare_product_filters() {

        $product_id = $this->input->post('prod_id'); // 105;
        $filters = $this->shop_model->getProduct1lvlRequiredFilter($product_id);
        $return['html'] = '';

        if ($filters != FALSE && $filters[0]['uniqueFilter'] == 0) {
            $product['filters_array'] = $filters;
            $product['required_filters'] = $this->common_model->unique_multidimentional_array($filters, 'filter_id');
            $return['html'] .= "<div class='shop-det-rgt-upr'>";
            $return['html'] .= "<h5>please specify the product's options</h5>";
//            $return['html'] .= "<div class='container' id='add_to_cart_msg' style='display: none;'>
//    <div class='row'>
//        <div id='alert_msg' class='alert fade in alert-dismissable' style='clear: both; text-align: center; font-size: 15px;'>
//            Product added to cart.
//            <a href='#' class='close' data-dismiss='alert' aria-label='close' title='close'>&times;</a>
//        </div>
//    </div>
//</div>";

            
            foreach ($product['required_filters'] as $filters) {
                $return['html'] .= "<div class='row pf-row'>";
                $return['html'] .= "<div class='col-md-4 col-sm-12 col-xs-12'>";
                $return['html'] .= "<label>" . $filters['filter_title'] . ":</label>";
                $return['html'] .= "</div>";
                $return['html'] .= "<div class='col-md-8 col-sm-12 col-xs-12'>";
                $return['html'] .= "<select class='required-filters' data-name='".$filters['filter_title']."' name='". $filters['filter_slug'] ."' id='".$filters['filter_slug']."' onchange=filtervalue(".$filters['prod_id'].",'".$filters['filter_slug']."') >";
                // $return['html'] .= "<option value='' > Select " . $filters['filter_title'] . "</option>";
                foreach ($product['filters_array'] as $op) {
                    if ($op['filter_id'] == $filters['filter_id']) {
                        $return['html'] .= "<option value=" . $op['filter_value'] . ">" . $op['filter_value'] . "</option>";
                    }
                }
                $return['html'] .= "</select>";
                $return['html'] .= "<input type='hidden' name='".$filters['prod_id']."_".$filters['filter_slug']."' data-name='".$filters['filter_title']."'  data-slug='".$filters['filter_slug']."' class='required_filters' id='".$filters['prod_id']."_".$filters['filter_slug']."'>";
                $return['html'] .= "</div>";
                $return['html'] .= "</div>";
            }
            $return['html'] .= "</div>";
            $return['html'] .= "<div class='pf-wrap'></div>";
            $return['html'] .= "<div class='shop-det-rgt-lowr-1'>";
            $return['html'] .= "<a href='javascript:;' onclick=cart(".$product_id.") class='ad-crt-btn'>Add to cart</a>";
            $return['html'] .= "</div>";
//                
        }
        print_r(json_encode($return));
    }

//	public function index(){
//		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
//		$this->form_validation->set_message('required', 'This field is required.');
//		$this->form_validation->set_message('valid_email', 'Please enter a valid email address.');
//                $data['title']= "eDucki";
//                $data['meta_description']= "eDucki";
//                $data['meta_keyword']= "index";
//                $data['content_view']= "index";
//		if ($this->form_validation->run() == FALSE)
//		{
//			$this->load->view("front/custom_template",$data);
//		}else{
//			$subEmail = $this->input->post('sub_email');
//			$config = array(
//						'protocol' => 'smtp',
//						//'smtp_host'=>'smtp.mailgun.org',
//						'smtp_host' => 'smtp.gmail.com',
//						'smtp_port'=>465,
//						'_smtp_auth'=>true, 
//						'smtp_user'=>'mikesmith1166@gmail.com',
//						'smtp_pass'=>'S1234mike',  
//						'smtp_crypto'=>'ssl',
//						'mailtype'=>'html', 
//						'charset'=>'utf-8',
//						'validate'=>true
//					);
//			$this->email->initialize($config);
//			$this->email->set_newline("\r\n");
//			$CI->email->from('mikesmith1166@gmail.com');
//			$this->email->to($subEmail);
//			$this->email->set_mailtype("html");
//			$this->email->subject($subject);
//			$this->email->message($message);
//			if($this->email->send()){
//				echo 1;
//			}else{
//				echo "fail<br>";
//				echo "m=".mail("sales@broadwaylumber.com", $subject, $msg)."<br>";
//				echo $this->email->print_debugger(); exit;
//			}
//		}
//	}
}

?>