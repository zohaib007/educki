<?php
/**
 * - Code by Haseeb
 * - Code for EDucki
 * - Code for add product and filters in front-end.
 */

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Products extends CI_Controller
{
    public $storeInfo = '';

    public function __construct()
    {

        parent::__construct();
        $this->load->model('front/product/product_model');
        $this->load->library('pagination'); // load Pagination library
        $this->load->helper('url');  // load URL helper
        $this->load->library('upload');
        $this->storeInfo = $this->common_model->commonselect('tbl_stores', 'user_id', $this->session->userdata('userid'))->row();
        if (count($this->storeInfo) <= 0) {
            redirect(base_url('my-profile'));
            exit;
        }

    }

    public function index()
    {
        $data = array();
        $user_id = $this->session->userdata('userid');
        $data['user_id'] = $user_id;
        $data['store_info'] = $this->common_model->commonselect('tbl_stores', 'user_id', $data['user_id'])->row();
        $data['store_id'] = $data['store_info']->store_id;
        $data['title'] = "Manage Products";
        $data['content_view'] = "products/manage-product";
        $limit_per_page = 10;
        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) : 0;
        $data['total_records'] = count($this->product_model->view_product_detail($user_id));
        $config['base_url'] = base_url() . 'manage-products/index';
        $config['total_rows'] = $data['total_records'];
        $config['per_page'] = $limit_per_page;
        $config["uri_segment"] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['cur_tag_open'] = '&nbsp;<li><a class="current">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_link'] = '';
        $config['prev_link'] = '';
        $this->pagination->initialize($config);
        $data['to'] =  $page;
        $data['from'] = $page + $config['per_page'];
        $start = (int) $this->uri->segment(3) * $config['per_page'] + 1;
        $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int) $this->uri->segment(3) * $config['per_page'] + $config['per_page'];
        $data['result_count'] = "" . $start . " - " . $end . " of " . $config['total_rows'] . " ";
        $data["links"] = $this->pagination->create_links();
        $data['results'] = $this->product_model->get_product_detail($user_id, $limit_per_page, $page * $limit_per_page);
       
        $this->load->view("front/custom_template", $data);
    }

    public function get_sub_category()
    {
        $main_cat_id = $this->input->post('cat_id');
        /*$sub_cat = $this->common_model->commonselect2('tbl_categories', 'cat_parent_id', $main_cat_id, 'cat_status', 1)->result();*/
        $sub_cat = $this->db->query("
                            SELECT *
                            FROM tbl_categories
                            WHERE cat_parent_id = '".$main_cat_id."' AND cat_status = 1 AND cat_is_delete = 0
                        ")->result();
        if (count($sub_cat) > 0) {

            $str = '<h5>Sub Category<span> * </span></h5>';
            $str .= "<select name='sub_cat' class='sub-cat' id='sub_cat' >";
            $str .= "<option value=''>Select</option>";
            foreach ($sub_cat as $subCat) {
                $str .= "<option value=" . $subCat->cat_id . ">" . $subCat->cat_name . "</option>";
            }
            $str .= "</select>";
            $str .= "<div class='error'></div>";


            echo $str;
        } else {
            echo 0;
        }
    }

    public function get_sub_sub_category()
    {
        $main_cat_id = $this->input->post('cat_id');
        /*$sub_cat = $this->common_model->commonselect2('tbl_categories', 'cat_parent_id', $main_cat_id, 'cat_status', 1)->result();*/
        $sub_cat = $this->db->query("
                            SELECT *
                            FROM tbl_categories
                            WHERE cat_parent_id = '".$main_cat_id."' AND cat_status = 1 AND cat_is_delete = 0
                        ")->result();
        if (count($sub_cat) > 0) {

            $str = '<h5>Sub Sub Category<span> * </span></h5>';
            $str .= "<select name='sub_sub_cat' class='sub-sub-cat'  id='sub_sub_cat' >";
            $str .= "<option value=''>Select</option>";
            foreach ($sub_cat as $subCat) {
                $str .= "<option value=" . $subCat->cat_id . ">" . $subCat->cat_name . "</option>";
            }
            $str .= "</select>";
            $str .= "<div class='error'></div>";

            echo $str;
        } else {
            echo 0;
        }
    }

// Mehmood

    public function getProductDetails()
    {
        $id = $this->input->post('id');
        $product = $this->common_model->commonselect('tbl_products', 'prod_id', $id)->row();
        $image = $this->common_model->commonselect('tbl_product_images', 'img_prod_id', $id)->result();
        $product_cat = $this->common_model->commonselect('tbl_product_category', 'prod_id', $id)->row();
        $product_filter = $this->common_model->commonselect('tbl_product_filters', 'prod_id', $id)->result();
        $product_filters_detail = $this->common_model->commonselect('tbl_product_filters_detail', 'prod_id', $id)->result();

        $result['product'] = $product;
        $result['images'] = $image;
        $result['product_cat'] = $product_cat;
        $result['product_filter'] = $product_filter;
        $result['product_filters_detail'] = $product_filters_detail;


        echo json_encode($result);
        // print_r($product);


    }

    public function get_required_filter_unique()
    {
        $cat_id = $this->input->post('cat_id');
        $is_has = 0;
        $filterReq = $this->common_model->commonselect3('tbl_cat_filter_title', 'filter_cat_id', $cat_id, 'is_required', 1, 'cat_filter_values != ', '')->result();
        $filterReqCon = $this->db->query("
                                SELECT f.*
                                FROM tbl_cat_filter_title f
                                INNER JOIN tbl_cat_filter_detail df ON f.filter_title_id = df.filter_detail_title_id
                                WHERE f.cat_filter_is_conditional = 1 AND
                                f.filter_cat_id = $cat_id AND
                                df.filter_detail != '' AND
                                f.is_required = 1 AND 
                                df.filter_detail != ''
                                GROUP BY f.filter_title_id
                                ")->result();
        $str = '';
        if (count($filterReq) > 0) {
            $is_has = 1;
            $str = '';

            //$str .=     "<option value=''>Select</option>";
            foreach ($filterReq as $filter) {
                $str .= '<div class="unique-itm-main-row-bx">';
                $str .= "<h5>$filter->filter_title <span> * </span></h5>";
                $str .= "<select name='$filter->filter_slug' required >";
                if ($filter->cat_filter_values != '') {

                    $options = explode(",", ltrim(rtrim($filter->cat_filter_values, ','), ','));
                    foreach ($options as $op) {
                        $str .= "<option value='" . $op . "'>" . $op . "</option>";
                    }
                }
                $str .= "</select>";
                $str .= "</div>";
            }
            //echo $str;
        }
        if (count($filterReqCon) > 0) {
            $is_has = 1;
            foreach ($filterReqCon as $filter) {
                $str .= '<div class="unique-itm-main-row-bx">';
                $str .= "<h5>$filter->filter_title <span> * </span></h5>";
                $str .= "<select name='$filter->filter_slug' required class='conditional-req'>";
                $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_title_id', $filter->filter_title_id)->result();
                if (count($values) > 0) {
                    foreach ($values as $op) {
                        $str .= "<option value='" . $op->filter_detail_id . "'>" . $op->filter_title . "</option>";
                    }
                }
                $str .= "</select>";
                $str .= "</div>";

                $str .= '<div class="unique-itm-main-row-bx ' . $filter->filter_slug . '">';
                $str .= "<h5>" . $values[0]->filter_title . "<span> * </span></h5>";
                $str .= "<select name='" . $values[0]->filter_slug . "' required >";
                $options = explode(",", ltrim(rtrim($values[0]->filter_detail, ','), ','));
                foreach ($options as $op) {
                    $str .= "<option value='" . $op . "'>" . $op . "</option>";
                }
                $str .= "</select>";
                $str .= "</div>";

            }
            //echo $str;
        }

        if ($is_has == 0) {
            echo 0;
        } else {
            echo $str;
        }
    }

    public function get_sub_conditional_req_unique()
    {
        $filter_id = $this->input->post('filter_id');
        $str = '';
        $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_id', $filter_id)->result();
        if (count($values) > 0) {
            $str .= "<h5>" . $values[0]->filter_title . "<span> * </span></h5>";
            $str .= "<select name='" . $values[0]->filter_slug . "' required >";
            $options = explode(",", ltrim(rtrim($values[0]->filter_detail, ','), ','));
            foreach ($options as $op) {
                $str .= "<option value='" . $op . "'>" . $op . "</option>";
            }
            $str .= "</select>";
            echo $str;
        } else {
            echo 0;
        }
    }

    public function get_additional_filter_unique()
    {
        $cat_id = $this->input->post('cat_id');
        $is_has = 0;
        $filterReq = $this->common_model->commonselect3('tbl_cat_filter_title', 'filter_cat_id', $cat_id, 'is_required', 0, 'cat_filter_values != ', '')->result();
        $filterReqCon = $this->db->query("
                                SELECT f.*
                                FROM tbl_cat_filter_title f
                                INNER JOIN tbl_cat_filter_detail df ON f.filter_title_id = df.filter_detail_title_id
                                WHERE f.cat_filter_is_conditional = 1 AND
                                f.filter_cat_id = $cat_id AND
                                df.filter_detail != '' AND
                                f.is_required = 0 AND 
                                df.filter_detail != ''
                                GROUP BY f.filter_title_id
                                ")->result();
        $str = '';
        if (count($filterReq) > 0) {
            $is_has = 1;
            $str = '';

            //$str .=     "<option value=''>Select</option>";
            foreach ($filterReq as $filter) {
                $str .= '<div class="unique-itm-main-row-bx">';
                $str .= "<h5>$filter->filter_title</h5>";
                $str .= "<select name='$filter->filter_slug'  >";
                $str .= "<option value=''>Select</option>";
                if ($filter->cat_filter_values != '') {

                    $options = explode(",", ltrim(rtrim($filter->cat_filter_values, ','), ','));
                    foreach ($options as $op) {
                        $str .= "<option value='" . $op . "'>" . $op . "</option>";
                    }
                }
                $str .= "</select>";
                $str .= "</div>";
            }
            //echo $str;
        }
        if (count($filterReqCon) > 0) {
            $is_has = 1;
            foreach ($filterReqCon as $filter) {
                $str .= '<div class="unique-itm-main-row-bx">';
                $str .= "<h5>$filter->filter_title</h5>";
                $str .= "<select name='$filter->filter_slug'  class='conditional'>";
                $str .= "<option value=''>Select</option>";
                $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_title_id', $filter->filter_title_id)->result();
                if (count($values) > 0) {
                    foreach ($values as $op) {
                        $str .= "<option value='" . $op->filter_detail_id . "'>" . $op->filter_title . "</option>";
                    }
                }
                $str .= "</select>";
                $str .= "</div>";

                $str .= '<div class="unique-itm-main-row-bx ' . $filter->filter_slug . '">';
                /* $str .= "<h5>".$values[0]->filter_title."</h5>";
                  $str .= "<select name='".$values[0]->filter_slug."'  >";
                  $options = explode(",",ltrim(rtrim($values[0]->filter_detail,','),',') );
                  foreach($options as $op){
                  $str .=  "<option value='".$op."'>".$op."</option>";
                  }
                  $str .= "</select>"; */
                $str .= "</div>";
            }
            //echo $str;
        }

        if ($is_has == 0) {
            echo 0;
        } else {
            echo $str;
        }
    }

    public function get_sub_conditional_unique()    {
        $filter_id = $this->input->post('filter_id');
        $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_id', $filter_id)->result();
        $str = '';
        if (count($values) > 0) {
            $str .= "<h5>" . $values[0]->filter_title . "</h5>";
            $str .= "<select name='" . $values[0]->filter_slug . "' >";
            $str .= "<option value=''>Select</option>";
            $options = explode(",", ltrim(rtrim($values[0]->filter_detail, ','), ','));
            foreach ($options as $op) {
                $str .= "<option value='" . $op . "'>" . $op . "</option>";
            }
            $str .= "</select>";
            echo $str;
        } else {
            echo 0;
        }
    }


    /*
     * ***************************************************************
     * ********************---------------------**********************
     * ****************** [ For Multiple Filters ] *******************
     * ******************** -------------------- *********************
     * ***************************************************************
     */
    public function get_required_filter_multi()
    {
        $cat_id = $this->input->post('cat_id');
        $is_has = 0;
        $filterReq = $this->common_model->commonselect3('tbl_cat_filter_title', 'filter_cat_id', $cat_id, 'is_required', 1, 'cat_filter_values != ', '')->result();
        $filterReqCon = $this->db->query("
                                SELECT f.*
                                FROM tbl_cat_filter_title f
                                INNER JOIN tbl_cat_filter_detail df ON f.filter_title_id = df.filter_detail_title_id
                                WHERE f.cat_filter_is_conditional = 1 AND
                                f.filter_cat_id = $cat_id AND
                                df.filter_detail != '' AND
                                f.is_required = 1 AND 
                                df.filter_detail != ''
                                GROUP BY f.filter_title_id
                                ")->result();
        $str = '<h4>Select at least one of each category<span>*</span></h4>';

        if (count($filterReq) > 0) {
            $is_has = 1;
            foreach ($filterReq as $filter) {
                $str .= '<div class="atleadst-one-catrg-bx">';
                $str .= "<h6>$filter->filter_title <span> * </span></h6>";
                $str .= '<div class="atleadst-one-catrg-bx-row">';
                if ($filter->cat_filter_values != '') {
                    $options = explode(",", ltrim(rtrim($filter->cat_filter_values, ','), ','));
                    $i = 1;
                    foreach ($options as $op) {
                        $str .= '<div class="catrg-bx-row-clr">';
                        $str .= '<input required type="checkbox" id="' . $i . '_' . $filter->filter_title_id . '" value="'.$op.'" name="multi_' . $filter->filter_slug . '[]">';
                        $str .= '<label for="' . $i . '_' . $filter->filter_title_id . '"><span></span><p>' . $op . '</p></label>';
                        $str .= '</div>';
                        $i++;
                    }
                }
                $str .= "</div>";
                $str .= "</div>";
            }
            //echo $str;
        }
        if (count($filterReqCon) > 0) {
            $i = $is_has = 1;

            foreach ($filterReqCon as $filter) {
                $str .= '<div class="atleadst-one-catrg-bx">';
                $str .= "<h6>$filter->filter_title <span> * </span></h6>";
                $str .= '   <div class="atleadst-one-catrg-bx-row">';
                $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_title_id', $filter->filter_title_id)->result();
                if (count($values) > 0) {
                    foreach ($values as $op) {
                        $str .= '<div class="catrg-bx-row-clr ">';
                        $str .= '   <input class="multi-conditional" required type="checkbox" id="' . $i . '_' . $filter->filter_title_id . '" value="'.$op->filter_detail_id.'" data-name2="multi_' . $filter->filter_slug . '' . $i . '_' . $filter->filter_title_id . '" name="multi_' . $filter->filter_slug . '[]">';
                        $str .= '   <label for="' . $i . '_' . $filter->filter_title_id . '"><span></span><p>' . $op->filter_title . '</p></label>';
                        $str .= '   <div class="multi_' . $filter->filter_slug . '' . $i . '_' . $filter->filter_title_id . '"></div>';
                        $str .= '</div>';
                        $i++;
                    }
                }
                $str .= "   </div>";
                $str .= "</div>";
            }
            //echo $str;
        }

        if ($is_has == 0) {
            echo 0;
        } else {
            echo $str;
        }
    }

    public function get_sub_conditional_req_multi()
    {
        $filter_id = $this->input->post('filter_id');
        $str = '';
        $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_id', $filter_id)->result();
        if (count($values) > 0) {
            $options = explode(",", ltrim(rtrim($values[0]->filter_detail, ','), ','));
            $i = 1;
            foreach ($options as $op) {
                $str .= '<div class="catrg-bx-row-clr">';
                $str .= '   <input required type="checkbox" id="sub_' . $i . '_' . $values[0]->filter_detail_id . '" value="'.$op.'" name="multi_' . $values[0]->filter_slug . '[]">';
                $str .= '   <label for="sub_' . $i . '_' . $values[0]->filter_detail_id . '"><span></span><p>' . $op . '</p></label>';
                $str .= '</div>';
                $i++;
            }
            echo $str;
        } else {
            echo 0;
        }
    }

    public function valid_sku_add()
    {
        $sku = $this->input->post('sku');
        $prod_sku = $this->common_model->commonselect('tbl_products', 'prod_sku', $sku)->result();
        if (count($prod_sku) > 0) {
            echo 0;
        } else {
            echo 1;
        }
    }

    public function addNewProduct()
    {
        delete_cookie('img_counter');
        $data['title'] = "Create New Product";
        /*$data['mainCat'] = $this->common_model->commonselect2('tbl_categories', 'cat_parent_id', 0, 'cat_status', 1, 'cat_sort', 'ASC')->result();*/
        $data['mainCat'] = $this->db->query("
                            SELECT *
                            FROM tbl_categories
                            WHERE cat_parent_id = 0 AND cat_status = 1 AND cat_is_delete = 0
                        ")->result();
        $this->form_validation->set_rules('looking_for', 'This', 'trim|required');
        $this->form_validation->set_rules('condition', 'This', 'trim|required');
        $this->form_validation->set_rules('qty', 'This', 'trim|required|integer|greater_than[0]');
        $this->form_validation->set_rules('prod_title', 'This', 'trim|required');
        //$this->form_validation->set_rules('prod_sku', 'This', 'trim|required|is_unique[tbl_products.prod_sku]');
        $this->form_validation->set_rules('prod_description', 'This', 'trim|required');
//        $this->form_validation->set_rules('img_name[]','This','trim|required');
        $this->form_validation->set_rules('youtube_links', 'This', 'trim');
        $this->form_validation->set_rules('prod_price', 'This', 'trim|required');

        if ($this->form_validation->run() == false) {
            $data['content_view'] = "products/add-new-product";
            //Haseeb Code
            $data['TireInfo'] = $this->common_model->commonselect('tbl_tier_list','tier_id',$this->storeInfo->store_tier_id)->row();
            
            // Mehmood Code
            $data['products'] = $this->common_model->commonselect2('tbl_products', 'prod_store_id', $this->storeInfo->store_id, 'prod_is_delete', 0)->result();
            /*echo "<pre>";
            print_r($data);
            exit;*/
            $this->load->view("front/custom_template", $data);
        } else {
            /*echo '<pre>';
            print_r($_FILES);
            exit;*/
            $prodData['looking_for'] = $this->input->post('looking_for');
            $prodData['condition'] = $this->input->post('condition');
            $prodData['qty'] = $this->input->post('qty');
            $prodData['prod_title'] = $this->input->post('prod_title');
            $prodData['prod_url'] = $this->common_model->create_slug($prodData['prod_title'], 'tbl_categories', 'cat_url');
            $prodData['prod_url'] = $this->common_model->create_slug($prodData['prod_url'], 'tbl_products', 'prod_url');
            //$prodData['prod_sku'] = $this->input->post('prod_sku');
            $prodData['prod_description'] = $this->input->post('prod_description');
            $prodData['youtube_links'] = $this->input->post('youtube_links');
            $prodData['prod_price'] = number_format($this->input->post('prod_price'),2,'.','');
            $prodData['prod_onsale'] = $this->input->post('is_onsale');
            if ($prodData['prod_onsale'] == 1) {
                $prodData['sale_price'] = number_format($this->input->post('sale_price'),2,'.','');
                $prodData['sale_start_date'] = '';
                $prodData['sale_end_date'] = '';
                if($this->input->post('sale_start_date') != ''){
                    $prodData['sale_start_date'] = date('Y-m-d', strtotime($this->input->post('sale_start_date')));
                }
                if($this->input->post('sale_start_date') != ''){
                    $prodData['sale_end_date'] =  date('Y-m-d', strtotime($this->input->post('sale_end_date')));
                }
            }
            $prodData['shipping'] = $this->input->post('shipping');
            if ($prodData['shipping'] == "Charge for Shipping") {
                $prodData['ship_price'] = $this->input->post('ship_price');
                $prodData['ship_days'] = $this->input->post('ship_days');
            } else if ($prodData['shipping'] == "Offer free Shipping") {
                $prodData['free_ship_days'] = $this->input->post('free_ship_days');
            }
            $prodData['prod_return'] = $this->input->post('prod_return');

            $prodData['prod_cat_id'] = $this->input->post('mainCat');
            if ($this->input->post('sub_sub_cat') != '') {
                $prodData['prod_cat_id'] = $this->input->post('sub_sub_cat');
            } else if ($this->input->post('sub_cat') != '') {
                $prodData['prod_cat_id'] = $this->input->post('sub_cat');
            }
            $prodData['prod_created_date'] = date('Y-m-d H:i:s');;
            $prodData['prod_store_id'] = $this->input->post('sub_cat');
            $prodData['prod_user_id'] = $this->session->userdata('userid');

            $prodData['prod_store_id'] = $this->storeInfo->store_id;
            $prodData['is_unique_filter'] = $this->input->post('is_unique');

           
            // Insert Product Data
            $prod_id = $this->common_model->commonSave('tbl_products', $prodData);

            $catData['category_id'] = $this->input->post('mainCat');
            $catData['sub_category_id'] = $this->input->post('sub_cat');
            $catData['sub_sub_category_id'] = $this->input->post('sub_sub_cat');
            $catData['prod_id'] = $prod_id;

            // Insert Categories
            $prod_cat_id = $this->common_model->commonSave('tbl_product_category', $catData);

            // Insert image Data
           
            /*foreach ($this->input->post('img_name') as $img) {
               $imgData['img_name'] = $img;
                $imgData['img_prod_id'] = $prod_id;
                $this->common_model->commonSave('tbl_product_images', $imgData);
            }*/

            $name = $this->input->post('img_name');
            $label = $this->input->post('img_label');
            for ($i=0; $i < count($this->input->post('img_name')) ; $i++) {                 
                $imgData['img_name'] = $name[$i];
                $imgData['img_label'] = $label[$i];
                $imgData['img_prod_id'] = $prod_id;
                $this->common_model->commonSave('tbl_product_images', $imgData);
            }

            // Insert Filters Data in the case of Unique Filter
            $is_unique = $this->input->post('is_unique'); // 1 for unique and 0 for multiple

            if ($is_unique == 1) {
                // Add unique filters
                $sub_sub_cat_id = $this->input->post('sub_sub_cat');
                if ($this->input->post('sub_sub_cat') != '') {
                    $filters = $this->common_model->commonselect('tbl_cat_filter_title', 'filter_cat_id', $sub_sub_cat_id)->result();
                    foreach ($filters as $row) {
                        if ($this->input->post($row->filter_slug)) {
                            // If value post is not empty
                            $filtersData['prod_id'] = $prod_id;
                            $filtersData['category_id'] = $sub_sub_cat_id;
                            $filtersData['filter_id'] = $row->filter_title_id;
                            $filtersData['filter_slug'] = $row->filter_slug;
                            $filtersData['filter_value'] = $this->input->post($row->filter_slug);
                            $filtersData['is_unique'] = 1;
                            $filterId = $this->common_model->commonSave('tbl_product_filters', $filtersData);

                            // If this filter is conditional get the detail value and save it.
                            $filter_dtil = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_id', $this->input->post($row->filter_slug))->row();
                            if ($row->cat_filter_is_conditional == 1 && $this->input->post($filter_dtil->filter_slug) != '') {
                                $filtersDetailData['prod_filter_id'] = $filterId;
                                $filtersDetailData['prod_id'] = $prod_id;
                                $filtersDetailData['category_id'] = $sub_sub_cat_id;
                                $filtersDetailData['filter_id'] = $row->filter_title_id;
                                $filtersDetailData['filter_detail_id'] = $this->input->post($row->filter_slug);
                                $filtersDetailData['filter_slug'] = $filter_dtil->filter_slug;
                                $filtersDetailData['filter_value'] = $this->input->post($filter_dtil->filter_slug);
                                $filtersDetailData['filter_is_unique'] = 1;
                                $filterDetailId = $this->common_model->commonSave('tbl_product_filters_detail', $filtersDetailData);
                            }
                        }
                    }
                }
            } else {
                // Add multiple filters.
                $sub_sub_cat_id = $this->input->post('sub_sub_cat');
                if ($this->input->post('sub_sub_cat') != '') {
                    $filters = $this->common_model->commonselect('tbl_cat_filter_title', 'filter_cat_id', $sub_sub_cat_id)->result();
                    foreach ($filters as $row) {
                        $filtersData['prod_id'] = $prod_id;
                        $filtersData['category_id'] = $sub_sub_cat_id;
                        $filtersData['filter_id'] = $row->filter_title_id;
                        $filtersData['filter_slug'] = $row->filter_slug;
                        //$filtersData['filter_value'] = $this->input->post($row->filter_slug);
                        $filtersData['is_unique'] = 0;
                        $fil_value = $is_multi = '';

                        if ($row->is_required == 1) {
                            $fil_value = $this->input->post('multi_' . $row->filter_slug);
                            // echo 'if: '.$row->filter_slug.' : '.($fil_value=='').'<br/>';
                            // $is_multi = 1;
                        } else {
                            $fil_value = $this->input->post($row->filter_slug);
                            if ($fil_value != '') {
                                $fil_value = (array)$fil_value;
                            } else {
                                $fil_value = array();
                            }
                            // echo 'else: '.$row->filter_slug.' : '.($fil_value=='').'<br/>';
                            // $is_multi = 0;
                        }

                        if (count($fil_value) > 0) {
                            if (!is_array($fil_value)) {
                                $fil_value = (array)$fil_value;
                            }
                            foreach ($fil_value as $filVal) {
                                $filtersData['filter_value'] = $filVal;
                                $filterId = $this->common_model->commonSave('tbl_product_filters', $filtersData);
                                // If this filter is conditional get the detail value and save it.
                                $filter_dtil = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_id', $filVal)->row();
                                if ($row->cat_filter_is_conditional == 1) {
                                    $filtersDetailData['prod_filter_id'] = $filterId;
                                    $filtersDetailData['prod_id'] = $prod_id;
                                    $filtersDetailData['category_id'] = $sub_sub_cat_id;
                                    $filtersDetailData['filter_id'] = $row->filter_title_id;
                                    $filtersDetailData['filter_slug'] = $filter_dtil->filter_slug;
                                    $filtersDetailData['filter_is_unique'] = 0;

                                    if ($row->is_required == 1) {
                                        $fil_detail_val = $this->input->post('multi_' . $filter_dtil->filter_slug);
                                    } else {
                                        $fil_detail_val = $this->input->post($filter_dtil->filter_slug);
                                        if ($fil_detail_val != '') {
                                            $fil_detail_val = (array)$fil_value;
                                        } else {
                                            $fil_detail_val = array();
                                        }
                                    }

                                    if (count($fil_detail_val) > 0) {
                                        foreach ($fil_detail_val as $fil_del_val) {
                                            $filtersDetailData['filter_detail_id'] = $filVal;
                                            $filtersDetailData['filter_value'] = $fil_del_val;
                                            $filterDetailId = $this->common_model->commonSave('tbl_product_filters_detail', $filtersDetailData);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $this->session->set_flashdata('addProduct', '1');
            redirect(base_url('manage-products'));

            // echo '<pre>';
            // print_r($_POST);
            exit;

        }
    }

    public function get_required_filter_unique_edit($prod_id = '')
    {
        $cat_id = $this->input->post('cat_id');
        $is_has = 0;
        $filterReq = $this->common_model->commonselect3('tbl_cat_filter_title', 'filter_cat_id', $cat_id, 'is_required', 1, 'cat_filter_values != ', '')->result();
        $filterReqCon = $this->db->query("
                                SELECT f.*
                                FROM tbl_cat_filter_title f
                                INNER JOIN tbl_cat_filter_detail df ON f.filter_title_id = df.filter_detail_title_id
                                WHERE f.cat_filter_is_conditional = 1 AND
                                f.filter_cat_id = $cat_id AND
                                df.filter_detail != '' AND
                                f.is_required = 1 AND 
                                df.filter_detail != ''
                                GROUP BY f.filter_title_id
                                ")->result();
        $str = '';
        if (count($filterReq) > 0) {
            $is_has = 1;
            $str = '';

            //$str .=     "<option value=''>Select</option>";
            foreach ($filterReq as $filter) {
                $prodFilter = $this->common_model->commonselect2('tbl_product_filters', 'filter_id', $filter->filter_title_id, 'prod_id', $prod_id)->row();
                //
                $str .= '<div class="unique-itm-main-row-bx">';
                $str .= "<h5>$filter->filter_title <span> * </span></h5>";
                $str .= "<select name='$filter->filter_slug' required >";
                if ($filter->cat_filter_values != '') {

                    $options = explode(",", ltrim(rtrim($filter->cat_filter_values, ','), ','));
                    foreach ($options as $op) {
                        $str .= "<option value='".$op."'".((@$prodFilter->filter_value == $op) ? 'selected' : '') . " >" . $op . "</option>";
                    }
                }
                $str .= "</select>";
                $str .= "</div>";
            }
            //echo $str;
        }
        if (count($filterReqCon) > 0) {
            $is_has = 1;
            foreach ($filterReqCon as $filter) {
                $prodFilter = $this->common_model->commonselect2('tbl_product_filters', 'filter_id', $filter->filter_title_id, 'prod_id', $prod_id)->row();
                $prodFilter_id = 0;
                $subFilterTitle = $subFilterSlug = '';

                $str .= '<div class="unique-itm-main-row-bx">';
                $str .= "<h5>$filter->filter_title <span> * </span></h5>";
                $str .= "<select name='$filter->filter_slug' required class='conditional-req'>";
                $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_title_id', $filter->filter_title_id)->result();
                if (count($values) > 0) {
                    foreach ($values as $op) {
                        $str .= "<option value='" . $op->filter_detail_id . "'" . ((@$prodFilter->filter_value == $op->filter_detail_id) ? 'selected' : '') . " >" . $op->filter_title . "</option>";
                        if (@$prodFilter->filter_value == $op->filter_detail_id) {
                            $prodFilter_id = $op->filter_detail_id;
                            $subFilterTitle = $op->filter_title;
                            $subFilterSlug = $op->filter_slug;
                        }
                    }
                }
                $str .= "</select>";
                $str .= "</div>";
                //
                if ($prodFilter_id > 0) {
                    $prodDeFilter = $this->common_model->commonselect('tbl_product_filters_detail', 'prod_filter_id', $prodFilter->id)->row();
                    //
                    $str .= '<div class="unique-itm-main-row-bx ' . $prodDeFilter->filter_slug . '">';
                    $str .= "<h5>" . $subFilterTitle . "<span> * </span></h5>";
                    $str .= "<select name='" . $subFilterSlug . "' required >";
                    $options = explode(",", ltrim(rtrim($values[0]->filter_detail, ','), ','));
                    foreach ($options as $op) {
                        $str .= "<option value='".$op."'" . ((@$prodDeFilter->filter_value == $op) ? 'selected' : '') . " >" . $op . "</option>";
                    }
                    $str .= "</select>";
                    $str .= "</div>";
                } else {
                    $str .= '<div class="unique-itm-main-row-bx ' . $filter->filter_slug . '">';
                    $str .= "<h5>" . $values[0]->filter_title . "<span> * </span></h5>";
                    $str .= "<select name='" . $values[0]->filter_slug . "' required >";
                    $options = explode(",", ltrim(rtrim($values[0]->filter_detail, ','), ','));
                    foreach ($options as $op) {
                        $str .= "<option value='" . $op . "'>" . $op . "</option>";
                    }
                    $str .= "</select>";
                    $str .= "</div>";
                }
            }
            //echo $str;
        }

        if ($is_has == 0) {
            echo 0;
        } else {
            echo $str;
        }
    }

    public function get_additional_filter_unique_edit($prod_id = '')
    {
        $cat_id = $this->input->post('cat_id');
        $is_has = 0;
        $filterReq = $this->common_model->commonselect3('tbl_cat_filter_title', 'filter_cat_id', $cat_id, 'is_required', 0, 'cat_filter_values != ', '')->result();
        $filterReqCon = $this->db->query("
                                SELECT f.*
                                FROM tbl_cat_filter_title f
                                INNER JOIN tbl_cat_filter_detail df ON f.filter_title_id = df.filter_detail_title_id
                                WHERE f.cat_filter_is_conditional = 1 AND
                                f.filter_cat_id = $cat_id AND
                                df.filter_detail != '' AND
                                f.is_required = 0 
                                GROUP BY f.filter_title_id
                                ")->result();
        $str = '';
        if (count($filterReq) > 0) {
            $is_has = 1;
            $str = '';

            //$str .=     "<option value=''>Select</option>";
            foreach ($filterReq as $filter) {
                $prodFilter = $this->common_model->commonselect2('tbl_product_filters', 'filter_id', $filter->filter_title_id, 'prod_id', $prod_id)->row();
                //
                $str .= '<div class="unique-itm-main-row-bx">';
                $str .= "<h5>$filter->filter_title</h5>";
                $str .= "<select name='$filter->filter_slug'  >";
                $str .= "<option value=''>Select</option>";
                if ($filter->cat_filter_values != '') {

                    $options = explode(",", ltrim(rtrim($filter->cat_filter_values, ','), ','));
                    foreach ($options as $op) {
                        $str .= "<option value='".$op."'" . ((@$prodFilter->filter_value == $op) ? 'selected' : '') . " >".$op."</option>";
                    }
                }
                $str .= "</select>";
                $str .= "</div>";
            }
            //echo $str;
        }
        if (count($filterReqCon) > 0) {
            $is_has = 1;
            foreach ($filterReqCon as $filter) {
                $prodFilter = $this->common_model->commonselect2('tbl_product_filters', 'filter_id', $filter->filter_title_id, 'prod_id', $prod_id)->row();
                $prodFilter_id = 0;
                $subFilterTitle = $subFilterSlug = '';

                $str .= '<div class="unique-itm-main-row-bx">';
                $str .= "<h5>$filter->filter_title</h5>";
                $str .= "<select name='$filter->filter_slug'  class='conditional'>";
                $str .= "<option value=''>Select</option>";
                $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_title_id', $filter->filter_title_id)->result();
                if (count($values) > 0) {
                    foreach ($values as $op) {
                        $str .= "<option value='" . $op->filter_detail_id . "'" . ((@$prodFilter->filter_value == $op->filter_detail_id) ? 'selected' : '') . " >" . $op->filter_title . "</option>";
                        if (@$prodFilter->filter_value == $op->filter_detail_id) {
                            $prodFilter_id = $op->filter_detail_id;
                            $subFilterTitle = $op->filter_title;
                            $subFilterSlug = $op->filter_slug;
                        }
                    }
                }
                $str .= "</select>";
                $str .= "</div>";
                //
                if ($prodFilter_id > 0) {
                    $prodDeFilter = $this->common_model->commonselect('tbl_product_filters_detail', 'prod_filter_id', $prodFilter->id)->row();
                    //
                    $str .= '<div class="unique-itm-main-row-bx ' . $prodDeFilter->filter_slug . '">';
                    $str .= "<h5>" . $subFilterTitle . "<span> * </span></h5>";
                    $str .= "<select name='" . $subFilterSlug . "' >";
                    $options = explode(",", ltrim(rtrim($values[0]->filter_detail, ','), ','));
                    foreach ($options as $op) {
                        $str .= "<option value='".$op."'" . ((@$prodDeFilter->filter_value == $op) ? 'selected' : '') . " >".$op."</option>";
                    }
                    $str .= "</select>";
                    $str .= "</div>";
                } else {
                    $str .= '<div class="unique-itm-main-row-bx ' . $filter->filter_slug . '">';
                    /* $str .= "<h5>".$values[0]->filter_title."</h5>";
                      $str .= "<select name='".$values[0]->filter_slug."'  >";
                      $options = explode(",",ltrim(rtrim($values[0]->filter_detail,','),',') );
                      foreach($options as $op){
                      $str .=  "<option value='".$op."'>".$op."</option>";
                      }
                      $str .= "</select>"; */
                    $str .= "</div>";
                }
            }
            //echo $str;
        }

        if ($is_has == 0) {
            echo 0;
        } else {
            echo $str;
        }
    }

    public function get_required_filter_multi_edit($prod_id = '') {
        $cat_id = $this->input->post('cat_id');
        $is_has = 0;
        $filterReq = $this->common_model->commonselect3('tbl_cat_filter_title', 'filter_cat_id', $cat_id, 'is_required', 1, 'cat_filter_values != ', '')->result();
        $filterReqCon = $this->db->query("
                                SELECT f.*
                                FROM tbl_cat_filter_title f
                                INNER JOIN tbl_cat_filter_detail df ON f.filter_title_id = df.filter_detail_title_id
                                WHERE f.cat_filter_is_conditional = 1 AND
                                f.filter_cat_id = $cat_id AND
                                df.filter_detail != '' AND
                                f.is_required = 1 AND 
                                df.filter_detail != ''
                                GROUP BY f.filter_title_id
                                ")->result();
        $str = '<h4>Select at least one of each category<span>*</span></h4>';

        if (count($filterReq) > 0) {
            $is_has = 1;
            foreach ($filterReq as $filter) {
                $prodFilter = $this->common_model->commonselect2('tbl_product_filters', 'filter_id', $filter->filter_title_id, 'prod_id', $prod_id)->result_array();

                $newarray = array();
                foreach ($prodFilter as $value) {
                    $newarray[] = $value['filter_value'];
                }


                $str .= '<div class="atleadst-one-catrg-bx">';
                $str .= "<h6>$filter->filter_title <span> * </span></h6>";
                $str .= '<div class="atleadst-one-catrg-bx-row">';
                if ($filter->cat_filter_values != '') {
                    $options = explode(",", ltrim(rtrim($filter->cat_filter_values, ','), ','));
                    $i = 1;
                    foreach ($options as $op) {
                        $str .= '<div class="catrg-bx-row-clr">';
                        $str .= '<input required type="checkbox" id="' . $i . '_' . $filter->filter_title_id.'" value="'.$op.'"'.((in_array($op, $newarray)) ? "checked" : "") . ' name="multi_' . $filter->filter_slug . '[]">';
                        $str .= '<label for="' . $i . '_' . $filter->filter_title_id . '"><span></span><p>' . $op . '</p></label>';
                        $str .= '</div>';
                        $i++;
                    }
                }
                $str .= "</div>";
                $str .= "</div>";
            }
            //echo $str;
        }
        if (count($filterReqCon) > 0) {
            $i = $is_has = 1;

            foreach ($filterReqCon as $filter) {
                $prodFilter = $this->common_model->commonselect2('tbl_product_filters', 'filter_id', $filter->filter_title_id, 'prod_id', $prod_id)->result_array();

                $newarray = array();
                foreach ($prodFilter as $value) {
                    $newarray[] = $value['filter_value'];
                }

                $str .= '<div class="atleadst-one-catrg-bx">';
                $str .= "<h6>$filter->filter_title <span> * </span></h6>";
                $str .= '   <div class="atleadst-one-catrg-bx-row">';
                $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_title_id', $filter->filter_title_id)->result();
                if (count($values) > 0) {
                    foreach ($values as $op) {
                        $str .= '<div class="catrg-bx-row-clr ">';
                        $str .= '   <input class="multi-conditional" required type="checkbox" id="' . $i . '_' . $filter->filter_title_id . '" value="'.$op->filter_detail_id.'"' . ((in_array($op->filter_detail_id, $newarray)) ? "checked" : "") . ' data-name2="multi_' . $filter->filter_slug . '' . $i . '_' . $filter->filter_title_id . '" name="multi_' . $filter->filter_slug . '[]">';
                        $str .= '   <label for="' . $i . '_' . $filter->filter_title_id . '"><span></span><p>' . $op->filter_title . '</p></label>';
                        $str .= '   <div class="multi_' . $filter->filter_slug . '' . $i . '_' . $filter->filter_title_id . '">';

                        if (in_array($op->filter_detail_id, $newarray)) {
                            $filter_id = $op->filter_detail_id;
                            //
                            $values = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_id', $filter_id)->result();
                            if (count($values) > 0) {
                                $prodDeFilter = $this->common_model->commonselect2('tbl_product_filters_detail', 'filter_detail_id', $filter_id, 'prod_id', $prod_id)->result_array();
                                //

                                $newarray2 = array();
                                foreach ($prodDeFilter as $value2) {
                                    $newarray2[] = $value2['filter_value'];
                                }

                                $options = explode(",", ltrim(rtrim($values[0]->filter_detail, ','), ','));
                                $j = 1;
                                foreach ($options as $op) {
                                    $str .= '<div class="catrg-bx-row-clr">';
                                    $str .= '   <input required type="checkbox" id="sub_' . $j . '_' . $values[0]->filter_detail_id . '" value="'.$op.'"'.((in_array($op, $newarray2)) ? "checked" : "") . ' name="multi_' . $values[0]->filter_slug . '[]">';
                                    $str .= '   <label for="sub_' . $j . '_' . $values[0]->filter_detail_id . '"><span></span><p>' . $op . '</p></label>';
                                    $str .= '</div>';
                                    $j++;
                                }
                            }
                        }

                        $str .= '</div>';
                        $str .= '</div>';
                        $i++;
                    }
                }
                $str .= "   </div>";
                $str .= "</div>";
            }
            //echo $str;
        }

        if ($is_has == 0) {
            echo 0;
        } else {
            echo $str;
        }
    }
    public function editProduct($prod_url = '')
    {
        if ($prod_url == '') {
            redirect(base_url('manage-products'));
            exit;
        }
        
        $data['prodData'] = $this->common_model->commonselect3('tbl_products','prod_url', $prod_url,'prod_store_id',$this->storeInfo->store_id,'prod_is_delete', 0)->row();
        if (count($data['prodData']) <= 0) {
            redirect(base_url('manage-products'));
            exit;
        }

        $data['title'] = "Edit Product";
        /*$data['mainCat'] = $this->common_model->commonselect2('tbl_categories', 'cat_parent_id', 0, 'cat_status', 1, 'cat_sort', 'ASC')->result();*/
        $data['mainCat'] = $this->db->query("
                            SELECT *
                            FROM tbl_categories
                            WHERE cat_parent_id = 0 AND cat_status = 1 AND cat_is_delete = 0
                        ")->result();
        $data['prodCat'] = $this->common_model->commonselect('tbl_product_category', 'prod_id', $data['prodData']->prod_id)->row();
        $data['prodImg'] = $this->common_model->commonselect('tbl_product_images', 'img_prod_id', $data['prodData']->prod_id)->result();
        setcookie('img_counter',count($data['prodImg']), 0, '/');
        $this->form_validation->set_rules('looking_for', 'This', 'trim|required');
        $this->form_validation->set_rules('condition', 'This', 'trim|required');
        $this->form_validation->set_rules('qty', 'This', 'trim|required|integer|greater_than[0]');
        $this->form_validation->set_rules('prod_title', 'This', 'trim|required');
        //$this->form_validation->set_rules('prod_sku','This','trim|required');
        $this->form_validation->set_rules('prod_description', 'This', 'trim|required');
        //$this->form_validation->set_rules('img_name[]','This','trim|required');
        $this->form_validation->set_rules('youtube_links', 'This', 'trim');
        $this->form_validation->set_rules('prod_price', 'This', 'trim|required');
//        echo 1;
//        die;
        if ($this->form_validation->run() == false) {
            $data['content_view'] = "products/edit-product";
            //Haseeb Code
            $data['TireInfo'] = $this->common_model->commonselect('tbl_tier_list','tier_id',$this->storeInfo->store_tier_id)->row();
            $this->load->view("front/custom_template", $data);
        } else {
            delete_cookie('img_counter');
            $prodData['looking_for'] = $this->input->post('looking_for');
            $prodData['condition'] = $this->input->post('condition');
            $prodData['qty'] = $this->input->post('qty');
            $prodData['prod_title'] = $this->input->post('prod_title');
            $prodData['prod_url'] = $this->common_model->create_slug($prodData['prod_title'], 'tbl_categories', 'cat_url');
            $prodData['prod_url'] = $this->common_model->create_slug($prodData['prod_url'], 'tbl_products', 'prod_url', 'prod_id', $data['prodData']->prod_id);
            //$prodData['prod_sku'] = $this->input->post('prod_sku');
            $prodData['prod_description'] = $this->input->post('prod_description');
            $prodData['youtube_links'] = $this->input->post('youtube_links');
            $prodData['prod_price'] = number_format($this->input->post('prod_price'),2,'.','');
            $prodData['prod_onsale'] = $this->input->post('is_onsale');
            if ($prodData['prod_onsale'] == 1) {
                $prodData['sale_price'] = number_format($this->input->post('sale_price'),2,'.','');
                $prodData['sale_start_date'] = '';
                $prodData['sale_end_date'] = '';
                if($this->input->post('sale_start_date') != ''){
                    $prodData['sale_start_date'] = date('Y-m-d', strtotime($this->input->post('sale_start_date')));
                }
                if($this->input->post('sale_start_date') != ''){
                    $prodData['sale_end_date'] =  date('Y-m-d', strtotime($this->input->post('sale_end_date')));
                }
                
                // $prodData['sale_start_date'] = date('Y-m-d', strtotime($this->input->post('sale_start_date')));
                // $prodData['sale_end_date'] =  date('Y-m-d', strtotime($this->input->post('sale_end_date')));

            }
            $prodData['shipping'] = $this->input->post('shipping');
            if ($prodData['shipping'] == "Charge for Shipping") {
                $prodData['ship_price'] = $this->input->post('ship_price');
                $prodData['ship_days'] = $this->input->post('ship_days');
            } else if ($prodData['shipping'] == "Offer free Shipping") {
                $prodData['free_ship_days'] = $this->input->post('free_ship_days');
            }
            $prodData['prod_return'] = $this->input->post('prod_return');

            $prodData['prod_cat_id'] = $this->input->post('mainCat');
            if ($this->input->post('sub_sub_cat') != '') {
                $prodData['prod_cat_id'] = $this->input->post('sub_sub_cat');
            } else if ($this->input->post('sub_cat') != '') {
                $prodData['prod_cat_id'] = $this->input->post('sub_cat');
            }
            $prodData['prod_created_date'] = date('Y-m-d H:i:s');;
            $prodData['prod_store_id'] = $this->input->post('sub_cat');
            $prodData['prod_user_id'] = $this->session->userdata('userid');

            $prodData['prod_store_id'] = $this->storeInfo->store_id;

            // Insert Product Data
            $prod_id = $data['prodData']->prod_id;

            $prodData['is_unique_filter'] = $this->input->post('is_unique');

            $this->common_model->commonUpdate('tbl_products', $prodData, 'prod_id', $prod_id);


            $catData['category_id'] = $this->input->post('mainCat');
            $catData['sub_category_id'] = $this->input->post('sub_cat');
            $catData['sub_sub_category_id'] = $this->input->post('sub_sub_cat');
            $catData['prod_id'] = $prod_id;

            // Insert Categories
            $prod_cat_id = $data['prodCat']->prod_cat_id;
            $this->common_model->commonUpdate('tbl_product_category', $catData, 'prod_id', $prod_id);

            $this->common_model->commonDelete('tbl_product_images', 'img_prod_id', $prod_id);
            foreach ($data['prodImg'] as $img) {
                if (!in_array($img->img_name, $this->input->post('img_name'))) {
                    unlink(FCPATH . 'resources/prod_images/' . $img->img_name);
                    unlink(FCPATH . 'resources/prod_images/thumb/' . $img->img_name);
                }
            }
            $this->common_model->commonDelete('tbl_product_filters', 'prod_id', $prod_id);
            $this->common_model->commonDelete('tbl_product_filters_detail', 'prod_id', $prod_id);

            // Insert image Data
            $name = $this->input->post('img_name');
            $label = $this->input->post('img_label');
            for ($i=0; $i < count($this->input->post('img_name')) ; $i++) {                 
                $imgData['img_name'] = $name[$i];
                $imgData['img_label'] = $label[$i];
                $imgData['img_prod_id'] = $prod_id;
                $this->common_model->commonSave('tbl_product_images', $imgData);
            }
            /*foreach ($this->input->post('img_name') as $img) {
                $imgData['img_name'] = $img;
                $imgData['img_prod_id'] = $prod_id;
                $this->common_model->commonSave('tbl_product_images', $imgData);
            }*/

            // Insert Filters Data in the case of Unique Filter
            $is_unique = $this->input->post('is_unique'); // 1 for unique and 0 for multiple
            if ($is_unique == 1) {
                // Add unique filters
                $sub_sub_cat_id = $this->input->post('sub_sub_cat');
                if ($this->input->post('sub_sub_cat') != '') {
                    $filters = $this->common_model->commonselect('tbl_cat_filter_title', 'filter_cat_id', $sub_sub_cat_id)->result();
                    foreach ($filters as $row) {
                        if ($this->input->post($row->filter_slug)) {
                            // If value post is not empty
                            $filtersData['prod_id'] = $prod_id;
                            $filtersData['category_id'] = $sub_sub_cat_id;
                            $filtersData['filter_id'] = $row->filter_title_id;
                            $filtersData['filter_slug'] = $row->filter_slug;
                            $filtersData['filter_value'] = $this->input->post($row->filter_slug);
                            $filtersData['is_unique'] = 1;
                            $filterId = $this->common_model->commonSave('tbl_product_filters', $filtersData);

                            // If this filter is conditional get the detail value and save it.
                            $filter_dtil = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_id', $this->input->post($row->filter_slug))->row();
                            if ($row->cat_filter_is_conditional == 1 && $this->input->post($filter_dtil->filter_slug) != '') {
                                $filtersDetailData['prod_filter_id'] = $filterId;
                                $filtersDetailData['prod_id'] = $prod_id;
                                $filtersDetailData['category_id'] = $sub_sub_cat_id;
                                $filtersDetailData['filter_id'] = $row->filter_title_id;
                                $filtersDetailData['filter_detail_id'] = $this->input->post($row->filter_slug);
                                $filtersDetailData['filter_slug'] = $filter_dtil->filter_slug;
                                $filtersDetailData['filter_value'] = $this->input->post($filter_dtil->filter_slug);
                                $filtersDetailData['filter_is_unique'] = 1;
                                $filterDetailId = $this->common_model->commonSave('tbl_product_filters_detail', $filtersDetailData);
                            }
                        }
                    }
                }
            } else {
                // Add multiple filters.
                $sub_sub_cat_id = $this->input->post('sub_sub_cat');
                if ($this->input->post('sub_sub_cat') != '') {
                    $filters = $this->common_model->commonselect('tbl_cat_filter_title', 'filter_cat_id', $sub_sub_cat_id)->result();
                    foreach ($filters as $row) {
                        $filtersData['prod_id'] = $prod_id;
                        $filtersData['category_id'] = $sub_sub_cat_id;
                        $filtersData['filter_id'] = $row->filter_title_id;
                        $filtersData['filter_slug'] = $row->filter_slug;
                        //$filtersData['filter_value'] = $this->input->post($row->filter_slug);
                        $filtersData['is_unique'] = 0;
                        $fil_value = $is_multi = '';

                        if ($row->is_required == 1) {
                            $fil_value = $this->input->post('multi_' . $row->filter_slug);
                            // echo 'if: '.$row->filter_slug.' : '.($fil_value=='').'<br/>';
                            // $is_multi = 1;
                        } else {
                            $fil_value = $this->input->post($row->filter_slug);
                            if ($fil_value != '') {
                                $fil_value = (array)$fil_value;
                            } else {
                                $fil_value = array();
                            }
                            // echo 'else: '.$row->filter_slug.' : '.($fil_value=='').'<br/>';
                            // $is_multi = 0;
                        }

                        if (count($fil_value) > 0) {
                            if (!is_array($fil_value)) {
                                $fil_value = (array)$fil_value;
                            }
                            foreach ($fil_value as $filVal) {
                                $filtersData['filter_value'] = $filVal;
                                $filterId = $this->common_model->commonSave('tbl_product_filters', $filtersData);
                                // If this filter is conditional get the detail value and save it.
                                $filter_dtil = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_id', $filVal)->row();
                                if ($row->cat_filter_is_conditional == 1) {
                                    $filtersDetailData['prod_filter_id'] = $filterId;
                                    $filtersDetailData['prod_id'] = $prod_id;
                                    $filtersDetailData['category_id'] = $sub_sub_cat_id;
                                    $filtersDetailData['filter_id'] = $row->filter_title_id;
                                    $filtersDetailData['filter_slug'] = $filter_dtil->filter_slug;
                                    $filtersDetailData['filter_is_unique'] = 0;

                                    if ($row->is_required == 1) {
                                        $fil_detail_val = $this->input->post('multi_' . $filter_dtil->filter_slug);
                                    } else {
                                        $fil_detail_val = $this->input->post($filter_dtil->filter_slug);
                                        if ($fil_detail_val != '') {
                                            $fil_detail_val = (array)$fil_value;
                                        } else {
                                            $fil_detail_val = array();
                                        }
                                    }

                                    if (count($fil_detail_val) > 0) {
                                        foreach ($fil_detail_val as $fil_del_val) {
                                            $filtersDetailData['filter_detail_id'] = $filVal;
                                            $filtersDetailData['filter_value'] = $fil_del_val;
                                            $filterDetailId = $this->common_model->commonSave('tbl_product_filters_detail', $filtersDetailData);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $this->session->set_flashdata('editProduct', '1');
            redirect(base_url('manage-products'));

            // echo '<pre>';
            // print_r($_POST);
            exit;

        }

    }

    public function upload_prod_image()
    {
        $path = $_SERVER['DOCUMENT_ROOT'] . '/front_resources/new_filer/php/class.uploader.php';
        include $path;

        $uploader = new Uploader();
        $data = $uploader->upload($_FILES['files'], array(
            'limit' => 10, //Maximum Limit of files. {null, Number}
            'maxSize' => 10, //Maximum Size of files {null, Number(in MB's)}
            'extensions' => array('jpeg', 'jpg', 'gif', 'png', 'JPEG', 'JPG', 'GIF', 'PNG'), //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
            'required' => false, //Minimum one file is required for upload {Boolean}
            'uploadDir' => $_SERVER['DOCUMENT_ROOT'] . '/resources/prod_images/', //Upload directory {String}
            'title' => array('auto'), //New file name {null, String, Array} *please read documentation in README.md
            'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
            'replace' => false, //Replace the file if it already exists  {Boolean}
            'perms' => null, //Uploaded file permisions {null, Number}
            'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
            'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
            'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
            'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
            'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
            'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
        ));



        if ($data['isComplete']) {
            $files = $data['data'];
            $this->load->library('image_lib');
            $configt['image_library'] = 'gd2';
            $configt['source_image'] = $files['metas'][0]['file'];
            $configt['new_image'] = $_SERVER['DOCUMENT_ROOT'] . '/resources/prod_images/thumb/';
            $configt['maintain_ratio'] = true;
            $configt['width'] = 80;
            $configt['height'] = 60;
            $this->image_lib->initialize($configt);
            $this->image_lib->resize();

            $dbData['img_name'] = $files['metas'][0]['name'];
            //$dbData['img_unique_str'] = $this->input->post('unique_str');
            /*echo "<pre>";
            print_r($files['metas']);
            exit;*/
            //$this->db->insert('tbl_product_images', $dbData);
            //$insertId = $this->db->insert_id();
            //$files['metas'][0]['image_id'] = $insertId;
            echo json_encode($files['metas'][0]);            
        }

        if($data['hasErrors']){
            $errors = $data['errors'];
            echo json_encode($errors);
        }

        exit;
    }

    public function edit_upload_prod_image()
    {
        $path = $_SERVER['DOCUMENT_ROOT'] . '/front_resources/new_filer/php/class.uploader.php';
        include $path;

        $no_of_images = $_COOKIE['img_counter'];
        /*echo $no_of_images;
        exit*/;
        if($no_of_images>=9){
            echo '200';
        }else{
            $no_of_images = $no_of_images + 1;
            setcookie('img_counter',$no_of_images, 0, '/');
            $uploader = new Uploader();
            $data = $uploader->upload($_FILES['files'], array(
                'limit' => 10, //Maximum Limit of files. {null, Number}
                'maxSize' => 10, //Maximum Size of files {null, Number(in MB's)}
                'extensions' => array('jpeg', 'jpg', 'gif', 'png', 'JPEG', 'JPG', 'GIF', 'PNG'), //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
                'required' => false, //Minimum one file is required for upload {Boolean}
                'uploadDir' => $_SERVER['DOCUMENT_ROOT'] . '/resources/prod_images/', //Upload directory {String}
                'title' => array('auto'), //New file name {null, String, Array} *please read documentation in README.md
                'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
                'replace' => false, //Replace the file if it already exists  {Boolean}
                'perms' => null, //Uploaded file permisions {null, Number}
                'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
                'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
                'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
                'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
                'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
                'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
            ));



            if ($data['isComplete']) {
                $files = $data['data'];
                $this->load->library('image_lib');
                $configt['image_library'] = 'gd2';
                $configt['source_image'] = $files['metas'][0]['file'];
                $configt['new_image'] = $_SERVER['DOCUMENT_ROOT'] . '/resources/prod_images/thumb/';
                $configt['maintain_ratio'] = true;
                $configt['width'] = 80;
                $configt['height'] = 60;
                $this->image_lib->initialize($configt);
                $this->image_lib->resize();

                $dbData['img_name'] = $files['metas'][0]['name'];
                //$dbData['img_unique_str'] = $this->input->post('unique_str');
                /*echo "<pre>";
                print_r($files['metas']);
                exit;*/
                //$this->db->insert('tbl_product_images', $dbData);
                //$insertId = $this->db->insert_id();
                //$files['metas'][0]['image_id'] = $insertId;
                echo json_encode($files['metas'][0]);            
            }

            if($data['hasErrors']){
                $errors = $data['errors'];
                echo json_encode($errors);
            }
        }
        exit;
    }

    public function remove_images()
    {
        // $data = $this->common_model->commonselect('tbl_product_images', 'img_id', $img_unique_str)->row();
        // if(count($data) > 0){
        $no_of_images = $_COOKIE['img_counter'];
        if($no_of_images>0){
            $no_of_images = $no_of_images - 1;
            setcookie('img_counter',$no_of_images, 0, '/');
        }        
        $img_name = $_POST['file'];
        unlink(FCPATH . 'resources/prod_images/' . $img_name['name']);
        unlink(FCPATH . 'resources/prod_images/thumb/' . $img_name['name']);
        //}
        //$this->common_model->commonDelete('tbl_product_images','img_id', $img_unique_str);
        echo 1;
    }

    public function edit_remove_images()
    {
        $img_name = $_POST['file'];
        $no_of_images = $_COOKIE['img_counter'];
        $no_of_images = $no_of_images - 1;
        $data = $this->common_model->commonselect('tbl_product_images', 'img_name', $img_name)->row();
        if(count($data) > 0){
            setcookie('img_counter',$no_of_images, 0, '/');
            unlink(FCPATH . 'resources/prod_images/' . $img_name);
            unlink(FCPATH . 'resources/prod_images/thumb/' . $img_name);
            $this->common_model->commonDelete('tbl_product_images', 'img_name', $img_name);
        }        
        echo 1;
    }

    public function product_delete($slug)
    {

        $data['title'] = "Manage Products";
        $data['content_view'] = "products/manage-product";
        $array['prod_is_delete'] = 1;
        $nId = $this->product_model->product_delete($slug);
        $this->common_model->commonUpdate('tbl_products', $array, "prod_id", $nId);
        $this->common_model->commonDelete('tbl_wish_list', "product_id", $nId);
        $this->session->set_flashdata('msg', 'Item deleted successfully.');
        redirect(base_url() . "manage-products");
        exit;
    }

    public function search_product()
    {
        $data = array();
        $user_id = $this->session->userdata('userid');
        $data['user_id'] = $user_id;
        $data['title'] = "Manage Products";
        $data['content_view'] = "products/manage-product";
        $limit_per_page = 10;
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['total_records'] = count($this->product_model->search_product($user_id,$to=0, $from = '', $is_limit = 0));
        $config['base_url'] = base_url() . 'manage-products/index';
        $config['total_rows'] = $data['total_records'];
        $config['per_page'] = $limit_per_page;
        $config["uri_segment"] = 3;
        $config['use_page_numbers'] = TRUE;
        $config['cur_tag_open'] = '&nbsp;<li><a class="current">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['next_link'] = '';
        $config['prev_link'] = '';
        $data['to'] =  $page;
        $data['from'] = $page + $config['per_page'];
        $this->pagination->initialize($config);
        $start = (int)$this->uri->segment(3) * $config['per_page'] + 1;
        $end = ($this->uri->segment(3) == floor($config['total_rows'] / $config['per_page'])) ? $config['total_rows'] : (int)$this->uri->segment(3) * $config['per_page'] + $config['per_page'];
        $data['result_count'] = "" . $start . " - " . $end . " of " . $config['total_rows'] . " ";
        $data["links"] = $this->pagination->create_links();
        $data['results'] = $this->product_model->search_product($user_id,$limit_per_page, $page * $limit_per_page,1);
        //echo $this->db->last_query();die;
        $this->load->view("front/custom_template", $data);
    }

    public function bluk_product_delete()
    {

        $response = $this->product_model->product_bluk_delete();
        if ($response == 1) {
            echo json_encode(array('msg' => 'Selected Products deleted successfully.'));
            die;
        }
    }

    public function download_csv()
    {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=user_products_' . date("m-d-Y") . '.csv');
        $output = fopen('php://output', 'w');
        fputcsv($output, array('ID', 'product Title', 'Price', 'Shipping', 'Qty', 'Status'));
        $resultsProd = $this->product_model->product_download_csv();
        for ($n = 0; $n < count($resultsProd); $n++) {
            fputcsv($output, $resultsProd[$n]);
        }
    }

    public function bluk_product_copy()
    {
        $response = $this->product_model->product_bluk_copy();
        if ($response == 1) {
            echo json_encode(array('msg' => 'Selected Products Copied successfully.'));
            die;
        }
    }
    
    public function product_report(){
        $response = $this->product_model->product_report();
        if ($response == 1) {
            echo json_encode(array('msg' => 'Selected Products reported successfully.'));
            die;
        }
    }

}

?>