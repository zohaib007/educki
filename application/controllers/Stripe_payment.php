<?php

class Stripe_payment extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->data['url'] = base_url();
        if ( !empty($_GET) ) $_GET = array_stripslash($_GET);
        if ( !empty($_POST) ) $_POST = array_stripslash($_POST);
        date_default_timezone_set('America/New_York');
        //ini_set('display_errors', 0);
    }
    public function subscribe($id, $tier, $token) {
        //Get Stripe Customer from tbl_user

        $data = array(
            "email" => $this->input->post($this->session->all_userdata()['useremail']),
            "source" => $token
        );
        
        $customerId = $this->stripe->create_customer($data);
        
        $this->db->set('stripe_id', $customerId);
        $this->db->where('user_id', $this->session->userdata('userid'));
        $this->db->update('tbl_user');

        $this->db->select("plan_id");
        $this->db->from('tbl_tier_list');
        $this->db->where('tier_id', $tier);
        $query = $this->db->get();
        $plan = $query->result();
        $plan_id = $plan[0]->plan_id;

        $url = 'https://api.stripe.com/v1/subscriptions';
        $header = array(
            // 'Authorization: Bearer sk_live_0LCxhw41WzTGHHH4Uf6FebBR',
            'Authorization: Bearer sk_test_A6QSsQkwmXHH0ntrcKwz18PR',
        );
        $values = array(
            'customer' => $customerId,
            'items' => array(
                0 => array(
                    'plan' => $plan_id
                )
            )
        );
        $params = http_build_query($values);

        try{
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,$url);

            curl_setopt($ch, CURLOPT_HEADER, TRUE);
            curl_setopt($ch,CURLOPT_HTTPHEADER, $header);

            //curl_setopt($ch, CURLOPT_USERPWD, "$this->clientId:$this->secret");

            curl_setopt($ch,CURLOPT_POST,true);
            curl_setopt($ch,CURLOPT_POSTFIELDS,$params);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);
        
            curl_close($ch);

            $r = explode('strict-transport-security: max-age=31556926; includeSubDomains; preload',$result);
                        
            $re = trim($r[1]);
            $sub_id = json_decode($re)->id;
            
            echo $sub_id;

        }catch(Exception $e){
            return $e->getMessage();
        }
    
    }
    
    public function subscribeTonewTier($id, $tier, $token) {
        //Get Stripe Customer from tbl_user

        $data = array(
            "email" => $this->input->post($this->session->all_userdata()['useremail']),
            "source" => $token
        );
        
        $customerId = $this->stripe->create_customer($data);
        
        $this->db->set('stripe_id', $customerId);
        $this->db->where('user_id', $this->session->userdata('userid'));
        $this->db->update('tbl_user');

        $this->db->select("plan_id");
        $this->db->from('tbl_tier_list');
        $this->db->where('tier_id', $tier);
        $query = $this->db->get();
        $plan = $query->result();
        $plan_id = $plan[0]->plan_id;

        $url = 'https://api.stripe.com/v1/subscriptions';
        $header = array(
            // 'Authorization: Bearer sk_live_0LCxhw41WzTGHHH4Uf6FebBR',
           'Authorization: Bearer sk_test_A6QSsQkwmXHH0ntrcKwz18PR',
        );
        $values = array(
            'customer' => $customerId,
            'items' => array(
                0 => array(
                    'plan' => $plan_id
                )
            )
        );
        $params = http_build_query($values);

        try{
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,$url);

            curl_setopt($ch, CURLOPT_HEADER, TRUE);
            curl_setopt($ch,CURLOPT_HTTPHEADER, $header);

            //curl_setopt($ch, CURLOPT_USERPWD, "$this->clientId:$this->secret");

            curl_setopt($ch,CURLOPT_POST,true);
            curl_setopt($ch,CURLOPT_POSTFIELDS,$params);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);
        
            curl_close($ch);

            $r = explode('strict-transport-security: max-age=31556926; includeSubDomains; preload',$result);
                        
            $re = trim($r[1]);
            $sub_id = json_decode($re)->id;
            
            return $sub_id;

        }catch(Exception $e){
            return $e->getMessage();
        }
    
    }
    
    public function retrieveSubscription($sub)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.stripe.com/v1/subscriptions/".$sub,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // "Authorization: Bearer sk_live_0LCxhw41WzTGHHH4Uf6FebBR",
                "Authorization: Bearer sk_test_A6QSsQkwmXHH0ntrcKwz18PR",
                "Cache-Control: no-cache",
                "Postman-Token: 96bedb1a-f935-4af2-b8ce-cf61f57e7cb3"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }
    public function getUpcomingInvoice($customerid, $sub, $items, $date)
    {
        $curl = curl_init();
        $values = array(
            'customer' => $customerid,
            'subscription' => $sub,
            'subscription_items' => $items,
            'subscription_proration_date' => $date,
        );
        $uri = http_build_query($values);
        /*print_r($items);
        echo $uri;
        exit();*/
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.stripe.com/v1/invoices/upcoming?".$uri,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // "Authorization: Bearer sk_live_0LCxhw41WzTGHHH4Uf6FebBR",
                "Authorization: Bearer sk_test_A6QSsQkwmXHH0ntrcKwz18PR",
                "Cache-Control: no-cache",
                "Postman-Token: 4ad44b70-a076-4c4f-8e95-d1619ce67eb7"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response);
        }
    }


    public function updateSubscription($subscription, $items)
    {
        $curl = curl_init();
        $values = array(
            'items' => $items,
        );
        $params = http_build_query($values);
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.stripe.com/v1/subscriptions/".$subscription,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_HTTPHEADER => array(
                // "Authorization: Bearer sk_live_0LCxhw41WzTGHHH4Uf6FebBR",
                "Authorization: Bearer sk_test_A6QSsQkwmXHH0ntrcKwz18PR",
                "Cache-Control: no-cache",
                "Postman-Token: cc4ef2de-326f-4325-9adf-ec2c2bf3bd8b"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response);
        }
    }
    public function changeSubscription($id, $tier, $token) {
        
                //update
                $this->db->set('store_tier_id', $tier, FALSE);
                $this->db->where('user_id', $id);
                $this->db->update('tbl_stores');
        
                //Get Subscription from tbl_store
                $this->db->select("sub_id");
                $this->db->from('tbl_stores');
                $this->db->where('user_id', $id);
                $query = $this->db->get();
                $sub = $query->result();
            if(($sub[0]->sub_id == null) || ($sub[0]->sub_id == ''))
                {
                    $new_sub_id = $this->subscribeTonewTier($id, $tier, $token);
                   
            
                    $this->db->set('sub_id', $new_sub_id);
                    $this->db->where('user_id', $id);
                    $this->db->update('tbl_stores');
    
                    echo $new_sub_id;
                    
                }
                
            else if ($tier > 1)
            {
                
                // Get customer id from tbl_user
                $this->db->select("stripe_id");
                $this->db->from('tbl_user');
                $this->db->where('user_id', $id);
                $que = $this->db->get();
                $customer = $que->result();
        
                // Get plan id from tbl_tier_table
                $this->db->select("plan_id");
                $this->db->from('tbl_tier_list');
                $this->db->where('tier_id', $tier);
                $qu = $this->db->get();
                $plan = $qu->result();
    
                $proration_date = time();
                $subscriptio = $this->retrieveSubscription($sub[0]->sub_id);
        
                $subscription = json_decode($subscriptio);
        
                $items = [
                    [
                        'id' => $subscription->items->data[0]->id,
                        'plan' => $plan[0]->plan_id, # Switch to new plan
                    ],
                ];
        
                $invoice = $this->getUpcomingInvoice($customer[0]->stripe_id, $sub[0]->sub_id, $items, $proration_date);
                //$date = strtotime('2018/10/12');
                $cost = 0;
                $current_prorations = [];
                foreach ($invoice->lines->data as $line) {
                    if ($line->period->start == $proration_date) {
                        array_push($current_prorations, $line);
                        $cost += $line->amount;
                    }
                }
                $amount_to_be_paid = $invoice->amount_remaining/100;
    
            
                $new_subscription = $this->updateSubscription($subscription->id, $items);
                    //update Subscription_end_date in tbl_store
                    $this->db->set('sub_id', $new_subscription->id);
                    $this->db->where('user_id', $id);
                    $this->db->update('tbl_stores');
                    echo json_encode($new_subscription);
            }
            else
            {
                $this->db->set('sub_id', null);
                $this->db->where('user_id', $id);
                $this->db->update('tbl_stores');
                echo false;
            }
            
            
    }




    public function changeTierToFree($id) {

        $this->db->select("sub_id");
        $this->db->from('tbl_stores');
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        $res = $query->result();
        $sub_id = $res[0]->sub_id;
        //$current_next_month_date = date("Y-m-d", strtotime('+1 month'));

        $url = 'https://api.stripe.com/v1/subscriptions/'.$sub_id;
        $header = array(
            // 'Authorization: Bearer sk_live_0LCxhw41WzTGHHH4Uf6FebBR',
            "Authorization: Bearer sk_test_A6QSsQkwmXHH0ntrcKwz18PR",
        );
        $values = array(
            /*'customer' => $customerid,
            'items' => array(
                0 => array(
                    'plan' => $plan_id
                )
            )*/
        );
        $params = http_build_query($values);

        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            //
            curl_setopt($ch, CURLOPT_HEADER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            //
//            curl_setopt($ch, CURLOPT_USERPWD, "$this->clientId:$this->secret");
            //
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            //
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);
            curl_close($ch);
        }catch(Exception $e){
            return $e->getMessage();
        }
        $r = explode('Strict-Transport-Security: max-age=31556926; includeSubDomains; preload',$result);
        $re = trim($r[1]);
        $sub_status = json_decode($re)->status;
        /*echo '<pre>';
        print_r(json_decode($re)->status);
        exit();*/
        if ($sub_status != "active")
        {
            //update
            $this->db->set('store_tier_id', 1);
            $this->db->set('store_created_date', null);
            $this->db->set('store_end_date', null);
            $this->db->set('sub_id', null);
            $this->db->where('user_id', $id);
            $this->db->update('tbl_stores');
        }
    }

    public function changeTierToFreeByStoreEndDate() {
        // Get Store End Dates
        $this->db->select('store_id, store_end_date, sub_id');
        $this->db->from('tbl_stores');

        $query = $this->db->get();
        $data = $query->result();

        foreach ($data as $datum) {
            $store_end_date = date('Y-m-d', strtotime($datum->store_end_date));
            $today_date = date('Y-m-d');
            if ($store_end_date == $today_date)
            {
                if (isset($datum->sub_id)){
                    $this->changeTierToFree($datum->sub_id);
                    //echo "Store ". $datum->store_id . " is shifted on Tier 1 on date ". $datum->store_end_date . " with Subscription id ". $datum->sub_id . '<br>';
                }
                else{
                    //update
                    $this->db->set('store_tier_id', 1);
                    $this->db->set('store_created_date', null);
                    $this->db->set('store_end_date', null);
                    $this->db->where('store_id', $datum->store_id);
                    $this->db->update('tbl_stores');
                    //echo "Store ". $datum->store_id . " is shifted on Tier 1 on date ". $datum->store_end_date . '<br>';
                }
            }

        }
        //echo "There is no store with end date matching with today " . "<br>";
    }
    public function cancelSubscription($sub_id) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.stripe.com/v1/subscriptions/".$sub_id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_HTTPHEADER => array(
                // 'Authorization: Bearer sk_live_0LCxhw41WzTGHHH4Uf6FebBR',
                "Authorization: Bearer sk_test_A6QSsQkwmXHH0ntrcKwz18PR",
                "Cache-Control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }

    }
    //This function not in used
    public function changeSubscriptionnnn($id, $tier) {

        //update
        $this->db->set('store_tier_id', $tier, FALSE);
        $this->db->where('user_id', $id);
        $this->db->update('tbl_stores');

        //Get Subscription from tbl_store
        $this->db->select("sub_id");
        $this->db->from('tbl_stores');
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        $sub = $query->result();
        /*echo '<pre>';
        print_r($sub);
        exit();*/

        //Cancel Previous Subscription
        $this->cancelSubscription($sub[0]->sub_id);

        if ($tier > 1)
        {
            //Get Stripe Customer from tbl_user
            $this->db->select("stripe_id");
            $this->db->from('tbl_user');
            $this->db->where('user_id', $id);
            $query = $this->db->get();
            $res = $query->result();
            $customerid = $res[0]->stripe_id;

            //get plan_id from tbl_tier_list
            $this->db->select("plan_id");
            $this->db->from('tbl_tier_list');
            $this->db->where('tier_id', $tier);
            $query = $this->db->get();

            $plan = $query->result();
            $plan_id = $plan[0]->plan_id;

            // $customer = $this->stripe->get_customer($customerid);
            $url = 'https://api.stripe.com/v1/subscriptions';
            $header = array(
                // 'Authorization: Bearer sk_live_0LCxhw41WzTGHHH4Uf6FebBR',
                "Authorization: Bearer sk_test_A6QSsQkwmXHH0ntrcKwz18PR",
            );
            $values = array(
                'customer' => $customerid,
                'items' => array(
                    0 => array(
                        'plan' => $plan_id
                    )
                )
            );
            $params = http_build_query($values);

            try{
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL,$url);
                //
                curl_setopt($ch, CURLOPT_HEADER, TRUE);
                curl_setopt($ch,CURLOPT_HTTPHEADER, $header);
                //
//            curl_setopt($ch, CURLOPT_USERPWD, "$this->clientId:$this->secret");
                //
                curl_setopt($ch,CURLOPT_POST,true);
                curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
                //
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $result = curl_exec($ch);
                curl_close($ch);

                $r = explode('Strict-Transport-Security: max-age=31556926; includeSubDomains; preload',$result);
                $re = trim($r[1]);
                $sub_id = json_decode($re)->id;
                //$sub_end_date_at_stripe = json_decode($re)->current_period_end;
                //$sub_end_date = date("Y-m-d", $sub_end_date_at_stripe);

                //update Subscription_end_date in tbl_store
                $this->db->set('sub_id', $sub_id);
                $this->db->where('user_id', $id);
                $this->db->update('tbl_stores');

                //echo json_encode($re);

            }catch(Exception $e){
                return $e->getMessage();
            }
        }
    }
}