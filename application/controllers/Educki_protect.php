<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Educki_protect extends CI_Controller
{

    public function index()
    {
        $data['pagedata']         = $this->common_model->commonSelect('tbl_pages_sub', 'sub_id', 14)->row();
        $data['pageurl']          = $this->common_model->commonSelect('tbl_pages_sub', 'sub_id', 5)->row();
        $data['title']            = $data['pagedata']->sub_pg_title;
        $data['meta_title']       = $data['pagedata']->meta_title;
        $data['meta_description'] = $data['pagedata']->meta_description;
        $data['meta_keyword']     = $data['pagedata']->meta_keywords;
        $data['content_view']     = "about/about-protect";
        $this->load->view("front/custom_template", $data);
    }

}
