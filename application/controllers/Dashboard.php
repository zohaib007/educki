<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('front/dashboard/dashboard_model');
        $this->load->model('front/order/order_model');
        authFront();
    }

    public function index() {
        $data['title'] = "My Dashboard";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";
        $data['content_view'] = "dashboard/dashboard";
        $data['userid'] = $this->session->userdata('userid');

        $data['user'] = $this->db->query("
                                    SELECT *
                                    FROM tbl_user
                                    WHERE
                                    user_id = " . $data['userid'] . "
                                    AND
                                    user_status = 1")->row();

        /*$data['orders'] = $this->db->query("
                            SELECT *
                            FROM tbl_orders
                            WHERE buyer_id = " . $data['userid'] . " ORDER BY order_id DESC LIMIT 0,3")->result();*/

        $data['orders'] = $this->order_model->get_my_orders($data['userid'], 3, 0);
        
        /*echo "<pre>";
        print_r($data);
        exit;*/

        $data['default_shipping'] = $this->common_model->default_shipping($data['userid']);
        $data['default_billing'] = $this->common_model->default_billing($data['userid']);
        $this->load->view("front/custom_template", $data);
    }

    public function seller_dashboard() {
        $order_status = 'Awaiting Tracking';
        $data['userid'] = $this->session->userdata('userid');
        $data['store_info'] = $this->common_model->commonselect('tbl_stores','user_id',$data['userid'])->row();
        $data['counts'] = $this->dashboard_model->product_views($data['userid']);
        $data['most_view_product'] = $this->dashboard_model->most_view_product($data['store_info']->store_id);
        
        if ( !$data['most_view_product'] ) {
            $data['most_view_product'] = [];
        }        
        
        $data['todays_order'] = $this->dashboard_model->orders($data['store_info']->store_id);
        $data['last_orders'] = $this->dashboard_model->get_user_orders($data['store_info']->store_id,$order_status, 5, 0);

        $data['total_sales'] = getTotalSalesByStore($data['store_info']->store_id);
        if(empty($data['total_sales'])){
            $data['total_sales'] = 0.00;
        }

        $data['store_profit'] = $this->dashboard_model->get_store_profit($data['store_info']->store_id);
        $data['orders_stats'] = $this->dashboard_model->get_order_stats($data['store_info']->store_id);
        $data['title'] = "Seller Dashboard";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";
        $data['content_view'] = "dashboard/seller_dashboard";
        $data['userid'] = $this->session->userdata('userid');
        $data['month_sales'] = $this->dashboard_model->get_sales_per_month($data['store_info']->store_id);
        $data['months'] = [0,0,0,0,0,0,0,0,0,0,0,0];
        for($i = 0; $i <= 11; $i++) {
            if(($data['month_sales'][$i]->sale_month-1)>0){
                $data['months'][$data['month_sales'][$i]->sale_month-1] = number_format($data['month_sales'][$i]->today_total_sale, 2, '.', '');
            }            
        }

        $this->load->view("front/custom_template", $data);
    }

}
