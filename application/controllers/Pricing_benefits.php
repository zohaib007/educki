<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pricing_benefits extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('common/common_model');
        $this->load->helper(array('form', 'url'));
    }

    public function index()
    {        
        $this->session->set_userdata('pricing_benefits_clicked',0);
        $this->session->unset_userdata('tier_id');

        $data['pagedata']         = $this->common_model->commonselect('tbl_pricing_benefits', 'p_id', 1)->row();
        $data['title']            = $data['pagedata']->page_title;
        $data['meta_title']       = $data['pagedata']->meta_title;
        $data['meta_description'] = $data['pagedata']->meta_description;
        $data['meta_keyword']     = $data['pagedata']->meta_keywords;
        $data['content_view']     = "pricing-and-benefits/pricing-and-benefits";
        $data['tierdata']         = $this->common_model->getCombox('tbl_tier_list')->result();
        $data['base_value']       = $data['tierdata'][1]->tier_amount;

        /*echo "<pre>";
        print_r($this->session->userdata());
        exit;*/

        if (count($data) <= 0) {
            redirect(base_url());
        } else {
            $this->load->view("front/custom_template", $data);
        }
    }

    public function set_pandb_session()
    {
        if ($this->input->is_ajax_request()) {
            $tier = $this->input->post('tier');
            $sessiondata = array(
                'tier_id' => $tier,
                'pricing_benefits_clicked' => 1
            );
            $this->session->set_userdata($sessiondata);
            $return['status'] = 200;
            $return['status'] = "Successfully updated";
        }

        echo json_encode($return);
    }

    public function unset_pandb_session()
    {
        if ($this->input->is_ajax_request()) {
            $this->session->set_userdata('pricing_benefits_clicked',0);
            $this->session->unset_userdata('tier_id');
            $return['status'] = 200;
            $return['status'] = "Successfully updated";
        }

        echo json_encode($return);
    }

}
