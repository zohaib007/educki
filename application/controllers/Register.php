<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Register extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }
    
     public function index()
    {}

    public function is_unique_register_email(){
    	$email = $this->input->post('email');

    	$regData = $this->db->query("
    						SELECT *
    						FROM tbl_user
    						WHERE user_email = '".$email."' AND user_is_delete = 0
    					")->row();
    	if(count($regData) > 0){
    		echo 1;
            exit;
    	}else{
    		echo 'sadasd';
            exit;
    	}
    }

    public function register_user (){

        $array['user_email'] = $this->input->post('email');
        $array['user_password'] = $this->input->post('password');
        $array['user_password_encryp'] = md5($this->input->post('password'));
        $array['user_fname'] = $this->input->post('fname');
        $array['user_lname'] = $this->input->post('lname');
        $array['user_zip'] = $this->input->post('zipcode');
        $array['user_age'] = $this->input->post('age');
        $array['user_interests'] = $this->input->post('interest1');
        $array['user_other_interest'] = $this->input->post('otherInst1');
        $array['user_interests_second'] = $this->input->post('interest2');
        $array['user_other_interest_second'] = $this->input->post('otherInst2');
        $array['user_business_owner'] = $this->input->post('small_bus');
        $array['user_sex'] = $this->input->post('gender');
        $array['user_status'] = 1;

        $array['user_register_date'] = date('Y-m-d H:i:s');
        add_action('init', 'add_user');
        $username = $this->input->post('email');
        $password = $this->input->post('password');
        $email = $this->input->post('email');
        $user_id = wp_create_user( $username, $password, $email );
        $user_id = $this->common_model->commonSave('tbl_user',$array);
        
        if ( $user_id ) {
            
            //email to admin
            $message = "A new user has regitered. <br><br>";
            
            $tmp = $array;
            
            unset($tmp['user_password']);
            unset($tmp['user_password_encryp']);
            
            $tmp['user_name'] = $tmp['user_fname']." ".$tmp['user_lname'];
            
            unset($tmp['user_fname']);
            unset($tmp['user_lname']);
            unset($tmp['user_status']);
            
            foreach ( $tmp as $k => $ar ) {
                
                $ar = trim($ar);
                
                if ( !empty($ar) ) {
                    
                    if ( $k == "user_business_owner" ) {
                        if ( $ar == 1 ) {
                            $ar = "Yes";
                        } else {
                            $ar = "No";
                        }
                    }
                    
                    if ( $k == "user_register_date" ) {
                        $ar = date("d-m-Y h:i a", strtotime($ar));
                    }
                    
                    if ( $k == "user_other_interest" ) {
                        $k = "User Interests 1";
                    } else if ( $k == "user_interests_second" ) {
                        $k = "User Interests 2";
                    } else if ( $k == "user_other_interest_second" ) {
                        $k = "User Interests 3";
                    } else if ( $k == "user_sex" ) {
                        $k = "Gender";
                    } else {
                        $k = ucwords(str_replace("_", " ", $k));
                    }

                    $message .= "<strong>$k: </strong>$ar<br>";
                    
                }
                
            }
            
            send_email(getSiteSettings('site_email'), "noreply@educki.com", "New User Registered", $message);
            //email to admin
            
            //email to user

            $email_data           = $this->common_model->commonselect('tbl_email_temp', 'e_email_slug', 'welcome')->row();
            $emailData['subject'] = $email_data->e_email_subject;
            $message              = $email_data->e_email_text;
            $message              = str_replace("{{username}}", $tmp['user_name'], $message);
            //$message              = str_replace("{{clickHere}}", $click, $message);

            send_email($array['user_email'], "noreply@educki.com", $emailData['subject'], $message);
            
            //email to user
            
        }
        if($this->session->userdata('pricing_benefits_clicked')==1){
            echo 0;
            exit;
        }else{
            echo 1;
            exit;
        }
    }


}

?>