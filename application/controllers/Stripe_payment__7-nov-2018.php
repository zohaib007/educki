<?php

class Stripe_payment extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->data['url'] = base_url();
        if ( !empty($_GET) ) $_GET = array_stripslash($_GET);
        if ( !empty($_POST) ) $_POST = array_stripslash($_POST);
        date_default_timezone_set('America/New_York');
        //ini_set('display_errors', 0);
    }
    public function subscribe($id, $tier) {
        //Get Stripe Customer from tbl_user
        $this->db->select("stripe_id");
        $this->db->from('tbl_user');
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        $res = $query->result();
        $customerid = $res[0]->stripe_id;
        //get plan_id from tbl_tier_list
        $this->db->select("plan_id");
        $this->db->from('tbl_tier_list');
        $this->db->where('tier_id', $tier);
        $query = $this->db->get();
        $plan = $query->result();
        $plan_id = $plan[0]->plan_id;
       // $customer = $this->stripe->get_customer($customerid);
        $url = 'https://api.stripe.com/v1/subscriptions';
        $header = array(
            'Authorization: Bearer sk_live_0LCxhw41WzTGHHH4Uf6FebBR',
        );
        $values = array(
            'customer' => $customerid,
            'items' => array(
                0 => array(
                    'plan' => $plan_id
                )
            )
        );
        $params = http_build_query($values);

        try{
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL,$url);
            //
            curl_setopt($ch, CURLOPT_HEADER, TRUE);
            curl_setopt($ch,CURLOPT_HTTPHEADER, $header);
            //
//            curl_setopt($ch, CURLOPT_USERPWD, "$this->clientId:$this->secret");
            //
            curl_setopt($ch,CURLOPT_POST,true);
            curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
            //
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);
            curl_close($ch);
            echo '<pre>';
            print_r($result);
            exit();

            $r = explode('Strict-Transport-Security: max-age=31556926; includeSubDomains; preload',$result);
            $re = trim($r[1]);
            //echo json_encode($re);

        }catch(Exception $e){
            return $e->getMessage();
        }
        //$r = explode('Strict-Transport-Security: max-age=31556926; includeSubDomains; preload',$result);

        $sub_id = json_decode($re)->id;
        $this->db->set('sub_id', $sub_id);
        $this->db->where('user_id', $id);
        $this->db->update('tbl_stores');
        echo $sub_id;
    }

    public function changeSubscription($id, $tier) {

        //update
        $this->db->set('store_tier_id', $tier, FALSE);
        $this->db->where('user_id', $id);
        $this->db->update('tbl_stores');

        //Get Subscription from tbl_store
        $this->db->select("sub_id");
        $this->db->from('tbl_stores');
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        $sub = $query->result();
        /*echo '<pre>';
        print_r($sub);
        exit();*/

        //Cancel Previous Subscription
        $this->cancelSubscription($sub[0]->sub_id);

        if ($tier > 1)
        {
            //Get Stripe Customer from tbl_user
            $this->db->select("stripe_id");
            $this->db->from('tbl_user');
            $this->db->where('user_id', $id);
            $query = $this->db->get();
            $res = $query->result();
            $customerid = $res[0]->stripe_id;

            //get plan_id from tbl_tier_list
            $this->db->select("plan_id");
            $this->db->from('tbl_tier_list');
            $this->db->where('tier_id', $tier);
            $query = $this->db->get();

            $plan = $query->result();
            $plan_id = $plan[0]->plan_id;

            // $customer = $this->stripe->get_customer($customerid);
            $url = 'https://api.stripe.com/v1/subscriptions';
            $header = array(
                'Authorization: Bearer sk_live_0LCxhw41WzTGHHH4Uf6FebBR',
            );
            $values = array(
                'customer' => $customerid,
                'items' => array(
                    0 => array(
                        'plan' => $plan_id
                    )
                )
            );
            $params = http_build_query($values);

            try{
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL,$url);
                //
                curl_setopt($ch, CURLOPT_HEADER, TRUE);
                curl_setopt($ch,CURLOPT_HTTPHEADER, $header);
                //
//            curl_setopt($ch, CURLOPT_USERPWD, "$this->clientId:$this->secret");
                //
                curl_setopt($ch,CURLOPT_POST,true);
                curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
                //
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $result = curl_exec($ch);
                curl_close($ch);

                $r = explode('Strict-Transport-Security: max-age=31556926; includeSubDomains; preload',$result);
                $re = trim($r[1]);
                $sub_id = json_decode($re)->id;
                //$sub_end_date_at_stripe = json_decode($re)->current_period_end;
                //$sub_end_date = date("Y-m-d", $sub_end_date_at_stripe);

                //update Subscription_end_date in tbl_store
                $this->db->set('sub_id', $sub_id);
                $this->db->where('user_id', $id);
                $this->db->update('tbl_stores');

                //echo json_encode($re);

            }catch(Exception $e){
                return $e->getMessage();
            }
        }
    }

    public function changeTierToFree($id) {

        $this->db->select("sub_id");
        $this->db->from('tbl_stores');
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        $res = $query->result();
        $sub_id = $res[0]->sub_id;
        //$current_next_month_date = date("Y-m-d", strtotime('+1 month'));

        $url = 'https://api.stripe.com/v1/subscriptions/'.$sub_id;
        $header = array(
            'Authorization: Bearer sk_live_0LCxhw41WzTGHHH4Uf6FebBR',
        );
        $values = array(
            /*'customer' => $customerid,
            'items' => array(
                0 => array(
                    'plan' => $plan_id
                )
            )*/
        );
        $params = http_build_query($values);

        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            //
            curl_setopt($ch, CURLOPT_HEADER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            //
//            curl_setopt($ch, CURLOPT_USERPWD, "$this->clientId:$this->secret");
            //
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            //
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);
            curl_close($ch);
        }catch(Exception $e){
            return $e->getMessage();
        }
        $r = explode('Strict-Transport-Security: max-age=31556926; includeSubDomains; preload',$result);
        $re = trim($r[1]);
        $sub_status = json_decode($re)->status;
        /*echo '<pre>';
        print_r(json_decode($re)->status);
        exit();*/
        if ($sub_status != "active")
        {
            //update
            $this->db->set('store_tier_id', 1);
            $this->db->set('store_created_date', null);
            $this->db->set('store_end_date', null);
            $this->db->where('user_id', $id);
            $this->db->update('tbl_stores');
        }
    }

    public function changeTierToFreeByStoreEndDate() {
        // Get Store End Dates
        $this->db->select('store_id, store_end_date, sub_id');
        $this->db->from('tbl_stores');

        $query = $this->db->get();
        $data = $query->result();

        foreach ($data as $datum) {
            $store_end_date = date('Y-m-d', strtotime($datum->store_end_date));
            $today_date = date('Y-m-d');
            if ($store_end_date == $today_date)
            {
                if (isset($datum->sub_id)){
                    $this->changeTierToFree($datum->sub_id);
                    //echo "Store ". $datum->store_id . " is shifted on Tier 1 on date ". $datum->store_end_date . " with Subscription id ". $datum->sub_id . '<br>';
                }
                else{
                    //update
                    $this->db->set('store_tier_id', 1);
                    $this->db->set('store_created_date', null);
                    $this->db->set('store_end_date', null);
                    $this->db->where('store_id', $datum->store_id);
                    $this->db->update('tbl_stores');
                    //echo "Store ". $datum->store_id . " is shifted on Tier 1 on date ". $datum->store_end_date . '<br>';
                }
            }

        }
        //echo "There is no store with end date matching with today " . "<br>";
    }
    public function cancelSubscription($sub_id) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.stripe.com/v1/subscriptions/".$sub_id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer sk_live_0LCxhw41WzTGHHH4Uf6FebBR",
                "Cache-Control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }

    }


}