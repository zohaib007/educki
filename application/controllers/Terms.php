<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Terms extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('common/common_model');
        $this->load->helper(array('form', 'url'));
        $this->load->model('front/seller/seller_model');
        //auth();
    }

    public function index()
    {
        $data['pagedata'] = $this->common_model->commonselect('tbl_pages', 'pg_id', 9)->row();
        $data['title']            = $data['pagedata']->pg_title;
        $data['meta_title']       = $data['pagedata']->meta_title;
        $data['meta_description'] = $data['pagedata']->meta_description;
        $data['meta_keyword']     = $data['pagedata']->meta_keywords;
        $data['content_view']     = "terms-of-service";
        if (count($data) <= 0) {
            redirect(base_url());
        } else {
            $this->load->view("front/custom_template", $data);
        }
    }

    public function favourite_shops($store_url = '', $page = 0) {

        if ($store_url == '') {
            redirect(base_url());
        }

        $data['store_data'] = $this->common_model->commonSelect('tbl_stores', 'store_url', $store_url)->row();

        if(empty($data['store_data'])){
            redirect(base_url());
        }

        $data['title'] = "Favorite Shops";
        $data['meta_title'] = "meta_title";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";

        $store_user_id = get_store_user_id($this->uri->segment(2))->user_id;
        $data['logged_user'] = $store_user_id;

        if ($page == 0) {
            $page = 0;
        }
        $data['active'] = "wish_list";
        $sort = 'DESC';
        $data['per_page'] = 10;

        if (@$this->input->get('per_page') != '') {
            $data['per_page'] = @$this->input->get('per_page');
        }

        $data['user_data'] = $this->common_model->commonSelect('tbl_stores', 'user_id', $store_user_id)->row();
        //echo $store_user_id;
        $data['total_rows'] = count($this->seller_model->get_favourite_shops($store_user_id)->result_array());

        $mypaing['per_page'] = $data['per_page'];
        $mypaing['total_rows'] = $data['total_rows'];
        $mypaing['base_url'] = base_url() . "favorite-shop/".$store_url;
        $mypaing['uri_segment'] = 3;
        $mypaing['next_link'] = '&nbsp;';
        $mypaing['prev_link'] = '&nbsp;';
        $mypaing['prev_tag_open'] = '<li class="page-left" >';
        $mypaing['prev_tag_close'] = '</li>';
        $mypaing['next_tag_open'] = '<li class="page-right">';
        $mypaing['next_tag_close'] = '</li>';
        $mypaing['num_tag_open'] = '<li>';
        $mypaing['num_tag_close'] = '</li>';
        $mypaing['cur_tag_open'] = '<li><a class = "active">';
        $mypaing['cur_tag_close'] = '</a></li>';
        $mypaing['num_links'] = 5;
        $mypaing['first_tag_open'] = '<div style="display:none;">';
        $mypaing['first_tag_close'] = '</div>';
        $mypaing['last_tag_open'] = '<div style="display:none;">';
        $mypaing['last_tag_close'] = '</div>';

        $data['stores'] = $this->seller_model->get_favourite_shops($store_user_id, $sort, $page, $mypaing['per_page'], 1)->result_array();

        $data['to'] = $page;
        $data['from'] = $page + $mypaing['per_page'];

        $this->pagination->initialize($mypaing);
        $data['paginglink'] = $this->pagination->create_links();

        $data['content_view'] = "seller/favourite-stores";
        $this->load->view("front/custom_template", $data);
    }
}
