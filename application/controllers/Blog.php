<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Blog extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        /*echo "<pre>";
        send_email($to = 'mikesmith1166@gmail.com', $from = 'noreply@educki.com', $subject = 'Test', $message = 'Test');
        print_r($this->email->print_debugger());
        exit;*/
        $this->load->model('front/blog/blog_model');
        $this->load->library('pagination'); // load Pagination library
        $this->load->helper('url'); // load URL helper
    }

    public function index()
    {
        $data                       = array();
        $limit_per_page             = 10;
        $page                       = ($this->uri->segment(2)) ? ($this->uri->segment(2) - 1) : 0;
        $data['title']              = "News & Advice";
        $data['meta_title']         = "eDucki";
        $data['meta_description']   = "eDucki";
        $data['meta_keyword']       = "eDucki";
        $data['content_view']       = "blog/blog_list";
        $count_where                = '';
        $data['total_records']      = count($this->blog_model->get_blog_count());
        $config['base_url']         = base_url() . 'blog/';
        $config['total_rows']       = $data['total_records'];
        $config["uri_segment"]      = 2;
        $config['per_page']         = $limit_per_page;
        $config['use_page_numbers'] = true;
        $config['cur_tag_open']     = '&nbsp;<li><a class="active">';
        $config['cur_tag_close']    = '</a></li>';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['prev_tag_open']    = '<li>';
        $config['prev_tag_close']   = '</li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        $config['next_link']        = '';
        $config['prev_link']        = '';
        $config['first_link']       = false;
        $config['last_link']        = false;
        $data['is_cat']             = 0;

        $this->pagination->initialize($config);
        $data["links"]         = $this->pagination->create_links();
        $data['results']       = $this->blog_model->get_blog_data($limit_per_page, $page * $limit_per_page);
        $data['blog_category'] = $this->common_model->commonselect('tbl_blog_category', 'blog_cat_status', 1)->result();
        /*echo "<pre>";
        print_r($data);
        exit;*/
        $this->load->view("front/custom_template", $data);
    }

    public function blog_new()
    {
        $data['title']            = "Blog";
        $data['meta_title']       = "eDucki";
        $data['meta_description'] = "eDucki";
        $data['meta_keyword']     = "eDucki";
        $data['content_view']     = "blog/blog_list_1";
        $this->load->view("front/custom_template", $data);
    }
    public function blog_detail()
    {
        $data['title']            = "Blog";
        $data['meta_title']       = "eDucki";
        $data['meta_description'] = "eDucki";
        $data['meta_keyword']     = "eDucki";
        $data['content_view']     = "blog/blog_details_1";
        $this->load->view("front/custom_template", $data);
    }

    public function blog_details($blog_id = '')
    {
        $data['title']     = 'News & Advice';
        $data['blog_data'] = $this->blog_model->view_details($blog_id);
        foreach ($data['blog_data'] as $list) {
            $data['meta_title']        = htmlentities($list['blog_name']);
            $data['meta_description']  = htmlentities($list['blog_description']);
            $data['meta_keyword']      = htmlentities($list['blog_name']);
            $data['blog_id']           = $list['blog_id'];
            $data['blog_name']         = $list['blog_name'];
            $data['blog_author']       = $list['blog_author'];
            $data['blog_url']          = $list['blog_url'];
            $data['blog_image']        = $list['blog_image'];
            $data['blog_description']  = $list['blog_description'];
            $data['blog_status']       = $list['blog_status'];
            $data['blog_date']         = $list['blog_date'];
            $data['blog_created_date'] = $list['blog_created_date'];
            $data['blog_admin_id']     = $list['blog_admin_id'];
            $data['blog_category_id']  = $list['blog_category_id'];
            $data['share_title']       = $list['blog_name'];
            $data['share_image']       = base_url() . "resources/blog_image/" . $list['blog_image'];

        }

        $data['blog_cat'] = $this->db->query("select cat.* from tbl_blog_category cat inner join tbl_blog blog on cat.blog_cat_id = blog.blog_category_id where blog.blog_url = '".$blog_id."'")->row();

        $data['blog_category'] = $this->common_model->commonselect('tbl_blog_category', 'blog_cat_status', 1)->result();

        $data['content_view'] = "blog/blog_details";
        $this->load->view("front/custom_template", $data);
    }

    public function category_blog($blog_name)
    {
        $data                       = array();
        $limit_per_page             = 10;
        $page                       = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) : 0;
        $data['title']              = 'Blog Details';
        $data['meta_title']         = "eDucki";
        $data['meta_description']   = "eDucki";
        $data['meta_keyword']       = "eDucki";
        $data['content_view']       = "blog/blog_list";
        $data['total_records']      = count($this->blog_model->view_category_blog_details($blog_name));
        $config['base_url']         = base_url() . 'blog/' . $blog_name . '/';
        $config['total_rows']       = $data['total_records'];
        $config["uri_segment"]      = 3;
        $config['per_page']         = $limit_per_page;
        $config['use_page_numbers'] = true;
        $config['cur_tag_open']     = '&nbsp;<li><a class="active">';
        $config['cur_tag_close']    = '</a></li>';
        $config['next_tag_open']    = '<li>';
        $config['next_tag_close']   = '</li>';
        $config['prev_tag_open']    = '<li>';
        $config['prev_tag_close']   = '</li>';
        $config['num_tag_open']     = '<li>';
        $config['num_tag_close']    = '</li>';
        $config['next_link']        = '';
        $config['prev_link']        = '';
        $config['first_link']       = false;
        $config['last_link']        = false;
        $data['is_cat']             = 1;

        $data['blog_cat'] = $this->db->query("select blog_cat_name from tbl_blog_category where blog_cat_url = '".$blog_name."'")->row();

        $this->pagination->initialize($config);
        $data["links"]         = $this->pagination->create_links();
        $data['results']       = $this->blog_model->get_category_blog_data($blog_name, $limit_per_page, $page * $limit_per_page);
        $data['blog_category'] = $this->common_model->commonselect('tbl_blog_category', 'blog_cat_status', 1)->result();
        $this->load->view("front/custom_template", $data);
    }
}
