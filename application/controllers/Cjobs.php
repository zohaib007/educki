<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cjobs extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->database();
        date_default_timezone_set('America/New_York');
    }

   
    public function removeOnSale(){
        $prod = $this->db->select('*')->from('tbl_products')->where('prod_is_delete', 0)->where('prod_onsale', 1)->get();
        $prod = $prod->result();
        foreach($prod as $p){
            $sDate = date('Y-m-d', strtotime($p->sale_start_date));
            $eDate = date('Y-m-d', strtotime($p->sale_end_date));
            $tDate = date('Y-m-d');
            if ($p->sale_end_date != '') {
                if( $tDate > $eDate){
                    $udata['prod_onsale'] = 0;
                    $udata['sale_price'] = NULL;
                    $this->common_model->commonUpdate('tbl_products', $udata, 'prod_id', $p->prod_id );
                }
            }
        }
        echo count($prod).' Sale Products Updated.';
    }
}
