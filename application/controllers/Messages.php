<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Messages extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Message_model');
        $this->load->library('email');
        $config = array();
        /*
        $config['useragent']           = "CodeIgniter";
        $config['mailpath']            = "/usr/sbin/sendmail"; // or "/usr/sbin/sendmail"
        $config['protocol']            = "smtp";
        $config['smtp_host']           = $_SERVER['HTTP_HOST'];//"192.185.173.5";
        $config['smtp_port']           = "25";
        $config['priority'] = 1;
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->set_mailtype('html');
         */
        $config['protocol']    = 'smtp';
        $config['smtp_host']   = 'boomslangit.com';
        $config['smtp_port']   = "465";
        $config['_smtp_auth']  = true;
        $config['smtp_user']   = 'no-reply@boomslangit.com';
        $config['smtp_pass']   = '(%@Qqync(*RN';
        $config['smtp_crypto'] = 'ssl';
        $config['mailtype']    = 'html';
        $config['charset']     = 'utf-8';
        $config['validate']    = true;
        $config['priority']    = 1;
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->set_mailtype('html');
        authFront();
    }

    public function index($conversation_id = 0)
    {
        //sellAuth();
        $data['title'] = "Message Center";
        if (!is_numeric($conversation_id)) {            
            $conversation            = $this->Message_model->get_conversation_attr($conversation_id);
            if($conversation){
                $data['prod_id'] = $conversation['product_id'];
                $data['conversation_id'] = $conversation['conversation_id'];
                $data['conversation']    = $this->Message_model->fetchMessages($this->session->userdata('userid'), $conversation_id);
                $data['content_view']    = "messages/view-messages";
            }else{
                redirect(base_url('messages'));
            }
            /*echo "<pre>";
            print_r($data);
            exit;*/
            $this->load->view("front/custom_template", $data);
        } else {
            $Allconversation = $this->Message_model->fetchAllConversations($this->session->userdata('userid'));

            $mypaing['total_rows']  = count($Allconversation);
            $mypaing['base_url']    = base_url() . "messages";
            $mypaing['per_page']    = 10;
            $mypaing['uri_segment'] = 2;

            $mypaing['full_tag_open']  = '<div class="paginationInner2">';
            $mypaing['full_tag_close'] = '</div>';
            $mypaing['cur_tag_open']   = '<a class="active">';
            $mypaing['cur_tag_close']  = '</a>';
            $mypaing['next_tag_open']  = '<div class="page-nextBtn">';
            $mypaing['next_tag_close'] = '</div>';
            $mypaing['next_link']      = '';
            $mypaing['prev_tag_open']  = '<div class="page-preBtn">';
            $mypaing['prev_tag_close'] = '</div>';
            $mypaing['prev_link']      = '';
            $mypaing['last_link']      = '';
            $mypaing['first_link']     = '';

            $this->pagination->initialize($mypaing);
            $data['paginglink'] = $this->pagination->create_links();
            $page               = ($this->uri->segment(2) != '') ? $this->uri->segment(2) : 0;
            $data['page']       = $page;

            $data['Allconversation'] = $this->Message_model->fetchAllConversations($this->session->userdata('userid'), $mypaing['per_page'], $page);

            /*echo "<pre>";
            print_r($data);
            exit;*/

            $data['content_view'] = "messages/message-center";
            $this->load->view("front/custom_template", $data);
        }

        /*$data['content_view'] = "messages/message-center";
    $this->load->view("front/custom_template", $data);*/
    }

    public function delete($conversation,$user)
    {
        if ($conversation) {
            $this->Message_model->DeleteConversation($user, $conversation);
            $this->session->set_flashdata('msg', 'Item deleted successfully.');
            redirect(base_url('messages'));
        }

    }

    public function delete_chats()
    {
        $user   = $this->session->userdata('userid');
        foreach($this->input->post('ulist1') as $key){
            $this->delete_multiple($key,$user);
        }
        $this->session->set_flashdata('msg', 'Item deleted successfully.');
        redirect(base_url('messages'));
    }

    public function delete_multiple($conversation,$user)
    {
        if ($conversation) {
            $this->Message_model->DeleteConversation($user, $conversation);
        }

    }

    /** JSON : save message */
    public function submitMessage()
    {
        $sender   = $this->session->userdata('userid');
        $receiver = $this->input->post('seller_id');
        $product  = $this->input->post('prod_id');
        $message  = $this->input->post('message');
        $seller_data = get_user_data($receiver);
        $user_email = $seller_data['store_user']->user_email;

        $resp     = "202";
        if (!empty($user_email) && !empty($sender) && !empty($receiver) && !empty($message)) {
            $confirm = $this->Message_model->submitMessage($sender, $product, $receiver, $message);

            if ($confirm) {
                $message = "You received a new message."."<br><br><br>";
                $message .= "Please login to view the message : ".base_url();
                $to   = $user_email;
                $from = 'contact@educki.com';
                send_email_2($to, $from, 'New Message', $message);
                $this->session->set_flashdata('msg', 'Message sent successfully.');
                $resp = "200";
            }

        }

        echo $resp;
    }

    private function send_email_to_user($data)
    {
        $this->email->set_mailtype('html');

        $site_email = $this->db->get('tbl_site_setting')->row_array();

        $this->email->from("no-reply@boomslangit.com", $site_email['name']);
        $this->email->to($data['user_email']);
        $this->email->subject($data['subject']);
        $this->email->message($data['message']);
        $sen = $this->email->send();
        if ($sen == 1) {
            $this->email->clear(true);
            return true;
        }
        return false;
    }

    /**
    send a json response conversation id
     */
    public function get_conversation($prodSlug)
    {
        $product = $this->Message_model->getProductDetailBySlug($prodSlug);
        if ($product) {
            $buyer           = $this->input->post('b');
            $conversation_id = $this->Message_model->get_conversation_id($product->id, $product->userId, $buyer);
            $resp            = $this->json_encode_app('200', $conversation_id, 'Requested conversation token');
        } else {
            $resp = $this->json_encode_app('202', '', 'Invalid Request');
        }

        echo $resp;

    }

    /**
    fetch new message if any
     */
    public function get_updates()
    {
        $receiver = $this->input->post('r');
        $con_id   = $this->input->post('c');

        $resp = $this->json_encode_app('202', '', 'No new update');

        if (!empty($receiver) && !empty($con_id)) {
            $messages = $this->Message_model->fetch_unread_Messages($receiver, $con_id);
            if ($messages) {
                $resp = $this->json_encode_app('200', $messages, 'Requested conversation token');
            }

        }

        echo $resp;

    }

    /** encode into api response*/
    public function json_encode_app($status, $data, $message = false)
    {
        $resp            = array();
        $resp['status']  = ($status) ? $status : '203';
        $resp['data']    = ($data) ? $data : array();
        $resp['message'] = ($message) ? $message : '';
        return json_encode($resp);
    }

}
