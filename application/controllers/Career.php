<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Career extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('front/career/career_model');
//        $this->load->library('upload');
        $this->load->helper(array('form', 'url'));
        date_default_timezone_set('America/New_York');
//        auth();
    }

    public function index()
    {

        $pagedata['pagedata']      = $this->common_model->commonselect('tbl_pages', 'pg_id', 12)->row();
        $data['title']             = $pagedata['pagedata']->pg_title;
        $data['meta_title']        = $pagedata['pagedata']->meta_title;
        $data['meta_description']  = $pagedata['pagedata']->meta_description;
        $data['meta_keyword']      = $pagedata['pagedata']->meta_keywords;
        $data['content_view']      = "career/career_list";
        $count_where               = 'WHERE career_is_deleted = 0 AND career_is_active = 1';
        $data['total_records']     = count($this->common_model->get_data_page("tbl_career", $count_where)->result_array());
        $where                     = 'career_is_deleted = 0 AND career_is_active = 1';
        $jointable                 = 'tbl_states';
        $joincondition             = 'tbl_states.stat_id = tbl_career.career_state';
        $jointype                  = 'inner';
        $orderby                   = 'career_id';
        $order                     = 'desc';
        $data['list']              = $this->career_model->career_applied_user_listing("tbl_career", $where, $jointable, $joincondition, $jointype, '', '', $is_count = 0, $orderby, $order)->result_array();
        $orderby                   = 'career_id';
        $data['alphabetical_list'] = $this->career_model->career_applied_user_listing("tbl_career", $where, $jointable, $joincondition, $jointype, '', '', $is_count = 0, $orderby, $order = 'DESC')->result_array();
        $data['page_data']         = $this->common_model->commonselect('tbl_career_page', 'carrier_page_id', 1)->row();
        $this->load->view("front/custom_template", $data);
    }

    public function career_details($career_url = '')
    {

        $data['career_data'] = $this->career_model->view_details($career_url);
        foreach ($data['career_data'] as $list) {
            $data['meta_title']       = $list['career_title'];
            $data['career_id']        = $list['career_id'];
            $data['career_url']       = $career_url;
            $data['meta_description'] = $list['career_description'];
            $data['meta_keyword']     = $list['career_title'];
            $data['title']            = $list['career_title'];
            $data['image']            = $list['career_image'];
            $data['area']             = $list['career_area'];
            $data['description']      = $list['career_description'];
            $data['city']             = $list['career_city'];
            $data['state_id']         = $list['career_state'];
            $data['state_name']       = $list['stat_name'];
            $data['created_date']     = $list['career_created_date'];
            $data['end_date']         = $list['career_end_date'];
            $data['share_title']      = $list['career_title'];
            $data['share_image']      = base_url()."resources/applied_user_career_images/career.png";

        }
        $data['content_view'] = "career/career_details";
        $this->load->view("front/custom_template", $data);
    }

    public function apply_career()
    {
        $career_id = $this->career_model->add_applied_user();
        if ($career_id != '') {
            $success_data['title']            = 'Success';
            $success_data['meta_description'] = 'success Message';
            $success_data['meta_keyword']     = 'success Message';
            $success_data['message']          = 'Applied Successfully';
            $success_data['content_view']     = "career/career_thankyou";
            $this->load->view("front/custom_template", $success_data);
        } else {
            $success_data['title']            = 'Failure';
            $success_data['meta_description'] = 'Failure Message';
            $success_data['meta_keyword']     = 'Failure Message';
            $success_data['message']          = 'Error occur Please contact Administrator.';
            $data['content_view']             = "career/career_details";
            $this->load->view("front/custom_template", $data);
        }
    }

    public function check_apply(){
        $data_check['career_id'] = $this->input->post('career_id');
        $data_check['email'] = $this->input->post('email');
        $data['result'] = $this->common_model->commonselect2('tbl_careers_applied_user','career_id',$data_check['career_id'],'career_user_email',$data_check['email'])->result();
        if(count($data['result'])>0){
            echo "1";
        }else{
            echo "2";
        }
    }

}
