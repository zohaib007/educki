<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Preview extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->storeInfo = $this->common_model->commonselect('tbl_stores', 'user_id', $this->session->userdata('userid'))->row();
        if (count($this->storeInfo) <= 0) {
            redirect(base_url('my-profile'));
            exit;
        }
    }

    public function product_preview() {
//        var_dump($_POST);
//        die;

        $data['title'] = "Product Preview";
        $prodData['looking_for'] = $this->input->post('looking_for');
        $prodData['condition'] = $this->input->post('condition');
        $prodData['qty'] = $this->input->post('qty');
        $prodData['prod_title'] = $this->input->post('prod_title');
        $prodData['prod_url'] = $this->common_model->create_slug($prodData['prod_title'], 'tbl_products', 'prod_url');
        $prodData['prod_sku'] = $this->input->post('prod_sku');
        $prodData['prod_description'] = $this->input->post('prod_description');
        $prodData['youtube_links'] = $this->input->post('youtube_links');
        $prodData['prod_price'] = $this->input->post('prod_price');
        $prodData['prod_onsale'] = $this->input->post('is_onsale');
        $prodData['ship_method'] = $this->input->post('shipping_method');
        if ($prodData['prod_onsale'] == 1) {
            $prodData['sale_price'] = $this->input->post('sale_price');
            $prodData['sale_start_date'] = $this->input->post('sale_start_date');
            $prodData['sale_end_date'] = $this->input->post('sale_end_date');
        }
        $prodData['shipping'] = $this->input->post('shipping');
        if ($prodData['shipping'] == "Charge for Shipping") {
            $prodData['ship_price'] = $this->input->post('ship_price');
            $prodData['ship_days'] = $this->input->post('ship_days');
        } else if ($prodData['shipping'] == "Offer free Shipping") {
            $prodData['ship_days'] = $this->input->post('free_ship_days');
        }
        $prodData['prod_return'] = $this->input->post('prod_return');

        $prodData['prod_cat_id'] = $this->input->post('mainCat');
        if ($this->input->post('sub_sub_cat') != '') {
            $prodData['prod_cat_id'] = $this->input->post('sub_sub_cat');
        } else if ($this->input->post('sub_cat') != '') {
            $prodData['prod_cat_id'] = $this->input->post('sub_cat');
        }
        $prodData['prod_created_date'] = date('Y-m-d H:i:s');

        $prodData['prod_store_id'] = $this->input->post('sub_cat');
        $prodData['prod_user_id'] = $this->session->userdata('userid');
        $prodData['seller_data'] = $this->common_model->commonselect('tbl_user', 'user_id', $this->storeInfo->user_id)->row();
        $prodData['img'] = $this->input->post('img_name');

        $prodData['title'] = "Product Preview";

        $prodData['content_view'] = "products/product_preview";
        $this->load->view("front/preview_template", $prodData);
    }

}
