<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Address extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('front/address/address_model');
        authFront();
    }

    public function index() {
        $data['title'] = "Address Book";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";
        $data['userid'] = $this->session->userdata('userid');
        $data['default_shipping'] = $this->common_model->default_shipping($data['userid']);
        $data['default_billing'] = $this->common_model->default_billing($data['userid']);
        $data['address_list'] = $this->address_model->address_list($data['userid']);

        /*echo "<pre>";
        print_r($data);
        exit;*/
        
        $data['content_view'] = "address/address_list";
        $this->load->view("front/custom_template", $data);
    }

    public function add_address() {
        $data['title'] = "Add New Address";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";
        $this->form_validation->set_rules('nick_name', 'Nick Name', 'trim|required');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('street', 'street', 'trim|required');
        $this->form_validation->set_rules('city', 'city', 'trim|required');
        $this->form_validation->set_rules('country', 'country', 'trim|required');
        $this->form_validation->set_rules('state', 'state', 'trim|required');
        $this->form_validation->set_rules('zip_code', 'zip code', 'trim|required');
        //$this->form_validation->set_rules('phone', 'phone', 'required');
        $this->form_validation->set_message('required', 'This field is required.');
        $data['userid'] = $this->session->userdata('userid');
        $data['page_data'] = $this->common_model->commonSelect('tbl_stores', 'user_id', $data['userid'])->row();
        $data['allstate'] = $this->db->query("SELECT stat_id, stat_name FROM tbl_states")->result_array();
        $data['allcountries'] = $this->db->query("SELECT iso, name FROM tbl_country")->result_array();
        if ($this->form_validation->run() == false) {
            $data['content_view'] = "address/add_address";
            $this->load->view("front/custom_template", $data);
        } else {
            $response = $this->address_model->add_address();
            if ($response == 1) {
                $this->session->set_flashdata('msg', 'Address Added Successfully.');
                redirect(base_url() . "my-address");
            }
            $data['content_view'] = "address/add_address";
            $this->load->view("front/custom_template", $data);
        }
    }

    public function edit_address($address_id = '') {
        $data['title'] = "Edit Address";
        $data['meta_description'] = "meta_description";
        $data['meta_keyword'] = "meta_keywords";
        $this->form_validation->set_rules('nick_name', 'Nick Name', 'trim|required');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('street', 'street', 'trim|required');
        $this->form_validation->set_rules('city', 'city', 'trim|required');
        $this->form_validation->set_rules('country', 'country', 'trim|required');
        $this->form_validation->set_rules('state', 'state', 'trim|required');
        $this->form_validation->set_rules('zip_code', 'zip code', 'trim|required');
        $this->form_validation->set_message('required', 'This field is required.');
        $data['userid'] = $this->session->userdata('userid');
        $data['page_data'] = $this->common_model->commonSelect('tbl_user_addreses', 'add_id', $address_id)->row();
        $data['allstate'] = $this->db->query("SELECT stat_id, stat_name FROM tbl_states")->result_array();
        $data['allcountries'] = $this->db->query("SELECT iso, name FROM tbl_country")->result_array();
        if ($this->form_validation->run() == false) {
            $data['content_view'] = "address/edit_address";
            $this->load->view("front/custom_template", $data);
        } else {
            $response = $this->address_model->edit_address();
            if ($response == 1) {
                $this->session->set_flashdata('msg', 'Address Updated Successfully.');
                redirect(base_url() . "my-address");
            }
        }
    }

    public function delete($address_id) {
        $response = $this->common_model->commonDelete('tbl_user_addreses', 'add_id', $address_id);
        if ($response != NULL) {
            $this->session->set_flashdata('msg', 'Address Deleted Successfully.');
            redirect(base_url() . "my-address");
        }
    }

}
