<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Privacy_policy extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('common/common_model');
        $this->load->helper(array('form', 'url'));
        //auth();
    }

    public function index()
    {
        $data['pagedata'] = $this->common_model->commonselect('tbl_pages', 'pg_id', 8)->row();
        $data['title']            = $data['pagedata']->pg_title;
        $data['meta_title']       = $data['pagedata']->meta_title;
        $data['meta_description'] = $data['pagedata']->meta_description;
        $data['meta_keyword']     = $data['pagedata']->meta_keywords;
        $data['content_view']     = "privacy-policy";

        if (count($data) <= 0) {
            redirect(base_url());
        } else {
            $this->load->view("front/custom_template", $data);
        }
    }
}
