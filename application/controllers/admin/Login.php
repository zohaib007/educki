<?php //if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
 		
		$this->load->model('admin/admin_model');
	}

	
// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function index()
	{
		if($this->session->userdata('admin_id')!='')
		{
			redirect(base_url().'admin/dashboard');
			exit;
		}
		
		$this->form_validation->set_rules('log', 'Username', 'trim|required');
		$this->form_validation->set_rules('pwd', 'Password', 'trim|required');
		
		if($this->form_validation->run() === FALSE)
		{
			$data['MetaTitle'] = "Admin - Login";
			$this->load->view('admin/login', $data);
		}
		else
		{
			$post_array = array(
							'user_name' => $this->db->escape_str($this->input->post('log')),
							'password' => $this->db->escape_str(md5($this->input->post('pwd')))
						 );
			
			$result = $this->admin_model->check_admin($post_array);
            
			if(!empty($result))
			{
				$this->session->set_userdata('admin_id',$result['admin_id']);
				$this->session->set_userdata('admin_status',$result['admin_status']);
				//$this->session->set_userdata('admin_type',$result['admin_type']);
				$this->session->set_userdata('admin_email',$result['admin_email']);
				$this->session->set_userdata('admin_name',$result['admin_username']);
				$this->session->set_userdata('admin_is_super',$result['admin_is_superadmin']);
								
				redirect(base_url("admin/dashboard"));	
				exit;				
			}
			else
			{
				$this->session->set_flashdata('msg','The username or password is not correct.');
				redirect(base_url("admin/login"));
				exit;
			}
			
			exit;
		}
	}
// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function check_admin($str)
	{
		$user_name = $this->input->post('log');
		$password  = md5($this->input->post('pwd'));
		$result = $this->admin_model->check_admin($user_name,$password);
		//echo $this->db->last_query();
		//print_r($result);
		if($result->num_rows()>0)
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('check_admin','Invalid username or password.');
			return FALSE;
		}
	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function logout()
	{
		$this->session->set_userdata('admin_id','');
		$array_items = array('admin_id' => '');
		$this->session->unset_userdata($array_items);
		$this->session->sess_destroy();	
		session_start();
		session_destroy();
		redirect(base_url('admin/login'));
		exit;	
	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function change_password()
	{
		$adminid = $this->session->userdata('admin_id');
		$this->form_validation->set_rules('old', 'Old Password', 'trim|required|callback_check_old');
		$this->form_validation->set_rules('pwd', 'New Password', 'trim|required|min_length[6]|max_length[20]|matches[retype]');
		$this->form_validation->set_rules('retype', 'Confirm Password', 'trim|required');
		if($this->form_validation->run() === FALSE)
		{
			$data['class'] = 'changepassword';
			$data['contents'] = $this->load->view('admin/change_password',$data,true);
			$this->load->view('admin/template',$data);		
		}
		else
		{
			$adminid = $this->session->userdata('admin_id');
			$oldpass = md5($this->input->post('old'));
			$data['user_password_md5'] = md5($this->input->post('pwd'));
			
			$this->input->post('log');
			$result = $this->admin_model->change_password($data,$adminid,$oldpass);
	
			$this->session->set_userdata('msg' , 'Password changed successfully');
			redirect(base_url("admin/login/change_password"));
			exit;
		}
		
	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function check_old($old)
	{
		$ol = md5($old);  	
		auth();
		$result = $this->admin_model->checkold($ol);
		
		if($result == TRUE)
		{
			$this->form_validation->set_message('check_old', '* Old Password is Wrong');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function forget_password()
	{
		$this->form_validation->set_rules('txt_email', 'Email',  'trim|required|valid_email|callback_validate_email');

		if($this->form_validation->run()==FALSE)
		{
			$data['MetaTitle'] = "Admin - Login";
			$this->load->view('admin/foget_pass_view', $data);
		}
		else
		{
			$admin_email = $this->input->post('txt_email');
			$result = $this->admin_model->forget_password($admin_email);

			if($result->num_rows()>0)
			{
				$user = $result->row_array();							
				$admin_forget_email = $user['admin_email'];
				$admin_forget_pass = $user['admin_password'];
				$this->retrive_password($admin_forget_email,$admin_forget_pass);
				$this->session->set_flashdata('msg', 'Your password has been emailed to you.');
				redirect(base_url("admin/login"));
				exit;
			}
			else
			{
				$this->session->set_userdata('resetpassword',"Email is not matched");
				redirect(base_url("admin/login"));
				exit;
			}
		}
	}

		

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function validate_email($a_email)
	{
		if($a_email!=''){
			$result = $this->admin_model->validate_email($a_email);
			if($result == TRUE){
				$this->form_validation->set_message('validate_email', 'You have typed a wrong email address.');
				return FALSE;
			}		
			else{
				return TRUE;
			}
		}else{
			$this->form_validation->set_message('validate_email', 'This field is required.');
			return FALSE;
		}
	}
	
// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

	public function validate_password($a_email)
	{
		$result = $this->admin_model->validate_password($a_email);
		if ($result == TRUE)
		{
			$this->form_validation->set_message('validate_password', ' You have typed a wrong Password.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

	public function retrive_password($admin_forget_email,$admin_forget_pass)
	{
		
		$message = "<table cellpadding='5' cellspacing='0' width='100%' bgcolor='#ecf1f6' style='border: 1px solid #1c1c1c;'>";
		$message.= "<tr bgcolor='#5c5d5e'><td colspan='2' align='left' height='20' style='color:FFFFFF; font-weight:bold;'>".$this->config->item("SiteName")." Admin </td></tr>";
		$message.= "<tr bgcolor='#d3d7db'><td colspan='2' align='left' height='10' style='color:FFFFFF; font-weight:bold;'></td></tr>";
		$message.= "<tr><td colspan='2'>Thank you for using ".$this->config->item("SiteName").".</td></tr>";
		$message.= "<tr><td colspan='2'>Please keep this Email safe in your record. Your account information is as follows:</td></tr>";
		$message.= "<tr><td><b>Account Password</b> : <font color='#06577d'>".$admin_forget_pass."</font></td></tr>";
		$message.= "<tr><td colspan='2'>".$this->config->item("SiteName")." Team</td></tr>";
		$message.= "<tr bgcolor='#d3d7db'><td colspan='2' align='left' height='10' style='color:FFFFFF; font-weight:bold;'></td></tr>";
		$message.= "<table>";
		$subject = 'Password Retrieval';
		$message =  $message ;

		$is_send = send_email_2($admin_forget_email, 'noreply@educki.com', $subject, $message);
	}
	
// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

//////////////////////////////////////////// Change Password //////////////////////////////////////////////////

	public function changepassword()
	{
		$data['title'] = "Change Password";
		
		$this->form_validation->set_rules('cpassword', 'Current password', 'trim|required|xss_clean|callback_validate_password');
		$this->form_validation->set_rules('npassword', 'Password', 'trim|required|matches[cnpassword]');
		$this->form_validation->set_rules('cnpassword', 'Password', 'trim|required');
		
		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('admin/changepassword/password', $data);
		}
		else
		{
			$data_save['admin_password'] = $this->input->post('npassword');
			$data_save['admin_password_md5'] = md5($this->input->post('npassword'));
			$this->common_model->commonUpdate('tbl_admin',$data_save ,"admin_id",$this->session->userdata('admin_id'));
			
			$this->session->set_flashdata('pass_chnage_msg',"Your Password Changed Successfully");	
			redirect("admin/login/changepassword");
			exit;
		}
	}


// MNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMNMN

} // end of class
