<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/admin_model');
        $this->load->model('common/common_model');
        auth();

    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function index()
    {
        $data['marketplace_data'] = $this->marketplace_dashboard('2017-01-01', date('Y-m-d'));
        $data['_revenue_data'] = $this->total_revenue();
        $data['subscribers'] = $this->subscribers();
        $data['cancel_subscriber'] = $this->cancel_subscription(date('Y-m-d'), date('Y-m-d'));
        $data['seller_metrics'] = $this->seller_metrics('2017-01-01', date('Y-m-d'));
        $data['tier_revenue'] = $this->total_revenue_by_tier();

        $data['tier_1'] = $this->common_model->getTierStatistics(1);
        $data['tier_2'] = $this->common_model->getTierStatistics(2);
        $data['tier_3'] = $this->common_model->getTierStatistics(3);
        $data['tier_4'] = $this->common_model->getTierStatistics(4);

        $data['reported_user_item'] = $this->user_and_item_reported('2017-01-01', date('Y-m-d'));

        $data['shipping_summary'] = $this->shipping_list();

        $data['search_result'] = $this->most_searched_categories();

        $data['buyers'] = $this->buyers_growth();

        $data['stores'] = $this->top_stores('2017-01-01', date('Y-m-d'));

        $data['products'] = $this->product_listing('2017-01-01', date('Y-m-d'));

        $data['EMRR'] = $this->existing_recurrig_revenue();

        $data['CMRR'] = $this->churn_recurrig_revenue();

        $data['EXMRR'] = $this->expan_recurrig_revenue();

        /*echo '<pre>';
        print_r($data);
        exit;*/


        $this->load->view('admin/dashboard', $data);
    }

    public function marketplace_dashboard($start, $end)
    {

        $row['GMV'] = $this->db->query("SELECT SUM(order_grand_total) as sum FROM tbl_orders WHERE DATE(order_date) >= DATE('" . $start . "') AND DATE('" . $end . "') >= DATE(order_date) AND order_status = 'completed';")->row()->sum;
        $row['transactions_number'] = $this->db->query("SELECT COUNT(order_grand_total) as count FROM tbl_orders WHERE DATE(order_date) >= DATE('" . $start . "') AND DATE('" . $end . "') >= DATE(order_date) AND order_status = 'completed';")->row()->count;
        $row['avg_order_value'] = $this->db->query("SELECT AVG(order_grand_total) as avg FROM tbl_orders WHERE DATE(order_date) >= DATE('" . $start . "') AND DATE('" . $end . "') >= DATE(order_date) AND order_status = 'completed';")->row()->avg;

        $months = $this->get_months_array('M y');
        $dates = $this->get_months_array('Y-m-01');
        $months_number = $this->get_months_array('m');

        $years = $this->get_years_array('Y');
        $first_day = date("Y-m-d", strtotime('first day of January '.$years[11]));
        $last_day = date("Y-m-d", strtotime('last day of December '.$years[0]));


        $Chart_1 = $this->db->query("SELECT SUM(order_grand_total) as sum,order_date,MONTH(order_date) as month
                                        FROM tbl_orders 
                                        WHERE DATE(order_date) <= DATE('" . $dates[0] . "') AND 
                                        DATE('" . $dates[11] . "') <= DATE(order_date) AND order_status = 'completed'                                         
                                        GROUP BY  MONTH(order_date)
                                        ")->result();

        $Chart_2 = $this->db->query("SELECT SUM(order_grand_total) as sum,order_date,YEAR(order_date) as year
                                        FROM tbl_orders 
                                        WHERE DATE(order_date) <= DATE('" . $last_day . "') AND 
                                        DATE('" . $first_day . "') <= DATE(order_date) AND order_status = 'completed'                                         
                                        GROUP BY  YEAR(order_date)
                                        ")->result();

        $row['Chart_1'] = [0,0,0,0,0,0,0,0,0,0,0,0];
        $row['Chart_2'] = [0,0,0,0,0,0,0,0,0,0,0,0];

        foreach ($Chart_1 as $i => $s) {
            $key = array_search($s->month, $months_number);
            $row['Chart_1'][$key] = $s->sum;
        }

        foreach ($Chart_2 as $i => $s) {
            $key = array_search($s->year, $years);
            $row['Chart_2'][$key] = $s->sum;
        }

        /*echo "<pre>";
        print_r($row['Chart_2']);*/

        for ($i=0; $i < 12 ; $i++) {
            if($row['Chart_1'][$i]>0 && $row['Chart_1'][$i+1]==0){
                $row['Chart_1'][$i] = 100;
            }elseif($row['Chart_1'][$i]==0 && $row['Chart_1'][$i+1]==0){
                $row['Chart_1'][$i] = 0;
            }else{
                $row['Chart_1'][$i] = number_format((($row['Chart_1'][$i]-$row['Chart_1'][$i+1])/$row['Chart_1'][$i+1])*100,2,'.','');
            }
        }

        for ($i=0; $i < 12 ; $i++) {
            if($row['Chart_2'][$i]>0 && $row['Chart_2'][$i+1]==0){
                $row['Chart_2'][$i] = 100;
            }elseif($row['Chart_2'][$i]==0 && $row['Chart_2'][$i+1]==0){
                $row['Chart_2'][$i] = 0;
            }else{
                $row['Chart_2'][$i] = number_format((($row['Chart_2'][$i]-$row['Chart_2'][$i+1])/$row['Chart_2'][$i+1])*100,2,'.','');
            }
        }

        $months = array_reverse($months);

        $row['Chart_1'] = array_reverse($row['Chart_1']);

        $years = array_reverse($years);

        $row['Chart_2'] = array_reverse($row['Chart_2']);

        $row['months'] = "'" . join("', '",$months) . "'";

        $row['years'] = "'" . join("', '",$years) . "'";

        $row['Chart_1'] = "'" . join("', '",$row['Chart_1']) . "'";

        $row['Chart_2'] = "'" . join("', '",$row['Chart_2']) . "'";

        //$row['MOM'] = ($thisMonth - $lastMonth) / $lastMonth;

        /*echo "<pre>";
        print_r($years);
        print_r($Chart_2);
        print_r($row['Chart_2']);
        exit;*/

        return $row;

    }

    public function get_months_array($format)
    {
        $months = array();        
        for($i = 12; $i > 0; $i--){
            if($i==1 && $format == 'Y-m-01'){
                array_unshift($months,date('Y-m-t',strtotime('-'. $i .' months')));
            }else{
                array_unshift($months,date($format,strtotime('-'. $i .' months')));
            }
        }
        return $months;
    }

    public function get_years_array($format)
    {
        $months = array();        
        for($i = 11; $i > 0; $i--){
            if($i==1 && $format == 'Y-m-01'){
                array_unshift($months,date('Y',strtotime('-'. $i .' years')));
            }else{
                array_unshift($months,date($format,strtotime('-'. $i .' years')));
            }            
        }
        array_unshift($months,date($format));
        /*if($format=='Y-m-01'){
            array_unshift($months,date('Y-m-t'));
        }else{
            array_unshift($months,date($format));
        }*/
        return $months;
    }

    public function ajax_marketplace_dashboard($start, $end)
    {

        //    $row['GMV'] = $this->db->query("SELECT SUM(order_grand_total) as sum FROM tbl_orders WHERE DATE(order_date) >= DATE('" . $start . "') AND DATE('" . $end . "') >= DATE(order_date) AND order_status = 'completed';")->row()->sum;
        //    $row['transactions_number'] = $this->db->query("SELECT COUNT(order_grand_total) as count FROM tbl_orders WHERE DATE(order_date) >= DATE('" . $start . "') AND DATE('" . $end . "') >= DATE(order_date) AND order_status = 'completed';")->row()->count;
        //   $row['avg_order_value'] = $this->db->query("SELECT AVG(order_grand_total) as avg FROM tbl_orders WHERE DATE(order_date) >= DATE('" . $start . "') AND DATE('" . $end . "') >= DATE(order_date) AND order_status = 'completed';")->row()->avg;

        echo json_encode($this->marketplace_dashboard($start, $end));

    }

    public function total_revenue()
    {

        $row['t1'] = $this->db->query("SELECT SUM(sub_store_id) AS t1 FROM tbl_user_store_subscription  WHERE sub_tier_id = 2 AND sub_is_renewal = 1;")->row()->t1;
        $row['t2'] = $this->db->query("SELECT SUM(sub_store_id) AS t2 FROM tbl_user_store_subscription  WHERE sub_tier_id = 3 AND sub_is_renewal = 1;")->row()->t2;
        $row['t3'] = $this->db->query("SELECT SUM(sub_store_id) AS t3 FROM tbl_user_store_subscription  WHERE sub_tier_id = 4 AND sub_is_renewal = 1;")->row()->t3;

        return $row;
    }

    public function subscribers()
    {

        $row['total_subscriber_t1'] = $this->db->query("SELECT COUNT(DISTINCT(sub_store_id)) AS t1 FROM tbl_user_store_subscription  WHERE sub_tier_id = 2;")->row()->t1;
        $row['total_subscriber_t2'] = $this->db->query("SELECT COUNT(DISTINCT(sub_store_id)) AS t1 FROM tbl_user_store_subscription  WHERE sub_tier_id = 3;")->row()->t2;
        $row['total_subscriber_t3'] = $this->db->query("SELECT COUNT(DISTINCT(sub_store_id)) AS t1 FROM tbl_user_store_subscription  WHERE sub_tier_id = 4;")->row()->t3;

        $row['renew_percentage_subscriber_t1'] = $this->db->query("SELECT (COUNT(DISTINCT(sub_store_id))/(SELECT COUNT(*) FROM tbl_user_store_subscription)) * 100 AS t1 FROM tbl_user_store_subscription  WHERE sub_tier_id = 2 AND sub_is_renewal = 1 ;")->row()->t1;
        $row['renew_percentage_subscriber_t2'] = $this->db->query("SELECT (COUNT(DISTINCT(sub_store_id))/(SELECT COUNT(*) FROM tbl_user_store_subscription)) * 100 AS t2 FROM tbl_user_store_subscription  WHERE sub_tier_id = 3 AND sub_is_renewal = 1 ;")->row()->t2;
        $row['renew_percentage_subscriber_t3'] = $this->db->query("SELECT (COUNT(DISTINCT(sub_store_id))/(SELECT COUNT(*) FROM tbl_user_store_subscription)) * 100 AS t3 FROM tbl_user_store_subscription  WHERE sub_tier_id = 4 AND sub_is_renewal = 1 ;")->row()->t3;
        return $row;
    }

    public function cancel_subscription($start, $end)
    {

        $row['number'] = $this->db->query("SELECT COUNT(sub_id) AS count  FROM tbl_user_store_subscription  WHERE DATE(sub_ends) >= DATE('" . $start . "') AND DATE('" . $end . "') >= DATE(sub_ends) AND is_cancel = 1;")->row()->count;

        return $row;
    }

    public function ajax_cancel_subscription($start, $end)
    {
        echo json_encode($this->cancel_subscription($start, $end));
    }

    public function seller_metrics($start, $end)
    {

        $row['total_users'] = $this->db->query("SELECT COUNT(user_id) AS count FROM `tbl_user` WHERE DATE(user_register_date) >= DATE('" . $start . "') AND DATE('" . $end . "') >= DATE(user_register_date) AND user_is_delete = 0 and user_status =1;")->row()->count;
        $row['total_sellers'] = $this->db->query("SELECT COUNT(user_id) AS count FROM `tbl_user` WHERE DATE(user_register_date) >= DATE('" . $start . "') AND DATE('" . $end . "') >= DATE(user_register_date) AND user_is_delete = 0 and user_status =1 and user_type = 'Seller';")->row()->count;
        $row['new_sellers'] = $this->db->query("SELECT COUNT(user_id) AS count FROM `tbl_user` WHERE DATE(user_register_date) >= DATE('" . $start . "') AND DATE('" . $end . "') >= DATE(user_register_date) AND user_is_delete = 0 and user_status =1 and user_type = 'Seller' and user_renewal = 0;")->row()->count;
        //  $row['new_sellers'] = $this->db->query("SELECT COUNT(user_id) AS count FROM `tbl_user` WHERE DATE(user_register_date) >= DATE('" . $start . "') AND DATE('" . $end . "') >= DATE(user_register_date) AND user_is_delete = 0 and user_status =1 and user_type = 'Seller' and user_renewal = 0;")->row()->count;
        $thisMonth = $this->db->query("SELECT COUNT(user_id) AS count FROM `tbl_user` WHERE DATE(user_register_date) >= DATE_SUB('" . $end . "',INTERVAL 30 DAY) AND DATE('" . $end . "') >= DATE(user_register_date) AND user_is_delete = 0 and user_status =1 and user_type = 'Seller';")->row()->count;
        $lastMonth = $this->db->query("SELECT COUNT(user_id) AS count FROM `tbl_user` WHERE DATE(user_register_date) >= DATE_SUB('" . $end . "',INTERVAL 60 DAY) AND DATE_SUB('" . $end . "',INTERVAL 30 DAY) >= DATE(user_register_date) AND user_is_delete = 0 and user_status =1 and user_type = 'Seller';")->row()->count;
        if ($lastMonth == 0) {
            $row['MOM'] = 'none';
        } else {
            $row['MOM'] = ($thisMonth - $lastMonth) / $lastMonth;
        }

        $thisYear = $this->db->query("SELECT COUNT(user_id) AS count FROM `tbl_user` WHERE DATE(user_register_date) >= DATE_SUB('" . $end . "',INTERVAL 1 YEAR) AND DATE('" . $end . "') >= DATE(user_register_date) AND user_is_delete = 0 and user_status =1 and user_type = 'Seller';")->row()->count;
        $lastYear = $this->db->query("SELECT COUNT(user_id) AS count FROM `tbl_user` WHERE DATE(user_register_date) >= DATE_SUB('" . $end . "',INTERVAL 2 YEAR) AND DATE_SUB('" . $end . "',INTERVAL 1 YEAR) >= DATE(user_register_date) AND user_is_delete = 0 and user_status =1 and user_type = 'Seller';")->row()->count;
        if ($lastYear == 0) {
            $row['YOY'] = 'none';
        } else {
            $row['YOY'] = ($thisYear - $lastYear) / $lastYear;
        }

        $row['avg_rev'] = $this->db->query("SELECT SUM(store_total)/COUNT(DISTINCT(store_id)) as value  FROM `tbl_order_seller_info` where DATE(order_date) >= DATE('" . $start . "') AND DATE('" . $end . "') >= DATE(order_date);")->row()->value;
        $result = $this->db->query("SELECT COUNT(DISTINCT(store_id)) AS count,SUM(store_total) AS sale  FROM `tbl_order_seller_info`;")->row();
      $totalusers = $result->count;
      $totalrev = $result->sale;

      $limit =round( (20 * $totalusers) / 100);
        $result1 = $this->db->query("SELECT COUNT(sub.store_id) AS COUNT,SUM(sub.sale) AS sale FROM (SELECT DISTINCT(store_id),SUM(store_total) AS sale  FROM `tbl_order_seller_info` GROUP BY store_id ORDER BY sale DESC LIMIT " . $limit .") sub;")->row();
       $limitrev = $result1->sale;
       if($limitrev == null){
           $limitrev = 0;

       }

        $row['per_rev'] = ($limitrev/$totalrev) * 100;

        $subscribed = $this->db->query("SELECT SUM(timestampdiff(DAY,sub.sub_start,sub.sub_ends))/30 as subscribed
                                        FROM tbl_user_store_subscription sub
                                        /*INNER JOIN tbl_stores store*/
                                        WHERE sub.sub_tier_id != 1 
                                        GROUP BY sub.sub_store_id
                                        ")->result();
        $monthly_count = 0;
        $yearly_count = 0;
        foreach ($subscribed as $i => $month) {
            if($month->subscribed>1 && $month->subscribed<12){
                $monthly_count = $i;
            }elseif($month->subscribed>12){
                $yearly_count = $i;
            }
        }
        $row['monthly'] = number_format((($monthly_count/$row['total_sellers'])*100),2,'.','');
        $row['yearly'] = number_format((($yearly_count/$row['total_sellers'])*100),2,'.','');
        
        return $row;
    }

    public function ajax_seller_metrics($start, $end)
    {
        echo json_encode($this->seller_metrics($start, $end));
    }

    public function total_revenue_by_tier()
    {
        $row['tier_1'] = $this->db->query("SELECT SUM(orders.order_total_commission) AS count 
                        FROM `tbl_order_seller_info` info
                        INNER JOIN tbl_orders orders ON orders.order_id = info.order_id
                        WHERE  info.store_tier = 1 AND orders.order_status = 'completed';
                        ")->row()->count;
        $row['tier_2'] = $this->db->query("SELECT SUM(orders.order_total_commission) AS count 
                        FROM `tbl_order_seller_info` info
                        INNER JOIN tbl_orders orders ON orders.order_id = info.order_id
                        WHERE  info.store_tier = 2 AND orders.order_status = 'completed';
                        ")->row()->count;
        $row['tier_3'] = $this->db->query("SELECT SUM(orders.order_total_commission) AS count 
                        FROM `tbl_order_seller_info` info
                        INNER JOIN tbl_orders orders ON orders.order_id = info.order_id
                        WHERE  info.store_tier = 3 AND orders.order_status = 'completed';
                        ")->row()->count;
        $row['tier_4'] = $this->db->query("SELECT SUM(orders.order_total_commission) AS count 
                        FROM `tbl_order_seller_info` info
                        INNER JOIN tbl_orders orders ON orders.order_id = info.order_id
                        WHERE  info.store_tier = 4 AND orders.order_status = 'completed';
                        ")->row()->count;
        return $row;
    }

    public function user_and_item_reported($start, $end)
    {
        $row['item_reported'] = $this->db->query("SELECT COUNT(*) AS COUNT
                                                    FROM `tbl_item_flag_history` flag
                                                    INNER JOIN tbl_products prod ON prod.prod_id = flag.flag_prod_id
                                                    INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
                                                    INNER JOIN tbl_user users ON users.user_id = store.user_id
                                                    WHERE DATE(flag.flag_date) >= DATE('".$start."') AND DATE('".$end."') >= DATE(flag.flag_date) 
                                                    AND users.user_is_delete = 0
                                                    ")->row()->COUNT;
        $row['user_reported'] = $this->db->query("SELECT COUNT(*) AS COUNT
                                                    FROM `tbl_user_flag_history` flag
                                                    INNER JOIN tbl_user users ON users.user_id = flag.flag_user_id
                                                    WHERE DATE(flag.flag_date) >= DATE('".$start."') AND DATE('".$end."') >= DATE(flag.flag_date) 
                                                    AND users.user_is_delete = 0
                                                    ")->row()->COUNT;
        return $row;
    }

    public function ajax_user_and_item_reported($start, $end)
    {
        echo json_encode($this->user_and_item_reported($start, $end));
    }

    public function shipping_list()
    {
        $row['free_shipping'] = $this->db->query("SELECT COUNT(*) AS COUNT
                                                    FROM tbl_products prod
                                                    INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
                                                    INNER JOIN tbl_user users ON users.user_id = store.user_id
                                                    WHERE users.user_is_delete = 0 AND prod.prod_is_delete = 0 AND prod.qty>0 AND users.user_status = 1 AND store.store_is_hide = 0
                                                    AND prod.shipping = 'Offer free Shipping'
                                                    ")->row()->COUNT;
        $row['charge_shipping'] = $this->db->query("SELECT COUNT(*) AS COUNT
                                                    FROM tbl_products prod
                                                    INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
                                                    INNER JOIN tbl_user users ON users.user_id = store.user_id
                                                    WHERE users.user_is_delete = 0 AND prod.prod_is_delete = 0 AND prod.qty>0 AND users.user_status = 1 AND store.store_is_hide = 0
                                                    AND prod.shipping = 'Charge for Shipping'
                                                    ")->row()->COUNT;
        $row['local_shipping'] = $this->db->query("SELECT COUNT(*) AS COUNT
                                                    FROM tbl_products prod
                                                    INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
                                                    INNER JOIN tbl_user users ON users.user_id = store.user_id
                                                    WHERE users.user_is_delete = 0 AND prod.prod_is_delete = 0 AND prod.qty>0 AND users.user_status = 1 AND store.store_is_hide = 0
                                                    AND prod.shipping = 'Local Pick Up'
                                                    ")->row()->COUNT;
        return $row;
    }

    public function most_searched_categories()
    {
        $row['search_categories'] = $this->db->query("SELECT COUNT(*) AS total,cat_name
                                                    FROM `tbl_search_terms` search
                                                    INNER JOIN tbl_categories cat ON cat.cat_id = search.`cat_id`
                                                    GROUP BY search.cat_id  ORDER BY total DESC LIMIT 5 
                                                    ")->result();
        return $row;
    }

    public function buyers_growth()
    {
        $row['total_buyers'] = $this->db->query("SELECT COUNT(*) AS total
                                            FROM `tbl_user`
                                            WHERE user_is_delete = 0 AND user_type = 'Buyer'
                                            ")->row()->total;

        $fd_this_month = date('Y-m-01');
        $ld_this_month = date('Y-m-t');
        $fd_prev_month = date('Y-m-d', strtotime('first day of previous month'));
        $ld_prev_month = date('Y-m-d', strtotime('last day of previous month'));        
        $buyers = $this->get_buyers($fd_this_month,$ld_this_month);
        $prev_buyers = $this->get_buyers($fd_prev_month,$ld_prev_month);
        $row['growth_rate'] = (($buyers-$prev_buyers)/$prev_buyers)*100;
        return $row;
    }

    public function get_buyers($start,$end)
    {
        $users = $this->db->query("SELECT COUNT(*) AS COUNT
                                                    FROM `tbl_user`
                                                    WHERE user_is_delete = 0 AND user_type = 'Buyer' AND 
                                                    DATE(user_register_date) >= DATE('".$start."') AND DATE('".$end."') >= DATE(user_register_date) 
                                                    ")->row()->COUNT;
        return $users;
    }

    public function get_prod_listing($start,$end)
    {
        $prod = $this->db->query("SELECT COUNT(*) AS COUNT
                                FROM tbl_products prod
                                INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
                                INNER JOIN tbl_user users ON users.user_id = store.user_id
                                WHERE users.user_is_delete = 0 AND prod.prod_is_delete = 0 AND prod.qty>0 AND users.user_status = 1 AND store.store_is_hide = 0
                                AND DATE(prod.prod_created_date) >= DATE('".$start."') AND DATE('".$end."') >= DATE(prod.prod_created_date) 
                                ")->row()->COUNT;
        return $prod;
    }

    public function top_stores($start,$end)
    {
        $row['top_stores'] = $this->db->query("SELECT SUM(orders.order_total_commission) AS revenue,COUNT(prod.order_prod_id) AS sold,store.store_name
                                                FROM tbl_orders orders
                                                INNER JOIN `tbl_order_products` prod ON prod.order_id = orders.order_id
                                                INNER JOIN `tbl_order_seller_info` info ON info.order_id = prod.order_id
                                                INNER JOIN tbl_stores store ON store.store_id = info.store_id
                                                WHERE  orders.order_status = 'completed' AND 
                                                    DATE(orders.order_date) >= DATE('".$start."') AND DATE('".$end."') >= DATE(orders.order_date) 
                                                     GROUP BY store.store_id  ORDER BY revenue DESC LIMIT 10
                                                ")->result();
        return $row;
    }

    public function ajax_top_stores($start, $end)
    {
        $data = $this->top_stores($start, $end);
        $html='';
        $html .= "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><colgroup><col width=\"52%\"/><col width=\"24%\"/><col width=\"24%\"/></colgroup>";
        $html .= "<tr><th align=\"left\">Stores Name</th><th align=\"center\">Sales</th><th align=\"center\" class=\"last\"># Products Sold</th></tr>";
        foreach ($data['top_stores'] as $i => $store) {
            $html .= "<tr><td align=\"left\">$store->store_name</td><td align=\"center\"><strong>$ ".number_format($store->revenue,2,'.','')."</strong></td><td align=\"center\"><strong>$store->sold</strong></td></tr>";
        }
        if(count($data['top_stores'])==0){
            $html .= "<tr><td align=\"center\" colspan=\"3\"><strong>No Data Found</strong></td></tr>";
        }        
        echo $html;
    }

    public function product_listing($start, $end)
    {
        $row['sell'] = $this->db->query("SELECT COUNT(*) AS COUNT
                                                    FROM tbl_products prod
                                                    INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
                                                    INNER JOIN tbl_user users ON users.user_id = store.user_id
                                                    WHERE users.user_is_delete = 0 AND prod.prod_is_delete = 0 AND prod.qty>0 AND users.user_status = 1 AND store.store_is_hide = 0
                                                    AND prod.looking_for = 'Sell' AND 
                                                    DATE(prod.prod_created_date) >= DATE('".$start."') AND DATE('".$end."') >= DATE(prod.prod_created_date) 
                                                    ")->row()->COUNT;
        $row['trade'] = $this->db->query("SELECT COUNT(*) AS COUNT
                                                    FROM tbl_products prod
                                                    INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
                                                    INNER JOIN tbl_user users ON users.user_id = store.user_id
                                                    WHERE users.user_is_delete = 0 AND prod.prod_is_delete = 0 AND prod.qty>0 AND users.user_status = 1 AND store.store_is_hide = 0
                                                    AND prod.looking_for = 'Trade' AND 
                                                    DATE(prod.prod_created_date) >= DATE('".$start."') AND DATE('".$end."') >= DATE(prod.prod_created_date) 
                                                    ")->row()->COUNT;
        $row['donate'] = $this->db->query("SELECT COUNT(*) AS COUNT
                                                    FROM tbl_products prod
                                                    INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
                                                    INNER JOIN tbl_user users ON users.user_id = store.user_id
                                                    WHERE users.user_is_delete = 0 AND prod.prod_is_delete = 0 AND prod.qty>0 AND users.user_status = 1 AND store.store_is_hide = 0
                                                    AND prod.looking_for = 'Donate' AND 
                                                    DATE(prod.prod_created_date) >= DATE('".$start."') AND DATE('".$end."') >= DATE(prod.prod_created_date) 
                                                    ")->row()->COUNT;

        $fd_this_month = date('Y-m-01');
        $ld_this_month = date('Y-m-t');
        $fd_prev_month = date('Y-m-d', strtotime('first day of previous month'));
        $ld_prev_month = date('Y-m-d', strtotime('last day of previous month'));        
        $prod = $this->get_prod_listing($fd_this_month,$ld_this_month);
        //echo $this->db->last_query();
        $prev_prod = $this->get_prod_listing($fd_prev_month,$ld_prev_month);
        //echo $this->db->last_query();
        $row['growth_rate'] = number_format(((($prod-$prev_prod)/$prev_prod)*100),2,'.','');

        $avg = $this->db->query("SELECT AVG(prod.prod_price) AS COUNT
                                        FROM tbl_products prod
                                        INNER JOIN tbl_stores store ON store.store_id = prod.prod_store_id
                                        INNER JOIN tbl_user users ON users.user_id = store.user_id
                                        WHERE users.user_is_delete = 0 AND prod.prod_is_delete = 0 AND prod.qty>0 AND users.user_status = 1 AND store.store_is_hide = 0
                                        AND DATE(prod.prod_created_date) >= DATE('".$start."') AND DATE('".$end."') >= DATE(prod.prod_created_date) 
                                        ")->row()->COUNT;

        $row['avg'] = number_format($avg,2,'.','');

        return $row;
    }

    public function ajax_product_listing($start, $end)
    {
        echo json_encode($this->product_listing($start, $end));
    }

    public function existing_recurrig_revenue()
    {
        /*$ld_this_month = date('Y-m-t');
        $ld_next_month = date('Y-m-d', strtotime('last day of next month')); */
        $one_month = date('Y-m-d', strtotime('+1 month'));
        $row['EMRR_1']  = $this->db->query("SELECT SUM(tier_price) as COUNT
                                            FROM tbl_user_store_subscription
                                            WHERE sub_id IN (
                                                SELECT MAX(sub_id)
                                                FROM tbl_user_store_subscription
                                                GROUP BY sub_store_id
                                            ) AND sub_tier_id = 2 AND DATE(sub_ends) > DATE('".$one_month."')
                                        ")->row()->COUNT;
        $row['EMRR_2']  = $this->db->query("SELECT SUM(tier_price) as COUNT
                                            FROM tbl_user_store_subscription
                                            WHERE sub_id IN (
                                                SELECT MAX(sub_id)
                                                FROM tbl_user_store_subscription
                                                GROUP BY sub_store_id
                                            ) AND sub_tier_id = 3 AND DATE(sub_ends) > DATE('".$one_month."')
                                        ")->row()->COUNT;
        $row['EMRR_3']  = $this->db->query("SELECT SUM(tier_price) as COUNT
                                            FROM tbl_user_store_subscription
                                            WHERE sub_id IN (
                                                SELECT MAX(sub_id)
                                                FROM tbl_user_store_subscription
                                                GROUP BY sub_store_id
                                            ) AND sub_tier_id = 4 AND DATE(sub_ends) > DATE('".$one_month."')
                                        ")->row()->COUNT;
        return $row;
    }

    public function churn_recurrig_revenue()
    {
        $fd_this_month = date('Y-m-01');
        $ld_this_month = date('Y-m-t');
        $row['CMRR_1']  = $this->db->query("SELECT SUM(tier_price) as COUNT
                                            FROM tbl_user_store_subscription
                                            WHERE sub_id IN (
                                                SELECT MAX(sub_id)
                                                FROM tbl_user_store_subscription
                                                GROUP BY sub_store_id
                                            ) AND sub_tier_id = 2 AND sub_is_churn = 1 AND DATE(sub_ends) >= DATE('".$fd_this_month."') AND DATE(sub_ends) <= DATE('".$ld_this_month."')
                                        ")->row()->COUNT;
        $row['CMRR_2']  = $this->db->query("SELECT SUM(tier_price) as COUNT
                                            FROM tbl_user_store_subscription
                                            WHERE sub_id IN (
                                                SELECT MAX(sub_id)
                                                FROM tbl_user_store_subscription
                                                GROUP BY sub_store_id
                                            ) AND sub_tier_id = 3 AND sub_is_churn = 1 AND DATE(sub_ends) >= DATE('".$fd_this_month."') AND DATE(sub_ends) <= DATE('".$ld_this_month."')
                                        ")->row()->COUNT;
        $row['CMRR_3']  = $this->db->query("SELECT SUM(tier_price) as COUNT
                                            FROM tbl_user_store_subscription
                                            WHERE sub_id IN (
                                                SELECT MAX(sub_id)
                                                FROM tbl_user_store_subscription
                                                GROUP BY sub_store_id
                                            ) AND sub_tier_id = 4 AND sub_is_churn = 1 AND DATE(sub_ends) >= DATE('".$fd_this_month."') AND DATE(sub_ends) <= DATE('".$ld_this_month."')
                                        ")->row()->COUNT;
        return $row;
    }

    public function expan_recurrig_revenue()
    {
        $one_month = date('Y-m-d', strtotime('+1 month'));
        $row['EXMRR_1']  = $this->db->query("SELECT SUM(tier_price) as COUNT
                                            FROM tbl_user_store_subscription
                                            WHERE sub_id IN (
                                                SELECT MAX(sub_id)
                                                FROM tbl_user_store_subscription
                                                GROUP BY sub_store_id
                                            ) AND sub_tier_id = 2 AND((sub_is_renewal=1) OR (sub_is_renewal=0 AND sub_is_churn=0)) AND DATE(sub_ends) > DATE('".$one_month."')
                                        ")->row()->COUNT;
        $row['EXMRR_2']  = $this->db->query("SELECT SUM(tier_price) as COUNT
                                            FROM tbl_user_store_subscription
                                            WHERE sub_id IN (
                                                SELECT MAX(sub_id)
                                                FROM tbl_user_store_subscription
                                                GROUP BY sub_store_id
                                            ) AND sub_tier_id = 3 AND((sub_is_renewal=1) OR (sub_is_renewal=0 AND sub_is_churn=0)) AND DATE(sub_ends) > DATE('".$one_month."')
                                        ")->row()->COUNT;
        $row['EXMRR_3']  = $this->db->query("SELECT SUM(tier_price) as COUNT
                                            FROM tbl_user_store_subscription
                                            WHERE sub_id IN (
                                                SELECT MAX(sub_id)
                                                FROM tbl_user_store_subscription
                                                GROUP BY sub_store_id
                                            ) AND sub_tier_id = 4 AND((sub_is_renewal=1) OR (sub_is_renewal=0 AND sub_is_churn=0)) AND DATE(sub_ends) > DATE('".$one_month."')
                                        ")->row()->COUNT;
        return $row;
    }

}

?>