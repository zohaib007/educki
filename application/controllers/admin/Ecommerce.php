<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ecommerce extends CI_Controller {
	
 	public function __construct()
 	{
 		parent::__construct();
 		
		$this->load->model('categories/categories_model');
		$this->load->model('product/product_model');
		$this->load->model('ecommerce/ecommerce_model');
	
 		auth();
		
	}
	
	
	public function index()
	{
		$data['order_prod_ecommerce'] = $this->ecommerce_model->get_ecommerce_prod_report();
		if($this->input->post('date_range'))
		{
			$date = $this->input->post('date_range');
			$expload_date = explode("to", $date);
			@$date1 = date('Y-m-d', strtotime($expload_date[0]));
			@$date2 = date('Y-m-d', strtotime($expload_date[1]));
			$where = "(tbl_order.order_datetime between '$date1' and '$date2')";
			$data['order_prod_ecommerce'] = $this->ecommerce_model->sort_prod_report($where);
		}

		$this->load->view('admin/ecommerce/listprod_ecommerce' ,$data);
		
	}
	public function top_five_sold()
	{
		$data['order_prod_ecommerce'] = $this->ecommerce_model->top_five_prod();
		$this->load->view('admin/ecommerce/listfiveprod_ecommerce' ,$data);

	}
	
	
	
	
}