<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Product extends CI_Controller {

    public function __construct() 
	{
		parent::__construct();
		
        $this->load->model('categories/categories_model');
        $this->load->model('product/product_model');
        auth();
        
    }

    public function index()
	{
		$strWere = '';
        $data['MetaTitle'] = "Admin - Product List";
		$data['title'] = "Manage Products";
    	$data['resultsChannel'] = $this->product_model->prod_listing($strWere)->result_array();
	    $data['total_records'] = count($data['resultsChannel']);
	    /*echo "<pre>";
	    print_r($data);
	    exit;*/
        $this->load->view('admin/product/product_listing', $data);
    }
	
	public function edit($nId = '')
	{
		if(!is_numeric($nId) || $nId == '')
		{
			redirect(base_url()."admin/product");
			exit;
		}
		$data['title'] = "Edit Product";
		$prod_where = " AND prod.prod_id = ".$nId." AND prod_is_delete = 0";
		$data['prod_data'] = $this->product_model->get_product_data($prod_where)->row();
		
		if(empty($data['prod_data']))
		{
			redirect(base_url()."admin/product");
			exit;
		}
		$data['prodImg'] = $this->common_model->commonselect('tbl_product_images','img_prod_id',$data['prod_data']->prod_id)->result();
        $data['prodFilters'] = $this->db->query("
											SELECT *
											FROM tbl_product_filters s
											INNER JOIN tbl_cat_filter_title m ON m.filter_slug = s.filter_slug
											WHERE 
											s.prod_id = ".$nId."
											GROUP BY s.filter_id
											ORDER BY id ASC
										")->result();

		$this->form_validation->set_rules('prod_status', 'Product status', 'trim|required');
		$this->form_validation->set_rules('prod_feature', 'Product status', 'trim|required');
		$this->form_validation->set_message('required', 'This field is required.');

		if( $this->form_validation->run() == FALSE)
		{
			
                
			$this->load->view('admin/product/detail_product' ,$data);
        }
		else
		{
			$array['prod_status'] = $this->input->post('prod_status');
			$array['prod_feature'] = $this->input->post('prod_feature');
			$array['prod_update_date'] = date('Y-m-d h:i:s');
			$array['prod_update_by'] = $this->session->userdata('admin_id');
			
			$this->common_model->commonUpdate('tbl_products', $array, "prod_id", $nId);
			$this->session->set_flashdata('msg', 'Item updated successfully.');
			redirect(base_url()."admin/product");
			exit;
		}
	}
	
	public function delete($nId = '')
	{
		$array['prod_is_delete'] = 1;
		$this->common_model->commonUpdate('tbl_products', $array, "prod_id", $nId);
		$this->common_model->commonDelete('tbl_wish_list', "product_id", $nId);
		/*
        $this->common_model->commonDelete('tbl_product', "prod_id", $nId);
		$this->common_model->commonDelete('tbl_product_region_prices', "prod_region_prod_id", $nId);
		$this->common_model->commonDelete('tbl_related_products', "relp_prod_id", $nId);
		$this->common_model->commonDelete('tbl_product_category', "prod_cat_prod_id", $nId);
		*/
        $this->session->set_flashdata('msg', 'Item deleted successfully.');
        redirect(base_url()."admin/product");
		exit;
	}
	
	
   	public function export_csv()
	{
		$data = array();
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=product-listing-'.date("m-d-Y").'.csv');
		$output = fopen('php://output', 'w');
		fputcsv($output, array('Product Date', 'Product Name', 'Product ID', 'Inventory', 'Price', 'Status'));
		
		$resultsProd = $this->product_model->product_export_csv($strWere = "");
		//$data=strip_tag($resultsProd[$n]);

		for($n = 0; $n < count($resultsProd); $n++)
		{
			$arr[$n] = array(date('m-d-Y', strtotime($resultsProd[$n]['prod_date'])), $resultsProd[$n]['prod_name'], $resultsProd[$n]['prod_sku'], $resultsProd[$n]['prod_quantity'], $resultsProd[$n]['prod_price'], $resultsProd[$n]['prod_status']);
			fputcsv($output, $arr[$n]);
		}
    }

	public function export_excel_xml()
	{
		$arr = "";
        $resultsProd = $this->product_model->product_export_csv($strWere = "");
		
		for($n = 0; $n < count($resultsProd); $n++)
		{
        	$arr[] = array(date('m-d-Y', strtotime($resultsProd[$n]['prod_date'])), $resultsProd[$n]['prod_name'], $resultsProd[$n]['prod_sku'], $resultsProd[$n]['prod_quantity'], $resultsProd[$n]['prod_price'], $resultsProd[$n]['prod_status']);
        }
		
        $arrTitle = array('Product Date', 'Product Name', 'Product ID', 'Inventory', 'Price', 'Status');
        $data[] = $arrTitle;
		
        for ($n = 0; $n < count($arr); $n++)
        {
			$data[] = $arr[$n];
		}
		$sheetname = array('filename' => 'product-listing-'.date('m-d-Y'));
		$this->load->library('XSpreadsheet', $sheetname);
		$this->xspreadsheet->AddWorksheet('Awesome Sheet', $data)->Generate()->Send();
    }

	public function getCategory()
	{
        $resultsChannel = $this->common_model->common_where('tbl_categories', array('cat_type' => '1', "cat_cat_id" => $this->input->post('nId')));
        $str = "<option value=''>Select Category</option>";
        foreach ($resultsChannel->result() as $name_result)
            $str.="<option value=" . $name_result->cat_id . ">" . $name_result->cat_name . "</option>";

        echo $str;
    }
   
	public function getSubCategory()
	{
        $resultsChannel = $this->common_model->common_where('tbl_categories', array('cat_type' => '2', "cat_cat_id" => $this->input->post('nId')));
        $str = "<option value=''>Select Sub-Category</option>";
        foreach ($resultsChannel->result() as $name_result)
            $str.="<option value=" . $name_result->cat_id . ">" . $name_result->cat_name . "</option>";

        echo $str;
    }

	/*-------------------------------------------------------------------------------------------------*/
	/***************************************************************************************************/
	//									[ CALL BACK FUNCTION ]
	/***************************************************************************************************/
	/*-------------------------------------------------------------------------------------------------*/
	
	
	function validate_prod_name($value)
	{
		// () - _ " ' 0-9 space a-z and A-Z
		
		if( preg_match ("/^[0-9-_(),&\'\"\s]+$/",$value))
		{
			$this->form_validation->set_message('validate_prod_name', "This field contain only valid characters.");
			return false;
		}
		if( preg_match ("/^[a-zA-Z0-9-_(),&\'\"\s]+$/",$value))
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('validate_prod_name', "This field contain only valid characters.");
			return false;
			
		}
	}
	
	function validate_alphaNumericHypen($value)
	{
		if( preg_match ("/^[a-zA-Z0-9-]+$/",$value))
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('validate_alphaNumericHypen', "This field contain only alphanumeric and hypens.");
			return false;
			
		}
	}
	
	function validate_alphabet_space($value)
	{
		if( preg_match("/^[a-zA-Z\s]+$/",$value))
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('validate_alphabet_space', "This field contain only alphabets and spaces.");
			return false;
			
		}
	}
	
	function validate_alphabet_space_special_character($value)
	{
		if( preg_match("/^[a-zA-Z0-9_\s\W]+$/",$value) || $value == '' )
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('validate_alphabet_space_special_character', "This field contain only alphabets, special symbols and spaces.");
			return false;
			
		}
	}
	
	
	function less_than_prod_price($sale_price, $prod_price)
	{
		if($sale_price < $prod_price)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('less_than_prod_price', "This field must be less than product price.");
			return false;
			
		}
	}
	
	
	function valid_price_without_zero($value)
	{
		if($value > 0)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('valid_price_without_zero', "This field must be greater than zero.");
			return false;
			
		}
	}
	
	function valid_price_with_zero($value)
	{
		if($value >= 0)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('valid_price_with_zero', "This field must be greater than or equal to zero.");
			return false;
			
		}
	}
	
	
	function edit_prod_sku($value, $nId)
	{
		$result = $this->db->query("
									SELECT *
									FROM tbl_product
									WHERE
									prod_sku = '".$value."'
									AND
									prod_id != ".$nId."
									AND
									prod_is_delete = 0
									");
		
		if($result->num_rows() > 0)
		{
			$this->form_validation->set_message('edit_prod_sku','Product SKU already exists.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	function unique_sku($value)
	{
		$result = $this->db->query("
									SELECT *
									FROM tbl_product
									WHERE
									prod_sku = '".$value."'
									AND
									prod_is_delete = 0
									");
		
		if($result->num_rows() > 0)
		{
			$this->form_validation->set_message('unique_sku','This field must be unique.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	
	function valid_file($name)
	{
		if($name != "" && !empty($name))
		{	
			$ext = explode('.',$name);
			$ext = strtolower(end($ext));
			if ($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "png")
			{
				return true;
			}
			else
			{
				$this->form_validation->set_message('valid_file', 'The filetype you are attempting to upload is not allowed.');
				return false;
			}
		}
		else
		{
			$this->form_validation->set_message('valid_file', 'This field is required.');
			return false;
		}
	}
	
	function valid_length_string($str)
	{
		$str = strip_tags($str);
		if(strlen($str) <= 1000)
		{
			return true;
			
		}
		else
		{
			$this->form_validation->set_message('valid_length_string', 'This field can not exceed 1000 characters in length.');
			return false;
		}
	}
	
	
	
}



?>