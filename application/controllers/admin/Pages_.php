<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pages extends CI_Controller {

    public function __construct() {
        parent::__construct();
//        $this->load->library('upload');
        $this->load->model('content/content_model');
        //$this->load->helper(array('form', 'url'));

        auth();
    }

    public function index() {
        $query = "
                SELECT *
                FROM tbl_pages
                WHERE pg_id IN(1,4,8,9,10)
                ";
        /* echo "<pre>";
          print_r($this->db->query($query)->result_array());
          exit; */
        $data['pageList'] = $this->db->query($query)->result();
        $this->load->view('admin/pages/list', $data);
    }

    public function add_page() {

        $data['title'] = "Add New Page";
        $this->form_validation->set_rules('pg_title', 'page title', 'trim|required');
        $this->form_validation->set_rules('pg_url', 'page title', 'trim|required|valid_url|is_unique[tbl_pages.pg_url]');
//        $this->form_validation->set_rules('pg_is_listing', 'is listing', 'trim|required');
        $this->form_validation->set_rules('pg_description', 'Page description', 'trim|required');
        $this->form_validation->set_rules('pg_status', 'page title', 'trim|required');
        $this->form_validation->set_rules('meta_title', 'meta title', 'trim');
        $this->form_validation->set_rules('meta_keywords', 'meta keywords', 'trim');
        $this->form_validation->set_rules('meta_description', 'meta description', 'trim');

        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('valid_url', 'Please enter a valid URL.');
        $this->form_validation->set_message('is_unique', 'This URL already exists.');

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/pages/add_page', $data);
        } else {
            $date = date('Y-m-d H:i:s');
            /////////////// upload multiypal image
            $myFiles1 = $_FILES['image'];
            $fileDP1 = array();
            if ($myFiles1['name'] !== "") {

                $config = array(
                    'allowed_types' => 'jpg|jpeg|gif|png',
                    'upload_path' => FCPATH . '/resources/page_image/',
                    'file_name' => 'pg_img_' . date('m_d_Y') . '_' . time(),
                );
                $this->load->library('upload');
                $this->upload->initialize($config);
                //$this->upload->do_upload('file');
                if (!$this->upload->do_upload('image')) {
                    //Print message if file is not uploaded
                    $this->session->set_flashdata('msg', $this->upload->display_errors());
                    header("Location: " . base_url() . "admin/pages/add_page/");
                    exit();
                } else {
                    //////uplaod thumbnail image
                    $dataDP = $this->upload->data();
                    $fileDP1 = $dataDP['file_name'];
                    $this->load->library('image_lib');
                    $configt['image_library'] = 'gd2';
                    $configt['source_image'] = $dataDP['full_path'];
                    $configt['new_image'] = FCPATH . "/resources/page_image/thumb/";
                    $configt['maintain_ratio'] = false;
                    $configt['width'] = 80;
                    $configt['height'] = 80;
                    $this->image_lib->initialize($configt);
                    $this->image_lib->resize();
                }
            } else {
                $fileDP1 = $this->input->post('image');
            }
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            $array = array(
                'pg_title' => stripcslashes($this->input->post('pg_title')),
                'pg_description' => stripcslashes($this->input->post('pg_description')),
                'pg_url' => stripcslashes($this->input->post('pg_url')), //$this->common_model->create_slug($this->input->post('pg_title'), 'tbl_pages','pg_url')),
                'pg_image' => $fileDP1,
                'pg_status' => stripcslashes($this->input->post('pg_status')),
                'meta_title' => stripcslashes($this->input->post('meta_title')),
                'meta_keywords' => stripcslashes($this->input->post('meta_keywords')),
                'meta_description' => stripcslashes($this->input->post('meta_description')),
                'pg_is_deleteable' => 1,
//                'pg_is_listing' => $this->input->post('pg_is_listing'),
            );
            $array['date'] = date('Y-m-d H:i:s');

            $this->common_model->commonSave('tbl_pages', $array);
            $this->session->set_flashdata('msg', 'Item added successfully.');
            redirect(base_url() . "admin/pages");
        }
    }

    public function url_unique($url, $pg_id) {
        $check = $this->db->query("SELECT * FROM tbl_pages WHERE pg_url = '" . $url . "' AND is_delete = 0 AND pg_id != " . $pg_id)->row();
        if (count($check) > 0) {
            $this->form_validation->set_message('url_unique', 'This URL already exists.');
            return false;
        } else {
            return true;
        }
    }

    public function helper_url($url = '') {
        if (check_unique_url($url)) {
            $this->form_validation->set_message('helper_url', 'This URL already exists.');
            return false;
        } else {
            return true;
        }
    }

    public function edit_page($pg_id = '') {

        $data['page_data'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $pg_id)->row();

        $this->form_validation->set_rules('pg_title', 'page title', 'required');

        if ($data['page_data']->pg_url == $this->input->post('pg_url')) {
            $this->form_validation->set_rules('pg_url', 'page URL', 'trim|required|valid_url');
        } else {
            $this->form_validation->set_rules('pg_url', 'page URL', 'trim|required|valid_url|is_unique[tbl_pages_url.page_url]|callback_helper_url');
        }
//        $this->form_validation->set_rules('pg_is_listing', 'is listing', 'trim|required');
        if ($pg_id != 4) {
            $this->form_validation->set_rules('pg_description', 'Page description', 'trim|required');
        }
        //$this->form_validation->set_rules('pg_status', 'page title', 'trim|required');
        $this->form_validation->set_rules('meta_title', 'meta title', 'trim');
        $this->form_validation->set_rules('meta_keywords', 'meta keywords', 'trim');
        $this->form_validation->set_rules('meta_description', 'meta description', 'trim');

        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('valid_url', 'Please enter a valid URL.');
        $page_url_old = $data['page_data']->pg_url;

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/pages/edit_page', $data);
        } else {
            $date = date('Y-m-d H:i:s');
            /////////////// upload multiypal image
            $myFiles1 = $_FILES['image'];
            $fileDP1 = array();
            if ($myFiles1['name'] !== "" && $pg_id != 9 && $pg_id != 8 && $pg_id != 4) {

                $config = array(
                    'allowed_types' => 'jpg|jpeg|gif|png',
                    'upload_path' => FCPATH . '/resources/page_image/',
                    'file_name' => 'pg_img_' . date('m_d_Y') . '_' . time(),
                );
                $this->load->library('upload');
                $this->upload->initialize($config);
                //$this->upload->do_upload('file');
                if (!$this->upload->do_upload('image')) {
                    //Print message if file is not uploaded
                    $this->session->set_flashdata('msg', $this->upload->display_errors());
                    header("Location: " . base_url() . "admin/pages/edit_page/" . $pg_id);
                    exit();
                } else {
                    //////uplaod thumbnail image
                    $dataDP = $this->upload->data();
                    $fileDP1 = $dataDP['file_name'];
                    $this->load->library('image_lib');
                    $configt['image_library'] = 'gd2';
                    $configt['source_image'] = $dataDP['full_path'];
                    $configt['new_image'] = FCPATH . "/resources/page_image/thumb/";
                    $configt['maintain_ratio'] = false;
                    $configt['width'] = 80;
                    $configt['height'] = 80;
                    $this->image_lib->initialize($configt);
                    $this->image_lib->resize();
                }
            } else {
                $fileDP1 = $this->input->post('image');
            }
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            $page_url_new = $this->input->post('pg_url');
            $array = array(
                'pg_title' => stripcslashes($this->input->post('pg_title')),
                'pg_description' => stripcslashes($this->input->post('pg_description')),
                'pg_url' => $page_url_new, //$this->common_model->create_slug($this->input->post('pg_title'), 'tbl_pages','pg_url', 'pg_id', $pg_id),
                'pg_image' => $fileDP1,
                //'pg_status' => $this->input->post('pg_status'),
                'meta_title' => stripcslashes($this->input->post('meta_title')),
                'meta_keywords' => stripcslashes($this->input->post('meta_keywords')),
                'meta_description' => stripcslashes($this->input->post('meta_description')),
//                'pg_is_listing' => $this->input->post('pg_is_listing'),
            );
            $array['update'] = date('Y-m-d h:i:s');

            $url_array = array(
                'page_id' => $pg_id,
                'page_url' => $page_url_new,
                'page_table' => 'tbl_pages',
            );

            $this->update_routes($page_url_old, $page_url_new);

            $this->common_model->commonUpdate('tbl_pages', $array, 'pg_id', $pg_id);
            $this->common_model->commonUpdate('tbl_pages_url', $url_array, 'page_id', $pg_id);
            $this->session->set_flashdata('msg', 'Item updated successfully.');
            redirect(base_url() . "admin/pages");
        }
    }

    public function deltepage($id) {

        $this->common_model->commonDelete('tbl_pages', "pg_id", $id);
        $this->session->set_flashdata('msg', 'Item deleted successfully.');
        redirect(base_url() . "admin/pages");
    }

    public function delete_page_img() {
        if (!$this->input->is_ajax_request()) {
            
        } else {
            $id = $this->input->post('nId');
            $data = array("pg_image" => "");
            $this->db->where('pg_id', $id);
            $this->db->update('tbl_pages', $data);
        }
    }

    /*     * ***************************************************************************
      ------------------------------------------------------------------------------
      Sub pages listing add and edit
      ------------------------------------------------------------------------------
     * **************************************************************************** */

    public function sub_page_listing($pg_id = '') {

        if ($pg_id == '' || !is_numeric($pg_id)) {
            redirect(base_url('admin/pages'));
            exit;
        }
        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $pg_id)->row();
        if ( empty( $data['main_pg'] ) ) {
            redirect(base_url("admin/pages"));
            exit;
        }
        $data['pageList'] = $this->common_model->commonselect('tbl_pages_sub', 'sub_pg_id', $pg_id, 'sub_id', "ASC")->result();
        $this->load->view('admin/pages/sub_page_list', $data);
    }

    public function add_sub_page($pg_id = '') {
        if ($pg_id == '' || !is_numeric($pg_id)) {
            redirect(base_url('admin/pages'));
            exit;
        }
        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $pg_id)->row();
        $order = $this->db->query("SELECT MAX(side_menu_order) as max_order FROM tbl_pages_sub")->row();
        if (count($data['main_pg']) <= 0) {
            redirect(base_url("admin/pages"));
            exit;
        }

        $data['title'] = "Add New Sub Page";
        $this->form_validation->set_rules('pg_title', 'page title', 'trim|required');

        $this->form_validation->set_rules('pg_url', 'page title', 'trim|required|valid_url|is_unique[tbl_pages_url.page_url]|callback_helper_url');

        $this->form_validation->set_rules('pg_description', 'Page description', 'trim|required');
        $this->form_validation->set_rules('pg_status', 'page title', 'trim|required');
        $this->form_validation->set_rules('meta_title', 'meta title', 'trim');
        $this->form_validation->set_rules('meta_keywords', 'meta keywords', 'trim');
        $this->form_validation->set_rules('meta_description', 'meta description', 'trim');
        //$this->form_validation->set_rules('hidden_file1', 'image', 'trim|callback_valid_file');
        if ((isset($_FILES['image']) || @$_FILES['image']['name'] != '' || @$_FILES['image']['type'] != '')) {
            //$this->form_validation->set_rules('hidden_file1', 'file1', 'trim');
            $this->form_validation->set_rules('hidden_file1', 'Image', 'trim|callback_valid_file');
        }
//        $this->form_validation->set_rules('pg_is_listing', 'is listing', 'trim|required');

        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('valid_url', 'Please enter a valid URL.');
        $this->form_validation->set_message('is_unique', 'This URL already exists.');

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/pages/sub_add_page', $data);
        } else {
            $date = date('Y-m-d H:i:s');
            /////////////// upload multiypal image
            $myFiles1 = $_FILES['image'];
            $fileDP1 = array();
            if ($myFiles1['name'] !== "") {

                $config = array(
                    'allowed_types' => 'jpg|jpeg|gif|png',
                    'upload_path' => FCPATH . '/resources/page_image/',
                    'file_name' => 'sub_pg_img_' . date('m_d_Y') . '_' . time(),
                );
                $this->load->library('upload');
                $this->upload->initialize($config);
                //$this->upload->do_upload('file');
                if (!$this->upload->do_upload('image')) {
                    //Print message if file is not uploaded
                    $this->session->set_flashdata('msg', $this->upload->display_errors());
                    header("Location: " . base_url() . "admin/pages/add_sub_page/" . $pg_id);
                    exit();
                } else {
                    //////uplaod thumbnail image
                    $dataDP = $this->upload->data();
                    $fileDP1 = $dataDP['file_name'];
                    $this->load->library('image_lib');
                    $configt['image_library'] = 'gd2';
                    $configt['source_image'] = $dataDP['full_path'];
                    $configt['new_image'] = FCPATH . "/resources/page_image/thumb/";
                    $configt['maintain_ratio'] = false;
                    $configt['width'] = 80;
                    $configt['height'] = 80;
                    $this->image_lib->initialize($configt);
                    $this->image_lib->resize();
                }
            } else {
                $fileDP1 = $this->input->post('image');
            }
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            $array = array(
                'sub_pg_title' => stripcslashes($this->input->post('pg_title')),
                'sub_pg_name' => stripcslashes($this->input->post('pg_title')),
                'sub_pg_description' => stripcslashes($this->input->post('pg_description')),
                'sub_pg_image' => $fileDP1,
                'sub_pg_status' => stripcslashes($this->input->post('pg_status')),
                'meta_title' => stripcslashes($this->input->post('meta_title')),
                'meta_keywords' => stripcslashes($this->input->post('meta_keywords')),
                'meta_description' => stripcslashes($this->input->post('meta_description')),
                'sub_pg_id' => $pg_id,
                'sub_pg_url' => stripcslashes($this->input->post('pg_url')),
                'side_menu_order' => $order->max_order + 1,
                    //$this->common_model->create_slug($this->input->post('pg_title'), 'tbl_pages_sub','sub_pg_url', 'sub_id'),
                    //                'sub_pg_is_listing' => $this->input->post('pg_is_listing'),
            );
            $array['date'] = date('Y-m-d H:i:s');

            /* echo "<pre>";
              print_r($data);
              exit; */

            $sub_page_id = $this->common_model->commonSave('tbl_pages_sub', $array);

            $url_array = array(
                'page_id' => $sub_page_id,
                'page_url' => $this->input->post('pg_url'),
                'page_table' => 'tbl_pages_sub',
            );

            $this->common_model->commonSave('tbl_pages_url', $url_array);
            $this->session->set_flashdata('msg', 'Item added successfully.');
            redirect(base_url() . "admin/pages/sub_page_listing/" . $pg_id);
        }
    }

    public function sub_url_unique($url, $pg_id) {
        $check = $this->db->query("SELECT * FROM tbl_pages_sub WHERE sub_pg_url = '" . $url . "' AND sub_is_delete = 0 AND sub_id != " . $pg_id)->row();
        if (count($check) > 0) {
            $this->form_validation->set_message('sub_url_unique', 'This URL already exists.');
            return false;
        } else {
            return true;
        }
    }

    public function edit_sub_page($pg_id = '', $sub_id = '') {

        if ($pg_id == '' || !is_numeric($pg_id)) {
            redirect(base_url('admin/pages'));
            exit;
        }
        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $pg_id)->row();
        if (count($data['main_pg']) <= 0) {
            redirect(base_url("admin/pages"));
            exit;
        }
        if ($sub_id == '' || !is_numeric($sub_id)) {
            redirect(base_url('admin/pages/sub_page_listing/' . $pg_id));
            exit;
        }

        $data['page_data'] = $this->common_model->commonselect('tbl_pages_sub', 'sub_id', $sub_id)->row();

        $this->form_validation->set_rules('pg_title', 'page title', 'trim|required');

        if ($data['page_data']->sub_pg_url == $this->input->post('pg_url')) {
            $this->form_validation->set_rules('pg_url', 'page URL', 'trim|required|valid_url');
        } else {
            $this->form_validation->set_rules('pg_url', 'page URL', 'trim|required|valid_url|is_unique[tbl_pages_url.page_url]|callback_helper_url');
        }

        /* $this->form_validation->set_rules('pg_url', 'page title', 'trim|required|valid_url|callback_sub_url_unique[' . $sub_id . ']'); */
//        if ($sub_id != 5 && $sub_id != 6 && $sub_id != 9 && $sub_id != 10 && $sub_id != 14 && $sub_id != 15) {
        if ($sub_id != 15) {
            $this->form_validation->set_rules('pg_status', 'page Status', 'trim|required');
            /*            $this->form_validation->set_rules('pg_is_listing', 'is listing', 'trim|required'); */
        }
        $this->form_validation->set_rules('pg_description', 'Page description', 'trim|required');
        $this->form_validation->set_rules('meta_title', 'meta title', 'trim');
        $this->form_validation->set_rules('meta_keywords', 'meta keywords', 'trim');
        $this->form_validation->set_rules('meta_description', 'meta description', 'trim');

        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('valid_url', 'Please enter a valid URL.');

        if ((isset($_FILES['image']) || @$_FILES['image']['name'] != '' || @$_FILES['image']['type'] != '')) {
            //$this->form_validation->set_rules('hidden_file1', 'file1', 'trim');
            $this->form_validation->set_rules('file1', 'Image', 'trim|callback_valid_file');
        }

        $page_url_old = $data['page_data']->sub_pg_url;
        if ($this->form_validation->run() == false) {

            if (count($data['page_data']) <= 0) {
                redirect(base_url('admin/pages/sub_page_listing/' . $pg_id));
                exit;
            }
            $this->load->view('admin/pages/sub_edit_page', $data);
        } else {
            // echo '<pre>';
            // print_r($this->input->post('pg_title'));exit;
            $date = date('Y-m-d H:i:s');
            /////////////// upload multiypal image
            $myFiles1 = $_FILES['image'];
            $fileDP1 = array();
            if ($myFiles1['name'] !== "") {

                $config = array(
                    'allowed_types' => 'jpg|jpeg|gif|png',
                    'upload_path' => FCPATH . '/resources/page_image/',
                    'file_name' => 'sub_pg_img_' . date('m_d_Y') . '_' . time(),
                );
                $this->load->library('upload');
                $this->upload->initialize($config);
                //$this->upload->do_upload('file');
                if (!$this->upload->do_upload('image')) {
                    //Print message if file is not uploaded
                    $this->session->set_flashdata('msg', $this->upload->display_errors());
                    header("Location: " . base_url() . "admin/pages/edit_page/" . $pg_id . '/' . $sub_id);
                    exit();
                } else {
                    //////uplaod thumbnail image
                    $dataDP = $this->upload->data();
                    $fileDP1 = $dataDP['file_name'];
                    $this->load->library('image_lib');
                    $configt['image_library'] = 'gd2';
                    $configt['source_image'] = $dataDP['full_path'];
                    $configt['new_image'] = FCPATH . "/resources/page_image/thumb/";
                    $configt['maintain_ratio'] = false;
                    $configt['width'] = 80;
                    $configt['height'] = 80;
                    $this->image_lib->initialize($configt);
                    $this->image_lib->resize();
                }
            } else {
                $fileDP1 = $this->input->post('image');
            }
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            $page_url_new = $this->input->post('pg_url');

            $array = array(
                'sub_pg_title' => stripcslashes($this->input->post('pg_title')),
                'sub_pg_url' => $page_url_new, //$this->common_model->create_slug($this->input->post('pg_title'), 'tbl_pages_sub','sub_pg_url', 'sub_id', $sub_pg_id),
                'sub_pg_description' => stripcslashes($this->input->post('pg_description')),
                'sub_pg_image' => $fileDP1,
                //'sub_pg_status' => $this->input->post('pg_status'),
                'meta_title' => stripcslashes($this->input->post('meta_title')),
                'meta_keywords' => stripcslashes($this->input->post('meta_keywords')),
                'meta_description' => stripcslashes($this->input->post('meta_description')),
//                'sub_pg_is_listing' => $this->input->post('pg_is_listing'),
            );
            $array['update'] = date('Y-m-d H:i:s');
            if ($sub_id != 15) {
//            if ($sub_id != 5 && $sub_id != 6 && $sub_id != 9 && $sub_id != 10 && $sub_id != 14 && $sub_id != 15) {
                $array['sub_pg_status'] = $this->input->post('pg_status');
            }

            $url_array = array(
                'page_id' => $sub_id,
                'page_url' => $this->input->post('pg_url'),
                'page_table' => 'tbl_pages_sub',
            );

            $this->update_routes($page_url_old, $page_url_new);

            $this->common_model->commonUpdate('tbl_pages_sub', $array, 'sub_id', $sub_id);
            $this->common_model->commonUpdate('tbl_pages_url', $url_array, 'page_id', $sub_id);
            if ($sub_id != 15) {
                $activepage = about_side_menu_active();
                if ($array['sub_pg_status'] == 0) {

                    $pagedatadb['pg_url'] = $activepage->sub_pg_url;
                    $this->common_model->commonUpdate('tbl_pages', $pagedatadb, 'pg_id', 4);
                } else {
                    $pagedatadb['pg_url'] = $activepage->sub_pg_url;
                    $this->common_model->commonUpdate('tbl_pages', $pagedatadb, 'pg_id', 4);
                }
            }
            $this->session->set_flashdata('msg', 'Item updated successfully.');
            redirect(base_url('admin/pages/sub_page_listing/' . $pg_id));
            exit;
        }
    }

    public function delteSubPage($pg_id = '', $sub_id = '') {

        if ($pg_id == '' || !is_numeric($pg_id)) {
            redirect(base_url('admin/pages'));
            exit;
        }
        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $pg_id)->row();
        if (count($data['main_pg']) <= 0) {
            redirect(base_url("admin/pages"));
            exit;
        }
        if ($sub_id == '' || !is_numeric($sub_id)) {
            redirect(base_url('admin/pages/sub_page_listing/' . $pg_id));
            exit;
        }

        $this->common_model->commonDelete('tbl_pages_sub', "sub_id", $sub_id);
        $this->common_model->commonDelete('tbl_pages_url', "page_id", $sub_id);
        $this->session->set_flashdata('msg', 'Item deleted successfully.');
        redirect(base_url('admin/pages/sub_page_listing/' . $pg_id));
        exit;
    }

    public function delete_sub_page_img() {
        if (!$this->input->is_ajax_request()) {
            
        } else {
            $id = $this->input->post('nId');
            $data = array("sub_pg_image" => "");
            $this->db->where('sub_id', $id);
            $this->db->update('tbl_pages_sub', $data);
        }
    }

    /*     * *************************************************************************
      ----------------------------------------------------------------------------
      Sub Sub pages listing add and edit
      ----------------------------------------------------------------------------
     * ************************************************************************** */

    public function sub_sub_page_listing($pg_id = '', $sub_id = '') {

        if ($pg_id == '' || !is_numeric($pg_id)) {
            redirect(base_url('admin/pages'));
            exit;
        }
        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $pg_id)->row();
        if (count($data['main_pg']) <= 0) {
            redirect(base_url("admin/pages"));
            exit;
        }

        if ($sub_id == '' || !is_numeric($sub_id)) {
            redirect(base_url('admin/pages/sub_page_listing/' . $pg_id));
            exit;
        }
        $data['sub_pg'] = $this->common_model->commonselect2('tbl_pages_sub', 'sub_pg_id', $pg_id, 'sub_id', $sub_id)->row();
        if (count($data['sub_pg']) <= 0) {
            redirect(base_url('admin/pages/sub_page_listing/' . $pg_id));
            exit;
        }

        $data['pageList'] = $this->common_model->commonselect('tbl_pages_sub_sub', 'sub_pg_id', $sub_id, 'sub_sub_id', "ASC")->result();
        $this->load->view('admin/pages/sub_sub_page_list', $data);
    }

    public function add_sub_sub_page($pg_id = '', $sub_id = '') {

        if ($pg_id == '' || !is_numeric($pg_id)) {
            redirect(base_url('admin/pages'));
            exit;
        }
        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $pg_id)->row();
        if (count($data['main_pg']) <= 0) {
            redirect(base_url("admin/pages"));
            exit;
        }

        if ($sub_id == '' || !is_numeric($sub_id)) {
            redirect(base_url('admin/pages/sub_page_listing/' . $pg_id));
            exit;
        }
        $data['sub_pg'] = $this->common_model->commonselect2('tbl_pages_sub', 'sub_pg_id', $pg_id, 'sub_id', $sub_id)->row();
        if (count($data['sub_pg']) <= 0) {
            redirect(base_url('admin/pages/sub_page_listing/' . $pg_id));
            exit;
        }

        $data['title'] = "Add New Question";
        $this->form_validation->set_rules('pg_title', 'page title', 'trim|required');
        /* $this->form_validation->set_rules('pg_url', 'page title', 'trim|required|valid_url|is_unique[tbl_pages_sub_sub.sub_sub_pg_url]'); */
        $this->form_validation->set_rules('pg_description', 'Page description', 'trim|required');
        $this->form_validation->set_rules('pg_status', 'page title', 'trim|required');
        $this->form_validation->set_rules('meta_title', 'meta title', 'trim');
        $this->form_validation->set_rules('meta_keywords', 'meta keywords', 'trim');
        $this->form_validation->set_rules('meta_description', 'meta description', 'trim');

        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('valid_url', 'Please enter a valid URL.');
        $this->form_validation->set_message('is_unique', 'This URL already exists.');

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/pages/sub_sub_add_page', $data);
        } else {
            $date = date('Y-m-d H:i:s');
            /* /////////////// upload multiypal image
              $myFiles1 = $_FILES['image'];
              $fileDP1  = array();
              if ($myFiles1['name'] !== "") {

              $config = array(
              'allowed_types' => 'jpg|jpeg|gif|png',
              'upload_path'   => FCPATH . '/resources/page_image/',
              'file_name'     => 'sub_pg_img_' . date('m_d_Y') . '_' . time(),
              );
              $this->load->library('upload');
              $this->upload->initialize($config);
              //$this->upload->do_upload('file');
              if (!$this->upload->do_upload('image')) {
              //Print message if file is not uploaded
              $this->session->set_flashdata('msg', $this->upload->display_errors());
              header("Location: " . base_url() . "admin/pages/add_sub_page/" . $pg_id);
              exit();
              } else {
              //////uplaod thumbnail image
              $dataDP  = $this->upload->data();
              $fileDP1 = $dataDP['file_name'];
              $this->load->library('image_lib');
              $configt['image_library']  = 'gd2';
              $configt['source_image']   = $dataDP['full_path'];
              $configt['new_image']      = FCPATH . "/resources/page_image/thumb/";
              $configt['maintain_ratio'] = false;
              $configt['width']          = 80;
              $configt['height']         = 80;
              $this->image_lib->initialize($configt);
              $this->image_lib->resize();
              }
              } else {
              $fileDP1 = $this->input->post('image');
              } */
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            $array = array(
                'sub_sub_pg_title' => stripcslashes($this->input->post('pg_title')),
                'sub_sub_pg_description' => stripcslashes($this->input->post('pg_description')),
                'sub_sub_pg_status' => stripcslashes($this->input->post('pg_status')),
                'meta_title' => stripcslashes($this->input->post('meta_title')),
                'meta_keywords' => stripcslashes($this->input->post('meta_keywords')),
                'meta_description' => stripcslashes($this->input->post('meta_description')),
                'sub_pg_id' => $sub_id,
                'sub_sub_pg_admin_id' => stripcslashes($this->session->userdata('admin_id')),
                'date_created' => date('Y-m-d H:i:s'),
            );
            $array['date'] = date('Y-m-d H:i:s');

            $this->common_model->commonSave('tbl_pages_sub_sub', $array);
            $this->session->set_flashdata('msg', 'Item added successfully.');
            redirect(base_url() . "admin/pages/sub_sub_page_listing/" . $pg_id . '/' . $sub_id);
        }
    }

    public function sub_sub_url_unique($url, $pg_id) {
        $check = $this->db->query("SELECT * FROM tbl_pages_sub_sub WHERE sub_sub_pg_url = '" . $url . "' AND sub_sub_is_delete = 0 AND sub_sub_id != " . $pg_id)->row();
        if (count($check) > 0) {
            $this->form_validation->set_message('sub_sub_url_unique', 'This URL already exists.');
            return false;
        } else {
            return true;
        }
    }

    public function edit_work_page($id, $nId = 4) {
        $data['title'] = "Edit How Does Work Page";
        if ($id == '' || !is_numeric($id)) {
            redirect(base_url('admin/pages'));
            exit;
        }

        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $nId)->row();
        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $nId)->row();
        $data['work_data'] = $this->common_model->commonSelect('tbl_how_work_page', 'work_id', $id)->row();

        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('page_title', 'Page Title', 'trim|required');
        /* $this->form_validation->set_rules('banner_title', 'Banner Title', 'trim|required'); */
        $this->form_validation->set_rules('desc1', 'Description', 'trim|required');
        $this->form_validation->set_rules('desc2', 'Description', 'trim|required');
        $this->form_validation->set_rules('desc3', 'Description', 'trim|required');
        $this->form_validation->set_rules('desc4', 'Description', 'trim|required');
        $this->form_validation->set_rules('meta_title', 'meta title', 'trim');
        $this->form_validation->set_rules('meta_keywords', 'meta keyword', 'trim');
        $this->form_validation->set_rules('meta_description', 'meta description', 'trim');
        $this->form_validation->set_rules('work_status', 'status', 'trim');

        if ((isset($_FILES['file1']) || @$_FILES['file1']['name'] != '' || @$_FILES['file1']['type'] != '')) {
            $this->form_validation->set_rules('hidden_file1', 'file1', 'trim|callback_valid_file');
        }

        if ((isset($_FILES['file2']) || @$_FILES['file2']['name'] != '' || @$_FILES['file2']['type'] != '')) {
            $this->form_validation->set_rules('hidden_file2', 'file2', 'trim|callback_valid_file');
        }

        if ((isset($_FILES['file3']) || @$_FILES['file3']['name'] != '' || @$_FILES['file3']['type'] != '')) {
            $this->form_validation->set_rules('hidden_file3', 'file3', 'trim|callback_valid_file');
        }

        if ((isset($_FILES['file4']) || @$_FILES['file4']['name'] != '' || @$_FILES['file4']['type'] != '')) {
            $this->form_validation->set_rules('hidden_file4', 'file4', 'trim|callback_valid_file');
        }

        if ((isset($_FILES['file5']) || @$_FILES['file5']['name'] != '' || @$_FILES['file5']['type'] != '')) {
            $this->form_validation->set_rules('hidden_file5', 'file5', 'trim|callback_valid_file');
        }

        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('valid_url', 'Please enter a valid URL.');
        $this->form_validation->set_message('valid_file', 'The file type you are attempting to upload is not allowed.');

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/pages/how_does_work_edit', $data);
        } else {
            /* echo "<pre>";
              print_r($data);
              print_r($_FILES);
              //var_dump($data);
              exit; */
            $arrImg = '';
            $j = 0;
            for ($j = 0; $j < 5; $j++) {
                #$totalFiles
                if (@$_FILES['file' . ($j + 1)]['name'] == '' && $this->input->post('hidden_file' . ($j + 1)) != '') {
                    $arrImg[$j] = $this->input->post('hidden_file' . ($j + 1));
                } else {
                    if (isset($_FILES['file' . ($j + 1)])) {
                        unlink('resources/work_page_images/thumb/thumb_' . $this->input->post('hidden_file' . ($j + 1) . 'old'));
                        unlink('resources/work_page_images/' . $this->input->post('hidden_file' . ($j + 1) . 'old'));
                        $myFiles = $_FILES['file' . ($j + 1)];

                        $_FILES['file' . ($j + 1)]['name'] = $myFiles['name'];
                        $_FILES['file' . ($j + 1)]['type'] = $myFiles['type'];
                        $_FILES['file' . ($j + 1)]['tmp_name'] = $myFiles['tmp_name'];
                        $_FILES['file' . ($j + 1)]['error'] = $myFiles['error'];
                        $_FILES['file' . ($j + 1)]['size'] = $myFiles['size'];

                        $file_name = $myFiles['name'];

                        $file_ext = explode('.', $file_name);
                        $file_extension = end($file_ext);

                        $image_path = $myFiles['tmp_name'];

                        $this->load->library('upload');
                        $file_Post = '';

                        if ($myFiles['name'] != '') {
                            $new_prod_image_name = "img_" . rand(1, 1000) . time();

                            $arrImg[$j] = $new_prod_image_name_ext = $new_prod_image_name . '.' . $file_extension;

                            //If user selected the photo to upload .
                            $uploaddir_11 = FCPATH . "resources/work_page_images/";
                            $uploaddir_11 = str_replace(" ", "", $uploaddir_11);
                            $config_11['upload_path'] = $uploaddir_11;
                            $config_11['allowed_types'] = 'jpg|jpeg|gif|png|JPG|JPEG|GIF|PNG';
                            $config_11['overwrite'] = false;
                            $config_11['remove_spaces'] = true;
                            $config_11['file_name'] = $new_prod_image_name_ext;

                            $this->upload->initialize($config_11);
                            if (!$this->upload->do_upload('file' . ($j + 1))) {
                                $this->session->set_flashdata('msg' . $j, $this->upload->display_errors());
                                redirect(base_url() . "admin/pages/sub_page_listing/" . $nId);
                                exit();
                            } else {
                                $uploadthumb = FCPATH . "resources/work_page_images/thumb/";
                                $uploadthumb = str_replace(" ", "", $uploadthumb);

                                # page
                                $width = '80';
                                $height = '80';
                                $thumb_image_name = 'thumb_' . $new_prod_image_name_ext;
                                ratio_image_resize($image_path, $uploadthumb, $thumb_image_name, $width, $height);
                                $config_thumb['upload_path'] = $uploadthumb;
                                $this->upload->initialize($config_thumb);
                                $dataDP = $this->upload->data();
                            } //else upload END
                        } //if (file[name] != '') END
                    } //if isset(file) END
                }
            } //for loop END

            $array['work_image_1'] = $arrImg[0];
            $array['work_image_2'] = $arrImg[1];
            $array['work_image_3'] = $arrImg[2];
            $array['work_image_4'] = $arrImg[3];
            $array['work_banner'] = $arrImg[4];
            $array['work_title'] = stripcslashes($this->input->post('title'));
            $array['work_page_title'] = stripcslashes($this->input->post('page_title'));
            /* $array['banner_title']    = $this->input->post('banner_title'); */
            $array['work_desc_1'] = stripcslashes($this->input->post('desc1'));
            $array['work_desc_2'] = stripcslashes($this->input->post('desc2'));
            $array['work_desc_3'] = stripcslashes($this->input->post('desc3'));
            $array['work_desc_4'] = stripcslashes($this->input->post('desc4'));
            $array['meta_title'] = stripcslashes($this->input->post('meta_title'));
            $array['meta_keywords'] = stripcslashes($this->input->post('meta_keywords'));
            $array['meta_desc'] = stripcslashes($this->input->post('meta_description'));
            $array['work_status'] = stripcslashes($this->input->post('work_status'));

            $datadb['sub_pg_title'] = $this->input->post('page_title');
            $datadb['sub_pg_status'] = $this->input->post('work_status');
            $datadb['update'] = $array['update'] = date('Y-m-d H:i:s');


            $this->common_model->commonUpdate('tbl_how_work_page', $array, 'work_id', $id);
            $this->common_model->commonUpdate('tbl_pages_sub', $datadb, 'sub_id', 5);
            $activepage = about_side_menu_active();

//          var_dump($datadb['sub_pg_status']);die;

            if ($datadb['sub_pg_status'] == 0) {

                $pagedatadb['pg_url'] = $activepage->sub_pg_url;
                $this->common_model->commonUpdate('tbl_pages', $pagedatadb, 'pg_id', 4);
            } else {
                $pagedatadb['pg_url'] = 'what-is-educki';
                $this->common_model->commonUpdate('tbl_pages', $pagedatadb, 'pg_id', 4);
            }


            $this->session->set_flashdata('msg', 'Item updated successfully.');
            redirect(base_url() . "admin/pages/sub_page_listing/" . $nId);
            exit;
        }
    }

    private function file_upload($Files1, $field_name) {

        $filename = array();
        if ($Files1['name'] !== "") {
            $config = array(
                'allowed_types' => 'pdf', //doc|docx|ppt|ppt|txt|
                'upload_path' => FCPATH . 'resources/contact_form_files/',
                'file_name' => 'file_' . date('Y_m_d_h_i_s'),
            );
            $this->load->library('upload');
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($field_name)) {

                //Print message if file is not uploaded
                $data = array($field_name . '_error' => $this->upload->display_errors());
                return false;
            } else {
                $dataDP = $this->upload->data();
                return $filename = $dataDP['file_name'];
            }
        }
    }

    public function edit_sub_sub_page($pg_id = '', $sub_id = '', $sub_sub_id = '') {
        if ($pg_id == '' || !is_numeric($pg_id)) {
            redirect(base_url('admin/pages'));
            exit;
        }
        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $pg_id)->row();
        if (count($data['main_pg']) <= 0) {
            redirect(base_url("admin/pages"));
            exit;
        }
        if ($sub_id == '' || !is_numeric($sub_id)) {
            redirect(base_url('admin/pages/sub_page_listing/' . $pg_id));
            exit;
        }
        $data['sub_pg'] = $this->common_model->commonselect2('tbl_pages_sub', 'sub_pg_id', $pg_id, 'sub_id', $sub_id)->row();
        if (count($data['sub_pg']) <= 0) {
            redirect(base_url('admin/pages/sub_page_listing/' . $pg_id));
            exit;
        }
        if ($sub_sub_id == '' || !is_numeric($sub_sub_id)) {
            redirect(base_url('admin/pages/sub_sub_page_listing/' . $pg_id . '/' . $sub_id));
            exit;
        }

        $this->form_validation->set_rules('pg_title', 'page title', 'trim|required');
        /* $this->form_validation->set_rules('pg_url', 'page title', 'trim|required|valid_url|callback_sub_sub_url_unique[' . $sub_sub_id . ']'); */
        $this->form_validation->set_rules('pg_description', 'Page description', 'trim|required');
        $this->form_validation->set_rules('pg_status', 'page title', 'trim|required');
        $this->form_validation->set_rules('meta_title', 'meta title', 'trim');
        $this->form_validation->set_rules('meta_keywords', 'meta keywords', 'trim');
        $this->form_validation->set_rules('meta_description', 'meta description', 'trim');

        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('valid_url', 'Please enter a valid URL.');

        if ($this->form_validation->run() == false) {
            $data['page_data'] = $this->common_model->commonselect2('tbl_pages_sub_sub', 'sub_sub_id', $sub_sub_id, 'sub_pg_id', $sub_id)->row();
            if (count($data['page_data']) <= 0) {
                redirect(base_url('admin/pages/sub_sub_page_listing/' . $pg_id . '/' . $sub_id));
                exit;
            }
            $this->load->view('admin/pages/sub_sub_edit_page', $data);
        } else {
            $date = date('Y-m-d H:i:s');
            /* /////////////// upload multiypal image
              $myFiles1 = $_FILES['image'];
              $fileDP1  = array();
              if ($myFiles1['name'] !== "") {
              $config = array(
              'allowed_types' => 'jpg|jpeg|gif|png',
              'upload_path'   => FCPATH . '/resources/page_image/',
              'file_name'     => 'sub_pg_img_' . date('m_d_Y') . '_' . time(),
              );
              $this->load->library('upload');
              $this->upload->initialize($config);
              //$this->upload->do_upload('file');
              if (!$this->upload->do_upload('image')) {
              //Print message if file is not uploaded
              $this->session->set_flashdata('msg', $this->upload->display_errors());
              header("Location: " . base_url() . "admin/pages/edit_page/" . $pg_id . '/' . $sub_id);
              exit();
              } else {
              //////upload thumbnail image
              $dataDP  = $this->upload->data();
              $fileDP1 = $dataDP['file_name'];
              $this->load->library('image_lib');
              $configt['image_library']  = 'gd2';
              $configt['source_image']   = $dataDP['full_path'];
              $configt['new_image']      = FCPATH . "/resources/page_image/thumb/";
              $configt['maintain_ratio'] = false;
              $configt['width']          = 80;
              $configt['height']         = 80;
              $this->image_lib->initialize($configt);
              $this->image_lib->resize();
              }
              } else {
              $fileDP1 = $this->input->post('image');
              } */
            //////////////////////////////////////////////////////////////////////////////////////////////////////////
            $array = array(
                'sub_sub_pg_title' => stripcslashes($this->input->post('pg_title')),
                'sub_sub_pg_description' => stripcslashes($this->input->post('pg_description')),
                'sub_sub_pg_status' => stripcslashes($this->input->post('pg_status')),
                'meta_title' => stripcslashes($this->input->post('meta_title')),
                'meta_keywords' => stripcslashes($this->input->post('meta_keywords')),
                'meta_description' => stripcslashes($this->input->post('meta_description')),
            );
            $array['update'] = date('Y-m-d H:i:s');

            $this->common_model->commonUpdate('tbl_pages_sub_sub', $array, 'sub_sub_id', $sub_sub_id);
            $this->session->set_flashdata('msg', 'Item updated successfully.');
            redirect(base_url('admin/pages/sub_sub_page_listing/' . $pg_id . '/' . $sub_id));
            exit;
        }
    }

    public function delteSubSubPage($pg_id = '', $sub_id = '', $sub_sub_id) {

        if ($pg_id == '' || !is_numeric($pg_id)) {
            redirect(base_url('admin/pages'));
            exit;
        }
        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $pg_id)->row();
        if (count($data['main_pg']) <= 0) {
            redirect(base_url("admin/pages"));
            exit;
        }
        if ($sub_id == '' || !is_numeric($sub_id)) {
            redirect(base_url('admin/pages/sub_page_listing/' . $pg_id));
            exit;
        }
        $data['sub_pg'] = $this->common_model->commonselect2('tbl_pages_sub', 'sub_pg_id', $pg_id, 'sub_id', $sub_id)->row();
        if (count($data['sub_pg']) <= 0) {
            redirect(base_url('admin/pages/sub_page_listing/' . $pg_id));
            exit;
        }
        if ($sub_sub_id == '' || !is_numeric($sub_sub_id)) {
            redirect(base_url('admin/pages/sub_sub_page_listing/' . $pg_id . '/' . $sub_id));
            exit;
        }

        $this->common_model->commonDelete('tbl_pages_sub_sub', "sub_sub_id", $sub_sub_id);
        $this->session->set_flashdata('msg', 'Item deleted successfully.');
        redirect(base_url('admin/pages/sub_sub_page_listing/' . $pg_id . '/' . $sub_id));
        exit;
    }

    public function delete_sub_sub_page_img() {
        if (!$this->input->is_ajax_request()) {
            
        } else {
            $id = $this->input->post('nId');
            $data = array("sub_pg_image" => "");
            $this->db->where('sub_sub_id', $id);
            $this->db->update('tbl_pages_sub_sub', $data);
        }
    }

    public function homepage($id = 1) {
        $data['title'] = "Edit Homepage";
        if ($id == '' || !is_numeric($id)) {
            redirect(base_url('admin/pages'));
            exit;
        }

        $this->form_validation->set_rules('category_title', 'category title', 'trim|required');
        $this->form_validation->set_rules('feaSeller', 'seller', 'trim|required');
        $this->form_validation->set_rules('searchtitle', 'search title', 'trim|required');
        $this->form_validation->set_rules('searchtext', 'search description', 'trim|required');
        $this->form_validation->set_rules('searchButton', 'search button', 'trim|required');
        $this->form_validation->set_rules('gettingStartTitle', 'getting start title', 'trim|required');
        $this->form_validation->set_rules('startTitle1', 'image title 1', 'trim|required');
        $this->form_validation->set_rules('startTitle2', 'image title 2', 'trim|required');
        $this->form_validation->set_rules('startTitle3', 'image title 3', 'trim|required');
        $this->form_validation->set_rules('membertitle', 'member title', 'trim|required');
        $this->form_validation->set_rules('memberdesc', 'member title', 'trim|required');
        $this->form_validation->set_rules('meta_title', 'meta title', 'trim');
        $this->form_validation->set_rules('meta_keywords', 'meta keyword', 'trim');
        $this->form_validation->set_rules('meta_description', 'meta description', 'trim');
        /*
          $this->form_validation->set_rules('startUrl', 'start url', 'trim|required');
          $this->form_validation->set_rules('signupTitle', 'newsletter title','trim|required');
          $this->form_validation->set_rules('signupDesc', 'newsletter description','trim|required');
         */
        if ((isset($_FILES['file1']) || @$_FILES['file1']['name'] != '' || @$_FILES['file1']['type'] != '')) {
            $this->form_validation->set_rules('hidden_file1', 'file1', 'trim|required|callback_valid_file');
        }
        if ((isset($_FILES['file2']) || @$_FILES['file2']['name'] != '' || @$_FILES['file2']['type'] != '')) {
            $this->form_validation->set_rules('hidden_file2', 'file2', 'trim|required|callback_valid_file');
        }
        if ((isset($_FILES['file3']) || @$_FILES['file3']['name'] != '' || @$_FILES['file3']['type'] != '')) {
            $this->form_validation->set_rules('hidden_file3', 'file3', 'trim|required|callback_valid_file');
        }

        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('valid_url', 'Please enter a valid URL.');

        if ($this->form_validation->run() == false) {
            $data['home_data'] = $this->common_model->commonSelect('tbl_homepage', 'home_id', 1)->row();
            $this->load->view('admin/pages/homepage_edit', $data);
        } else {
            $arrImg = '';
            $j = 0;
            for ($j = 0; $j < 3; $j++) {
#$totalFiles
                if (@$_FILES['file' . ($j + 1)]['name'] == '' && $this->input->post('hidden_file' . ($j + 1)) != '') {
                    $arrImg[$j] = $this->input->post('hidden_file' . ($j + 1));
                } else {
                    if (isset($_FILES['file' . ($j + 1)])) {
                        $myFiles = $_FILES['file' . ($j + 1)];

                        $_FILES['file' . ($j + 1)]['name'] = $myFiles['name'];
                        $_FILES['file' . ($j + 1)]['type'] = $myFiles['type'];
                        $_FILES['file' . ($j + 1)]['tmp_name'] = $myFiles['tmp_name'];
                        $_FILES['file' . ($j + 1)]['error'] = $myFiles['error'];
                        $_FILES['file' . ($j + 1)]['size'] = $myFiles['size'];

                        $file_name = $myFiles['name'];

                        $file_ext = explode('.', $file_name);
                        $file_extension = end($file_ext);

                        $image_path = $myFiles['tmp_name'];

                        $this->load->library('upload');
                        $file_Post = '';

                        if ($myFiles['name'] != '') {
                            $new_prod_image_name = "img_" . rand(1, 1000) . time();

                            $arrImg[$j] = $new_prod_image_name_ext = $new_prod_image_name . '.' . $file_extension;

                            //If user selected the photo to upload .
                            $uploaddir_11 = FCPATH . "resources/home_image/";
                            $uploaddir_11 = str_replace(" ", "", $uploaddir_11);
                            $config_11['upload_path'] = $uploaddir_11;
                            $config_11['allowed_types'] = 'jpg|jpeg|gif|png|JPG|JPEG|GIF|PNG';
                            $config_11['overwrite'] = false;
                            $config_11['remove_spaces'] = true;
                            $config_11['file_name'] = $new_prod_image_name_ext;

                            $this->upload->initialize($config_11);
                            if (!$this->upload->do_upload('file' . ($j + 1))) {
                                $this->session->set_flashdata('msg_err' . $j, $this->upload->display_errors());
                                redirect(base_url() . "admin/pages/homepage/" . $id);
                                exit();
                            } else {
                                $uploadthumb = FCPATH . "resources/home_image/thumb/";
                                $uploadthumb = str_replace(" ", "", $uploadthumb);

                                # page
                                $width = '80';
                                $height = '';
                                $thumb_image_name = 'thumb_' . $new_prod_image_name_ext;
                                ratio_image_resize($image_path, $uploadthumb, $thumb_image_name, $width, $height);
                                $config_thumb['upload_path'] = $uploadthumb;
                                $this->upload->initialize($config_thumb);
                                $dataDP = $this->upload->data();
                            } //else upload END
                        } //if (file[name] != '') END
                    } //if isset(file) END
                }
            } //for loop END
            $array['start_image1'] = $arrImg[0];
            $array['start_image2'] = $arrImg[1];
            $array['start_image3'] = $arrImg[2];
            $array['category_title'] = stripcslashes($this->input->post('category_title'));
            $array['feaSeller'] = stripcslashes($this->input->post('feaSeller'));
            $array['searchtitle'] = stripcslashes($this->input->post('searchtitle'));
            $array['searchtext'] = stripcslashes($this->input->post('searchtext'));
            $array['searchButton'] = stripcslashes($this->input->post('searchButton'));
            $array['gettingStartTitle'] = stripcslashes($this->input->post('gettingStartTitle'));
            $array['startTitle1'] = stripcslashes($this->input->post('startTitle1'));
            $array['startTitle2'] = stripcslashes($this->input->post('startTitle2'));
            $array['startTitle3'] = stripcslashes($this->input->post('startTitle3'));
            $array['membertitle'] = stripcslashes($this->input->post('membertitle'));
            $array['memberdesc'] = stripcslashes($this->input->post('memberdesc'));
            $array['meta_title'] = stripcslashes($this->input->post('meta_title'));
            $array['meta_keywords'] = stripcslashes($this->input->post('meta_keywords'));
            $array['meta_description'] = stripcslashes($this->input->post('meta_description'));
           $array2['update'] = $array['update'] = date('Y-m-d H:i:s');
            $this->common_model->commonUpdate('tbl_pages', $array2, 'pg_id', 1);
            $this->common_model->commonUpdate('tbl_homepage', $array, 'home_id', $id);
            $this->session->set_flashdata('msg', 'Item updated successfully.');
            redirect(base_url('admin/pages'));
            exit;
        }
    }

    public function valid_file($name) {
        if ($name != "" && !empty($name) && $name != "Upload File") {
            $ext = explode('.', $name);
            $ext = strtolower(end($ext));
            if ($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "png") {
                return true;
            } else {
                $this->form_validation->set_message('valid_file', 'The file type you are attempting to upload is not allowed.');
                return false;
            }
        } else {
            $this->form_validation->set_message('valid_file', 'This field is required.');
            return false;
        }
    }

    public function homepage_banner() {
        $data['images'] = $this->common_model->getCombox('tbl_homepage_banner', 'banner_id', "ASC");
        $this->load->view('admin/pages/homepage_banner_list', $data);
    }

    public function image_delete($id) {
        if (!is_numeric($id)) {
            redirect(base_url() . "admin/pages/homepage_banner/1");
            exit;
        }
        $this->data['data'] = $this->common_model->commonselect('tbl_homepage_banner', 'banner_id', $id);
        if ($this->data['data']->num_rows() == 0) {
            $this->session->set_flashdata('msg', 'No Record Found.');
            redirect(base_url() . "admin/pages/homepage_banner/1");
        } else {
            foreach ($this->data['data']->result() as $row) {
                @unlink($_SERVER['DOCUMENT_ROOT'] . '/educki/resources/home_image/' . $row->banner_image);
                @unlink($_SERVER['DOCUMENT_ROOT'] . '/educki/resources/home_image/thumb/' . $row->banner_image);
            }
        }

        $this->common_model->commonDelete('tbl_homepage_banner', 'banner_id', $id);

        $this->session->set_flashdata('msg', 'Item deleted successfully.');
        redirect(base_url() . "admin/pages/homepage_banner/1");
        exit;
    }

    public function gallery_upload() {
        $path = $_SERVER['DOCUMENT_ROOT'] . '/educki/js/filer/php/class.uploader.php';
        include $path;
        $uploader = new Uploader();
        $data = $uploader->upload($_FILES['files'], array(
            'limit' => 10, //Maximum Limit of files. {null, Number}
            'maxSize' => 10, //Maximum Size of files {null, Number(in MB's)}
            'extensions' => array('jpeg', 'jpg', 'gif', 'png', 'JPEG', 'JPG', 'GIF', 'PNG'), //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
            'required' => false, //Minimum one file is required for upload {Boolean}
            'uploadDir' => $_SERVER['DOCUMENT_ROOT'] . '/educki/resources/home_image/', //Upload directory {String}
            'title' => array('auto'), //New file name {null, String, Array} *please read documentation in README.md
            'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
            'replace' => false, //Replace the file if it already exists  {Boolean}
            'perms' => null, //Uploaded file permisions {null, Number}
            'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
            'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
            'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
            'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
            'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
            'onRemove' => null, //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
        ));

        if ($data['isComplete']) {
            $files = $data['data'];
            $this->load->library('image_lib');
            $configt['image_library'] = 'gd2';
            $configt['source_image'] = $files['metas'][0]['file'];
            $configt['new_image'] = $_SERVER['DOCUMENT_ROOT'] . '/educki/resources/home_image/thumb/';
            $configt['maintain_ratio'] = true;
            $configt['width'] = 80;
            $configt['height'] = 60;
            $this->image_lib->initialize($configt);
            $this->image_lib->resize();

            $dbData['banner_image'] = $files['metas'][0]['name'];
            //$dbData['date_created'] = date('Y-m-d H:i:s');

            $this->db->insert('tbl_homepage_banner', $dbData);
            $insertId = $this->db->insert_id();
            $files['metas'][0]['image_id'] = $insertId;
            echo json_encode($files['metas'][0]);
        }

        if ($data['hasErrors']) {
            $errors = $data['errors'];
            echo json_encode($errors);
        }

        //print_r($this->input->post());exit;
    }

    public function homepage_member() {
        $data['pageList'] = $this->common_model->getCombox('tbl_homepage_member', 'mem_id', "ASC")->result();
        $this->load->view('admin/pages/member-list', $data);
    }

    public function add_member() {

        $data['title'] = "Add New Member";
        $this->form_validation->set_rules('mem_description', 'description', 'trim|required');

        $this->form_validation->set_rules('hidden_file1', 'file1', 'trim|required|callback_valid_file');

        /* if(!empty($_FILES['file1']['name'])){
          $this->form_validation->set_rules('hidden_file1', 'file1', 'trim|required|callback_valid_file');
          }
          else{
          $this->form_validation->set_rules('hidden_file1', 'file1', 'trim|required');
          } */

        $this->form_validation->set_message('required', 'This field is required.');

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/pages/member_add', $data);
        } else {
            $arrImg = '';
            $j = 0;
            for ($j = 0; $j < 1; $j++) {
#$totalFiles
                $myFiles = $_FILES['file' . ($j + 1)];

                $_FILES['file' . ($j + 1)]['name'] = $myFiles['name'];
                $_FILES['file' . ($j + 1)]['type'] = $myFiles['type'];
                $_FILES['file' . ($j + 1)]['tmp_name'] = $myFiles['tmp_name'];
                $_FILES['file' . ($j + 1)]['error'] = $myFiles['error'];
                $_FILES['file' . ($j + 1)]['size'] = $myFiles['size'];

                $file_name = $myFiles['name'];

                $file_ext = explode('.', $file_name);
                $file_extension = end($file_ext);

                $image_path = $myFiles['tmp_name'];

                $this->load->library('upload');
                $file_Post = '';

                if ($myFiles['name'] != '') {
                    $new_prod_image_name = "member_" . rand(1, 1000) . time();

                    $arrImg[$j] = $new_prod_image_name_ext = $new_prod_image_name . '.' . $file_extension;

                    //If user selected the photo to upload .
                    $uploaddir_11 = FCPATH . "resources/member_image/";
                    $uploaddir_11 = str_replace(" ", "", $uploaddir_11);
                    $config_11['upload_path'] = $uploaddir_11;
                    $config_11['allowed_types'] = 'jpg|jpeg|gif|png|JPG|JPEG|GIF|PNG';
                    $config_11['overwrite'] = false;
                    $config_11['remove_spaces'] = true;
                    $config_11['file_name'] = $new_prod_image_name_ext;

                    $this->upload->initialize($config_11);
                    if (!$this->upload->do_upload('file' . ($j + 1))) {
                        $this->session->set_flashdata('msg_err' . $j, $this->upload->display_errors());
                        redirect(base_url() . "admin/pages/add_member");
                        exit();
                    } else {
                        $uploadthumb = FCPATH . "resources/member_image/thumb/";
                        $uploadthumb = str_replace(" ", "", $uploadthumb);

                        # page
                        $width = '80';
                        $height = '';
                        $thumb_image_name = 'thumb_' . $new_prod_image_name_ext;
                        ratio_image_resize($image_path, $uploadthumb, $thumb_image_name, $width, $height);
                        $config_thumb['upload_path'] = $uploadthumb;
                        $this->upload->initialize($config_thumb);
                        $dataDP = $this->upload->data();
                    } //else upload END
                } //if (file[name] != '') END
            } //for loop END
            $array['mem_image'] = $arrImg[0];
            $array['mem_description'] = stripcslashes($this->input->post('mem_description'));
$array['date'] = date('Y-m-d H:i:s');
            $this->common_model->commonSave('tbl_homepage_member', $array);
            $this->session->set_flashdata('msg', 'Item added successfully.');
            redirect(base_url() . "admin/pages/homepage_member");
        }
    }

    public function member_edit($mem_id = '') {
        if ($mem_id == '' || !is_numeric($mem_id)) {
            redirect(base_url('admin/pages/homepage_member'));
            exit;
        }
        $data['title'] = "Edit Member";
        $this->form_validation->set_rules('mem_description', 'description', 'trim|required');
        $this->form_validation->set_rules('hidden_file1', 'file1', 'trim|required|callback_valid_file');

        $this->form_validation->set_message('required', 'This field is required.');

        $data['mem_data'] = $this->common_model->commonselect('tbl_homepage_member', 'mem_id', $mem_id)->row();

        if (count($data['mem_data']) <= 0) {
            redirect(base_url('admin/pages/homepage_member'));
            exit;
        }
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/pages/member_edit', $data);
        } else {
            $arrImg = '';
            $j = 0;
            for ($j = 0; $j < 1; $j++) {
#$totalFiles
                if (@$_FILES['file' . ($j + 1)]['name'] == '' && $this->input->post('hidden_file' . ($j + 1)) != '') {
                    $arrImg[$j] = $this->input->post('hidden_file' . ($j + 1));
                } else {
                    if (isset($_FILES['file' . ($j + 1)])) {
                        $myFiles = $_FILES['file' . ($j + 1)];

                        $_FILES['file' . ($j + 1)]['name'] = $myFiles['name'];
                        $_FILES['file' . ($j + 1)]['type'] = $myFiles['type'];
                        $_FILES['file' . ($j + 1)]['tmp_name'] = $myFiles['tmp_name'];
                        $_FILES['file' . ($j + 1)]['error'] = $myFiles['error'];
                        $_FILES['file' . ($j + 1)]['size'] = $myFiles['size'];

                        $file_name = $myFiles['name'];

                        $file_ext = explode('.', $file_name);
                        $file_extension = end($file_ext);

                        $image_path = $myFiles['tmp_name'];

                        $this->load->library('upload');
                        $file_Post = '';

                        if ($myFiles['name'] != '') {
                            $new_prod_image_name = "img_" . rand(1, 1000) . time();

                            $arrImg[$j] = $new_prod_image_name_ext = $new_prod_image_name . '.' . $file_extension;

                            //If user selected the photo to upload .
                            $uploaddir_11 = FCPATH . "resources/member_image/";
                            $uploaddir_11 = str_replace(" ", "", $uploaddir_11);
                            $config_11['upload_path'] = $uploaddir_11;
                            $config_11['allowed_types'] = 'jpg|jpeg|gif|png|JPG|JPEG|GIF|PNG';
                            $config_11['overwrite'] = false;
                            $config_11['remove_spaces'] = true;
                            $config_11['file_name'] = $new_prod_image_name_ext;

                            $this->upload->initialize($config_11);
                            if (!$this->upload->do_upload('file' . ($j + 1))) {
                                $this->session->set_flashdata('msg_err' . $j, $this->upload->display_errors());
                                redirect(base_url() . "admin/pages/member_edit/" . $mem_id);
                                exit();
                            } else {
                                $uploadthumb = FCPATH . "resources/member_image/thumb/";
                                $uploadthumb = str_replace(" ", "", $uploadthumb);

                                # page
                                $width = '80';
                                $height = '';
                                $thumb_image_name = 'thumb_' . $new_prod_image_name_ext;
                                ratio_image_resize($image_path, $uploadthumb, $thumb_image_name, $width, $height);
                                $config_thumb['upload_path'] = $uploadthumb;
                                $this->upload->initialize($config_thumb);
                                $dataDP = $this->upload->data();
                            } //else upload END
                        } //if (file[name] != '') END
                    } //if isset(file) END
                }
            } //for loop END
            $array['mem_image'] = $arrImg[0];
            $array['mem_description'] = stripcslashes($this->input->post('mem_description'));
$array['update'] = date('Y-m-d H:i:s');
            $this->common_model->commonUpdate('tbl_homepage_member', $array, 'mem_id', $mem_id);
            $this->session->set_flashdata('msg', 'Item updated successfully.');
            redirect(base_url() . "admin/pages/homepage_member");
            exit;
        }
    }

    public function delete_memeber($id) {
        $this->common_model->commonDelete('tbl_homepage_member', "mem_id", $id);
        $this->session->set_flashdata('msg', 'Item deleted successfully.');
        redirect(base_url() . "admin/pages/homepage_member");
        exit;
    }

    public function add_listing($pg_id = '', $listing_id = '') {
        if ($pg_id == '' || !is_numeric($pg_id)) {
            redirect(base_url('admin/pages/sub_sub_listing/' . $pg_id . '/' . $listing_id));
            exit;
        }
        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $pg_id)->row();
        $data['sub_pg'] = $this->common_model->commonselect('tbl_pages_sub', 'sub_id', $listing_id)->row();
        $data['listing_pg'] = $this->common_model->commonselect('tbl_pages_listing', 'pg_listing_id', $pg_id)->row();

        if (count($data['main_pg']) <= 0) {
            redirect(base_url("admin/pages"));
            exit;
        }
        $data['title'] = "Add New Listing";
        $this->form_validation->set_rules('listing_title', 'listing title', 'trim|required');
        $this->form_validation->set_rules('listing_url', 'page url', 'trim|required|valid_url|is_unique[tbl_pages_url.page_url]|callback_helper_url');
        $this->form_validation->set_rules('listing_status', 'listing status', 'trim|required');
        $this->form_validation->set_rules('banner_title', 'banner title', 'trim|required');
        $this->form_validation->set_rules('file1', 'image', 'trim|required|callback_valid_file');
        $this->form_validation->set_rules('file2', 'hover_image', 'trim|required|callback_valid_file');
        if ((isset($_FILES['banner_image']) || @$_FILES['banner_image']['name'] != '' || @$_FILES['banner_image']['type'] != '')) {
            $this->form_validation->set_rules('hidden_file3', 'banner_image', 'trim|callback_valid_file');
        }
        //$this->form_validation->set_rules('pg_description', 'Page description', 'trim|required');
        $this->form_validation->set_message('required', 'This field is required.');
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/pages/listing_page', $data);
        } else {
            $redirect_url = "add_listing/" . $pg_id . "/" . $listing_id;
            $banneruploadpath = 'resources/page_image/listing/banner';
            $bannerFiles = $_FILES['banner_image'];
            $bannerimage = $this->content_model->banner_upload($bannerFiles, 'banner_image', $banneruploadpath, $pg_id, $redirect_url);
            /* $fileuploadpath   = 'resources/page_image/listing'; */
            $Files1 = $_FILES['image'];
            $Files2 = $_FILES['hover_image'];
            //$redirect_url     = "add_listing/" . $pg_id . "/" . $listing_id;
            $listingimage = $this->common_model->content_upload($Files1, 'image', $banneruploadpath, $pg_id, $redirect_url);
            $listingimage2 = $this->common_model->content_upload($Files2, 'hover_image', $banneruploadpath, $pg_id, $redirect_url);
            /* $banneruploadpath = 'resources/page_image/listing/banner';
              $bannerFiles      = $_FILES['banner_image'];
              $bannerimage      = $this->content_model->banner_upload($bannerFiles, 'banner_image', $banneruploadpath, $pg_id, $redirect_url); */
            $array = array(
                'listing_title' => stripcslashes($this->input->post('listing_title')),
                'listing_url' => stripcslashes($this->input->post('listing_url')),
                'lisintg_image' => $listingimage,
                'lisintg_image2' => $listingimage2,
                'listing_banner_title' => stripcslashes($this->input->post('banner_title')),
                'listing_banner_image' => $bannerimage,
                'listing_description' => stripcslashes($this->input->post('pg_description')),
                'listing_status' => stripcslashes($this->input->post('listing_status')),
                'meta_title' => stripcslashes($this->input->post('meta_title')),
                'meta_keywords' => stripcslashes($this->input->post('meta_keywords')),
                'meta_description' => stripcslashes($this->input->post('meta_description')),
                'pg_listing_id' => $listing_id,
            );
            $array['date'] = date('Y-m-d H:i:s');
            $sub_list_id = $this->common_model->commonSave('tbl_pages_listing', $array);

            $url_array = array(
                'page_id' => $sub_list_id,
                'page_url' => $this->input->post('listing_url'),
                'page_table' => 'tbl_pages_listing',
            );

            $this->common_model->commonSave('tbl_pages_url', $url_array);

            $this->session->set_flashdata('msg', 'Item added successfully.');
            redirect(base_url() . "admin/pages/sub_sub_listing/" . $pg_id . "/" . $listing_id);
        }
    }

    public function deltelisting($pg_id = '', $subpage_id = '', $listing_id = '') {

        if ($pg_id == '' || !is_numeric($pg_id)) {
            redirect(base_url("admin/pages"));
            exit;
        }
        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $pg_id)->row();
        if (count($data['main_pg']) <= 0) {
            redirect(base_url("admin/pages"));
            exit;
        }
        $this->common_model->commonDelete('tbl_pages_listing', "listing_id", $listing_id);
        $this->common_model->commonDelete('tbl_pages_url', "page_id", $listing_id);
        $this->session->set_flashdata('msg', 'Item deleted successfully.');
        redirect(base_url('admin/pages/sub_sub_listing/' . $pg_id . '/' . $subpage_id));
        exit;
    }

    public function sub_sub_listing($pg_id = '', $listing_id = '') {
        if ($pg_id == '' || !is_numeric($pg_id)) {
            redirect(base_url('admin/pages'));
            exit;
        }
        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $pg_id)->row();
        $data['sub_pg'] = $this->common_model->commonselect('tbl_pages_sub', 'sub_id', $listing_id)->row();
        $data['listing_pg'] = $this->common_model->commonselect('tbl_pages_listing', 'pg_listing_id', $listing_id)->row();
        if (count($data['main_pg']) <= 0) {
            redirect(base_url("admin/pages"));
            exit;
        }
        if ($listing_id == '' || !is_numeric($listing_id)) {
            redirect(base_url('admin/pages/sub_page_listing/' . $pg_id));
            exit;
        }

        //$data['listing_pg'] = $this->common_model->commonselect2('tbl_pages_listing', 'pg_listing_id', $pg_id, 'listing_id', $listing_id)->row();
        //        if (count($data['listing_pg']) <= 0) {
        //            redirect(base_url('admin/pages/sub_listing/' . $pg_id));
        //            exit;
        //        }

        $data['pageList'] = $this->common_model->commonselect('tbl_pages_listing', 'pg_listing_id', $listing_id, 'listing_id', "ASC")->result();
        /* echo "<pre>";
          echo $this->db->last_query();
          print_r($data);
          exit; */
        $this->load->view('admin/pages/sub_listing', $data);
    }

    public function list_sub_listing($pg_id = '', $listing_id = '', $lid = '') {
        if ($pg_id == '' || !is_numeric($pg_id)) {
            redirect(base_url('admin/pages'));
            exit;
        }

        if ($listing_id == '' || !is_numeric($listing_id)) {
            redirect(base_url('admin/pages/sub_page_listing/' . $listing_id));
            exit;
        }

        if ($lid == '' || !is_numeric($lid)) {
            redirect(base_url('admin/pages/sub_sub_listing/' . $pg_id . '/' . $listing_id));
            exit;
        }

        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $pg_id)->row();
        $data['listing_pg'] = $this->common_model->commonselect('tbl_pages_listing', 'listing_id', $lid)->row();
        $data['listing_list'] = $this->common_model->commonselect('tbl_pages_sub_listing', 'list_listing_id', $lid)->result();

        if (count($data['main_pg']) <= 0) {
            redirect(base_url("admin/pages"));
            exit;
        }

        if (count($data['listing_pg']) <= 0) {
            redirect(base_url('admin/pages/sub_page_listing/' . $listing_id));
            exit;
        }

//        if (count($data['listing_list']) <= 0) {
        //            redirect(base_url('admin/pages/sub_sub_listing/' . $pg_id . '/' . $listing_id));
        //            exit;
        //        }

        $data['title'] = "Sub Listing";
        $this->load->view('admin/pages/sub_sub_listing', $data);
    }

    public function sub_listing($pg_id = '') {

        if ($pg_id == '' || !is_numeric($pg_id)) {
            redirect(base_url('admin/pages'));
            exit;
        }
        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $pg_id)->row();
        if (count($data['main_pg']) <= 0) {
            redirect(base_url("admin/pages"));
            exit;
        }
        $data['listingList'] = $this->common_model->commonselect('tbl_pages_listing', 'pg_listing_id', $pg_id, 'listing_id', "ASC")->result();
        $this->load->view('admin/pages/sub_listing', $data);
    }

    public function add_sub_listing($pg_id = '', $listing_id = '', $lid = '') {
        if ($pg_id == '' || !is_numeric($pg_id)) {
            redirect(base_url('admin/pages'));
            exit;
        }

        if ($listing_id == '' || !is_numeric($listing_id)) {
            redirect(base_url('admin/pages/sub_page_listing/' . $listing_id));
            exit;
        }

        if ($lid == '' || !is_numeric($lid)) {
            redirect(base_url('admin/pages/sub_sub_listing/' . $pg_id . '/' . $listing_id));
            exit;
        }

        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $pg_id)->row();
        $data['listing_pg'] = $this->common_model->commonselect('tbl_pages_listing', 'listing_id', $lid)->row();
        $data['listing_list'] = $this->common_model->commonselect('tbl_pages_sub_listing', 'list_listing_id', $lid)->result();

        if (count($data['main_pg']) <= 0) {
            redirect(base_url("admin/pages"));
            exit;
        }

        if (count($data['listing_pg']) <= 0) {
            redirect(base_url('admin/pages/sub_page_listing/' . $listing_id));
            exit;
        }

        $data['title'] = "Add New Sub Listing";
        $data['sub_title'] = "Sub Listing";
        $this->form_validation->set_rules('pg_description', 'Page description', 'trim|required');
        $this->form_validation->set_rules('pg_status', 'page title', 'trim|required');
        if ((isset($_FILES['image']) || @$_FILES['image']['name'] != '' || @$_FILES['image']['type'] != '')) {
            $this->form_validation->set_rules('hidden_file', 'image', 'trim|callback_valid_file');
        }

        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('valid_file', 'The file type you are attempting to upload is not allowed.');
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/pages/sub_listing_page', $data);
        } else {
            $fileuploadpath = 'resources/page_image/sub_listing';
            $Files1 = $_FILES['image'];
            $page_name = "add_listing/";
            $listingimage = $this->common_model->content_upload($Files1, 'image', $fileuploadpath, $pg_id, $page_name);
            $array = array(
                'listing_description' => stripcslashes($this->input->post('pg_description')),
                'listing_status' => $this->input->post('pg_status'),
                'lisintg_image' => $listingimage,
                'list_listing_id' => $lid,
            );
            $array['date'] = date('Y-m-d H:i:s');

            $this->common_model->commonSave('tbl_pages_sub_listing', $array);
            $this->session->set_flashdata('msg', 'Item added successfully.');
            redirect(base_url() . "admin/pages/list_sub_listing/" . $pg_id . '/' . $listing_id . '/' . $lid);
        }
    }

    public function delete_sub_listing($pg_id = '', $listing_id = '', $lid = '', $cid = '') {
        if ($pg_id == '' || !is_numeric($pg_id)) {
            redirect(base_url('admin/pages'));
            exit;
        }

        if ($listing_id == '' || !is_numeric($listing_id)) {
            redirect(base_url('admin/pages/sub_page_listing/' . $listing_id));
            exit;
        }

        if ($lid == '' || !is_numeric($lid)) {
            redirect(base_url('admin/pages/sub_sub_listing/' . $pg_id . '/' . $listing_id));
            exit;
        }

        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $pg_id)->row();
        $data['listing_pg'] = $this->common_model->commonselect('tbl_pages_listing', 'pg_listing_id', $listing_id)->row();
        $data['listing_list'] = $this->common_model->commonselect('tbl_pages_sub_listing', 'list_listing_id', $lid)->result();

        if (count($data['main_pg']) <= 0) {
            redirect(base_url("admin/pages"));
            exit;
        }

        if (count($data['listing_pg']) <= 0) {
            redirect(base_url('admin/pages/sub_page_listing/' . $listing_id));
            exit;
        }

        if (count($data['listing_list']) <= 0) {
            redirect(base_url('admin/pages/sub_sub_listing/' . $pg_id . '/' . $listing_id));
            exit;
        }
        $data['img_name'] = $this->common_model->commonselect('tbl_pages_sub_listing', 'listing_id', $cid)->row();

        unlink('resources/page_image/sub_listing/thumb/' . $data['img_name']->lisintg_image);
        unlink('resources/page_image/sub_listing/' . $data['img_name']->lisintg_image);
        $this->common_model->commonDelete('tbl_pages_sub_listing', "listing_id", $cid);
        $this->session->set_flashdata('msg', 'Item deleted successfully.');
        redirect(base_url('admin/pages/list_sub_listing/' . $pg_id . '/' . $listing_id . '/' . $lid));
        exit;
    }

    public function edit_listing($pg_id = '', $subpage_id = '', $listing_id = '') {

        if ($pg_id == '' || !is_numeric($pg_id)) {
            redirect(base_url('admin/pages/sub_sub_listing/' . $pg_id . '/' . $listing_id));
            exit;
        }
        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $pg_id)->row();
        $data['sub_pg'] = $this->common_model->commonselect('tbl_pages_sub', 'sub_id', $subpage_id)->row();
        $data['listing_pg'] = $this->common_model->commonselect('tbl_pages_listing', 'listing_id', $listing_id)->row();

        if (count($data['main_pg']) <= 0) {
            redirect(base_url("admin/pages"));
            exit;
        }

        if ($this->input->post('old_url') == $this->input->post('listing_url')) {
            $this->form_validation->set_rules('listing_url', 'page URL', 'trim|required|valid_url');
        } else {
            $this->form_validation->set_rules('listing_url', 'page URL', 'trim|required|valid_url|is_unique[tbl_pages_url.page_url]|callback_helper_url');
        }

        $data['title'] = "Edit Listing";
        $this->form_validation->set_rules('listing_title', 'listing title', 'trim|required');

        /* if ($this->input->post('listing_url') != $this->input->post('old_url')) {
          $this->form_validation->set_rules('listing_url', 'page url', 'trim|required|valid_url|is_unique[tbl_pages_listing.listing_url]');
          } */

        $this->form_validation->set_rules('listing_status', 'listing status', 'trim|required');
        $this->form_validation->set_rules('banner_title', 'banner title', 'trim|required');
        //$this->form_validation->set_rules('pg_description', 'Page description', 'trim|required');
        if ((isset($_FILES['image']) || @$_FILES['image']['name'] != '' || @$_FILES['image']['type'] != '')) {
            $this->form_validation->set_rules('hidden_file1', 'image', 'trim|callback_valid_file');
        }

        if ((isset($_FILES['hover_image']) || @$_FILES['hover_image']['name'] != '' || @$_FILES['hover_image']['type'] != '')) {
            $this->form_validation->set_rules('hidden_file2', 'hover_image', 'trim|callback_valid_file');
        }

        if ((isset($_FILES['banner_image']) || @$_FILES['banner_image']['name'] != '' || @$_FILES['banner_image']['type'] != '')) {
            $this->form_validation->set_rules('hidden_file3', 'banner_image', 'trim|callback_valid_file');
        }

        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('valid_file', 'The file type you are attempting to upload is not allowed.');

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/pages/listing_page_edit', $data);
        } else {
            /* echo "<pre>";
              print_r($_FILES);
              exit; */
            $redirect_url = "edit_listing/" . $pg_id . "/" . $subpage_id . "/" . $listing_id;
            $banneruploadpath = 'resources/page_image/listing/banner';
            $bannerFiles = $_FILES['banner_image'];
            $bannerimage = $this->content_model->banner_upload($bannerFiles, 'banner_image', $banneruploadpath, $pg_id, $redirect_url);
            $fileuploadpath = 'resources/page_image/listing';
            $Files1 = $_FILES['image'];
            $Files2 = $_FILES['hover_image'];
            $listingimage = $this->common_model->content_upload($Files1, 'image', $banneruploadpath, $pg_id, $redirect_url);
            $listingimage2 = $this->common_model->content_upload($Files2, 'hover_image', $banneruploadpath, $pg_id, $redirect_url);
            $banneruploadpath = 'resources/page_image/listing/banner';
            $bannerFiles = $_FILES['banner_image'];
            $bannerimage = $this->content_model->banner_upload($bannerFiles, 'banner_image', $banneruploadpath, $pg_id, $redirect_url);
            $array = array(
                'listing_title' => stripcslashes($this->input->post('listing_title')),
                'listing_url' => stripcslashes($this->input->post('listing_url')),
                'lisintg_image' => ($listingimage == '' ? $this->input->post('image') : $listingimage),
                'lisintg_image2' => ($listingimage2 == '' ? $this->input->post('hover_image') : $listingimage2),
                'listing_banner_title' => stripcslashes($this->input->post('banner_title')),
                'listing_banner_image' => ($bannerimage == '' ? $this->input->post('banner_image') : $bannerimage),
                'listing_description' => stripcslashes($this->input->post('pg_description')),
                'listing_status' => stripcslashes($this->input->post('listing_status')),
                'meta_title' => stripcslashes($this->input->post('meta_title')),
                'meta_keywords' => stripcslashes($this->input->post('meta_keywords')),
                'meta_description' => stripcslashes($this->input->post('meta_description')),
                'pg_listing_id' => $subpage_id,
            );
            $array['update'] = date('Y-m-d H:i:s');

            $url_array = array(
                'page_url' => $this->input->post('listing_url'),
                'page_table' => 'tbl_pages_listing',
            );

            /* echo "<pre>";
              print_r($array);
              print_r($url_array);
              print_r($_FILES);
              exit; */
            $this->common_model->commonUpdate('tbl_pages_listing', $array, 'listing_id', $listing_id);
            $this->common_model->commonUpdate('tbl_pages_url', $url_array, 'page_id', $listing_id);
            $this->session->set_flashdata('msg', 'Item Updated successfully.');
            redirect(base_url() . "admin/pages/sub_sub_listing/" . $pg_id . "/" . $subpage_id);
        }
    }

    public function delete_listing_img() {
        if (!$this->input->is_ajax_request()) {
            
        } else {
            $id = $this->input->post('nId');
            $data = array($this->input->post('fieldname') => "");
            $this->db->where('listing_id', $id);
            $this->db->update('tbl_pages_listing', $data);
        }
    }

    public function edit_sub_listing($pg_id = '', $listing_id = '', $lid = '', $cid = '') {
        /* echo $lid;
          exit; */
        if ($pg_id == '' || !is_numeric($pg_id)) {
            redirect(base_url('admin/pages'));
            exit;
        }

        if ($listing_id == '' || !is_numeric($listing_id)) {
            redirect(base_url('admin/pages/sub_page_listing/' . $listing_id));
            exit;
        }

        if ($lid == '' || !is_numeric($lid)) {
            redirect(base_url('admin/pages/sub_sub_listing/' . $pg_id . '/' . $listing_id));
            exit;
        }

        if ($cid == '' || !is_numeric($cid)) {
            redirect(base_url('admin/pages/list_sub_listing/' . $pg_id . '/' . $listing_id . '/' . $lid));
            exit;
        }

        $data['main_pg'] = $this->common_model->commonselect('tbl_pages', 'pg_id', $pg_id)->row();
        /* $data['listing_pg']   = $this->common_model->commonselect('tbl_pages_listing', 'pg_listing_id', $listing_id)->row(); */
        $data['listing_pg'] = $this->common_model->commonselect('tbl_pages_listing', 'listing_id', $lid)->row();
        $data['listing_list'] = $this->common_model->commonselect('tbl_pages_sub_listing', 'list_listing_id', $lid)->result();
        $data['page'] = $this->common_model->commonselect('tbl_pages_sub_listing', 'listing_id', $cid)->row();

        if (count($data['main_pg']) <= 0) {
            redirect(base_url("admin/pages"));
            exit;
        }

        if (count($data['listing_pg']) <= 0) {
            redirect(base_url('admin/pages/sub_page_listing/' . $listing_id));
            exit;
        }

        if (count($data['listing_list']) <= 0) {
            redirect(base_url('admin/pages/sub_sub_listing/' . $pg_id . '/' . $listing_id));
            exit;
        }

        /* if (count($data['page']) <= 0) {
          redirect(base_url('admin/pages/list_sub_listing/' . $pg_id . '/' . $listing_id . '/' . $lid));
          exit;
          } */

        /* echo "<pre>";
          print_r($data);
          exit; */

        $data['title'] = "Edit Sub Listing";
        $data['sub_title'] = "Sub Listing";
        $this->form_validation->set_rules('pg_description', 'Page description', 'trim|required');
        $this->form_validation->set_rules('pg_status', 'page title', 'trim|required');
        if ((isset($_FILES['image']) || @$_FILES['image']['name'] != '' || @$_FILES['image']['type'] != '')) {
            $this->form_validation->set_rules('hidden_file', 'image', 'trim|callback_valid_file');
        }
        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('valid_file', 'The file type you are attempting to upload is not allowed.');
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/pages/edit_sub_listing', $data);
        } else {
            $array = array(
                'listing_description' => stripcslashes($this->input->post('pg_description')),
                'listing_status' => $this->input->post('pg_status'),
                'list_listing_id' => $lid,
            );
            if (@$_FILES['image']['name'] != '' || @$_FILES['image']['type'] != '') {
                $fileuploadpath = 'resources/page_image/sub_listing';
                $Files1 = $_FILES['image'];
                $page_name = "add_listing/";
                $listingimage = $this->common_model->content_upload($Files1, 'image', $fileuploadpath, $pg_id, $page_name);
                unlink('resources/page_image/sub_listing/thumb/' . $this->input->post('old-txt'));
                unlink('resources/page_image/sub_listing/' . $this->input->post('old-txt'));
                $array['lisintg_image'] = $listingimage;
            } else {
                $array['lisintg_image'] = $this->input->post('hidden_file');
            }
            $array['update'] = date('Y-m-d H:i:s');
            $this->common_model->commonUpdate('tbl_pages_sub_listing', $array, 'listing_id', $cid);
            $this->session->set_flashdata('msg', 'Item updated successfully.');
            redirect(base_url() . "admin/pages/list_sub_listing/" . $pg_id . '/' . $listing_id . '/' . $lid);
        }
    }

    public function edit_pricing_benefits_page($id = 1) {
        $data['title'] = "Pricing & Benefits";
        if ($id == '' || !is_numeric($id)) {
            redirect(base_url('admin/pages'));
            exit;
        }

        $data['work_data'] = $this->common_model->commonSelect('tbl_pricing_benefits', 'p_id', $id)->row();
        $old_url = $data['work_data']->page_url;

        $this->form_validation->set_rules('page_title', 'Page Title', 'trim|required');

        if ($data['work_data']->page_url == $this->input->post('page_url')) {
            $this->form_validation->set_rules('page_url', 'page URL', 'trim|required|valid_url');
        } else {
            $this->form_validation->set_rules('page_url', 'page URL', 'trim|required|valid_url|is_unique[tbl_pages_url.page_url]|callback_helper_url');
        }

        $this->form_validation->set_rules('page_description', 'Page Description', 'trim|required');
        $this->form_validation->set_rules('desc1', 'Description', 'trim|required');
        $this->form_validation->set_rules('desc2', 'Description', 'trim|required');
        $this->form_validation->set_rules('desc3', 'Description', 'trim|required');
        $this->form_validation->set_rules('meta_title', 'meta title', 'trim');
        $this->form_validation->set_rules('meta_keywords', 'meta keyword', 'trim');
        $this->form_validation->set_rules('meta_description', 'meta description', 'trim');
        $this->form_validation->set_rules('offer', 'Offer', 'trim|required');
        $this->form_validation->set_rules('benefits_description', 'Benefits Description', 'trim|required');
        //$this->form_validation->set_rules('pg_status', 'Status', 'trim|required');
        /*
          $this->form_validation->set_rules('startUrl', 'start url', 'trim|required');
          $this->form_validation->set_rules('signupTitle', 'newsletter title','trim|required');
          $this->form_validation->set_rules('signupDesc', 'newsletter description','trim|required');
         */
        if ((isset($_FILES['file1']) || @$_FILES['file1']['name'] != '' || @$_FILES['file1']['type'] != '')) {
            //$this->form_validation->set_rules('hidden_file1', 'file1', 'trim');
            $this->form_validation->set_rules('hidden_file1', 'file1', 'trim|callback_valid_file');
        }

        if ((isset($_FILES['file2']) || @$_FILES['file2']['name'] != '' || @$_FILES['file2']['type'] != '')) {
            $this->form_validation->set_rules('hidden_file2', 'file2', 'trim|callback_valid_file');
        }
        if ((isset($_FILES['file3']) || @$_FILES['file3']['name'] != '' || @$_FILES['file3']['type'] != '')) {
            $this->form_validation->set_rules('hidden_file3', 'file3', 'trim|callback_valid_file');
        }

        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('valid_url', 'Please enter a valid URL.');

        /* echo "<pre>";
          print_r($data);
          print_r($old_url);
          exit; */

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/pages/pricing_and_benefits', $data);
        } else {

            $arrImg = '';
            $j = 0;
            for ($j = 0; $j < 3; $j++) {
                #$totalFiles
                if (@$_FILES['file' . ($j + 1)]['name'] == '' && $this->input->post('hidden_file' . ($j + 1)) != '') {
                    $arrImg[$j] = $this->input->post('hidden_file' . ($j + 1));
                } else {
                    if (isset($_FILES['file' . ($j + 1)])) {
                        $myFiles = $_FILES['file' . ($j + 1)];

                        $_FILES['file' . ($j + 1)]['name'] = $myFiles['name'];
                        $_FILES['file' . ($j + 1)]['type'] = $myFiles['type'];
                        $_FILES['file' . ($j + 1)]['tmp_name'] = $myFiles['tmp_name'];
                        $_FILES['file' . ($j + 1)]['error'] = $myFiles['error'];
                        $_FILES['file' . ($j + 1)]['size'] = $myFiles['size'];

                        $file_name = $myFiles['name'];

                        $file_ext = explode('.', $file_name);
                        $file_extension = end($file_ext);

                        $image_path = $myFiles['tmp_name'];

                        $this->load->library('upload');
                        $file_Post = '';

                        if ($myFiles['name'] != '') {
                            $new_prod_image_name = "img_" . rand(1, 1000) . time();

                            $arrImg[$j] = $new_prod_image_name_ext = $new_prod_image_name . '.' . $file_extension;

                            //If user selected the photo to upload .
                            $uploaddir_11 = FCPATH . "resources/pricing_benefits/";
                            $uploaddir_11 = str_replace(" ", "", $uploaddir_11);
                            $config_11['upload_path'] = $uploaddir_11;
                            $config_11['allowed_types'] = 'jpg|jpeg|gif|png|JPG|JPEG|GIF|PNG';
                            $config_11['overwrite'] = false;
                            $config_11['remove_spaces'] = true;
                            $config_11['file_name'] = $new_prod_image_name_ext;

                            $this->upload->initialize($config_11);
                            if (!$this->upload->do_upload('file' . ($j + 1))) {
                                $this->session->set_flashdata('msg_err' . $j, $this->upload->display_errors());
                                redirect(base_url() . "admin/pages/");
                                exit();
                            } else {
                                unlink('resources/pricing_benefits/thumb/thumb_' . $this->input->post('hidden_file' . ($j + 1) . 'old'));
                                unlink('resources/pricing_benefits/' . $this->input->post('hidden_file' . ($j + 1) . 'old'));
                                $uploadthumb = FCPATH . "resources/pricing_benefits/thumb/";
                                $uploadthumb = str_replace(" ", "", $uploadthumb);

                                # page
                                $width = '80';
                                $height = '80';
                                $thumb_image_name = 'thumb_' . $new_prod_image_name_ext;
                                ratio_image_resize($image_path, $uploadthumb, $thumb_image_name, $width, $height);
                                $config_thumb['upload_path'] = $uploadthumb;
                                $this->upload->initialize($config_thumb);
                                $dataDP = $this->upload->data();
                            } //else upload END
                        } //if (file[name] != '') END
                    } //if isset(file) END
                }
            } //for loop END

            $array['image_1'] = $arrImg[0];
            $array['image_2'] = $arrImg[1];
            $array['image_3'] = $arrImg[2];
            $array['page_url'] = stripcslashes($this->input->post('page_url'));
            $array['page_title'] = stripcslashes($this->input->post('page_title'));
            $array['page_description'] = stripcslashes($this->input->post('page_description'));
            $array['img_description_1'] = stripcslashes($this->input->post('desc1'));
            $array['img_description_2'] = stripcslashes($this->input->post('desc2'));
            $array['img_description_3'] = stripcslashes($this->input->post('desc3'));
            $array['meta_title'] = stripcslashes($this->input->post('meta_title'));
            $array['meta_keywords'] = stripcslashes($this->input->post('meta_keywords'));
            $array['meta_description'] = stripcslashes($this->input->post('meta_description'));
            $array['offer_line'] = stripcslashes($this->input->post('offer'));
            $array['benefits_description'] = stripcslashes($this->input->post('benefits_description'));
            //$array['status'] = $this->input->post('pg_status');
            $array2['update'] =date('Y-m-d H:i:s');
            $this->common_model->commonUpdate('tbl_pages', $array2, 'pg_id', 10);
            if ($old_url != $this->input->post('page_url')) {
                $this->update_routes($old_url, $this->input->post('page_url'));
            }

            $url_array = array(
                'page_url' => $this->input->post('page_url'),
                'page_table' => 'tbl_pages',
            );

            $url = array(
                'pg_url' => $this->input->post('page_url'),
                'pg_title' => $this->input->post('page_title'),
            );

            /* echo "<pre>";
              print_r($array);
              print_r($url_array);
              exit; */

            $this->common_model->commonUpdate('tbl_pricing_benefits', $array, 'p_id', $id);
            $this->common_model->commonUpdate('tbl_pages_url', $url_array, 'page_id', 10);
            $this->common_model->commonUpdate('tbl_pages', $url, 'pg_id', 10);
            $this->session->set_flashdata('msg', 'Item updated successfully.');
            redirect(base_url() . "admin/pages/");
            exit;
        }
    }

    public function update_routes($page_url_old = '', $page_url_new = '') {

        $find = "'" . $page_url_old . "'";
        $replace = "$page_url_new";
        $files = file(APPPATH . "config/routes.php");
        $new_file = array();
        foreach ($files as $line) {
            $line = preg_replace($find, $replace, $line); // Edit line with replace
            $new_file[] = $line;
        }
        file_put_contents(APPPATH . "config/routes.php", $new_file);
    }

}
