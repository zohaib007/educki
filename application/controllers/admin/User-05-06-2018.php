<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model(array('user/user_model', 'myaccount_model'));
        $this->load->library('email');
        $this->load->model('front/seller/seller_model');
        $this->load->library('upload');
        $this->load->model('order/order_model');
        auth();
    }

    public function index()
    {
        $post_field_name    = $this->input->get("field_name");
        $post_order_by      = $this->input->get("order");
        $data['sort_order'] = 'DESC';

        if ($post_field_name != '') {
            $data['field_data'] = $post_field_name;
            if ($post_order_by == 'DESC') {
                $data['sort_order'] = 'ASC';
            } else {
                $data['sort_order'] = 'DESC';
            }
        }
        $strwhere   = "";
        $urlSegeent = $this->uri->segment(3);
        if ($this->input->get('srchOp') == 'Name') {
            $strwhere .= " AND  Concat(user_fname, ' ', user_lname)  LIKE '%" . $this->input->get('searchText') . "%'";
        } elseif ($this->input->get('srchOp') == 'Email') {
            $strwhere .= " AND user_email LIKE '%" . $this->input->get('searchText') . "%'";
        }
        $data['MetaTitle'] = "Admin - User Management";
        $total_records     = count($this->user_model->getusershort($strwhere, "", "", 0));
        $perPage           = 50;
        $last              = end($this->uri->segments);
        if ($last > 0) {
            $page = $last;
        } else {
            $page = 0;
        }
        if ($strwhere != '') {
            $page = 0;
        }

        $data['rstCategories']     = $this->user_model->getusershort($strwhere, $perPage, $page, 1);
        $data['total_records']     = $total_records;
        $mypaing['total_rows']     = $total_records;
        $mypaing['base_url']       = base_url() . "admin/user/index";
        $mypaing['per_page']       = $perPage;
        $mypaing['uri_segment']    = 4;
        $mypaing['full_tag_open']  = '<div class="pagination-rp">';
        $mypaing['full_tag_close'] = '</div>';
        $mypaing['cur_tag_open']   = '<div class="pagination-rp">';
        $mypaing['cur_tag_close']  = '</div>';
        $this->pagination->initialize($mypaing);
        $data['paginglink'] = $this->pagination->create_links();
        if ($this->input->post('date_range')) {
            $date                  = $this->input->post('date_range');
            $value                 = $this->uri->segment(4);
            $data['rstCategories'] = $this->user_model->user_sort($value, $date);
        }

        $this->load->view('admin/user/listuser', $data);
    }

    /* --delete    category channel and sub category-- */

    public function delteuser($nId)
    {
        require_once ABSPATH . 'wp-admin/includes/user.php';
        $data['user_info'] = $this->common_model->commonselect('tbl_user', 'user_id', $nId)->row();
        $email             = $data['user_info']->user_email;
        $userdata          = WP_User::get_data_by('email', $email);
        wp_delete_user($userdata->ID);
        $data_save['user_is_delete'] = 1;
        $data_save['user_status'] = 0;
        /*$this->common_model->commonDelete('tbl_user', "user_id", $nId);*/
        $this->common_model->commonUpdate('tbl_user', $data_save, "user_id", $nId);
        $this->session->set_flashdata('msg', 'Item deleted successfully.');
        redirect(base_url() . "admin/user/");
    }

    public function validate_alphabet_space($value)
    {
        if (preg_match("/^[a-zA-Z\s]+$/", $value)) {
            return true;
        } else {
            $this->form_validation->set_message('validate_alphabet_space', "This field contain only alphabets and spaces.");
            return false;
        }
    }

    /* --Add   channel -- */

    public function validate_zipcode($zipcode)
    {
        //$test = preg_match("/^[0-9-]{11}*$/",$zipcode);
        $test = preg_match('/^[0-9]{5}([- ]?[0-9]{4})?$/', $zipcode);

        if ($test == 0) {
            $this->form_validation->set_message('validate_zipcode', 'Zip code is invalid.');
            return false;
        } else {
            return true;
        }
    }

    public function unique_email($email='')
    {
        $regData = $this->db->query("
                            SELECT *
                            FROM tbl_user
                            WHERE user_email = '".$email."' AND user_is_delete = 0
                        ")->row();
        /*echo "<pre>";
        print_r($regdata);
        echo "Count : ".count($regData);
        echo "Email : ".$email;
        exit;*/
        if(count($regData) > 0){
            $this->form_validation->set_message('unique_email', 'User Already Exists.');
            return false;
        }else{
            return true;
        }
    }

    public function validate_postalcode($zipcode)
    {
        //$test = preg_match("/^[0-9-]{11}*$/",$zipcode);
        $test = preg_match('/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/', $zipcode);
        //$test = preg_match('/[A-Z0-9 ]+$/', $zipcode);
        $province = mysql_real_escape_string($this->input->post('province'));

        if ($test == 0) {
            //$this->form_validation->set_message('validate_postalcode', 'Postal code is invalid.');
            $this->form_validation->set_message('validate_postalcode', 'Please enter valid Postal code.');
            return false;
        } else {
            if ($this->myaccount_model->is_ca_postal_code($zipcode, $province)) {
                return true;
            } else {
                $this->form_validation->set_message('validate_postalcode', 'Please enter valid Postal code.');
                return false;
            }
        }
    }

    /* --Add   channel -- */

    public function add()
    {
        $data['title'] = "Add New User";
        $this->form_validation->set_rules('user_fname', 'first name', 'trim|required');
        $this->form_validation->set_rules('user_lname', 'last name', 'trim|required');
        $this->form_validation->set_rules('user_sex', 'user_sex', 'trim|required');
        $this->form_validation->set_rules('user_email', 'user_email', 'trim|required|valid_email|callback_unique_email');
        //is_unique[tbl_user.user_email]
        $this->form_validation->set_rules('password', 'passowrd', 'trim|required|min_length[6]');

        $this->form_validation->set_rules('user_interests', 'First Interest', 'trim|required');
        if ($this->input->post('user_interests') == "Other") {
            $this->form_validation->set_rules('user_other_interest', 'First Interest', 'trim|required');
        }

        $this->form_validation->set_rules('user_interests_second', 'Second Interest', 'trim|required');
        if ($this->input->post('user_interests_second') == "Other") {
            $this->form_validation->set_rules('user_other_interest_second', 'Second Interest', 'trim|required');
        }

        $this->form_validation->set_rules('user_age', 'Age', 'trim|required');
        $this->form_validation->set_rules('user_business_owner', 'Business Owner', 'trim|required');
        $this->form_validation->set_rules('user_zip', 'user_zip', 'trim|required');
        /*$this->form_validation->set_rules('user_phone', 'Phone', 'trim|numeric');*/
        /*$this->form_validation->set_rules('user_zip', 'user_zip', 'trim|required|callback_validate_zipcode|min_length[5]|max_length[10]');
        $this->form_validation->set_rules('state', 'State', 'trim|required');
        $this->form_validation->set_rules('user_country', 'user_country', 'trim|required');
        $this->form_validation->set_rules('city', 'City', 'trim|required');
        $this->form_validation->set_rules('user_fee', 'user_fee', 'trim|required');*/
        $this->form_validation->set_rules('user_flagged_status', 'user_flagged_status', 'trim|required');
        $this->form_validation->set_rules('user_status', 'User type', 'trim|required');
        $this->form_validation->set_message('is_unique', 'This email address already exists.');
        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('valid_email', 'Please enter a valid email address.');

        if ($this->form_validation->run() == false) {
            /*$data['states']  = $this->common_model->common_select('tbl_states')->result_array();
            $data['country'] = $this->common_model->common_select('tbl_country')->result_array();*/
            $this->load->view('admin/user/Adduser', $data);
        } else {
            $data_save['user_interests'] = $this->input->post('user_interests');
            if ($this->input->post('user_interests') == "Other") {
                $data_save['user_other_interest'] = $this->input->post('user_other_interest');
            }

            $data_save['user_interests_second'] = $this->input->post('user_interests_second');
            if ($this->input->post('user_interests_second') == "Other") {
                $data_save['user_other_interest_second'] = $this->input->post('user_other_interest_second');
            }

            $data_save['user_fname']           = $this->input->post('user_fname');
            $data_save['user_lname']           = $this->input->post('user_lname');
            $data_save['user_sex']             = $this->input->post('user_sex');
            $data_save['user_age']             = $this->input->post('user_age');
            $data_save['user_email']           = $this->input->post('user_email');
            $data_save['user_type']            = "Buyer";
            $data_save['user_password']        = $this->input->post('password');
            $data_save['user_zip']             = $this->input->post('user_zip');
            $data_save['user_password_encryp'] = md5($this->input->post('password'));
            $data_save['user_flagged_status']  = $this->input->post('user_flagged_status');
            /*$data_save['user_city']                  = $this->input->post('city');
            $data_save['user_fee']                   = $this->input->post('user_fee');
            $data_save['user_country']               = $this->input->post('user_country');
            $data_save['user_state']                 = ($this->input->post('user_country') == 'US' ? $this->input->post('state') : $this->input->post('other_state'));
            $data_save['user_zip']                   = $this->input->post('user_zip');
            $data_save['user_phone'] = $this->input->post('user_phone');
            $data_save['user_other_interest']        = ($this->input->post('user_interests') == "other") ? $this->input->post('user_other_interest') : '';
            $data_save['user_other_interest_second'] = ($this->input->post('user_interests_second') == "other") ? $this->input->post('user_other_interest_second') : '';*/
            //$data_save['user_type']           = $this->input->post('user_type');
            $data_save['user_status']         = $this->input->post('user_status');
            $data_save['user_business_owner'] = $this->input->post('user_business_owner');
            $data_save['user_register_date']  = date('Y-m-d h:i:s');

            /*echo "<pre>";
            print_r($data_save);
            exit;
             */
            $nId = $this->common_model->commonSave('tbl_user', $data_save);

            if ($data_save['user_flagged_status'] == 1 && $data['user_data']['user_flagged_status'] != $data_save['user_flagged_status']) {
                $flagInfo['flag_date']        = date('Y-m-d h:i:s');
                $flagInfo['flag_user_id']     = $nId;
                $flagInfo['flag_by_admin_id'] = $this->session->userdata('admin_id');
                $this->common_model->commonSave('tbl_user_flag_history', $flagInfo);
            }

            add_action('init', 'add_user');
            $username = $this->input->post('user_email');
            $password = $this->input->post('password');
            $email    = $this->input->post('user_email');
            $user_id  = wp_create_user($username, $password, $email);

            $this->session->set_flashdata('msg', 'Item added successfully.');
            redirect(base_url('admin/user'));
            exit;
        }
    }

    /* --Edit   channel -- */

    public function edit($nId)
    {
        if ($nId == '' || !is_numeric($nId)) {
            redirect(base_url('admin/user'));
            exit;
        }
        $data['title'] = "Edit User";
        $this->form_validation->set_rules('user_fname', 'first name', 'trim|required');
        $this->form_validation->set_rules('user_lname', 'last name', 'trim|required');
        $this->form_validation->set_rules('user_sex', 'user_sex', 'trim|required');
        $data['user_data'] = $this->user_model->get_user($strWhere = " AND user_id ='" . $nId . "' ");
        if($data['user_data']['user_facebook_id']==''){
            $this->form_validation->set_rules('password', 'passowrd', 'trim|required|min_length[6]');
        }
        $this->form_validation->set_rules('user_zip', 'user_zip', 'trim|required');
        /*$this->form_validation->set_rules('user_phone', 'Phone', 'trim|numeric|required');*/
        $this->form_validation->set_rules('user_age', 'Age', 'trim|required');
        /*$this->form_validation->set_rules('user_zip', 'user_zip', 'trim|required|callback_validate_zipcode|min_length[5]|max_length[10]');
        $this->form_validation->set_rules('state', 'State', 'trim|required');
        $this->form_validation->set_rules('user_country', 'Country', 'trim|required');
        $this->form_validation->set_rules('city', 'City', 'trim|required');
        $this->form_validation->set_rules('user_fee', 'user_fee', 'trim|required');*/

        $this->form_validation->set_rules('user_interests', 'First Interest', 'trim|required');
        if ($this->input->post('user_interests') == "Other") {
            $this->form_validation->set_rules('user_other_interest', 'First Interest', 'trim|required');
        }

        $this->form_validation->set_rules('user_interests_second', 'Second Interest', 'trim|required');
        if ($this->input->post('user_interests_second') == "Other") {
            $this->form_validation->set_rules('user_other_interest_second', 'Second Interest', 'trim|required');
        }

        //$this->form_validation->set_value('user_flagged_status', 'Flagged Status', 'trim|required');
        $this->form_validation->set_rules('user_flagged_status', 'user_flagged_status', 'trim|required');
        $this->form_validation->set_rules('user_status', 'User type', 'trim|required');
        $this->form_validation->set_rules('user_business_owner', 'Business Owner', 'trim|required');

        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('valid_email', 'Please enter a valid email address.');

        if ($this->form_validation->run() == false) {
            /*$data['states']    = $this->common_model->common_select('tbl_states')->result_array();
            $data['country']   = $this->common_model->common_select('tbl_country')->result_array();*/
            //$data['user_data'] = $this->user_model->get_user($strWhere = " AND user_id ='" . $nId . "' ");
            if (count($data['user_data']) <= 0) {
                redirect(base_url('admin/user'));
                exit;
            }

            $this->load->view('admin/user/edituser', $data);
        } else {

            if ($this->input->post('user_status') != '3') {
                $data_save['user_interests'] = $this->input->post('user_interests');
                if ($this->input->post('user_interests') == "Other") {
                    $data_save['user_other_interest'] = $this->input->post('user_other_interest');
                } else {
                    $data_save['user_other_interest'] = '';
                }

                $data_save['user_interests_second'] = $this->input->post('user_interests_second');
                if ($this->input->post('user_interests_second') == "Other") {
                    $data_save['user_other_interest_second'] = $this->input->post('user_other_interest_second');
                } else {
                    $data_save['user_other_interest_second'] = '';
                }

                //$data['user_data']                       = $this->user_model->get_user($strWhere = " AND user_id ='" . $nId . "' ");
                $data_save['user_fname']          = $this->input->post('user_fname');
                $data_save['user_lname']          = $this->input->post('user_lname');
                $data_save['user_sex']            = $this->input->post('user_sex');
                $data_save['user_age']            = $this->input->post('user_age');
                $data_save['user_email']          = $this->input->post('user_email');
                if($data['user_data']['user_facebook_id']==''){
                    $data_save['user_password']       = $this->input->post('password');
                    $data_save['user_password_encryp']       = md5($this->input->post('password'));
                }
                $data_save['user_flagged_status'] = $this->input->post('user_flagged_status');
                $data_save['user_zip']            = $this->input->post('user_zip');
                /*$data_save['user_city']                  = $this->input->post('city');
                $data_save['user_country']               = $this->input->post('user_country');
                $data_save['user_fee']                   = $this->input->post('user_fee');
                $data_save['user_state']                 = ($this->input->post('user_country') == 'US' ? $this->input->post('state') : $this->input->post('other_state'));
                $data_save['user_zip']                   = $this->input->post('user_zip');
                $data_save['user_phone']          = $this->input->post('user_phone');*/
                $data_save['user_status']         = $this->input->post('user_status');
                $data_save['user_business_owner'] = $this->input->post('user_business_owner');
                $data_save['user_is_featured']    = $this->input->post('user_is_featured');
                $data['user_data'] = $this->user_model->get_user($strWhere = " AND user_id ='" . $nId . "' ");
                //echo '<pre>';
                //print_r($data['user_data']['user_is_featured']);
                //exit;
                if($data_save['user_is_featured'] == 1 && $data['user_data']['user_type'] == 'Seller' && $data['user_data']['user_is_featured'] == 0){
                    $data_save['user_featured_date'] = date('Y-m-d h:i:s');
                }
                /*echo "<pre>";
                print_r($data_save);
                var_dump($data_save);
                exit;*/

                $this->common_model->commonUpdate('tbl_user', $data_save, "user_id", $nId);

                if ($data_save['user_flagged_status'] == 1 && $data['user_data']['user_flagged_status'] != $data_save['user_flagged_status']) {
                    $flagInfo['flag_date']        = date('Y-m-d h:i:s');
                    $flagInfo['flag_user_id']     = $nId;
                    $flagInfo['flag_by_admin_id'] = $this->session->userdata('admin_id');
                    $this->common_model->commonSave('tbl_user_flag_history', $flagInfo);
                }

                if ($data_save['user_status'] == 0) {
                    $statusInfo['store_is_hide'] = 1;
                    $this->common_model->commonUpdate('tbl_stores', $statusInfo, "user_id", $nId);
                }

                require_once ABSPATH . 'wp-load.php';
                $userdata                   = get_user_by('email', $this->input->post('user_email')); //get user wordpress Table id by email
                $wp_data_save['ID']         = $userdata->ID; //user wordpress table id
                $wp_data_save['first_name'] = $this->input->post('user_fname');
                $wp_data_save['last_name']  = $this->input->post('user_lname');
                $wp_data_save['user_email'] = $this->input->post('user_email');
                wp_update_user($wp_data_save); //wordpress user update function
                $this->session->set_flashdata('msg', 'Item updated successfully.');
                redirect(base_url('admin/user'));
                exit;
            } else {
                $this->delteuser($nId);
            }
        }
    }

//end function

    public function details($nId)
    {
        $data['title']   = 'User Details';
        $data['results'] = $this->user_model->get_user($strWhere = " AND user_id ='" . $nId . "' ");
        if (count($data['results']) <= 0) {
            redirect(base_url('admin/user'));
            exit;
        }
        $this->load->view('admin/user/user_detail', $data);
    }

    public function user_orders($user_id)
    {
        $data['title']   = 'User Orders';
        $data['user_id'] = $user_id;

        set_time_limit(0);

        if (empty($_POST) == true && (@$_POST['date_range'] == '' || @$_POST['last_year'] == '')) {
            $this->load->view('admin/user/user_orders', $data);
        } else {
            $this->load->model('order/order_model');

            if ($_POST['date_range'] != '') {
                $date_range = explode('to', $this->input->post('date_range'));

                $strWhere = " AND `order_user_id` =" . $user_id . " ";
                if (isset($date_range[1]) && $date_range[1] != '') {
                    $strWhere .= " AND (`order_datetime` BETWEEN '" . trim($date_range[0]) . "' AND '" . trim($date_range[1]) . "')";
                } else {
                    $strWhere .= " AND `order_datetime` = '" . trim($date_range[0]) . "'";
                }

                $data['order_data'] = $this->order_model->order_ordered_details($strWhere);
                //echo $this->load->view('pdf/cover-letter');
                //echo $this->load->view('pdf/pdf-structure', $data, true);
                $this->download_pdf($data);
            } else if ($_POST['last_year'] != '') {
                $last_year           = date('Y') - 1;
                $last_year_date_from = $last_year . "-01-01";
                $last_year_date_to   = $last_year . "-12-31";

                $strWhere = " AND `order_user_id` =" . $user_id . " AND (`order_datetime` BETWEEN '" . $last_year_date_from . "' AND '" . $last_year_date_to . "') ";

                $data['order_data'] = $this->order_model->order_ordered_details($strWhere);
                $this->download_pdf($data);
                exit;
            }
        }
    }

    public function download_pdf($data)
    {
        $this->load->library('pdf');

        //load the view, pass the variable and do not show it but "save" the output into $html variable
        //$html_one = $this->load->view('pdf/cover-letter', $data, true);
        $html = $this->load->view('pdf/pdf-structure', $data, true);
        //this the the PDF filename that user will get to download
        $pdfFilePath = "user_id_" . $data['user_id'] . "_order_details.pdf";

        //actually, you can pass mPDF parameter on this load() function
        $pdf = $this->pdf->load();
        //generate the PDF!
        $pdf->WriteHTML($html);
        //offer it to user via browser download! (The PDF won't be saved on your server HDD)
        $pdf->Output($pdfFilePath, "D");
        exit;
    }

    public function exportcsv()
    {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=user_accounts_' . date("m-d-Y") . '.csv');
        $output = fopen('php://output', 'w');
        fputcsv($output, array('Customer ID','Customer Since', 'First Name', 'Last Name', 'Email', 'Customer Type', 'Last Login', 'Status'));
        $resultsProd = $this->user_model->getUserExport($strWere = "");
        for ($n = 0; $n < count($resultsProd); $n++) {
            $resultsProd[$n]['user_register_date'] = date('m-d-Y', strtotime($resultsProd[$n]['user_register_date']));
            if($resultsProd[$n]['user_last_active']!=''){
                $resultsProd[$n]['user_last_active'] = date('m-d-Y', strtotime($resultsProd[$n]['user_last_active']));
            }
            else{
                $resultsProd[$n]['user_last_active'] = "N/A";
            }
            if ($resultsProd[$n]['user_status'] == 1) {
                $resultsProd[$n]['user_status'] = "Active";
            } else {
                $resultsProd[$n]['user_status'] = "Inactive";
            }

            if ($resultsProd[$n]['user_last_active'] == '') {
                $resultsProd[$n]['user_last_active'] = "N/A";
            }
            fputcsv($output, $resultsProd[$n]);
        }
    }

    public function exportexcelxml()
    {
        $sheetname = array('filename' => 'user_accounts_' . date('m-d-Y'));
        $this->load->library('XSpreadsheet', $sheetname);

        $arr         = "";
        $resultsProd = $this->user_model->getUserExport($strWere = "");
        for ($n = 0; $n < count($resultsProd); $n++) {
            $arr[] = array($resultsProd[$n]['user_register_date'], $resultsProd[$n]['user_fname'], $resultsProd[$n]['user_lname'], $resultsProd[$n]['user_email']);
        }
        $arrTitle = array('Date created', 'First Name', 'Last Name', 'Email');
        $data[]   = $arrTitle;
        for ($n = 0; $n < count($arr); $n++) {
            $data[] = $arr[$n];
        }

        $this->xspreadsheet->AddWorksheet('Awesome Sheet', $data)->Generate()->Send();
    }

    /* ---------------------retrive user email check ------------------ */

    public function check_email($str)
    {
        $checklogin = array('user_email' => $this->input->post('email'));
        $result     = $this->common_model->common_where('tbl_user', $checklogin);

        if ($result->num_rows() > 0) {
            $this->form_validation->set_message('check_email', 'This email address already exists.');
            return false;
        } else {
            return true;
        }
    }

    public function check_edit_email($str, $nId)
    {
        $result = $this->db->query("
                                    SELECT *
                                    FROM tbl_user
                                    WHERE
                                    user_email = '" . $str . "'
                                    AND user_is_delete = 0 AND
                                    user_id != " . $nId . "
                                    ");

        if ($result->num_rows() > 0) {
            $this->form_validation->set_message('check_edit_email', 'This email address already exists.');
            return false;
        } else {
            return true;
        }
    }

    /* ----------------------validation user password and email------------------------------ */

    public function check_user_name($str)
    {
        $checklogin = array('user_user_name' => $this->input->post('uName'));
        $result     = $this->common_model->common_where('tbl_user', $checklogin);

        if ($result->num_rows() > 0) {
            $this->form_validation->set_message('check_user_name', 'user name already exists.');
            return false;
        } else {
            return true;
        }
    }

    public function changeStatus()
    {
        $data_array   = array();
        $company_name = array();

        if ($this->input->post('bStatus') == 1) {
            $checklogin = array('user_id' => $this->input->post('nId'));
            $result     = $this->common_model->common_where('tbl_user', $checklogin);

            if ($result->num_rows() > 0) {
                $row = $result->row();
                $row->user_fname;
                $row->user_email;
                /*                 * ******* For Supplier to add Company Name ********* */
                if ($row->user_type == "supplier" && $row->user_supply_companyname == "") {
                    $company_data = $this->common_model->common_where('tbl_company', array('com_name' => $row->user_other_company));
                    if ($company_data->num_rows() > 0) {
                        $company_name                          = $company_data->result_array();
                        $data_array['user_supply_companyname'] = $company_name[0]['com_id'];
                    } else {
                        $company_insert = array('com_name' => $row->user_other_company, 'com_user_id' => $row->user_id, 'com_date' => date('Y-m-d H:i:s'));
                        $this->db->insert('tbl_company', $company_insert);
                        $data_array['user_supply_companyname'] = $this->db->insert_id();
                    }
                    $data_array['user_other_company'] = "";
                    $message                          = "";
                    $subject                          = 'Thank you for your Registration';
                    $message .= 'Hello ' . $row->user_fname . ',<br /><br />';
                    $message .= 'Your account has now been activated, please login to your dashboard to manage your products and orders. <br /><br />Regards, <br /><br />Beye Team';
                    $this->load->library('email');
                    $this->email->set_mailtype('html');
                    $this->email->from("no-reply@broadwaybaskeeters.com");
                    $this->email->to("dotlogics159@gmail.com");
                    $this->email->subject($subject);
                    $this->email->message($message);
                    $this->email->send();
                    $this->email->clear(true);
                }

                /*                 * ******* For Supplier to add Company Name ends ********* */
            }
        }
        $data_array['user_status'] = $this->input->post('bStatus');
        $this->common_model->commonUpdate('tbl_user', $data_array, "user_id", $this->input->post('nId'));
        echo "Status changed successfully";
    }

    public function canada_zip()
    {
        if (!$this->input->is_ajax_request()) {
            redirect(base_url());
            exit;
        }
        $postalcode = mysql_real_escape_string(trim($this->input->post('postal_code')));
        $province   = mysql_real_escape_string(trim($this->input->post('province')));
        $zip_data   = $this->db->query("
                                    SELECT *
                                    FROM tbl_ca_state_zip
                                    WHERE
                                    postalcode = '" . $postalcode . "'
                                    AND
                                    provincecode = '" . $province . "'
                                    ")->row_array();
        if (count($zip_data) > 0) {
            echo "ok";
        } else {
            echo "empty";
        }
    }

    public function address($id, $page = 1)
    {
        //read_only_user();
        if (!is_numeric($id)) {
            redirect("admin/user/");
            exit;
        }
        $this->data['data'] = $this->customer_model->getCustomerById($id);
        if ($this->data['data']->num_rows() == 0) {
            $this->session->set_flashdata('msg', 'No Record Found.');
            redirect("admin/user/");
        }
        $limit                      = getSetting('per_page');
        $limitStart                 = (($page - 1) * $limit);
        $this->data['title']        = "Customer Addresses";
        $this->data['data1']        = $this->customer_model->getCustomerAddresses($id);
        $this->data['data']         = $this->customer_model->getCustomerAddressesLimit($id, $limitStart, $limit);
        $config['base_url']         = $this->data['url'] . 'customers_address/' . $id;
        $config['total_rows']       = $this->data['data1']->num_rows();
        $config['per_page']         = getSetting('per_page');
        $config['uri_segment']      = 3;
        $config['num_links']        = 2;
        $config['full_tag_open']    = '<div class="flt-rit">';
        $config['full_tag_close']   = '</div>';
        $config['cur_tag_open']     = '<a id="pagi-active" href="javascript:void(0);">';
        $config['cur_tag_close']    = '</a>';
        $config['use_page_numbers'] = true;
        $config['next_link']        = false;
        $config['prev_link']        = false;
        $config['last_link']        = 'Last';
        $config['first_link']       = 'First';
        $this->data['page']         = $page;
        $this->pagination->initialize($config);
        $this->data['pagi'] = $this->pagination->create_links();
        $this->data['id']   = $id;
        $this->load->view('customers/address', $this->data);
    }

    public function statistics()
    {
        $data['tier_1']               = $this->common_model->getTierStatistics(2);
        $data['tier_2']               = $this->common_model->getTierStatistics(3);
        $data['tier_3']               = $this->common_model->getTierStatistics(4);
        $data['activeSellers']        = $this->common_model->getActiveSellers();
        $data['SellersOnVacation']    = $this->common_model->getSellersOnVacation();
        $data['TotalSellers']         = $this->common_model->getTotalSellers();
        $data['TotalBuyers']          = $this->common_model->getTotalBuyers();
        $data['TotalFlaggedUsers']    = $this->common_model->getTotalFlaggedUsers();
        $data['TotalRegisteredUsers'] = $this->common_model->getTotalRegisteredUsers();

        /*echo "<pre>";
        print_r($data);
        exit;*/

        $this->load->view('admin/user/statistics', $data);
    }

    public function update_user_flag_status($user_id, $new_flagged_status)
    {

        $user_info = $this->common_model->getFlagStatusById($user_id);

        $flagged_status  = $user_info['user_flagged_status'];
        $flagged_counter = $user_info['user_flagged_counter'];
    }

    public function edit_store($id)
    {
        $data['tiers'] = $this->common_model->getCommon('tbl_tier_list', 'tier_id', 'ASC')->result();
        $data['userid']       = $id;
        $data['page_data']    = $this->common_model->commonSelect('tbl_stores', 'user_id', $data['userid'])->row();
        $this->form_validation->set_rules('store_name', 'Store Name', 'trim|required');
        if($data['page_data']->store_paypal_id!=$this->input->post('paypal_id')){
            $this->form_validation->set_rules('paypal_id', 'PayPal ID', 'trim|required|callback_paypal_id');
        }        
        if ($this->input->post('store_country') == "US") {
            $this->form_validation->set_rules('store_state', 'State', 'trim|required');
        } else {
            $this->form_validation->set_rules('other_state', 'State', 'trim|required');
        }
        $this->form_validation->set_rules('store_city', 'Store City', 'trim|required');
        $this->form_validation->set_rules('store_zip', 'Store zip', 'trim|required');
        $this->form_validation->set_rules('store_country', 'Store Country', 'trim|required');
        $this->form_validation->set_message('required', 'This field is required.');
        if (isset($_FILES['logo_image']) && @$_FILES['logo_image']['name'] != '') {
            $this->form_validation->set_rules('logo_img', 'Store Logo', 'trim|callback_edit_valid_file[1]');
        }else{
            $this->form_validation->set_rules('logo_img', 'Store Logo', 'trim|required');
        }
        if (isset($_FILES['banner_image']) && @$_FILES['banner_image']['name'] != '') {
            $this->form_validation->set_rules('banner_img', 'Store Banner', 'trim|callback_edit_valid_file[2]');
        }else{
            $this->form_validation->set_rules('banner_img', 'Store Banner', 'trim|required');
        }

        $data['allstate']     = $this->db->query("SELECT stat_id, stat_name FROM tbl_states")->result_array();
        $data['allcountries'] = $this->db->query("SELECT iso, name FROM tbl_country")->result_array();

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/user/edit-store', $data);
        } else {
            if (!empty($this->input->post('store_name'))) {

                if ($this->input->post('old_store') != $this->input->post('store_name')) {
                    $store_name = $this->input->post('store_name');
                    $response   = $this->seller_model->check_availability($store_name);
                    if ($response == 0) {
                        $this->edit_store_details($id);
                        $this->session->set_flashdata('msg', 'Store Updated successfully.');
                        redirect(base_url('admin/user'));
                    }
                } else {
                    $this->edit_store_details($id);
                    $this->session->set_flashdata('msg', 'Store Updated successfully.');
                    redirect(base_url('admin/user'));
                }
            }
            exit;
        }
    }

    public function edit_store_details($id = '')
    {
        $store_id               = $this->input->post('store_id');
        $data['store_name']     = $this->input->post('store_name');
        $data['store_describe'] = $this->input->post('describe');
        $data['store_address']  = $this->input->post('store_address');
        $data['store_city']     = $this->input->post('store_city');
        if ($this->input->post('store_country') == "US") {
            $data['store_state'] = $this->input->post('store_state');
        } else {

            $data['store_state'] = $this->input->post('other_state');
        }
        $data['store_zip']           = $this->input->post('store_zip');
        $data['store_country']       = $this->input->post('store_country');
        $data['store_number']        = $this->input->post('store_number');
        $data['store_description']   = $this->input->post('store_description');
        $data['store_paypal_id']     = $this->input->post('paypal_id');
        $data['store_facebook_url']  = $this->input->post('store_facebook_url');
        $data['store_twitter_url']   = $this->input->post('store_twitter_url');
        $data['store_instagram_url'] = $this->input->post('store_instagram_url');
        $data['store_pintrest_url']  = $this->input->post('store_pintrest_url');

        $data['user_id'] = $id;
        $bannerFiles1    = $_FILES['banner_image'];
        $logoFiles1      = $_FILES['logo_image'];
        $logoimage       = '';
        $bannerimage     = '';
        if ($bannerFiles1['name'] !== "") {
            $config = array(
                'allowed_types'  => 'jpg|jpeg|png|txt',
                'upload_path'    => FCPATH . 'resources/seller_account_image/banner/',
                'file_name'      => 'banner_' . date('Y_m_d_h_i_s'),
                'maintain_ratio' => false,
                'width'          => 1100,
                'height'         => 234,
            );

            $this->upload->initialize($config);
            if (!$this->upload->do_upload('banner_image')) {
                //Print message if file is not uploaded
                $bannererror = array('bannererror' => $this->upload->display_errors());
                $this->load->view("admin/user/edit-store", $bannererror);
            } else {
                $bannerdataDP               = $this->upload->data();
                $bannerimage                = $bannerdataDP['file_name'];
                $data['store_banner_image'] = $bannerimage;
                $this->load->library('image_lib');
                $configt['image_library']  = 'gd2';
                $configt['source_image']   = $bannerdataDP['full_path'];
                $configt['new_image']      = FCPATH . "resources/seller_account_image/banner/thumb/";
                $configt['maintain_ratio'] = false;
                $configt['width']          = 150;
                $configt['height']         = 60;
                $this->image_lib->initialize($configt);
                $this->image_lib->resize();
            }
        }
        if ($logoFiles1['name'] !== "") {
            $confignew = array(
                'allowed_types'  => 'jpg|jpeg|png|txt',
                'upload_path'    => FCPATH . 'resources/seller_account_image/logo/',
                'file_name'      => 'logo_' . date('Y_m_d_h_i_s'),
                'maintain_ratio' => false,
                'width'          => 150,
                'height'         => 110,
            );
            $this->upload->initialize($confignew);
            if (!$this->upload->do_upload('logo_image')) {
                //Print message if file is not uploaded
                $logoerror = array('logoerror' => $this->upload->display_errors());
                $this->load->view("admin/user/edit-store", $logoerror);
            } else {
                $logodataDP               = $this->upload->data();
                $logoimage                = $logodataDP['file_name'];
                $data['store_logo_image'] = $logoimage;
                $this->load->library('image_lib');
                $configt['image_library']  = 'gd2';
                $configt['source_image']   = $logodataDP['full_path'];
                $configt['new_image']      = FCPATH . "resources/seller_account_image/logo/thumb/";
                $configt['maintain_ratio'] = false;
                $configt['width']          = 80;
                $configt['height']         = 80;
                $this->image_lib->initialize($configt);
                $this->image_lib->resize();
            }
        }

        $this->common_model->commonUpdate('tbl_stores', $data, 'store_id', $this->input->post('store_id'));
    }

    public function check_availability()
    {
        $store_name = $this->input->post('store_name');
        $response   = $this->seller_model->check_availability($store_name);
        if ($response == 0) {
            echo json_encode(array('status' => 'Available'));
            die;
        } else {
            $name = $this->suggest_name($store_name);
            echo json_encode(array('status' => 'error', 'new_name' => $name));
            die;
        }
    }

    public function suggest_name($data)
    {
        $new_data = $data . mt_rand(0, 10000);
        $response = $this->seller_model->check_availability($new_data);
        if ($response == 0) {
            return $new_data;
        }
    }

    public function delete_page_img()
    {
        if (!$this->input->is_ajax_request()) {

        } else {
            $id   = $this->input->post('nId');
            $data = array($this->input->post('fieldname') => "");
            $this->db->where('store_id', $id);
            $this->db->update('tbl_stores', $data);
        }
    }

    public function list_addresses($id = '')
    {
        $query = "
                SELECT *
                FROM tbl_user_addreses
                WHERE add_user_id = " . $id . "
                ";

        $data['pageList'] = $this->db->query($query)->result();
        $data['user_id']  = $id;
        $this->load->view('admin/user/address-book', $data);
    }

    public function delete_address($nId = '', $uid = '')
    {
        $this->common_model->commonDelete('tbl_user_addreses', "add_id", $nId);
        $this->session->set_flashdata('msg', 'Address deleted successfully.');
        redirect(base_url() . "admin/user/list_addresses/" . $uid);
    }

    public function add_new_address($uid = '')
    {
        $data['title'] = "Add an Address";
        $this->form_validation->set_rules('user_fname', 'first name', 'trim|required');
        $this->form_validation->set_rules('user_nname', 'Nick name', 'trim|required');
        $this->form_validation->set_rules('user_lname', 'last name', 'trim|required');

        $this->form_validation->set_rules('street', 'Street', 'trim|required');

        $this->form_validation->set_rules('user_phone', 'Phone', 'trim|numeric');
        $this->form_validation->set_rules('user_zip', 'user_zip', 'trim|required');
        $this->form_validation->set_rules('user_country', 'user_country', 'trim|required');
        $this->form_validation->set_rules('city', 'City', 'trim|required');

        if ($this->input->post('user_country') == 'US') {
            $this->form_validation->set_rules('state', 'State', 'trim|required');
        } else {
            $this->form_validation->set_rules('state_other', 'State', 'trim|required');
        }

        $this->form_validation->set_message('required', 'This field is required.');

        if ($this->form_validation->run() == false) {
            $data['user_id'] = $uid;
            $data['states']  = $this->common_model->common_select('tbl_states')->result_array();
            $data['country'] = $this->common_model->common_select('tbl_country')->result_array();
            $this->load->view('admin/user/add-address', $data);
        } else {
            $data_save['add_user_id']  = $uid;
            $data_save['nick_name']    = $this->input->post('user_nname');
            $data_save['first_name']   = $this->input->post('user_fname');
            $data_save['last_name']    = $this->input->post('user_lname');
            $data_save['street']       = $this->input->post('street');
            $data_save['city']         = $this->input->post('city');
            $data_save['created_date'] = date('Y-m-d h:i:s');

            if ($this->input->post('user_country') == 'US') {
                $data_save['state']       = $this->input->post('state');
                $data_save['state_other'] = '';
            } else {
                $data_save['state_other'] = $this->input->post('state_other');
                $data_save['state']       = '';
            }

            $data_save['zip']     = $this->input->post('user_zip');
            $data_save['phone']   = $this->input->post('user_phone');
            $data_save['country'] = $this->input->post('user_country');

            $billing                             = $this->input->post('Billing');
            $shipping                            = $this->input->post('shipping');
            $data_save['default_billing_check']  = 'N';
            $data_save['default_shipping_check'] = 'N';

            if (!empty($billing)) {
                $data_save['default_billing_check']       = 'Y';
                $default_billing['default_billing_check'] = 'N';
                $this->common_model->commonUpdate('tbl_user_addreses', $default_billing, "add_user_id", $uid);
            }
            if (!empty($shipping)) {
                $data_save['default_shipping_check']        = 'Y';
                $default_shipping['default_shipping_check'] = 'N';
                $this->common_model->commonUpdate('tbl_user_addreses', $default_shipping, "add_user_id", $uid);
            }

            $this->common_model->commonSave('tbl_user_addreses', $data_save);
            $this->session->set_flashdata('msg', 'Address added successfully.');
            redirect(base_url() . "admin/user/list_addresses/" . $uid);
        }
    }

    public function edit_address($uid = '', $id = '')
    {
        $data['title'] = "Edit Address";
        $this->form_validation->set_rules('user_fname', 'first name', 'trim|required');
        $this->form_validation->set_rules('user_nname', 'Nick name', 'trim|required');
        $this->form_validation->set_rules('user_lname', 'last name', 'trim|required');

        $this->form_validation->set_rules('street', 'Street', 'trim|required');

        $this->form_validation->set_rules('user_phone', 'Phone', 'trim|numeric');
        $this->form_validation->set_rules('user_zip', 'user_zip', 'trim|required');
        $this->form_validation->set_rules('user_country', 'user_country', 'trim|required');
        $this->form_validation->set_rules('city', 'City', 'trim|required');

        if ($this->input->post('user_country') == 'US') {
            $this->form_validation->set_rules('state', 'State', 'trim|required');
        } else {
            $this->form_validation->set_rules('state_other', 'State', 'trim|required');
        }

        $this->form_validation->set_message('required', 'This field is required.');

        if ($this->form_validation->run() == false) {
            $data['address_id']   = $id;
            $data['user_id']      = $uid;
            $data['states']       = $this->common_model->common_select('tbl_states')->result_array();
            $data['country']      = $this->common_model->common_select('tbl_country')->result_array();
            $data['address_data'] = $this->common_model->commonselect('tbl_user_addreses', 'add_id', $id)->row();
            $this->load->view('admin/user/edit-address', $data);
        } else {
            $data_save['add_user_id'] = $uid;
            $data_save['nick_name']   = $this->input->post('user_nname');
            $data_save['first_name']  = $this->input->post('user_fname');
            $data_save['last_name']   = $this->input->post('user_lname');
            $data_save['street']      = $this->input->post('street');
            $data_save['city']        = $this->input->post('city');

            if ($this->input->post('user_country') == 'US') {
                $data_save['state']       = $this->input->post('state');
                $data_save['state_other'] = '';
            } else {
                $data_save['state_other'] = $this->input->post('state_other');
                $data_save['state']       = '';
            }

            $data_save['zip']     = $this->input->post('user_zip');
            $data_save['phone']   = $this->input->post('user_phone');
            $data_save['country'] = $this->input->post('user_country');

            $billing  = $this->input->post('Billing');
            $shipping = $this->input->post('shipping');

            $data_save['default_billing_check']  = 'N';
            $data_save['default_shipping_check'] = 'N';

            if (!empty($billing)) {
                $data_save['default_billing_check']       = 'Y';
                $default_billing['default_billing_check'] = 'N';
                $this->common_model->commonUpdate('tbl_user_addreses', $default_billing, "add_user_id", $uid);
            }
            if (!empty($shipping)) {
                $data_save['default_shipping_check']        = 'Y';
                $default_shipping['default_shipping_check'] = 'N';
                $this->common_model->commonUpdate('tbl_user_addreses', $default_shipping, "add_user_id", $uid);
            }

            $this->common_model->commonUpdate2('tbl_user_addreses', $data_save, 'add_id', $id, 'add_user_id', $uid);
            $this->session->set_flashdata('msg', 'Address updated successfully.');
            redirect(base_url() . "admin/user/list_addresses/" . $uid);
        }
    }

    public function create_store($id = '')
    {
        $data['title']  = 'Create Store';
        $data['userid'] = $id;
        if ($id == '') {
            redirect(base_url('admin/user'));
        }
        $data['tiers'] = $this->common_model->getCommon('tbl_tier_list', 'tier_id', 'ASC')->result();
        $data['allstate']     = $this->db->query("SELECT stat_id, stat_name FROM tbl_states")->result_array();
        $data['allcountries'] = $this->db->query("SELECT iso, name FROM tbl_country")->result_array();

        $data['is_seller'] = $this->db->query("SELECT * FROM tbl_user WHERE user_id=" . $id)->row();

        if (count($data['is_seller']) <= 0 || $data['is_seller']->user_type == 'Seller') {
            redirect(base_url('admin/user'));
        }

        $this->form_validation->set_rules('store_name', 'Store Name', 'trim|required|is_unique[tbl_stores.store_name]');
        $this->form_validation->set_rules('describe', 'Describe', 'trim|required');
        $this->form_validation->set_rules('paypal_id', 'PayPal ID', 'trim|required|callback_paypal_id');
        $this->form_validation->set_rules('store_address', 'Address', 'trim');
        $this->form_validation->set_rules('store_country', 'Store Country', 'trim|required');
        $this->form_validation->set_rules('store_city', 'Store City', 'trim|required');
        $this->form_validation->set_rules('store_zip', 'Store Zip', 'trim|required');
        $this->form_validation->set_rules('store_number', 'Phone Number', 'trim|is_numeric');
        $this->form_validation->set_rules('store_description', 'Description', 'trim');
        $this->form_validation->set_rules('store_facebook_url', 'Facebook URL', 'trim');
        $this->form_validation->set_rules('store_twitter_url', 'Twitter URL', 'trim');
        $this->form_validation->set_rules('store_instagram_url', 'Instagram URL', 'trim');
        $this->form_validation->set_rules('store_pintrest_url', 'Pinterest URL', 'trim');
        $this->form_validation->set_rules('logo_image', 'Store Logo', 'trim|callback_valid_file[1]');
        $this->form_validation->set_rules('store_banner_image', 'Store Banner', 'trim|callback_valid_file[2]');

        if ($this->input->post('store_country') == "US") {
            $this->form_validation->set_rules('store_state', 'State', 'trim|required');
        } else {
            $this->form_validation->set_rules('other_state', 'State', 'trim|required');
        }

        //$this->form_validation->set_message('is_unique', 'Store Name Not Avaliable');
        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('is_unique', 'Your %s should be unique.');

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/user/create-store', $data);
        } else {
            $tier_id = 1;
            $user_id = $this->input->post('user_id');

            $tier['tier_data'] = $this->common_model->commonSelect('tbl_tier_list', 'tier_id', $tier_id)->row();

            $store_data['user_id']            = $this->input->post('user_id');
            $store_data['store_name']         = $this->input->post('store_name');
            $store_data['store_url']          = $this->common_model->create_slug($this->input->post('store_name'), 'tbl_stores', 'store_url');
            $store_data['store_describe']     = $this->input->post('describe');
            $store_data['store_tier_id']      = $tier_id;
            $store_data['store_paypal_id']    = $this->input->post('paypal_id');
            $store_data['store_created_date'] = date('Y-m-d h:i:s');
            $store_data['store_end_date']     = date('Y-m-d h:i:s');
            $store_data['store_address']      = $this->input->post('store_address');
            $store_data['store_city']         = $this->input->post('store_city');
            $store_data['store_country']      = $this->input->post('store_country');
            if ($store_data['store_country'] == "US") {
                $store_data['store_state'] = $this->input->post('store_state');
            } else {
                $store_data['store_state'] = $this->input->post('other_state');
            }
            $store_data['store_zip']           = $this->input->post('store_zip');
            $store_data['store_number']        = $this->input->post('store_number');
            $store_data['store_description']   = $this->input->post('store_description');
            $store_data['store_facebook_url']  = $this->input->post('store_facebook_url');
            $store_data['store_twitter_url']   = $this->input->post('store_twitter_url');
            $store_data['store_instagram_url'] = $this->input->post('store_instagram_url');
            $store_data['store_pintrest_url']  = $this->input->post('store_pintrest_url');
            $store_data['store_longitude']     = ($this->input->post('store_longitude') ? $this->input->post('store_longitude') : "");
            $store_data['store_latitude']      = ($this->input->post('store_latitude') ? $this->input->post('store_latitude') : "");
            $bannerFiles1                      = $_FILES['store_banner_img'];
            $logoFiles1                        = $_FILES['logo_img'];
            $logoimage                         = '';
            $bannerimage                       = '';
            if ($bannerFiles1['name'] !== "") {
                $config = array(
                    'allowed_types'  => 'jpg|jpeg|png',
                    'upload_path'    => FCPATH . 'resources/seller_account_image/banner/',
                    'file_name'      => 'banner_' . date('Y_m_d_h_i_s'),
                    'maintain_ratio' => false,
                    'width'          => 1100,
                    'height'         => 234,
                );
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('store_banner_img')) {
                } else {
                    $bannerdataDP = $this->upload->data();
                    $bannerimage  = $bannerdataDP['file_name'];
                    $this->load->library('image_lib');
                    $configt['image_library']  = 'gd2';
                    $configt['source_image']   = $bannerdataDP['full_path'];
                    $configt['new_image']      = FCPATH . "resources/seller_account_image/banner/thumb/";
                    $configt['maintain_ratio'] = false;
                    $configt['width']          = 180;
                    $configt['height']         = 80;
                    $this->image_lib->initialize($configt);
                    $this->image_lib->resize();
                }
            }
            if ($logoFiles1['name'] !== "") {

                $confignew = array(
                    'allowed_types'  => 'jpg|jpeg|png',
                    'upload_path'    => FCPATH . 'resources/seller_account_image/logo/',
                    'file_name'      => 'logo_' . date('Y_m_d_h_i_s'),
                    'maintain_ratio' => false,
                    'width'          => 150,
                    'height'         => 110,
                );
                $this->upload->initialize($confignew);
                if (!$this->upload->do_upload('logo_img')) {
                } else {
                    $logodataDP = $this->upload->data();
                    $logoimage  = $logodataDP['file_name'];
                    $this->load->library('image_lib');
                    $configt['image_library']  = 'gd2';
                    $configt['source_image']   = $logodataDP['full_path'];
                    $configt['new_image']      = FCPATH . "resources/seller_account_image/logo/thumb/";
                    $configt['maintain_ratio'] = false;
                    $configt['width']          = 80;
                    $configt['height']         = 80;
                    $this->image_lib->initialize($configt);
                    $this->image_lib->resize();
                }
            }

            $store_data['store_logo_image']   = ($logoimage == '' ? $this->input->post('logo_img') : $logoimage);
            $store_data['store_banner_image'] = ($bannerimage == '' ? $this->input->post('store_banner_img') : $bannerimage);

            $store_id = $this->common_model->commonSave('tbl_stores', $store_data);

            // Update User Table
            $user_data['user_type'] = 'Seller';
            $this->common_model->commonUpdate('tbl_user', $user_data, 'user_id', $user_id);

            // create subscription record

            $_SESSION['usertype']               = $user_data['user_type'];
            $subscription_data['sub_tier_id']   = $tier_id;
            $subscription_data['sub_store_id']  = $store_id;
            $subscription_data['sub_start']     = date('Y-m-d h:i:s');
            $subscription_data['sub_ends']      = date('Y-m-d h:i:s');
            $subscription_data['sub_free_tier'] = 'Y';
            $subscription_data['tier_price']    = $tier['tier_data']->tier_amount;

            $subscription_id = $this->common_model->commonSave('tbl_user_store_subscription', $subscription_data);

            redirect(base_url('admin/user'));
        }
    }

    public function valid_file($name, $fileNo = '')
    {
        if ($name != "" && !empty($name) && $name != "Upload File") {
            $img_data = '';
            $width    = '';
            $height   = '';
            if ($fileNo == 1) {
                $path     = $_FILES['logo_img']['tmp_name'];
                $img_data = getimagesize($path);
                $width    = $img_data[0];
                $height   = $img_data[1];
            } else {
                $path     = $_FILES['store_banner_img']['tmp_name'];
                $img_data = getimagesize($path);
                $width    = $img_data[0];
                $height   = $img_data[1];
            }
            $ext = explode('.', $name);
            $ext = strtolower(end($ext));
            if ($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "png") {
                return true;
                /*if ($fileNo == 1) {
                    if ($width > 150 && $height > 110) {
                        $this->form_validation->set_message('valid_file', 'The file size you are attempting to upload is larger.');
                        return false;
                    } else {
                        return true;
                    }
                } elseif ($fileNo == 2) {
                    if ($width > 1100 && $height > 234) {
                        $this->form_validation->set_message('valid_file', 'The file size you are attempting to upload is larger.');
                        return false;
                    } else {
                        return true;
                    }
                }*/
            } else {
                $this->form_validation->set_message('valid_file', 'The file type you are attempting to upload is not allowed.');
                return false;
            }
        } else {
            $this->form_validation->set_message('valid_file', 'This field is required.');
            return false;
        }
    }

    public function edit_valid_file($name, $fileNo = '')
    {
        if ($name != "" && !empty($name) && $name != "Upload File") {
            $img_data = '';
            $width    = '';
            $height   = '';
            if ($fileNo == 1) {
                $path     = $_FILES['logo_image']['tmp_name'];
                $img_data = getimagesize($path);
                $width    = $img_data[0];
                $height   = $img_data[1];
            } else {
                $path     = $_FILES['banner_image']['tmp_name'];
                $img_data = getimagesize($path);
                $width    = $img_data[0];
                $height   = $img_data[1];
            }
            $ext = explode('.', $name);
            $ext = strtolower(end($ext));
            if ($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "png") {
                return true;
                /*if ($fileNo == 1) {
                    if ($width > 150 && $height > 110) {
                        $this->form_validation->set_message('edit_valid_file', 'The file size you are attempting to upload is larger.');
                        return false;
                    } else {
                        return true;
                    }
                } elseif ($fileNo == 2) {
                    if ($width > 1100 && $height > 234) {
                        $this->form_validation->set_message('edit_valid_file', 'The file size you are attempting to upload is larger.');
                        return false;
                    } else {
                        return true;
                    }
                }*/
            } else {
                $this->form_validation->set_message('edit_valid_file', 'The file type you are attempting to upload is not allowed.');
                return false;
            }
        } else {
            $this->form_validation->set_message('edit_valid_file', 'This field is required.');
            return false;
        }
    }

    public function paypal_id($paypal_id){
        $user_data =  $this->db->query("
                            SELECT * FROM tbl_stores store
                            INNER JOIN tbl_user user ON store.user_id = user.user_id
                            WHERE store.store_paypal_id = '".$paypal_id."' AND user.user_is_delete = 0")->row();
        if(count($user_data)>0)
        {
            $this->form_validation->set_message('paypal_id', 'Your Paypal ID should be unique.');
            return false;
        }else{
            return true;
        }
    }


    public function  buyer_orders($user_id){
        if ($this->input->post('date_range')){
            $date = $this->input->post('date_range');
            if ($date!='') {
                $expload_date = explode("to", $date);
                @$date1 = $expload_date[0];
                @$date2 = $expload_date[1];
                $where .= "AND (tbl_orders.order_date between '$date1' and '$date2')";
            }
        }        
        if ($this->input->get('srchOp') == 'status') {
            $where .= "AND tbl_orders.order_status LIKE '%" . $this->input->get('searchText') . "%'";
        } elseif ($this->input->get('srchOp') == 'id') {
            $where .= "AND tbl_orders.order_id LIKE '%" . $this->input->get('searchText') . "%'";
        }

        $query="SELECT *
                FROM tbl_orders 
                WHERE buyer_id = ".$user_id." ".$where."
                ORDER BY order_id DESC
                 ";

        $data['rstCategories'] =  $this->db->query($query)->result();
        $this->load->view('admin/user/listorder', $data);
    }


    public function view_order($order_id){
        $data['title'] = 'Order Detail';
        $data['order_details'] =  $this->db->query("
                            SELECT * FROM tbl_orders o
                            WHERE o.`order_id` = ".$order_id."")->row();
        $data['order_products'] = $this->db->query("SELECT * FROM tbl_order_products o WHERE o.`order_id` = ".$order_id."")->result();
        $data['order_sellers'] = $this->order_model->order_sellers($order_id);
        $data['b_crums'] = "<ul><li><a href='".base_url()."buyer_orders/".$data['order_details']->buyer_id."'>User Orders</a></li><li><div class='crms-sep'>&gt;</div></li><li><a href='javascript:void(0);'>".$data['title']."</a></li></ul>";
        $this->load->view('admin/user/order_detail', $data);
    }

}
