<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Catalog extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();
        auth();
        set_time_limit(0);
    }

    public function index()
    {
        $data['top_cat']     = $this->get_top_categories();
        $data['seller_data'] = $this->get_seller_data();
        /*echo "<pre>";
        print_r($data);
        exit;*/
        //$data['top_cat'] = array();
        $this->load->view('admin/catalog/catalog.php', $data);
    }

    public function get_top_categories()
    {
        $data['top_categories'] = $this->db->query("
                                        SELECT count(prod.prod_id) as total,cat.cat_name
                                        FROM tbl_products as prod
                                        INNER JOIN tbl_categories as cat ON cat.cat_id = prod.prod_cat_id
                                        INNER JOIN tbl_stores as store ON store.store_id = prod.prod_store_id
                                        INNER JOIN tbl_user as user ON user.user_id = store.user_id
                                        WHERE prod.prod_is_delete = 0 AND store.store_is_hide = 0 AND user.user_is_delete = 0 AND user.user_status = 1 AND prod.prod_status = 1
                                        GROUP BY cat.cat_id
                                        ORDER BY total DESC
                                        LIMIT 5
                                        ")->result();

        return $data['top_categories'];
    }

    public function get_seller_data()
    {
        $data['active_sellers'] = $this->db->query("
                                                SELECT COUNT(u.user_id) as ActiveSellers
                                                FROM tbl_user u 
                                                INNER JOIN tbl_stores s ON s.user_id = u.user_id 
                                                WHERE u.user_status = 1 AND u.user_is_delete = 0 AND u.user_type = 'Seller' AND s.store_is_hide = 0
                                        ")->row();

        $data['seller_vacation'] = $this->db->query("
                                        SELECT COUNT(store_id) as SellersOnVacation
                                        FROM tbl_stores s
                                        INNER JOIN tbl_user u ON u.user_id = s.user_id 
                                        WHERE store_is_hide = 1 AND user_status = 1 AND user_is_delete = 0 AND 
                                        user_type = 'Seller'
                                        ")->row();

        $data['cart_items'] = $this->db->query("
                                        SELECT count(cart.cart_detail_id) as total
                                        FROM tbl_cart_detail cart
                                        INNER JOIN tbl_products prod ON prod.prod_id = cart.cart_prod_id
                                        INNER JOIN tbl_stores as store ON store.store_id = prod.prod_store_id 
                                        INNER JOIN tbl_user as user ON user.user_id = store.user_id
                                        WHERE user.user_is_delete = 0 AND prod.prod_is_delete = 0
                                        ")->result();

        $total_prod = $this->db->query("
                                        SELECT count(prod_id) as total_prod
                                        FROM tbl_products prod
                                        INNER JOIN tbl_stores as store ON store.store_id = prod.prod_store_id 
                                        INNER JOIN tbl_user as user ON user.user_id = store.user_id
                                        WHERE prod_is_delete = 0 AND user.user_is_delete = 0
                                        ")->row('total_prod');

        $total_stores = $this->db->query("
                                        SELECT count(user.user_id) as total
                                        FROM tbl_user as user
                                        INNER JOIN tbl_stores as store ON store.user_id = user.user_id
                                        WHERE user.user_status = 1 AND user.user_is_delete = 0
                                        ")->row('total');

        $avergae = $total_prod / $total_stores;

        $data['average_prod'] = number_format((float) $avergae, 2, '.', '');

        return $data;
    }
}
