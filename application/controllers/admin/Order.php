<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Order extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('order/order_model');
        $this->load->helper('url'); // load URL helper
        auth();
    }

    public function index() {
        $data['GMV'] = $this->db->query("SELECT SUM(order_grand_total) as sum FROM tbl_orders WHERE order_status = 'completed';")->row()->sum;
        $data['transactions_number'] = $this->db->query("SELECT COUNT(order_sub_total) as count FROM tbl_orders WHERE order_status = 'completed';")->row()->count;
        if($data['GMV']>0 && $data['transactions_number']>0){
            $data['avg_order_value'] = $data['GMV']/$data['transactions_number'];
        }else{
            $data['avg_order_value'] = 0;
        }
        $data['Totalcommission'] = $this->order_model->getTotalcommission()->educki_commission;
        $data['title'] = "Manage Orders";
        $data['list'] = $this->order_model->order_list();
        /*echo "<pre>";
        print_r($data['list']);
        exit;*/
        if ($this->input->post('date_range')) {
            $date = $this->input->post('date_range');
            $data['list'] = $this->order_model->order_list($date);
        }
        $this->load->view('admin/order/order_list', $data);
    }

    public function view_orders($order_id) {
        $data['title'] = 'Order Detail';
        $data['order_details'] = $this->order_model->order_details($order_id);
        $data['order_products'] = $this->order_model->order_products($order_id);
        $data['order_sellers'] = $this->order_model->order_sellers($order_id);
        $data['b_crums'] = "<ul><li><a href='".base_url()."admin/order'>Manage Orders</a></li><li><div class='crms-sep'>&gt;</div></li><li><a href='javascript:void(0);'>".$data['title']."</a></li></ul>";
        /*echo "<pre>";
        print_r($data);
        exit;*/
        $this->load->view('admin/user/order_detail', $data);    
    }

    public function exportcsv()
    {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=orders_' . date("m-d-Y") . '.csv');
        $output = fopen('php://output', 'w');
        fputcsv($output, array('Order ID','Buyer Name', 'Order Status', 'Date Ordered', 'Date Completed', 'Total Sale Amount'));
        $resultsProd = $this->getOrderExport();
        for ($n = 0; $n < count($resultsProd); $n++) {

            if ($resultsProd[$n]['order_completion_date'] == ''){
                $resultsProd[$n]['order_completion_date'] = "N/A";
            }else{
                $resultsProd[$n]['order_completion_date'] = date('m-d-Y', strtotime($resultsProd[$n]['order_completion_date']));
            }

            $resultsProd[$n]['order_date'] = date('m-d-Y', strtotime($resultsProd[$n]['order_date']));

            fputcsv($output, $resultsProd[$n]);
        }
    }

    function  getOrderExport()
    {
        $query="SELECT order_id,buyer_name,order_status,order_date,order_completion_date,order_grand_total
                FROM tbl_orders
                ORDER BY order_id DESC
                 "; 
        $query = $this->db->query($query);
  
        if ($query->num_rows() > 0){
            return $query->result_array();
        }
        else{
            return NULL;
        }
    }

}
