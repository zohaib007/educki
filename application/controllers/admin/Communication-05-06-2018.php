<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Communication extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('product/product_model');
        $this->load->model('careers/career_model');
        $this->load->model('communication_model');
        
        auth();
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
    function emailtrigger_list() {
        $strwhere = "";
        $data['MetaTitle'] = "Admin - Email Manager";
        $total_records = count($this->communication_model->getEmailMessages($strwhere));
        $data['rstCategories'] = $this->communication_model->getEmailMessages($strwhere);
        $data['total_records'] = $total_records;
        $this->load->view('admin/email/list_email_messages', $data);
    }

    //MMMMMMMMMMMMMMMMMMMMMMM  EDIT EMAIL MESSAGE    MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

    public function emailtrigger_edit($id = '') {

        if ($id == '' || !is_numeric($id)) {
            redirect(base_url('admin/communication/emailtrigger_list'));
            exit;
        }

        $data['resultsArray'] = $this->communication_model->edit_emailtrigger($id)->row();

        $this->form_validation->set_rules('nl_title', 'Title', 'trim|required');
        $this->form_validation->set_rules('nl_text', 'nl_text', 'trim|required');
        $this->form_validation->set_rules('nl_subject', 'Subject', 'trim|required');
        $this->form_validation->set_rules('email_status', 'Title', 'trim|required');
        $this->form_validation->set_rules('nl_to', 'To', 'trim');

        $this->form_validation->set_message('required', 'This field is required.');

        if ($this->form_validation->run() == FALSE) {

            $this->load->view('admin/email/edit_news', $data);
        } else {
            $date = date('Y-m-d H:i:s');

            $array = array(
                'e_email_title' => $this->input->post('nl_title'),
                'e_email_subject' => $this->input->post('nl_subject'),
                'e_email_text' => stripcslashes($this->input->post('nl_text')),
                'is_active' => $this->input->post('email_status')
            );

            $this->communication_model->update_emailtrigger($array, $id);
            $this->session->set_flashdata('msg', 'Item updated successfully.');

            if (trim($this->input->post('nl_to')) != '') {

                $is_send2 = $this->send_email_manual($id, $this->input->post('nl_to'));
                $this->session->set_flashdata('msg', 'Email trigger sent successfully.');
            }
            redirect(base_url() . "admin/communication/emailtrigger_list");
            exit;
        }
    }

    /////////////////////////////subscriber functions//////////////////////////
    function subscriber() {
        $data['sub_data'] = $this->communication_model->listing_sub();
        $this->load->view('admin/subscriber/list', $data);
    }

    public function sub_exportcsv() {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=Email-Subscribers-' . date("m-d-Y") . '.csv');
        $output = fopen('php://output', 'w');
        fputcsv($output, array('Created date', 'Emails'));
        $resultsProd = $this->communication_model->export($strWere = "");
        for ($n = 0; $n < count($resultsProd); $n++) {
            fputcsv($output, $resultsProd[$n]);
        }
    }

    public function sub_exportexcelxml() {
        $arr = "";
        $resultsProd = $this->communication_model->export($strWere = "");
        for ($n = 0; $n < count($resultsProd); $n++) {
            $arr[] = array(date('m-d-Y', strtotime($resultsProd[$n]['sub_created_date'])), $resultsProd[$n]['sub_email']);
        }

        $arrTitle = array('Created date', 'Emails');
        $data[] = $arrTitle;
        for ($n = 0; $n < count($arr); $n++) {
            $data[] = $arr[$n];
        }
        $sheetname = array('filename' => 'subscribers-' . date('d-m-Y'));
        $this->load->library('XSpreadsheet', $sheetname);
        $this->xspreadsheet->AddWorksheet('Awesome Sheet', $data)->Generate()->Send();
    }

    /* ---------------------retrive user email check ------------------ */

    function add_sub() {

        $this->load->view('admin/subscriber/add_sub');
    }

    function submit_sub() {

        $this->form_validation->set_rules('sub_email', 'Email', 'trim|required|valid_email');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/subscriber/add_sub');
        } else {
            $date = date('Y-m-d H:i:s');

            $array = array('sub_email' => $this->input->post('sub_email'), 'date_created' => $date);
            $this->communication_model->add_sub($array);
            redirect(base_url() . "admin/communication/subscriber");
            exit;
        }
    }

    function deltesub($id) {


        $this->communication_model->commonDelete('tbl_subscriber', "sub_id", $id);
        $this->session->set_flashdata('msg', 'Item deleted successfully');

        redirect(base_url() . "admin/communication/subscriber");
        exit;
        // echo $this->db->last_query();		
    }

    function chnage_contact_status($id){
        $result = $this->common_model->commonUpdate('tbl_contact_form', $_POST, "c_id", $id);
        if($result){
            $this->session->set_flashdata('msg', 'Item Updated successfully');
            redirect(base_url() . "admin/communication/contact");
        }
    }

    function send_email() {
        $id = $this->uri->segment(4);
        $data['resultsChannel'] = $this->communication_model->edit_news($id);
        $data['resultsChannel'][0]['nl_from'];
        $data['resultsChannel'][0]['nl_subject'];
        $data['resultsChannel'][0]['nl_text'];
        $sub_listing = $this->communication_model->listing_sub();

        $count = count($sub_listing);

        for ($n = 0; $n < $count; $n++) {
            $sub_listing[$n]['sub_email'];
            $this->load->library('email');
            $this->email->from($data['resultsChannel'][0]['nl_from'], 'Your Name');
            $this->email->to($sub_listing[$n]['sub_email']);
            $this->email->subject($data['resultsChannel'][0]['nl_subject']);
            $this->email->message($data['resultsChannel'][0]['nl_text']);
            $this->email->send();
        }
        $this->session->set_flashdata('notify', 'Your email has been send');
        redirect(base_url() . "admin/communication");
        //echo $this->email->print_debugger();
    }

    function contact() {
        $data['title'] = "Contact";

        $total_records = count($this->common_model->contact_communication_listing('tbl_contact_form'));

        $perPage = 100;
        $last = end($this->uri->segments);
        if ($last > 0)
            $page = $last;
        else
            $page = 0;


        $data['results'] = $this->common_model->contact_communication_listing('tbl_contact_form', $perPage, $page);
        $data['total_records'] = $total_records;
        $mypaing['total_rows'] = $total_records;
        $mypaing['base_url'] = base_url() . "admin/communication/contact/index";
        $mypaing['per_page'] = $perPage;
        $mypaing['uri_segment'] = 5;
        $mypaing['full_tag_open'] = '<div class="pagination-rp">';
        $mypaing['full_tag_close'] = '</div>';
        $mypaing['cur_tag_open'] = '<div class="pagination-rp">';
        $mypaing['cur_tag_close'] = '</div>';
        $this->pagination->initialize($mypaing);
        $data['paginglink'] = $this->pagination->create_links();

        $this->load->view('admin/communication/listcontactus', $data);
    }

    function contact_detail($nId) {
        $data['title'] = "Contact Form Details";
        $data['results'] = $this->common_model->common_where('tbl_contact_form', array('c_id' => $nId));
        $this->load->view('admin/communication/contact_detail', $data);
    }

    function dropship_forms() {
        $data['title'] = "Dropship Form";

        $total_records = count($this->communication_model->get_dropship_data('tbl_drop_form'));

        $perPage = 100;
        $last = end($this->uri->segments);
        if ($last > 0)
            $page = $last;
        else
            $page = 0;


        $data['results'] = $this->communication_model->get_dropship_data('tbl_drop_form', $perPage, $page);
        $data['total_records'] = $total_records;
        $mypaing['total_rows'] = $total_records;
        $mypaing['base_url'] = base_url() . "admin/communication/contact/index";
        $mypaing['per_page'] = $perPage;
        $mypaing['uri_segment'] = 5;
        $mypaing['full_tag_open'] = '<div class="pagination-rp">';
        $mypaing['full_tag_close'] = '</div>';
        $mypaing['cur_tag_open'] = '<div class="pagination-rp">';
        $mypaing['cur_tag_close'] = '</div>';
        $this->pagination->initialize($mypaing);
        $data['paginglink'] = $this->pagination->create_links();

        $this->load->view('admin/communication/listdropship', $data);
    }

    function dropdetail($nId) {
        $data['title'] = "Dropship Details";

        $data['results'] = $this->common_model->common_where('tbl_drop_form', array('drop_id' => $nId))->row_array();
        $data['state'] = $this->common_model->common_where('tbl_states', array('stat_id' => $data['results']['ustate']))->row_array();

        $this->load->view('admin/communication/drop_detail', $data);
    }

    function catlog_detail($nId) {
        $data['title'] = "Catalog Details";
        $data['results'] = $this->common_model->common_where('tbl_catlog', array('id' => $nId));
        $this->form_validation->set_rules('catlog_status', 'status', 'trim|xss_clean|strip_tags|required');
        $this->form_validation->set_message('required', 'This field is required.');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/communication/catlog_detail', $data);
        } else {
            $array['catlog_status'] = $this->input->post('catlog_status');

            $this->common_model->commonUpdate('tbl_catlog', $array, "id", $nId);
            $this->session->set_flashdata('msg', 'Item updated successfully.');
            redirect(base_url() . "admin/communication/request_catlog");
            exit;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    function request_catlog() {
        $data['title'] = "Catalog";
        $total_records = count($this->common_model->getCommon_pagination('tbl_catlog'));
        $perPage = 100;
        $last = end($this->uri->segments);
        if ($last > 0)
            $page = $last;
        else
            $page = 0;

        $data['results'] = $this->common_model->getCommon_pagination('tbl_catlog', $perPage, $page);
        $data['total_records'] = $total_records;
        $mypaing['total_rows'] = $total_records;
        $mypaing['base_url'] = base_url() . "admin/communication/contact/index";
        $mypaing['per_page'] = $perPage;
        $mypaing['uri_segment'] = 5;
        $mypaing['full_tag_open'] = '<div class="pagination-rp">';
        $mypaing['full_tag_close'] = '</div>';
        $mypaing['cur_tag_open'] = '<div class="pagination-rp">';
        $mypaing['cur_tag_close'] = '</div>';
        $this->pagination->initialize($mypaing);
        $data['paginglink'] = $this->pagination->create_links();

        $this->load->view('admin/communication/listcatlog', $data);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    function commdelete($nId) {

        $this->common_model->commonDelete('tbl_drop_form', "drop_id", $nId);
        $this->session->set_flashdata('msg', 'Item deleted successfully');
        redirect("admin/communication/dropship_forms");
    }

    function condelete($nId) {

        $this->common_model->commonDelete('tbl_contact_form', "c_id", $nId);
        $this->session->set_flashdata('msg', 'Item deleted successfully');
        redirect(base_url("admin/communication/contact"));
    }

    function catlogdelete($nId) {

        $this->common_model->commonDelete('tbl_catlog', "id", $nId);
        $this->session->set_flashdata('msg', 'Item deleted successfully');
        redirect("admin/communication/request_catlog");
    }

    public function reviews($page = 0) {

        $data['title'] = "Reviews";

        $strWere = " AND prod_is_delete = 0";

        $total_records = count($this->product_model->product_review_listing($strWere, "", "", 0));
        $perPage = 50;
        $last = end($this->uri->segments);
        if ($last > 0) {
            $page = $last;
        } else {
            $page = 0;
        }

        $data['review_result'] = $this->product_model->product_review_listing($strWere, $perPage, $page, 1);

        $data['total_records'] = $total_records;
        $mypaing['total_rows'] = $total_records;
        $mypaing['base_url'] = base_url() . "admin/communication/reviews/";
        $mypaing['per_page'] = $perPage;
        $mypaing['uri_segment'] = 4;
        $mypaing['full_tag_open'] = '<div class="pagination-rp">';
        $mypaing['full_tag_close'] = '</div>';
        $mypaing['cur_tag_open'] = '<div class="pagination-rp">';
        $mypaing['cur_tag_close'] = '</div>';
        $this->pagination->initialize($mypaing);
        $data['paginglink'] = $this->pagination->create_links();
        $data['title'] = 'Reviews';

        $this->load->view('admin/communication/list_reviews', $data);
    }

    public function review_detail($nId = '') {
        if (!is_numeric($nId)) {
            redirect('admin/communication/reviews/');
            exit;
        }
        $data['title'] = 'Review Detail';

        $strWere = "AND review_id = " . $nId . " AND prod_is_delete = 0 ";
        $data['review_result'] = $this->product_model->product_review_listing($strWere);
        $this->form_validation->set_rules('review_status', 'Status', 'trim|xss_clean|required');
        $this->form_validation->set_message('required', 'This field is required.');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/communication/review_detail', $data);
        } else {

            $array['review_status'] = $this->input->post('review_status');
            $this->common_model->commonUpdate('tbl_reviews', $array, "review_id", $nId);
            $this->session->set_flashdata('msg', 'Item updated successfully.');
            redirect('admin/communication/reviews');
            exit;
        }
    }

    public function review_delete($nId = '') {


        if (!is_numeric($nId)) {
            redirect('admin/communication/reviews/');
            exit;
        }

        $this->common_model->commonDelete('tbl_reviews', "review_id", $nId);
        $this->session->set_flashdata('msg', 'Item deleted successfully');
        redirect("admin/communication/reviews");
        exit;
    }

    private function send_email_manual($email_id, $all_emails) {
        $email_data = $this->common_model->commonselect('tbl_email_temp', 'e_email_id', $email_id)->row_array();

        $message['message'] = $email_data['e_email_text'];
        //$message['email_title'] = $email_data['e_email_title'];
        //$message['message'] = $message; //$this->load->view('email_temp/email-template', $message, true);
        //$send_email['user_email'] = $all_emails;
        //$send_email['user_email'] = rtrim($send_email['user_email'],",")."";

        $send_email['subject'] = $email_data['e_email_subject'];
        $send_email['message'] = $message['message'];
        $user_emails = trim($all_emails, ",");
        $user_emails = explode(",", $user_emails);
        foreach ($user_emails as $emails) {
            $send_email['user_email'] = $emails;

            $this->send_email_to_user($send_email);
        }
        return true;
    }

    private function send_email_to_user($data) {

        $site_email = $this->db->get('tbl_site_setting')->row_array();
        $msg = str_replace('../../../', base_url(), $data['message']);
        send_email($data['user_email'], "no-reply@educki.com", $data['subject'], $msg);
        return TRUE;
        
    }

    function appliedusers() {
        $data['title'] = "Careers";
        $where = '';
        $data['total_records'] = count($this->common_model->get_data_page("tbl_careers_applied_user", $where)->result_array());
        $jointable = 'tbl_career';
        $joincondition = 'tbl_career.career_id = tbl_careers_applied_user.career_id';
        $jointype = 'inner';
        $data['list'] = $this->career_model->career_applied_user_listing("tbl_careers_applied_user", $where, $jointable, $joincondition, $jointype, '', '', $is_count = 0, 'career_user_id', $order = 'DESC')->result_array();
        $this->load->view('admin/communication/applied_career_user_list', $data);
    }

    function appliedusers_details($user_id) {
        $data['title'] = "Applied user Details";
        $data['results'] = $this->career_model->view_details($user_id);
        $this->load->view('admin/communication/applied_career_user_detail', $data);
    }

    function appliedusers_delete($user_id) {
        $this->career_model->appliedusers_delete($user_id);
        $this->session->set_flashdata('msg', 'Item deleted successfully.');
        redirect(base_url("admin/communication/appliedusers"));
        exit;
    }

#########################################################################################################################################	
}

?>