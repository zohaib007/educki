<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Homepage extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
        
        $this->load->library('upload');
        auth();
    }
	
	public function validate_alphabet_space($value)
	{
		if(preg_match("/^[a-zA-Z\s]+$/",$value))
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('validate_alphabet_space', "Only alphabets, or spaces.");
			return false;
		}
		
	}
	
	public function is_valid_domain_name($domain_name)
	{
		$validate =  (preg_match("|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i", $domain_name)); //length of each label
		if($validate == false)
		{
			$this->form_validation->set_message('is_valid_domain_name', 'Invalid domain name.');
			return false;
		}
			
	}
	
	public function is_valid_file_size($file_name, $test)
	{		
		if($test == false)
		{
			$this->form_validation->set_message('is_valid_file_size', 'File image should be greater than 1000 x 336.');
			return false;
		}
			
	}
	function valid_file($name, $tmp_name)
	{
		list($width, $height) = getimagesize($tmp_name);
		
		
		if($name != "" && !empty($name))
		{	
			$ext = explode('.',$name);
			$ext = end($ext);
			if ($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "png")
			{
				if($width >= 236 && $height >= 236 )
				{
					return true;
				}
				else
				{
					$this->form_validation->set_message('valid_file','File image should be greater than 236 x 236.');
					return false;
				}
				
				
			}
			else
			{
				$this->form_validation->set_message('valid_file', 'The filetype you are attempting to upload is not allowed.');
				return false;
			}
		}
		else
		{
			$this->form_validation->set_message('valid_file', 'This field is required.');
			return false;
		}
	}
	
	
	public function des_check($str)
	{
		$test = strip_tags($str);
		
		
		if ($test == '')
		{
			$this->form_validation->set_message('des_check', 'This field is required.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	
	function valid_feature_file($name, $tmp_name)
	{
		list($width, $height) = getimagesize($tmp_name);
		
		
		if($name != "" && !empty($name))
		{	
			$ext = explode('.',$name);
			$ext = end($ext);
			if ($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "png")
			{
				if($width >= 331 && $height >= 268 )
				{
					return true;
				}
				else
				{
					$this->form_validation->set_message('valid_feature_file','Image size should not be greater than 331 x 268.');
					return false;
				}
				
				
			}
			else
			{
				$this->form_validation->set_message('valid_feature_file', 'The filetype you are attempting to upload is not allowed.');
				return false;
			}
		}
		else
		{
			$this->form_validation->set_message('valid_feature_file', 'This field is required.');
			return false;
		}
	}
	function valid_slider_file($name, $tmp_name)
	{
		list($width, $height) = getimagesize($tmp_name);
		
		if($name != "" && !empty($name))
		{	
			$ext = explode('.',$name);
			$ext = end($ext);
			if ($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "png")
			{
				if($width >= 1000 && $height >= 336 )
				{
					return true;
				}
				else
				{
					$this->form_validation->set_message('valid_slider_file','File image should be greater than 1000 x 336.');
					return false;
				}
				
				
			}
			else
			{
				$this->form_validation->set_message('valid_slider_file', 'The filetype you are attempting to upload is not allowed.');
				return false;
			}
		}
		else
		{
			$this->form_validation->set_message('valid_slider_file', 'This field is required.');
			return false;
		}
	}
	
    public function index()
	{
		$result['title'] = "Main Slider";
        $result['data'] = $this->db->query("SELECT * FROM tbl_homepage WHERE home_type = 1 ORDER BY home_id DESC")->result_array();        
        $this->load->view('admin/homepage/list_home_page_slider',$result);
    }
	

	
    public function add_slider() 
	{
		read_only_user();
		$result['title'] = "Add Slider";
		$this->form_validation->set_rules('url', 'name', 'required|callback_is_valid_domain_name');
		$this->form_validation->set_rules('home_status', 'Status', 'required');
		$this->form_validation->set_message('required', 'This field is required.');
		if(isset($_FILES['file']['name']) && $_FILES['file']['name'] !='')
		{
			list($width, $height) = getimagesize($_FILES['file']['tmp_name']);
			
			if($width < 1000 || $height < 336)
			{
				$this->form_validation->set_rules('file', 'image', 'required');
				$this->form_validation->set_message('required', 'File image should be greater than 1000 x 336.');
			}
		}
		else
		{
			$this->form_validation->set_rules('file', 'image', 'required');
		}
		
		
		if ($this->form_validation->run() == FALSE) 
		{
			$this->load->view('admin/homepage/add_slider', $result);
		}
		else
		{
			
			//////////////////////////////////////////// upload multiypal image
			$myFiles = $_FILES['file'];
			$fileDP = array();
		
				if($myFiles['name'] !== "")
				{
					$config = array(
								'allowed_types' => 'jpg|jpeg|gif|png',
								'upload_path' => './resources/homepage_image/',
								'file_name' => time(),
								
								);
					$this->load->library('upload');
					$this->upload->initialize($config);
					//$this->upload->do_upload('file');
					if ( ! $this->upload->do_upload('file'))
					{ //Print message if file is not uploaded
						$this->session->set_flashdata('msg', $this->upload->display_errors());
						header("Location: ".base_url()."admin/homepage/add_slider/".$id);
						exit();
					}
					else
					{
						// //////////////////////////////////////////////uplaod thumbnail image
						$dataDP = $this->upload->data();
						$fileDP = $dataDP['file_name'];
						$this->load->library('image_lib');
						$configt['image_library'] = 'gd2';
						$configt['source_image'] = $dataDP['full_path'];
						$configt['new_image'] = "./resources/homepage_image/thumb/";
						$configt['maintain_ratio'] = FALSE;
						$configt['width'] = 80;
						$configt['height'] = 80;
						$this->image_lib->initialize($configt);
						$this->image_lib->resize();
					}
				}
				else
				{
					$fileDP = "";
				}
			
			///////////////////////////////////////image upload code end///////////////////////////////////////////////
			$array = array('home_url' => $this->input->post('url'),'home_status' => $this->input->post('home_status'),'home_date' => date('Y-m-d'), 'home_image' => $fileDP,'home_type' => $this->input->post('slider_type'));
			$this->common_model->commonSave("tbl_homepage",$array);
			$this->session->set_flashdata('msg', 'Item added successfully.');
        	redirect(base_url() . "admin/homepage");
		}
    }
	
	public function edit_slider($id) 
	{
		read_only_user();
		$result['title'] = "Edit Main Slider";
		$result['data'] = $this->common_model->common_where("tbl_homepage",array('home_id' => $id))->result_array();
		$this->form_validation->set_rules('url', 'name', 'required|callback_is_valid_domain_name');
		//$this->form_validation->set_rules('home_status', 'Status', 'required');
		$this->form_validation->set_message('required', 'This field is required.');
		if((isset($_FILES['file']) || @$_FILES['file']['name'] != '' || @$_FILES['file']['type'] != '') && $this->input->post('new_file') != $this->input->post('file_old'))
		{
			//list($width, $height) = getimagesize($_FILES['file']['tmp_name']);
			$this->form_validation->set_rules('new_file', 'img file' , 'trim|callback_valid_slider_file['.$_FILES['file']['tmp_name'].']');
		}
		
		if((isset($_FILES['file']) || @$_FILES['file']['name'] != '' || @$_FILES['file']['type'] != '') && $this->input->post('new_file') != $this->input->post('file_old'))
		{
			//list($width, $height) = getimagesize($_FILES['file']['tmp_name']);
			$this->form_validation->set_rules('new_file', 'img file' , 'trim|callback_valid_slider_file['.$_FILES['file']['tmp_name'].']');
		}
	/*	if(isset($_FILES['file']['name']) && $_FILES['file']['name'] !='')
		{
			list($width, $height) = getimagesize($_FILES['file']['tmp_name']);
			
			if($width < 1000 || $height < 336 && $this->input->post('file_old') == '')
			{
			
				$this->form_validation->set_rules('file', 'image', 'required');
				$this->form_validation->set_message('required', 'File image should be greater than 1000 x 336.');
			}
		}*/
		
		
		
		if ($this->form_validation->run() == FALSE) 
		{
			
			$this->load->view('admin/homepage/edit_slider', $result);
		}
		
		else
		{
			//////////////////////////////////////////// upload multiypal image
			$myFiles = $_FILES['file'];
 
			$fileDP = array();
				if($myFiles['name']!== "")
				{
					$config = array(
								'allowed_types' => 'jpg|jpeg|gif|png',
								'upload_path' => './resources/homepage_image/',
								'file_name' => time(),
								
								);
					$this->load->library('upload');
					$this->upload->initialize($config);
					//$this->upload->do_upload('file');
					if ( ! $this->upload->do_upload('file'))
					{ //Print message if file is not uploaded
						$this->session->set_flashdata('msg', $this->upload->display_errors());
						header("Location: ".base_url()."admin/homepage/edit_slider/".$id);
						exit();
					}
					else
					{
						
						// //////////////////////////////////////////////uplaod thumbnail image
						$dataDP = $this->upload->data();
						$fileDP = $dataDP['file_name'];
						$this->load->library('image_lib');
						$configt['image_library'] = 'gd2';
						$configt['source_image'] = $dataDP['full_path'];
						$configt['new_image'] = "./resources/homepage_image/thumb/";
						$configt['maintain_ratio'] = FALSE;
						$configt['width'] = 80;
						$configt['height'] = 80;
						$this->image_lib->initialize($configt);
						$this->image_lib->resize();
					}
				}
				else
				{
					$fileDP = $this->input->post('file_old');
				}
			
			///////////////////////////////////////image upload code end///////////////////////////////////////////////
			$array = array('home_url' => $this->input->post('url'),'home_status' => 1, 'home_image' => $fileDP,'home_type' => $this->input->post('slider_type'));
			$this->common_model->commonUpdate("tbl_homepage",$array , "home_id", $id);
			$this->session->set_flashdata('msg', 'Item updated successfully.');
        	redirect(base_url() . "admin/homepage");
			exit;
		}
    }
 

    public function delete_slider($id)
    {
		read_only_user();
		$this->common_model->commonDelete('tbl_homepage', "home_id", $id);
		$this->session->set_flashdata('msg', 'Item deleted successfully.');
		redirect(base_url() . "admin/homepage");
		exit;
    }
		///////////////////////////////////////slider listing  end///////////////////////////////////////////////
		
		
		
	 public function feature_product_list()
	{
		$result['title'] = "Featured Products";
        $result['data'] = $this->db->query("SELECT * FROM tbl_homepage WHERE home_type = 2 ORDER BY home_id DESC")->result_array();
		        
        $this->load->view('admin/homepage/list_feature_product',$result);
    }
	

	
    public function add_feature_product() 
	{
		read_only_user();
		
		$result['title'] = "Add Featured Product";
		$this->form_validation->set_rules('title', 'name', 'required');
		$this->form_validation->set_rules('home_status', 'Status', 'required');
		//$this->form_validation->set_rules('file', 'image', 'required');
		$this->form_validation->set_message('required', 'This field is required.');
		if(isset($_FILES['file']['name']) && $_FILES['file']['name'] !='')
		{
			list($width, $height) = getimagesize($_FILES['file']['tmp_name']);
			
			if($width < 331 || $height < 268 && $this->input->post('file_old') == '')
			{
			
				$this->form_validation->set_rules('file', 'image', 'required');
				$this->form_validation->set_message('required', 'File image should be greater than 1000 x 336.');
			}
		}
		else
		{
			$this->form_validation->set_rules('file', 'image', 'required');
		}
		
		if ($this->form_validation->run() == FALSE) 
		{	
			
        	$this->load->view('admin/homepage/add_feature_product', $result);
		}
		else
		{
		
			//////////////////////////////////////////// upload single image
			$myFiles = $_FILES['file'];
			$fileDP = array();
	
				if($myFiles['name'] !== "")
				{
			
					$config = array(
								'allowed_types' => 'jpg|jpeg|gif|png',
								'upload_path' => './resources/homepage_image/',
								'file_name' => time(),
								
								);
					$this->load->library('upload');
					$this->upload->initialize($config);
					//$this->upload->do_upload('file');
					if ( ! $this->upload->do_upload('file'))
					{ //Print message if file is not uploaded
						$this->session->set_flashdata('msg', $this->upload->display_errors());
						header("Location: ".base_url()."admin/homepage/add_feature_product/".$id);
						exit();
					}
					else
					{
						
						// //////////////////////////////////////////////uplaod thumbnail image
						$dataDP = $this->upload->data();
						$fileDP = $dataDP['file_name'];
						$this->load->library('image_lib');
						$configt['image_library'] = 'gd2';
						$configt['source_image'] = $dataDP['full_path'];
						$configt['new_image'] = "./resources/homepage_image/thumb/";
						$configt['maintain_ratio'] = FALSE;
						$configt['width'] = 80;
						$configt['height'] = 80;
						$this->image_lib->initialize($configt);
						$this->image_lib->resize();
					}
				}
				else
				{
					$fileDP = "";
				}
			
			///////////////////////////////////////image upload code end///////////////////////////////////////////////
			$array = array('home_title' => $this->input->post('title'),'home_status' => $this->input->post('home_status'),'home_date' => date('Y-m-d'), 'home_image' => $fileDP,'home_type' => $this->input->post('feature_type'));
			$this->common_model->commonSave("tbl_homepage",$array);
			$this->session->set_flashdata('msg', 'Item added successfully.');
        	redirect(base_url() . "admin/homepage/feature_product_list");
		}
    }
	
	public function edit_feature_product($id) 
	{
		read_only_user();
		$result['title'] = "Edit Featured Product";
		$result['data'] = $this->common_model->common_where("tbl_homepage",array('home_id' => $id))->result_array();
		$this->form_validation->set_rules('url', 'name', 'required|callback_is_valid_domain_name');
		$this->form_validation->set_rules('title', 'name', 'required|max_length[37]');
		$this->form_validation->set_message('max_length', 'Maximum character limit exceeded.');
		//$this->form_validation->set_rules('file', 'image', 'required');
		$this->form_validation->set_message('required', 'This field is required.');
		if((isset($_FILES['file']) || @$_FILES['file']['name'] != '' || @$_FILES['file']['type'] != '') && $this->input->post('new_file') != $this->input->post('file_old'))
		{
			//list($width, $height) = getimagesize($_FILES['file']['tmp_name']);
			$this->form_validation->set_rules('new_file', 'img file' , 'trim|callback_valid_feature_file['.$_FILES['file']['tmp_name'].']');
		}
	/*	if(isset($_FILES['file']['name']) && $_FILES['file']['name'] !='')
		{
			list($width, $height) = getimagesize($_FILES['file']['tmp_name']);
			
			if($width < 331 || $height < 268 && $this->input->post('file_old') == '')
			{
				$this->form_validation->set_rules('file', 'image', 'required');
				$this->form_validation->set_message('required', 'File image should be greater than 1000 x 336.');
			}
		}*/
		
		
		if ($this->form_validation->run() == FALSE) 
		{
			
        	$this->load->view('admin/homepage/edit_feature_product', $result);
 
		}
		else
		{
			//////////////////////////////////////////// upload multiypal image
			$myFiles = $_FILES['file'];
			$fileDP = array();
				if($myFiles['name']!== "")
				{
					
					$config = array(
								'allowed_types' => 'jpg|jpeg|gif|png',
								'upload_path' => './resources/homepage_image/',
								'file_name' => time(),
								);
					$this->load->library('upload');
					$this->upload->initialize($config);
					//$this->upload->do_upload('file');
					if ( ! $this->upload->do_upload('file'))
					{ //Print message if file is not uploaded
						$this->session->set_flashdata('msg', $this->upload->display_errors());
						header("Location: ".base_url()."admin/resources/homepage_image/".$id);
						exit();
					}
					else
					{
						
						// //////////////////////////////////////////////uplaod thumbnail image
						$dataDP = $this->upload->data();
						$fileDP = $dataDP['file_name'];
						$this->load->library('image_lib');
						$configt['image_library'] = 'gd2';
						$configt['source_image'] = $dataDP['full_path'];
						$configt['new_image'] = "./resources/homepage_image/thumb/";
						$configt['maintain_ratio'] = FALSE;
						$configt['width'] = 80;
						$configt['height'] = 80;
						$this->image_lib->initialize($configt);
						$this->image_lib->resize();
					}
				}
				else
				{
					$fileDP = $this->input->post('file_old');
				}
			
			///////////////////////////////////////image upload code end///////////////////////////////////////////////
			$array = array('home_title' => $this->input->post('title'),'home_url' => $this->input->post('url'), 'home_image' => $fileDP,'home_type' => $this->input->post('feature_type'));
			$this->common_model->commonUpdate("tbl_homepage",$array , "home_id", $id);
			$this->session->set_flashdata('msg', 'Item updated successfully.');
        	redirect(base_url() . "admin/homepage/feature_product_list");
			exit;
		}
    }
 

    public function delete_feature_product($id)
    {
		read_only_user();
		$this->common_model->commonDelete('tbl_homepage', "home_id", $id);
		$this->session->set_flashdata('msg', 'Item deleted successfully.');
		redirect(base_url() . "admin/homepage/feature_product_list");
		exit;
    }
	
	
	//////////////////////*********@@@@@@@@@@@@@@@/////////////////homepage_product/////////////////@#$$$$$$$$$$$$$%%%%%%%%%%%%%%%%@@@@@@@@@@@@@//////////////////////////////
	
	 public function homepage_product()
	 {
		$result['title'] = "Product Area";
        $result['data'] = $this->db->query("SELECT * FROM tbl_homepage WHERE home_type = 3 ORDER BY home_id DESC")->result_array();        
        $this->load->view('admin/homepage/list_homepage_product',$result);
     }
	

	
    public function add_homepage_product() 
	{
		read_only_user();
		
		$result['title'] = "Add Product Area";
		$this->form_validation->set_rules('title', 'name', 'required');
		$this->form_validation->set_rules('home_status', 'Status', 'required');
		$this->form_validation->set_rules('url', 'image', 'required');
		$this->form_validation->set_message('required', 'This field is required.');
		if(isset($_FILES['file']['name']) && $_FILES['file']['name'] !='')
		{
			list($width, $height) = getimagesize($_FILES['file']['tmp_name']);
			
			if($width < 236 || $height < 236 && $this->input->post('file_old') == '')
			{
			
				$this->form_validation->set_rules('file', 'image', 'required');
				//$this->form_validation->set_message('required', 'File image should be greater than 1000 x 336.');
			}
		}
		else
		{
			$this->form_validation->set_rules('file', 'image', 'required');
		}
		
		if ($this->form_validation->run() == FALSE) 
		{	
			
        	$this->load->view('admin/homepage/add_homepage_product', $result);
		}
		else
		{
		
			//////////////////////////////////////////// upload single image
			$myFiles = $_FILES['file'];
			$fileDP = array();
	
				if($myFiles['name'] !== "")
				{
			
					$config = array(
								'allowed_types' => 'jpg|jpeg|gif|png',
								'upload_path' => './resources/homepage_image/',
								'file_name' => time(),
								
								);
					$this->load->library('upload');
					$this->upload->initialize($config);
					//$this->upload->do_upload('file');
					if ( ! $this->upload->do_upload('file'))
					{ //Print message if file is not uploaded
						$this->session->set_flashdata('msg', $this->upload->display_errors());
						header("Location: ".base_url()."admin/homepage/add_homepage_product/".$id);
						exit();
					}
					else
					{
						
						// //////////////////////////////////////////////uplaod thumbnail image
						$dataDP = $this->upload->data();
						$fileDP = $dataDP['file_name'];
						$this->load->library('image_lib');
						$configt['image_library'] = 'gd2';
						$configt['source_image'] = $dataDP['full_path'];
						$configt['new_image'] = "./resources/homepage_image/thumb/";
						$configt['maintain_ratio'] = FALSE;
						$configt['width'] = 80;
						$configt['height'] = 80;
						$this->image_lib->initialize($configt);
						$this->image_lib->resize();
					}
				}
				else
				{
					$fileDP = "";
				}
			
			///////////////////////////////////////image upload code end///////////////////////////////////////////////
			$array = array('home_title' => $this->input->post('title'),'home_url' => $this->input->post('url'),'home_status' => $this->input->post('home_status'),'home_date' => date('Y-m-d'), 'home_image' => $fileDP,'home_type' => $this->input->post('homepage_type'));
			$this->common_model->commonSave("tbl_homepage",$array);
			$this->session->set_flashdata('msg', 'Item added successfully.');
        	redirect(base_url() . "admin/homepage/homepage_product");
		}
    }

	public function edit_homepage_product($id) 
	{
		read_only_user();
		$result['title'] = "Edit Product Area";
		$result['data'] = $this->common_model->common_where("tbl_homepage",array('home_id' => $id))->result_array();
		$this->form_validation->set_rules('title', 'name', 'required|max_length[25]');
		$this->form_validation->set_message('max_length', 'Maximum character limit exceeded.');
		
		//$this->form_validation->set_rules('home_status', 'Status', 'required');
		$this->form_validation->set_rules('url', 'name', 'required|callback_is_valid_domain_name');
		//$this->form_validation->set_rules('file', 'image', 'required');
		$this->form_validation->set_message('required', 'This field is required.');
		
		
		if((isset($_FILES['file']) || @$_FILES['file']['name'] != '' || @$_FILES['file']['type'] != '') && $this->input->post('new_file') != $this->input->post('file_old'))
		{
			//list($width, $height) = getimagesize($_FILES['file']['tmp_name']);
			$this->form_validation->set_rules('new_file', 'img file' , 'trim|callback_valid_file['.$_FILES['file']['tmp_name'].']');
		}
		/*if(isset($_FILES['file']['name']) && $_FILES['file']['name'] !='')
		{
			list($width, $height) = getimagesize($_FILES['file']['tmp_name']);
			
			if($width < 236 || $height < 236 && $this->input->post('file_old') == '')
			{
				$this->form_validation->set_rules('file', 'image');
				$this->session->set_userdata('greater_img','File image should be greater than 1000 x 336.');
				//$this->session->set_flashdata('greater_img', 'File image should be greater than 1000 x 336.');
				//$this->form_validation->set_message('required', 'File image should be greater than 1000 x 336.');
			}
		}*/
		
		
		if ($this->form_validation->run() == FALSE) 
		{
			
        	$this->load->view('admin/homepage/edit_homepage_product', $result);
 
		}
		else
		{
			//////////////////////////////////////////// upload multiypal image///////////////////
			$myFiles = $_FILES['file'];
			$fileDP = array();
				if($myFiles['name']!== "")
				{
					
					$config = array(
								'allowed_types' => 'jpg|jpeg|gif|png',
								'upload_path' => './resources/homepage_image/',
								'file_name' => time(),
								);
					$this->load->library('upload');
					$this->upload->initialize($config);
					//$this->upload->do_upload('file');
					if ( ! $this->upload->do_upload('file'))
					{ //Print message if file is not uploaded
						$this->session->set_flashdata('msg', $this->upload->display_errors());
						header("Location: ".base_url()."admin/resources/homepage_image/".$id);
						exit();
					}
					else
					{
						
						// //////////////////////////////////////////////uplaod thumbnail image
						$dataDP = $this->upload->data();
						$fileDP = $dataDP['file_name'];
						$this->load->library('image_lib');
						$configt['image_library'] = 'gd2';
						$configt['source_image'] = $dataDP['full_path'];
						$configt['new_image'] = "./resources/homepage_image/thumb/";
						$configt['maintain_ratio'] = FALSE;
						$configt['width'] = 80;
						$configt['height'] = 80;
						$this->image_lib->initialize($configt);
						$this->image_lib->resize();
					}
				}
				else
				{
					$fileDP = $this->input->post('file_old');
				}
			
			///////////////////////////////////////image upload code end///////////////////////////////////////////////
			$array = array('home_title' => $this->input->post('title'),'home_status' => 1,'home_url' => $this->input->post('url'),'home_image' => $fileDP,'home_type' => $this->input->post('homepage_type'));
			$this->common_model->commonUpdate("tbl_homepage",$array , "home_id", $id);
			$this->session->set_flashdata('msg', 'Item updated successfully.');
        	redirect(base_url() . "admin/homepage/homepage_product");
			exit;
		}
    }
    public function delete_homepage_product($id)
    {
		read_only_user();
		$this->common_model->commonDelete('tbl_homepage', "home_id", $id);
		$this->session->set_flashdata('msg', 'Item deleted successfully.');
		redirect(base_url() . "admin/homepage/homepage_product");
		exit;
    }
  
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	 public function homepage_banner()
	 {
		$result['title'] = "Static Banner";
		$result['data'] = $this->db->query("SELECT * FROM tbl_homepage WHERE home_type = 4 ORDER BY home_id DESC")->result_array();        
		$this->load->view('admin/homepage/list_homepage_banner',$result);
	 }
	public function edit_homepage_banner($id) 
	{
		read_only_user();
		$result['title'] = "Edit Static Banner";
		$result['data'] = $this->common_model->common_where("tbl_homepage",array('home_id' => $id))->result_array();
		$this->form_validation->set_rules('title', 'name', 'required|max_length[50]');
		$this->form_validation->set_message('max_length', 'Maximum character limit exceeded.');
		
		$this->form_validation->set_rules('url', 'name', 'required');
		
		$this->form_validation->set_rules('text', 'description', 'trim|required|callback_des_check|max_length[485]');
		
		//$this->form_validation->set_rules('text', 'image', 'required|max_length[480]');
		$this->form_validation->set_message('required', 'This field is required.');
		
		if(isset($_FILES['file']['name']) && $_FILES['file']['name'] !='')
		{
			list($width, $height) = getimagesize($_FILES['file']['tmp_name']);
			
			if($width < 236 || $height < 236 && $this->input->post('file_old') == '')
			{
				$this->form_validation->set_rules('file', 'image', 'required');
				$this->form_validation->set_message('required', 'File image should be greater than 1000 x 336.');
			}
		}
		if ($this->form_validation->run() == FALSE) 
		{
        	$this->load->view('admin/homepage/edit_homepage_banner', $result);
		}
		else
		{
			//////////////////////////////////////////// upload multiypal image///////////////////
			$myFiles = $_FILES['file'];
			$fileDP = array();
			if($myFiles['name']!== "")
			{
				$config = array(
							'allowed_types' => 'jpg|jpeg|gif|png',
							'upload_path' => './resources/homepage_image/',
							'file_name' => time(),
							);
				$this->load->library('upload');
				$this->upload->initialize($config);
				//$this->upload->do_upload('file');
				if ( ! $this->upload->do_upload('file'))
				{ //Print message if file is not uploaded
					$this->session->set_flashdata('msg', $this->upload->display_errors());
					header("Location: ".base_url()."admin/resources/homepage_image/".$id);
					exit();
				}
				else
				{
					// //////////////////////////////////////////////uplaod thumbnail image
					$dataDP = $this->upload->data();
					$fileDP = $dataDP['file_name'];
					$this->load->library('image_lib');
					$configt['image_library'] = 'gd2';
					$configt['source_image'] = $dataDP['full_path'];
					$configt['new_image'] = "./resources/homepage_image/thumb/";
					$configt['maintain_ratio'] = FALSE;
					$configt['width'] = 80;
					$configt['height'] = 80;
					$this->image_lib->initialize($configt);
					$this->image_lib->resize();
				}
			}
			else
			{
				$fileDP = $this->input->post('file_old');
			}
			///////////////////////////////////////image upload code end///////////////////////////////////////////////
			$array = array('home_title' => $this->input->post('title'),'home_url' => $this->input->post('url'),'home_text' => stripcslashes($this->input->post('text')), 'home_image' => $fileDP,'home_type' => $this->input->post('homepage_type'));
			$this->common_model->commonUpdate("tbl_homepage",$array , "home_id", $id);
			$this->session->set_flashdata('msg', 'Item updated successfully.');
        	redirect(base_url() . "admin/homepage/homepage_banner");
			exit;
		}
    }
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	 public function homepage_static_text()
	 {
		$result['title'] = "Static Text";
		$result['data'] = $this->db->query("SELECT * FROM tbl_homepage WHERE home_type = 5 ORDER BY home_id DESC")->result_array();        
		$this->load->view('admin/homepage/list_static_text',$result);
	 }
	public function edit_static_text($id) 
	{
		read_only_user();
		$result['title'] = "Edit Static Text";
		$result['data'] = $this->common_model->common_where("tbl_homepage",array('home_id' => $id))->result_array();
		$this->form_validation->set_rules('title', 'name', 'required|max_length[50]');
		$this->form_validation->set_message('max_length', 'Maximum character limit exceeded.');
		
		$this->form_validation->set_rules('text', 'image', 'required|max_length[380]');
		$this->form_validation->set_message('required', 'This field is required.');

		if ($this->form_validation->run() == FALSE) 
		{
        	$this->load->view('admin/homepage/edit_static_text', $result);
		}
		else
		{
			
			///////////////////////////////////////image upload code end///////////////////////////////////////////////
			$array = array('home_title' => $this->input->post('title'),'home_text' => $this->input->post('text'),'home_type' => $this->input->post('home_type'));
			$this->common_model->commonUpdate("tbl_homepage",$array , "home_id", $id);
			$this->session->set_flashdata('msg', 'Item updated successfully.');
        	redirect(base_url() . "admin/homepage/homepage_static_text");
			exit;
		}
    }
	
	public function homepage_prod_text()
	{
		$result['title'] = "Product Area Text";
		$result['data'] = $this->db->query("SELECT * FROM tbl_homepage WHERE home_type = 6 ORDER BY home_id DESC")->result_array();        
		$this->load->view('admin/homepage/list_prod_text',$result);
		
	}
	
	public function edit_prod_text($id) 
	{
		read_only_user();
		$result['title'] = "Edit Product Area Text";
		$result['data'] = $this->common_model->common_where("tbl_homepage",array('home_id' => $id))->result_array();
		$this->form_validation->set_rules('title', 'name', 'required|max_length[51]');
		$this->form_validation->set_message('max_length', 'Maximum character limit exceeded.');
		
		$this->form_validation->set_rules('text', 'image', 'required|max_length[186]');
		$this->form_validation->set_message('required', 'This field is required.');

		if ($this->form_validation->run() == FALSE) 
		{
        	$this->load->view('admin/homepage/edit_prod_text', $result);
		}
		else
		{

			
			///////////////////////////////////////image upload code end///////////////////////////////////////////////
			$array = array('home_title' => $this->input->post('title'),'home_text' => $this->input->post('text'),'home_type' => $this->input->post('home_type'));
			$this->common_model->commonUpdate("tbl_homepage",$array , "home_id", $id);
			$this->session->set_flashdata('msg', 'Item updated successfully.');
        	redirect(base_url() . "admin/homepage/homepage_prod_text");
			exit;
		}
    }
	
	
}

?>