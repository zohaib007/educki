<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {

    public function __construct()
	{
        parent::__construct();
        $this->load->library('upload');
        auth();
	}
	
	public function index()
	{
		$data['list'] = $this->common_model->getCombox('tbl_blog', 'blog_id', 'DESC' )->result();
		$this->load->view('admin/blog/blog_list', $data);
	}
	   
	public function add_blog()
	{
		$data['title'] = "Add New Blog";
		$this->form_validation->set_rules('name', 'blog name', 'trim|required');
		$this->form_validation->set_rules('author', 'author name', 'trim|required');
		$this->form_validation->set_rules('description', 'description', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');
		$this->form_validation->set_rules('blog_category', 'category', 'trim|required');

		$this->form_validation->set_rules('meta_title', 'meta title', 'trim');
		$this->form_validation->set_rules('meta_keywords', 'meta keywords', 'trim');
		$this->form_validation->set_rules('meta_description', 'meta description', 'trim');

		if ((isset($_FILES['image']) || @$_FILES['image']['name'] != '' || @$_FILES['image']['type'] != '')) {
            $this->form_validation->set_rules('hidden_file3', 'image', 'trim|callback_valid_file');
        }

		$this->form_validation->set_message('required', 'This field is required.');
		
		if($this->form_validation->run() == FALSE) {
			$data['blog_category'] = $this->common_model->getCombox('tbl_blog_category', 'blog_cat_name', 'ASC')->result();
			$this->load->view('admin/blog/blog_add',$data);
		}
		else
		{
			$date = date('Y-m-d H:i:s');
			/////////////// upload multiypal image
			$myFiles1 = $_FILES['image'];
			$fileDP1 = array();
				if($myFiles1['name']!== "")
				{
					
					$config = array(
								'allowed_types' => 'jpg|jpeg|gif|png',
								'upload_path' => FCPATH.'/resources/blog_image/',
								'file_name' => 'img_'.date('Y_m_d_h_i_s'),
								);
					$this->load->library('upload');
					$this->upload->initialize($config);
					//$this->upload->do_upload('file');
					if ( ! $this->upload->do_upload('image'))
					{ //Print message if file is not uploaded
						$this->session->set_flashdata('msg', $this->upload->display_errors());
						header("Location: ".base_url()."admin/blog/add_blog");
						exit();
					}
					else
					{
						//////uplaod thumbnail image
						$dataDP = $this->upload->data();
						$fileDP1 = $dataDP['file_name'];
						$this->load->library('image_lib');
						$configt['image_library'] = 'gd2';
						$configt['source_image'] = $dataDP['full_path'];
						$configt['new_image'] = FCPATH."/resources/blog_image/thumb/";
						$configt['maintain_ratio'] = FALSE;
						$configt['width'] = 80;
						$configt['height'] = 80;
						$this->image_lib->initialize($configt);
						$this->image_lib->resize();
					}
				}
				else
				{
					$fileDP1 = $this->input->post('image');
				}
			//////////////////////////////////////////////////////////////////////////////////////////////////////////
			$array = array(
						'blog_name' => stripcslashes($this->input->post('name')),
						'blog_description' => stripcslashes($this->input->post('description')),
						'blog_author' => stripcslashes($this->input->post('author')),
						'blog_url'=> $this->common_model->create_slug($this->input->post('name'), 'tbl_blog','blog_url'),
						'blog_image' => $fileDP1,
						'blog_status' => stripcslashes($this->input->post('status')),
						'meta_title' => stripcslashes($this->input->post('meta_title')),
						'meta_keywords' => stripcslashes($this->input->post('meta_keywords')),
						'meta_description' => stripcslashes($this->input->post('meta_description')),
						'blog_created_date' => date('Y-m-d h:i:s'),
						'blog_category_id' => stripcslashes($this->input->post('blog_category')),
						'blog_admin_id' => $this->session->userdata('admin_id')
					);
			
			$this->common_model->commonSave('tbl_blog',$array);
			$this->session->set_flashdata('msg', 'Item added successfully.');
			redirect(base_url() . "admin/blog");
			exit;
		}
	}

	public function valid_file($name)
    {
        if ($name != "" && !empty($name) && $name != "Upload File") {
            $ext = explode('.', $name);
            $ext = strtolower(end($ext));
            if ($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "png") {
                return true;
            } else {
                $this->form_validation->set_message('valid_file', 'The file type you are attempting to upload is not allowed.');
                return false;
            }
        }
    }


	public function edit_blog($blog_id = '')
	{
		if($blog_id =='' || !is_numeric($blog_id)){
			redirect(base_url('admin/blog'));
			exit;
		}
		$this->form_validation->set_rules('name', 'blog name', 'trim|required');
		$this->form_validation->set_rules('author', 'author name', 'trim|required');
		$this->form_validation->set_rules('description', 'description', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');
		$this->form_validation->set_rules('blog_category', 'category', 'trim|required');

		$this->form_validation->set_rules('meta_title', 'meta title', 'trim');
		$this->form_validation->set_rules('meta_keywords', 'meta keywords', 'trim');
		$this->form_validation->set_rules('meta_description', 'meta description', 'trim');

		$this->form_validation->set_message('required', 'This field is required.');
		
		if($this->form_validation->run() == FALSE) {
			$data['blog_category'] = $this->common_model->getCombox('tbl_blog_category', 'blog_cat_name', 'ASC')->result();
			$data['blog_data'] = $this->common_model->commonselect('tbl_blog', 'blog_id', $blog_id)->row();
			$this->load->view('admin/blog/blog_edit',$data);
		}
		else
		{
			$date = date('Y-m-d H:i:s');
			/////////////// upload multiypal image
			$myFiles1 = $_FILES['image'];
			$fileDP1 = array();
			if($myFiles1['name']!== "")
			{
				unlink(FCPATH."resources/blog_image/".$data['blog_data']->blog_image);
				unlink(FCPATH."resources/blog_image/thumb/".$data['blog_data']->blog_image);
				$config = array(
							'allowed_types' => 'jpg|jpeg|gif|png',
							'upload_path' => FCPATH.'/resources/blog_image/',
							'file_name' => 'img_'.date('Y_m_d_h_i_s'),
							);
				$this->load->library('upload');
				$this->upload->initialize($config);
				//$this->upload->do_upload('file');
				if ( ! $this->upload->do_upload('image'))
				{ //Print message if file is not uploaded
					$this->session->set_flashdata('msg', $this->upload->display_errors());
					header("Location: ".base_url()."admin/blog/edit_blog/".$pg_id);
					exit();
				}
				else
				{
					//////uplaod thumbnail image
					$dataDP = $this->upload->data();
					$fileDP1 = $dataDP['file_name'];
					$this->load->library('image_lib');
					$configt['image_library'] = 'gd2';
					$configt['source_image'] = $dataDP['full_path'];
					$configt['new_image'] = FCPATH."/resources/blog_image/thumb/";
					$configt['maintain_ratio'] = FALSE;
					$configt['width'] = 80;
					$configt['height'] = 80;
					$this->image_lib->initialize($configt);
					$this->image_lib->resize();
				}
			}
			else
			{
				$fileDP1 = $this->input->post('image');
			}
			//////////////////////////////////////////////////////////////////////////////////
			$array = array(
						'blog_name' => stripcslashes($this->input->post('name')),
						'blog_description' => stripcslashes($this->input->post('description')),
						'blog_author' => stripcslashes($this->input->post('author')),
						'blog_url'=> $this->common_model->create_slug($this->input->post('name'), 'tbl_blog','blog_url', 'blog_id',$blog_id),
						'blog_image' => $fileDP1,
						'blog_status' => stripcslashes($this->input->post('status')),
						'meta_title' => stripcslashes($this->input->post('meta_title')),
						'meta_keywords' => stripcslashes($this->input->post('meta_keywords')),
						'meta_description' => stripcslashes($this->input->post('meta_description')),
						'blog_created_date' => date('Y-m-d h:i:s'),
						'blog_category_id' => stripcslashes($this->input->post('blog_category')),
						);
			
			$this->common_model->commonUpdate('tbl_blog',$array,'blog_id', $blog_id);
			$this->session->set_flashdata('msg', 'Item updated successfully.');
			redirect(base_url() . "admin/blog");
			exit;
		}
	}

	public function delete_blog($blog_id)
	{
		if($blog_id =='' || !is_numeric($blog_id)){
			redirect(base_url('admin/blog'));
			exit;
		}
		$data['blog_data'] = $this->common_model->commonselect('tbl_blog', 'blog_id', $blog_id)->row();
		unlink(FCPATH."resources/blog_image/".$data['blog_data']->blog_image);
		unlink(FCPATH."resources/blog_image/thumb/".$data['blog_data']->blog_image);
		$this->common_model->commonDelete('tbl_blog', "blog_id", $blog_id);
		$this->session->set_flashdata('msg', 'Item deleted successfully.');
		redirect(base_url() . "admin/blog");	
	}

	public function delete_blog_img()
	{
		if(!$this->input->is_ajax_request())
		{

		}else{
			$blog_id = $this->input->post('nId');
			$data = array("blog_image" => "");
			$this->db->where('blog_id', $blog_id);
			$this->db->update('tbl_blog', $data);
			$data['blog_data'] = $this->common_model->commonselect('tbl_blog', 'blog_id', $blog_id)->row();
			unlink(FCPATH."resources/blog_image/".$data['blog_data']->blog_image);
			unlink(FCPATH."resources/blog_image/thumb/".$data['blog_data']->blog_image);
		}
		
	}

	public function categories()
	{
		$data['list'] = $this->common_model->getCombox('tbl_blog_category', 'blog_cat_id', 'DESC' )->result();
		$this->load->view('admin/blog/cat_list', $data);
	}
	public function add_category()
	{
		$data['title'] = "Add New Category";
		$this->form_validation->set_rules('cat_name', 'blog name', 'trim|required');
		$this->form_validation->set_rules('cat_status', 'status', 'trim|required');

		$this->form_validation->set_rules('meta_title', 'meta title', 'trim');
		$this->form_validation->set_rules('meta_keywords', 'meta keywords', 'trim');
		$this->form_validation->set_rules('meta_description', 'meta description', 'trim');

		$this->form_validation->set_message('required', 'This field is required.');
		
		if($this->form_validation->run() == FALSE) {
			$this->load->view('admin/blog/cat_add',$data);
		}
		else
		{
			$array = array(
						'blog_cat_name' => stripcslashes($this->input->post('cat_name')),
						'blog_cat_url' => $this->common_model->create_slug($this->input->post('cat_name'), 'tbl_blog_category','blog_cat_url'),
						'blog_cat_status' => stripcslashes($this->input->post('cat_status')),
						'meta_title' => stripcslashes($this->input->post('meta_title')),
						'meta_keywords' => stripcslashes($this->input->post('meta_keywords')),
						'meta_description' => stripcslashes($this->input->post('meta_description')),
						'blog_cat_date' => date('Y-m-d h:i:s'),
						'blog_cat_admin_id' => $this->session->userdata('admin_id')
					);
			
			$this->common_model->commonSave('tbl_blog_category',$array);
			$this->session->set_flashdata('msg', 'Item added successfully.');
			redirect(base_url() . "admin/blog/categories");
			exit;
		}
	}

	public function edit_category($blog_cat_id = '')
	{
		if($blog_cat_id =='' || !is_numeric($blog_cat_id)){
			redirect(base_url('admin/blog/categories'));
			exit;
		}
		$this->form_validation->set_rules('cat_name', 'blog name', 'trim|required');
		$this->form_validation->set_rules('cat_status', 'status', 'trim|required');

		$this->form_validation->set_rules('meta_title', 'meta title', 'trim');
		$this->form_validation->set_rules('meta_keywords', 'meta keywords', 'trim');
		$this->form_validation->set_rules('meta_description', 'meta description', 'trim');

		$this->form_validation->set_message('required', 'This field is required.');
		
		$data['blog_cat_data'] = $this->common_model->commonselect('tbl_blog_category', 'blog_cat_id', $blog_cat_id)->row();
		
		if($this->form_validation->run() == FALSE) {
			$this->load->view('admin/blog/cat_edit',$data);
		}
		else
		{
			$array = array(
						'blog_cat_name' => stripcslashes($this->input->post('cat_name')),
						'blog_cat_url' => $this->common_model->create_slug($this->input->post('cat_name'), 'tbl_blog_category','blog_cat_url', 'blog_cat_id', $blog_cat_id),
						'blog_cat_status' => stripcslashes($this->input->post('cat_status')),
						'meta_title' => stripcslashes($this->input->post('meta_title')),
						'meta_keywords' => stripcslashes($this->input->post('meta_keywords')),
						'meta_description' => stripcslashes($this->input->post('meta_description'))
					);
			
			$this->common_model->commonUpdate('tbl_blog_category',$array,'blog_cat_id', $blog_cat_id);
			$this->session->set_flashdata('msg', 'Item updated successfully.');
			redirect(base_url() . "admin/blog/categories");
			exit;
		}
	}

	public function delete_category($blog_cat_id)
	{
		if($blog_cat_id =='' || !is_numeric($blog_cat_id)){
			redirect(base_url('admin/blog'));
			exit;
		}
		
		$this->common_model->commonDelete('tbl_blog_category', "blog_cat_id", $blog_cat_id);
		$this->session->set_flashdata('msg', 'Item deleted successfully.');
		redirect(base_url() . "admin/blog/categories");
		exit;
	}






}

?>