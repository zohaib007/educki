<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tier_management extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('common/common_model');
        $this->load->helper(array('form', 'url'));
        auth();
    }

    public function index()
    {
        $data['title']        = "Tier Listing";
        $data['listing_list'] = $this->common_model->getCombox('tbl_tier_list', 'tier_id', "ASC")->result();
        $this->load->view('admin/pages/tier_management', $data);
    }

    public function edit_tier($id)
    {
        if ($id == '' || !is_numeric($id)) {
            redirect(base_url('admin/tier_management'));
        }
        $data['title']     = "Tier Listing";
        
        $data['tier_data'] = $this->common_model->commonselect('tbl_tier_list', 'tier_id', $id)->row();
        if(count($data['tier_data'])<=0)
        {
        	redirect(base_url('admin/tier_management'));
        }


        $data['sub_title'] = "Edit ".$data['tier_data']->tier_name;

        $this->form_validation->set_rules('tier_name', 'Tier Name', 'trim|required');
        $this->form_validation->set_rules('tier_title', 'Tier Title', 'trim|required');
        $this->form_validation->set_rules('fee', 'Transaction Fee', 'trim|required|numeric|greater_than_equal_to[1]');
        $this->form_validation->set_rules('months', 'Subscription duration', 'trim|required|numeric|greater_than_equal_to[0]|less_than_equal_to[12]');
        $this->form_validation->set_rules('amount', 'Amount/month', 'trim|required|numeric|greater_than_equal_to[0]');
        $this->form_validation->set_rules('pickup', 'Local Pickup', 'trim|required');
        $this->form_validation->set_rules('store', 'Facebook Store', 'trim|required');

        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('numeric', 'This field must be numeric.');

        if ($this->form_validation->run() == false) {

            $this->load->view('admin/pages/edit_tier', $data);
        } else {
            $array = array(
                'tier_name'   => $this->input->post('tier_name'),
                'tier_title'  => $this->input->post('tier_title'),
                'tier_fee'    => $this->input->post('fee'),
                'tier_months' => $this->input->post('months'),
                'tier_amount' => $this->input->post('amount'),

                'is_pickup'   => $this->input->post('pickup'),
                'is_fb_store' => $this->input->post('store'),
            );
            $array['update'] = date('Y-m-d H:i:s');

            $this->common_model->commonUpdate('tbl_tier_list', $array, 'tier_id', $id);

            $new_Plan = $this->updatePlan($id, $array);

            $data['msg'] = 'Item updated successfully.';
            $this->session->set_flashdata('msg', $data['msg']);

            redirect(base_url('admin/tier_management'));
            exit;
        }
    }
    public function updatePlan($tierid, $array) {

        $this->db->select("plan_id");
        $this->db->from('tbl_tier_list');
        $this->db->where('tier_id', $tierid);
        $query = $this->db->get();
        $res = $query->result();
        $old_plan_id = $res[0]->plan_id;


        $url = 'https://api.stripe.com/v1/plans/'.$old_plan_id;
        $header = array(
            'Authorization: Bearer sk_live_0LCxhw41WzTGHHH4Uf6FebBR',
        );
        $values = array();

        $params = http_build_query($values);

        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            //
            curl_setopt($ch, CURLOPT_HEADER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            //
//            curl_setopt($ch, CURLOPT_USERPWD, "$this->clientId:$this->secret");
            //
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            //
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);
            curl_close($ch);
        }catch(Exception $e){
            return $e->getMessage();
        }
        $r = explode('Strict-Transport-Security: max-age=31556926; includeSubDomains; preload',$result);
        $prod = trim($r[1]);
        $product_name = json_decode($prod)->product;

        $new = $this->createNewPlanIn($product_name, $array);
        $plan_id = json_decode($new)->id;

        //Store tier_id in tbl_tier_id

        $this->db->set('plan_id', $plan_id);
        $this->db->where('tier_id', $tierid);
        $this->db->update('tbl_tier_list');

        return $new;
    }

    public function createNewPlanIn($product, $array)
    {
        $url = 'https://api.stripe.com/v1/plans';
        $header = array(
            'Authorization: Bearer sk_live_0LCxhw41WzTGHHH4Uf6FebBR',
        );
        $duration = '';
       // if ($array['tier_months'] == 1){ $duration =  "Da" }
        $values = array(
            "product" => array(
                "name" => $product,
                "metadata"=>array(
                    "total_number_of_cycles" => $array['tier_months']
                ),
            ),
            'nickname' => $array['tier_name'],
            'interval' => "month",
            'currency' => 'usd',
            'amount' => (number_format($array['tier_amount'],2,'.','')*100) * $array['tier_months'],
        );

        $params = http_build_query($values);

        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            //
            curl_setopt($ch, CURLOPT_HEADER, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            //
//            curl_setopt($ch, CURLOPT_USERPWD, "$this->clientId:$this->secret");
            //
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            //
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($ch);
            curl_close($ch);
        }catch(Exception $e){
            return $e->getMessage();
        }

        $r = explode('Strict-Transport-Security: max-age=31556926; includeSubDomains; preload',$result);
        $plan = trim($r[1]);
        //$product_name = json_decode($re)->product;
        return $plan;
    }
}
?>