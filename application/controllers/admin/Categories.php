<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Categories extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('categories/categories_model');
        auth();
        //admin_permission("category");
    }

    function valid_file($name) {
        if ($name != "" && !empty($name)) {
            $ext = explode('.', $name);
            $ext = strtolower(end($ext));
            if ($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "png") {
                return true;
            } else {
                $this->form_validation->set_message('valid_file', 'The filetype you are attempting to upload is not allowed.');
                return false;
            }
        } else {
            $this->form_validation->set_message('valid_file', 'This field is required.');
            return false;
        }
    }

    public function url_unique($url) {
        $check = $this->db->query("SELECT * FROM tbl_categories WHERE cat_url = '" . $url . "' AND cat_is_delete = 0")->row();
        if (count($check) > 0) {
            $this->form_validation->set_message('url_unique', 'This URL already exists.');
            return false;
        } else {
            return true;
        }
    }

    public function index() {
        $set_where = '';
        if ($this->input->get('searchText') != '') {
            $set_where = " AND cat_name LIKE '%" . $this->input->get('searchText') . "%'";
        } else {
            $set_where = '';
        }
        $where = ' WHERE cat_is_delete = 0' . $set_where;
        $data['total_records'] = count($this->common_model->get_data_page("tbl_categories", $where)->result_array());
        $where = ' WHERE cat_parent_id = 0 AND cat_is_delete = 0' . $set_where;
        $data['list'] = $this->common_model->get_data_page("tbl_categories", $where, '', '', $is_count = 0, $orderby = 'cat_sort', $order = 'ASC')->result_array();
        $this->load->view('admin/categories/category_list', $data);
    }

    public function add_category() {
        $data['title'] = "Add Category";
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('url', 'URL', 'trim|required|callback_url_unique');
        $this->form_validation->set_rules('cat_status', 'status', 'trim|required');
        //$this->form_validation->set_rules('cat_desc', 'Description', 'trim|required');
        $this->form_validation->set_rules('meta_title', 'title', 'trim');
        $this->form_validation->set_rules('meta_keyword', 'keyword', 'trim');
        $this->form_validation->set_rules('meta_desc', 'desc', 'trim');
        $this->form_validation->set_rules('cat_parent_id', 'cat_parent_id', 'trim|required');

        $data['pCategories'] = $this->db->query("
										SELECT cat_id, cat_name 
										FROM tbl_categories
										WHERE
										(cat_parent_id = 0
										OR cat_parent_id IN (SELECT cat_id FROM tbl_categories WHERE cat_parent_id = 0 AND cat_is_delete = 0) )
										AND
										cat_is_delete = 0
										ORDER BY cat_sort ASC
									")->result_array();

        //if ($this->input->post('cat_parent_id') == 0) {
            if ((isset($_FILES['file1']) || @$_FILES['file1']['name'] != '' || @$_FILES['file1']['type'] != '')) {
                $this->form_validation->set_rules('hidden_file', 'file1', 'trim|callback_valid_file');
            }
        //}
        /* if((isset($_FILES['file2']) || @$_FILES['file2']['name'] != '' || @$_FILES['file2']['type'] != ''))
          {
          $this->form_validation->set_rules('hidden_file2', 'file2' , 'trim|callback_valid_file');
          } */

        $this->form_validation->set_message('required', 'This field is required.');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('admin/categories/category_add', $data);
        } else {
            $data_save['cat_name'] = stripcslashes($this->input->post('name'));
            /* $data_save['cat_url'] = $this->common_model->create_slug($data_save['cat_name'], 'tbl_products', 'prod_url');
              $data_save['cat_url'] = $this->common_model->create_slug($data_save['cat_url'], 'tbl_categories', 'cat_url'); */
            $data_save['cat_url'] = stripcslashes($this->input->post('url'));
            $data_save['cat_status'] = stripcslashes($this->input->post('cat_status'));
            //$data_save['cat_desc'] = stripcslashes($this->input->post('cat_desc'));
            $data_save['cat_parent_id'] = $this->input->post('cat_parent_id');
            $data_save['meta_title'] = stripcslashes($this->input->post('meta_title'));
            $data_save['meta_keyword'] = stripcslashes($this->input->post('meta_keyword'));
            $data_save['meta_desc'] = stripcslashes($this->input->post('meta_desc'));
            if ($data_save['cat_parent_id'] > 0) {
                $parent_level = $this->db->query("SELECT cat_level FROM tbl_categories WHERE cat_id = " . $data_save['cat_parent_id'])->row();
                $data_save['cat_level'] = $parent_level->cat_level + 1;
            } else {
                $data_save['cat_level'] = 1;
            }
            $t_cat = $this->db->select('*')->from('tbl_categories')->where(array('cat_is_delete' => 0))->get()->result();
            $data_save['cat_sort'] = count($t_cat);
            $j = 0;
            for ($j = 0; $j < 1; $j++) {#$totalFiles
                if (isset($_FILES['file' . ($j + 1)])) {
                    $myFiles = $_FILES['file' . ($j + 1)];
                    $_FILES['file' . ($j + 1)]['name'] = $myFiles['name'];
                    $_FILES['file' . ($j + 1)]['type'] = $myFiles['type'];
                    $_FILES['file' . ($j + 1)]['tmp_name'] = $myFiles['tmp_name'];
                    $_FILES['file' . ($j + 1)]['error'] = $myFiles['error'];
                    $_FILES['file' . ($j + 1)]['size'] = $myFiles['size'];
                    $file_name = $myFiles['name'];
                    $file_ext = explode('.', $file_name);
                    $file_extension = end($file_ext);
                    $image_path = $myFiles['tmp_name'];
                    $this->load->library('upload');
                    $file_Post = '';
                    if ($myFiles['name'] != '') {
                        $new_prod_image_name = "img_" . rand(1, 1000) . time();
                        $arrImg[$j] = $new_prod_image_name_ext = $new_prod_image_name . '.' . $file_extension;
                        //If user selected the photo to upload .
                        $uploaddir_11 = FCPATH . "resources/cat_image/";
                        $uploaddir_11 = str_replace(" ", "", $uploaddir_11);
                        $config_11['upload_path'] = $uploaddir_11;
                        $config_11['allowed_types'] = 'jpg|jpeg|gif|png|JPG|JPEG|GIF|PNG';
                        $config_11['overwrite'] = false;
                        $config_11['remove_spaces'] = true;
                        $config_11['file_name'] = $new_prod_image_name_ext;
                        $this->upload->initialize($config_11);
                        if (!$this->upload->do_upload('file' . ($j + 1))) {
                            $this->session->set_flashdata('msg_err' . $j, $this->upload->display_errors());
                            redirect(base_url() . "admin/categories/add_category/");
                            exit;
                        } else {
                            $uploadthumb = FCPATH . "resources/cat_image/thumb/";
                            $uploadthumb = str_replace(" ", "", $uploadthumb);
                            $width = 260;
                            $height = 380; //200
                            $thumb_image_name = 'thumb_' . $new_prod_image_name_ext;
                            ratio_image_resize($image_path, $uploadthumb, $thumb_image_name, $width, $height);
                            $config_thumb['upload_path'] = $uploadthumb;
                            $this->upload->initialize($config_thumb);
                            $dataDP = $this->upload->data();
                        } //else upload END
                    }//if (file[name] != '') END
                }//if isset(file) END
            }//for loop END
            $data_save['cat_image'] = $arrImg[0];
            //$data_save['cat_image_thumb'] = $arrImg[1];
            $cat_id = $this->common_model->commonSave('tbl_categories', $data_save);
            $this->session->set_flashdata('msg', 'Item added successfully.');
            redirect(base_url("admin/categories"));
            exit;
        }
    }

    public function edit_category($nId = '') {

        if ($nId == '' || !is_numeric($nId)) {
            redirect(base_url("admin/categories"));
            exit;
        }
        $data['title'] = "Edit Category";

        $data['cat_data'] = $this->common_model->commonselect('tbl_categories', 'cat_id', $nId)->row_array();

        if (empty($data['cat_data'])) {
            redirect(base_url("admin/categories"));
            exit;
        }

        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        if ($data['cat_data']['cat_url'] != $this->input->post('url')) {
            $this->form_validation->set_rules('url', 'URL', 'trim|required|callback_url_unique');
        } else {
            $this->form_validation->set_rules('url', 'URL', 'trim|required|is_unique[tbl_products.prod_url]');
        }
        $this->form_validation->set_rules('cat_status', 'status', 'trim|required');
        //$this->form_validation->set_rules('cat_desc', 'Description', 'trim|required');
        $this->form_validation->set_rules('meta_title', 'title', 'trim');
        $this->form_validation->set_rules('meta_keyword', 'keyword', 'trim');
        $this->form_validation->set_rules('meta_desc', 'desc', 'trim');
        $this->form_validation->set_rules('cat_parent_id', 'cat_parent_id', 'trim|required');

        //if ($this->input->post('cat_parent_id') == 0) {
            if ((isset($_FILES['file1']) || @$_FILES['file1']['name'] != '' || @$_FILES['file1']['type'] != '')) {
                $this->form_validation->set_rules('hidden_file1', 'file1', 'trim|required|callback_valid_file');
            }
        //}
        /* if((isset($_FILES['file2']) || @$_FILES['file2']['name'] != '' || @$_FILES['file2']['type'] != ''))
          {
          $this->form_validation->set_rules('hidden_file2', 'file2' , 'trim|required|callback_valid_file');
          } */

        $this->form_validation->set_message('required', 'This field is required.');

        if ($this->form_validation->run() == FALSE) {
            $data['pCategories'] = $this->db->query("
										SELECT cat_id, cat_name 
										FROM tbl_categories
										WHERE
										(cat_parent_id = 0
										OR cat_parent_id IN (SELECT cat_id FROM tbl_categories WHERE cat_parent_id = 0 AND cat_is_delete = 0) )
										AND
										cat_is_delete = 0
										ORDER BY cat_sort ASC
									")->result_array();

            $this->load->view('admin/categories/category_edit', $data);
        } else {
            $data_save['cat_name'] = stripcslashes($this->input->post('name'));
            $data_save['cat_url'] = $this->common_model->create_slug($data_save['cat_name'], 'tbl_products', 'prod_url');
            $data_save['cat_url'] = $this->common_model->create_slug($data_save['cat_url'], 'tbl_categories', 'cat_url', 'cat_id', $nId);
            $data_save['cat_status'] = stripcslashes($this->input->post('cat_status'));
            //$data_save['cat_desc'] = stripcslashes($this->input->post('cat_desc'));
            $data_save['cat_parent_id'] = $this->input->post('cat_parent_id');
            $data_save['meta_title'] = stripcslashes($this->input->post('meta_title'));
            $data_save['meta_keyword'] = stripcslashes($this->input->post('meta_keyword'));
            $data_save['meta_desc'] = stripcslashes($this->input->post('meta_desc'));
            if ($data_save['cat_parent_id'] > 0) {
                $parent_level = $this->db->query("SELECT cat_level FROM tbl_categories WHERE cat_id = " . $data_save['cat_parent_id'])->row();
                $data_save['cat_level'] = $parent_level->cat_level + 1;
            } else {
                $data_save['cat_level'] = 1;
            }

            $j = 0;
            for ($j = 0; $j < 1; $j++) {#$totalFiles
                if (@$_FILES['file' . ($j + 1)]['name'] == '' && $this->input->post('hidden_file' . ($j + 1)) != '') {
                    $arrImg[$j] = $this->input->post('hidden_file' . ($j + 1));
                } else {
                    if (isset($_FILES['file' . ($j + 1)])) {
                        if ($j == 0) {
                            unlink(FCPATH . "resources/cat_image/" . $data['cat_data']['cat_image']);
                            unlink(FCPATH . "resources/cat_image/thumb/thumb_" . $data['cat_data']['cat_image']);
                        } else {
                            unlink(FCPATH . "resources/cat_image/" . $data['cat_data']['cat_image_thumb']);
                            unlink(FCPATH . "resources/cat_image/thumb/thumb_" . $data['cat_data']['cat_image_thumb']);
                        }
                        $myFiles = $_FILES['file' . ($j + 1)];

                        $_FILES['file' . ($j + 1)]['name'] = $myFiles['name'];
                        $_FILES['file' . ($j + 1)]['type'] = $myFiles['type'];
                        $_FILES['file' . ($j + 1)]['tmp_name'] = $myFiles['tmp_name'];
                        $_FILES['file' . ($j + 1)]['error'] = $myFiles['error'];
                        $_FILES['file' . ($j + 1)]['size'] = $myFiles['size'];
                        $file_name = $myFiles['name'];
                        $file_ext = explode('.', $file_name);
                        $file_extension = end($file_ext);
                        $image_path = $myFiles['tmp_name'];
                        $this->load->library('upload');
                        $file_Post = '';

                        if ($myFiles['name'] != '') {
                            $new_prod_image_name = "img_" . rand(1, 1000) . time();

                            $arrImg[$j] = $new_prod_image_name_ext = $new_prod_image_name . '.' . $file_extension;

                            //If user selected the photo to upload .
                            $uploaddir_11 = FCPATH . "resources/cat_image/";
                            $uploaddir_11 = str_replace(" ", "", $uploaddir_11);
                            $config_11['upload_path'] = $uploaddir_11;
                            $config_11['allowed_types'] = 'jpg|jpeg|gif|png|JPG|JPEG|GIF|PNG';
                            $config_11['overwrite'] = false;
                            $config_11['remove_spaces'] = true;
                            $config_11['file_name'] = $new_prod_image_name_ext;

                            $this->upload->initialize($config_11);
                            if (!$this->upload->do_upload('file' . ($j + 1))) {
                                $this->session->set_flashdata('msg_err' . $j, $this->upload->display_errors());
                                redirect(base_url() . "admin/categories/edit_category/" . $nId);
                                exit();
                            } else {
                                $uploadthumb = FCPATH . "resources/cat_image/thumb/";
                                $uploadthumb = str_replace(" ", "", $uploadthumb);
                                $width = 260;
                                $height = 380; //200
                                $thumb_image_name = 'thumb_' . $new_prod_image_name_ext;
                                ratio_image_resize($image_path, $uploadthumb, $thumb_image_name, $width, $height);
                                $config_thumb['upload_path'] = $uploadthumb;
                                $this->upload->initialize($config_thumb);
                                $dataDP = $this->upload->data();
                            } //else upload END
                        }//if (file[name] != '') END
                    }//if isset(file) END
                }
            }//for loop END
            $data_save['cat_image'] = $arrImg[0];
            //$data_save['cat_image_thumb'] = $arrImg[1];

            $this->common_model->commonUpdate('tbl_categories', $data_save, "cat_id", $nId);
            $this->session->set_flashdata('msg', 'Item updated successfully.');
            redirect(base_url("admin/categories"));
            exit;
        }
    }

    public function delete_category($nId = '') {
        if ($nId == '' || !is_numeric($nId)) {
            redirect(base_url("admin/categories"));
            exit;
        }

        $data['cat_data'] = $this->common_model->commonselect('tbl_categories', 'cat_id', $nId)->row_array();
        if (empty($data['cat_data'])) {
            redirect(base_url("admin/categories"));
            exit;
        }
        /* $this->common_model->commonDelete('tbl_categories', 'cat_id', $nId);
          $this->common_model->commonDelete('tbl_categories', 'cat_parent_id', $nId); */
        $data_save['cat_is_delete'] = 1;
        $this->common_model->commonUpdate('tbl_categories', $data_save, "cat_id", $nId);
        $this->common_model->commonUpdate('tbl_categories', $data_save, "cat_parent_id", $nId);

        $this->session->set_flashdata('msg', 'Item deleted successfully.');
        redirect(base_url("admin/categories"));
        exit;
    }

    public function sorting() {

        // 
        $return['code'] = 200;
        $return['message'] = 'Success';
        $data = $this->input->post('data');

        foreach ($data as $row) {
            $dbData['cat_sort'] = $this->db->escape_str($row['ord']);
            $this->db->where('cat_id', $row['id']);
            $this->db->update('tbl_categories', $dbData);
        }

        echo json_encode($return);
    }

    public function filters($id = '') {
        $data['title'] = "Category Filters";
        if ($id == '' || !is_numeric($id)) {
            redirect(base_url("admin/categories"));
            exit;
        }
        $data['category'] = $this->common_model->commonselect3('tbl_categories', 'cat_id', $id, 'cat_level', 3, 'cat_is_delete', 0);
        if ($data['category']->num_rows() == 0) {
            redirect(base_url("admin/categories"));
            exit;
        }
        $data['categoryData'] = $data['category']->row();
        $data['filters'] = $this->common_model->commonselect('tbl_cat_filter_title', 'filter_cat_id', $id);
        /* echo '<pre>';
          print_r($data['filters']);exit; */
        $this->load->view('admin/categories/category_filter', $data);
    }

    /*     * *******************************************************************
     * ************************** CATEGORY FILTERS ************************
     * ******************************************************************* */

    private function filters_html($id) {
        $data['category'] = $this->common_model->commonselect3('tbl_categories', 'cat_id', $id, 'cat_level', 3, 'cat_is_delete', 0);
        $data['categoryData'] = $data['category']->row();
        $data['filters'] = $this->common_model->commonselect('tbl_cat_filter_title', 'filter_cat_id', $id);
        $html = $this->load->view('admin/categories/filter_html', $data, true);
        return $html;
    }

    public function add_nonfilter() {
        $categoryId = stripcslashes($this->input->post('id'));
        $action = stripcslashes($this->input->post('act'));
        $filterId = stripcslashes($this->input->post('tid'));

        $title = stripcslashes($this->input->post('title'));
        $values = stripcslashes($this->input->post('detail'));
        $dbData['filter_cat_id'] = $categoryId;
        $dbData['filter_title'] = $title;
        $dbData['cat_filter_values'] = $values;
        if ($action == 'add') {
            $dbData['filter_slug'] = $this->common_model->create_filter_slug($title, 'tbl_cat_filter_title', 'filter_slug');
            $this->db->insert('tbl_cat_filter_title', $dbData);
        } else {
            $previousvalue['cat_filter'] = $this->common_model->commonselect('tbl_cat_filter_title', 'filter_title_id', $filterId)->row();
            $prev_title = $previousvalue['cat_filter']->filter_title;
            $prev_detail = $previousvalue['cat_filter']->cat_filter_values;
            $dbData['filter_slug'] = $this->common_model->create_filter_slug($title, 'tbl_cat_filter_title', 'filter_slug', 'filter_title_id', $filterId);
            $this->db->where('filter_title_id', $filterId);
            $this->db->update('tbl_cat_filter_title', $dbData);
            if ($prev_title != $title || $prev_detail != $values) {
                $this->common_model->commonDelete('tbl_product_filters', 'filter_id', $filterId);
            }
        }
        $return['html'] = $this->filters_html($categoryId);
        echo json_encode($return);
    }

    public function delete_nonfilter() {
        $categoryId = stripcslashes($this->input->post('id'));
        $filterId = stripcslashes($this->input->post('tid'));
        $this->db->where('filter_title_id', $filterId);
        $this->db->delete('tbl_cat_filter_title');
        $return['html'] = $this->filters_html($categoryId);
        echo json_encode($return);
    }

    public function get_nonfilter() {
        $categoryId = stripcslashes($this->input->post('id'));
        $filterId = stripcslashes($this->input->post('tid'));
        $result = $this->categories_model->getFilterById($filterId);
        $return['data'] = $result->row();
        echo json_encode($return);
    }

    public function add_confilter() {
        $categoryId = stripcslashes($this->input->post('id'));
        $title = stripcslashes($this->input->post('title'));
        $dbData['filter_cat_id'] = $categoryId;
        $dbData['filter_title'] = $title;
        $dbData['cat_filter_is_conditional'] = 1;
        $dbData['filter_slug'] = $this->common_model->create_filter_slug($title, 'tbl_cat_filter_title', 'filter_slug');

        $this->db->insert('tbl_cat_filter_title', $dbData);
        $return['id'] = $this->db->insert_id();
        $return['html'] = $this->filters_html($categoryId);
        echo json_encode($return);
    }

    public function add_confilterdata() {
        $categoryId = stripcslashes($this->input->post('id'));
        $action = stripcslashes($this->input->post('act'));
        $filterId = stripcslashes($this->input->post('fid'));
        $dfilterId = stripcslashes($this->input->post('did'));
        $title = stripcslashes($this->input->post('title'));
        $values = stripcslashes($this->input->post('value'));
        $dbData['filter_detail_title_id'] = $filterId;
        $dbData['filter_detail_cat_id'] = $categoryId;
        $dbData['filter_title'] = $title;
        $dbData['filter_detail'] = $values;
        if ($action == 'add' || $action == 'addm') {
            $dbData['filter_slug'] = $this->common_model->create_filter_slug($title, 'tbl_cat_filter_detail', 'filter_slug');
            $this->db->insert('tbl_cat_filter_detail', $dbData);
        } else {
            $previousvalue['cat_filter'] = $this->common_model->commonselect('tbl_cat_filter_detail', 'filter_detail_id', $dfilterId)->row();
            $prev_title = $previousvalue['cat_filter']->filter_title;
            $prev_detail = $previousvalue['cat_filter']->filter_detail;
            $dbData['filter_slug'] = $this->common_model->create_filter_slug($title, 'tbl_cat_filter_detail', 'filter_slug', 'filter_detail_id', $dfilterId);
            $this->db->where('filter_detail_id', $dfilterId);
            $this->db->update('tbl_cat_filter_detail', $dbData);
            if ($prev_title != $title || $prev_detail != $values) {
                $this->common_model->commonDelete('tbl_product_filters_detail', 'filter_detail_id', $dfilterId);
            }
        }
        $return['html'] = $this->filters_html($categoryId);
        echo json_encode($return);
    }

    public function delete_confilter() {
        $categoryId = stripcslashes($this->input->post('id'));
        $filterId = stripcslashes($this->input->post('tid'));
        $this->db->where('filter_title_id', $filterId);
        $this->db->delete('tbl_cat_filter_title');
        $this->db->where('filter_detail_title_id', $filterId);
        $this->db->delete('tbl_cat_filter_detail');
        $return['html'] = $this->filters_html($categoryId);
        echo json_encode($return);
    }

    public function delete_confiltersingle() {
        $categoryId = stripcslashes($this->input->post('id'));
        $filterId = stripcslashes($this->input->post('tid'));
        $dfilterId = stripcslashes($this->input->post('did'));
        $this->db->where('filter_detail_id', $dfilterId);
        $this->db->where('filter_detail_title_id', $filterId);
        $this->db->delete('tbl_cat_filter_detail');
        $return['html'] = $this->filters_html($categoryId);
        echo json_encode($return);
    }

    public function update_contitle() {
        $categoryId = stripcslashes($this->input->post('cat'));
        $filterId = stripcslashes($this->input->post('id'));
        $value = stripcslashes($this->input->post('val'));
        $dbData['filter_title'] = $value;
        $dbData['filter_slug'] = $this->common_model->create_filter_slug($value, 'tbl_cat_filter_title', 'filter_slug', 'filter_title_id', $filterId);
        $this->db->where('filter_title_id', $filterId);
        $this->db->update('tbl_cat_filter_title', $dbData);
        //echo json_encode($return);
    }

    public function getdata_confilter() {
        $categoryId = stripcslashes($this->input->post('id'));
        $filterId = stripcslashes($this->input->post('tid'));
        $dfilterId = stripcslashes($this->input->post('did'));
        if (strlen($dfilterId) == 0) {
            $dfilterId = 0;
        }
        $result = $this->categories_model->getFilterById($filterId);
        $return['data'] = $result->row();
        $return['dataDetail'] = $this->categories_model->getFilterDetailById($dfilterId)->row();
        echo json_encode($return);
        //echo json_encode($return);
    }

    public function is_required() {

        $id = $this->input->post('id');
        $val = $this->input->post('val');
        $dbData['is_required'] = $val;
        $this->db->where('filter_title_id', (int) $id);
        $this->db->update('tbl_cat_filter_title', $dbData);
        echo $id . ' -> ' . $val;
    }

}

?>