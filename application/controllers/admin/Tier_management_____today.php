<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Tier_management extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('common/common_model');
        $this->load->helper(array('form', 'url'));
        auth();
    }

    public function index()
    {
        $data['title']        = "Tier Listing";
        $data['listing_list'] = $this->common_model->getCombox('tbl_tier_list', 'tier_id', "ASC")->result();
        $this->load->view('admin/pages/tier_management', $data);
    }

    public function edit_tier($id)
    {
        if ($id == '' || !is_numeric($id)) {
            redirect(base_url('admin/tier_management'));
        }
        $data['title']     = "Tier Listing";
        
        $data['tier_data'] = $this->common_model->commonselect('tbl_tier_list', 'tier_id', $id)->row();
        if(count($data['tier_data'])<=0)
        {
        	redirect(base_url('admin/tier_management'));
        }


        $data['sub_title'] = "Edit ".$data['tier_data']->tier_name;

        $this->form_validation->set_rules('tier_name', 'Tier Name', 'trim|required');
        $this->form_validation->set_rules('tier_title', 'Tier Title', 'trim|required');
        $this->form_validation->set_rules('fee', 'Transaction Fee', 'trim|required|numeric|greater_than_equal_to[1]');
        $this->form_validation->set_rules('months', 'Subscription duration', 'trim|required|numeric|greater_than_equal_to[0]|less_than_equal_to[12]');
        $this->form_validation->set_rules('amount', 'Amount/month', 'trim|required|numeric|greater_than_equal_to[0]');
        $this->form_validation->set_rules('pickup', 'Local Pickup', 'trim|required');
        $this->form_validation->set_rules('store', 'Facebook Store', 'trim|required');

        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('numeric', 'This field must be numeric.');

        if ($this->form_validation->run() == false) {

            $this->load->view('admin/pages/edit_tier', $data);
        } else {
            $array = array(
                'tier_name'   => $this->input->post('tier_name'),
                'tier_title'  => $this->input->post('tier_title'),
                'tier_fee'    => $this->input->post('fee'),
                'tier_months' => $this->input->post('months'),
                'tier_amount' => $this->input->post('amount'),

                'is_pickup'   => $this->input->post('pickup'),
                'is_fb_store' => $this->input->post('store'),
            );
            $array['update'] = date('Y-m-d H:i:s');

            $this->common_model->commonUpdate('tbl_tier_list', $array, 'tier_id', $id);

            $data['msg'] = 'Item updated successfully.';
            $this->session->set_flashdata('msg', $data['msg']);

            redirect(base_url('admin/tier_management'));
            exit;
        }
    }
}
?>

