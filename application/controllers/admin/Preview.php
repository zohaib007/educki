<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Preview extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->helper(array('form', 'url'));
        $this->load->model('common_model');
        $this->load->model('front/faq/faq_model');
        $this->load->model('careers/career_model');
        auth();
    }

    public function terms_of_service_preview()
    {
        $data['pg_title']       = $this->input->post('pg_title');
        $data['pg_description'] = $this->input->post('pg_description');

        $data['title']          = $data['pg_title'];

        $data['content_view'] = "terms-of-service";
        $this->load->view("admin/preview/preview_template", $data);
    }

    public function privacy_policy_preview()
    {
        $data['pg_title']       = $this->input->post('pg_title');
        $data['pg_description'] = $this->input->post('pg_description');

        $data['title']          = $data['pg_title'];

        $data['content_view'] = "privacy-policy";
        $this->load->view("admin/preview/preview_template", $data);
    }

    public function pricing_benefits_preview()
    {
        $data['title']                = "Pricing & Benefits";
        $data['page_title']           = $this->input->post('page_title');
        $data['page_description']     = $this->input->post('page_description');
        $data['img_description_1']    = $this->input->post('desc1');
        $data['img_description_2']    = $this->input->post('desc2');
        $data['img_description_3']    = $this->input->post('desc3');
        $data['meta_title']           = $this->input->post('meta_title');
        $data['meta_keywords']        = $this->input->post('meta_keywords');
        $data['meta_description']     = $this->input->post('meta_description');
        $data['offer_line']           = $this->input->post('offer');
        $data['benefits_description'] = $this->input->post('benefits_description');
        $data['status']               = $this->input->post('pg_status');

        $file1 = $this->input->post('hidden_file1old');

        if (!empty($_FILES["file1"]['name'])) {
            $imagedata          = file_get_contents($_FILES["file1"]["tmp_name"]);
            $data['image_data'] = base64_encode($imagedata);
            $data['image_1']    = "data:image/png;base64, " . $data['image_data'] . "";
        } else {
            $data['image_1'] = base_url('resources/pricing_benefits/' . $file1);
        }

        $file2 = $this->input->post('hidden_file2old');

        if (!empty($_FILES["file2"]['name'])) {
            $imagedata          = file_get_contents($_FILES["file2"]["tmp_name"]);
            $data['image_data'] = base64_encode($imagedata);
            $data['image_2']    = "data:image/png;base64, " . $data['image_data'] . "";
        } else {
            $data['image_2'] = base_url('resources/pricing_benefits/' . $file2);
        }

        $file3 = $this->input->post('hidden_file3old');

        if (!empty($_FILES["file3"]['name'])) {
            $imagedata          = file_get_contents($_FILES["file3"]["tmp_name"]);
            $data['image_data'] = base64_encode($imagedata);
            $data['image_3']    = "data:image/png;base64, " . $data['image_data'] . "";
        } else {
            $data['image_3'] = base_url('resources/pricing_benefits/' . $file3);
        }

        $data['tierdata']   = $this->common_model->getCombox('tbl_tier_list')->result();
        $data['base_value'] = $data['tierdata'][1]->tier_amount;

        $data['content_view'] = "pricing-and-benefits";
        $this->load->view("admin/preview/preview_template", $data);
    }

    public function career_preview()
    {
        $data['title']             = "Career";
        $data['page_title']        = $this->input->post('pg_title');
        $data['page_description']  = $this->input->post('pg_description');
        $data['banner_title']      = $this->input->post('banner_title');
        $data['image_1_title']     = $this->input->post('first_image_title');
        $data['image_2_title']     = $this->input->post('second_image_title');
        $data['image_3_title']     = $this->input->post('third_image_title');
        $data['img_description_1'] = $this->input->post('first_image_description');
        $data['img_description_2'] = $this->input->post('second_image_description');
        $data['img_description_3'] = $this->input->post('third_image_description');
        $data['meta_title']        = $this->input->post('meta_title');
        $data['meta_keywords']     = $this->input->post('meta_keywords');
        $data['meta_description']  = $this->input->post('meta_description');

        if (!empty($_FILES["banner_image"]['name'])) {
            $imagedata          = file_get_contents($_FILES["banner_image"]["tmp_name"]);
            $data['image_data'] = base64_encode($imagedata);
            $data['banner']     = "data:image/png;base64, " . $data['image_data'] . "";
        } else {
            $banner         = $this->input->post('b_image');
            $data['banner'] = base_url('resources/career_image/career_page/' . $banner);
        }

        if (!empty($_FILES["first_image"]['name'])) {
            $imagedata          = file_get_contents($_FILES["first_image"]["tmp_name"]);
            $data['image_data'] = base64_encode($imagedata);
            $data['image_1']    = "data:image/png;base64, " . $data['image_data'] . "";
        } else {
            $file1           = $this->input->post('fir_name');
            $data['image_1'] = base_url('resources/career_image/career_page/' . $file1);
        }

        if (!empty($_FILES["second_image"]['name'])) {
            $imagedata          = file_get_contents($_FILES["second_image"]["tmp_name"]);
            $data['image_data'] = base64_encode($imagedata);
            $data['image_2']    = "data:image/png;base64, " . $data['image_data'] . "";
        } else {
            $file2           = $this->input->post('sec_image');
            $data['image_2'] = base_url('resources/career_image/career_page/' . $file2);
        }

        if (!empty($_FILES["third_image"]['name'])) {
            $imagedata          = file_get_contents($_FILES["third_image"]["tmp_name"]);
            $data['image_data'] = base64_encode($imagedata);
            $data['image_3']    = "data:image/png;base64, " . $data['image_data'] . "";
        } else {
            $file3           = $this->input->post('thir_image');
            $data['image_3'] = base_url('resources/career_image/career_page/' . $file3);
        }

        $count_where               = 'WHERE career_is_deleted = 0 AND career_is_active = 1';
        $data['total_records']     = count($this->common_model->get_data_page("tbl_career", $count_where)->result_array());
        $where                     = 'career_is_deleted = 0 AND career_is_active = 1';
        $jointable                 = 'tbl_states';
        $joincondition             = 'tbl_states.stat_id = tbl_career.career_state';
        $jointype                  = 'inner';
        $orderby                   = 'career_id';
        $order                     = 'desc';
        $data['list']              = $this->career_model->career_applied_user_listing("tbl_career", $where, $jointable, $joincondition, $jointype, '', '', $is_count = 0, $orderby, $order)->result_array();
        $orderby                   = 'career_title';
        $data['alphabetical_list'] = $this->career_model->career_applied_user_listing("tbl_career", $where, $jointable, $joincondition, $jointype, '', '', $is_count = 0, $orderby, $order = 'ASC')->result_array();

        $data['content_view'] = "career/career_list";
        $this->load->view("admin/preview/preview_template", $data);
    }

    public function what_is_educki_preview()
    {
        $data['title']             = "What is eDucki";
        $data['page_title']        = $this->input->post('page_title');
        $data['title']             = $this->input->post('title');
        $data['page_description']  = $this->input->post('pg_description');
        $data['img_description_1'] = $this->input->post('desc1');
        $data['img_description_2'] = $this->input->post('desc2');
        $data['img_description_3'] = $this->input->post('desc3');
        $data['img_description_4'] = $this->input->post('desc4');

        $file1 = $this->input->post('hidden_file1');

        if (!empty($_FILES["file1"]['name'])) {
            $imagedata          = file_get_contents($_FILES["file1"]["tmp_name"]);
            $data['image_data'] = base64_encode($imagedata);
            $data['image_1']    = "data:image/png;base64, " . $data['image_data'] . "";
        } else {
            $data['image_1'] = base_url('resources/work_page_images/' . $file1);
        }

        $file2 = $this->input->post('hidden_file2');

        if (!empty($_FILES["file2"]['name'])) {
            $imagedata          = file_get_contents($_FILES["file2"]["tmp_name"]);
            $data['image_data'] = base64_encode($imagedata);
            $data['image_2']    = "data:image/png;base64, " . $data['image_data'] . "";
        } else {
            $data['image_2'] = base_url('resources/work_page_images/' . $file2);
        }

        $file3 = $this->input->post('hidden_file3');

        if (!empty($_FILES["file3"]['name'])) {
            $imagedata          = file_get_contents($_FILES["file3"]["tmp_name"]);
            $data['image_data'] = base64_encode($imagedata);
            $data['image_3']    = "data:image/png;base64, " . $data['image_data'] . "";
        } else {
            $data['image_3'] = base_url('resources/work_page_images/' . $file3);
        }

        $file4 = $this->input->post('hidden_file4');

        if (!empty($_FILES["file4"]['name'])) {
            $imagedata          = file_get_contents($_FILES["file4"]["tmp_name"]);
            $data['image_data'] = base64_encode($imagedata);
            $data['image_4']    = "data:image/png;base64, " . $data['image_data'] . "";
        } else {
            $data['image_4'] = base_url('resources/work_page_images/' . $file4);
        }

        $file5 = $this->input->post('hidden_file5');

        if (!empty($_FILES["file5"]['name'])) {
            $imagedata          = file_get_contents($_FILES["file5"]["tmp_name"]);
            $data['image_data'] = base64_encode($imagedata);
            $data['image_5']    = "data:image/png;base64, " . $data['image_data'] . "";
        } else {
            $data['image_5'] = base_url('resources/work_page_images/' . $file5);
        }

        $data['content_view'] = "about/what-is-educki";
        $this->load->view("admin/preview/preview_template", $data);
    }

    public function community_guidelines_preview()
    {
        $data['title']          = "Community Guidelines";
        $data['pg_title']       = $this->input->post('pg_title');
        $data['pg_description'] = $this->input->post('pg_description');

        $file4 = $this->input->post('image');

        if (!empty($_FILES["image"]['name'])) {
            $imagedata          = file_get_contents($_FILES["image"]["tmp_name"]);
            $data['image_data'] = base64_encode($imagedata);
            $data['banner']     = "data:image/png;base64, " . $data['image_data'] . "";
        } else {
            if (empty($this->input->post('image'))) {
                $data['banner'] = '';
            } else {
                $data['banner'] = base_url('resources/page_image/' . $file4);
            }
        }

        $data['content_view'] = "about/about-community";
        $this->load->view("admin/preview/preview_template", $data);
    }

    public function educki_protect_preview()
    {
        $data['title']          = "eDucki Protect";
        $data['pg_title']       = $this->input->post('pg_title');
        $data['pg_description'] = $this->input->post('pg_description');

        $file4 = $this->input->post('image');

        if (!empty($_FILES["image"]['name'])) {
            $imagedata          = file_get_contents($_FILES["image"]["tmp_name"]);
            $data['image_data'] = base64_encode($imagedata);
            $data['banner']     = "data:image/png;base64, " . $data['image_data'] . "";
        } else {
            if (empty($this->input->post('image'))) {
                $data['banner'] = '';
            } else {
                $data['banner'] = base_url('resources/page_image/' . $file4);
            }
        }

        $data['content_view'] = "about/about-protect";
        $this->load->view("admin/preview/preview_template", $data);
    }

    public function faq_preview()
    {
        $data['title']    = "FAQs";
        $data['pg_title'] = $this->input->post('pg_title');

        $file4 = $this->input->post('image');

        if (!empty($_FILES["image"]['name'])) {
            $imagedata          = file_get_contents($_FILES["image"]["tmp_name"]);
            $data['image_data'] = base64_encode($imagedata);
            $data['banner']     = "data:image/png;base64, " . $data['image_data'] . "";
        } else {
            if (empty($this->input->post('image'))) {
                $data['banner'] = '';
            } else {
                $data['banner'] = base_url('resources/page_image/' . $file4);
            }
        }

        $data['list'] = $this->faq_model->view_list(6);

        $data['content_view'] = "about/faq";
        $this->load->view("admin/preview/preview_template", $data);
    }

    public function educki_guide_preview()
    {
        $data['title']    = "FAQs";
        $data['pg_title'] = $this->input->post('pg_title');

        $file4 = $this->input->post('image');

        if (!empty($_FILES["image"]['name'])) {
            $imagedata          = file_get_contents($_FILES["image"]["tmp_name"]);
            $data['image_data'] = base64_encode($imagedata);
            $data['banner']     = "data:image/png;base64, " . $data['image_data'] . "";
        } else {
            if (empty($this->input->post('image'))) {
                $data['banner'] = '';
            } else {
                $data['banner'] = base_url('resources/page_image/' . $file4);
            }
        }

        $data['list'] = $this->faq_model->view_list(6);

        $data['content_view'] = "about/faq";
        $this->load->view("admin/preview/preview_template", $data);
    }

    public function sub_page_preview($id = '')
    {
        $data['title']        = $this->input->post('listing_title');
        $data['banner_title'] = $this->input->post('banner_title');
        $data['description']  = $this->input->post('pg_description');

        $file4 = $this->input->post('banner_image');

        if (!empty($_FILES["banner_image"]['name'])) {
            $imagedata          = file_get_contents($_FILES["banner_image"]["tmp_name"]);
            $data['image_data'] = base64_encode($imagedata);
            $data['banner']     = "data:image/png;base64, " . $data['image_data'] . "";
        } else {
            if (empty($this->input->post('banner_image'))) {
                $data['banner'] = '';
            } else {
                $data['banner'] = base_url('resources/page_image/listing/banner/' . $file4);
            }
        }

        if ($id == 7 || $id == 8 || $id == 9 || $id == 10 || $id == 11 || $id == 12 || $id == 18 || $id == 19 || $id == 20) {
            $where        = 'WHERE list_listing_id = ' . $id;
            $data['list'] = $this->common_model->get_data_page("tbl_pages_sub_listing", $where)->result();
            if (count($data['list']) > 0) {
                $data['id'] = $data['list'][0]->list_listing_id;
            } else {
                $data['id'] = '';
            }
        } else {
            $data['id'] = '';
        }

        /*echo "<pre>";
        print_r($data);
        exit;*/

        /*$data['list'] = $this->faq_model->view_list(6);*/

        $data['content_view'] = "about/listing-page";
        $this->load->view("admin/preview/preview_template", $data);
    }
}
