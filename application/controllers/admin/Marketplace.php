<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Marketplace extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('admin/admin_model');
        $this->load->model('common/common_model');
        auth();
    }

    public function billing()
    {
        $data['title']   = "Marketplace Billing";
        $data['sellers'] = $this->db->query("SELECT * 
                                            FROM tbl_user user
                                            WHERE user.user_type = 'Seller' AND user.user_is_delete = 0 
                                            ORDER BY user.user_id DESC
                                            ")->result_array();
        $this->load->view('admin/marketplace/billing/marketplace_listing', $data);
    }

    public function billing_detail($seller_id)
    {
        $data['title'] = "Marketplace Billing Details";

        $data['default_shipping']     = $this->common_model->default_shipping($seller_id);
        $data['default_billing']      = $this->common_model->default_billing($seller_id);
        $data['personal_information'] = $this->common_model->getSellerInfoById($seller_id);

        $store_id = getStoreInfoByUserId($seller_id)->store_id;
        $data['user_reported'] = $this->db->query("
                                            SELECT *
                                            FROM tbl_stores
                                            WHERE user_id = ".$seller_id."
                                            ")->row()->store_reported;
        $data['products']             = $this->db->query("SELECT prod.*
                                            FROM tbl_products prod
                                            INNER JOIN tbl_stores as store ON store.store_id = prod.prod_store_id
                                            WHERE store.user_id = " . $seller_id . "
                                            ")->result_array();

        $data['subscription'] = $this->db->query("SELECT subs.*,tier.tier_name
                                            FROM tbl_user_store_subscription subs
                                            INNER JOIN tbl_stores as store ON store.store_id = subs.sub_store_id
                                            INNER JOIN tbl_tier_list as tier ON tier.tier_id = subs.sub_tier_id
                                            WHERE store.user_id = " . $seller_id . "
                                            ")->result();

        $data['comments'] = $this->db->query("SELECT *
                                            FROM tbl_admin_comments
                                            WHERE seller_id = " . $seller_id . "
                                            ")->result();

        $data['GMV'] = $this->db->query("
                                            SELECT SUM(com.store_total) as gmv
                                            FROM tbl_order_seller_info com
                                            INNER JOIN tbl_orders ordr ON com.order_id = ordr.order_id 
                                            WHERE com.store_id = ".$store_id." AND ordr.order_status = 'completed'
                                            ")->row()->gmv;

        $data['fees'] = $this->db->query("
                                            SELECT SUM(com.store_commission_total) as fees
                                            FROM tbl_order_seller_info com
                                            INNER JOIN tbl_orders ordr ON com.order_id = ordr.order_id 
                                            WHERE com.store_id = ".$store_id." AND ordr.order_status = 'completed'
                                            ")->row()->fees;

        $data['subscribed'] = $this->db->query("
                                            SELECT SUM(timestampdiff(DAY,sub_start,sub_ends)) as subscribed
                                            FROM tbl_user_store_subscription 
                                            WHERE sub_store_id = ".$store_id."
                                            ")->row()->subscribed;

        $data['no_of_products'] = $this->db->query("
                                            SELECT count(*) as total_products
                                            FROM tbl_products prod
                                            INNER JOIN tbl_user user ON user.user_id = prod.prod_user_id
                                            WHERE user.user_id = ".$seller_id." AND prod.prod_is_delete = 0
                                            ")->row()->total_products;

        $data['sub_revenue'] = $this->db->query("
                                            SELECT SUM((timestampdiff(DAY,sub_start,sub_ends)/30)*tier_price) as sub_rev
                                            FROM tbl_user_store_subscription 
                                            WHERE sub_store_id = ".$store_id."
                                            ")->row()->sub_rev;

        $data['sub_left'] = $this->db->query("
                                            SELECT timestampdiff(DAY,CURDATE(),sub_ends) as sub_left
                                            FROM tbl_user_store_subscription 
                                            WHERE sub_store_id = ".$store_id."
                                            ")->row()->sub_left;

        /*echo "<pre>";
        print_r($data);
        exit;*/

        $this->load->view('admin/marketplace/billing/view_billing_details', $data);
    }

    public function management_listing()
    {
        $data['title']   = "Marketplace Management";
        $data['sellers'] = $this->db->query("
                                            SELECT * 
                                            FROM tbl_user user
                                            INNER JOIN tbl_stores store ON user.user_id = store.user_id
                                            WHERE user.user_type = 'Seller' AND user.user_is_delete = 0 
                                            ORDER BY user.user_id DESC                                        
                                            ")->result_array();
            /*echo "<pre>";
            print_r($data);
            exit;*/
        $this->load->view('admin/marketplace/management/marketplace_listing', $data);
    }

    public function management_detail($seller_id)
    {
        $data['title']   = "Details Page";
        $data['seller'] = $this->db->query("
                                            SELECT *
                                            FROM tbl_user user
                                            INNER JOIN tbl_stores store ON store.user_id = user.user_id
                                            WHERE user.user_id = ".$seller_id." AND user.user_is_delete = 0
                                            ")->row();

        if(count($data['seller'])<=0){
            redirect(base_url('admin/marketplace/management_listing'));
        }
        
        $data['no_of_products'] = $this->db->query("
                                            SELECT count(*) as total_products
                                            FROM tbl_products prod
                                            INNER JOIN tbl_user user ON user.user_id = prod.prod_user_id
                                            WHERE user.user_id = ".$seller_id." AND prod.prod_is_delete = 0
                                            ")->row();
        $data['tier_data'] = $this->db->query("
                                            SELECT sub.*
                                            FROM tbl_user_store_subscription sub
                                            INNER JOIN tbl_stores store ON store.store_id = sub.sub_store_id
                                            WHERE sub.sub_store_id = '".$data['seller']->store_id."'
                                            ORDER BY sub.sub_id DESC LIMIT 1
                                            ")->row();
        $data['user_reported'] = $this->db->query("
                                            SELECT *
                                            FROM tbl_stores
                                            WHERE user_id = ".$seller_id."
                                            ")->row()->store_reported;

        $data['prod_reported'] = $this->db->query("
                                            SELECT SUM(prod_report) as prod_report
                                            FROM tbl_products
                                            WHERE prod_is_delete = 0 AND prod_user_id = ".$seller_id."
                                            ")->row()->prod_report;

        $data['months_left'] = $this->db->query("
                                            SELECT timestampdiff(MONTH,CURDATE(),sub_ends) as months_left
                                            FROM tbl_user_store_subscription
                                            WHERE sub_store_id = ".$data['seller']->store_id." AND sub_id = ".$data['tier_data']->sub_id."
                                            ")->row()->months_left;

        $tier_starts = date("m-d-Y",strtotime($data['tier_data']->sub_start));
        $tier_ends   = date("m-d-Y",strtotime($data['tier_data']->sub_ends));
        $today_date  = date("m-d-Y");

        if($data['tier_data']->sub_free_tier!='Y'){
            if($today_date>=$tier_starts && $today_date<=$tier_ends){
                //$data['next_payment'] = date('Y-m-d', strtotime($data['tier_data']->sub_start . "+".$data['tier_data']->months." months"));
                $data['next_payment'] = date("m-d-Y",strtotime($data['tier_data']->next_payment));
            }elseif($today_date>$tier_ends){                
                $data['months_left'] = "Expired";
                $data['next_payment'] = "Expired";
            }
        }else{
            $data['months_left'] = "N/A";
            $data['next_payment'] = "N/A";
        }

        $data['renewal'] = $this->db->query("
                                            SELECT count(*) as renewal
                                            FROM tbl_user_store_subscription
                                            WHERE sub_is_renewal = 1 AND sub_store_id = ".$data['seller']->store_id."
                                            ")->row()->renewal;

        $data['lifetime_value'] = $this->db->query("
                                            SELECT SUM(ordr.order_total_commission)+SUM(timestampdiff(DAY,sub.sub_start,sub.sub_ends)/30)*sub.tier_price as lifetime
                                            FROM tbl_orders ordr
                                            INNER JOIN tbl_order_seller_info com ON ordr.order_id = com.order_id
                                            INNER JOIN  tbl_user_store_subscription sub ON sub.sub_store_id = com.store_id                                            
                                            WHERE com.store_id = ".$data['seller']->store_id." AND ordr.order_status = 'completed'
                                            ")->row()->lifetime;

        $data['fees'] = $this->db->query("
                                            SELECT SUM(com.store_commission_total) as fees
                                            FROM tbl_order_seller_info com
                                            INNER JOIN tbl_orders ordr ON com.order_id = ordr.order_id 
                                            WHERE com.store_id = ".$data['seller']->store_id." AND ordr.order_status = 'completed'
                                            ")->row()->fees;

        $data['GMV'] = $this->db->query("
                                            SELECT SUM(com.store_total-com.store_shipping_total) as gmv
                                            FROM tbl_order_seller_info com
                                            INNER JOIN tbl_orders ordr ON com.order_id = ordr.order_id 
                                            WHERE com.store_id = ".$data['seller']->store_id." AND ordr.order_status = 'completed'
                                            ")->row()->gmv;

        $data['subscribed'] = $this->db->query("
                                            SELECT SUM(timestampdiff(DAY,sub_start,sub_ends))/30 as subscribed
                                            FROM tbl_user_store_subscription 
                                            WHERE sub_store_id = ".$data['seller']->store_id."
                                            ")->row()->subscribed;

        /*echo "<pre>";
        print_r($this->db->last_query());
        print_r($data);
        exit;*/

        $this->load->view('admin/marketplace/management/detail_page', $data);
    }

    public function cancel_subscription($seller_id)    
    {
        $data['title'] = "Cancel Subscription";

        $this->form_validation->set_rules('other_reason', 'Reason', 'trim|required');
        if($this->input->post('other_reason')==7){
            $this->form_validation->set_rules('other', 'Reason', 'trim|required');
        }

        $this->form_validation->set_message('required', 'This field is required.');

        $data['seller'] = $this->db->query("
                                            SELECT *
                                            FROM tbl_user user
                                            INNER JOIN tbl_stores store ON store.user_id = user.user_id
                                            WHERE user.user_id = ".$seller_id." AND user.user_is_delete = 0 AND store.store_tier_id != 1
                                            ")->row();
        if(count($data['seller'])<=0){
            redirect(base_url('admin/marketplace/management_listing'));
        }

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/marketplace/management/cancel_sub', $data);
        }else{
            //update previous subscription
            $update_data['sub_ends'] = date('Y-m-d h:i:s');
            $this->common_model->commonUpdate('tbl_user_store_subscription', $update_data,'sub_tier_id',$data['seller']->store_tier_id);
            
            //update store subscription
            $dbdata['store_tier_id'] = 1;
            $dbdata['store_end_date'] = date('Y-m-d h:i:s');
            $this->common_model->commonUpdate('tbl_stores', $dbdata, 'store_id', $data['seller']->store_id);

            //update tbl_user_store_subscription
            $sub_data['sub_tier_id'] = 1;
            $sub_data['sub_start'] = date('Y-m-d h:i:s');
            $sub_data['next_payment'] = date('Y-m-d h:i:s');
            $sub_data['sub_ends'] = date('Y-m-d h:i:s');
            $sub_data['sub_store_id'] = $data['seller']->store_id;
            $sub_data['tier_price'] = 0;            
            $sub_data['is_cancel'] = 1;
            $sub_data['sub_free_tier'] = 'Y';
            $sub_data['sub_is_renewal'] = 0;
            $sub_data['sub_is_churn'] = 1;

            $this->common_model->commonSave('tbl_user_store_subscription', $sub_data);

            $cancel['reason'] = $this->input->post('other_reason');
            $cancel['other'] = $this->input->post('other');
            $cancel['seller_id'] = $seller_id;
            $cancel['cancel_date'] = date('Y-m-d h:i:s');

            $this->common_model->commonSave('tbl_cancel_subscription', $cancel);
            /*echo "<pre>";
            print_r($data);
            print_r($tier);
            print_r($sub_data);
            exit;*/
            $this->session->set_flashdata('msg','Tier Canceled Successfully.');
            redirect(base_url().'admin/marketplace/management_listing/');
        }        
    }

    public function register_comment()
    {
        $this->form_validation->set_rules('comment', 'Comment', 'trim|required');

        $this->form_validation->set_message('required', 'This field is required.');

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('comment_error', 'This field is required');
            redirect(base_url().'admin/marketplace/billing_detail/' . $this->input->post('seller_id'));
        } else {
            $data_save['seller_id'] = $this->input->post('seller_id');
            $data_save['admin_id']  = $this->session->userdata('admin_id');
            $data_save['comment']   = $this->input->post('comment');

            /*echo "<pre>";
            print_r($data_save);
            exit;*/

            $this->common_model->commonSave('tbl_admin_comments', $data_save);
            $this->session->set_flashdata('msg', 'Item added successfully.');
            redirect(base_url('admin/marketplace/billing_detail/'.$data_save['seller_id']));
        }
    }

    public function delete_comment($id='',$seller_id='')
    {
        $this->common_model->commonDelete('tbl_admin_comments', 'comment_id',$id);
        $this->session->set_flashdata('msg', 'Item deleted successfully.');
        redirect(base_url('admin/marketplace/billing_detail/'.$seller_id));
    }

    public function edit_comment()
    {
        $id = $this->input->post('id');
        $data['comment'] = $this->input->post('comment');
        $result = $this->common_model->commonUpdate('tbl_admin_comments', $data,'comment_id',$id);
        if($result){
            $return['status'] = 200;
            $return['message'] = "Item updated successfully.";
            $this->session->set_flashdata('msg', 'Item updated successfully.');
        }else{
            $return['status'] = 201;
            $return['message'] = "There was a problem.";
            $this->session->set_flashdata('msg', 'There was a problem.');
        }
        echo json_encode($return);
    }

    public function exportcsv()
    {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=billing_' . date("m-d-Y") . '.csv');
        $output = fopen('php://output', 'w');
        fputcsv($output, array('Customer ID','Seller Name','Email','Customer Since','Subscription Tier','Gross Sales','YTD Transaction Cost','Status'));
        $seller_data = $this->db->query("SELECT *  
                                            FROM tbl_user user
                                            WHERE user.user_type = 'Seller' AND user.user_is_delete = 0 
                                            ORDER BY user.user_id DESC
                                            ")->result_array();
        //echo "<pre>";

        for ($n = 0; $n < count($seller_data); $n++) {
            $data['user_id'] = $seller_data[$n]['user_id'];
            $data['user_name'] = $seller_data[$n]['user_fname']." ".$seller_data[$n]['user_lname'];
            $data['user_email'] = $seller_data[$n]['user_email'];
            $data['user_register_date'] = date('m-d-Y',strtotime($seller_data[$n]['user_register_date']));
            $data['tier'] = getTierName($seller_data[$n]['user_id']);
            $data['GMV'] = (calculateGMV($seller_data[$n]['user_id'])>0)?number_format(calculateGMV($seller_data[$n]['user_id']),2,'.',''):'0.00';
            $data['YTD'] = (calculateYTD($seller_data[$n]['user_id'])>0)?number_format(calculateYTD($seller_data[$n]['user_id']),2,'.',''):'0.00';
            $data['status'] = ($seller_data[$n]['user_status'] == 1)?'Active':'InActive';
            fputcsv($output, $data);
            //print_r($data);
        }
        //exit;
    }
}
