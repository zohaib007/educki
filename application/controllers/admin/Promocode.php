<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class promocode extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
//        redirect(base_url('admin'));
//        exit;
        $this->load->model('categories/categories_model');
        $this->load->model('product/product_model');
        $this->load->model('promocode_model');
        $this->load->library('upload');
        date_default_timezone_set("US/Eastern");

    }

    public function index()
    {
        $data['promo_list'] = $this->promocode_model->listing_data();
        /* Expire promo code */
        foreach ($data['promo_list'] as $promo_list) {
            $promo_end    = strtotime($promo_list['promo_end_date']);
            $current_time = strtotime(date('Y-m-d'));
            if ($current_time > $promo_end) {
                $array['promo_status'] = 0;
                $this->common_model->commonUpdate('tbl_promocode', $array, 'promo_id', $promo_list['promo_id']);
            }
        }
        /* Expire promo code */
        $data['promo_list']    = $this->promocode_model->listing_data();
        $data['total_records'] = count($data['promo_list']);
        $this->load->view('admin/promocode/promocode_listing', $data);
    }

    public function add()
    {
        $this->form_validation->set_rules('promo_code', 'promocode code', 'required');
        $this->form_validation->set_rules('promo_status', 'promo_category_id', 'required');
        if ($this->input->post('promo_type') == 1) {
            $this->form_validation->set_rules('promo_category_id', 'promo_category_id', 'required');
            $this->form_validation->set_rules('promo_sub_cat_id', 'promo_sub_cat_id', 'required');
            $this->form_validation->set_rules('promo_product_id', 'promo_product_id', 'required');
        }

        $this->form_validation->set_rules('promo_type', 'promo_price_amount', 'required');
        if ($this->input->post('promo_discount_type') == 0) {
            $this->form_validation->set_rules('promo_price_usd', 'price', 'required|numeric|greater_than[0]');

            $price = $this->input->post('promo_price_usd');
        } else {
            $this->form_validation->set_rules('promo_price_percentage', 'price', 'required|numeric|less_than[100]|greater_than[0]');
            $price = $this->input->post('promo_price_percentage');
        }
        $this->form_validation->set_rules('promo_discount_type', 'promo_start_date', 'required');
        $this->form_validation->set_rules('promo_start_date', 'promo_start_date', 'required');
        $this->form_validation->set_rules('promo_end_date', 'promo_end_date', 'required');
        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('less_than', 'This field must be less then 100.');
        $this->form_validation->set_message('greater_than', 'This field must be greater then 0.');

        $data['main_cat'] = $main_cat = $this->categories_model->getCategories();
        
        if ($this->form_validation->run() == false) {
            $this->load->view('admin/promocode/add_promocode', $data);

        } else {
            $date          = date('Y-m-d');
            $orignal_date  = explode("-", $this->input->post('promo_start_date'));
            $start_date    = $orignal_date[2] . "-" . $orignal_date[0] . "-" . $orignal_date[1];
            $orignal_date1 = explode("-", $this->input->post('promo_end_date'));
            $end_date      = $orignal_date1[2] . "-" . $orignal_date1[0] . "-" . $orignal_date1[1];
            if (strtotime($start_date) > strtotime($end_date)) {
                $this->session->set_flashdata('msg', 'Please select the correct date range.');
                redirect(base_url() . "admin/promocode/add");
                exit;

            }
            $array = array(
                'promo_code'          => $this->input->post('promo_code'),
                'promo_type'          => $this->input->post('promo_type'),
                'promo_category_id'   => $this->input->post('promo_category_id'),
                'promo_sub_cat_id'    => $this->input->post('promo_sub_cat_id'),
                'promo_product_id'    => $this->input->post('promo_product_id'),
                'promo_discount_type' => $this->input->post('promo_discount_type'),
                'promo_price_amount'  => $price,
                'promo_start_date'    => $start_date,
                'promo_end_date'      => $end_date,
                'promo_status'        => $this->input->post('promo_status'),
                'promo_created_date'  => $date,
            );

            $this->promocode_model->submitdata($array);
            $this->session->set_flashdata('msg', 'Item added successfully.');
            redirect(base_url() . "admin/promocode");
            exit;
        }
    }

    public function edit($promo_id)
    {
        
        $data['promo_edit_list']   = $this->promocode_model->edit_promo($promo_id);
        $data['promo_category_id'] = $this->common_model->common_where('tbl_categories', array("cat_id" => $data['promo_edit_list'][0]['promo_category_id']))->result_array();
//        $data['promo_sub_cat_id']  = $this->common_model->common_where('tbl_categories', array("cat_cat_id" => $data['promo_edit_list'][0]['promo_category_id']))->result_array();
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
        die;
        $data['cat_prod']          = $this->promocode_model->get_cat_prod($data['promo_edit_list'][0]['promo_sub_cat_id'])->result_array();
        $data['main_cat']          = $this->categories_model->getCategories('and cat_type=0');
        $this->form_validation->set_rules('promo_type', 'promo_price_amount', 'required');
        $this->form_validation->set_rules('promo_code', 'promocode code', 'required');
        $this->form_validation->set_rules('promo_status', 'promo_category_id', 'required');
        if ($this->input->post('promo_type') == 1) {
            $this->form_validation->set_rules('promo_category_id', 'promo_category_id', 'required');
            $this->form_validation->set_rules('promo_sub_cat_id', 'promo_sub_cat_id', 'required');
            $this->form_validation->set_rules('promo_product_id', 'promo_product_id', 'required');
        }
        if ($this->input->post('promo_discount_type') == 0) {

            $this->form_validation->set_rules('promo_price_usd', 'price', 'required|numeric|greater_than[0]');
        } else {

            $this->form_validation->set_rules('promo_price_percentage', 'price', 'required|numeric|less_than[100]|greater_than[0]');
        }
        $this->form_validation->set_rules('promo_start_date', 'promo_start_date', 'required');
        $this->form_validation->set_rules('promo_end_date', 'promo_end_date', 'required');
        $this->form_validation->set_message('required', 'This field is required.');
        $this->form_validation->set_message('less_than', 'This field must be less then 100.');
        $this->form_validation->set_message('greater_than', 'This field must be greater then 0.');
        if ($this->form_validation->run() == false) {

            $this->load->view('admin/promocode/edit_promocode', $data);

        } else {
            if ($this->input->post('promo_discount_type') == 0) {
                $price = $this->input->post('promo_price_usd');
            } else {
                $price = $this->input->post('promo_price_percentage');
            }

            $orignal_date  = explode("-", $this->input->post('promo_start_date'));
            $start_date    = $orignal_date[2] . "-" . $orignal_date[0] . "-" . $orignal_date[1];
            $orignal_date1 = explode("-", $this->input->post('promo_end_date'));
            $end_date      = $orignal_date1[2] . "-" . $orignal_date1[0] . "-" . $orignal_date1[1];
            if (strtotime($start_date) > strtotime($end_date)) {

                $this->session->set_flashdata('msg', 'Please select the correct date range.');
                redirect(base_url() . "admin/promocode/edit/" . $promo_id);
                exit;

            }

            $array = array(
                'promo_code'          => $this->input->post('promo_code'),
                'promo_type'          => $this->input->post('promo_type'),
                'promo_category_id'   => $this->input->post('promo_category_id'),
                'promo_sub_cat_id'    => $this->input->post('promo_sub_cat_id'),
                'promo_product_id'    => $this->input->post('promo_product_id'),
                'promo_discount_type' => $this->input->post('promo_discount_type'),
                'promo_price_amount'  => $price,
                'promo_start_date'    => $start_date,
                'promo_end_date'      => $end_date,
                'promo_status'        => $this->input->post('promo_status'),
            );

            $this->promocode_model->promo_update($promo_id, $array);
            $this->session->set_flashdata('msg', 'Item updated successfully.');
            redirect(base_url() . "admin/promocode");
            exit;
        }
    }

    public function randstring()
    {
        $alphabet    = "abcdAEDVGTef259ghijklmn684351opqr54VB25965QWSstuwALKUBN654VCXxyz0123456789";
        $pass        = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 6; $i++) {
            $n      = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        echo strtoupper(implode($pass)); //turn the array into a string
    }

    public function delete($id)
    {

        $this->promocode_model->commonDelete('tbl_promocode', "promo_id", $id);
        $this->session->set_flashdata('msg', 'Item deleted successfully.');
        redirect(base_url() . "admin/promocode");
        exit;
    }

    public function getmiddleCategory()
    {
        $resultsChannel = $this->common_model->common_where('tbl_categories', array("cat_parent_id" => $this->input->post('nId')));
        $result         = count($resultsChannel->result());
        $str            = '';
        $str .= "<div class='mu-flds-wrp'>";
        $str .= "<div class='mu-frmFlds_long'>";
        $str .= "<label>Select Sub Category*</label>";
        $str .= "<select required='required' onchange='get_cat_prod(this.value);' name='promo_sub_cat_id'>";
        $str .= "<option value=''>Select</option>";

        foreach ($resultsChannel->result() as $name_result) {
            $str .= "<option value=" . $name_result->cat_id . ">" . $name_result->cat_name . "</option>";
        }

        $str .= "</select>";
        $str .= "</div>";
        $str .= "<div class='error'> <?php echo form_error('promo_sub_cat_id')?> </div>";
        $str .= "</div>";

        if ($result > 0) {
            echo $str;
        } else {
            unset($str);
        }
    }

    public function get_cat_prod()
    {
        $resultsChannel = $this->promocode_model->get_cat_prod($this->input->post('nId'));
        $result         = count($resultsChannel->result());
        $str            = "";
        $str .= "<div id='promo_prod' class='mu-flds-wrp'>";
        $str .= "<div class='mu-frmFlds_long'>";
        $str .= "<label>Select Product*</label>";
        $str .= "<select required='required' name='promo_product_id'>";
        $str .= "<option value=''>Select</option>";

        foreach ($resultsChannel->result() as $name_result) {
            $str .= "<option value=" . $name_result->prod_id . ">" . $name_result->prod_name . "</option>";
            // echo $name_result->cat_name;
        }

        $str .= "</select>";
        $str .= "</div>";
        $str .= "</div>";

        if ($result > 0) {
            echo $str;
        } else {
            echo $str;
        }
    }

}
