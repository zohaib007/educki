<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Shop extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('front/shop/shop_model');
        $this->load->database();
        //authFront();
        date_default_timezone_set('America/New_York');
    }

    public function _remap($page = 0) {

        $url = $_SERVER['REQUEST_URI'];
        $url = str_replace('/dev/dev/', '/dev/',$url);
        $url = explode('/', $url);
        $Part = array_pop($url);
        $test = explode('?', $Part);
        $lastPart = $test[0];

        $page_data['lastPart'] = $lastPart;
        $page_data['title'] = "Shop";
        $page_data['meta_title'] = "meta_title";
        $page_data['meta_description'] = "meta_description";
        $page_data['meta_keyword'] = "meta_keywords";
        $page_data['per_page'] = 10;
        $page_data['sort_by'] = 1;
        $page_data['segment1'] = $this->uri->segment(1);
        $page_data['segment2'] = $this->uri->segment(2);
        $page_data['segment3'] = $this->uri->segment(3);
        $page_data['segment4'] = $this->uri->segment(4);
        if ($page == 0) {
            $page = 0;
        }
        if (@$this->input->get('sort_by') != '') {
            $page_data['sort_by'] = @$this->input->get('sort_by');
        }
        $url_data['url_level'] = '';
        $homepage_data = '';
        if (strlen($page_data['segment1']) <= 0) {


            //No first url segment
            $homepage_data = $this->home();
            $page_data = $homepage_data;
        } elseif (strlen($page_data['segment1']) > 0) {
            //No first segment present
       

            if ($page_data['segment4'] != '') {
                if (is_numeric($lastPart)) {
                    $page_data['type'] = "cat";
                    $page_data['category_level'] = 3;
                    $result = $this->categoryfilters($page_data, $lastPart);
                    $page_data['category_details'] = $result['category_details'];
                    $page_data['category_filters'] = $result['category_filters'];
                    $page_data['category_detail_filters'] = $result['category_detail_filters'];
                    $page_data['product_filters'] = $result['product_filters'];
                    
                    $page_data['product_detail_filters'] = $result['product_detail_filters'];
                } else {
                    $page_data['type'] = $this->check_url($page_data['segment4']);
                    if ($page_data['type'] == 'prod') {
                        $page_data['prod_level'] = 4;
                        $page_data['prod_details'] = $this->get_product_details($page_data);
                    }
                }
            } elseif ($page_data['segment3'] != '') {

                if (is_numeric($lastPart)) {
                    $page_data['type'] = "cat";
                    $page_data['category_details'] = $this->get_category_details($page_data['segment2'], $page_data['sort_by'], $lastPart);
                } else {
                    $page_data['type'] = $this->check_url($page_data['segment3']);
                    if ($page_data['type'] == 'prod') {
                        $page_data['prod_level'] = 3;

                        $page_data['prod_details'] = $this->get_product_details($page_data);
                    } elseif ($page_data['type'] == "cat") {
                        $page_data['category_level'] = 3;
                 // echo 1; exit;

                        $result = $this->categoryfilters($page_data);
                        $page_data['category_details'] = $result['category_details'];
                        $page_data['category_filters'] = $result['category_filters'];
                        $page_data['category_detail_filters'] = $result['category_detail_filters'];
                        $page_data['product_filters'] = $result['product_filters'];
                        $page_data['product_detail_filters'] = $result['product_detail_filters'];
                       // echo '<pre>';
                       // print_r($result);
                       // die;
                        //    echo '<pre>';
                        //   print_r($this->session->userdata("catagory_details"));
                        //  print_r($productid);
                        //   exit;
                        //   echo '<pre>';
                        //   print_r($this->session->userdata("catagory_details"));
                        //  exit;
                    }
                }
            } elseif ($page_data['segment2'] != '') {


                if (is_numeric($lastPart)) {
                    $page_data['type'] = "cat";
                    $page_data['category_details'] = $this->get_category_details($page_data['segment1'], $page_data['sort_by'], $lastPart);
                } else {
                    $page_data['type'] = $this->check_url($page_data['segment2']);
                    
                    if ($page_data['type'] == 'prod') {
                        $page_data['prod_level'] = 2;
                        $page_data['prod_details'] = $this->get_product_details($page_data);
                        // echo '<pre>';
                        // echo $this->db->last_query();
                        // print_r($page_data['prod_details']);
                        // exit;
                    } elseif ($page_data['type'] == "cat") {
                        $page_data['category_level'] = 2;
                        $page_data['category_details'] = $this->get_category_details($page_data['segment2'], $page_data['sort_by']);
                    }
                }
            } elseif ($page_data['segment1'] != '') {
                $page_data['type'] = $this->check_url($page_data['segment1']);

                if ($page_data['type'] == "cat") {
                    $page_data['category_level'] = 1;
                    $page_data['category_details'] = $this->get_category_details($page_data['segment1'], $page_data['sort_by']);
                }
            }
        }

        // echo '<pre>';
        // print_r($page_data);
        // exit;

        if ($homepage_data != '') {
            $page_data['title'] = $homepage_data['title'];
            $page_data['meta_title'] = $homepage_data['meta_title'];
            $page_data['meta_description'] = $homepage_data['meta_description'];
            $page_data['meta_keyword'] = $homepage_data['meta_keyword'];
            $page_data['content_view'] = "homepage";
        } else if ($page_data['type'] == "cat") {
            $page_data['breadcrumbs_data'] = $this->shop_model->getbreadcrumbs_category_data($page_data);
            $page_data['content_view'] = "shop/shop-listing";
        } else {
            $page_data['meta_title'] = $page_data['prod_details']['product_data']->prod_title;
            $page_data['meta_description'] = $page_data['prod_details']['product_data']->prod_description;
            $page_data['meta_keyword'] = $page_data['prod_details']['product_data']->prod_title;
            $page_data['share_title'] = $page_data['prod_details']['product_data']->prod_title;
            if (count($page_data['prod_details']['product_images']) > 0) {
                $page_data['share_image'] = base_url() . "resources/prod_images/" . $page_data['prod_details']['product_images'][0]['img_name'];
            }
            $page_data['breadcrumbs_data'] = $this->shop_model->getbreadcrumbs_product_data($page_data);
            $page_data['content_view'] = "shop/shop-details";
            $product_count_data['ip_address'] = getHostByName(php_uname('n'));
            $product_count_data['product_id'] = $page_data['prod_details']['product_data']->prod_id;
            $product_count_data['product_store_id'] = $page_data['prod_details']['product_data']->prod_store_id;
            $product_count_data['view_date'] = date('Y-m-d h:i:s');
            $product_count_data['product_user_id'] = $page_data['prod_details']['product_data']->prod_user_id;
            $this->common_model->commonSave('tbl_product_views', $product_count_data);

            $page_data['share_url'] = base_url();
            if ($page_data['breadcrumbs_data']->catURL != '') {
                $page_data['share_url'] .= $page_data['breadcrumbs_data']->catURL;
            }
            if ($page_data['breadcrumbs_data']->subCatURL != '') {
                $page_data['share_url'] .= "/" . $page_data['breadcrumbs_data']->subCatURL;
            }
            if ($page_data['breadcrumbs_data']->subSubCatURL != '') {
                $page_data['share_url'] .= "/" . $page_data['breadcrumbs_data']->subSubCatURL;
            }
            if ($page_data['breadcrumbs_data']->prodURL != '') {
                $page_data['share_url'] .= "/" . $page_data['breadcrumbs_data']->prodURL;
            }
        }
//        echo "<pre>";print_r($_COOKIE);die; 
        //echo "<pre>";print_r($page_data);die;
        $this->load->view("front/custom_template", $page_data);
    }

    public function home() {
        $data['title'] = "eDucki";
        $data['meta_title'] = "eDucki";
        $data['meta_description'] = "eDucki";
        $data['meta_keyword'] = "eDucki";
        $data['home_data'] = $this->common_model->commonselect('tbl_homepage', 'home_id', 1)->row();
        $data['home_banner'] = $this->common_model->getCombox('tbl_homepage_banner', 'banner_id', 'ASC')->result();
        $data['home_member'] = $this->common_model->getCombox('tbl_homepage_member', 'mem_id', 'ASC')->result();
        $query = "
                SELECT *
                FROM tbl_categories
                WHERE cat_level = 1 AND cat_is_delete = 0 AND cat_status = 1
                ";
        $data['home_category'] = $this->db->query($query)->result();
        $query = "
                SELECT *
                FROM tbl_stores store
                INNER JOIN tbl_user user ON store.user_id = user.user_id
                WHERE
                store_is_hide = 0 AND user.user_is_delete = 0 AND user.user_status = 1
                ";
        $data['home_store'] = $this->db->query($query)->result();
        // echo '<pre>';
        // print_r($data['home_store']);
        // exit;
        /*$data['featured_sellers'] = $this->common_model->commonselect2('tbl_user', 'user_type', 'Seller', 'user_is_featured', 1, 'user_featured_date', 'DESC')->result();*/
        $query = "
                SELECT store.*
                FROM tbl_stores store
                INNER JOIN tbl_user user ON store.user_id = user.user_id
                WHERE
                store_is_hide = 0 AND user.user_is_delete = 0 AND user.user_status = 1 AND user.user_is_featured = 1
                ";
        $data['featured_sellers'] = $this->db->query($query)->result();

        return $data;
    }

    public function check_url($url = '') {
        $category_url['url_data'] = $this->common_model->commonselect('tbl_categories', 'cat_url', $url)->row();
        $product_url['url_data'] = $this->common_model->commonselect3('tbl_products', 'prod_url', $url, 'prod_status', 1, 'prod_is_delete', 0)->row();

        if (!empty($category_url['url_data'] ) ) {
            $type = "cat";
            return $type;
        } elseif (!empty($product_url['url_data']) ) {
            $type = "prod";
            return $type;
        } else {
            redirect(base_url());
        }
        /** comment by haseeb
        if (count($category_url['url_data']) == 0) {
            redirect(base_url());
        } elseif (count($product_url['url_data']) == 0)
        */
    }

    public function get_product_details($data = '') {
//        $save_data['sub_sub_cat'] = $this->common_model->commonselect('tbl_categories', 'cat_url', $data['segment3'])->num_rows();
        //
        //        var_dump($save_data);die;
        //        if ($save_data['sub_sub_cat'] > 0) {
        //            $save_data['sub_cat'] = $this->common_model->commonselect('tbl_categories', 'cat_url', $data['segment2'])->num_rows();
        //            if ($save_data['sub_cat'] > 0) {
        //                $save_data['main_cat'] = $this->common_model->commonselect('tbl_categories', 'cat_url', $data['segment1'])->num_rows();
        //                if ($save_data['main_cat'] > 0) {
        //                    $save_data['tree'] = 1;
        //                } else {
        //                    redirect(base_url());
        //                }
        //            } else {
        //                redirect(base_url());
        //            }
        //        } else {
        //            redirect(base_url());
        //        }

        if ($data['prod_level'] == 4) {
            $data = $this->get_level_4_data($data);
        } elseif ($data['prod_level'] == 3) {
            $data = $this->get_level_3_data($data);
        } elseif ($data['prod_level'] == 2) {
            $data = $this->get_level_2_data($data);
        } elseif ($data['prod_level'] == 1) {
            redirect(base_url());
        }
        return $data;
    }

    public function get_category_details($product = '', $sort_val = '', $lastPart = '') {

        $page = 0;

        $url = '/dev/'.uri_string();
        //echo $url;
        //$url = str_replace('/dev/dev/', '/dev/',$url);
        $url = str_replace('/page', 'page',$url);
        // echo '<pre>';
        // echo "REQUEST_URI: ".$_SERVER["REQUEST_URI"]."<br/>";
        // echo "uri_string: ".uri_string()."<br/>";
        // echo "current_url: ".current_url()."<br/>";
        // echo "uri->uri_string: ".$this->uri->uri_string()."<br/>";
        // echo '</pre>';
        $url_without_string = strtok($url, '?');

        $limit_per_page = 10;
        if (@$this->input->get('page') != '') {
            $page_number = @$this->input->get('page');
            $page = $page_number - 1;
        }
        $data['sort_by'] = $sort_val;
        if (@$this->input->get('per_page') != '') {
            $limit_per_page = @$this->input->get('per_page');
        }
        $data['per_page'] = $limit_per_page;
        $data['url_data'] = $this->common_model->commonselect('tbl_categories', 'cat_url', $product)->row();

        $cat_ids = array();
        array_push($cat_ids,$data['url_data']->cat_id);
        $_subCatId = $this->db->query("
                        SELECT cat_id 
                        FROM tbl_categories 
                        WHERE cat_parent_id = ".$data['url_data']->cat_id."
                        AND cat_status = 1 
                        AND cat_is_delete = 0
                    ")->result_array();
        if(count($_subCatId) > 0){
            foreach($_subCatId as $_ids){
                array_push($cat_ids,$_ids['cat_id']);
                $_subCatId2 = $this->db->query("
                            SELECT cat_id 
                            FROM tbl_categories 
                            WHERE cat_parent_id = ".$_ids['cat_id']."
                            AND cat_status = 1 
                            AND cat_is_delete = 0
                        ")->result_array();
                if(count($_subCatId2) > 0){
                    foreach($_subCatId2 as $_ids2){
                        array_push($cat_ids,$_ids2['cat_id']);
                    }
                }
            }
        }
        
        $data['total_rows'] = count($this->shop_model->view_product_listing_by_category($cat_ids, $sort_val));
        $mypaing['per_page'] = $limit_per_page;
        $mypaing['total_rows'] = $data['total_rows'];
        $mypaing['base_url'] = base_url() . str_replace('/dev', '', $_SERVER['REQUEST_URI']) . "";
        $mypaing['base_url'] = str_replace('/dev/dev/', '/dev/',$mypaing['base_url']);

//        $mypaing['uri_segment'] = 4;
        $mypaing['use_page_numbers'] = TRUE;
        $mypaing['next_link'] = '&nbsp;';
        $mypaing['prev_link'] = '&nbsp;';
        $mypaing['prev_tag_open'] = '<li class="page-left" >';
        $mypaing['prev_tag_close'] = '</li>';
        $mypaing['next_tag_open'] = '<li class="page-right">';
        $mypaing['next_tag_close'] = '</li>';
        $mypaing['num_tag_open'] = '<li>';
        $mypaing['num_tag_close'] = '</li>';
        $mypaing['cur_tag_open'] = '<li><a class = "active">';
        $mypaing['cur_tag_close'] = '</a></li>';
        $mypaing['page_query_string'] = TRUE;
        $mypaing['reuse_query_string'] = FALSE;
        $mypaing['query_string_segment'] = 'page';
//        $mypaing['num_links'] = 5;
        $mypaing['first_link'] = '';
        $mypaing['last_link'] = '';
        $mypaing['first_tag_open'] = '<div style="display:none;">';
        $mypaing['first_tag_close'] = '</div>';
        $mypaing['last_tag_open'] = '<div style="display:none;">';
        $mypaing['last_tag_close'] = '</div>';
        $mypaing['first_url'] = base_url() . $url_without_string;
        $this->pagination->initialize($mypaing);
        $data['paginglink'] = $this->pagination->create_links();
        $data['paginglink'] = str_replace('/dev/dev/','/dev/',$data['paginglink']);
        
        $start = 1;
        $end = $limit_per_page;
        $data['to'] = $start;
        $data['from'] = $end;
        if($page > 0){
            for($i=1; $i<=$page; $i++){
                $data['to'] += $limit_per_page;
                $data['from'] += $limit_per_page;
            }
        }


        $data['product_price'] = $this->shop_model->product_price($cat_ids, $sort_val);
        $data['product_lising'] = $this->shop_model->product_listing_by_category($cat_ids, $sort_val, $limit_per_page, $page * $limit_per_page);
        // echo '<pre>';
        // print_r($data['product_lising']);
        // exit;
        return $data;
    }

    public function get_products_by_filters($product = '', $sort_val = '', $singlefilter, $multifilter, $lastPart = '') {

        $page = 0;
        $url = $_SERVER["REQUEST_URI"];
        $url = str_replace('/dev/dev/', '/dev/',$url);
        $url = str_replace('/page', 'page',$url);
        $url_without_string = strtok($url, '?');

        $limit_per_page = 10;
        if (@$this->input->get('page') != '') {
            $page_number = @$this->input->get('page');
            $page = $page_number - 1;
        }
        $data['sort_by'] = $sort_val;
        if (@$this->input->get('per_page') != '') {
            $limit_per_page = @$this->input->get('per_page');
        }
        $data['per_page'] = $limit_per_page;
        $data['url_data'] = $this->common_model->commonselect('tbl_categories', 'cat_url', $product)->row();
        $data['total_rows'] = count($this->shop_model->view_product_listing_by_filters($data['url_data']->cat_id, $sort_val, $singlefilter, $multifilter));
        $mypaing['per_page'] = $limit_per_page;
        $mypaing['total_rows'] = $data['total_rows'];
        //$mypaing['base_url'] = base_url() . $_SERVER['REQUEST_URI'] . "/";
        $mypaing['base_url'] = base_url() . str_replace('/dev', '', $_SERVER['REQUEST_URI']) . "/";
        $mypaing['base_url'] = str_replace('/dev/dev/', '/dev/',$mypaing['base_url']);
//        $mypaing['uri_segment'] = 2;
        $mypaing['use_page_numbers'] = true;
        $mypaing['next_link'] = '&nbsp;';
        $mypaing['prev_link'] = '&nbsp;';
        $mypaing['prev_tag_open'] = '<li class="page-left" >';
        $mypaing['prev_tag_close'] = '</li>';
        $mypaing['next_tag_open'] = '<li class="page-right">';
        $mypaing['next_tag_close'] = '</li>';
        $mypaing['num_tag_open'] = '<li>';
        $mypaing['num_tag_close'] = '</li>';
        $mypaing['cur_tag_open'] = '<li><a class = "active">';
        $mypaing['cur_tag_close'] = '</a></li>';
        $mypaing['page_query_string'] = TRUE;
        $mypaing['reuse_query_string'] = FALSE;
        $mypaing['query_string_segment'] = 'page';
//        $mypaing['num_links'] = 5;
        $mypaing['first_link'] = false;
        $mypaing['last_link'] = false;
        $mypaing['first_tag_open'] = '<div style="display:none;">';
        $mypaing['first_tag_close'] = '</div>';
        $mypaing['last_tag_open'] = '<div style="display:none;">';
        $mypaing['last_tag_close'] = '</div>';
        $mypaing['first_url'] = base_url() . $url_without_string;
        $this->pagination->initialize($mypaing);
        $data['paginglink'] = $this->pagination->create_links();
        $data['paginglink'] = str_replace('/dev/dev/','/dev/',$data['paginglink']);

        
        $start = 1;
        $end = $limit_per_page;
        $data['to'] = $start;
        $data['from'] = $end;
        if($page > 0){
            for($i=1; $i<=$page; $i++){
                $data['to'] += $limit_per_page;
                $data['from'] += $limit_per_page;
            }
        }

        
        $data['product_price'] = $this->shop_model->product_price($data['url_data']->cat_id, $sort_val);
        $data['product_lising'] = $this->shop_model->product_listing_by_filters($data['url_data']->cat_id, $sort_val, $limit_per_page, $page * $limit_per_page, $singlefilter, $multifilter);
        // echo '<pre>';
        // echo $this->db->last_query();
        //  exit;
        return $data;
    }

    public function get_level_4_data($data = '') {
        $data['url_data'] = $this->common_model->commonselect3('tbl_products', 'prod_url', $data['segment4'], 'prod_status',1, 'prod_is_delete',0)->row();
        $save_data['prod_cat'] = $this->common_model->commonselect('tbl_product_category', 'prod_id', $data['url_data']->prod_id)->row();
        $product['product_data'] = $this->shop_model->get_product_data($save_data['prod_cat']);

        if(empty($product['product_data'])){
            redirect(base_url());
            exit;
        }
        $product['product_images'] = $this->shop_model->get_product_images($save_data['prod_cat']->prod_id);
        $product['seller_data'] = $this->shop_model->get_seller_details($save_data['prod_cat']->prod_id);
        $product['other_products'] = $this->shop_model->get_seller_other_products($save_data['prod_cat']->prod_id);

        $filters = $this->shop_model->getProduct1lvlRequiredFilter($save_data['prod_cat']->prod_id);
        
        $product['uniqueFilter'] = $filters[0]['uniqueFilter'];
        
        if ($filters != FALSE) {

            $product['filters_array'] = $filters;
            $product['required_filters'] = $this->common_model->unique_multidimentional_array($filters, 'filter_id');
        }

        $filters = $this->shop_model->getProduct1lvlAddFilter($save_data['prod_cat']->prod_id);
        if ($filters != FALSE) {
            $product['filters_array2'] = $filters;
            $product['required_filters2'] = $this->common_model->unique_multidimentional_array($filters, 'filter_id');
        }

        /*echo "<pre>";
        print_r($product['required_filters']);
        exit;*/
        return $product;
    }

    public function get_level_3_data($data = '') {
# code...
        $data['url_data'] = $this->common_model->commonselect3('tbl_products', 'prod_url', $data['segment3'], 'prod_status',1, 'prod_is_delete',0)->row();
        $product_data = $this->product_lvl_data($data['url_data']->prod_id);
        if(empty($product_data)){
            redirect(base_url());
            exit;
        }
        return $product_data;
    }

    public function get_level_2_data($data = '') {
# code...
        $data['url_data'] = $this->common_model->commonselect3('tbl_products', 'prod_url', $data['segment2'], 'prod_status',1, 'prod_is_delete',0)->row();

        $product_data = $this->product_lvl_data($data['url_data']->prod_id);
        if(empty($product_data)){
            redirect(base_url());
            exit;
        }
        return $product_data;
    }

    function categoryfilters($page_data, $pagination = '') {
        $cat_detail = $this->session->userdata("catagory_details");
        if (@$this->input->get('q') != '' && isset($cat_detail)) {

            //  echo '<pre>';
            $productid = array();
            $filterslist = array();
            $singlefilter = '';
            $multiplefilter = '';

            $singlefilterflag = true;
            // $multiplefilterflag = true;

            $sflag = true;
            $mflag = true;


            $cat_filter = $this->session->userdata("category_filters");
            $cat_detail_filter = $this->session->userdata("category_detail_filters");



            $query = $this->input->get();

            foreach ($query as $key => $item) {


                foreach ($cat_filter as $cat_fil) {
                    if ($cat_fil->filter_slug == $key) {

                        if ($singlefilterflag) {

                            if ($cat_fil->cat_filter_is_conditional == 0) {
                                $items = explode(",", $item);



                                $singlefilter = $singlefilter . ' pf.filter_slug = "' . $key . '"';
                                //   . "' AND pf.filter_value = '" . $item . "'";
                                foreach ($items as $i) {

                                    if ($sflag) {
                                        $singlefilter = $singlefilter . ' AND pf.filter_value = "' . $i . '"';
                                        $sflag = false;
                                    } else {
                                        $singlefilter = $singlefilter . ' AND prod.prod_id IN (SELECT prod_id FROM `tbl_product_filters` `pf` WHERE pf.filter_slug = "' . $key . '" AND pf.filter_value = "' . $i . '" ) ';
                                    }
                                }

                                //  $singlefilter = $singlefilter . " pf.filter_slug = '" . $key . "' AND pf.filter_value = '" . $item . "'";
                            } else {
                                if ($sflag) {
                                    $singlefilter = $singlefilter .  'pf.filter_slug = "' . $key . '" AND pd.filter_slug = "' . $item . '"';
                                    $sflag = false;
                                } else {
                                    $singlefilter = $singlefilter . ' AND prod.prod_id IN (SELECT pf.prod_id FROM tbl_product_filters pf JOIN tbl_product_filters_detail pd ON pd.prod_filter_id = pf.id WHERE pf.filter_slug = "' . $key . '" AND pd.filter_slug ="' . $item . '" )';
                                }
                            }
                            array_push($filterslist, $cat_fil);
                            $singlefilterflag = false;
                        } else {


                            if ($cat_fil->cat_filter_is_conditional == 0) {


                                $items = explode(",", $item);
                                // $singlefilter = $singlefilter . "AND pf.filter_slug = '" . $key ."'";

                                $customquery = 'pf.filter_slug = "' . $key . '"';
                                //   . "' AND pf.filter_value = '" . $item . "'";
                                foreach ($items as $i) {

                                    //  $singlefilter = $singlefilter . " AND pf.filter_value = '" . $i . "'";


                                    if ($sflag) {
                                        $sflag = false;
                                        $singlefilter = $singlefilter . '  prod.prod_id IN (SELECT prod_id FROM `tbl_product_filters` `pf` WHERE ' . $customquery . ' AND pf.filter_value = "' . $i . '" ) ';
                                    } else {
                                        $singlefilter = $singlefilter . ' AND  prod.prod_id IN (SELECT prod_id FROM `tbl_product_filters` `pf` WHERE ' . $customquery . ' AND pf.filter_value = "' . $i . '" ) ';
                                    }
                                }
                            } else {

                                if ($sflag) {
                                    $sflag = false;
                                    // echo `<pre>`;
                                    //   echo $i;
                                    //  exit;
                                    $checkflag = true;
                                    $items = explode(",", $item);
                                    foreach ($items as $i) {
                                        if ($checkflag) {
                                            $checkflag = false;
                                            $singlefilter = $singlefilter . '  prod.prod_id IN (SELECT pf.prod_id FROM tbl_product_filters pf JOIN tbl_product_filters_detail pd ON pd.prod_filter_id = pf.id WHERE pf.filter_slug = "' . $key . '" AND pd.filter_slug ="' . $i . '" ) ';
                                        } else {
                                            $singlefilter = $singlefilter . ' AND prod.prod_id IN (SELECT pf.prod_id FROM tbl_product_filters pf JOIN tbl_product_filters_detail pd ON pd.prod_filter_id = pf.id WHERE pf.filter_slug = "' . $key . '" AND pd.filter_slug ="' . $i . '" ) ';
                                        }
                                    }
                                } else {
                                    $items = explode(",", $item);
                                    foreach ($items as $i) {
                                        $singlefilter = $singlefilter . ' AND prod.prod_id IN (SELECT pf.prod_id FROM tbl_product_filters pf JOIN tbl_product_filters_detail pd ON pd.prod_filter_id = pf.id WHERE pf.filter_slug = "' . $key . '" AND pd.filter_slug ="' . $i . '" ) ';
                                    }
                                }
                                //$singlefilter = $singlefilter . " AND pf.filter_slug = '" . $key . "'";
                            }
                            array_push($filterslist, $cat_fil);
                        }
                        break;
                    }
                }

                foreach ($cat_detail_filter as $cat_fil) {
                    if ($cat_fil->filter_slug == $key) {
                        if ($singlefilterflag) {
                            $multiplefilter = $multiplefilter . ' pd.filter_slug = "' . $key . '"';
                            $items = explode(",", $item);
                            // $singlefilter = $singlefilter . " pf.filter_slug = '" . $key;
                            //   . "' AND pf.filter_value = '" . $item . "'";
                            foreach ($items as $i) {
                                if ($mflag) {
                                    $multiplefilter = $multiplefilter . ' AND pd.filter_value = "' . $i . '"';
                                    $mflag = false;
                                } else {
                                    $multiplefilter = $multiplefilter . ' AND prod.prod_id IN (SELECT prod_id FROM `tbl_product_filters_detail` `pd` WHERE pd.filter_slug = "' . $key . '" AND pd.filter_value = "' . $i . '" ) ';
                                }
                            }

                            array_push($filterslist, $cat_fil);
                            $singlefilterflag = false;
                        } else {
                            //  $multiplefilter = $multiplefilter . "AND pd.filter_slug = '" . $key . "'";
                            $customquery = ' pd.filter_slug = "' . $key . '"';
                            $items = explode(",", $item);

                            foreach ($items as $i) {
                                //  $multiplefilter = $multiplefilter . " pd.filter_value = '" . $item . "'";

                                if ($mflag) {

                                    $multiplefilter = $multiplefilter . ' prod.prod_id IN (SELECT prod_id FROM `tbl_product_filters_detail` `pd` WHERE ' . $customquery . ' AND pd.filter_value = "' . $i . '" ) ';
                                    $mflag = false;
                                } else {
                                    $multiplefilter = $multiplefilter . ' AND prod.prod_id IN (SELECT prod_id FROM `tbl_product_filters_detail` `pd` WHERE ' . $customquery . ' AND pd.filter_value = "' . $i . '" ) ';
                                }
                            }
                            array_push($filterslist, $cat_fil);
                        }
                        break;
                        // Currently Here
                    }
                }
            }

            //     echo '<pre>';
            //    echo $singlefilter;
            //    echo $multiplefilter;
            //    exit;


            $data['category_details'] = $this->get_products_by_filters($page_data['segment3'], $page_data['sort_by'], $singlefilter, $multiplefilter, $pagination);
            // echo $this->db->last_query();
            // exit;
            $data['category_filters'] = $this->session->userdata("category_filters");
            $data['category_detail_filters'] = $this->session->userdata("category_detail_filters");

            $data['product_filters'] = $this->session->userdata("product_filters");
            $data['product_detail_filters'] = $this->session->userdata("product_detail_filters");
        } else {

            $data['category_details'] = $this->get_category_details($page_data['segment3'], $page_data['sort_by'], $pagination);
            // Mehmood
            $data['category_filters'] = $this->common_model->query('SELECT c.* FROM `tbl_cat_filter_title` c JOIN `tbl_categories` tc ON c.filter_cat_id = tc.cat_id WHERE  tc.cat_is_delete = 0 and tc.cat_status = 1 and c.filter_cat_id = ' . $data['category_details']['url_data']->cat_id)->result();
            $data['category_detail_filters'] = $this->common_model->query('SELECT d.* FROM `tbl_cat_filter_detail` d  JOIN `tbl_categories` tc ON d.filter_detail_cat_id = tc.cat_id WHERE tc.cat_is_delete = 0  and tc.cat_status = 1 AND d.filter_detail_cat_id = ' . $data['category_details']['url_data']->cat_id)->result();
            $data['product_filters'] = $this->common_model->query('SELECT d.* FROM `tbl_product_filters` d LEFT JOIN `tbl_categories` tc ON d.category_id = tc.cat_id LEFT JOIN `tbl_products` p ON d.prod_id = p.prod_id LEFT JOIN `tbl_stores` s ON s.store_id = p.prod_store_id LEFT JOIN tbl_user u ON s.user_id = u.user_id WHERE s.store_is_hide = 0 AND tc.cat_is_delete = 0 AND tc.cat_status = 1 AND p.prod_is_delete = 0  AND p.prod_status = 1 AND  u.user_is_delete = 0 AND d.category_id =  ' . $data['category_details']['url_data']->cat_id)->result();
            $data['product_detail_filters'] = $this->common_model->query('SELECT d.* FROM `tbl_product_filters_detail` d LEFT JOIN `tbl_categories` tc ON d.category_id = tc.cat_id LEFT JOIN `tbl_products` p ON d.prod_id = p.prod_id LEFT JOIN `tbl_stores` s ON s.store_id = p.prod_store_id LEFT JOIN tbl_user u ON s.user_id = u.user_id WHERE s.store_is_hide = 0 AND tc.cat_is_delete = 0 AND tc.cat_status = 1  AND p.prod_is_delete = 0 AND p.prod_status = 1 AND u.user_is_delete = 0 AND d.category_id = ' . $data['category_details']['url_data']->cat_id)->result();
        }

        //  echo '<pre>';
        //  print_r($data['category_details']);
        //   exit;
        foreach ($data['category_filters'] as $cat) {

            if ($cat->cat_filter_is_conditional == 0) {

                if (@$this->input->get('q') != '' && isset($cat_detail)) {

                    foreach ($cat->cat_filter_values as $i) {
                        $total = 0;
                        foreach ($data['product_filters'] as $product_filter) {
                            // echo '<pre>';
                            // print_r($product);;
                            //exit;
                            if ($product_filter->filter_id == $cat->filter_title_id) {
                                if ($i->value == $product_filter->filter_value) {
                                    foreach ($data['category_details']['product_lising'] as $product) {
                                        if ($product['prod_id'] == $product_filter->prod_id && $i->value == $product_filter->filter_value) {
                                            $total++;
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        $i->count = $total;
                        //        echo '<pre>';
                    }
                } else {
                    $cat->cat_filter_values = explode(",", $cat->cat_filter_values);

                    $newarray = array();
                    foreach ($cat->cat_filter_values as $i) {
                        $val = new stdClass();
                        $val->value = $i;
                        $total = 0;
                        foreach ($data['product_filters'] as $product_filter) {
                            // echo '<pre>';
                            // print_r($product);;
                            //exit;
                            if ($product_filter->filter_id == $cat->filter_title_id) {
                                if ($i == $product_filter->filter_value) {
                                    $total++;
                                }
                            }
                        }
                        $val->count = $total;
                        //        echo '<pre>';
                        $newarray[] = $val;
                    }
                    $cat->cat_filter_values = $newarray;
                }
            } else {
                $filter_details = array();

                foreach ($data['category_detail_filters'] as $fil) {

                    if ($cat->filter_title_id == $fil->filter_detail_title_id) {

                        if (@$this->input->get('q') != '' && isset($cat_detail)) {

                            $sum = 0;
                            foreach ($data['category_details']['product_lising'] as $product) {
                                foreach ($data['product_detail_filters'] as $product_filter) {

                                    if ($product_filter->filter_id == $fil->filter_detail_title_id && $product_filter->filter_slug == $fil->filter_slug) {


                                        if ($product['prod_id'] == $product_filter->prod_id) {

                                            $sum++;
                                            break;
                                        }
                                    }
                                }
                            }
                            $fil->size = $sum;



                            // New Code
                            //  $newarray = array();
                            foreach ($fil->filter_detail as $i) {
                                $total = 0;
                                // $val = new stdClass();
                                //$val->value = $i;

                                foreach ($data['product_detail_filters'] as $product_detail_filter) {

                                    if ($product_detail_filter->filter_detail_id == $fil->filter_detail_id) {
                                        if ($i->value == $product_detail_filter->filter_value) {
                                            foreach ($data['category_details']['product_lising'] as $product) {
                                                if ($product['prod_id'] == $product_detail_filter->prod_id && $i->value == $product_detail_filter->filter_value) {
                                                    $total++;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }

                                $i->count = $total;
                                /*  if($sum < $total) {
                                  $sum =  $total;
                                  } */

                                //  $newarray[] = $val;
                            }

                            // $fil->filter_detail = $newarray;

                            array_push($filter_details, $fil);

                            // $fil->size = $sum;
                        } else {
                            $fil->filter_detail = explode(",", $fil->filter_detail);

                            $newarray = array();

                            // new code

                            $sum = 0;
                            foreach ($data['category_details']['product_lising'] as $product) {
                                foreach ($data['product_detail_filters'] as $product_filter) {

                                    if ($product_filter->filter_id == $fil->filter_detail_title_id && $product_filter->filter_slug == $fil->filter_slug) {


                                        if ($product['prod_id'] == $product_filter->prod_id) {

                                            $sum++;
                                            break;
                                        }
                                    }
                                }
                            }
                            $fil->size = $sum;

                            // new code

                            foreach ($fil->filter_detail as $i) {
                                $val = new stdClass();

                                $val->value = $i;

                                $total = 0;
                                foreach ($data['product_detail_filters'] as $product_detail_filter) {

                                    if ($product_detail_filter->filter_detail_id == $fil->filter_detail_id) {
                                        if ($i == $product_detail_filter->filter_value) {
                                            $total++;
                                        }
                                    }
                                }

                                $val->count = $total;
                                /* if($sum < $total) {
                                  $sum =  $total;
                                  } */
                                //    echo '<pre>';

                                $newarray[] = $val;
                            }

                            $fil->filter_detail = $newarray;

                            array_push($filter_details, $fil);

                            //$fil->size = $sum;
                        }
                    }
                }

                $cat->cat_filter_values = $filter_details;
            }
        }

        //  echo '<pre>';
        //  echo print_r($data['category_filters']);
        //  exit;

        if (@$this->input->get('q') != '') {
            
        } else {
            $this->session->set_userdata("catagory_details", $data['category_details']);
            $this->session->set_userdata("category_filters", $data['category_filters']);
            $this->session->set_userdata("category_detail_filters", $data['category_detail_filters']);
            $this->session->set_userdata("product_filters", $data['product_filters']);
            $this->session->set_userdata("product_detail_filters", $data['product_detail_filters']);
        }
        return $data;
    }

    function product_lvl_data($prod_id) {
        $save_data['prod_cat'] = $this->common_model->commonselect('tbl_product_category', 'prod_id', $prod_id)->row();
        
        $product['product_data'] = $this->shop_model->get_product_data_lvl3($save_data['prod_cat']);

       

        if(empty($product['product_data'])){
            redirect(base_url());
            exit;
        }
        $product['product_images'] = $this->shop_model->get_product_images($save_data['prod_cat']->prod_id);
        $product['seller_data'] = $this->shop_model->get_seller_details($save_data['prod_cat']->prod_id);
        $product['other_products'] = $this->shop_model->get_seller_other_products($save_data['prod_cat']->prod_id);
        $filters = $this->shop_model->getProduct1lvlRequiredFilter($save_data['prod_cat']->prod_id);
        if ($filters != FALSE) {
            if ($filters != FALSE) {
            $product['filters_array'] = $filters;
            $product['required_filters'] = $this->common_model->unique_multidimentional_array($filters, 'filter_id');
        }
        }
        return $product;
    }

}
