<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                        <?php if($is_cat==1){?>
                        <li class="breadcrumb-item"><a href="<?= base_url()?>blog">News & Advice</a></li>
                        <li class="breadcrumb-item"><?=$blog_cat->blog_cat_name?></li>
                        <?php }else{?>
                        <li class="breadcrumb-item">News & Advice</li>
                        <?php }?>
                    </ol>
                </div>	
            </div>
        </div>
    </div>
</div>  <!--Breadcrumbs ends  here-->
<!--Blog main starts  here-->
<section class="blog-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="col-md-2 col-lg-2 col-sm-3 col-xs-12 pad-lft-0">
                    <div class="blog-aside">
                        <h3>News & Trends</h3>
                        <ul>
                            <?php if (isset($blog_category) && $blog_category != '') { ?>
                                <?php foreach ($blog_category as $category) { ?>
                                    <li><a class="<?php if($blog_cat->blog_cat_name==$category->blog_cat_name){echo "active";}?>" href="<?= base_url() ?>blog/<?=$category->blog_cat_url?>"><?=$category->blog_cat_name?></a></li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                        <div class="discus-sec">
                            <a href="<?=base_url()?>news-advice">Discussions &amp; Forum </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 col-lg-10 col-sm-9 col-xs-12">
                    <div class="blog-main">
                        <?php if (isset($results)) { ?>
                            <?php foreach ($results as $page) { ?>
                                <div class = "blog-list-bx">
                                    <div class = "blog-list-inner"> <!--  pad-tp-0 -->
                                        <div class = "blog-lst-wrp">
                                            <div class = "blog-list-tit">
                                                <h2><?= $page->blog_name ?></h2>
                                            </div>
                                            <div class = "blog-list-date">
                                                <p>By <span><?= $page->blog_author ?></span> on <?= date('F d, Y', strtotime($page->blog_created_date)) ?></p>
                                            </div>
                                        </div>
                                        <div class="blog-lst-img">
                                            <img src="<?= base_url() ?>resources/blog_image/<?= $page->blog_image ?>" alt="" title="" class="img-responsive"/> 
                                        </div>
                                        <div class = "blog-lst-wrp2">
                                            <div class = "blog-list-txt">
                                                <p><?php echo limited_text(clean_string($page->blog_description), 170); ?></p>
                                            </div>
                                            <div class = "blog-lst-btn">
                                                <a href = "<?= base_url() ?>blog-details/<?= $page->blog_url ?>">Read<span>&gt;</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="ualert-row">
                                <div id="message" class="alert alert-success message " style="text-align: center;">
                                    <a href="javascript:;" class="close" data-dismiss="alert" aria-label="close" title="close"></a>
                                    No Content Avaliable
                                </div>
                            </div>
                        <?php } ?>
                        <div class="pagnation-start">
                            <ul>
                                <!-- Show pagination links -->
                                <?php
                                if (isset($links)) {
                                    echo $links;
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Blog main ends  here-->
<script>
    $(function () {
    $('a').filter('a[rel^="next"]').addClass('page-right');
    $('a').filter('a[rel^="prev"]').addClass('page-left');
     });
    </script>