<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Thank you</title>

<meta name="Description" content="educki" />
<meta name="Keywords" content="educki" />
<meta name="author" content="Dotlogics" />

<!--header starts here-->
<?php $this->load->view('front/includes/js')?>
<!--header ends here-->

<!-- Thankyou CSS Starts -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front_resources/css/thankyou.css" />
<!-- Thankyou CSS Ends -->



</head>

<body>

<!-- Thankyou Structure Starts -->
<div class="thankyou-bg">
	<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td>
				<span class="topicon"></span> <!--Top Tick Icon -->
				<h3>Thank You for Purchasing</h3>
				<p>
					Your order number 123456 was completed successfully.
				</p>
				<div class="socialMedia">
					<a href="https://www.facebook.com/eDucki/" target="_blank" class="sociallk ufb" title="Facebook"></a>
					<a href="https://twitter.com/eDuckiCommunity" target="_blank" class="sociallk utwt" title="Twitter"></a>
					<a href="#" target="_blank" class="sociallk ugplus" title="Google Plus"></a>
					<a href="https://www.pinterest.com/educkiCommunity/" target="_blank" class="sociallk upintrest" title="Pintrest"></a>
					<a href="https://www.linkedin.com/company/16259215/" target="_blank" class="sociallk ulkin" title="LinkedIn"></a>
				</div>
				<a href="#" class="backhome">Back To Home Page</a>
			</td>
		</tr>
	</table>
</div>
<!-- Thankyou Structure Ends -->

</body>


</html>