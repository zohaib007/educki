<!--Breadcrumbs starts here-->
<?php
$url = '';
if ($results->order_prod_tracking_number == '' && $order_pickup == 0 && $order_history == 0) {
    $url = base_url()."manage-order";
}else if($results->order_prod_completion_date == NULL && $order_pickup == 0 && $order_history == 0) {
    $url = base_url()."order-waiting";
}else if($order_history == 1 && $order_pickup == 0) {
    $url = base_url()."order-history";
}else{
    $url = base_url()."order-pickup";
}
?>
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url()?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?=base_url()?>my-dashboard">My Account</a></li>
                        <li class="breadcrumb-item"><a href="<?=$url?>">Manage Orders</a></li>
                        <li class="breadcrumb-item active">Order Detail</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view("front/includes/seller-left-nav") ?>
                <!--Left section starts here-->
                <!-- Right Pannel Starts Here -->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">
                        <!-- Inner Section Starts -->
                        <div class="message-wrapper">
                            <!-- Heading Section Starts -->
                            <div class="my-account-right-head addbtmPad" style="display:none;">
                                <div class="msg-hd-inrow">
                                    <h5>Order Detail</h5>
                                </div>
                                <!-- <p>Sample Text Here.</p> -->
                            </div>
                            <!-- Heading Section Ends -->
                            <!-- Bottom Section Starts -->
                            <div class="store-btmWrap">
                                <!--<form name="track-frm" method="post">-->
                                <!-- Store Section Starts -->
                                <div class="store-mainsec">
                                    <!-- Top Grey Bar Starts -->
                                    <div class="topGrey-bar">
                                        <div class="topGrey-inner">
                                            <!-- Search By Form Starts -->
                                            <div class="store-tophdr">
                                                <!-- Left Heading Starts -->
                                                <div class="store-th">
                                                    <h3>Order Detail</h3>
                                                </div>
                                                <!-- Left Heading Ends -->
                                            </div>
                                            <!-- Search By Form Ends -->
                                        </div>
                                    </div>
                                    <!-- Top Grey Bar Ends -->

                                    <!-- Bottom Grey Section Starts -->
                                    <div class="btm-boxwrap">
                                        <div class="btm-boxinner">
                                            <!-- Page Section Starts -->
                                            <div class="btm-odrdtl-wrap">
                                                <?php if ($results->order_prod_tracking_number == '' && $order_pickup == 0 && $order_history == 0) { ?>
                                                    <!-- Border Row Starts -->
                                                    <div class="btm-odbdr">
                                                        <div id="message" class="alert message hide"></div>
                                                        <!-- Track Section Starts -->

                                                        <?php echo form_open('', array('name' => 'update_tracking', 'id' => 'update_tracking_form')); ?>

                                                        <div class="btm-trackwrp">
                                                            <!-- Row Starts -->
                                                            <div class="btm-trackrow">
                                                                <label>Track Your Package</label>
                                                                <div class="btm-tsubrow2">
                                                                    <!-- Select Starts -->
                                                                    <!-- <div class="stextbox-rp">
                                                                        <input type="text" name="carrierName" id="carrierName" placeholder="Enter Carrier" value="" />
                                                                        <div class="error" style="display:none;">
                                                                            <p>This field is required.</p>
                                                                        </div>
                                                                    </div> -->
                                                                    <!-- Select Ends -->
                                                                    <!-- Textbox Starts -->
                                                                    <div class="stextbox-rp">
                                                                        <input type="text" name="tracknumber" id="tracknumber" placeholder="Enter Tracking Number" value="" />
                                                                        <div class="error" id="tracknumber_validate"></div>
                                                                    </div>
                                                                    <!-- Textbox Ends -->
                                                                </div>
                                                            </div>
                                                            <!-- Row Ends -->
                                                            <input type="hidden" id="order_id" name="order_id" value="<?= $results->order_id ?>">
                                                            <!-- Row Starts -->
                                                            <div class="btm-trackrow">
                                                                <div class="uptrack-btn">
                                                                    <div class="custombtn-large">
                                                                        <input type="submit" value="Update Tracking" id="update_tracking" title="Update Tracking" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Row Ends -->
                                                        </div>
                                                        <!-- Track Section Ends -->
                                                        <?php echo form_close(); ?>
                                                    </div>
                                                    <!-- Border Row Ends -->
                                                <?php } ?>
                                                <!-- Border Row Starts -->
                                                <div class="btm-odbdr">
                                                    <div class="order-viewdtl">
                                                        <!-- Track Section Starts -->
                                                        <div class="banything-wrapper">
                                                            <!-- Row Starts -->
                                                            <div class="banything-row">
                                                                <div class="banything-lbl">Order Date:</div>
                                                                <div class="banything-dtl"><?= date('F d, Y', strtotime($results->order_date)) ?></div>
                                                            </div>
                                                            <!-- Row Starts -->
                                                            <!-- Row Starts -->
                                                            <div class="banything-row">
                                                                <div class="banything-lbl">Order ID:</div>
                                                                <div class="banything-dtl"><?= $results->order_id ?></div>
                                                            </div>
                                                            <!-- Row Starts -->
                                                            <!-- Row Starts -->
                                                            <div class="banything-row">
                                                                <div class="banything-lbl">Order Total:</div>
                                                                <div class="banything-dtl">$<?= number_format($results->order_grand_total, 2, '.', '') ?></div>
                                                            </div>
                                                            <?php $status = '';
                                                                if($results->order_status == 'cancel'){
                                                                    $status = 'Cancel';
                                                                }elseif($results->order_status == 'Awaiting Tracking'){
                                                                    $status = 'Awaiting Tracking';
                                                                }elseif($results->order_status == 'Awaiting Pickup'){
                                                                    $status = 'Awaiting Pickup';
                                                                }elseif($results->order_status == 'completed'){
                                                                    $status = 'Completed';
                                                                }elseif($results->order_status == 'In Process'){
                                                                    $status = 'In Process';
                                                                }
                                                                
                                                                ?>
                                                            <div class="banything-row">
                                                                <div class="banything-lbl">Order Status:</div>
                                                                <div class="banything-dtl"><?=$status?></div>
                                                            </div>
                                                            <!-- Row Starts -->
                                                            <?php  if ($order_pickup == 0 && $results->order_prod_tracking_number != ''): ?>
                                                            <!-- Row Starts -->
                                                            <div class="banything-row">
                                                                <div class="banything-lbl">
                                                                    <span class="tracknum">Tracking #</span>
                                                                </div>
                                                                <div class="banything-dtl">
                                                                    <span class="tracknum" id="tracknum"><?=$results->order_prod_tracking_number==''?'N/A':$results->order_prod_tracking_number?></span>
                                                                </div>
                                                            </div>
                                                            <?php endif; ?>
                                                            <!-- Row Starts -->
                                                        </div>
                                                        <!-- Track Section Ends -->
                                                    </div>
                                                </div>
                                                <!-- Border Row Ends -->

                                                <!-- Border Row Starts -->
                                                <div class="btm-odbdr">
                                                    <div class="shipbil-view">
                                                        <!-- Main Row Starts -->
                                                        <div class="shipbil-vrow">
                                                            <!-- Col Starts -->
                                                            <div class="shipbil-lcol">
                                                                <!-- Track Section Starts -->
                                                                <div class="inany-wrapper">
                                                                    <!-- Row Starts -->
                                                                    <div class="inany-row">
                                                                        <h3>Shipping Address</h3>
                                                                    </div>
                                                                    <!-- <div class="inany-row">
                                                                        <div class="inany-lbl">Nickname:</div>
                                                                        <div class="inany-dtl"><?= $default_shipping_address->shipping_nick_name ?></div>
                                                                    </div> -->
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Name:</div>
                                                                        <div class="inany-dtl"><?= $default_shipping_address->shipping_first_name . ' ' . $default_shipping_address->shipping_last_name ?></div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Telephone:</div>
                                                                        <div class="inany-dtl"><?= $default_shipping_address->shipping_phone ?></div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Street:</div>
                                                                        <div class="inany-dtl"><?= $default_shipping_address->shipping_street ?></div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">City:</div>
                                                                        <div class="inany-dtl"><?= $default_shipping_address->shipping_city ?></div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">State:</div>
                                                                        <div class="inany-dtl"><?php
                                                                            if ($default_shipping_address->shipping_country == 'US') {
                                                                                echo get_state_name($default_shipping_address->shipping_state);
                                                                            } else {
                                                                                echo $default_shipping_address->shipping_state_other;
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Country:</div>
                                                                        <div class="inany-dtl"><?= get_country_name($default_shipping_address->shipping_country) ?></div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Zip:</div>
                                                                        <div class="inany-dtl"><?= $default_shipping_address->shipping_zip ?></div>
                                                                    </div>
                                                                    <!-- Row Starts -->
                                                                </div>
                                                                <!-- Track Section Ends -->
                                                            </div>
                                                            <!-- Col Ends -->
                                                            <!-- Col Starts -->
                                                            <div class="shipbil-lcol">
                                                                <!-- Track Section Starts -->
                                                                <div class="inany-wrapper">
                                                                    <!-- Row Starts -->
                                                                    <div class="inany-row">
                                                                        <h3>Billing Address</h3>
                                                                    </div>
                                                                    <!-- <div class="inany-row">
                                                                        <div class="inany-lbl">Nickname:</div>
                                                                        <div class="inany-dtl"><?= $default_billing_address->billing_nick_name?></div>
                                                                    </div> -->
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Name:</div>
                                                                        <div class="inany-dtl"><?= $default_billing_address->billing_first_name . ' ' . $default_billing_address->billing_last_name ?></div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Telephone:</div>
                                                                        <div class="inany-dtl"><?= (!empty($default_billing_address->billing_phone))?$default_billing_address->billing_phone:''?></div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Street:</div>
                                                                        <div class="inany-dtl"><?= $default_billing_address->billing_street ?></div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">City:</div>
                                                                        <div class="inany-dtl"><?= $default_billing_address->billing_city ?></div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">State:</div>
                                                                        <div class="inany-dtl"><?php
                                                                            if ($default_billing_address->billing_country == 'US') {
                                                                                echo get_state_name($default_billing_address->billing_state);
                                                                            } else {
                                                                                echo $default_billing_address->billing_state_other;
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Country:</div>
                                                                        <div class="inany-dtl"><?= get_country_name($default_billing_address->billing_country) ?></div>
                                                                    </div>
                                                                    <div class="inany-row">
                                                                        <div class="inany-lbl">Zip:</div>
                                                                        <div class="inany-dtl"><?= $default_billing_address->billing_zip ?></div>
                                                                    </div>
                                                                    <!-- Row Starts -->
                                                                </div>
                                                                <!-- Track Section Ends -->
                                                            </div>
                                                            <!-- Col Ends -->
                                                        </div>
                                                        <!-- Main Row Ends -->
                                                        <!-- Main Row Starts -->
                                                        <div class="shipbil-vrow last">
                                                            <!-- Sub Section Starts -->
                                                            <div class="inany-subsec">
                                                                <!-- Col Starts -->
                                                                <div class="shipbil-lcol">
                                                                    <!-- Track Section Starts -->
                                                                    <div class="inany-wrapper">
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <h3>Payment Method</h3>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <?php if ($results->order_payment_method == 2) { ?>
                                                                                Paypal
                                                                            <?php } else { ?>
                                                                                Credit Card
                                                                            <?php } ?>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                    </div>
                                                                    <!-- Track Section Ends -->
                                                                </div>
                                                                <!-- Col Ends -->
                                                                <!-- Col Starts -->
                                                                <div class="shipbil-rcol">
                                                                    <!-- Track Section Starts -->
                                                                    <div class="inany-wrapper odrsummary">
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <h3>Order Summary</h3>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">Order Sub Total:</div>
                                                                            <div class="inany-dtl">$<?= number_format($results->order_sub_total, 2, '.', '') ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->

                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">Order Discount:</div>
                                                                            <div class="inany-dtl">$<?= number_format($results->order_discount, 2, '.', '') ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">Shipping &amp; Handing:</div>
                                                                            <div class="inany-dtl">$<?= number_format($results->order_shipping_price, 2, '.', '') ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                    </div>
                                                                    <!-- Track Section Ends -->
                                                                </div>
                                                                <!-- Col Ends -->
                                                            </div>
                                                            <!-- Sub Section Ends -->

                                                            <!-- Sub Section Starts -->
                                                            <div class="inany-subsec">
                                                                <!-- Col Starts -->
                                                                <div class="shipbil-lcol">
                                                                    <!-- Track Section Starts -->
                                                                    <?php if ($results->order_prod_completion_date != NULL) { ?>
                                                                    <div class="inany-wrapper">
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <h3>Payment Status</h3>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <?php if(!empty($results->order_prod_completion_date)){?>
                                                                        <div class="inany-row">
                                                                                Acknowledged On <?=date("F j, Y", strtotime($results->order_prod_completion_date)); ?>
                                                                        </div>
                                                                        <?php }?>
                                                                        <!-- Row Starts -->
                                                                    </div>
                                                                    <?php } ?>
                                                                    <!-- Track Section Ends -->
                                                                </div>
                                                                <!-- Col Ends -->
                                                                <!-- Col Starts -->
                                                                <div class="shipbil-rcol">
                                                                    <!-- Track Section Starts -->
                                                                    <div class="inany-wrapper odrsummary">
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl odrtotal">Order Total</div>
                                                                            <div class="inany-dtl odrtotal">$<?= number_format($results->order_grand_total, 2, '.', '') ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                    </div>
                                                                    <!-- Track Section Ends -->
                                                                </div>
                                                                <!-- Col Ends -->
                                                            </div>
                                                            <!-- Sub Section Ends -->
                                                        </div>
                                                        <!-- Main Row Ends -->
                                                    </div>
                                                </div>
                                                <!-- Border Row Ends -->
                                                <!-- Border Row Starts -->
                                                <div class="btm-odbdr myfan-suny">
                                                    <div class="odrList-wrap">
                                                        <!-- Bottom My Fan Section Starts -->
                                                        <div class="myfan-listwrap">
                                                            <?php
                                                            if (isset($order_detail_results)) {
                                                                foreach ($order_detail_results as $data) {                                                                    
                                                                    ?>
                                                                    <!-- List Row Starts -->
                                                                    <div class="myfan-listrow">                                                                        <!-- Image Section Starts -->
                                                                        <div class="myfan-img">
                                                                            <table cellpadding="0" cellspacing="0" border="0">
                                                                                <tr>
                                                                                    <td>
                                                                                        <a href="<?= productDetailUrl($data['order_prod_id']) ?>/<?= $data['order_prod_url'] ?>"><img src="<?= base_url() ?>resources/prod_images/thumb/<?= $data['order_prod_image'] ?>" alt="" /></a>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>  <!-- Image Section Ends -->
                                                                        <!-- Right Section Starts -->
                                                                        <div class="myfan-right">
                                                                            <div class="myfan-rinner">
                                                                                <!-- Order Content Section Starts -->
                                                                                <div class="myfan-rtoptxt sec01">
                                                                                    <p><a href="<?= productDetailUrl($data['order_prod_id']) ?>/<?= $data['order_prod_url'] ?>">
                                                                                        <?= $data['order_prod_name'] ?>
                                                                                    </a>
                                                                                    </p>
                                                                                    <p>
                                                                                        <?php $filters = json_decode($data['order_prod_filters']);
                                                                                        
                                                                                        if(count($filters) > 0)
                                                                                            foreach ($filters as $i => $filter) {?>
                                                                                            <p><b><?=$filter->filterName?></b>:<?=$filter->filtervalue?></p>
                                                                                        <?php }?>
                                                                                    </p>
                                                                                    <p>
                                                                                        <?php
                                                                                        if ($data['order_prod_return'] != '') {
                                                                                            echo $data['order_prod_return'];
                                                                                        }
                                                                                        ?>
                                                                                        <br/>
                                                                                        Qty: <?= $data['order_prod_qty'] ?>
                                                                                    </p>                                                                                    
                                                                                </div>                                                                                
                                                                                <!-- Order Content Section Ends -->
                                                                                <!-- Acknowledge Content Section Starts -->
                                                                                <?php if ($data['order_prod_completion_date'] != NULL) { ?>
                                                                                <div class="myfan-rtoptxt ack-txt sec02">
                                                                                    <p>
                                                                                        Acknowledged<br />
                                                                                        On<br />
                                                                                        <?php echo date("F j, Y", strtotime($data['order_prod_completion_date'])); ?>
                                                                                    </p>
                                                                                </div>
                                                                                <?php } ?>
                                                                                <!-- Acknowledge Content Section Ends -->
                                                                                <!-- Ship Method Section Starts -->
                                                                                <div class="shipbil-rcol sec03">
                                                                                    <!-- Track Section Starts -->
                                                                                    <div class="inany-wrapper">    
                                                                                        <!-- Row Starts -->
                                                                                        <div class="inany-row">
                                                                                            <h3>Shipping Method</h3>
                                                                                            <?php if(empty($data['order_prod_tracking_number']) && $data['order_prod_status'] != 'completed'){?>
                                                                                            <div class="inany-lk">
                                                                                                <a href="#" class="editMethod">Edit</a>
                                                                                            </div>
                                                                                            <?php }?>
                                                                                            
                                                                                            <!-- Drop Down Starts -->
                                                                                            <div class="sm-drop" style="display:none;">
                                                                                                <form id="form_shipping_method_<?=$data['order_products_id']; ?>" method="post">
                                                                                                <!-- Drop Inner Wrapper Starts -->
                                                                                                <div class="shipd-wrap">
                                                                                                    
                                                                                                    <a href="#" class="shipd-close"></a>
                                                                                                    
                                                                                        <!-- Row Starts -->
                                                                                                    <div class="shipd-row">
                                                                                                        <label>Available Shipping Type:</label>
                                                                                                        <!-- Select Starts -->
                                                                                                        <div class="sselect-rp">
                                                                                                            <select name="ucareer" id="ucareer_<?=$data['order_products_id']; ?>" onchange="toggleShipping(<?=$data['order_products_id']; ?>, this.value)">
                                                                                                                <option value="">Select</option>
                                                                                                                <option value="Charge for Shipping"  <?php echo ($data['order_prod_shipping'] == "Charge for Shipping")?"selected":""; ?>>Charge for Shipping</option>
                                                                                                                <option value="Offer free Shipping" <?php echo ($data['order_prod_shipping'] == "Offer free Shipping")?"selected":""; ?>>Offer free Shipping</option>
                                                                                                                <option value="Local Pick Up" <?php echo ($data['order_prod_shipping'] == "Local Pick Up")?"selected":""; ?>>Local Pick Up</option>
                                                                                                            </select>
                                                                                                            <div class="error" style="display:none;">
                                                                                                                <p>This field is required.</p>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- Select Ends -->
                                                                                                    </div>
                                                                                                    <!-- Row Ends -->
                                                                                                    
                                                                                                    <!-- Bottom Section Starts -->
                                                                                                    <div class="shipdbtm-rp" <?php echo($data['order_prod_shipping'] != "Charge for Shipping")?"style='display:none'":"" ?>>
                                                                                                        <label>Shipping Method</label>
                                                                                                        
                                                                                                            <div class="sdb-inner">
                                                                                                            
                                                                                        <!-- Row Starts -->
                                                                                                                <div class="sdb-row">
                                                                                                                    <div class="sdb-lbl">
                                                                                                                        <div class="sdb-radio">
                                                                                                                            <div class="ao-radio">
                                                                                                                                <input type="radio" id="radio01_<?=$data['order_products_id']; ?>" name="shipmethod_<?=$data['order_products_id']; ?>" value="USPS" <?php echo ($data['order_prod_shipping_methods']=="USPS")?"checked='true'":""; ?> />
                                                                                                                                <label for="radio01_<?=$data['order_products_id']; ?>"></label>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="sdb-rtxt">
                                                                                                                            <label for="radio01_<?=$data['order_products_id']; ?>">USPS</label>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <!--<div class="sdb-val">$ 33.00</div>-->
                                                                                                                </div>
                                                                                                            
                                                                                                                <!-- Row Starts -->
                                                                                                                <div class="sdb-row">
                                                                                                                    <div class="sdb-lbl">
                                                                                                                        <div class="sdb-radio">
                                                                                                                            <div class="ao-radio">
                                                                                                                                <input type="radio" id="radio02_<?=$data['order_products_id']; ?>" name="shipmethod_<?=$data['order_products_id']; ?>" value="FedEx"  <?php echo ($data['order_prod_shipping_methods']=="FedEx")?"checked='true'":""; ?>/>
                                                                                                                                <label for="radio02_<?=$data['order_products_id']; ?>"></label>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="sdb-rtxt">
                                                                                                                            <label for="radio02_<?=$data['order_products_id']; ?>">FedEx</label>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <!--<div class="sdb-val">$ 33.00</div>-->
                                                                                                                </div>
                                                                                                            
                                                                                                                <!-- Row Starts -->
                                                                                                                <div class="sdb-row">
                                                                                                                    <div class="sdb-lbl">
                                                                                                                        <div class="sdb-radio">
                                                                                                                            <div class="ao-radio">
                                                                                                                                <input type="radio" id="radio03_<?=$data['order_products_id']; ?>" name="shipmethod_<?=$data['order_products_id']; ?>" value="DHL"  <?php echo ($data['order_prod_shipping_methods']=="DHL")?"checked='true'":""; ?>/>
                                                                                                                                <label for="radio03_<?=$data['order_products_id']; ?>"></label>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="sdb-rtxt">
                                                                                                                            <label for="radio03_<?=$data['order_products_id']; ?>">DHL</label>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <!--<div class="sdb-val">$ 33.00</div>-->
                                                                                                                </div>
                                                                                                            
                                                                                                                <!-- Row Starts -->
                                                                                                                <div class="sdb-row">
                                                                                                                    <div class="sdb-lbl">
                                                                                                                        <div class="sdb-radio">
                                                                                                                            <div class="ao-radio">
                                                                                                                                <input type="radio" id="radio04_<?=$data['order_products_id']; ?>" name="shipmethod_<?=$data['order_products_id']; ?>" value="UPS"  <?php echo ($data['order_prod_shipping_methods']=="UPS")?"checked='true'":""; ?>/>
                                                                                                                                <label for="radio04_<?=$data['order_products_id']; ?>"></label>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="sdb-rtxt">
                                                                                                                            <label for="radio04_<?=$data['order_products_id']; ?>">UPS</label>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <!--<div class="sdb-val">$ 33.00</div>-->
                                                                                                                </div>
                                                                                                            
                                                                                                            </div>                                                                                                        
                                                                                                    </div>
                                                                                                    <!-- Bottom Section Ends -->
                                                                                                    <button type="button" onclick="saveShippingMethod(<?=$data['order_products_id']; ?>)">Save</button>
                                                                                                </div>
                                                                                                <!-- Drop Inner Wrapper Ends -->
                                                                                                </form>
                                                                                            </div>
                                                                                            <!-- Drop Down Ends -->

                                                                                        </div>
                                                                                        <!-- Row Starts -->
                                                                                        <!-- Row Starts -->
                                                                                        <div class="inany-row">
                                                                                            <?php $shipping='';
                                                                                            if($data['order_prod_shipping']=='Charge for Shipping'){
                                                                                                $shipping = $data['order_prod_shipping_methods'];
                                                                                            }else{
                                                                                                $shipping = '';
                                                                                            }
                                                                                            echo (!empty($shipping))?$shipping:''?>
                                                                                        </div>
                                                                                        <div class="inany-row">
                                                                                            <?php $days='';
                                                                                        if($data['order_prod_shipping']=='Charge for Shipping'){
                                                                                            $days = $data['order_prod_ship_days']+$dayz;
                                                                                        }elseif($data['order_prod_shipping']=='Offer free Shipping'){
                                                                                            $days = $data['order_prod_free_ship_days']+$dayz;
                                                                                        }else{
                                                                                            $days = +$dayz;
                                                                                        }
                                                                                        /*echo "<pre>";
                                                                                        print_r($data);
                                                                                        exit;*/                                                                                        
                                                                                        echo $data['order_prod_shipping']?> <?php if($data['order_prod_status']!='Delivered' && $data['order_prod_status']!='In Process' && $data['order_prod_status']!='completed' && $data['order_prod_status']!='cancel'){ echo '( '.$days.' day(s))';} ?>
                                                                                            <!-- <?= $data['order_prod_shipping'] ?> ( <?= $data['order_prod_free_ship_days'] ?> ) -->
                                                                                        </div>
                                                                                        <!-- Row Starts -->
                                                                                    </div>
                                                                                    <!-- Track Section Ends -->

                                                                                    <!-- New Section Starts -->
                                                                                    <div class="fan-con">
                                                                                        <div class="shipbil-rcol">
                                                                                            <!-- Track Section Starts -->
                                                                                            <div class="inany-wrapper odrsummary">
                                                                                                <!-- Row Starts -->
                                                                                                <div class="inany-row">
                                                                                                    <div class="inany-lbl">Item Price:</div>
                                                                                                    <div class="inany-dtl">$<?= $data['order_prod_total_price'] ?></div>
                                                                                                </div>
                                                                                                <!-- Row Starts -->
                                                                                                <!-- Row Starts -->
                                                                                                <div class="inany-row">
                                                                                                    <div class="inany-lbl">Shipping &amp; Handing:</div>
                                                                                                    <div class="inany-dtl">$<?= number_format($data['order_prod_ship_price'], 2, '.', '') ?></div>
                                                                                                </div>
                                                                                                <!-- Row Starts -->
                                                                                            </div>
                                                                                            <!-- Track Section Ends -->
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- New Section Ends -->
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!-- Right Section Ends -->
                                                                    </div>
                                                                    <!-- List Row Ends -->
                                                                <?php } ?>
                                                            <?php } ?>
                                                            <!-- Back To Order Section Starts -->
                                                            
                                                            <?php if ($results->order_prod_tracking_number == '' && $order_pickup == 0 && $order_history == 0) { ?>
                                                            <div class="btorder-row">
                                                                <div class="btorder-lk">
                                                                    <a href="<?= base_url() ?>manage-order" title="Back to Orders">&lt;&lt; Back to Orders</a>
                                                                </div>
                                                            </div>
                                                            <?php } else if($results->order_prod_completion_date == NULL && $order_pickup == 0 && $order_history == 0) { ?>
                                                            <div class="btorder-row">
                                                                <div class="btorder-lk">
                                                                    <a href="<?= base_url() ?>order-waiting" title="Back to Orders">&lt;&lt; Back to Orders</a>
                                                                </div>
                                                            </div>
                                                            <?php } else if($order_pickup == 0 && $order_history == 1) { ?>
                                                            <div class="btorder-row">
                                                                <div class="btorder-lk">
                                                                    <a href="<?= base_url() ?>order-history" title="Back to Orders">&lt;&lt; Back to Orders</a>
                                                                </div>
                                                            </div>
                                                            <?php } else { ?>
                                                            <div class="btorder-row">
                                                                <div class="btorder-lk">
                                                                    <a href="<?= base_url() ?>order-pickup" title="Back to Orders">&lt;&lt; Back to Orders</a>
                                                                </div>
                                                            </div>
                                                            <?php } ?>
                                                            <!-- Back To Order Section Ends -->
                                                        </div>
                                                        <!-- Bottom My Fan Section Ends -->
                                                    </div>
                                                </div>
                                                <!-- Border Row Ends -->
                                            </div>
                                            <!-- Page Section Ends -->
                                        </div>
                                    </div>
                                    <!-- Bottom Grey Section Ends -->
                                </div>
                                <!-- Store Section Ends -->
                                <!--</form>-->
                            </div>
                            <!-- Bottom Section Ends -->
                        </div>
                        <!-- Inner Section Ends -->
                    </div>
                </div>
                <!-- Right Pannel Starts Here -->
            </div>
        </div>
    </div>
</section>
<!--Blog main ends  here-->
<script type="text/javascript">

        function toggleShipping(order_id, value) {
            
            if ( order_id == "" || typeof order_id == undefined || typeof order_id == "undefined" ) {
                
                alert("Sorry! Can't save the shipping method. Please try again later.");
                console.log("Error! Invalid or empty order_id");
                return false;
                
            }
            
            if ( value == "Charge for Shipping") {
                jQuery("#form_shipping_method_"+order_id+" .shipdbtm-rp").slideDown();
            } else {
                jQuery("#form_shipping_method_"+order_id+" .shipdbtm-rp").slideUp();
            }
            
            
        }
        
        function saveShippingMethod(order_id) {
            
            if ( order_id == "" || typeof order_id == undefined || typeof order_id == "undefined" ) {
                
                alert("Sorry! Can't save the shipping method. Please try again later.");
                console.log("Error! Invalid or empty order.");
                return false;
                
            }
            
            var ucareer = jQuery("#ucareer_"+order_id).val();
            
            if( ucareer == "" ) {
                jQuery("#form_shipping_method_"+order_id+" .shipd-row").append("<div class='error' id='shippingMessage'><p>Shipping type is requried.</p></div>");
                    setTimeout(function(){
                        jQuery("#shippingMessage").slideUp(function(){jQuery("#shippingMessage").remove();});
                    }, 2000);
                    return false;
            }
            
            var shipping_method = "";
            
            if ( ucareer == "Charge for Shipping") {
                
                shipping_method = jQuery("#form_shipping_method_"+order_id+" input[name='shipmethod_"+order_id+"']:checked").val();
//console.log(shipping_method);
                if( shipping_method == "" || shipping_method == undefined || shipping_method == "undefined" ) {
                    jQuery("#form_shipping_method_"+order_id+" .shipd-row").append("<div class='error' id='shippingMessage'><p>Shipping method is requried.</p></div>");
                    setTimeout(function(){
                        jQuery("#shippingMessage").slideUp(function(){jQuery("#shippingMessage").remove();});
                    }, 2000);
                    return false;
                }                
                
            }
            
//            var data = "shipping:"+shipping_method+",shipping_methods:"+ucareer+",order_id:"+order_id;
            
            jQuery.ajax({
                
                type:"POST",
                url:'<?= base_url() . "saveShipping" ?>',
                dataType:"JSON",
                data:{shipping:shipping_method,shipping_methods:ucareer,order_id:order_id,<?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>'},
                beforeSend: function(){
                    jQuery("#form_shipping_method_"+order_id+" button[type='button']").html("Saving...");
                    jQuery("#form_shipping_method_"+order_id+" button[type='button']").prop("disabled", true);
                },
                success: function(response){
                    console.log(response);
                    var url ='<?=$url?>';
                    window.location.replace(url);
                    /*setTimeout(function(){
                        jQuery("#form_shipping_method_"+order_id+" button[type='button']").prop("disabled", false);
                        jQuery("#form_shipping_method_"+order_id+" button[type='button']").html("Save");
                    }, 2500);
                    
                    if ( response.status == "success" ) {
                        jQuery("#form_shipping_method_"+order_id+" .shipd-row").append("<div class='success' id='shippingMessage'><p>"+response.message+"</p></div>");
                    } else {
                        jQuery("#form_shipping_method_"+order_id+" .shipd-row").append("<div class='error' id='shippingMessage'><p>"+response.message+"</p></div>");
                    }
                    
                    setTimeout(function(){
                        jQuery("#shippingMessage").slideUp(function(){jQuery("#shippingMessage").remove();});
                    }, 2000)*/

                },
                error: function(a, b, c){
                    console.log("Error! Ajax error.");
                    console.log(b);
                }
                
            });
            
        }

    $(document).ready(function () {

        $('.editMethod').click(function () {
            //alert('yello');
            $('.sm-drop').fadeOut();
            $(this).parent().parent().find('.sm-drop').fadeToggle();
            //$('.sm-drop').fadeToggle();
            return false;
        });

        $('.shipd-close').click(function () {
            //alert('yello');
            $('.sm-drop').fadeOut();
            return false;
        });

    });

    $(document).on('click', function (e) {
        if ($(e.target).closest('.sm-drop').length === 0) {
            $('.sm-drop').fadeOut();
        }
    });
    $(document).ready(function () {
        $('#update_tracking').click(function (e) {
            e.preventDefault();
            var order = $('#order_id').val();
            var tracking_number = $('#tracknumber').val();
            /*var carrier_name = $('#carrierName').val();*/
            if (tracking_number == '') {
                $("#tracknumber").addClass('error');
                $("#tracknumber_validate").html('');
                $("#tracknumber_validate").html('This field is required.');
            } else {
                var url = "<?= base_url() ?>update-tracking";
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: "json",
                    data: {tracking_number: tracking_number, order_id: order, <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                    success: function (data) {
                        setTimeout(function () {
                            $('#message').html('');
                            $('#message').removeClass("alert-success");
                            $('#message').addClass("hide");
                        }, 5000);
                        if (data.status == 'Available') {
                            $('#message').html(data.msg);
                            $('#message').addClass("alert-success");
                            $('#message').removeClass("hide");
                            $('#tracknum').html(tracking_number);
                        }

                    }
                });
            }
        });
    });
</script>