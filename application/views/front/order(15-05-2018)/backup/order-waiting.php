<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard') ?>">My Account</a> </li>
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>manage-order">Manage Orders</a></li>
                        <li class="breadcrumb-item active">Awaiting Delivery</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view("front/includes/seller-left-nav") ?>  
                <!--Left section starts here-->
                <!--right section starts here-->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">
                        <div class="mang-prod-mian">

                            <!--Manage product section-->	
                            <div class="mang-prod-mian-hed">
                                <h5>Manage Orders</h5>
                                <div class="mang-prod-mian-hed-rg">
                                    <p>Total Amount Received:<span class="clor-ble">$<?= number_format(store_amount_received($store_id), 2, '.', '') ?></span></p>
                                    <p> | </p>
                                    <p class="pad-rgt-0">Amount Pending:<span class="clor-ble">$<?= number_format(store_amount_pending($store_id), 2, '.', '') ?></span></p>
                                </div>
                            </div>
                            <!--Manage product section-->
                            <div class="ualert-row">
                                <?php
                                if ($this->session->flashdata('msg') != "") {
                                    echo '<div id="message" class="alert alert-success message "><a href="javascript:;" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>' . $this->session->flashdata('msg') . '</div>';
                                }
                                ?>
                                <div id="message" class="alert message hide"></div>
                            </div>
                            <!--pending shipping section starts here-->
                            <div class="pend-shp-ord">
                                <a href="<?= base_url() ?>manage-order">Pending Shipping</a>
                                <a href="<?= base_url() ?>order-waiting" class="active">Awaiting Delivery</a>
                                <a href="<?= base_url() ?>order-pickup">Awaiting Pickup</a>
                                <a href="<?= base_url() ?>order-history">Order History</a>
                            </div>
                            <!--pending shipping section ends here-->

                            <!--Select option starts here-->
                            <div class="select-optn-sec">
                                <div class="my-account-right-2">
                                    <div class="well">
                                        <?php echo form_open_multipart('search_order', array('name' => 'search_order', 'id' => 'search_order_form')); ?>
                                        <div class="mang-search-opt">
                                            <label>Search by:</label>
                                            <select class="selectpicker" id="select_search" name="select_search">
                                                <option class="prod-id" value="0" >Select</option>
                                                <option class="prod-id" value="1" >Order ID</option>
                                                <!--<option class="prod-id" value="2" >Tracking Number</option>-->
                                                <!-- <option class="prod-id" value="3" >Product ID</option> -->
                                                <option class="prod-id" value="4" >Product Title</option>                                                
                                            </select>
                                            <div class="error" id="select_search_validate"></div>
                                        </div>

                                        <div class="search-product">
                                            <input type="text" name="search_value" id="search_value" />
                                            <input type="hidden" value="2" name="step" />
                                            <input type="submit" value="Search" />
                                            <a href="<?=base_url('order-waiting')?>" class="resetbutton">Reset</a>
                                            <div class="error" id="search_value_validate"></div>
                                        </div>
                                        <?php echo form_close(); ?>
                                    </div>
                                    <div class="pendshp-tb2">	 <!-- pendshp-tb -->
                                        <div class="my-account-right-table">
                                            <?php if (isset($results)) { ?>
                                                <table class="table">
                                                    <colgroup>
                                                        <col class="manage-order-c1" width="9%" />
                                                        <col class="manage-order-c2" width="9%" />
                                                        <col class="manage-order-c3" width="8%" />
                                                        <col class="manage-order-c4" width="13%" />
                                                        <col class="manage-order-c5" width="13%" />
                                                        <col class="manage-order-c6" width="16%" />
                                                        <col class="manage-order-c7" width="11%" />
                                                        <col class="manage-order-c8" width="8%" />
                                                        <col class="manage-order-c9" width="6%" />
                                                        <col class="manage-order-c10" width="7%" />
                                                    </colgroup>
                                                    <thead>
                                                        <tr>
                                                            <th class="pad-lft-0">Date</th>
                                                            <th class="text-center">Order #</th>
                                                            <th class="text-center">Item</th>
                                                            <th>&nbsp;</th>
                                                            <th class="text-center">Tracking #</th>
                                                            <th class="text-center">Price + Shipping</th>
                                                            <th class="text-center">My Profit</th>
                                                            <th class="text-center">Status</th>
                                                            <th class="text-center">Print</th>
                                                            <th class="text-center pad-rgt-0">Detail</th>
                                                        </tr>
                                                    </thead>
                                                    <?php
                                                    foreach ($results as $data) {
                                                        ?>
                                                        <tr id="<?=$data->order_prod_id?>">
                                                            <td class="pad-lft-0"><?= date('m-d-Y', strtotime($data->order_date)) ?></td>
                                                            <td class="text-center"><?= $data->order_id ?></td>

                                                            <td class="text-center">
                                                                <a href="<?= productDetailUrl($data->order_prod_id) ?>/<?= $data->order_prod_url ?>">
                                                                    <img src="<?= base_url() ?>resources/prod_images/thumb/<?= $data->order_prod_image ?>" alt="" title="" class="img-nset" />
                                                                </a>
                                                            </td>
                                                            <td><a href="<?= productDetailUrl($data->order_prod_id) ?>/<?= $data->order_prod_url ?>" class="prt-btn"><?= $data->order_prod_name ?></a></td>
                                                            <td class="text-center">
                                                                <?= $data->order_prod_tracking_number ?>
                                                            </td>
                                                            <td class="text-center">
                                                                <p>$<?= number_format($data->order_prod_unit_price, 2, '.', '') ?>+$<?= number_format($data->order_prod_ship_price, 2, '.', '') ?></p>
                                                            </td>
                                                            <td class="text-center">
                                                                <?php
                                                                // $profit = calculateSellerProfit($store_id,$data->order_id);
                                                                $profit = calculateSellerProfit($store_id,$data->order_id)->totalProfit - calculateSellerProfit($store_id,$data->order_id)->shipping_total;
                                                                
                                                                echo "$ ".number_format($profit, 2, '.', '');
                                                                ?>
                                                            </td>
                                                            <td class="text-center"><?=$data->order_prod_status?></td>
                                                            <td class="text-center">
                                                                <a href="<?= base_url() ?>print_order/<?= $data->order_id ?>" class="prt-btn" data-toggle="tooltip" data-placement="bottom" title="Print Order in PDF format" target="_blank">Print</a>
                                                            </td>
                                                            <td class="pad-rgt-0 text-center">
                                                                <a href="<?= base_url() ?>order-details/<?= $data->order_id ?>" class="detl-btn"></a>
                                                                <!-- Btn Structure Starts -->
                                                                <div class="btn-addspace"></div>
                                                                <div class="tblconfrm-btn">
                                                                    <a href="javascript:void(0);" data-prodid="<?=$data->order_prod_id?>" data-storeid="<?= $data->order_id ?>" onclick="status(this)">Confirm</a>
                                                                </div>
                                                                <!-- Btn Structure Ends -->
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            <?php } else { ?>
                                                <div id="message" class="alert alert-danger message "><a href="javascript:;" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>No record found.</div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Select option ends here-->   
                            <?php if (isset($results)) { ?>
                                <div class="shop-right-third">
                                    <div class="shop-right-third-lft">
                                        <p>
                                            <?php
                                            if ($total_records > 0) {
                                                echo $to + 1;
                                            } else {
                                                echo $to;
                                            }
                                            ?> - <?php
                                            if ($from > $total_records) {
                                                echo $total_records;
                                            } else {
                                                echo $from;
                                            }
                                            ?> of <?= $total_records ?><!--  in Category Name -->
                                        </p>
                                    </div>
                                    <div class="shop-right-third-rgt">
                                        <ul>
                                            <?php
                                            if (isset($links)) {
                                                echo $links;
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
                <!--right section starts here-->
            </div>
        </div>
    </div>
</section>
<!--Blog main ends  here-->
<script type="text/javascript">
//    $(document).ready(function () {
//        $(".submit_product_form").on('click', function (e) {    
//        });
//    });
    function status(d){
        var prodId = d.getAttribute("data-prodid");
        var orderId = d.getAttribute("data-storeid");
        var url = "<?= base_url() ?>confirm-order";
        $.ajax({
            url: url,
            type: "POST",
            dataType: "json",
            data: {order_id: orderId,Prod_id:prodId,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function (data) {
                setTimeout(function () {
                    $('#message').html('');
                    $('#message').removeClass("alert-success");
                    $('#message').addClass("hide");
                }, 3000);
                if (data.status == 'Available') {
                    $('#message').html(data.msg);
                    $('#message').addClass("alert-success");
                    $('#message').removeClass("hide");
                    $('#'+ prodId).hide();
                }

            }
        });
    }
    $(function () {
        $('a').filter('a[rel^="next"]').addClass('page-right');
        $('a').filter('a[rel^="prev"]').addClass('page-left');
    });
</script>
<script>
    $(function () {

        $(document).ready(function () {
            $.validator.addMethod("valueNotEquals", function (value, element, arg) {
                return arg !== value;
            }, "This field is required.");
            $(function validate() {
                // body...
                var rules = {
                    rules: {
                        search_value: {
                            required: true
                        },
                        select_search: {valueNotEquals: "0"}
                    },
                    errorPlacement: function (error, element) {
                        var name = $(element).attr("name");
                        //error.html($("#" + name + "_validate"));
                        $("#" + name + "_validate").html(error);
                    },
                };
                $('#search_order_form').validate(rules);
            });
        });
        
        $('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');
        //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
        $('.message .close').hover(
                function () {
                    $(this).addClass('hover');
                },
                function () {
                    $(this).removeClass('hover');
                }
        );

        $('.message .close').click(function () {
            $(this).parent().fadeOut('slow', function () {
                $(this).remove();
            });
        });
    });
</script>

