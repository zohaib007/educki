<?php
if (empty($pagedata->store_banner_image)) {
    $path = base_url('resources/no_banner/no_banner.png');
} else {
    $path = base_url('resources/seller_account_image/banner/' . $pagedata->store_banner_image);
}
?>
<!--Breadcrumbs starts here-->
<section class="membr-pg-bg" style="background-image: url(<?= $path ?>); !important">
    <div class="frg-layr"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
                <div class="auto-container">
                    <div class="brd-whit">
                        <div class="brd-sec pad-btm-0">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                                <li class="breadcrumb-item active">
                                    <?= $pagedata->store_name ?>
                                </li>
                            </ol>
                        </div>
                        <h2>Store</h2>
                    </div>
                    <div class="memb-crc-img">
                        <img src="<?= base_url() ?>resources/seller_account_image/logo/thumb/<?= $pagedata->store_logo_image ?>" alt="" title="" class="img-responsive img-circle" />
                    </div>
                    <div class="membr-crc-left">
                        <h4><?= $pagedata->store_name ?></h4>
                        <p class="membr-cty">
                            <?= $pagedata->store_city ?>,
                                <?= $pagedata->store_state ?>
                        </p>
                        <?php if($userdata->user_id!=$seller_data->user_id){?>
                        <a href="mailto: <?= $seller_data->user_email ?>" target="_top">
                            <p class="membr-contct">Contact</p>
                        </a>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Breadcrumbs ends  here-->
<section class="membr-pg-lowr">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-10 col-sm-12 col-xs-12">
                <div class="membr-snce">
                    <div class="membr-snce-rw">
                        <div class="membr-snce-rw-left">
                            <h5><?= $pagedata->store_description ?></h5>
                            <h6>eDucki Member </h6><span>Since <?= date('Y', strtotime($seller_data->user_register_date)); ?></span>
                        </div>
                        <div class="membr-snce-rw-right">
                            <div class="membr-share">
                                <p>Share:</p>
                                <div class="shr-img">
                                    <ul>
                                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?=base_url()?>store/<?=$pagedata->store_url?>" class="shr-fb" target="_blank"></a></li>
                                        <li><a href="https://twitter.com/home?status=<?=$pagedata->store_name?>%20<?=base_url()?>store/<?=$pagedata->store_url?>" class="shr-fb2" target="_blank"></a></li>
                                        <li><a href="https://pinterest.com/pin/create/button/?url=<?=base_url();?>store/<?=$pagedata->store_url?>&media=<?=$share_image;?>&description=<?=$pagedata->store_description?>" class="shr-fb3" target="_blank"></a></li>
                                        <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?=base_url();?>store/<?=$pagedata->store_url?>&title=<?=$pagedata->store_name?>&summary=<?=$pagedata->store_description?>" class="shr-fb4" target="_blank"></a></li>
                                        <li><a href="mailto:?Subject=<?=$pagedata->store_name?>&body=<?=base_url()?>store/<?=$pagedata->store_url?>" class="shr-fb5"></a></li>
                                    </ul>
                                </div>
                            </div>
                            
                            <div class="membr-stry-btn">
                                <a href="<?= base_url() ?>story/<?= $pagedata->store_url ?>" class="styr-btn">My Story</a>
                                <?php if($this->session->userdata('logged_in')){?>
                                <?php if($userdata->user_id!=$seller_data->user_id){?>
                                <a href="javascript:void(0)" data-id="<?=$fans?>" id="favrt-btn" class="favrt-btn <?php if($is_fan!=0){echo " active ";}?>">Favorite Shop<span id="counter">(<?=$fans?>)</span></a>
                                <?php }}?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
$(document).on('click', '.favrt-btn', function() {
    var store_id = <?=$pagedata->store_id;?>;
    var fan_id = <?=$userdata->user_id;?>;
    var curr_value = <?=$fans?>;
    $.ajax({
            url: '<?=base_url('register-fan')?>',
            type: 'POST',
            data: { store_id: store_id, fan_id: fan_id, <?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>' },
        })
        .done(function(result) {
            if (result == 1) {
                //save succesfully
                $('#favrt-btn').addClass('active');
                new_val = curr_value+1;
                $('#counter').text("("+new_val+")");
            } else {
                error = 1;
            }
        })
});

$(document).ready(function(){
    $('#home').hide();
    $('#menu1').hide();
});
</script>
<?php if ($pagedata->store_discount > 0 && $pagedata->store_discount_item > 0) { ?>
    <section class="pls-itm">
        <div class="container">
            <div class="row">
                <div class="auto-container">
                    <div class="pls-itm-sec">
                        <h4>
                            <?php echo $pagedata->store_discount ?>% off when you buy <?php echo $pagedata->store_discount_item ?>+ Items
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<!--Shop Filters starts here-->
<section class="shp-fltr-sec pad-tp-0">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="col-sm-3 col-xs-3 pad-lft-0">
                    <?php
                    $this->load->view('front/includes/shop-left-nav');
                    ?> 
                </div>
                <div class="col-sm-9 col-xs-9">
                    <div class="comp-alert alert alert-danger alert-dismissable fade in hide" >
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span id="comp_msg">This alert box could indicate a dangerous or potentially negative action.</span>
                        </div>
                    <div class="shop-right">
                        <div class="shop-right-upr pad-tp-0">
                            <?php echo form_open('',array('id'=>'storeDetailForm','name'=>'storeDetailForm', 'method'=>'get')); ?>
                            <div class="srt-sec">
                                <h6>Sort By:</h6>
                                <select name="sort_by"  onchange="javascript: $('#storeDetailForm').submit();">
                                    <option value="new" <?php if($sort_by == "new") { echo "selected='selected'"; } ?>>Newest Arrivals</option>
                                    <option value="featured" <?php if($sort_by == "featured") { echo "selected='selected'"; } ?>>Featured</option>
                                    <option value="price_lh" <?php if($sort_by == "price_lh") { echo "selected='selected'"; } ?>>Price: Low to High</option>
                                    <option value="pricehl" <?php if($sort_by == "pricehl") { echo "selected='selected'"; } ?>>Price: High to Low</option>
                                </select>
                            </div>
                            <div class="tb-sec">
                                <ul class="nav nav-tabs">
                                    <li><a data-toggle="tab" href="#home" class="tab-1 <?php if($tabNo == 1){ echo 'active'; }?>"></a></li>
                                    <li><a data-toggle="tab" href="#menu1" class="tab-2 <?php if($tabNo == 2){ echo 'active'; }?>"></a></li>
                                    <input type="hidden" name="tabNo" id="tabNo" value="<?=$tabNo?>" >
                                </ul>
                            </div>
                            <div class="show-itm">
                                <h5><?php if($total_rows > 0) {echo $per_page;} else {echo $total_rows; }?> Item(s)</h5>
                                <h6>Show:</h6>
                                <select name="per_page" onchange="javascript: $('#storeDetailForm').submit();">
                                    <option value="10" <?php if($per_page == 10){ echo "selected='selected'";}?>>10</option>
                                    <option value="20" <?php if($per_page == 20){ echo "selected='selected'";}?>>20</option>
                                    <option value="40" <?php if($per_page == 40){ echo "selected='selected'";}?>>40</option>
                                    <option value="60" <?php if($per_page == 60){ echo "selected='selected'";}?>>60</option>
                                    <option value="80" <?php if($per_page == 80){ echo "selected='selected'";}?>>80</option>
                                    <option value="100" <?php if($per_page == 100){ echo "selected='selected'";}?>>100</option>
                                </select>
                            </div>
                        <?php echo form_close(); ?>
                        </div>
                        <div class="shop-right-lwr">
                            <div class="tab-content">

                                <div id="home" class="tab-pane fade <?php if($tabNo == 1){ echo 'in active'; }?>">
                                    <div class="cloths">
                                        <?php if(count($wish_list) > 0) { ?>
                                        <?php
                                        $divStart = 0;
                                        for($i = 0; $i < count($wish_list); $i++) { ?>
                                        <?php if($divStart == 0) { echo '<div class="cloths-row">'; $divStart = 1; } ?>
                                            <div class="cloths-bx <?php if( (($i+1)%3) == 0 ) { echo 'mrg-rgt-0';}?>">
                                                <div class="cloths-bx-img-sec">
                                                    <img src="<?=base_url('resources/prod_images/'.$wish_list[$i]['img_name'])?>" alt="" title="" class="img-responsive"/>
                                                    <?php if($wish_list[$i]['prod_feature'] == 1){ ?>
                                                    <span class="featured-absolt"></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="cloths-bx-hed">
                                                    <?php 
                                                    $produrl = base_url($wish_list[$i]['catURL']);
                                                    if($wish_list[$i]['subCatURL'] != '') {
                                                        $produrl .= '/'.$wish_list[$i]['subCatURL'];
                                                    }
                                                    if($wish_list[$i]['subSubCatURL'] != '') {
                                                        $produrl .= '/'.$wish_list[$i]['subSubCatURL'];
                                                    }
                                                    ?>
                                                    <a href="<?=$produrl.'/'.$wish_list[$i]['prod_url']?>" data-toggle="tooltip" data-placement="right" title="Products Detail" class="left-tol"><?=$wish_list[$i]['prod_title']?></a>
                                                </div>
                                                <div class="cloths-bx-lowr1">
                                                    <a href="<?=base_url('store/'.$wish_list[$i]['store_url'])?>" data-toggle="tooltip" data-placement="top" title="Seller Store"><?=$wish_list[$i]['store_name']?></a>
                                                    <p>
                                                        <?php
                                                        $sDate = date('Y-m-d', strtotime($wish_list[$i]['sale_start_date']));
                                                        $eDate = date('Y-m-d', strtotime($wish_list[$i]['sale_end_date']));
                                                        $tDate = date('Y-m-d');
                                                        if($wish_list[$i]['prod_onsale'] == 1) {
                                                            if($wish_list[$i]['sale_start_date'] != '' && $wish_list[$i]['sale_end_date'] != '') {
                                                                if($tDate >= $sDate && $tDate <= $eDate) {
                                                                    echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['sale_price'], 2,'.','').'</span> <span class="sp2">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                                } else {
                                                                    echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                                }
                                                            }else if($wish_list[$i]['sale_start_date'] != '') {
                                                                if($tDate >= $sDate) {
                                                                    echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['sale_price'], 2,'.','').'</span> <span class="sp2">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                                } else {
                                                                    echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                                }
                                                            } else if($wish_list[$i]['sale_end_date'] != '') {
                                                                if($tDate <= $eDate) {
                                                                    echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['sale_price'], 2,'.','').'</span> <span class="sp2">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                                } else {
                                                                    echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                                }
                                                            } else {
                                                                echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['sale_price'], 2,'.','').'</span> <span class="sp2">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                            }
                                                        } else {
                                                            echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                        }
                                                        ?>
                                                    </p>
                                                </div>
                                                <div class="cloths-bx-lowr2">
                                                    <input class="comp-checks" type="checkbox" id="comp-<?= $wish_list[$i]['prod_id'] ?>" name="cc" onclick="comp_prod(<?= $wish_list[$i]['prod_id'] ?>)"/>
                                                            <label for="comp-<?= $wish_list[$i]['prod_id'] ?>"><span></span><p>Compare</p></label>
                                                    <?php if($this->session->userdata('logged_in')){?>
                                                    <a href="javascript:;" data-id ="<?=$wish_list[$i]['prod_id']?>" class="wish <?php if(check_wishlist_item($wish_list[$i]['prod_id'])){ echo 'active';}?>" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"></a>
                                                    <?php }?>
                                                </div>
                                            </div>
                                        <?php if( (($i+1)%3) == 0 ) { echo '</div>'; $divStart = 0;}?>
                                
                                        <?php }
                                        if($divStart == 1){ echo "</div>"; }
                                        ?>
                                        <?php } else { 
                                            echo '<div class="cloths-row">No item found.</div>';
                                         } ?>
                                    </div>
                                </div>

                                <div id="menu1" class="tab-pane fade <?php if($tabNo == 1){ echo 'in active'; }?>">
                                    <div class="cloths">
                                        <div class="cloths-row">
                                            <?php if(count($wish_list) > 0) { ?>
                                            <?php
                                            $divStart = 0;
                                            for($i = 0; $i < count($wish_list); $i++) { ?>
                                            <div class="cloths-bx-2">
                                                <div class="cloths-bx-2-img">
                                                    <img src="<?=base_url('resources/prod_images/'.$wish_list[$i]['img_name'])?>" alt="" title="" class="img-responsive"/>
                                                    <?php if($wish_list[$i]['prod_feature'] == 1){ ?>
                                                    <span class="featured-absolt"></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="cloths-bx-2-rgt">
                                                    <div class="cloths-bx-hed2">
                                                        <?php 
                                                        $produrl = base_url($wish_list[$i]['catURL']);
                                                        if($wish_list[$i]['subCatURL'] != '') {
                                                            $produrl .= '/'.$wish_list[$i]['subCatURL'];
                                                        }
                                                        if($wish_list[$i]['subSubCatURL'] != '') {
                                                            $produrl .= '/'.$wish_list[$i]['subSubCatURL'];
                                                        }
                                                        ?>
                                                        <a href="<?=$produrl.'/'.$wish_list[$i]['prod_url']?>" data-toggle="tooltip" data-placement="top" title="Products Detail"><?=$wish_list[$i]['prod_title']?></a>
                                                    </div>
                                                    <div class="cloths-bx-lowr3">
                                                        <a href="<?=base_url('store/'.$wish_list[$i]['store_url'])?>" data-toggle="tooltip" data-placement="top" title="Seller Store"><?=$wish_list[$i]['store_name']?></a>
                                                        <p>
                                                            <?php
                                                            $sDate = date('Y-m-d', strtotime($wish_list[$i]['sale_start_date']));
                                                            $eDate = date('Y-m-d', strtotime($wish_list[$i]['sale_end_date']));
                                                            $tDate = date('Y-m-d');
                                                            if($wish_list[$i]['prod_onsale'] == 1) {
                                                                if($wish_list[$i]['sale_start_date'] != '' && $wish_list[$i]['sale_end_date'] != '') {
                                                                    if($tDate <= $sDate && $tDate >= $eDate) {
                                                                        echo 'Price:<span class="sp3">$'.number_format($wish_list[$i]['sale_price'], 2,'.','').'</span> <span class="sp4">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                                    } else {
                                                                        echo 'Price:<span class="sp3">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                                    }
                                                                }else if($wish_list[$i]['sale_start_date'] != '') {
                                                                    if($tDate <= $sDate) {
                                                                        echo 'Price:<span class="sp3">$'.number_format($wish_list[$i]['sale_price'], 2,'.','').'</span> <span class="sp4">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                                    } else {
                                                                        echo 'Price:<span class="sp3">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                                    }
                                                                } else {
                                                                    echo 'Price:<span class="sp3">$'.number_format($wish_list[$i]['sale_price'], 2,'.','').'</span> <span class="sp4">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                                }
                                                            } else {
                                                                echo 'Price:<span class="sp3">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                            }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="cloths-bx-lowr4">
                                                        <input class="comp-checks" type="checkbox" id="comp2-<?= $wish_list[$i]['prod_id'] ?>" name="cc" onclick="comp_prod(<?= $wish_list[$i]['prod_id'] ?>)"/>
                                                            <label for="comp-<?= $wish_list[$i]['prod_id'] ?>"><span></span><p>Compare</p></label>
                                                        <!-- <a href="#" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"></a> -->
                                                        <?php if($this->session->userdata('logged_in')){?>
                                                        <a href="javascript:;" data-id ="<?=$wish_list[$i]['prod_id']?>" class="wish <?php if(check_wishlist_item($wish_list[$i]['prod_id'])){ echo 'active';}?>" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"></a>
                                                        <?php }?>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <?php } else { 
                                                echo '<div class="cloths-bx-2">No item found.</div>';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="shop-right-third">
                            <div class="shop-right-third-lft">
                                <p>
                                    <?php if($total_rows > 0) {echo $to+1;} else {echo $to; }?> - <?php if($from > $total_rows) {echo $total_rows;} else {echo $from; }?> of <?=$total_rows?><!--  in Category Name -->
                                </p>
                            </div>
                            <div class="shop-right-third-rgt">
                                <ul class="pageing">
                                    <?=$paginglink?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $compare_listing = getCompareList2();?>
<div class="compare-bg <?php if(empty($compare_listing)){echo 'hide';}?>" id="top-pag1">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="compare-main">
                    <div id="compare_listing">
                        <?=$compare_listing?>
                        
                    </div>
                    <div class="col-md-3 pad-25">
                        <a id="comp_btn" href="<?=base_url().'compare-details'?>" class="com-btn <?php if(empty($compare_listing)){echo 'hide';}?>">Compare</a>
                        <a href="javascript:void(0);" onclick="clearcompare()" class="clear-btn">Clear List</a>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
    <a data-toggle="modal" data-target="#myModal4" id="myAnchor" style="display: none;">Click HERE</a>
    <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog popup-4" role="document">
    <div class="modal-content">
      <div class="modal-header pad-0">        
        <button type="button" style="z-index: 99;" class="close" data-dismiss="modal" aria-label="Close">
        </button>
      </div>
      <div class="modal-body pad-0">
       <p class="mrg-botm-0" id="wishlist-text"></p>
      </div>
      
    </div>
</section>
<!--Shop Filters ends here-->
<script>
    $(document).ready(function(){
        
        var tabNo = "<?=@$tabNo?>";
        if(tabNo == 1){
            $('.tab-1').click();
            $('#home').show();
        }else if(tabNo == 2){
            $('.tab-2').click();
            $('#menu1').show();
        }

        $(document).on('click','.tab-1', function(){
            $('.tab-2').removeClass('active');
            $(this).addClass('active');
            $('#tabNo').val(1);
            // alert('dsdsd');
            $('#storeDetailForm').submit();
        });

        $(document).on('click','.tab-2', function(){
            $('.tab-1').removeClass('active');
            $(this).addClass('active');
            $('#tabNo').val(2);
            // alert('2wsas');
            $('#storeDetailForm').submit();
        });
    });
</script>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function() {
        /* Toggle between adding and removing the "active" class,
         to highlight the button that controls the panel */
        this.classList.toggle("active");

        /* Toggle between hiding and showing the active panel */
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    }
}
</script>
<script>
var rangeOne = document.querySelector('input[name="rangeOne"]'),
    rangeTwo = document.querySelector('input[name="rangeTwo"]'),
    outputOne = document.querySelector('.outputOne'),
    outputTwo = document.querySelector('.outputTwo'),
    inclRange = document.querySelector('.incl-range'),
    updateView = function() {
        if (this.getAttribute('name') === 'rangeOne') {
            outputOne.innerHTML = this.value;
            outputOne.style.left = this.value / this.getAttribute('max') * 100 + '%';
        } else {
            outputTwo.style.left = this.value / this.getAttribute('max') * 100 + '%';
            outputTwo.innerHTML = this.value
        }
        if (parseInt(rangeOne.value) > parseInt(rangeTwo.value)) {
            inclRange.style.width = (rangeOne.value - rangeTwo.value) / this.getAttribute('max') * 100 + '%';
            inclRange.style.left = rangeTwo.value / this.getAttribute('max') * 100 + '%';
        } else {
            inclRange.style.width = (rangeTwo.value - rangeOne.value) / this.getAttribute('max') * 100 + '%';
            inclRange.style.left = rangeOne.value / this.getAttribute('max') * 100 + '%';
        }
    };

document.addEventListener('DOMContentLoaded', function() {
    updateView.call(rangeOne);
    updateView.call(rangeTwo);
    $('input[type="range"]').on('mouseup', function() {
        this.blur();
    }).on('mousedown input', function() {
        updateView.call(this);
    });
});
</script>
<script>
function myMap() {
    var geocoder = new google.maps.Geocoder();
    var address = "<?= $pagedata->store_zip ?>";

    geocoder.geocode({ 'address': address }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();

            var myCenter = new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude));
            var mapCanvas = document.getElementById("googleMap");
            var mapOptions = { center: myCenter, zoom: 5, zoomControl: true, scaleControl: true };
            var map = new google.maps.Map(mapCanvas, mapOptions);
            var marker = new google.maps.Marker({ position: myCenter });
            marker.setMap(map);
        }
    });
}
</script>
<script type="text/javascript">
    $(document).on('click', '.wish', function () {
        var element = $(this);
        var prod_id = element.attr("data-id");

        $.ajax({
            url: '<?= base_url('add-product-wishlist') ?>',
            type: 'POST',
            data: {prod_id: prod_id, <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
        })
                .done(function (result) {
                    if (result == 1) {
                        //save succesfully
                        element.addClass('active');
                        $("#wishlist-text").text('Item added to your wishlist');
                        $('#myAnchor').trigger('click'); 
                    } else if (result == 2) {
                        //already exists
                        $("#wishlist-text").text('Item already exits in your wishlist');
                        $('#myAnchor').trigger('click'); 
                    } else {
                        error = 1;
                    }
                    console.log("success");
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAscDVsx1aPq_cm_9DaZAmj6zOIPjhLDpo&callback=myMap"></script>
<script>
    function comp_prod(prod_id){
        var element = $(this);
        var bt_check = $('#comp-'+prod_id).is(':checked');
        var bt_check = $('#comp2-'+prod_id).is(':checked');
        //alert(prod_id+' '+bt_check);
        var url = "<?= base_url() ?>compare-prods-2";
        $.ajax({
            url: url,
            type: "post",
            data: {prod_id: prod_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function (data) {
                data = JSON.parse(data);
                
                    if(data.status == 201){
                        $('#comp_msg').html('Compare list is full.');
                                    $('.comp-alert').removeClass('hide');
                                    $('html, body').animate({
                                        scrollTop: $(".comp-alert").offset().top
                                    }, 2000);
                    }else if(data.status == 200 || data.status == 900){
                        if(data.html != ''){
                            $('#compare_listing').html(data.html);
                            $('.compare-bg').removeClass('hide');
                            $('.shop-container').addClass('compare-set');
                            if(data.status == 900){
                                $('#comp-'+prod_id).removeAttr("checked");
                                $('#comp2-'+prod_id).removeAttr("checked");
                                $('.prod-td-'+$prod_id).remove();
                            }
                            if(data.total_prods < 2){
                                $('#comp_btn').hide();
                            }else if(data.total_prods >= 2){

                                $('#comp_btn').attr('style','display:block;');
                                $('.com-btn').removeClass('hide');
                            }
                        }else{
                            $('.comp-checks').removeAttr("checked");
                            $('.compare-bg').addClass('hide');
                            $('.shop-container').removeClass('compare-set');
                        }
                    }
                //}
            }
        });
    }

    function clearcompare(){
        var url = "<?= base_url() ?>clear-compare-list";
        $.ajax({
            url: url,
            type: "post",
            data: {<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function (data) {
                data = JSON.parse(data);
                if(data.status == 200 ){
                    $('.close').click();
                    $('.comp-checks').removeAttr("checked");
                    $('.compare-bg').addClass('hide');
                    $('.shop-container').removeClass('compare-set');
                }
            }
        });
        //alert('Compare List is now Clear!');
    }

    function removeslctitem(prod_id){
        $.ajax({
            type:'POST',
            url:'<?=base_url()."remove-compare-prod"?>',
            data:{prod_id:prod_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function(data){
                data = JSON.parse(data);
                if(data.status == 200 ){
                    if(data.total_prods > 0){
                        console.log(data.msg);
                        $('#comp-'+prod_id).removeAttr("checked");
                        $('#comp2-'+prod_id).removeAttr("checked");
                        $('.prod-td-'+prod_id).remove();
                        console.log(data.compare_prods);
                        if(data.total_prods < 2){
                                $('#comp_btn').hide();
                            }else if(data.total_prods >= 2){

                                $('#comp_btn').attr('style','display:block;');
                                $('.com-btn').removeClass('hide');
                            }
                    }else{
                        $('.comp-checks').removeAttr("checked");
                        $('.compare-bg').addClass('hide');
                        $('.shop-container').removeClass('compare-set');
                    }
                }
            },
            error: function(){
                console.log('Network error.');
            }
        });
    }
</script>
