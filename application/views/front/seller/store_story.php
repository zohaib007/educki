<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-btm-0">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?=base_url('store/'.$store_url)?>"><?=$pagedata->store_name?></a></li>
                        <li class="breadcrumb-item active">My Story</li>
                    </ol>
                </div>

                <!--my story starts here-->
                <div class="my-stry-mian">
                    <div class="my-stry-upr">                        
                        <div class="my-stry-upr-left">                            
                            <!--Left image sec-->
                            <div class="my-stry-upr-left-inr">
                                <img src="<?= base_url() ?>resources/user_profile_image/<?=$seller_data->user_profile_picture ?>" alt="" title="" class="img-responsive img-circle" />
                            </div>
                        </div>
                        
                        <div class="my-stry-upr-mid">
                            <!--Mid sec-->
                            <div class="my-stry-mid-tp">
                                <div class="my-stry-mid-tp-rw">
                                    <h2><?= $pagedata->store_name ?></h2>
                                </div>
                                <div class="my-stry-mid-tp-rw">
                                    <p class="membr-cty-gry">
                                        <?= $pagedata->store_city ?>,
                                            <?= $pagedata->store_state ?>
                                    </p>
                                    <?php if($userdata->user_id!=$seller_data->user_id && $this->session->userdata('logged_in')){?>
                                    <a href="javascript:void(0);" target="_top">
                                        <p class="membr-contct-gry" data-toggle="modal" data-target="#exampleModal">Contact</p>
                                    </a>
                                    <?php }?>
                                    <h6>eDucki Member </h6>
                                    <span>Since <?=date('Y', strtotime($seller_data->user_register_date));?></span>
                                </div>
                                <div class="my-stry-mid-tp-rw">
                                    <?php if($this->session->userdata('logged_in')){?>
                                        <?php if($userdata->user_id!=$seller_data->user_id){?>
                                            <a href="javascript:void(0)" data-id="<?=$fans?>" id="favrt-btn" style="font-size: 13px;" class="favrt-btn <?php if($is_fan!=0){echo " active unfavrt";}?>"><?=($is_fan!=0)?"unFavorite Shop ":"Favorite Shop "?><span id="counter" style="float: none;"> (<?=$fans?>)</span></a>
                                        <?php }
                                    }?>
                                    <!--<button class="reprt-btn">Report</button>-->
                                    <span class="reprt-btn"><a href="mailto:info@educki.com?Subject=Report" onclick="report()">Report</a></span>
                                </div>
                            </div>
                            <div class="my-stry-upr-botm">
                                <!--bottom sec-->
                                <h4>My Story</h4>
                                <p>
                                    <?=$pagedata->store_description?>
                                </p>
                            </div>
                        </div>
                        <?php if($pagedata->store_facebook_url!='' || $pagedata->store_twitter_url!='' || $pagedata->store_pintrest_url!='' || $pagedata->store_instagram_url!=''){?>
                        <div class="my-stry-upr-right">
                            <h5>Follow Me</h5>
                            <?php if($pagedata->store_facebook_url!=''){?>
                            <a href="<?=$pagedata->store_facebook_url?>" target="_blank" class="folw-fb"></a>
                            <?php } if($pagedata->store_twitter_url!=''){?>
                            <a href="<?=$pagedata->store_twitter_url?>"  target="_blank" class="folw-twt"></a>
                            <?php } if($pagedata->store_pintrest_url!=''){?>
                            <a href="<?=$pagedata->store_pintrest_url?>"  target="_blank" class="folw-pin"></a>
                            <?php } if($pagedata->store_instagram_url!=''){?>
                            <a href="<?=$pagedata->store_instagram_url?>"  target="_blank" class="folw-inst"></a>
                            <?php }?>
                        </div>
                        <?php }?>
                    </div>
                    <div class="my-stry-centr">
                        <div class="my-stry-centr-left">
                            <h5>From Their Shop</h5>
                        </div>
                        <div class="my-stry-centr-right">
                            <div class="cloths">
                                <div class="cloths-row">
                                <?php if(count($wish_list) > 0) { ?>
                                    <?php for($i = 0; $i < count($wish_list); $i++) { ?>
                                        <div class="cloths-bx <?php if( (($i+1)%3) == 0 ) { echo 'mrg-rgt-0';}?>">
                                            <div class="cloths-bx-img-sec">
                                                <a href="<?=$produrl.'/'.$wish_list[$i]['prod_url']?>"><img src="<?=base_url('resources/prod_images/'.$wish_list[$i]['img_name'])?>" alt="" title="" class="img-responsive"/></a>
                                            </div>
                                            <div class="cloths-bx-hed">
                                                <?php 
                                                $produrl = base_url($wish_list[$i]['catURL']);
                                                if($wish_list[$i]['subCatURL'] != '') {
                                                    $produrl .= '/'.$wish_list[$i]['subCatURL'];
                                                }
                                                if($wish_list[$i]['subSubCatURL'] != '') {
                                                    $produrl .= '/'.$wish_list[$i]['subSubCatURL'];
                                                }
                                                ?>
                                                <a href="<?=$produrl.'/'.$wish_list[$i]['prod_url']?>" data-toggle="tooltip" data-placement="top" title="Product Detail"><?=$wish_list[$i]['prod_title']?></a>
                                            </div>
                                            <div class="cloths-bx-lowr1">
                                                <a href="<?=base_url('store/'.$wish_list[$i]['store_url'])?>" data-toggle="tooltip" data-placement="top" title="Seller Store"><?=$wish_list[$i]['store_name']?></a>
                                                <p>
                                                    <?php

                                                    $sDate = date('Y-m-d', strtotime($wish_list[$i]['sale_start_date']));
                                                    $eDate = date('Y-m-d', strtotime($wish_list[$i]['sale_end_date']));
                                                    $tDate = date('Y-m-d');
                                                    if($wish_list[$i]['prod_onsale'] == 1) {
                                                        if($wish_list[$i]['sale_start_date'] != '' && $wish_list[$i]['sale_end_date'] != '') {
                                                            if($tDate >= $sDate && $tDate <= $eDate) {
                                                                echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['sale_price'], 2,'.','').'</span> <span class="sp2">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                            } else {
                                                                echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                            }
                                                        }else if($wish_list[$i]['sale_start_date'] != '') {
                                                            if($tDate >= $sDate) {
                                                                echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['sale_price'], 2,'.','').'</span> <span class="sp2">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                            } else {
                                                                echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                            }
                                                        } else if($wish_list[$i]['sale_end_date'] != '') {
                                                            if($tDate <= $eDate) {
                                                                echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['sale_price'], 2,'.','').'</span> <span class="sp2">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                            } else {
                                                                echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                            }
                                                        } else {
                                                            echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['sale_price'], 2,'.','').'</span> <span class="sp2">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                        }
                                                    } else {
                                                        echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                    }
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                        <?php }
                                    } else { 
                                        echo '<div class="cloths-row">No item found.</div>';
                                    } ?>
                                </div>
                                <?php if(count($stores>0)){ ?>
                                    <?php if(count($wish_list) > 0) { ?>
                                        <div class="cloths-row">
                                            <a href="<?=base_url('store/'.$store_url)?>" class="center-block vst-thr-shp">Visit Their Shop</a>
                                        </div>
                                    <?php } 
                                }?>
                            </div>
                        </div>
                    </div>

                    <div class="my-stry-centr brd-top-0">
                        <div class="my-stry-centr-left">
                            <h5>Favorite Shops</h5>
                        </div>
                        <div class="my-stry-centr-right">
                            <div class="cloths">
                                <div class="cloths-row">
                                <?php if (count($stores) > 0) {
                                foreach ($stores as $i => $store) {?>
                                    <div class="cloths-bx <?php if( (($i+1)%3) == 0 ) { echo 'mrg-rgt-0';}?>">
                                        <div class="cloths-bx-img-sec">
                                            <a href="<?=base_url()?>store/<?=$store['store_url']?>"><img src="<?=base_url()?>resources/seller_account_image/banner/<?=$store['store_banner_image']?>" alt="" title="" class="img-responsive"></a>
                                            <!-- <span class="favrt-shp-abslt"></span> -->
                                        </div>
                                        <div class="elzbt-sec">
                                            <div class="elzbt-sec-left">
                                                <a href="<?=base_url()?>store/<?=$store['store_url']?>"><img src="<?=base_url()?>resources/seller_account_image/logo/<?=$store['store_logo_image']?>" alt="" title="" class="img-responsive"/></a>
                                            </div>
                                            <div class="elzbt-sec-right">
                                                <div class="storeNname">
                                                    <a href="<?=base_url()?>store/<?=$store['store_url']?>">
                                                        <?=$store['store_name']?>
                                                    </a>
                                                </div>
                                                <p class="membr-cty-gry">
                                                    <?=$store['store_city']?>,
                                                        <?=$store['store_state']?>
                                                </p>
                                            </div>
                                            <!--<div class="fav-chk"> 
                                                <input type="checkbox" id="c<?=$count?>" value="<?=$store['store_id']?>" name="cc">
                                                <label for="c<?=$count?>"><span></span></label>
                                            </div>-->
                                        </div>
                                    </div>
                                    <?php if($i==3){break;}
                                    }
                                } else {
                                    echo '<div class="cloths-row">No item found.</div>';
                                }?>
                                <?php if(count($stores)>0){?>
                                <div class="cloths-row">
                                    <?php if($this->session->userdata('logged_in')){?>
                                        <a href="<?=base_url()?>favorite-shops/<?=$this->uri->segment(2)?>" class="center-block vst-thr-shp">View All</a>
                                    <?php }else{?>
                                        <a href="<?=base_url()?>favorite-shop/<?=$this->uri->segment(2)?>" class="center-block vst-thr-shp">View All</a>
                                    <?php }?>
                                </div>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="fan_user_id" id="fan_user_id" value="<?php echo (!empty($userdata->user_id)?$userdata->user_id:0)?>">
                <!--my story ends here-->
                </div>
            </div>
        </div>
    </div>
</div>
<a data-toggle="modal" data-target="#myModal4" id="myAnchor"  style="display: none;">Click HERE</a>
<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog popup-4" role="document">
    <div class="modal-content">
      <div class="modal-header pad-0">        
        <button type="button" style="z-index: 99;" class="close" data-dismiss="modal" aria-label="Close">
        </button>
      </div>
          <div class="modal-body pad-0">
            <div class="frgt-sec oneline">
                <div class="fgt-tit">
                    <p class="mrg-botm-0" id="wishlist-text">User reported successfully.</p>
                </div>
            </div>
          </div>
      </div>
    </div>
</div>
<div class="modal fade nw-pop" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
            <div class="alert alert-success alert-dismissible" style="width:100%; float: left;display: none;">
                <strong>Success!</strong> message sent successfully.
            </div>
            
            <div class="detail-ques">
                <h6>As a Question</h6>
                    <textarea id="message"></textarea>
                    <input type="hidden" name="msg_sender" id="msg_sender" value="<?=$this->session->userdata('userid')?>">
                    <input type="hidden" name="msg_receiver" id="msg_receiver" value="<?=$seller_data->user_id?>">
                    <input type="hidden" name="msg_prod" id="msg_prod" value="0">
                    <div class="error" id="ask_error"></div>
                    <input type="submit" id="ask_question">
            </div>
        
      </div>
      
    </div>
  </div>
</div>
<script type="text/javascript">
    
    function report(prod_id) {
        var url = "<?=base_url()?>userReport";
        var userId = "<?=$pagedata->store_id?>";
        $.ajax({
            url: url,
            type: "post",
            data: {userId: userId,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function (data) {
                console.log('Success');                
                $('#myAnchor').trigger('click'); 
            }
        });
    }    
    
$(document).on('click', '.favrt-btn', function() {
    


    var store_id = <?=$pagedata->store_id;?>;
    var fan_id = $('#fan_user_id').val();
    var curr_value = $('#counter').text(); //<?=$fans?>;
    curr_value = curr_value.replace("(", "");
    curr_value = curr_value.replace(")", "");
    curr_value = parseInt(curr_value);
    

    $.ajax({
            url: '<?=base_url('register-fan')?>',
            type: 'POST',
            data: { store_id: store_id, fan_id: fan_id, <?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>' },
        })
        .done(function(result) {
            if (result == 1) {
                //save succesfully
                $('#favrt-btn').addClass('active');
                new_val = curr_value+1;
                $('#counter').text(" ("+new_val+")");
                $('#favrt-btn').html('unFavorite Shop '+"<span id='counter' style='float: none;'>("+new_val+")</span>");
            } else {
                error = 1;
                $('#favrt-btn').removeClass('active');
                new_val = curr_value-1;
                $('#counter').text(" ("+new_val+")");
                $('#favrt-btn').html('Favorite Shop '+"<span id='counter' style='float: none;'>("+new_val+")</span>");

            }
        });

    /*$(document).on('click','.unfavrt', function(){

        $(this).addClass('favrt-btn');
        $(this).removeClass('unfavrt');
        $(this).removeClass('active');
        var val = <?=$pagedata->store_id;?>;
        $.ajax({
            url: '<?=base_url('delete-favorite-shops')?>',
            type: 'POST',
            data: { val: val, <?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>' },
        })
        .done(function(result) {
            if (result == 1) {
                //save succesfully
                window.location.href = "<?=base_url()?>favorite-shops/<?=$this->uri->segment(2)?>";
            } else {
                error = 1;
            }
            console.log("success");
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    });*/
});
$(document).ready(function () {
    $("#ask_question").click(function (e) {
        $('.alert-dismissible').hide();
        e.preventDefault();
        var value = $('textarea#message').val();
        if(value.length > 0)
        {
            $('#ask_error').html('');
            var seller_id = $('#msg_receiver').val();
            var prod_id = $('#msg_prod').val();
            var message = $('#message').val();
            var sender = $('#msg_sender').val();

            $.ajax({
                type: 'POST',
                url: '<?= base_url() ?>submit-message',
                data: {seller_id: seller_id, prod_id: prod_id, message: message,sender:sender,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                success: function (res) {
                    if (res == '200') {
                        $('.alert-dismissible').show();
                    }
                },
                error: function () {
                    console.log('Network error.');
                }
            });
        }else{
            $('#ask_error').html('<p>This field is required.</p>');
        }
    });
});

$(document).on('click','.membr-contct',function(){
    $('.alert-dismissible').hide();
    $('#message').val('');
    $('#ask_error').html('');
});
</script>