<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard'); ?>">My Account</a></li>
                        <li class="breadcrumb-item active">My Store</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->
<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav'); ?>  
                <!--Left section starts here-->
                <!--right section starts here-->
                <div class="col-sm-9 col-xs-12">
                    <?php echo form_open_multipart('', array('name' => 'edittier', 'id' => 'edittier_form')); ?>
                    <div class="my-account-right">
                        <div class="my-account-right-head">
                            <h5>My Tier</h5>
                        </div>
                        <div class="become-seller-sec">
                            <div class="best-descb">
                                <div class="best-descb-lft">
                                    <h5>Select your plan <span>*</span></h5>
                                </div>
                                <div class="best-descb-rgt">
                                    <input type="hidden" name="isTireChange" value = 0 id="isTireChange" />
                                    <input type="hidden" name="tirePrice" value = 0 id="tirePrice" />
                                    <?php 
                                    foreach($tiers as $i => $tir) { ?>
                                    <div class="best-descb-rgt-row">
                                        <input type="radio" id="r_<?=$i?>" class="tiresVal" data-month = "<?=$tir->tier_months?>" data-price="<?=number_format($tir->tier_amount,2,'.','')?>" name="tiersId" value="<?=$tir->tier_id?>" <?=$tier_id == $i+1?'checked':''?>>
                                        <label for="r_<?=$i?>"><span></span><p>Tier <?=$i?>  <?=($tir->tier_months > 0)?'('.$tir->tier_months.' months)':'Free'?> – <?=($tir->tier_amount > 0)?'$'.number_format($tir->tier_amount,2,'.','').'/month,':''?> <?=$tir->tier_fee?>% transaction fee<?=($tir->tier_id == $page_data->store_tier_id && $page_data->store_tier_id!=1)?'<br/><small class="subsmall redtxt">* your subscription will expire on '.date("m-d-Y",strtotime($page_data->store_end_date)).'</small>':''?></p>
                                            <?php if($i == 2){?>
                                            <div class="mst-poplr"><h6>Most Popular</h6></div>
                                            <?php } ?>
                                        </label>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <!-- <div class="best-descb" style="border: none;">
                                <div class="alert alert-danger">
                                  Your subscription will expire on <strong>13-02-2018</strong>
                                </div>
                            </div> -->
                        </div>
                        <div class="store-profile-row">
                            <div class="sv-bck-sec" style="padding-top: 0px;">
                                <a href="<?=base_url()?>edit-store" class="back-button">Back</a>
                                <input class="upload-button" value="Change" type="submit">
                            </div>
                        </div>
                            <input type="hidden" id="store_id" name="store_id" value="<?= $page_data->store_id ?>"/>
                            <input type="hidden" name="user_id" value="<?=$userid?>"/>
                            <input type="hidden" id="old_paypal_id" value="<?php echo $page_data->store_paypal_id; ?>" />
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>              <!--right section starts here-->
            </div>
        </div>
    </div>
</section>
<!--Blog main ends  here-->

<!--register info popup starts here-->
<a href="#" id="tier-msg" data-toggle="modal" data-target="#myModal4" style="display: none;">test</a>
<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog popup-4" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="exampleModalLabel">Tier Change Request</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <p>
                    A new billing rate will be effective on your next billing
                    cycle, and the new subscription will begin effective immediately.
                </p>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var msg = '<?=$this->session->flashdata('msg')?>';
        if(msg!=''){
            $('#tier-msg').trigger('click');
        }
    });
</script>