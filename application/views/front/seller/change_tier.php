<?php
//echo '<pre>';
$userid = $this->session->userdata('userid');
//print_r($this->session->all_userdata());
//$user = $this->session->userdata('userid');
?>
<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard'); ?>">My Account</a></li>
                        <li class="breadcrumb-item active">My Store</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->
<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav'); ?>
                <!--Left section starts here-->
                <!--right section starts here-->
                <div class="col-sm-9 col-xs-12">
                    <?php echo form_open_multipart('', array('name' => 'edittier', 'id' => 'edittier_form')); ?>
                    <div class="my-account-right">
                        <div class="my-account-right-head">
                            <h5>My Tier</h5>
                        </div>
                        <div class="become-seller-sec">
                            <div class="best-descb">
                                <div class="best-descb-lft">
                                    <h5>Select your plan <span>*</span></h5>
                                </div>
                                <div class="best-descb-rgt">
                                    <input type="hidden" name="isTireChange" value = 0 id="isTireChange" />
                                    <input type="hidden" name="tirePrice" value = 0 id="tirePrice" />
                                    <?php
                                    foreach($tiers as $i => $tir) { ?>
                                        <div class="best-descb-rgt-row">
                                            <input type="radio" id="r_<?=$i?>" class="tiresVal" data-month = "<?=$tir->tier_months?>" data-price="<?=number_format($tir->tier_amount,2,'.','')?>" name="tiersId" value="<?=$tir->tier_id?>" <?=$tier_id == $i+1?'checked':''?>>
                                            <label for="r_<?=$i?>"><span></span><p>Tier <?=$i?>  <?=($tir->tier_months > 0)?'('.$tir->tier_months.' months)':'Free'?> – <?=($tir->tier_amount > 0)?'$'.number_format($tir->tier_amount,2,'.','').'/month,':''?> <?=$tir->tier_fee?>% transaction fee<?=($tir->tier_id == $page_data->store_tier_id && $page_data->store_tier_id!=1)?'<br/><small class="subsmall redtxt">* your subscription will expire on '.date("m-d-Y",strtotime($page_data->store_end_date)).'</small>':''?></p>
                                                <?php if($i == 2){?>
                                                    <div class="mst-poplr"><h6>Most Popular</h6></div>
                                                <?php } ?>
                                            </label>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <!-- <div class="best-descb" style="border: none;">
                                <div class="alert alert-danger">
                                  Your subscription will expire on <strong>13-02-2018</strong>
                                </div>
                            </div> -->
                        </div>
                        <div class="store-profile-row">
                            <div class="sv-bck-sec" style="padding-top: 0px;">
                                <a href="<?=base_url()?>edit-store" class="back-button">Back</a>
                                <input class="upload-button" value="Change" id="tierchangebutton" type="button" data-toggle="modal" data-target="#changeTierPayment">
                            </div>
                        </div>
                        <input type="hidden" id="store_id" name="store_id" value="<?= $page_data->store_id ?>"/>
                        <input type="hidden" name="user_id" value="<?=$userid?>"/>
                        <input type="hidden" id="old_paypal_id" value="<?php echo $page_data->store_paypal_id; ?>" />
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>              <!--right section starts here-->
        </div>
    </div>
    </div>

    <div class="modal fade" id="changeTierPayment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content stripe">

                <!-- Pre Loader Starts -->
                <!-- <div class="loading2">
                    <img class="loading-gif-2b" src="<?/*=base_url('front_resources/')*/?>images/loading22b.gif" alt="preloader" />
                </div>
-->
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">eDucki Subscription</h4>
                    <button type="button" class="close close-stripe y" data-dismiss="modal" aria-label="Close" id="changeTierButtonId">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-logos">
                        <img src="<?=base_url()?>front_resources/images/1.png">
                        <img src="<?=base_url()?>front_resources/images/2.png">
                        <img src="<?=base_url()?>front_resources/images/22.png">
                        <img src="<?=base_url()?>front_resources/images/14.png">
                        <span class="secure-pay"><img src="<?=base_url()?>front_resources/images/sec-pay.png">Secure Payment</span>
                    </div>
                    <form id="paymentForm">
                        <input type="hidden" name="_token" value="<?= $this->security->get_csrf_hash() ?>">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>Name On Card</label>
                                    <input type="tel" class="form-control" placeholder="Valid Card Name" name="card_brand" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>Card Number</label>
                                    <input type="tel" class="form-control" placeholder="Valid Card Number" data-stripe="number" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-md-4">
                                <div class="form-group">
                                    <label>Expiration Month</label>
                                    <input type="tel" class="form-control" placeholder="MM" data-stripe="exp-month" />
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-4">
                                <div class="form-group">
                                    <label>Expiration Year</label>
                                    <input type="tel" class="form-control" placeholder="YYYY" data-stripe="exp-year" />
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-4">
                                <div class="form-group">
                                    <label>Security Code</label>
                                    <input type="tel" class="form-control" placeholder="CVV" data-stripe="cvc"  />
                                </div>
                            </div>
                        </div>
                        <!--<input type="hidden" name="token" value="" />-->
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <button type="submit" class="subscribe btn btn-success btn-lg btn-block" type="button" id="subscriptionBtn">Pay Now</button>
                                    <img class="stipe-water" src="<?=base_url()?>front_resources/images/stripe1.png">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Pre Loader Starts -->
    <div class="loading">
        <img class="loading-gif-2" src="<?=base_url('front_resources/')?>images/preloader-2.gif" alt="preloader" />
    </div>

</section>
<!--Blog main ends  here-->
<input type="hidden" id="userid" value="<?php echo $userid ?>" />
<!--register info popup starts here-->
<a href="#" id="tier-msg" data-toggle="modal" data-target="#myModal4" style="display: none;">test</a>
<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog popup-4" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 id="exampleModalLabel">Tier Change Request</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <p>
                    A new billing rate will be effective on your next billing
                    cycle, and the new subscription will begin effective immediately.
                </p>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title error" id="exampleModalLabel">Error!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body error" id="error-message-text">
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            var newtierid1 = $("#r_0").is(':checked');
            if (newtierid1 == true)
            {
                $('#tierchangebutton').attr('data-toggle','');
                $('#tierchangebutton').attr('type','submit');
            }else {
                $('#tierchangebutton').prop('data-toggle','modal');
                $('#tierchangebutton').prop('type','button');
            }

            var msg = '<?=$this->session->flashdata('msg')?>';
            if(msg!=''){
                $('#tier-msg').trigger('click');
            }
            $('#changeTierPayment').hide();



            var oldtierid = $("input[name='tiersId']:checked"). val();
            //console.log("Old Value : " + oldtierid);
            $(".tiresVal").click(function () {

                var newtierid = $("#r_0").is(':checked');
                //console.log('hdijsgidjsbnf');
                if (newtierid == true)
                {
                    $('#tierchangebutton').attr('data-toggle','');
                    $('#tierchangebutton').attr('type','submit');
                }
                else {
                    $('#tierchangebutton').attr('type','button');
                    $('#tierchangebutton').attr('data-toggle','modal');
                }
            });
            console.log("sdsdsds: "+ $("#r_0").is(':checked') );



            // $("input[class='tiresVal']:checked").click();

        });
    </script>
</div>
<script>
    // Stripe.setPublishableKey('pk_live_9ysyjOnNJyEqaMs4X4UnBpSd');
    Stripe.setPublishableKey('pk_test_ClZLWrbM2x5SXNAn2vaC0Yaj');
    $('#paymentForm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            card_brand: {
                validators: {
                    notEmpty: {
                        message: 'The name is required'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z ]+$/,
                        message: 'The Card Name cannot be a number'
                    }
                }
            },
            ccNumber: {
                selector: '[data-stripe="number"]',
                validators: {
                    notEmpty: {
                        message: 'The credit card number is required'
                    },
                    creditCard: {
                        message: 'The credit card number is not valid'
                    }
                }
            },
            expMonth: {
                selector: '[data-stripe="exp-month"]',
                //row: '.col-xs-3',
                validators: {
                    notEmpty: {
                        message: 'The expiration month is required'
                    },
                    digits: {
                        message: 'The expiration month can contain digits only'
                    },
                    callback: {
                        message: 'Expired',
                        callback: function(value, validator) {
                            value = parseInt(value, 10);
                            var year         = validator.getFieldElements('expYear').val(),
                                currentMonth = new Date().getMonth() + 1,
                                currentYear  = new Date().getFullYear();
                            if (value < 0 || value > 12) {
                                return false;
                            }
                            if (year == '') {
                                return true;
                            }
                            year = parseInt(year, 10);
                            if (year > currentYear || (year == currentYear && value >= currentMonth)) {
                                validator.updateStatus('expYear', 'VALID');
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            },
            expYear: {
                selector: '[data-stripe="exp-year"]',
                //row: '.col-xs-3',
                validators: {
                    notEmpty: {
                        message: 'The expiration year is required'
                    },
                    digits: {
                        message: 'The expiration year can contain digits only'
                    },
                    callback: {
                        message: 'Expired',
                        callback: function(value, validator) {
                            value = parseInt(value, 10);
                            var month        = validator.getFieldElements('expMonth').val(),
                                currentMonth = new Date().getMonth() + 1,
                                currentYear  = new Date().getFullYear();
                            if (value < currentYear || value > currentYear + 100) {
                                return false;
                            }
                            if (month == '') {
                                return false;
                            }
                            month = parseInt(month, 10);
                            if (value > currentYear || (value == currentYear && month >= currentMonth)) {
                                validator.updateStatus('expMonth', 'VALID');
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            },
            cvvNumber: {
                selector: '[data-stripe="cvc"]',
                validators: {
                    notEmpty: {
                        message: 'The CVV number is required'
                    },
                    cvv: {
                        message: 'The value is not a valid CVV',
                        creditCardField: 'ccNumber'
                    }
                }
            }
        }
    })
        .on('success.form.fv', function(e) {
            e.preventDefault();
            var $form = $(e.target);
            var userid =  $('#userid').val();
            var tier = $("input[class='tiresVal']:checked").val();

            // Reset the token first
            var token = $form.find('[name="_token"]').val();
            //console.log(token);
            $('.loading').click(function () {
                $(this).show();
            });
            Stripe.card.createToken($form, function(status, response) {
                if (response.error) {
                    $('.close-stripe').click();
                    /*$('#error-message-stripe').click();*/
                    $('#exampleModal').modal('show');
                    $('#error-message-text').text(response.error.message);
                } else {
                    // Set the token value
                    $form.find('[name="_token"]').val(response.id);
                    // Or using Ajax
                    $.ajax({
                        // You need to change the url option to your back-end endpoint
                        url: "<?=base_url('changesubscription/')?>"+ userid+ '/'+ tier+'/'+response.id,
                        data: $form.serialize(),
                        method: 'POST',
                        dataType: 'json'
                    }).success(function(result) {
                        
                        $('#edittier_form').submit();
                        $('.loading').hide();
                        //var res = jQuery.parseJSON(result)
                        $form.formValidation('resetForm', true);
                    });
                }
            });
        });
</script>