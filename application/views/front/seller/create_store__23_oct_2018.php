<?php
//echo '<pre>';
$userid = $this->session->userdata('userid');
//print_r($this->session->all_userdata());
//$user = $this->session->userdata('userid');
?>
<!-- Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard'); ?>">My Account</a></li>
                        <li class="breadcrumb-item active">Become a Seller</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav'); ?>
                <!--Left section starts here-->
                <!--right section starts here-->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">
                        <div class="my-account-right-head">
                            <h5>Name Your Shop <span>*</span></h5>
                            <p>Choose a memorable name that reflects your style.</p>
                        </div>
                        <?php echo form_open_multipart('create_store', array('name' => 'create_store', 'id' => 'createstore_form')); ?>
                        <input type="hidden" name="sub_id" value="" id="sub_id" />
                        <div class="become-seller-sec">
                            <div class="chk-avalb">
                                <div class="chk-avalb-lft">
                                    <input type="text" name="store_name" id="store_name" onblur="check()" placeholder="Store name here" value="<?=set_value('store_name')?>" />
                                    <span id="Available" class="g1 hide">Available</span>
                                    <span id="not_available" class="rd1 hide">Not Available</span>
                                </div>
                                <div class="chk-avalb-rgt">
                                    <input type="submit" id="check_availability" value="Check Availability"/>
                                </div>
                                <p>Your shop name will appear in your shop and next to each of your listings throughout eDucki.</p>
                                <div class="optionl-struct">
                                    <p id="rnd_store_name"></p>
                                </div>
                                <div class="error" id="store_name_validate"><?php echo form_error('store_name') ?></div>
                            </div>
                            <div class="best-descb">
                                <div class="best-descb-lft">
                                    <h5>Which of these best describes you? <span>*</span></h5>
                                </div>
                                <div class="best-descb-rgt">
                                    <div class="best-descb-rgt-row">
                                        <input type="radio" id="r01" name="describe" value="1">
                                        <label for="r01"><span></span><p>Selling is my full-time job</p></label>
                                    </div>
                                    <div class="best-descb-rgt-row">
                                        <input type="radio" id="r02" name="describe" value="2">
                                        <label for="r02"><span></span><p>I sell part-time but hope to sell full-time</p></label>
                                    </div>
                                    <div class="best-descb-rgt-row">
                                        <input type="radio" id="r03" name="describe" checked value="3">
                                        <label for="r03"><span></span><p>I sell part-time and that’s how I like it</p></label>
                                    </div>
                                    <div class="best-descb-rgt-row">
                                        <input type="radio" id="r04" name="describe" value="4">
                                        <label for="r04"><span></span><p>Other</p></label>
                                    </div>
                                </div>
                                <div class="error" id="describe_validate"><?php echo form_error('describe') ?></div>
                            </div>
                            <div class="best-descb">
                                <div class="best-descb-lft">
                                    <h5>Select your plan <span>*</span></h5>
                                </div>
                                <div class="best-descb-rgt">
                                    <?php foreach($tiers as $i => $tir) { ?>
                                        <div class="best-descb-rgt-row">
                                            <input type="radio" id="r_<?=$i?>" class="tierclass" name="tier" value="<?=$i+1?>" <?=($tier_id==($i+1))?'checked':''?>>
                                            <label for="r_<?=$i?>"><span></span><p>Tier <?=$i?>  <?=($tir->tier_months > 0)?'('.$tir->tier_months.' months)':'Free'?> – <?=($tir->tier_amount > 0)?'$'.number_format($tir->tier_amount,2,'.','').'/month,':''?> <?=$tir->tier_fee?>% transaction fee</p>
                                                <?php if($i == 2){?>
                                                    <div class="mst-poplr"><h6>Most Popular</h6></div>
                                                <?php } ?>
                                            </label>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="error" id="tier_validate"><?php echo form_error('tier') ?></div>
                            </div>
                            <div class="get-paid-sec">
                                <div class="get-paid-sec-hed">
                                    <!-- <h4>How you’ll get paid</h4> -->
                                    <h4 style="margin-bottom: 0px;">Accepted</h4>
                                    <!--
                                    <span class="protected">Protected</span>
                                    <a href="https://www.paypal.com" target="_blank">
                                        <img src="<?= base_url() ?>front_resources/images/paypal3.png" alt="" title="" class="img-responsive"/>
                                    </a>
                                    -->
                                </div>
                                <div class="get-paid-sec-lowr">
                                    <img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/cc-badges-ppmcvdam.png" alt="Credit Card Badges">
                                    <p>
                                        eDucki Payment believes simple is better . We use Paypal for all your online sales so you can manage all of your accounts in
                                        one place. Don’t have a Paypal account? Don’t wory, you can create one here .<a href="https://www.paypal.com/us/home" target="_blank"> Learn more</a>
                                    </p>
                                </div>
                                <input type="hidden" name="user_id" value="<?= $userid ?>"/>
                                <!--<input type="hidden" name="user_id" value="1"/>-->
                                <input type="hidden" name="itemname" id="itemname" value="Pro"/>
                                <input type="hidden" name="itemprice" id="itemprice" value="8"/>
                                <input type="hidden" name="itemnumber" id="itemnumber" value="1"/>
                                <input type="hidden" name="itemdesc" id="itemdesc" value="Pro"/>
                                <input type="hidden" name="itemQty" id="itemQty" value="1"/>
                                <div class="get-paid-sec-proces-paymnt">
                                    <h6>Process Payment via Paypal Account<span>*</span></h6>
                                    <div class="best-descb-rgt-row">
                                        <input type="radio" id="r005" name="cc3" checked>
                                        <label for="r005"><span></span><p>Paypal Email</p></label>
                                    </div>
                                    <!-- onblur="pay_check()"  -->
                                    <input type="text" name="paypal_id" id="paypal_id" placeholder="JohnSmith@gmail.com" value="<?=set_value('paypal_id')?>"/>
                                    <p class="disclaim-ntxt">Provide the email address associated with your current PayPal account. If you do not have a PayPal account, the email you provide will be used to create a PayPal account for you.</p>
                                    <div class="error" id="paypal_id_validate"><?php echo form_error('paypal_id') ?></div>

                                    <input type="button" id="continue_button" value="PAY AND CONTINUE" data-toggle="modal" data-target="#paymentModal"/>
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>                <!--right section starts here-->
            </div>
        </div>
    </div>

    <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content stripe">
                <!-- Pre Loader Starts -->
                <!--<div class="loading2">
                    <img class="loading-gif-2b" src="<?/*=base_url('front_resources/')*/?>images/loading22b.gif" alt="preloader" />
                </div>-->

                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">eDucki Subscription</h4>
                    <button type="button" class="close close-stripe x" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-logos">
                        <img src="<?=base_url()?>front_resources/images/1.png">
                        <img src="<?=base_url()?>front_resources/images/2.png">
                        <img src="<?=base_url()?>front_resources/images/22.png">
                        <img src="<?=base_url()?>front_resources/images/14.png">
                        <span class="secure-pay"><img src="<?=base_url()?>front_resources/images/sec-pay.png">Secure Payment</span>
                    </div>
                    <form id="paymentForm">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>Name On Card</label>
                                    <input type="tel" class="form-control" placeholder="Valid Card Name" name="card_brand" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label>Card Number</label>
                                    <input type="tel" class="form-control" placeholder="Valid Card Number" data-stripe="number" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4 col-md-4">
                                <div class="form-group">
                                    <label>Expiration Month</label>
                                    <input type="tel" class="form-control" placeholder="MM" data-stripe="exp-month" />
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-4">
                                <div class="form-group">
                                    <label>Expiration Year</label>
                                    <input type="tel" class="form-control" placeholder="YYYY" data-stripe="exp-year" />
                                </div>
                            </div>
                            <div class="col-xs-4 col-md-4">
                                <div class="form-group">
                                    <label>Security Code</label>
                                    <input type="tel" class="form-control" placeholder="CVV" data-stripe="cvc"  />
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="token" value="" />
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <button type="submit" class="subscribe btn btn-success btn-lg btn-block" type="button" id="subscriptionBtn">Pay Now</button>
                                    <img class="stipe-water" src="<?=base_url()?>front_resources/images/stripe1.png">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</section>
<input type="hidden" id="userid" value="<?php echo $userid ?>" />
<!--Blog main ends  here-->
<!--<script src="https://www.paypalobjects.com/api/checkout.js"></script>-->

<script>
    $(document).ready(function () {
        $('.tierclass').click(function () {
            var tier = $("input[class='tierclass']:checked").val();
            if (tier == 1)
            {
                console.log("Click tier : " + tier);
                $('#continue_button').prop('type','submit');
                $('#continue_button').attr('data-toggle','');
                $('#continue_button').attr('data-target','');
            }
            else {
                $('#continue_button').prop('type','button');
                $('#continue_button').attr('data-toggle','modal');
                $('#continue_button').attr('data-target','#paymentModal');

            }
        });
    });
</script>

<script>

    $(document).ready(function () {
        $.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg !== value;
        }, "This field is required.");
        $(function validate() {
            // body...
            var rules = {
                rules: {
                    store_name: {
                        minlength: 2,
                        maxlength: 20,
                        required: true
                    },
                    tier: {
                        required: true
                    },
                    describe: {
                        required: true
                    },
                    paypal_id:{
                        required: true
                    }
                },
                errorPlacement: function (error, element) {
                    var name = $(element).attr("name");
                    //error.appendTo($("#" + name + "_validate"));
                    $("#"+name+"_validate").html(error);                },
            };
            $('#createstore_form').validate(rules);
        });
    });
</script>
<script>
    function check() {
        $("#check_availability").click();
    }
    $(document).ready(function () {
//        $('.tierclass').trigger('click');
        $("#check_availability").click(function (e) {
            e.preventDefault();

            var url = "<?= base_url() ?>check_availability";
            var value = $('#store_name').val();
            if (value == '') {
                $("#store_name").addClass('error');
                $("#store_name_validate").html('');
                $("#store_name_validate").html('This field is required');
                $("#Available").addClass("hide");
                $("#not_available").addClass("hide");
            } else {
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: "json",
                    data: {"store_name": value,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                success: function (data) {
                    if (data.status == 'Available') {
                        $("#Available").removeClass("hide");
                        $("#not_available").addClass("hide");
                    } else {
                        $("#not_available").removeClass("hide");
                        $("#store_name_validate").html("Store Name Not Avaliable try this " + data.new_name);
                    }
                }
            });
            }
        });
        $(".tierclass").click(function (e) {
            var tierValue = $('input[name=tier]:checked').val();
            if (tierValue == '1') {
                $("#continue_button").prop('value', 'SAVE AND CONTINUE');
            } else {
                var tierurl = "<?= base_url() ?>tier_info";
                $.ajax({
                    url: tierurl,
                    type: "POST",
                    dataType: "json",
                    data: {"tier_value": tierValue,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                success: function (data) {
                    $("#itemname").val(data.status.tier_name);
                    $("#itemprice").val(data.status.tier_fee);
                    $("#itemnumber").val(data.status.tier_id);
                    $("#itemdesc").val(data.status.tier_name);
                }
            });
                $("#continue_button").prop('value', 'PAY AND CONTINUE');
            }
        });
    });

</script>

<script>
    Stripe.setPublishableKey('pk_test_ClZLWrbM2x5SXNAn2vaC0Yaj');
    $('#paymentForm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            card_brand: {
                validators: {
                    notEmpty: {
                        message: 'The name is required'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z ]+$/,
                        message: 'The Card Name cannot be a number'
                    }
                }
            },
            ccNumber: {
                selector: '[data-stripe="number"]',
                validators: {
                    notEmpty: {
                        message: 'The credit card number is required'
                    },
                    creditCard: {
                        message: 'The credit card number is not valid'
                    }
                }
            },
            expMonth: {
                selector: '[data-stripe="exp-month"]',
                //row: '.col-xs-3',
                validators: {
                    notEmpty: {
                        message: 'The expiration month is required'
                    },
                    digits: {
                        message: 'The expiration month can contain digits only'
                    },
                    callback: {
                        message: 'Expired',
                        callback: function(value, validator) {
                            value = parseInt(value, 10);
                            var year         = validator.getFieldElements('expYear').val(),
                                currentMonth = new Date().getMonth() + 1,
                                currentYear  = new Date().getFullYear();
                            if (value < 0 || value > 12) {
                                return false;
                            }
                            if (year == '') {
                                return true;
                            }
                            year = parseInt(year, 10);
                            if (year > currentYear || (year == currentYear && value >= currentMonth)) {
                                validator.updateStatus('expYear', 'VALID');
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            },
            expYear: {
                selector: '[data-stripe="exp-year"]',
                //row: '.col-xs-3',
                validators: {
                    notEmpty: {
                        message: 'The expiration year is required'
                    },
                    digits: {
                        message: 'The expiration year can contain digits only'
                    },
                    callback: {
                        message: 'Expired',
                        callback: function(value, validator) {
                            value = parseInt(value, 10);
                            var month        = validator.getFieldElements('expMonth').val(),
                                currentMonth = new Date().getMonth() + 1,
                                currentYear  = new Date().getFullYear();
                            if (value < currentYear || value > currentYear + 100) {
                                return false;
                            }
                            if (month == '') {
                                return false;
                            }
                            month = parseInt(month, 10);
                            if (value > currentYear || (value == currentYear && month >= currentMonth)) {
                                validator.updateStatus('expMonth', 'VALID');
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            },
            cvvNumber: {
                selector: '[data-stripe="cvc"]',
                validators: {
                    notEmpty: {
                        message: 'The CVV number is required'
                    },
                    cvv: {
                        message: 'The value is not a valid CVV',
                        creditCardField: 'ccNumber'
                    }
                }
            }
        }
    })
        .on('success.form.fv', function(e) {
            e.preventDefault();
            var $form = $(e.target);
            var userid =  $('#userid').val();
            var tier = $("input[class='tierclass']:checked"). val();
            // Reset the token first
            $form.find('[name="token"]').val('');
            Stripe.card.createToken($form, function(status, response) {
                if (response.error) {
                    alert(response.error.message);
                } else {
                    // Set the token value
                    $form.find('[name="token"]').val(response.id);
                    // Or using Ajax
                    $.ajax({
                        // You need to change the url option to your back-end endpoint
                        url: "<?=base_url('storesubscription/')?>"+ userid+ '/'+ tier,
                        data: $form.serialize(),
                        method: 'POST',

                    }).success(function(result) {
                        console.log("Result : " + result);
                        $('#sub_id').val(result);

                        $('#createstore_form').submit();
                        //var res = jQuery.parseJSON(result)
                        console.log("Subscription JSON Response : " + result);
                        $form.formValidation('resetForm', true);
                    });
                }
            });
        });
</script>

