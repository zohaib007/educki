<!--footer starts here-->
<footer>
    <?php $segment = $this->uri->segment(1); if (strpos($segment, 'cart') !== false) { }else {?>
        <section class="footr-upr-sec">
            <div class="container">
                <div class="row">
                    <div class="footr-wrp">
                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                            <div class="hm-nws">
                                <h4 style="padding-top: 15px;">Sign Up For Newsletter!</h4>
                                <!-- <p>
                                    Sample Text Here Sample Text Here
                                </p> -->
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                            <div class="hm-srch-fld">
                                <?php //echo form_open('contact/register_subscriber', array('name' => 'newsletter', 'id' => 'newsletter_form')); ?>
                                <input type="text" name="email_newsletter" class="email-text" id="email-text" placeholder="Enter your email here" />
                                <input type="submit" class="submit-newsletter" id="submit-newsletter" value="Sign UP" />
                                <div class="error" id="email_newsletter_validate"></div>
                                <?php //echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script type="text/javascript">
            $(document).on('click', '.submit-newsletter', function () {
                var email = $('#email-text').val();
                var error = 0;
                if (email.length <= 0) {
                    error = 1;
                    $('#email_newsletter_validate').removeClass("success");
                    $('#email_newsletter_validate').addClass("error");
                    $('#submit-newsletter').next('div').text('This field is required.');
                } else if (!validateEmail(email)) {
                    error = 1;
                    $('#email_newsletter_validate').removeClass("success");
                    $('#email_newsletter_validate').addClass("error");
                    $('#submit-newsletter').next('div').text('Please enter the valid email address.');
                } else {
                    error = 0;
                    $('#submit-newsletter').next('div').text('');
                }

                if (error == 0) {
                    $.ajax({
                        url: '<?= base_url('subscribe-us') ?>',
                        type: 'POST',
                        data: {email: email, <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                    })
                            .done(function (result) {
                                if (result == 1) {
                                    //save succesfully
                                    $('#email-text').val('');
                                    $('#email_newsletter_validate').removeClass("error");
                                    $('#email_newsletter_validate').addClass("success");
                                    $('#submit-newsletter').next('div').text('Thank you for subscribing with us!');
                                } else if (result == 5) {
                                    //save succesfully
                                    $('#email-text').val('');
                                    $('#email_newsletter_validate').removeClass("success");
                                    $('#email_newsletter_validate').addClass("error");
                                    $('#submit-newsletter').next('div').text('You are already subscribed!');
                                } else {
                                    error = 1;
                                }
                                console.log("success");
                            })
                            .fail(function () {
                                console.log("error");
                            })
                            .always(function () {
                                console.log("complete");
                            });
                }
            });
        </script>
    <?php } ?>
    <section class="footr-mid-sec" id="pag1">
        <div class="container">
            <div class="row">
                <div class="footr-wrp-2">
                    <div class="col-md-3 col-sm-4">
                        <div class="ftr-comp-tit">
                            <h2>Company</h2>
                        </div>
                        <div class="fotr-lnk-1">
                            <ul>
                                <?php
                                $footermenu = footer_about_menu();
                                foreach ($footermenu as $items) {
                                    ?>
                                    <li>
                                        <a href="<?= base_url() ?><?= $items['sub_pg_url'] ?>"><?= $items['sub_pg_name'] ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="ftr-comp-tit">
                            <h2>Help</h2>
                        </div>
                        <div class="fotr-lnk-1">
                            <ul>
                                <!-- <li>
                                    <a href="">How it Works</a>
                                </li> -->
                                <li>
                                    <a href="<?= base_url() ?><?= get_page_url(6, 'tbl_pages_sub', 'sub_id')->sub_pg_url; ?>">Help Center</a>
                                </li>
                                <li>
                                    <a href="<?= base_url() ?><?= get_page_url(10, 'tbl_pages_sub', 'sub_id')->sub_pg_url; ?>/<?= get_page_url(7, 'tbl_pages_listing', 'listing_id')->listing_url; ?>">Getting Started</a>
                                </li>
                                <li>
                                    <a href="<?= base_url() ?><?= get_page_url(9, 'tbl_pages_sub', 'sub_id')->sub_pg_url; ?>"><?= get_page_url(9, 'tbl_pages_sub', 'sub_id')->sub_pg_title; ?></a>
                                </li>
                                <li>
                                    <a href="<?= base_url() ?>blog">News & Advice</a><!-- Blog -->
                                </li>
                                <li>
                                    <a href="<?=base_url()?>news-advice">Forum</a><!-- News & Advice -->
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="ftr-comp-tit emptyhdr"></div>
                        <div class="fotr-lnk-1 pad-tp-ft">
                            <ul>
                                <?php
                                $footer_parent_menu = footer_parent_menu();
                                foreach ($footer_parent_menu as $items) {
                                    ?>
                                    <li>
                                        <a href="<?= base_url() ?><?= $items['pg_url'] ?>"><?= $items['pg_title'] ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 pad-lft-0 pad-rgt-0 connectSec">
                        <div class="ftr-comp-tit">
                            <h2>Connect With Us</h2>
                        </div>
                        <div class="fotr-lnk-2">
                            <ul>
                                <li>
                                    <a href="<?php echo get_social_media_links()->facebook; ?>" class="footr-fb" target="_blank">Facebook</a>
                                </li>
                                <li class="mrg-rgt-0">
                                    <a href="<?php echo get_social_media_links()->twitter; ?>" class="footr-twt" target="_blank">Twitter</a>
                                </li>
                                <li>
                                    <a href="<?php echo get_social_media_links()->pinterest; ?>" class="footr-pin" target="_blank">Pinterest</a>
                                </li>
                                <li class="mrg-rgt-0">
                                    <a href="<?php echo get_social_media_links()->linkedin; ?>" class="footr-lnklnd" target="_blank">Linkedin</a>
                                </li>
                            </ul>
                        </div>
                        <div class="ftr-lnk-3">	
                            <img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/cc-badges-ppmcvdam.png" alt="Credit Card Badges">
                        </div>
                    </div>
                </div>        
            </div>    
        </div>
    </section>
    <section class="footr-bottm-sec">
        <div class="container">
            <div class="row">
                <div class="footr-wrp">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="fot-cpy">
                            <p>
                                &copy; Copyright <?php echo get_social_media_links()->name; ?> <?= Date('Y'); ?>. All Rights Reserved.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div id="google_translate_element" class="fot-selct"></div>
                        <script type="text/javascript">
                            function googleTranslateElementInit() {
                                new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
                            }
                        </script>
                        <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        <!-- <div class="fot-selct">
                            <select>
                                <option selected hidden="hidden">Select language</option>
                                <option>English</option>
                                <option>English</option>
                            </select>
                        </div> -->
                    </div>
                </div>  
            </div>    
        </div>
    </section>
</footer>
<!--footer ends here-->



<!--Bootstrap and custom js start-->




<script src="<?php echo base_url(); ?>js/additional-methods.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>front_resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>front_resources/js/bootstrap-dropdownhover.min.js"></script>
<!--Bootstrap and custom js start-->
<!-- bxSlider Javascript file -->
<script src="<?= base_url() ?>front_resources/js/jquery.bxslider.min.js" ></script>
<!-- slick slider Javascript file -->
<script src="<?= base_url() ?>front_resources/js/slick.min.js"></script>
<script type="text/javascript">
        $(document).ready(function () {
            $('.slider-for').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                arrows: true,
              responsive: [
                {
                  breakpoint: 992,
                  settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                  }
                },
                {
                  breakpoint: 576,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                  }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
              ]
            });
        });
</script>
<!-- Slider Ends -->

<script type="text/javascript">
    $(document).ready(function () {
        $('.bxslider').bxSlider({
            auto: ($('.bxslider').children().length < 2) ? false : true,
            controls: ($('.bxslider').children().length < 2) ? false : true,
            pause: 5000,
            mode: 'fade',
            adaptiveHeight: true,
            autoStart: true
        });
    });

</script>
<script>
    jQuery(document).on('click', '.mega-dropdown', function (e) {
        e.stopPropagation();
    });
    $(document).ready(function () {
        $('.secondstep').click(function () {
            /*if($('#myModal2').hasClass('in')){
             alert('hello');
             }*/
            // Changed
            //$('#myModal').hide();
            //$('body').css({"padding": "0px"});
        });
        /*
         $('.secondstep').click(function(){
         alert('hello');
         $('body').addClass('modal-open');
         });
         */
    });
    $(document).ready(function () {
        $('.thrd-step').click(function () {
            // Changed
            //$('#myModal').hide();
            //$('body').css({"padding": "0px"});
        });

    });
</script> 

<script>
    $(document).ready(function () {
        $(function validate() {
            // body...
            var rules = {
                rules: {
                    email_newsletter: {
                        maxlength: 50,
                        required: true,
                        email: true
                    }
                },
                errorPlacement: function (error, element) {
                    var name = $(element).attr("name");
                    error.appendTo($("#" + name + "_validate"));
                },
            };

            $('#newsletter_form').validate(rules);
        });
    });

    function validateEmail(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test(email);
    }
    $(document).ready(function () {
        $(document).on('click', '.regbtn1', function () {
            var email = $('#regEmail1').val();
            var pass = $('#regPass1').val();
            var error = 0;
            if (email.length <= 0) {
                error = 1;
                $('#regEmail1').next('div').text('This field is required.');
            } else if (!validateEmail(email)) {
                error = 1;
                $('#regEmail1').next('div').text('Please enter the valid email address.');
            } else {
                $('#regEmail1').next('div').text('');
            }
            if (pass.length <= 0) {
                error = 1;
                $('#regPass1').next('div').text('This field is required.');
            } else if (pass.length < 6) {
                error = 1;
                $('#regPass1').next('div').text('Password must be at least 6 characters.');
            } else {

                $('#regPass1').next('div').text('');
            }

            if (error == 0) {
                $.ajax({
                    url: '<?= base_url('check-register-email') ?>',
                    type: 'POST',
                    async: false,
                    data: {email: email,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                })
                        .done(function (result) {
                            if (result == 1) {
                                //found
                                $('#regEmail1').next('div').text('This email address already exist.');
                                error = 1;
                            } else {
                                $('#regEmail1').next('div').text('');
                                $('#regPass1').next('div').text('');
                                $('#regEmail1').val('');
                                $('#regPass1').val('');
                                $('#regEmail2').val(email);
                                $('#regPass2').val(pass);
                                $('#reg2page').click();

                            }
                            console.log("success");
                        })
                        .fail(function () {
                            console.log("error");
                        })
                        .always(function () {
                            console.log("complete");
                        });
            }
        });

        $(document).on('change', '#regInterest1', function () {
            var inst = $(this).val();
            if (inst == 'Other') {
                $('#other_inst1').show();
                $('#otherInst1').val('');
            } else {
                $('#other_inst1').hide();
                $('#otherInst1').val('');
            }
        });
        $(document).on('change', '#regInterest2', function () {
            var inst = $(this).val();
            if (inst == 'Other') {
                $('#other_inst2').show();
                $('#otherInst2').val('');
            } else {
                $('#other_inst2').hide();
                $('#otherInst2').val('');
            }
        });

        $(document).on('click', '#regbtn2', function () {
            var error = 0;
            var email = $('#regEmail2').val();
            var password = $('#regPass2').val();
            var fname = $('#fname').val();
            var lname = $('#lname').val();
            var zipcode = $('#regZip').val();
            var age = $('#regAge').val();
            var interest1 = $('#regInterest1').val();
            var interest2 = $('#regInterest2').val();
            var small_bus = $('#is_small_bus').val();
            var gender = $('.regGender:checked').val();
            var otherInst1 = $('#otherInst1').val();
            var otherInst2 = $('#otherInst2').val();

            if (email.length <= 0) {
                error = 1;
                $('#regEmail2').next('div').text('This field is required.');
            } else if (!validateEmail(email)) {
                error = 1;
                $('#regEmail2').next('div').text('Please enter the valid email address.');
            } else {
                $('#regEmail2').next('div').text('');
            }

            if (password.length <= 0) {
                error = 1;
                $('#regPass2').next('div').text('This field is required.');
            } else if (password.length < 6) {
                error = 1;
                $('#regPass2').next('div').text('Password must be at least 6 characters.');
            } else {
                $('#regPass2').next('div').text('');
            }

            if (fname.length <= 0) {
                error = 1;
                $('#fname').next('div').text('This field is required.');
            } else {
                $('#fname').next('div').text('');
            }

            if (lname.length <= 0) {
                error = 1;
                $('#lname').next('div').text('This field is required.');
            } else {
                $('#lname').next('div').text('');
            }

            if (zipcode.length <= 0) {
                error = 1;
                $('#regZip').next('div').text('This field is required.');
            } else{
                $('#regZip').next('div').text('');
            }

            if (!age) {
                console.log('age error');
                error = 1;
                $('#regAge').next('div').text('This field is required.');
            } else {
                $('#regAge').next('div').text('');
            }

            if (!small_bus) {
                error = 1;
                $('#is_small_bus').next('div').text('This field is required.');
            } else {
                $('#is_small_bus').next('div').text('');
            }

            if (!interest1) {
                error = 1;
                $('#regInterest1').next('div').text('This field is required.');
            } else if (interest1 == 'Other' && otherInst1.length <= 0) {
                error = 1;
                $('#regotherInterest1').next('div').text('This field is required.');
            } else {
                $('#regInterest1').next('div').text('');
                $('#regotherInterest1').next('div').text('');
            }

            if (!interest2) {
                error = 1;
                $('#regInterest2').next('div').text('This field is required.');
            } else if (interest1 == 'Other' && otherInst2.length <= 0) {
                error = 1;
                $('#regotherInterest2').next('div').text('This field is required.');
            } else {
                $('#regInterest2').next('div').text('');
                $('#regotherInterest2').next('div').text('');
            }

            if (error == 0) {
                $.ajax({
                    url: '<?= base_url('check-register-email') ?>',
                    type: 'POST',
                    async: false,
                    data: {email: email,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                })
                        .done(function (result) {
                            if (result == 1) {
                                //found
                                $('#regEmail2').next('div').text('This email address already exist.');
                                error = 1;
                            } else {
                                $('#regEmail2').next('div').text('');
                            }
                            console.log("success");
                        })
                        .fail(function () {
                            console.log("error");
                        })
                        .always(function () {
                            console.log("complete");
                        });

                if (error == 0) {
                    $.ajax({
                        url: '<?= base_url('register-data') ?>',
                        type: 'POST',
                        async: false,
                        data: {fname: fname, lname: lname, age: age, email: email, password: password, zipcode: zipcode, interest1: interest1, interest2: interest2,
                            small_bus: small_bus, gender: gender, otherInst1: otherInst1, otherInst2: otherInst2,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                    })
                    .done(function (result) {
                        if (result == 1) {
                            $('.close').click();
                            $('.regthank').click();
                        }else if(result==0){
                            $('.close').click();
                            $.ajax({
                                type: 'POST',
                                url: '<?= base_url('login-user') ?>',
                                async: false,
                                data: {email: email, password: password,check: 'false',<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                                success: function (res) {
                                    console.log('Success');
                                    if(res == 1){
                                        window.location.href = '<?= base_url('become-a-seller') ?>';
                                    }                                    
                                },
                                error: function () {
                                    console.log('Network error.');
                                }
                            });                                    
                        }
                        console.log("success");
                    })
                    .fail(function () {
                        console.log("error");
                    })
                    .always(function () {
                        console.log("complete");
                    });
                }
            }
        });

        $(document).on('click', '.loginbtn', function () {
            //alert('sdsd');
            var error = 0;
            var email = $('#loginEmail').val();
            var password = $('#loginPassword').val();
            var remember_me = document.getElementById("remember_me").checked;
            if (email.length <= 0) {
                error = 1;
                $('#loginEmail').next('div').text('This field is required.');
            } else if (!validateEmail(email)) {
                error = 1;
                $('#loginEmail').next('div').text('Please enter the valid email address.');
            } else {
                $('#loginEmail').next('div').text('');
            }

            if (password.length <= 0) {
                error = 1;
                $('.loginPasswordError').text('This field is required.');
            } else if (password.length < 6) {
                error = 1;
                $('.loginPasswordError').text('Password must be at least 6 characters.');
            } else {
                $('.loginPasswordError').text('');
            }

            if (error == 0) {
                //alert(1);
                $.ajax({
                    url: '<?= base_url('login-user') ?>',
                    type: 'POST',
                    async: false,
                    data: {email: email, password: password,check: remember_me,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                    dataType: "json",
                })
                .done(function (result) {
                    if (result['status'] == 1) {
                        //save succesfully
                        if (result['tier_check']  == 1) {
                            window.location.href = "<?= base_url() ?>/edit-tier";
                        }
                        <?php if ($this->uri->segment(1) != "cart") { ?>
                        window.location.href = "<?= base_url() ?>";
                        <?php } else { ?>
                            window.location.href = "<?= base_url() ?>cart-shipping";
                        <?php } ?>
                    } else {
                        error = 1;
                        $('#loginEmail').next('div').text('Invalid email or password.');
                    }
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
            }
        });
    });

    $(document).ready(function () {
        $('.secondstep').click(function () {
            $('#myModal').hide();
            //$('body').css({"padding": "0px"});
        });

        /*
         if($('#myModal2').hasClass('in')){
         alert('hello');
         }
         $('.secondstep').click(function(){
         alert('hello');
         $('body').addClass('modal-open');
         });
         */

        $('.thrd-step').click(function () {
            $('#myModal').hide();
            //$('body').css({"padding": "0px"});
        });

    });

</script>
<script>
var toppag=$("#top-pag1");
var pag=$("#pag1");

toppag.css({position:"fixed",bottom:"0px"});


$(window).scroll(function () {
    var scroll=$(this).scrollTop()+$(this).height();
 
    pag.each(function(i,n){
  
  if(scroll < ($(this).offset().top + toppag.eq(i).height()))  {
            toppag.eq(i).css({'position':'fixed',"top":""});
        }
  
        if(scroll > $(this).offset().top) {
            toppag.eq(i).css({'position':'relative'});
            if(i>0){
                 toppag.eq(i).css({'top':i*-50});
            }
        }
    }); 
});

$(document).on('keyup','.login-enter',function(e){
    if(e.which == 13) {
        $('.loginbtn').click();
    }
});

$(document).on('keyup','.register-1',function(e){
    console.log('haha-1');
    if(e.which == 13) {
        $('.regbtn1').click();
    }
});

$(document).on('keyup','.register-2',function(e){
    console.log('haha-1');
    if(e.which == 13) {
        $('#regbtn2').click();
    }
});

</script>

</body>
</html>