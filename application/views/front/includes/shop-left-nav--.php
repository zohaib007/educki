<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDswE9MLqvjoCNwwl2vkideo5i6pYKHmYs&libraries=places"></script>

<div class="shop-aside">
    <div class="shop-aside-tit">
        <h4>Shop By</h4>
    </div>
    <div class="shop-accord">
        <?php // if($segment4 != ''){ ?>
        <button class="accordion active">Category</button>
        <div class="panel" id="accordion" role="tablist" aria-multiselectable="true" style="display: block;">
            <?php $active_class = '';$color_class = '';
            if (count(getMainCategories() > 0)) {
                foreach (getMainCategories() as $cat) {
                    $cat_url = $cat['cat_url'];
                    ?>
                    <div class="catrg-chkbx">
                        <div class="catrg-chkbx-lft <?php
                        if ($this->uri->segment(1) == $cat_url) {
                            echo "active";
                            $class_active = 'collapse in';
                            $color_class = '';
                        }else{
                            $class_active = 'collapse';
                            $color_class = 'collapsed';
                        }
                        ?>">
                            <span class="new-arrow-2 <?=$color_class?>" data-toggle="collapse" data-target="#demo_<?= $cat['cat_id'] ?>"></span>
                            <a href="<?= base_url() ?><?= $cat_url ?>"
                               class="<?php
                               if ($this->uri->segment(1) == $cat_url) {
                                   echo "active";
                                   $class_active = 'collapse in';
                               }else{
                                $class_active = 'collapse';
                               }
                               ?>"
                               >
                                   <?= $cat['cat_name']; ?>
                            </a>
                        </div>
                        <?php $cat_prod = get_main_count($cat['cat_id']); ?>
                        <div class="catrg-chkbx-rgt">
                            <span class="nm-flt">(<?=$cat_prod?>)</span>
                        </div>
                        <?php
                        $sub_category = getSubCategories($cat['cat_id']);
                        if (count($sub_category) > 0) { ?>
                         <div class="crg-sub <?=$class_active?>" id="demo_<?= $cat['cat_id'] ?>"><!--collapse in-->
                         <?php
                            foreach ($sub_category as $sub_cat) {
                                $sub_cat_url = $cat['cat_url'] . "/" . $sub_cat['cat_url'];
                                ?>
                                <?php $sub_sub_category = getSubSubCategories($sub_cat['cat_id']); ?>
                               		<div class="cat-menurp">
                                        
                                        <div class="catrg-chkbx-lft <?php if ($this->uri->segment(2) == $sub_cat['cat_url']) { echo "active"; }
                                        ?>">
                                            <?php
                                               if ($this->uri->segment(2) == $sub_cat['cat_url']) {
                                                    $color_class = '';
                                               }else{
                                                    $color_class = 'collapsed';
                                               }
                                               ?>
                                            <span class="new-arrow-2  <?=$color_class?> <?=( ($sub_sub_category<1)?'last-step':'')?>" data-toggle="collapse" data-target="#demo_<?= $sub_cat['cat_id'] ?>"></span>
                                            <a href="<?= base_url() ?><?= $sub_cat_url ?>"
                                               class="<?php
                                               if ($this->uri->segment(2) == $sub_cat['cat_url']) {
                                                   echo "active";
                                                   $class_active = 'collapse in';
                                               }else{
                                                $class_active = 'collapse';
                                               }
                                               ?>"
                                               >
                                                   <?= $sub_cat['cat_name'] ?>
                                            </a>
                                        </div>
                                        <?php $sub_cat_prod = get_sub_count($sub_cat['cat_id']); ?>
                                        <div class="catrg-chkbx-rgt">
                                            <span class="nm-flt">(<?= $sub_cat_prod?>)</span>
                                        </div>
                                        
									</div>
                                    <?php
                                    $sub_sub_category = getSubSubCategories($sub_cat['cat_id']);
                                    if ($sub_sub_category > 0) {
                                        ?>
                                        <div id="demo_<?= $sub_cat['cat_id'] ?>" class="cat-sublist <?=$class_active?>"><!--collapse in-->
                                            <?php
                                            foreach ($sub_sub_category as $sub_sub_cat) {
                                                $sub_sub_cat_url = $cat['cat_url'] . "/" . $sub_cat['cat_url'] . "/" . $sub_sub_cat['cat_url']
                                                ?>

                                                <div class="crgt-sub-2" >
                                                    <div class="catrg-chkbx-lft <?php
                                                    if ($this->uri->segment(3) == $sub_sub_cat['cat_url']) {
                                                        echo "active";
                                                    }
                                                    ?>">
                                                        <span class="new-arrow-2 collapsed last-step"></span>
                                                        <a href="<?= base_url() ?><?= $sub_sub_cat_url ?>"
                                                           class="<?php
                                                           if ($this->uri->segment(3) == $sub_sub_cat['cat_url']) {
                                                               echo "active";
                                                           }
                                                           ?>"
                                                           >
                                                               <?= $sub_sub_cat['cat_name'] ?>
                                                        </a>
                                                    </div>
                                                    <?php $sub_sub_cat_prod = getproductcount($sub_sub_cat['cat_id']); ?>
                                                    <div class="catrg-chkbx-rgt">
                                                        <span class="nm-flt">(<?=$sub_sub_cat_prod->prod_count?>)</span>
                                                    </div>
                                                </div>

                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                
                            <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
        <?php // }   ?>
         <script>
        var min = "0";
        var max = "0";
        </script>
        <?php
        if ($category_details['product_price']->product_count > 1) {
            if ($category_details['product_price']->price_minimum != $category_details['product_price']->price_maximum) {
                ?>
                <button class="accordion">Price</button>
                <div class="panel pad-tp-0">
                    <div class="rangeSlidern">
                        <!-- New Slide Range Starts -->
                        <div class="customRange">
                            <div class="startValue">0</div>
                            <input id="ex2" type="text" class="span2" value="" data-slider-step="5"  /> 
                            <div class="endValue">0</div>
                        </div>
                        <!-- New Slide Range Ends -->
                       
                    </div>
                </div>
                <script>
                var min = parseInt("<?= $category_details['product_price']->price_minimum ?>");
                var max = parseInt("<?= $category_details['product_price']->price_maximum ?>");
                </script>
                <?php echo form_open('', array('name' => 'pricerangeForm', 'method' => 'get', 'id' => 'pricerangeForm')); ?>
                <input onchange="submit('pricerangeForm')" type="hidden" id="oneRange" name="rangeOne" value="<?=(int)$category_details['product_price']->price_minimum ?>" min="<?=(int)$category_details['product_price']->price_minimum ?>" max="<?=(int)$category_details['product_price']->price_maximum ?>" />
                <input onchange="submit('pricerangeForm')" type="hidden" id="twoRange" name="rangeTwo" value="<?=(int)$category_details['product_price']->price_maximum ?>" min="<?=(int)$category_details['product_price']->price_minimum ?>" max="<?=(int)$category_details['product_price']->price_maximum ?>" />
                <?php echo form_close(); ?>
                <!-- <?php if(@$_GET['rangeOne'] != ''){ ?>
                <script>
                var min = parseInt("<?= @$_GET['rangeOne']?>");
                var max = parseInt("<?= @$_GET['rangeTwo'] ?>");
                </script>
                <?php } ?> -->

            <?php } ?>
        <?php } ?>

        <?php if(!empty($pagedata)){?>
        <button class="accordion">Price</button>
        <div class="panel pad-tp-0">
            <div class="rangeSlidern">
                <!-- New Slide Range Starts -->
                <div class="customRange">
                    <div class="startValue">0</div>
                    <input id="ex2" type="text" class="span2" value="" data-slider-step="5"  /> 
                    <div class="endValue">0</div>
                </div>
                <!-- New Slide Range Ends -->
               
            </div>
        </div>
        <script>
        var min = parseInt("<?= $prod_price->MIN ?>");
        var max = parseInt("<?= $prod_price->MAX ?>");
        </script>
        <?php echo form_open('', array('name' => 'pricerangeForm', 'method' => 'get', 'id' => 'pricerangeForm')); ?>
        <input onchange="submit('pricerangeForm')" type="hidden" id="oneRange" name="rangeOne" value="<?=(int)$category_details['product_price']->price_minimum ?>" min="<?=(int)$category_details['product_price']->price_minimum ?>" max="<?=(int)$category_details['product_price']->price_maximum ?>" />
        <input onchange="submit('pricerangeForm')" type="hidden" id="twoRange" name="rangeTwo" value="<?=(int)$category_details['product_price']->price_maximum ?>" min="<?=(int)$category_details['product_price']->price_minimum ?>" max="<?=(int)$category_details['product_price']->price_maximum ?>" />
        <?php echo form_close(); ?>
        <?php }?>
        <?php if ($segment3 != '' && !is_numeric($lastPart)) { ?>

            <?php
            $count = 4;
            foreach ($category_filters as $filter) {
                ?>

                <?php if ($filter->cat_filter_is_conditional == 0) { ?>
                    <button class="accordion" id="<?= $filter->filter_slug ?>"><?= $filter->filter_title ?></button>
                    <div class="panel">
                        <?php foreach ($filter->cat_filter_values as $fil) { ?>
                            <div class="catrg-chkbx">
                                <div class="catrg-chkbx-lft">
                                    <input  class="filter_unique" type="checkbox" class="filter_unique" id="c<?= $count ?>" name="<?= $filter->filter_slug ?>" value="<?= $fil->value ?>" />
                                    <label for="c<?= $count ?>"><span></span><p><?= $fil->value ?></p></label>
                                </div>

                                <div class="catrg-chkbx-rgt">
                                    <span class="nm-flt">(<?= $fil->count ?>)</span>
                                </div>
                            </div>

                            <?php
                            $count++;
                        }
                        ?>
                    </div>
                <?php } else { ?>
                    <button class="accordion" id="<?= $filter->filter_slug ?>"><?= $filter->filter_title ?></button>
                    <div class="panel">
                        <?php foreach ($filter->cat_filter_values as $fil) { ?>
                            <div class="catrg-chkbx">
                                <div class="catrg-chkbx-lft">
                                    <input class="filter_unique" type="checkbox" id="c<?= $count ?>" name="<?= $filter->filter_slug ?>" value="<?= $fil->filter_slug ?>"/>
                                    <label for="c<?= $count ?>"><span></span><p><?= $fil->filter_title ?></p></label>
                                </div>

                                <div class="catrg-chkbx-rgt">
                                    <span class="nm-flt">(<?= $fil->size ?>)</span>
                                </div>
                                <div class="crg-sub">

                                    <?php
                                    $count++;
                                    foreach ($fil->filter_detail as $detail) {
                                        ?>

                                        <div class="catrg-chkbx-lft">
                                            <input type="checkbox" class="filter_unique" data-slug="<?= $filter->filter_slug ?>" id="c<?= $count ?>" name="<?= $fil->filter_slug ?>" value="<?= $detail->value ?>" />
                                            <label for="c<?= $count ?>"><span></span><p><?= $detail->value ?></p></label>
                                        </div>

                                        <div class="catrg-chkbx-rgt">
                                            <span class="nm-flt">(<?= $detail->count ?>)</span>
                                        </div>

                                        <?php
                                        $count++;
                                    }
                                    ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </div>
    <?php if(!empty($pagedata)){?>
    <a href="<?=base_url()?>store/<?=$pagedata->store_url?>" class="rest-btn-2" id="reset" style="margin-top: 10px;">Reset</a>
    <?php }?>
    <!-- <?=@$_GET['radius']==20?'selected':''?>  -->
    <!-- <?=@$_GET['location']?> -->
    <!-- <?=@$_GET['location']?> -->

    <?php if(!isset($no_map)){?>
        <div class="loct-sec">
            <?php echo form_open("", array("id" => "loc")) ?>
            <h6>Location</h6>
            <input type="text"  value="" name='pcpcinput' id="pac-input" placeholder="Enter City, State or Zip"/>
            <input type="hidden" value="" id="location" name="location" />
            <select name="radius" id="radius">
                <option value="5">Distance within 5 mi</option>
                <option value="10">Distance within 10 mi</option>
                <option value="20">Distance within 20 mi</option>
                <option value="50">Distance within 50 mi</option>
                <option value="100">Distance within 100 mi</option>
            </select>
            <input type="button" id="locsubmit" value="Search" class="rest-btn"/>
            <a href="javascript:void" class="rest-btn" id="reset" style="padding: 5.5px 19.20px;text-decoration: none;">Reset</a>
            <?php echo form_close(); ?>            
        </div>        
    <?php }?>
    <div class="shp-mem-map" id="googleMap" style="width:250px; height:250px;"></div>
    <!-- <script>
      function initMap() {
        var uluru = {lat: <?=$pagedata->store_latitude?>, lng: <?=$pagedata->store_longitude?>};
        var map = new google.maps.Map(document.getElementById('googleMap'), {
          zoom: 4,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script> -->
</div>
<script type="text/javascript">
    $('#reset').on('click',function(){
        //console.log(location.href.split("?")[0]);
        window.location = location.href.split("?")[0];
    });    
</script>
<script>
    var place;
    function initialize() {
        var input = document.getElementById('pac-input');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function () {

            place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                //map.fitBounds(place.geometry.viewport);
            } else {
                // map.setCenter(place.geometry.location);
                // map.setZoom(17);  // Why 17? Because it looks good.
            }
            //   marker.setPosition(place.geometry.location);
            //  marker.setVisible(true);

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }


        });

    }
    google.maps.event.addDomListener(window, 'load', initialize);
    $(document).on('submit', '#loc', function () {
    });
    $('#locsubmit').click(function (e) {
       
        if (place.geometry.location) {

            $('#location').val(place.geometry.location.lat() + '|' + place.geometry.location.lng());

            
            var completeurl = window.location.href;
            var _url = window.location.href.split('?')[0];
            var _querystring = window.location.href.split('?')[1];
            var _newUrl = _url;
            // console.log(completeurl);
            // console.log(_url);
            // console.log(_querystring);
            var location = $('#location').val();
            var radius = $('#radius').val();
            if(_querystring == undefined){

                _newUrl = _url+"?q=1&location="+location+"&radius="+radius;
            }else{
                var getLocParm = window.location.href.split('&location')[1];
                if(getLocParm == undefined){
                    _newUrl = completeurl+"&location="+location+"&radius="+radius;
                }else{
                    console.log(getLocParm);
                    _newUrl = updateQueryStringParameter(completeurl, 'location', location);
                    _newUrl = updateQueryStringParameter(_newUrl, 'radius', radius);
                    
                }
            }
            console.log(_newUrl);
            $('#loc').attr('action', _newUrl);
            $('#loc').submit();
        }



    });



</script>


<script>

    // var urlParams = new URLSearchParams(window.location.search);
    var completeurl = window.location.href;
    var url = window.location.href.split('?')[0];
    var querystring = window.location.href.split('?')[1];

    $('.filter_unique').change(function () {

        if (this.checked) {
            console.log(querystring);
            if (querystring === undefined) {
                querystring = ""
            }


            var q = url_query('q');
            
            console.log('qnew');
            console.log(q);

            if (q) {

            } else {

                if (completeurl.indexOf('?') > -1){
                    completeurl = completeurl + "&q=1";
                }else{
                    completeurl = completeurl + "?q=1";
                }
            }

            var url_param = url_query(this.name);


            if (url_param) {
                querystring = updateQueryStringParameter(completeurl, this.name, url_param + "," + this.value);
                console.log("yes"+querystring); // 
                querystring = removeURLParameter(querystring, 'page');
            } else {
                console.log("Earlier = " + url + "?" + querystring);
                querystring = updateQueryStringParameter(completeurl, this.name, this.value);
                console.log("After = " + querystring);
            }
            window.location = querystring;


        } else {

            var url_param = url_query(this.name);
            console.log("1"+url_param);


            if (url_param) {
                var str = url_param.split(',');

                console.log("str");
                console.log(str);
                if (str.length > 1) {

                    var querystr = "";
                    var flag = true;
                    for (var j = 0; j < str.length; j++) {
                        console.log(str[j] + "==" + this.value);
                        if (decodeURIComponent(str[j]) != this.value) {

                            if (flag) {

                                querystr = querystr + str[j];
                                flag = false;

                            } else {
                                querystr = querystr + "," + str[j];
                            }
                        }
                    }

                    querystring = updateQueryStringParameter(completeurl, this.name, querystr);


                } else {
                    console.log("here");
                    querystring = removeURLParameter(completeurl, this.name);
                }
                // deleteQueryStringParameter(querystring, this.name)
            }
            console.log(querystring);
            if (querystring == undefined) {
                window.location = url;

            } else {
                //  window.location= url + "?" + querystring;
                window.location = querystring;

            }


            // console.log(querystring);
        }
    });


    function url_query(query) {
        query = query.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var expr = "[\\?&]" + query + "=([^&#]*)";
        var regex = new RegExp(expr);
        var results = regex.exec(window.location.href);
        if (results !== null) {
            return results[1];
        } else {
            return false;
        }
    }
    function updateQueryStringParameter(uri, key, value) {
        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";
        if (uri.match(re)) {
            return uri.replace(re, '$1' + key + "=" + value + '$2');
        }
        else {
            return uri + separator + key + "=" + value;
        }
    }
    /* function deleteQueryStringParameter(uri, key) {
     var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
     console.log(re);
     var separator = uri.indexOf('?') !== -1 ? "&" : "?";
     console.log("separator = " + separator);
     if (uri.match(re)) {
     return uri.replace(re, '');
     }
     
     } */

    function removeURLParameter(url, parameter) {
        //prefer to use l.search if you have a location/link object
        var urlparts = url.split('?');
        if (urlparts.length >= 2) {

            var prefix = encodeURIComponent(parameter) + '=';
            var pars = urlparts[1].split(/[&;]/g);

            //reverse iteration as may be destructive
            for (var i = pars.length; i-- > 0; ) {
                //idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                    pars.splice(i, 1);
                }
            }

            url = urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : "");
            return url;
        } else {
            return url;
        }
    }
    function parseQueryString(queryString) {
        var params = {}, queries, temp, i, l;
        // Split into key/value pairs
        queries = queryString.split("&");
        // Convert the array of strings into an object
        for (i = 0, l = queries.length; i < l; i++) {
            temp = queries[i].split('=');
            params[temp[0]] = temp[1];
        }
        return params;
    }

    var queryValues;

    if (querystring) {
        console.log(querystring);
        queryValues = parseQueryString(querystring);
    }

    // var queryValues = parseQueryString( querystring );
    $(document).ready(function () {
        for (a in queryValues) {
            if (a != 'q') {



                var array = queryValues[a].split(",");

                if ($("#" + a).length) {

                    if (!$("#" + a).hasClass("active")) {
                        $("#" + a).click();
                    }

                } else {

                    var val = $("input[value='" + decodeURIComponent(array[0]) + "'][name='" + a + "']").data('slug');
                    console.log(val);
                    if (!$("#" + val).hasClass("active")) {
                        $("#" + val).click();
                    }
                }


                for (i in array) {
                    console.log(decodeURIComponent(array[i]));


                    $("input[value='" + decodeURIComponent(array[i]) + "'][name='" + a + "']").prop("checked", true);
                }


                //   console.log("input[value='" +decodeURIComponent(queryValues[a])  + "'][name='" + a + "']");



            }
        }
    });
</script>

<!-- Range Slider Starts -->
<link href="<?=base_url()?>front_resources/js/range-files/jquerysctipttop.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url()?>front_resources/js/range-files/bootstrap-slider.css" rel="stylesheet" type="text/css" />
<script src="<?=base_url()?>front_resources/js/range-files/bootstrap-slider.js" type="application/javascript"></script>
<script type="application/javascript">

$(document).ready(function(){


    $minRange = parseInt(min);
    $maxRange = parseInt(max);
    $stepValue = 5;
    if ($('.startValue').length){
    	$('.startValue').html($minRange);
    }
    if ($('.endValue').length){
    	$('.endValue').html($maxRange);
    }
    $("#ex2").bootstrapSlider({ id: "sliderRange", min: $minRange, max: $maxRange, value: $stepValue, range: true, value: [$minRange, $maxRange] });
    $("#ex2").on("slide", function(slideEvt) {
    	$(".startValue").text(slideEvt.value[0]);
    	$(".endValue").text(slideEvt.value[1]);

        $("#oneRange").val(slideEvt.value[0]);
        $("#twoRange").val(slideEvt.value[1]);
    });
    
    $("#sliderRange").on("click", function() {
    	$(".startValue").text($("#ex2").val().split(",")[0]);
    	$(".endValue").text($("#ex2").val().split(",")[1]);

        $("#oneRange").val($("#ex2").val().split(",")[0]);
        $("#twoRange").val($("#ex2").val().split(",")[1]);
    });
    $("#sliderRange").on("mousedown", function() {
    	$(".startValue").text($("#ex2").val().split(",")[0]);
    	$(".endValue").text($("#ex2").val().split(",")[1]);

        $("#oneRange").val($("#ex2").val().split(",")[0]);
        $("#twoRange").val($("#ex2").val().split(",")[1]);
    });

    $("#sliderRange").on("mouseup", function() {
        $('#pricerangeForm').submit();
    });
});
</script>
<!-- Range Slider Ends -->