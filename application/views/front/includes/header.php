<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo (isset($title) ? $title : '') ; ?></title>

<meta name="title" content="<?php echo (isset($meta_title) ? strip_tags($meta_title) : '');?>" />
<meta name="description" content="<?php echo (isset($meta_description) ? strip_tags($meta_description) : '');?>" />
<meta name="keywords" content="<?php echo (isset($meta_keyword) ? strip_tags($meta_keyword) : '');?>" />
<meta name="image" content="<?=@$share_image?>" />

<meta name="twitter:title" content="<?=@$share_title?>">
<meta name="twitter:site" content="eDucki Find your Community">
<meta name="twitter:image" content="<?=@$share_image?>">

<meta property="og:title" content="<?=@$share_title?>" />
<meta property="og:site_name" content="eDucki Find your Community" />
<meta property="og:image" content="<?=@$share_image?>" />
<meta property="og:description" content="<?php echo (isset($meta_description) ? strip_tags($meta_description) : '');?>" />

<link rel="shortcut icon" href="<?php echo base_url(); ?>front_resources/images/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo base_url(); ?>front_resources/images/favicon.ico" type="image/x-icon">
<link rel="icon" type="image/png" href="<?php echo base_url(); ?>front_resources/images/favicon.png">

<meta name="author" content="Dotlogics" />
<!-- Google Chrome Frame for IE -->
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge" /> -->
<!-- mobile meta (hooray!) -->
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" /> 
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no, shrink-to-fit=no" />



<!--Bootstrap and custom css start-->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/main.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/web-fonts/ufonts.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/main-dev2.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/animate.min.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/amazonmenu.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/bootstrap-dropdownhover.min.css" />

<!-- Responsive Files Starts -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/responsive-css.css" />
<!-- Responsive Files Ends -->

<!--Bootstrap and custom css end-->
<!-- bxSlider CSS file -->
<link href="<?=base_url()?>front_resources/css/jquery.bxslider.css" rel="stylesheet" />
<!-- Slider Ends -->

<!-- slick CSS file -->
<link href="<?=base_url()?>front_resources/css/slick.css" rel="stylesheet" />
<link href="<?=base_url()?>front_resources/css/slick-theme.css" rel="stylesheet" />
    <link href="<?=base_url()?>front_resources/css/formValidation.min.css" rel="stylesheet" />



    <!-- Slider Ends -->
<script type="text/javascript" src="<?=base_url()?>front_resources/js/jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script src="https://js.stripe.com/v2/"></script>
    <script src="https://checkout.stripe.com/checkout.js"></script>

    <!--Zebra date picker-->
    <script src="<?=base_url()?>front_resources/js/run_prettify.js"></script>
    <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>front_resources/js/jquery-ui.js"></script>
    <link rel="stylesheet" href="<?=base_url()?>front_resources/css/jquery-ui.css"/>
    <script type="text/javascript" src="<?=base_url()?>front_resources/js/amazonmenu.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>

    <script src="<?=base_url()?>front_resources/js/formValidation.min.js" ></script>
    <script>
  $( function() {
    $( "#datepicker" ).datepicker({
      showOn: "both",
      buttonImage: "<?=base_url('front_resources/')?>images/calendar.png",
      buttonImageOnly: true,
      buttonText: "Select date"
	  
    });
	$( "#datepicker2" ).datepicker({
      showOn: "both",
      buttonImage: "<?=base_url('front_resources/')?>images/calendar.png",
      buttonImageOnly: true,
      buttonText: "Select date"
    });
  } );
</script>



<link rel="Stylesheet" type="text/css" href="<?php echo base_url(); ?>front_resources/js/customscrollbar/jquery.mCustomScrollbar.css" />
<script src="<?php echo base_url(); ?>front_resources/js/customscrollbar/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
<script type="text/javascript">

/*
  $(function(){
    
  })(jQuery);
  */
$(window).on("load",function(){

  $(".nwhite-inn").mCustomScrollbar({
    theme:"dark-thick"
  });

  $(".second-nleveld").mCustomScrollbar({
    theme:"dark-thick"
  });

  $(".third-level").mCustomScrollbar({
    theme:"dark-thick"
  });

  $(".menu-nin").mCustomScrollbar({
    theme:"dark-thick"
  });

});

  /* New Menu Jquery Settings Starts */
  $(document).ready(function() {

      // Second Step
      $('.menu-colcenter .menu-nin').hide();
      $('.menu-colcenter .menu-nin.subfirst').show(); // shows very first sub menu

      // Third Step
      $('.menu-colright .menu-nin').hide();
      $('.menu-colright .menu-nin.subsubfirst').show(); // shows very first sub sub menu

      // Main Menu Hover Starts 
      $('.mainMenu').hover(function() {

        var splitMainClass = $(this).attr('class').split(' ')[1];
        var joinSubString = '.menu-colcenter '+'.'+splitMainClass
        //console.log(joinSubString);

        $('.menu-colcenter .menu-nin').hide();
        $('.menu-colright .menu-nin').hide();
        if($(joinSubString).length){
          $(joinSubString).show();
        }

    });
    // Main Menu Hover Ends 


      // Sub Menu Hover Starts 
      $('.subMenu').hover(function() {

        var splitSubClass = $(this).attr('class').split(' ')[1];
        var joinSubSubString = '.menu-colright '+'.'+splitSubClass
        //console.log(joinSubString);

        $('.menu-colright .menu-nin').hide();
        if($(joinSubSubString).length){
          $(joinSubSubString).show();
        }

    });
    // Sub Menu Hover Ends 


  });
  /* New Menu Jquery Settings Ends */
  
</script>


<!-- These two are all that's required for tidy-menu -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front_resources/js/mobileMenu/menu.css"/>
<!-- <script type="application/javascript" src="<?php echo base_url(); ?>front_resources/js/mobileMenu/jquery-1.10.1.min.js"></script> -->
<script type="application/javascript" src="<?php echo base_url(); ?>front_resources/js/mobileMenu/menu.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.Menu a.link').click(function(e){
          //e.preventDefault();
          e.stopPropagation();
        });
    });
</script>

</head>

<body>
<?php UrlRferer();?>
<?php educki_kmli(); ?>
<?php removeOnSale(); ?>

<!-- Pre Loader Starts -->
<div class="loading">
  <img class="loading-gif-2" src="<?=base_url('front_resources/')?>images/preloader-2.gif" alt="preloader" />
</div>

<script type="text/javascript">
// Preloader CSS Starts

$(window).on('load', function() { //"use strict";
  $('.loading').fadeOut();
});
// Preloader CSS Ends
</script>
<!-- Pre Loader Ends -->
<!--header starts here-->
<?php 
$segment = $this->uri->segment(1);

if (strpos($segment, 'cart') !== false) {
    include(FRONT_INCLUDE_PATH."includes/cart-header.php");
}else{
    if($segment != "product-preview"){
        include(FRONT_INCLUDE_PATH."includes/header_top_nav.php");
    }
}
?>



<!--header ends here-->

