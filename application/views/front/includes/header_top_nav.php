<script type="text/javascript">

$(document).ready(function() {    
    
    $(document).on('keyup','#searc',function(){
        var txt = $(this).val();
        if(txt.length > 0){
            $('#smartsearch').show();
            
        }
    });
    
    $(document).on('focus','#searc',function(){
        var txt = $(this).val();
        if(txt.length > 0){
            $('#smartsearch').show();
            
        }
    });

    $(document).click(function(e) {
      var target = e.target;
      if (!$(target).is('.nwhite-wrap') && !$(target).parents().is('.search-sec')) {
        $('#smartsearch').hide();
        
      }
    });

});
</script>
<script type="text/javascript">
$(document).ready(function() {
    
    $(document).on('keyup','#searc',function(){
        var txt = $(this).val();
        if(txt.length > 0){
            $.ajax({
                type : 'POST',
                url  : "<?php echo base_url('smart-search');?>",
                data : {txt:txt,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                success: function(data){
                    data = JSON.parse(data);
                    if(data.rows >0 ){
                        $('#searchli').html(data.html);
                    }else{
                        $('#searchli').html('<li><div class="noSearchData">No search results for "<span>'+txt+'</span>"</div></li>');
                    }
                    $('#smartsearch').show();
                }
            });
            
        }else{
            $('#searchli').html('');
        }
    });
    
    $(document).on('focus','#searc',function(){
        var txt = $(this).val();
        if(txt.length > 0){
            $.ajax({
                type : 'POST',
                url  : "<?php echo base_url('smart-search');?>",
                data : {txt:txt,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                success: function(data){
                    data = JSON.parse(data);
                    if(data.rows >0 ){
                        $('#searchli').html(data.html);
                    }else{
                        $('#searchli').html('<li><div class="noSearchData">No search results for "<span>'+txt+'</span>"</div></li>');
                    }
                    $('#smartsearch').show();
                }
            });
            
        }else{
            $('#searchli').html('');
        }
    });
    
    
});
</script>
<!--header starts here-->
<div class="uppr-bdr">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Upper header starts here-->
                <div class="upr-hed">
                    <div class="col-md-2 col-sm-2 col-xs-12 hidden-xs"></div>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <div class="upr-hed-rgt">
                            
                            
                            <?php if($this->session->userdata('logged_in')) { ?>
                            <div class="up-reg">
                            	<a><?=$this->session->userdata('userfname')." ".$this->session->userdata('userlname')?></a>
                            </div>
                            <div class="up-reg acc-lk">
                            	<a href="<?=base_url('my-dashboard')?>">My Account</a>
                            </div>
							<?php }else { ?>
                            <div class="up-reg">
                            	<a href="#" class="hdr-regbtn close_click" data-toggle="modal" data-target="#myModal" id="reg-btn-2-1" onclick="destroy_session()">Register</a>
                                <div class="upr-moblogn mobile-visibleObj">
                                	<a href="#" class="hdr-loginbtn close_click" data-toggle="modal" data-target="#myModal3" onclick="destroy_session()">Login</a>
                                </div>
                            </div>
                            <?php } ?>
                            
                            <div class="upr-logn mblout">
                                <ul>
                                    <li class="mobile-hiddenObj">
                                        <?php if($this->session->userdata('logged_in')) { ?>
                                        <a href="<?=base_url('logout')?>" onclick="destroy_session()">Logout</a>
                                        <?php }else { ?>
                                        <a href="#" class="hdr-loginbtn close_click" data-toggle="modal" data-target="#myModal3" onclick="destroy_session()">Login</a>
                                        <?php } ?>
                                    </li>
                                    <li class="large-view">
                                        <a href="<?php echo get_social_media_links()->facebook;?>" class="up-fb" target="_blank"></a>
                                    </li>
                                    <li class="large-view">
                                        <a href="<?php echo get_social_media_links()->twitter;?>" class="up-twt" target="_blank"></a>
                                    </li>
                                    <li class="large-view">
                                        <a href="<?php echo get_social_media_links()->pinterest;?>" class="pinrst" target="_blank"></a>
                                    </li>
                                    <li class="large-view">
                                        <a href="<?php echo get_social_media_links()->linkedin;?>" class="linklnd" target="_blank"></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="upr-crt">
                                <?php $total = totalCartItems();
                                if($total < 1){?>
                                <a href="javascript:void(0);" data-toggle="modal" class="compl" data-target="#empty-crt">
                                <?php }else{ ?>
                                <a href="<?=base_url('cart')?>" class="compl">
                                <?php } ?>
                                    <span class="upr-crt2"></span>
                                    <div class="crt-rgt">
                                        <p class="red-clr">
                                            $<span id="cart_sub_total"><?=number_format(getCartSubTotal(), 2, '.', '')?></span>
                                        </p>
                                        <p id="cart_items">
                                            <?php $total = totalCartItems();
                                            if($total > 1){
                                                echo (int)$total." items";
                                            }else{
                                                echo (int)$total." item";
                                            } ?>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Upper header starts here-->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function destroy_session(){
        $.ajax({
            type: 'POST',
            url: '<?= base_url() ?>destroy-session',
            async: false,
            data: {<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function (result) {
                if(result['status']=200){
                    console.log('Success');
                }
            },
            error: function () {
            }
        });
    }
</script>
<div class="hdr-new-rw">
    <div class="container">
        <div class="row">
            <div class="auto-container-hed">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <div class="upr-logn mobile-view">
                                <ul>
                                    <li>
                                        <a href="<?php echo get_social_media_links()->facebook;?>" class="up-fb" target="_blank"></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo get_social_media_links()->twitter;?>" class="up-twt" target="_blank"></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo get_social_media_links()->pinterest;?>" class="pinrst" target="_blank"></a>
                                    </li>
                                    <li>
                                        <a href="<?php echo get_social_media_links()->linkedin;?>" class="linklnd" target="_blank"></a>
                                    </li>
                                </ul>
                            </div>
                        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="brand img-responsive" href="<?= base_url() ?>"><img src="<?= base_url() ?>resources/company_logo/<?=getSiteLogo()?>"/></a>
                    </div>
                    <div class="collapse navbar-collapse js-navbar-collapse pad-rgt-0">
                        <ul class="nav navbar-nav">
                            <?php $menu = header_menu();
                            foreach($menu as $items) {
                            ?>
                            <li>
                                <a href="<?= base_url() ?><?=$items['pg_url']?>">
                                    <?=$items['pg_title']?>
                                </a>
                            </li>
                            <?php } ?>
                            
                            <!-- Mobile Menu -->
                            <!-- <li class="shop-nav-mobile" style="display:none;">
                                <ul class="Menu -vertical">
                                    <li class="-hasSubmenu">
                                        <a href="javascript: void(0);">Shop</a> <a href="javascript: void(0);" class="arrow"><span class="glyphicon glyphicon-chevron-down"></span></a>
                                        <ul>
                                            <li><a href="#">Bacon</a></li>
                                            <li><a href="#">Ipsum</a></li>
                                            
                                            <li class="-hasSubmenu">
                                            <a href="http://www.google.com" class="link">Submenu</a> <a href="javascript: void(0);" class="arrow"><span class="glyphicon glyphicon-chevron-right"></span></a>
                                                <ul>
                                                    <li><a href="#">Bacon</a></li>
                                                    <li><a href="#">Ipsum</a></li>
                                                    <li><a href="#">Dolor</a></li>
                                                    <li><a href="#">Amet</a></li>
                                                </ul>
                                            </li>
                                            
                                            <li class="-hasSubmenu">
                                            <a href="http://www.google.com" class="link">Submenu</a> <a href="javascript: void(0);" class="arrow"><span class="glyphicon glyphicon-chevron-right"></span></a>
                                                <ul>
                                                    <li><a href="#">Bacon</a></li>
                                                    <li><a href="#">Ipsum</a></li>
                                                    <li><a href="#">Dolor</a></li>
                                                    <li><a href="#">Amet</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li> -->
                            <!-- Mobile Menu Ends -->

                            <!-- Mobile Menu -->
                            <li class="shop-nav-mobile" style="display:none;">
                                <ul class="Menu -vertical">
                                    <li class="-hasSubmenu">
                                        <a href="javascript: void(0);">Shop</a> <a href="javascript: void(0);" class="arrow"><span class="glyphicon glyphicon-chevron-right"></span></a>
                                        <?php if(count(getMainCategories()) > 0){ ?>
                                        <ul>
                                            <?php $count=-1; foreach(getMainCategories() as $cat){ 
                                                $count ++;
                                            $sub_category = getSubCategories($cat['cat_id']);
                                            ?>
                                            <li class="<?=(count($sub_category)>0)?'-hasSubmenu':''?>"><a href="<?=base_url()?><?=$cat['cat_url'];?>" class="link"><?= $cat['cat_name'];?></a><?php if(count($sub_category)>0){?><a href="javascript: void(0);" class="arrow"><span class="glyphicon glyphicon-chevron-right"></span></a><?php }?>
                                            <?php if(count($sub_category)>0){ ?>
                                                <ul>
                                                    <?php foreach($sub_category as $sub_cat){ 
                                                    $sub_sub_category = getSubSubCategories($sub_cat['cat_id']);
                                                    ?>
                                                    <li class="<?=(count($sub_sub_category)>0)?'-hasSubmenu':''?>"><a href="<?=base_url()?><?=$cat['cat_url'];?>/<?=$sub_cat['cat_url'];?>" class="link"><?= $sub_cat['cat_name'];?></a><?php if(count($sub_sub_category)>0){?><a href="javascript: void(0);" class="arrow"><span class="glyphicon glyphicon-chevron-right"></span></a><?php }?>
                                                    <?php if(count($sub_sub_category)>0){ ?>
                                                        <ul>
                                                            <?php foreach($sub_sub_category as $sub_sub_cat){  ?>
                                                            <li><a href="<?=base_url()?><?=$cat['cat_url'];?>/<?=$sub_cat['cat_url'];?>/<?=$sub_sub_cat['cat_url'];?>"><?= $sub_sub_cat['cat_name'];?></a></li>
                                                            <?php }?>
                                                        </ul>
                                                <?php } }?>
                                                </ul>
                                                <?php }?>
                                            </li>
                                            <!-- <li><a href="#">Ipsum</a></li>
                                            
                                            <li class="-hasSubmenu">
                                            <a href="http://www.google.com" class="link">Submenu</a> <a href="javascript: void(0);" class="arrow"><span class="glyphicon glyphicon-chevron-right"></span></a>
                                                <ul>
                                                    <li><a href="#">Bacon</a></li>
                                                    <li><a href="#">Ipsum</a></li>
                                                    <li><a href="#">Dolor</a></li>
                                                    <li><a href="#">Amet</a></li>
                                                </ul>
                                            </li>
                                            
                                            <li class="-hasSubmenu">
                                            <a href="http://www.google.com" class="link">Submenu</a> <a href="javascript: void(0);" class="arrow"><span class="glyphicon glyphicon-chevron-right"></span></a>
                                                <ul>
                                                    <li><a href="#">Bacon</a></li>
                                                    <li><a href="#">Ipsum</a></li>
                                                    <li><a href="#">Dolor</a></li>
                                                    <li><a href="#">Amet</a></li>
                                                </ul>
                                            </li> -->
                                        <?php }?>
                                        </ul>
                                        <?php }?>
                                    </li>
                                </ul>
                            </li>
                            <!-- Mobile Menu Ends -->
                                    
                            <li class="dropdown shop-nav">            
                                <a href="#" class="dropdown-toggle post-relt" data-toggle="dropdown">Shop <span class="glyphicon glyphicon-chevron-down"></span></a>
                                <div class="nw-nav-bx dropdown-menu">

                                    <!-- Mobile Navigation Starts -->
                                    <nav id="mysidebarmenu" class="amazonmenu" style="display: none;">
                                        <?php if(count(getMainCategories()) > 0){ ?>
                                        <ul>
                                            <?php $count=-1; foreach(getMainCategories() as $cat){ 
                                                $count ++;
                                            $sub_category = getSubCategories($cat['cat_id']);
                                            ?>
                                            <li>
                                                <a href="<?=base_url()?><?=$cat['cat_url'];?>">
                                                    <?= $cat['cat_name'];?>
                                                        <?php if(count($sub_category)>0){?>
                                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                                        <?php }?>
                                                </a>
                                                <ul class="<?php if($count==0){echo " subf-active ";}?>">
                                                    <?php
                                                    if(count($sub_category)>0){
                                                    ?>
                                                        <?php foreach($sub_category as $sub_cat){ 
                                                        $sub_sub_category = getSubSubCategories($sub_cat['cat_id']);
                                                        ?>
                                                        <li>
                                                            <a href="<?=base_url()?><?=$cat['cat_url'];?>/<?=$sub_cat['cat_url'];?>" class="second-level">
                                                                <?=$sub_cat['cat_name']?>
                                                                    <?php if(count($sub_sub_category)>0){?>
                                                                    <span class="nw-arow"></span>
                                                                    <?php }?>
                                                            </a>
                                                            <?php if(count($sub_sub_category)>0){?>
                                                            <ul class="nw-wdth third-level<?php if($count==0){echo " subsubf-active ";}?>">
                                                                <li class="difr-stl">
                                                                    <?=$sub_cat['cat_name']?>
                                                                </li>
                                                                <?php foreach($sub_sub_category as $sub_sub_cat){ ?>
                                                                <li>
                                                                    <a href="<?=base_url()?><?=$cat['cat_url'];?>/<?=$sub_cat['cat_url'];?>/<?=$sub_sub_cat['cat_url'];?>">
                                                                        <?=$sub_sub_cat['cat_name']?>
                                                                    </a>
                                                                </li>
                                                                <?php
                                                                    $count++;
                                                                }
                                                                ?>
                                                            </ul>
                                                            <?php }$count++; ?>
                                                        </li>
                                                        <?php }?>
                                                        <?php } $count++;?>
                                                </ul>
                                            </li>
                                            <?php $count++; }?>
                                        </ul>
                                        <?php }?>
                                    </nav>
                                    <!-- Mobile Navigation Ends -->



                                    <!-- New Navigation Starts -->


                                    <!-- Left Section Starts -->
                                    <div class="menu-colleft">                                        
                                        <div class="menu-nin">
                                            <?php if(count(getMainCategories()) > 0){ ?>
                                                <ul>                                                
                                                <?php foreach(getMainCategories() as $cat){ 
                                                    $sub_category = getSubCategories($cat['cat_id']);?>
                                                    <li>
                                                        <a href="<?=base_url()?><?=$cat['cat_url'];?>" class="mainMenu sublink-<?= $cat['cat_id'];?>">
                                                            <?= $cat['cat_name'];
                                                            if(count($sub_category)>0){?>                                                            
                                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                                            <?php }?>
                                                        </a>
                                                    </li>
                                                <?php }?>
                                                </ul>
                                            <?php }?>
                                        </div>

                                        <div class="menu-bline"></div> <!-- For bordered line -->
                                    </div>
                                    <!-- Left Section Ends -->

                                    <!-- Middle Section Starts -->
                                    <div class="menu-colcenter">
                                        <?php if(count(getMainCategories()) > 0){ $count = 0;
                                            foreach(getMainCategories() as $cat){
                                                $sub_category = getSubCategories($cat['cat_id']);
                                                    if(count($sub_category)>0){?>
                                        <!-- Revisable Sub Section Starts -->                                                
                                        <div class="menu-nin <?=($count==0)?'subfirst':''?> sublink-<?=$cat['cat_id']?>">
                                            <?php $count++;?>
                                            <ul>
                                                <?php foreach($sub_category as $sub_cat){ 
                                                    $sub_sub_category = getSubSubCategories($sub_cat['cat_id']);?>
                                                <li>
                                                    <a href="<?=base_url()?><?=$cat['cat_url'];?>/<?=$sub_cat['cat_url'];?>" class="subMenu subsublink-<?=$sub_cat['cat_id']?>">
                                                        <?=$sub_cat['cat_name']?>
                                                        <?php if(count($sub_sub_category)>0){?>
                                                            <span class="glyphicon glyphicon-chevron-right right-arrow"></span>
                                                        <?php }?>
                                                    </a>
                                                </li>
                                                <?php }?>
                                            </ul>
                                        </div>
                                        <!-- Revisable Sub Section Ends -->
                                        <?php
                                                }
                                            }
                                        }
                                        ?>
                                                    

                                    </div>
                                    <!-- Middle Section Ends -->

                                    <!-- Right Section Starts -->
                                    <div class="menu-colright">
                                        <?php if(count(getMainCategories()) > 0){
                                            foreach(getMainCategories() as $cat){$count=0;
                                                $sub_category = getSubCategories($cat['cat_id']);
                                                    if(count($sub_category)>0){
                                                        foreach($sub_category as $sub_cat){ 
                                                            $sub_sub_category = getSubSubCategories($sub_cat['cat_id']);
                                                            if(count($sub_sub_category)>0){?>
                                                            <!-- Revisable Sub Sub Section Starts -->
                                                            <div class="menu-nin <?=($count==0)?'subsubfirst':''?> subsublink-<?=$sub_cat['cat_id']?>">
                                                                <h5><?=$sub_cat['cat_name']?></h5>
                                                                <ul>
                                                                    <?php $count++; foreach($sub_sub_category as $sub_sub_cat){ ?>
                                                                    <li>
                                                                        <a href="<?=base_url()?><?=$cat['cat_url'];?>/<?=$sub_cat['cat_url'];?>/<?=$sub_sub_cat['cat_url'];?>"><?=$sub_sub_cat['cat_name']?></a>
                                                                    </li>
                                                                    <?php }?>
                                                                </ul>
                                                            </div>
                                                            <?php }
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                                            <!-- Revisable Sub Sub Section Ends -->

                                    </div>
                                    <!-- Right Section Ends -->


                                    <!-- New Navigation Ends -->


                                </div>
                            </li>
                            <li>
                                <a href="<?=base_url()?><?=get_page_url(10,'tbl_pages_sub','sub_id')->sub_pg_url;?>/<?=get_page_url(7,'tbl_pages_listing','listing_id')->listing_url;?>">Sell On Educki </a>
                            </li>
                            <li class="mrg-rgt">
                                <a href="<?=base_url()?>blog">News & Advice</a><!-- News & Advice -->
                            </li>
							<?php if($this->session->userdata('logged_in')) { ?>
                                <li class="mobile-visibleObj">
                                    <a href="<?=base_url('logout')?>" onclick="destroy_session()">Logout</a>
                                </li>
                            <?php } ?>
                            </li>
                        </ul>
                    </div>
                    <!-- /.nav-collapse -->
                    <div class="search-sec">
                        <input type="search" placeholder="Search for items or shops" id="searc"/>
                        <input type="button" value="" />
                        <!-- Smart Search Dropdown Starts -->
                    <div class="nwhite-wrap" id="smartsearch" style="display:none;">
                        <div class="nwhite-inn">
                            
                            <div class="nwhite-row">
                                <ul id="searchli">

                                </ul>
                            </div>                           
                        </div>
                    </div>
                    <!-- Smart Search Dropdown Ends -->
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <!--lower header starts here-->
</div>
<script>
$('ul.nav li.dropdown').hover(function() {

    /* $('.subf-active').css('display', 'block');
    $('.subsubf-active').css('display', 'block');
    */
    $('.subf-active:first').css('display', 'block');
    $('.subsubf-active:first').css('display', 'block');
    $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});

$('.second-level').hover(function() {
    /* alert('hover'); */

    if(!$(this).parent().hasClass('hassub')){
        $('.third-level').hide();
        //alert('yes');
        //console.log(' not has class');
    }
});

jQuery(function() {
    amazonmenu.init({
        menuid: 'mysidebarmenu'
    })
})
$(document).ready(function(){
    $(document).on('click','.close_click',function(){
        $('#regEmail1').val('');
        $('#regPass1').val('');
        $('#fname').val('');
        $('#lname').val('');
        $('#regZip').val('');
        $('#regAge').val('');
        $('#remember_me').attr('checked',false);
        $('#regInterest1').val('');
        $('#regInterest2').val('');
        $('#otherInst1').val('');
        $('#otherInst2').val('');
        $('#is_small_bus').val(''); 
        $('#loginPassword').val('');
        $('#loginEmail').val('');
        $('.error').text('');
        $('#sign-in').prop('checked', false);
    });
});
</script>
<!--header ends here-->
<!--header ends here-->
<script type="text/javascript">
/*
    jQuery(document).on('click', '.mega-dropdown', function(e) {
      e.stopPropagation();
    });
    */
</script>
<!--register popup starts here-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog popup-1" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Welcome to eDucki</h5>
                <div class="pp-fr-mg">
                    <a href="<?php  echo $this->facebook->login_url(); ?>">
                        <img src="<?= base_url() ?>front_resources/images/popup-fb.png" alt="" title=""/>
                    </a>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="pop-bdr">
                <p>
                    or
                </p>
            </div>
            <div class="modal-body">
                <input type="text" id="regEmail1" value="" placeholder="Email" class="11a register-1"/>
                <div class="error"></div>
                <input type="password" id="regPass1" value="" placeholder="Password" class="register-1" />
                <div class="error"></div>
                <input type="button" class="regbtn1" value="Sign up" />
                <a href="javascript:void(0);" id="reg2page" class="secondstep" data-toggle="modal" data-target="#myModal2"></a>
            </div>
            <div class="modal-footer">
                <div class="alrdy-havacc">
                    <p>
                        Already have an account?
                        <a href="javascript:void(0);" class="thrd-step" data-toggle="modal" data-target="#myModal3">Log in</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!--register popup ends here-->
<!--register info popup starts here-->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog popup-2" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Introduce Yourself</h5>
                <p>
                    Tell us a little about you
                </p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>           
            <div class="modal-body">
                <input type="hidden" id="regEmail2" value="" placeholder="Email" />
                <div class="error"></div>
                <input type="hidden" id="regPass2" value="" placeholder="Password"   class="register-2"/>
                <div class="error"></div>
                <input type="text" id="fname" value="" placeholder="First Name"   class="register-2"/>
                <div class="error"></div>
                <input type="text" id="lname" value="" placeholder="Last Name"   class="register-2"/>
                <div class="error"></div>
                <input type="text" id="regZip" value="" placeholder="Zip Code"   class="register-2"/>
                <div class="error"></div>
                <div class="optn-bx">
                    <select name="regAge" id="regAge"  class="register-2">
                        <option value="" selected disabled>Select Age</option>
                        <option value="13-17">13-17</option>
                        <option value="18-24">18-24</option>
                        <option value="25-34">25-34</option>
                        <option value="35-44">35-44</option>
                        <option value="45-54">45-54</option>
                        <option value="55-64">55-64</option>
                        <option value="65+">65+</option>
                    </select>
                    <div class="error"></div>
                </div>
                <div class="optn-bx">
                    <p>Gender</p>
                    <div class="rdo-reg">
                        <input type="radio" checked value="Male" class="regGender" id="r1" name="rr"   class="register-2"/>
                        <label for="r1"><span></span>Male</label>
                    </div>
                    <div class="rdo-reg">
                        <input type="radio" value="Female" class="regGender" id="r2" name="rr"   class="register-2"/>
                        <label for="r2"><span></span>Female</label>
                    </div>
                    <div class="rdo-reg">
                        <input type="radio" value="Other" class="regGender" id="r3" name="rr"   class="register-2"/>
                        <label for="r3"><span></span>Other</label>
                    </div>
                    <div class="error"></div>
                </div>
                <div class="optn-bx">
                    <select name="regInterest1" id="regInterest1"  class="register-2">
                        <option value="" selected disabled>Interests</option>
                        <option value="Fertility">Fertility</option>
                        <option value="Pregnancy">Pregnancy</option>
                        <option value="Baby names">Baby Names</option>
                        <option value="Baby">Baby</option>
                        <option value="Toddler">Toddler</option>
                        <option value="Child">Child</option>
                        <option value="Family">Family</option>
                        <option value="Advice">Advice</option>
                        <option value="Food">Food</option>
                        <option value="Other">Other</option>
                    </select>
                    <div class="error"></div>
                </div>
                <div id="other_inst1" style="display: none;">
                    <input type="text" id="otherInst1" value="" placeholder=""   class="register-2"/>
                    <div class="error"></div>
                </div>
                <div class="optn-bx">
                    <select name="regInterest2" id="regInterest2"  class="register-2">
                        <option value="" selected disabled>Interests</option>
                        <option value="Fertility">Fertility</option>
                        <option value="Pregnancy">Pregnancy</option>
                        <option value="Baby names">Baby Names</option>
                        <option value="Baby">Baby</option>
                        <option value="Toddler">Toddler</option>
                        <option value="Child">Child</option>
                        <option value="Family">Family</option>
                        <option value="Advice">Advice</option>
                        <option value="Food">Food</option>
                        <option value="Other">Other</option>
                    </select>
                    <div class="error"></div>
                </div>
                <div id="other_inst2" style="display: none;">
                    <input type="text" id="otherInst2" value="" placeholder=""  class="register-2" />
                    <div class="error"></div>
                </div>
                <div class="optn-bx">
                    <select name="is_small_bus" id="is_small_bus"  class="register-2">
                        <option value="" selected disabled>Are you a small business owner</option>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                    <div class="error"></div>
                </div>
                <input type="button" value="Continue" id="regbtn2" />
            </div>
        </div>
    </div>
</div>
<!--register info popup ends here-->
<!--login popup starts here-->
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog popup-3" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Sign In to eDucki</h5>
                <div class="pp-fr-mg">
                    <a href="<?php  echo $this->facebook->login_url(); ?>">
                        <img src="<?= base_url() ?>front_resources/images/popup-fb.png" alt="" title=""/>
                    </a>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="pop-bdr">
                <p>
                    or
                </p>
            </div>
            <div class="modal-body fro-frg">
                <input type="text" id="loginEmail" placeholder="Email" class="login-enter"/>
                <div class="error"></div>
                <div class="only-reltve">
                    <input type="password" id="loginPassword" placeholder="Password" name="password" class="login-enter">
                    <a href="<?=base_url('forgot-password')?>" class="frg-pas-2">I forgot</a>
                    <div class="error loginPasswordError"></div>
                </div>
                <div class="chk-bx-lg">
                    <input type="checkbox" id="remember_me" />
                    <p>Keep me signed in</p>
                </div>
                <input type="button" data-dismiss="modal" aria-label="Close" value="Nevermind" class="nvrmnd" />

                <input type="button" class="loginbtn" value="Sign in" />
            </div>
        </div>
    </div>
</div>
<!--login popup ends here-->
<!--Empty cart starts here-->
<div class="modal fade" id="empty-crt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog empy-crt" role="document">
        <div class="modal-content">
            <div class="empty-bx">
                <div class="modal-header">
                    <h5>Shopping Cart</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <h6>You have no items in your shopping cart.</h6>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Empty cart ends here-->
<!--login popup starts here-->
<div class="modal fade" id="myModal-thank" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog popup-thnk" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thank You</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body fro-frg text-center">
                <p>You have registered Successfully</p>
                <a href="<?=base_url()?>">Go back to Homepage</a>

                <div class="row nnbtn-row">
                    <a href="javascript:void(0);" class="close_click thrd-step nnew-btn" data-toggle="modal" data-target="#myModal3" data-dismiss="modal">Log in</a>
                </div>
            </div>            
        </div>
    </div>
</div>
<a href="javascript:void(0);" class="regthank" data-toggle="modal" data-target="#myModal-thank"></a>

<div class="modal fade" id="myModal-error" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
    <div class="modal-dialog popup-thnk" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel2"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body fro-frg text-center">
                <p id="error_text"></p>
                <a href="<?=base_url()?>">Go back to Homepage</a>
                <div class="row nnbtn-row">
                    <a href="javascript:void(0);" class="close_click thrd-step nnew-btn" data-toggle="modal" data-target="#myModal3" data-dismiss="modal">Log in</a>
                </div>
            </div>            
        </div>
    </div>
</div>

<a href="javascript:void(0);" class="regerror" data-toggle="modal" data-target="#myModal-error"></a>
<!--login popup ends here-->

<script type="text/javascript">
    $(document).ready(function(){
        var msg = '<?php echo $this->session->flashdata('fbReg');?>';
        if(msg!=''){
            $('#error_text').text(msg);
            $('#exampleModalLabel2').html('Error');
            $('.regerror').click();
        }
    })
</script>

<script type="text/javascript">
    $(document).ready(function(){
        var msg = '<?php echo $this->session->flashdata('pass_chnage_msg');?>';
        if(msg!=''){
            $('#error_text').text(msg);
            $('#exampleModalLabel2').html('Thank You');
            $('.regerror').click();
        }
    })
</script>