<!--Breadcrumbs starts here-->
<div class="container">
	<div class="row">
    	<div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
    		<div class="auto-container">
            	<div class="brd-sec pad-9">
                	<ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="<?=base_url();?>">Home</a></li>
                      <li class="breadcrumb-item">My Account</li>
                      <li class="breadcrumb-item active">My Dashboard</li>
                      
                    </ol>
                </div>	
            </div>
    	</div>
    </div>

</div>
<!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="seller-sec">
	<div class="container">
		<div class="row">
        	<div class="auto-container">
        		<!--Left section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav'); ?> 
                <!--Left section starts here-->
                
                
                <!--right section starts here-->
                 <div class="col-sm-9 col-xs-12">
                 	<div class="my-account-right">
                    	
                        <div class="my-account-right-head">
                        	<h5>My Dashboard</h5>
                            <p>Hello <?=$user->user_fname." ".$user->user_lname?>!</p>
                            <p>Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text
Here Sample Text Here Sample Text Here.</p>
                        </div>
                        
                        <div class="my-account-right-2">
                        	<div class="well">
                            	<h4>Recent Orders</h4>	
                                <a href="#">View All</a>
                            </div>
                            
                            <div class="my-account-right-table">
                            	 <table class="table">
                                    <thead>
                                      <tr>
                                        <th class="pad-lft-0" style="width:14%;">Item</th>
                                        <th style="width:34%;">Product Description</th>
                                        <th class="text-center" style="width:14%;">Date</th>
                                        <th style="width:15%;">Order Total</th>
                                        <th style="width:5%;">Status</th>
                                        <th class="text-center" style="width:18%;">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td class="pad-lft-0"><img src="<?=base_url()?>front_resources/images/product-slide-thumbl.jpg"  alt="" title="" class="img-nset"/></td>
                                        <td>Sample Text Here Sample Text Here Sample Text Here Sample</td>
                                        <td class="text-center">10-23-2017</td>
                                        <td class="text-center">$200.00</td>
                                        <td>Pending</td>
                                        <td class="pad-rgt-0">
                                        	<a href="#" class="view-ord">View Order </a>
                                            <span class="lne">|</span>
                                            <a href="#" class="reord-ord">Reorder </a>
                                        </td>
                                      </tr>
                                      
                                      <tr>
                                        <td class="pad-lft-0"><img src="<?=base_url()?>front_resources/images/product-slide-thumbl.jpg"  alt="" title="" class="img-nset"/></td>
                                        <td>Sample Text Here Sample Text Here Sample Text Here Sample</td>
                                        <td class="text-center">10-23-2017</td>
                                        <td class="text-center">$200.00</td>
                                        <td>Pending</td>
                                        <td class="pad-rgt-0">
                                        	<a href="#" class="view-ord">View Order </a>
                                            <span class="lne">|</span>
                                            <a href="#" class="reord-ord">Reorder </a>
                                        </td>
                                      </tr>
                                      
                                      <tr>
                                        <td class="pad-lft-0"><img src="<?=base_url()?>front_resources/images/product-slide-thumbl.jpg"  alt="" title="" class="img-nset"/></td>
                                        <td>Sample Text Here Sample Text Here Sample Text Here Sample</td>
                                        <td class="text-center">10-23-2017</td>
                                        <td class="text-center">$200.00</td>
                                        <td>Pending</td>
                                        <td class="pad-rgt-0">
                                        	<a href="#" class="view-ord">View Order </a>
                                            <span class="lne">|</span>
                                            <a href="#" class="reord-ord">Reorder </a>
                                        </td>
                                      </tr>
                                      
                                    </tbody>
                                  </table>
                            
                            </div>
                            
                            <div class="well">
                            	<h4>Account Information</h4>	
                            </div>
                            
                            <div class="accort-info">
                            	<div class="accort-info-bx">
                                	<div class="accort-info-bx-tilt">
                                    	<h5>Contact Information</h5>
                                        <a href="#">Edit</a>
                                    </div>
                                    <div class="accort-info-bx-txt">
                                    	<p>
                                        	<?=$user->user_fname." ".$user->user_lname?>
                                        </p>
                                        <p>
                                        	<?=$user->user_email?>
                                        </p>
                                    	<a href="#" class="chang-pawrd">Change Password</a>
                                    </div>
                                
                                
                                </div>
                            
                            </div>
                            
                            <div class="well">
                            	<h4>Address Book</h4>	
                                <a href="#">Manage Addresses</a>
                            </div>
                            <div class="accort-info">
                            	<div class="accort-info-bx">
                                	<div class="accort-info-bx-tilt">
                                    	<h5>Default Billing Address</h5>
                                        <a href="#">Edit Address</a>
                                    </div>
                                    <div class="accort-info-bx-txt">
                                    	<p>
                                        	Name Here
										</p>
                                        <p>
                                        	Sample Text Here Sample Text Here
										</p>
                                        <p>
                                        	Name Here
										</p>
                                        <p>
                                        	Sample Text Here Sample Text Here

                                        </p>
                                        <p>
                                        	T: +1 234 567 8901
                                        </p>
                                    	
                                    </div>
                                
                                
                                </div>
                                <div class="accort-info-bx mrg-rgt-0">
                                	<div class="accort-info-bx-tilt">
                                    	<h5>Default Shipping Address</h5>
                                        <a href="#">Edit Address</a>
                                    </div>
                                    <div class="accort-info-bx-txt">
                                    	<p>
                                        	Name Here
										</p>
                                        <p>
                                        	Sample Text Here Sample Text Here
										</p>
                                        <p>
                                        	Name Here
										</p>
                                        <p>
                                        	Sample Text Here Sample Text Here

                                        </p>
                                        <p>
                                        	T: +1 234 567 8901
                                        </p>
                                    	
                                    </div>
                                
                                
                                </div>
                            
                            </div>
                        </div>
                    
                    </div>	
                 
                 </div>  
            	<!--right section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav-m-view'); ?>
            </div>
		</div>
    </div>
</section>


<!--Blog main ends  here-->