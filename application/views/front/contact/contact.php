<?php
if ($pagedata->sub_pg_image=='') {
    $path = base_url('resources/no_banner/no_banner.png');
} else {
    $path = base_url('resources/page_image/' . $pagedata->sub_pg_image);
}
?>
<!--banner  starts here-->
<section class="whted-bg"  style="background-image: url('<?= $path ?>'); !important">
    <div class="frg-layr"></div>
    <!--Breadcrumbs starts here-->
    <div class="container">
        <div class="row">
            <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
                <div class="auto-container">
                    <div class="brd-whit">
                        <div class="brd-sec">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?= base_url() ?>what-is-educki">About</a></li>
                                <li class="breadcrumb-item active">
                                    <?=$pagedata->sub_pg_title;?>
                                </li>
                            </ol>
                        </div>
                        <h2><?=$pagedata->sub_pg_title;?></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Breadcrumbs ends  here-->
</section>
<!--banner  starts here-->

<!--main sec starts here-->
<section class="weht-main">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12 pad-lft-0">
                    <div class="side-nav">
                        <ul>
                            <?php
                            $footermenu = about_side_menu();
                            foreach ($footermenu as $items) {
                                if($pagedata->sub_pg_url == $items['sub_pg_url']){
                                        $active = 'active'; 
                                    }else {
                                        $active = '';
                                    }
                                ?>
                                <li>
                                    <a class="<?=$active?>" href="<?= base_url() ?><?= $items['sub_pg_url'] ?>">
                                        <?= $items['sub_pg_title'] ?>
                                    </a>
                                </li>
                                <?php } ?>
                        </ul>
                    </div>
                    <div class="contct-accord">
                        <div class="panel-group" id="accordion">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                    	
                                        <a class="only-lft2" href="mailto:info@educki.com">
                                            <span class="arrow-rgt"></span>
                                            Report copyright issues
                                        </a>
                                        
                                        <!--
                                        <a class="accordion-toggle only-lft2" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            <span class="arrow-rgt"></span>
                                            Report copyright issues
                                        </a>
                                        -->
                                        
                                    </h4>
                                </div>
                                <!--
                                <div id="collapseOne" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            Sample Text Here Sample Text Here Sample Text Here Sample Text Here.
                                        </p>
                                    </div>
                                </div>
                                -->
                            </div>
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                    	
                                        <a class="only-lft2" href="mailto:info@educki.com">
                                            <span class="arrow-rgt"></span>
                                            Press inquiries
                                        </a>
                                        
                                        <!--
                                        <a class="accordion-toggle only-lft2" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                            <span class="arrow-rgt"></span>
                                            Press inquiries
                                        </a>
                                        -->
                                        
                                    </h4>
                                </div>
                                
                                <!--
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            Sample Text Here Sample Text Here Sample Text Here Sample Text Here.
                                        </p>
                                    </div>
                                </div>
                                -->
                                
                            </div>
                            <div class="panel">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                    	
                                        <a class="only-lft2" href="mailto:info@educki.com">
                                            <span class="arrow-rgt"></span>
                                            Want to chat?
                                        </a>
                                        
                                        <!--
                                        <a class="accordion-toggle only-lft2" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                            <span class="arrow-rgt"></span>
                                            Want to chat?
                                        </a>
                                        -->
                                    </h4>
                                </div>
                                
                                <!--
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p>
                                            Sample Text Here Sample Text Here Sample Text Here Sample Text Here.
                                        </p>
                                    </div>
                                </div>
                                -->
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-lg-9 col-sm-8 col-xs-12 pad-rgt-0">
                    <div class="whted-rgt-sec">
                        <div class="contact-sec">
                            <div class="contct-tit">
                                <h3>Email Us</h3>
                            </div>
                            <div class="contct-form">
                                <?php echo form_open_multipart('', array('name' => 'contact', 'id' => 'contact_form', 'enctype' => 'multipart/form-data')); ?>
                                <div class="contct-row">
                                    <input type="text" placeholder="Your Name *" name="name" / value="<?php echo set_value('name'); ?>">
                                    <div class="error" id="name_validate"></div>
                                    <div class="error">
                                        <?php echo form_error('name') ?>
                                    </div>
                                </div>
                                <div class="contct-row">
                                    <input type="text" placeholder="Your Email Address *" name="email" / value="<?php echo set_value('email'); ?>">
                                    <div class="error" id="email_validate"></div>
                                    <div class="error">
                                        <?php echo form_error('email') ?>
                                    </div>
                                </div>
                                <div class="contct-row">
                                    <input type="text" placeholder="Subject" / name="subject" value="<?php echo set_value('subject'); ?>">
                                    <div class="error"></div>
                                </div>
                                <div class="contct-row">
                                    <select name="category" id="category">
                                        <!--<option selected hidden="hidden">Category</option>-->
                                        <option value="">Choose a Category</option>
                                        <option value="1">Images</option>
                                        <option value="2">Product Name</option>
                                        <option value="4">Other Product Details</option>
                                        <option value="5">Where's my stuff?</option>
                                        <option value="6">Cancel an item or order</option>
                                        <option value="7">Other Shipping Question</option>
                                        <option value="8">Problem with order</option>
                                        <option value="9">Returns and refunds</option>
                                    </select>
                                    <div class="error" id="category_validate"></div>
                                </div>
                                <input type="text" name="cat_name" id="cat_name" hidden>
                                <div class="contct-row">
                                    <textarea placeholder="Message *" name="message"><?php echo set_value('message'); ?></textarea>
                                    <div class="error" id="message_validate"></div>
                                    <div class="error">
                                        <?php echo form_error('message') ?>
                                    </div>
                                </div>
                                <div class="contct-row mrg-botm-0">
                                    <div class="contct-left">
                                        
                                        <input type="text" id="filename" autocomplete="off" readonly placeholder="File Attachment">

                                        <label for="fileupload" id="buttonlabel">
                                            <span role="button" aria-controls="filename" tabindex="0">
                                                Choose File
                                            </span>
                                        </label>

                                        <input type="file" id="fileupload" name="fileupload">

                                        <label for="filename" class="hide">
                                            uploaded file
                                        </label>
                                        
                                    </div>
                                    <!-- <div class="error" id="fileupload_validate"></div> -->
                                    <div class="contct-right">
                                        <input type="submit" class="snd-btn" value="Send" />
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--main sec  ends here-->




<script>
// trigger upload on space & enter
// = standard button functionality
$("#buttonlabel span[role=button]").bind('keypress keyup', function(e) {
    if (e.which === 32 || e.which === 13) {
        //e.preventDefault();
        $('#fileupload').click();
    }
});

// return chosen filename to additional input
$('#fileupload').change(function(e) {
    var filename = $('#fileupload').val().split('\\').pop();
    $('#filename').val(filename);
    $('#filename').focus();
});
</script>
<!-- Contact us form validation starts here -->
<script>
$(document).ready(function() {
    $("#contact_form").validate({
        rules: {
            name: {
                minlength: 2,
                maxlength: 50,
                required: true
            },
            email: {
                minlength: 2,
                maxlength: 50,
                required: true,
                email: true
            },
            message: {
                required: true
            },
            fileupload: {
              extension: "docx|doc|pdf|ppt|txt|png|jpeg|jpg"
            }
        },
        messages: {
            name: "This field is required",
            email: {
                required: "This field is required",
                email: "Your email address must be in the format of name@domain.com"
            }
        }
    });
});
</script>
<!-- Contact us form validation ends here -->

<script>
$('.collapse').on('shown.bs.collapse', function() {
    $(this).parent().find(".arrow-rgt").removeClass("arrow-rgt").addClass("arrow-down");

}).on('hidden.bs.collapse', function() {
    $(this).parent().find(".arrow-down").removeClass("arrow-down").addClass("arrow-rgt");
});
</script>

<script type="text/javascript">
$("#category").change(function() {
    $("#cat_name").val($("#category option:selected").text());

});
</script>