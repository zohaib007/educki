<!--main bg here-->
<section class="frg-pas-bg">
    <div class="frg-layr"></div>
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="frgt-pasd-wrp">
                    <div class="frgt-sec">
                        <div class="fgt-tit">
                            <h5>Change Password</h5>
<!--                            <p>
                                Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text.
                            </p>-->
                            <?php echo form_open_multipart('', array('name' => 'updatepassword', 'id' => 'updatepassword_form')); ?>
                                <div class="nfield-wrap">
                                    <input type="password" placeholder="New Password" id="new_password" name="new_password" />
                                    <div class="error text-left" id="new_password_validate"><?php echo form_error('new_password')?></div>
                                    <input type="password" placeholder="Confirm Password" id="confirm_password" name="confirm_password" />
                                    <div class="error text-left" id="confirm_password_validate"><?php echo form_error('confirm_password')?></div>
                                </div>
                                <div class="fgt-btn">
                                    <input type="submit" class="frg-rgt" value="Change My Password"/>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--main bg  here-->
<script>
    $(document).ready(function () {

        $(function validate() {
            // body...
            var rules = {
                rules: {
                    new_password: {
                        required: true,
                        minlength: 6
                    },
                    confirm_password: {
                        required: true,
                        equalTo: "#new_password",
                        minlength: 6
                    }
                },
                messages: {
                    confirm_password: {
                         equalTo: "Your new password and confirmation password do not match"
                    }
                },
                errorPlacement: function (error, element) {
                    var name = $(element).attr("name");
                    error.appendTo($("#" + name + "_validate"));
                },
            };
            $('#updatepassword_form').validate(rules);
        });
    });
</script>
