

<link rel="stylesheet" href="<?= base_url() ?>js/filer/css/jquery.filer.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= base_url() ?>js/filer/css/themes/jquery.filer-dragdropbox-theme.css" type="text/css" media="all" />

<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard') ?>">My Account</a> </li>
                        <li class="breadcrumb-item"><a href="<?= base_url('manage-products') ?>">Manage Products</a></li>
                        <li class="breadcrumb-item active"> Edit Product</li>
                    </ol>
                </div>  
            </div>
        </div>
    </div>

</div>
<!--Breadcrumbs ends  here-->
<!-- <script type="text/javascript" src="<?=base_url()?>js/jquery.number.js"></script> -->
<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view("front/includes/seller-left-nav") ?>  
                <!--Left section starts here-->
                <input type="hidden" value=1 id="is_post" />
                <?php echo form_open('', array('name' => 'edit_product', 'id' => 'edit_prod')); ?>
                <input type="hidden" name="counter" id="counter" value="<?=count($prodImg)?>"/>
                <!--right section starts here-->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">

                        <div class="crte-prodct-main">
                            <h2>Edit Product</h2>

                            <!--preview listing sec starts here-->
                            <div class="prevw-list">

                                <div class="prevw-list-row">
                                    <div class="prevw-list-row-lft">
                                        <h4>I am Looking to:<span>*</span></h4>
                                        <select name="looking_for" id="looking_for">
                                            <option value = '' selected disabled>Select</option>
                                            <option value="Sell" <?= (set_select('looking_for', 'Sell') != '') ? set_select('looking_for', 'Sell') : set_select('looking_for', 'Sell', $prodData->looking_for == 'Sell' ? true : '') ?>>Sell</option>
                                            <option value="Trade" <?= (set_select('looking_for', 'Trade') != '') ? set_select('looking_for', 'Trade') : set_select('looking_for', 'Trade', $prodData->looking_for == 'Trade' ? true : '') ?>>Trade</option>
                                            <option value="Donate" <?= (set_select('looking_for', 'Donate') != '') ? set_select('looking_for', 'Donate') : set_select('looking_for', 'Donate', $prodData->looking_for == 'Donate' ? true : '') ?>>Donate</option>
                                        </select>
                                        <div class="error">
                                            <?= form_error('looking_for') ?>
                                        </div>
                                    </div>

                                    <div class="prevw-list-row-lft mrg-rgt-0">
                                        <h4>Condition:<span>*</span></h4>
                                        <select name="condition" id="condition">
                                            <option value = '' selected disabled>Select</option>
                                            <option value="New with tags" <?= (set_select('condition', 'New with tags') != '') ? set_select('condition', 'New with tags') : set_select('condition', 'New with tags', $prodData->condition == 'New with tags' ? true : '') ?>>New with tags</option>
                                            <option value="New without tags" <?= (set_select('condition', 'New without tags') != '') ? set_select('condition', 'New without tags') : set_select('condition', 'New without tags', $prodData->condition == 'New without tags' ? true : '') ?>>New without tags </option>
                                            <option value="New with defects" <?= (set_select('condition', 'New with defects') != '') ? set_select('condition', 'New with defects') : set_select('condition', 'New with defects', $prodData->condition == 'New with defects' ? true : '') ?>>New with defects</option>
                                            <option value="Excellent" <?= (set_select('condition', 'Excellent') != '') ? set_select('condition', 'Excellent') : set_select('condition', 'Excellent', $prodData->condition == 'Excellent' ? true : '') ?>>Excellent</option>
                                            <option value="Good" <?= (set_select('condition', 'Good') != '') ? set_select('condition', 'Good') : set_select('condition', 'Good', $prodData->condition == 'Good' ? true : '') ?>>Good</option>
                                            <option value="Fair" <?= (set_select('condition', 'Fair') != '') ? set_select('condition', 'Fair') : set_select('condition', 'Fair', $prodData->condition == 'Fair' ? true : '') ?>>Fair</option>
                                        </select>
                                        <div class="error">
                                            <?= form_error('condition') ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="prevw-list-row">
                                    <div class="prevw-list-row-lft">
                                        <h4>Quantity:<span>*</span></h4>
                                        <input type="text" name="qty" id="qty" value="<?= set_value('qty') != '' ? set_value('qty') : $prodData->qty ?>" min='1' placeholder="Enter Quantity" />
                                        <div class="error">
                                            <?= form_error('qty') ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="drecs-tit">
                                    <div class="prevw-list-row">
                                        <div class="prevw-list-row-lft">
                                            <div class="drecs-tit-hed">
                                                <h5 class="yelo-color">Write a Descriptive title of your item</h5>
                                                <p>
                                                    Use words people would search for when looking for your item.
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="prevw-list-row">
                                        <div class="prevw-list-row-lft">
                                            <h4>Product Title (Store View)<span>*</span></h4>
                                            <input type="text" name="prod_title" id="prod_title" value="<?= set_value('prod_title') != '' ? set_value('prod_title') : $prodData->prod_title ?>" placeholder="Enter Product Title" />
                                            <div class="error">
                                                <?= form_error('prod_title') ?>
                                            </div>
                                        </div>

                                        <div class="prevw-list-row-lft mrg-rgt-0">
                                            <h4>Product ID:</h4>
                                            <input type="text" name="prod_sku" readonly id="prod_sku" value="<?= set_value('prod_sku') != '' ? set_value('prod_sku') : $prodData->prod_id ?>" placeholder="1178408" />
                                            <div class="error">
                                                <?= form_error('prod_sku') ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="prevw-list-row">
                                        <div class="prevw-list-row-lft2">
                                            <h4>Short Description (Store View)<span>*</span></h4>
                                            <textarea name="prod_description" id="prod_description" maxlength="160" placeholder="Product Description" maxlength="160"><?= set_value('prod_description') != '' ? set_value('prod_description') : $prodData->prod_description ?></textarea>
                                            <span class="set-width"><span class="chatCount">0</span>/160 characters</span>
                                            <div class="error">
                                                <?= form_error('prod_description') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--preview listing sec ends here-->

                            <!--Add images sec starts here-->
                            <div class="add-img-sec">
                                <div class="add-img-sec-hed">
                                    <h5 class="yelo-color">Add Photos</h5>
                                    <p>Improve your buyer’s confidence by including as many as possible.</p>
                                </div>
                                <div class="drg-drp">
                                    <!-- <img src="<?= base_url('front_resources/') ?>images/drag-drop.jpg" /> -->
                                    <!-- Select Bulk Images Section Starts -->
                                    <div class="bulkimg-selector" id="upload">
                                        <input type="file" name="files[]" id="filer_input2" multiple>
                                        <div id="imagesdata" class="jFiler-items jFiler-row">
                                            <ul class="jFiler-items-list jFiler-items-grid">
                                            <?php foreach ($prodImg as $img) {
                                                ?>                                            
                                                    <li class="jFiler-item ">
                                                        <div class="newImgname">
                                                            <input type="hidden" name="img_name[]" value="<?= $img->img_name ?>" />
                                                            <div class="nimgDiv">
                                                                <table cellpadding="0" cellspacing="0" border="0">
                                                                    <tr>
                                                                        <td>
                                                                            <img src="<?= base_url('resources/prod_images/thumb/' . $img->img_name) ?>" alt="<?= $img->img_name ?>" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <!-- <a class="icon-jfi-trash jFiler-item-trash-action"></a> -->
                                                            <a title="Delete" data-del-file="<?= $img->img_name ?>" href="javascript:void(0);" class="deleteProdImg icon-jfi-times-circle jFiler-item-trash-action" onclick="return confirm('Are you sure you want to delete selected item(s)?');"></a>
                                                            <!--<input type="text" class="labelField"  name="img_label[]" value="<?=$img->img_label?>"/>-->
                                                        </div>
                                                    </li>
                                                <?php
                                            }
                                            ?>
                                            </ul>
                                        </div>
                                        <div class="error"><?= form_error('img_name[]') ?></div>
                                    </div>
                                    <!-- Select Bulk Images Section Ends -->
                                    
                                    <?php if(count($prodImg)<=0){?>
                                    <div class="img_validate">
                                        <input type="hidden" class="hiddenField"  name="img_name[]" value=""/>
                                        <div class="error"></div>
                                    </div>
                                    <?php }else{?>
                                    <div class="img_validate">
                                    </div>
                                    <?php }?>
                                </div>

                                <div class="you-tb-vid">
                                    <h5 class="yelo-color">Add Youtube Video</h5>
                                    <div class="prevw-list-row">
                                        <div class="prevw-list-row-lft">
                                            <h4>Youtube video URL</h4>
                                            <input type="text" name="youtube_links" id="youtube_links" value="<?= set_value('youtube_links') != '' ? set_value('youtube_links') : $prodData->youtube_links ?>" placeholder="Optional">
                                            <div class="error">
                                                <?= form_error('youtube_links') ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Add images sec ends here-->

                            <!--Product Details starts here-->
                            <div class="create-pro-detal">
                                <div class="create-pro-detal-hed">
                                    <h5 class="yelo-color">Product Details</h5>
                                    <p>Add specific detials about your item(s) to help buyers find it quickly.</p>
                                </div>

                                <div class="unq-and-multi-tb">
                                    <input type="hidden" name="is_unique" value="<?= $prodData->is_unique_filter ?>" id="is_unique" />
                                    <ul class="nav nav-tabs">
                                        <li class="no-lbrdr active">
                                            <a data-toggle="tab" href="#home" data-unq = "1" class="tab-unq-no uniqueFil">
                                                <div class="uniq-up">
                                                    <h6>unique Item</h6>
                                                    <p>
                                                        Select if you only have one of a particular item
                                                    </p>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="no-rbrdr">
                                            <a data-toggle="tab" href="#menu1" data-unq = "0" class="tab-unq-no multiFil">
                                                <div class="uniq-up">
                                                    <h6>Multiple Item</h6>
                                                    <p>
                                                        Select if you multiple colors and sizes of the same item
                                                    </p>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>

                                    <div class="unique-itm-main2">
                                        <div class="unique-itm-main-row">
                                            <div class="unique-itm-main-row-bx mainCat">
                                                <h5>Category<span> * </span></h5>
                                                <select name="mainCat" class="main-cat" id="mainCat">
                                                    <option value="">Select</option>
                                                    <?php foreach ($mainCat as $main) { ?>
                                                        <option value="<?= $main->cat_id ?>" <?php
                                                        if ($prodCat->category_id == $main->cat_id) {
                                                            echo "Selected";
                                                        }
                                                        ?> ><?= $main->cat_name ?></option>
                                                            <?php } ?>
                                                </select>
                                                <div class="error"><?= form_error('mainCat') ?></div>
                                            </div>
                                            <div class="unique-itm-main-row-bx subCatDiv">

                                            </div>
                                            <div class="unique-itm-main-row-bx mrg-rgt-0 subSubCat">

                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-content">
                                        <div id="home" class="tab-pane fade in active">
                                            <div class="unique-itm-main">
                                                <div class="unique-itm-main-row required-filter">

                                                </div>
                                            </div>
                                        </div>
                                        <div id="menu1" class="tab-pane fade">
                                            <div class="unique-itm-main">
                                                <div class="unique-itm-main-row">
                                                    <div class="atleadst-one-catrg required-multi-filter">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="unique-itm-main2">
                                        <div class="unique-itm-main-row addFilter" style="display:none;">
                                            <div class="additnol-featre">
                                                <h5 class="yelo-color">Additional Features</h5>
                                            </div>
                                        </div>
                                        <div class="unique-itm-main-row additional-filter">

                                        </div>
                                    </div>

                                </div>
                            </div>    
                            <!--Product Details ends here-->

                            <!--Price your item starts here-->
                            <div class="prc-yr-itm">
                                <div class="prc-yr-itm-hed">
                                    <h4>Price Your Item</h4>
                                </div>

                                <div class="prices-sec">
                                    <div class="prices-sec-left">
                                        <h5 class="yelo-color">Prices <span> * </span></h5>

                                        <div class="prices-sec-left-row">
                                            <div class="org-prc">
                                                <h4>Original Price</h4>
                                                <input type="text" name="prod_price" min="1.00" maxlength="10"  id="prod_price" value="<?= set_value('prod_price') != '' ? set_value('prod_price') : $prodData->prod_price ?>" placeholder="00.00"/>
                                                <span class="usd">[USD]</span>
                                                <div class="error">
                                                    <?= form_error('prod_price') ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="prices-sec-left-row">
                                            <div class="org-prc">
                                                <div class="setsa">
                                                    <input type="checkbox" id="is_onsale" name="is_onsale" value="1" <?= set_checkbox('is_onsale', 1) != '' ? set_checkbox('is_onsale', 1) : set_checkbox('is_onsale', 1, $prodData->prod_onsale == 1 ? true : '') ?> />
                                                </div>
                                                <h4>Sale Price<span class="setp">(Set a price for a limited time offer)</span></h4>
                                                <input  class="onsale"  min="1.00" maxlength="10" type="text" name="sale_price" id="sale_price" value='<?= set_value('sale_price') != '' ? set_value('sale_price') : $prodData->sale_price ?>' placeholder="00.00"/>
                                                <span class="usd onsale">[USD]</span>
                                                <div class="error onsale"><?= form_error('sale_price') ?></div>
                                            </div>

                                        </div>

                                        <div class="prices-sec-left-row onsale" style="display: <?= $prodData->prod_onsale == 1 ? 'block' : 'none' ?>;">
                                            <div class="org-prc" style="position:relative;">
                                                <h4>Sale Start Date</h4>
                                                <input type="text" id="datepicker" class="sDate" name="sale_start_date" value="<?= set_value('sale_start_date') != '' ? set_value('sale_start_date') : ($prodData->sale_start_date!='')?date('m/d/Y', strtotime($prodData->sale_start_date)):'' ?>" placeholder="10/20/2017" />
                                                <div class="error">
                                                    <?= form_error('sale_start_date') ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="prices-sec-left-row onsale">
                                            <div class="org-prc" style="position:relative;">
                                                <h4>Sale End Date</h4>
                                                <input type="text" id="datepicker2" class="eDate" name="sale_end_date" value="<?= set_value('sale_end_date') != '' ? set_value('sale_end_date') : ($prodData->sale_end_date!='')?date('m/d/Y', strtotime($prodData->sale_end_date)):'' ?>" placeholder="10/20/2017" />
                                                <div class="error">
                                                    <?= form_error('sale_end_date'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="prices-sec-right">

                                        <div class="prices-profir-sec">
                                            <div class="prices-profir-sec-bx">
                                                <h4>My Profit</h4>
                                                <h5>$<span class="my-profit calculateProfit">00.00</span></h5>
                                                <p>*Your profit after eDucki commissions</p>
                                            </div>
                                            <h6>*<span class="comminsionRate"><?=$TireInfo->tier_fee?></span>% current eDucki commissions rate </h6>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <!--Price your item starts here-->

                            <!--Shipping your item starts here-->
                            <div class="prc-yr-itm pad-botm-8">
                                <div class="prc-yr-itm-hed">
                                    <h4>Ship Your Item</h4>
                                </div>

                                <div class="prices-sec">
                                    <div class="prices-sec-left">
                                        <h5 class="yelo-color">Shipping <span> * </span></h5>

                                        <div class="prices-sec-left-row">
                                            <div class="chrg-fr-ship">
                                                <div class="chrg-fr-ship-rd">
                                                    <input type="radio" class="shipping" id="sp1" name="shipping" value="Charge for Shipping" <?= set_radio('shipping', 'Charge for Shipping', $prodData->shipping == "Charge for Shipping" ? true : '') ?>>
                                                    <label for="sp1"><span></span></label>
                                                </div>
                                                <div class="chrg-fr-ship-txt">
                                                    <h4>Charge for Shipping</h4>
                                                    <p>(The buyer will pay for the cost of shipping)</p>
                                                </div>
                                                <div class="inpt-sh ship1" id="ipt-hid" style="display:<?= $prodData->shipping == "Charge for Shipping" ? 'block' : 'none' ?>">
                                                    <select name="shipping_method" id="shipping_methods">
                                                        <option value="0">Select</option>
                                                        <option value="USPS" <?= $prodData->prod_shipping_methods == 'USPS' ? 'selected' : ''?> >USPS</option>
                                                        <option value="FedEx" <?= $prodData->prod_shipping_methods == 'FedEx' ? 'selected' : ''?>>FedEx</option>
                                                        <option value="DHL" <?= $prodData->prod_shipping_methods == 'DHL' ? 'selected' : ''?>>DHL</option>
                                                        <option value="UPS" <?= $prodData->prod_shipping_methods == 'UPS' ? 'selected' : ''?>>UPS</option>
                                                    </select>
                                                    <input type="text" name="ship_price" value="<?= set_value('ship_price') != '' ? set_value('ship_price') : $prodData->ship_price ?>" id="ship_price" placeholder="$5.00 Enter numeric value"/>
                                                    <div class="error"></div>
                                                    <input type="text" name="ship_days" value="<?= set_value('ship_days') != '' ? set_value('ship_days') : $prodData->ship_days ?>" id="ship_days" placeholder="# of Days to Ship (Ex. 3 or 1-3)" />
                                                    <p>(Let buyers know your shipping practices)</p>
                                                    <div class="error"></div>
                                                </div>


                                            </div>

                                            <div class="chrg-fr-ship">


                                                <div class="chrg-fr-ship-rd">
                                                    <input type="radio" id="sp2" class="shipping" name="shipping" value="Offer free Shipping" <?= set_radio('shipping', 'Offer free Shipping', $prodData->shipping == "Offer free Shipping" ? true : '') ?>>
                                                    <label for="sp2"><span></span></label>
                                                </div>
                                                <div class='chrg-fr-ship-txt'>
                                                    <h4>Offer free Shipping</h4>
                                                    <p>(You will cover the cost of shipping)</p>
                                                </div>

                                                <div class="inpt-sh" id="ipt-hid2" style="display:<?= $prodData->shipping == "Offer free Shipping" ? 'block' : 'none' ?>;">
                                                    <input type="text" name="free_ship_days" value="<?= set_value('free_ship_days') != '' ? set_value('free_ship_days') : $prodData->free_ship_days ?>" id="free_ship_days" placeholder="# of Days to Ship (Ex. 3 or 1-3)"/>
                                                    <p>(Let buyers know your shipping practices)</p>
                                                    <div class="error"></div>
                                                </div>


                                            </div>

                                            <?php
                                            $res = isStorePer($this->session->userdata('userid'), 'is_pickup');
                                            if ($res == 1)
                                            {
                                            ?>
                                                <div class="chrg-fr-ship">


                                                    <div class="chrg-fr-ship-rd">
                                                        <input type="radio" id="sp3" class="shipping" name="shipping" value="Local Pick Up" <?= set_radio('shipping', 'Local Pick Up', $prodData->shipping == "Local Pick Up" ? true : '') ?>>
                                                        <label for="sp3"><span></span></label>
                                                    </div>
                                                    <div class="chrg-fr-ship-txt">
                                                        <h4>Local Pick Up</h4>
                                                        <p>(Meet buyers locally to sell your products directly)</p>
                                                    </div>
                                                    <!-- <div class="inpt-sh" id="ipt-hid3" style="display:none;">
                                                            <input type="text" placeholder="$5.00 Enter numeric value"/>
                                                        <input type="text" placeholder="Product ships within (1-3) Business Days"/>
                                                        <p>(Let buyers know your shipping practices)</p>
                                                    </div> -->
                                                </div>
                                                <div class="error"></div>
                                            <?php
                                            }

                                            ?>

                                        </div>


                                    </div>

                                    <div class="prices-sec-right">
                                        <div class="help-est-sec">

                                            <h5>Help Estimate Shipping:</h5>

                                            <div class="help-est-sec-lnk">
                                                <div class="help-est-sec-lnk-row">
                                                    <div class="help-est-sec-lnk-left">
                                                        <h6>USPS</h6>
                                                    </div>
                                                    <div class="help-est-sec-lnk-right">
                                                        <a target="blank" href="https://postcalc.usps.com/">https://postcalc.usps.com/</a>
                                                    </div>
                                                </div>

                                                <div class="help-est-sec-lnk-row">
                                                    <div class="help-est-sec-lnk-left">
                                                        <h6>UPS</h6>
                                                    </div>
                                                    <div class="help-est-sec-lnk-right">
                                                        <a target="blank" href="https://www.theupsstore.com/tools/estimate-shipping-cost">https://www.theupsstore.com/tools/estimate-shipping-cost</a>
                                                    </div>
                                                </div>

                                                <div class="help-est-sec-lnk-row">
                                                    <div class="help-est-sec-lnk-left">
                                                        <h6>DHL</h6>
                                                    </div>
                                                    <div class="help-est-sec-lnk-right">
                                                        <a target="blank" href="http://www.dhl.com.pk/en/express/shipping.html">http://www.dhl.com.pk/en/express/shipping.html</a>
                                                    </div>
                                                </div>

                                                <div class="help-est-sec-lnk-row">
                                                    <div class="help-est-sec-lnk-left">
                                                        <h6>FedEx</h6>
                                                    </div>
                                                    <div class="help-est-sec-lnk-right">
                                                        <a target="blank" href="http://www.fedex.com/us/2016rates/rate-tools.html">http://www.fedex.com/us/2016rates/rate-tools.html</a>
                                                    </div>
                                                </div>
                                            </div>  

                                        </div>


                                    </div>
                                </div>

                            </div>
                            <!--Shipping your item starts here-->

                            <!--Return sec starts here-->
                            <div class="return-sec">
                                <div class="prices-sec-left">
                                    <h5 class="yelo-color">Returns</h5>
                                    <div class="chrg-fr-ship">
                                        <div class="chrg-fr-ship-rd">
                                            <input type="radio" id="acp1" name="prod_return" value="Returns Not Accepts" <?= set_radio('prod_return', 'Returns Not Accepts', $prodData->prod_return == "Returns Not Accepts" ? true : '') ?>>
                                            <label for="acp1"><span></span></label>
                                        </div>
                                        <div class="chrg-fr-ship-txt">
                                            <h4 class="txt-clr-gry">Returns Not Accepted</h4>
                                        </div>
                                    </div>
                                    <div class="chrg-fr-ship">
                                        <div class="chrg-fr-ship-rd">
                                            <input type="radio" id="acp2" name="prod_return" value="Accept Returns" <?= set_radio('prod_return', 'Accept Returns', $prodData->prod_return == "Accept Returns" ? true : '') ?>>
                                            <label for="acp2"><span></span></label>
                                        </div>
                                        <div class="chrg-fr-ship-txt">
                                            <h4 class="txt-clr-gry">Accept Returns</h4>
                                            <p>(Payment refunds are not managed through eDucki)</p>
                                        </div>
                                    </div>
                                    <div class="error"></div>
                                </div>  
                                <input type="hidden" id="list_items_value" name="list_items_value" value="0" />
                                <div class="three-btn-sec">
                                    <a href="javascript:void(0)" submit_url="<?= base_url() ?>edit-product/<?= $prodData->prod_url ?>" id="list_item">List Item</a>
                                    <a data_attr="1" submit_url="<?= base_url() ?>product-preview" class="submit_product_form" href="javascript:void(0)" id="preview">Preview</a>
                                    <a data_attr="1" submit_url="<?= base_url() ?>edit-product/<?= $prodData->prod_url ?>" class="submit_save" href="javascript:void(0)" >Save and Exit</a>

                                    <p>By clicking List it, you agree to pay all  applicable fees and receive reasonable offers from buyers.</p>
                                </div>  
                            </div>
                            <!--Return sec ends here-->

                        </div> 
                    </div>  
                </div>  
                <!--right section starts here-->

                <?php echo form_close(); ?>

            </div>
        </div>
    </div>
</section>
<!--Blog main ends  here-->
<script>
    $(document).ready(function(){
        var ship = '<?=$prodData->shipping?>';
        if(ship =="Charge for Shipping"){
            $('#sp1').click();
        }else if(ship =="Offer free Shipping"){
            $('#sp2').click();
        }else if(ship =="Local Pick Up"){
            $('#sp3').click();
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('keyup', '#prod_description', function () {
            var total_len = $(this).val();
            total_len = total_len.length;
            $('.chatCount').text(total_len);
        });

        // $('#sale_price').number( true, 2 );
        // $('#prod_price').number( true, 2 );

    });
</script>


<script>

    $(document).ready(function () {
        $('#sp1').click(function () {
            $('#ipt-hid2').hide('');
            $('#ipt-hid').show('');
            $('#ship_days').val('');
            $('#ship_price').val('');
            $('#shipping_methods').val('0');
            $('#ipt-hid3').hide('');
        });
        $('#sp2').click(function () {
            $('#ipt-hid2').show('');
            $('#free_ship_days').val('');
            $('#ship_days').val('');
            $('#ship_price').val('');
            $('#shipping_methods').val('0');
            $('#ipt-hid').hide('');
            $('#ipt-hid3').hide('');
        });
        $('#sp3').click(function () {
            $('#ipt-hid2').hide('');
            $('#ipt-hid').hide('');
            $('#ipt-hid3').show('');
            $('#shipping_methods').val('0');
            $('#ship_days').val('');
            $('#ship_price').val('');
            $('#free_ship_days').val('');
        });
    });

    $(document).ready(function () {
        //$('.ship1').hide();
        $('.onsale').hide();
        /*
         var radioBtn = '< ?= @$_POST['shipping'] ?>';
         if(radioBtn == 'Charge for Shipping'){
         $('.ship1').show();
         }else{
         $('.ship1').hide();
         }*/
        $(document).on('click', '.shipping', function () {
            if ($(this).is(':checked') && $(this).val() == 'Charge for Shipping') {
                $('.ship1').show();
            } else {
                $('.ship1').hide();
            }
        });

        var onsale = '<?= (@$_POST['is_onsale'] != '') ? @$_POST['is_onsale'] : $prodData->prod_onsale ?>';

        if (onsale == 1) {
            $('.onsale').show();
        } else {
            $('.onsale').hide();
        }
        $(document).on('click', '#is_onsale', function () {
            if ($(this).is(':checked')) {
                $('.onsale').show();
            } else {
                $('.onsale').hide();
            }
        });
    });
</script>

<script type="text/javascript">
    var root_url = base_url = '<?php echo base_url(); ?>';
    var token_name = '<?=$this->security->get_csrf_token_name()?>';
    var token_hash = '<?=$this->security->get_csrf_hash()?>';
</script>
<script src="<?=base_url()?>front_resources/js/jquery.cookie.js" type="text/javascript"></script>
<script src="<?=base_url()?>front_resources/new_filer/js/jquery.filer.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>front_resources/new_filer/custom_edit.js" type="text/javascript"></script>

<!-- <?php //include('js/file_filer_front.php'); ?> -->
<script>
    $.validator.setDefaults({ 
        ignore: [],
        // any other default options and/or rules
    });
    var category_id = '<?= $prodCat->category_id ?>';
    var sub_category_id = '<?= $prodCat->sub_category_id ?>';
    var sub_sub_category_id = '<?= $prodCat->sub_sub_category_id ?>';


    $(document).on('click', '.tab-unq-no', function () {
        var vlu = $(this).attr('data-unq');
        $('#is_unique').val(vlu);
    });

    $(document).ready(function () {
        var is_unique = '<?= $prodData->is_unique_filter ?>';
        if (is_unique == 1) {
            $('.uniqueFil').click();
        } else {
            $('.multiFil').click();
        }
    });

    $(document).on('click', '.deleteProdImg', function () {
        var file = $(this).attr('data-del-file');
        $.ajax({
            url: base_url + 'removeEditProdImg/',
            type: 'POST',
            data: {file:file,<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>'},
        })        
        .done(function(res){
            console.log(res + "Image Removed Success");
            var counter = $('#counter').val();
            counter--;
            $('#counter').val(counter);
            if(counter<=0){             
                $('.img_validate').html('<input type="hidden" class="hiddenField"  name="img_name[]" value=""/><div class="error"></div>');
            }else{
                $('.img_validate').html('');
            }
        });


        $(this).parent().remove();
    });

    /*
     $(document).on('click', '.deleteProdImg', function(){
     var idsrem = $(this).attr('data-imgid');
     $.ajax({
     url: base_url+'removeProdImg/'+$(this).attr('data-imgid'),
     type: 'POST',
     data: { < ?php echo $this->security->get_csrf_token_name(); ?>: '< ?php echo $this->security->get_csrf_hash(); ?>'},
     })
     .done(function(res) {
     
     $('.rem_'+idsrem).remove();
     console.log(res+"Image Removed Success");
     
     })
     .fail(function() {
     console.log("error");
     })
     .always(function() {
     console.log("Image Removed Complete");
     });
     });
     */




    $(document).on('change', '.main-cat', function () {
        $('.subCatDiv').html('');
        $('.subSubCat').html('');
        $('.required-filter').html('');
        $('.additional-filter').html('');
        $('.addFilter').hide();
        $('.required-multi-filter').html('');
        $('.additional-multi-filter').html('');

        var main_id = $(this).val();
        $('.main-cat').val(main_id);
        if (main_id > 0) {
            $.ajax({
                url: '<?= base_url('get-sub-cat') ?>',
                type: 'POST',
                async: false,
                data: {cat_id: main_id,<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'},
            })
                    .done(function (res) {
                        if (res != 0) {
                            $('.subCatDiv').html(res);
                            if (sub_category_id > 0) {

                                if (parseInt($('#is_post').val()) == 1) {
                                    $('#sub_cat').val(sub_category_id);
                                    $('.sub-cat').change();
                                }
                            }
                        } else {
                            console.log("success");
                        }
                    })
                    .fail(function () {
                        console.log("error");
                    });
        }
    });


    $(document).on('change', '.sub-cat', function () {
        $('.subSubCat').html('');
        $('.required-filter').html('');
        $('.additional-filter').html('');
        $('.addFilter').hide();
        $('.required-multi-filter').html('');
        $('.additional-multi-filter').html('');


        var main_id = $(this).val();
        $('.sub-cat').val(main_id);
        if (main_id > 0) {
            $.ajax({
                url: '<?= base_url('get-sub-sub-cat') ?>',
                type: 'POST',
                async: false,
                data: {cat_id: main_id,<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'},
            })
                    .done(function (res) {
                        if (res != 0) {
                            $('.subSubCat').html(res);
                            if (sub_category_id > 0) {

                                if (parseInt($('#is_post').val()) == 1) {
                                    $('#sub_sub_cat').val(sub_sub_category_id);
                                    $('.sub-sub-cat').change();
                                    $('.sub-sub-cat').change();
                                }
                            }
                        } else {
                            console.log("success");
                        }
                    })
                    .fail(function () {
                        console.log("error");
                    });
        }
    });

    $(document).on('change', '.sub-sub-cat', function () {

        var main_id = $(this).val();
        $('.sub-sub-cat').val(main_id);
        if (main_id > 0) {
            $.ajax({
                url: '<?= base_url('get-req-filter-uqe-edit/' . $prodData->prod_id) ?>',
                type: 'POST',
                data: {cat_id: main_id,<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'},
            })
                    .done(function (res) {
                        if (res != 0) {
                            $('.required-filter').html(res);
                            //console.log(res);
                        } else {
                            console.log("success");
                        }
                    })
                    .fail(function () {
                        console.log("error");
                    });
        }
    });

    $(document).on('change', '.sub-sub-cat', function () {
        $('.required-filter').html('');
        $('.additional-filter').html('');
        $('.addFilter').hide();

        var main_id = $(this).val();
        $('.sub-sub-cat').val(main_id);
        if (main_id > 0) {
            $.ajax({
                url: '<?= base_url('get-addi-filter-uqe-edit/' . $prodData->prod_id) ?>',
                type: 'POST',
                data: {cat_id: main_id,<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'},
            })
                    .done(function (res) {
                        if (res != 0) {
                            $('.addFilter').show();
                            $('.additional-filter').html(res);
                        } else {
                            console.log("success");
                            $('.addFilter').hide();
                        }
                    })
                    .fail(function () {
                        console.log("error");
                    });
        }
    });


    if (parseInt($('#is_post').val()) == 1) {
        if (category_id > 0) {
            $('.main-cat').change();
        }
        $('#is_post').val(0);
    }


    $(document).on('change', '.conditional', function () {

        var filter_id = $(this).val();
        var subFilterClass = $(this).attr('name');
        $('.' + subFilterClass).html('');
        $.ajax({
            url: '<?= base_url('get-sub-conditional-uqe') ?>',
            type: 'POST',
            data: {filter_id: filter_id,<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'},
        })
                .done(function (res) {
                    if (res != 0) {
                        $('.' + subFilterClass).html(res);
                    } else {
                        console.log("success");
                    }
                })
                .fail(function () {
                    console.log("error");
                });
    });

    $(document).on('change', '.conditional-req', function () {

        var filter_id = $(this).val();
        var subFilterClass = $(this).attr('name');
        $('.' + subFilterClass).html('');
        $.ajax({
            url: '<?= base_url('get-sub-conditional-req-uqe') ?>',
            type: 'POST',
            data: {filter_id: filter_id,<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'},
        })
                .done(function (res) {
                    if (res != 0) {
                        $('.' + subFilterClass).html(res);
                    } else {
                        console.log("success");
                    }
                })
                .fail(function () {
                    console.log("error");
                });
    });




    /**
     *********************---------------------**********************
     ******************* [ For Multiple Filters ] *******************
     ********************* -------------------- *********************
     */
    $(document).on('change', '.sub-sub-cat', function () {
        $('.required-multi-filter').html('');
        $('.additional-multi-filter').html('');
        var main_id = $(this).val();

        $('.sub-sub-cat').val(main_id);
        if (main_id > 0) {
            $.ajax({
                url: '<?= base_url('get-req-filter-multi-edit/' . $prodData->prod_id) ?>',
                type: 'POST',
                data: {cat_id: main_id,<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'},
            })
                    .done(function (res) {
                        if (res != 0) {
                            $('.required-multi-filter').html(res);
                        } else {
                            console.log("Multi success");
                        }
                    })
                    .fail(function () {
                        console.log("Multi error");
                    });
        }
    });

    /*if(parseInt($('#is_post').val()) == 1){
     if(category_id > 0){
     $('.sub-sub-cat').change();
     }
     $('#is_post').val(0);
     }*/
    $('.sub-sub-cat').change();


    $(document).on('click', '.multi-conditional', function () {
        //alert(1);
        var is_checked = $(this).is(':checked');
        var filter_id = $(this).val();
        var subFilterClass = $(this).attr('data-name2');
        $('.' + subFilterClass).html('');
        if (is_checked == true) {
            $.ajax({
                url: '<?= base_url('get-sub-conditional-req-multi') ?>',
                type: 'POST',
                data: {filter_id: filter_id,<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'},
            })
                    .done(function (res) {
                        if (res != 0) {
                            $('.' + subFilterClass).html(res);
                        } else {
                            console.log("success");
                        }

                    })
                    .fail(function () {
                        console.log("error");
                    });

        } else {
            $('.' + subFilterClass).html('');
        }

    });

</script>

<script type="text/javascript">

    $.validator.addMethod("uniqueSku",
            function (value, element) {
                var result = false;
                $.ajax({
                    type: "POST",
                    async: false,
                    url: "<?= base_url('valid-sku-edit/' . $prodData->prod_id) ?>", // script to validate in server side
                    data: {sku: $('#prod_sku').val(), <?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'},
                    success: function (data) {
                        result = (data == true) ? true : false;
                    }
                });
                // return true if username is exist in database
                return result;
            },
            "This SKU already exists."
            );

    $.validator.addMethod("grateThanPrice",
        function (value, element) {
            var result = false;
            var prod_price = $('#prod_price').val();
            prod_price = parseFloat(prod_price);
            var is_onsale = $("#is_onsale").is(":checked");
            var sale_price = parseFloat($('#sale_price').val());
            if (sale_price >= prod_price && $("#is_onsale").is(":checked")) {
                result = false;
            } else {
                result = true;
            }
            return result;
        },
        "This field must be less than origional price."
    );

    $.validator.addMethod('allValidate', function (v, e){
        var ret = true;
        var lab = $('.labelField');
        $.each(lab, function(i,v){
            if(lab[i].value == ''){
                ret = false;
            }
        });
        return ret;
    });

    $.validator.addMethod('greaterThanStartDate', function (v, e){
        var result = true;
        var isOnSale = $("#is_onsale").is(":checked");
        var startPrice = $('.sDate').val();
        var endPrice = $('.eDate').val();
        if(startPrice == ''){
            startPrice = '12/31/9999';
        }
        // alert(startPrice);
        if(isOnSale == true && endPrice != '' && endPrice < startPrice){
            result = false;
        }
        return result;
    },
    "Sale end date cannot be less than sale start date."
    );

    $('#edit_prod').validate({// initialize the plugin 
        rules: {
            files: {
                required: true,
            },
            looking_for: {
                required: true,
            },
            condition: {
                required: true,
            },
            qty: {
                required: true,
                number: true,
                min: 1,
            },
            prod_title: {
                required: true,
            },
            /*prod_sku: {
             required: true,
             uniqueSku :true,
             },*/
            prod_description: {
                required: true,
            },
            youtube_links: {
                //required: true,
                url: true,
            },
            mainCat: {
                required: true,
            },
            sub_cat: {
                required: true,
            },
            "img_name[]": {
                required: true
            },
            sub_sub_cat: {
                required: true,
            },
            prod_price: {
                required: true,
                min: 1,
                number: true,
            },            
            sale_price: {
                required: {
                    depends: function (element) {
                        return $("#is_onsale").is(":checked");
                    },
                },
                grateThanPrice: true,
                number: true,
            },
            /*sale_start_date: {
             required: {
             depends: function(element) {
             return $("#is_onsale").is(":checked");
             },
             },
             },
             sale_end_date: {
             required: {
             depends: function(element) {
             return $("#is_onsale").is(":checked");
             },
             },
             },*/
            sale_end_date: {
                greaterThanStartDate:true,
            },
            shipping: {
                required: true,
            },
            ship_price: {
                required: {
                    depends: function (element) {
                        return $('#sp1').is(":checked");
                    },                    
                },            
            },
            ship_days: {
                required: {
                    depends: function (element) {
                        return $('#sp1').is(":checked");
                    },
                },
            },
            free_ship_days: {
                required: {
                    depends: function (element) {
                        return $('#sp2').is(":checked");
                    },
                },
            },
            prod_return: {
                required: true,
            }
        },
        messages: {
            looking_for: {
                required: "This field is required.",
            },
            condition: {
                required: "This field is required.",
            },
            qty: {
                required: "This field is required.",
                number: "This field must contain the valid number.",
                min: "This field must be greater than zero.",
            },
            prod_title: {
                required: "This field is required.",
            },
            /*prod_sku: {
             required: "This field is required.",
             //uniqueSku: "Test Message",
             },*/
            prod_description: {
                required: "This field is required.",
            },
            /*youtube_links: {
             //required: "This field is required.",
             //url: "",
             },*/
            mainCat: {
                required: "This field is required.",
            },
            sub_cat: {
                required: "This field is required.",
            },
            sub_sub_cat: {
                required: "This field is required.",
            },
            prod_price: {
                required: "This field is required.",
                number: "This field must contain the valid number.",
                min: "This field must be greater than zero.",
            },
            sale_price: {
                required: "This field is required.",                
            },
            /*sale_start_date: {
             required: "This field is required.",
             },
             sale_end_date: {
             required: "This field is required.",
             },*/
            shipping: {
                required: "This field is required.",
            },
            free_ship_days: {
                required: "This field is required.",
            },
            ship_price: {
                required: "This field is required.",
                number: "This field must contain the valid number.",
                min: "This field must be greater than zero.",
            },
            ship_days: {
                required: "This field is required.",
            },
            prod_return: {
                required: "This field is required.",
            },
            "img_name[]": {
                required: "This field is required."
            }
        },
        //errorElement: 'none',
        //errorClass: "error",
        errorPlacement: function (error, element)
        {
            element.removeClass('error');
            if (element.attr('name') == "ship_price") {
                //console.log(element.parent().parent().parent());
                error.insertAfter(element);
                element.removeClass('error');
                console.log(error);
            } else if (element.attr('name') == "ship_days" || element.attr('name') == "free_ship_days") {
                error.insertAfter(element.next());
                element.removeClass('error');
            }else {

                error.insertAfter(element.parent().find("div.error"));
                element.removeClass('error');
            }
            //element.removeClass('error');
        },
        submitHandler: function (form) {
            

            var is_unique = $('#is_unique').val();
            // if is_unique == 1 its mean unique filter and if is_unique == 0 its mean multiple
            if (is_unique == '0') {
                var submit = true;
                var count = 0;
                var count1 = 0;
                $('.required-multi-filter').find('.atleadst-one-catrg-bx').each(function () {
                    var flag = false;
                    $(this).find('input[type=checkbox]').each(function () {
                        if ($(this).prop("checked") == true) {
                            if ($(this).hasClass("multi-conditional")) {
                                console.log("Multi");
                                flag = true;
                                var flag1 = false;
                                var subFilterClass = $(this).attr('data-name2');
                                $('.' + subFilterClass).find('input[type=checkbox]').each(function () {
                                    console.log(count1);
                                    if ($(this).prop("checked") == true) {
                                        flag1 = true;
                                    }
                                });
                                count1++;
                                if (flag1) {
                                    $('#customemultirrorr' + count1).css("display", "none");
                                    console.log('here');
                                } else {
                                    submit = false;
                                    $('.' + subFilterClass).append('<label  id="customemultirrorr' + count1 + '" generated="true" class="error">This field is required.</label>');
                                    console.log("Feild required");
                                }
                            } else {
                                flag = true;
                            }
                        }
                    });
                    count++;
                    if (flag) {
                        $('#customerrorr' + count).css("display", "none");
                        console.log('here');
                    } else {
                        submit = false;
                        $(this).append('<label  id="customerror' + count + '" generated="true" class="error">This field is required.</label>');
                        console.log("Feild required");
                    }
                });
                if (submit) {
                    form.submit();
                }
            } else {
                form.submit();
            }
        },
        invalidHandler: function (event, validator) {
            var is_unique = $('#is_unique').val();
            // if is_unique == 1 its mean unique filter and if is_unique == 0 its mean multiple
            if (is_unique == '0') {
                var count = 0;
                var count1 = 0;
                $('.required-multi-filter').find('.atleadst-one-catrg-bx').each(function () {
                    var flag = false;
                    $(this).find('input[type=checkbox]').each(function () {
                        if ($(this).prop("checked") == true) {
                            if ($(this).hasClass("multi-conditional")) {
                                console.log("Multi");
                                flag = true;
                                var flag1 = false;
                                var subFilterClass = $(this).attr('data-name2');
                                $('.' + subFilterClass).find('input[type=checkbox]').each(function () {
                                    console.log(count1);
                                    if ($(this).prop("checked") == true) {
                                        flag1 = true;
                                    }
                                });
                                count1++;
                                if (flag1) {
                                    $('#customemultirrorr' + count1).css("display", "none");
                                    console.log('here');
                                } else {
                                    $('.' + subFilterClass).append('<label  id="customemultirrorr' + count1 + '" generated="true" class="error">This field is required.</label>');
                                    console.log("Feild required");
                                }
                            } else {
                                flag = true;
                            }
                        }
                    });
                    count++;
                    if (flag) {
                        $('#customerrorr' + count).css("display", "none");
                        console.log('here');
                    } else {
                        $(this).append('<label  id="customerror' + count + '" generated="true" class="error">This field is required.</label>');
                        console.log("Feild required");
                    }
                });
            }
        }
    });


</script>
<script>
    $(document).ready(function () {
        $('#list_item').click(function () {
            $('#list_items_value').val('1');
            var sub_url = $(this).attr('submit_url');
            $('#edit_prod').attr('action', sub_url);
            $("#edit_prod").attr('target', '');
            $('#edit_prod').submit();
        });
        $(".submit_product_form").on('click', function (e) {
            var sub_url = $(this).attr('submit_url');

            $('#edit_prod').attr('action', sub_url);
            $("#edit_prod").attr('target', '_blank');
            $('#edit_prod').submit();
        });
    });
</script>
<script>
    $(document).ready(function () {
        $(".submit_save").on('click', function (e) {
            var sub_url = $(this).attr('submit_url');
            $('#edit_prod').attr('action', sub_url);
            $("#edit_prod").removeAttr("target");
            $('#edit_prod').submit();
        });
    });
</script>
<script>
    /**
    - Code by Haseeb
    - Code for Calculate the educki commision and seller profit.
    - Start
    */
    $(document).ready(function(){
        $(document).on('keyup', '#prod_price', function(){
            var commisionRate = parseFloat($('.comminsionRate').text());
            var calculateRate = 0.0;
            var price = 0;
            price = salePriceOrOrginalPrice();
            if(price > 0){
                calculateRate = price - ((price*commisionRate)/100);
            }
            $('.calculateProfit').text(calculateRate.toFixed(2));
            console.log(calculateRate);
        });
        $(document).on('keyup', '#sale_price', function(){
            var commisionRate = parseFloat($('.comminsionRate').text());
            var calculateRate = 0.0;
            var price = 0;
            price = salePriceOrOrginalPrice();
            if(price > 0){
                calculateRate = price - ((price*commisionRate)/100);
            }
            $('.calculateProfit').text(calculateRate.toFixed(2));
            console.log(calculateRate);
        });
        $(document).on('click', '#is_onsale', function(){
            var commisionRate = parseFloat($('.comminsionRate').text());
            var calculateRate = 0.0;
            var price = 0;
            price = salePriceOrOrginalPrice();
            if(price > 0){
                calculateRate = price - ((price*commisionRate)/100);
            }
            $('.calculateProfit').text(calculateRate.toFixed(2));
            console.log(calculateRate);
        });
        $(document).on('change', '.sDate', function(){
            var commisionRate = parseFloat($('.comminsionRate').text());
            var calculateRate = 0.0;
            var price = 0;
            price = salePriceOrOrginalPrice();
            if(price > 0){
                calculateRate = price - ((price*commisionRate)/100);
            }
            $('.calculateProfit').text(calculateRate.toFixed(2));
            console.log(calculateRate);
        });
        $(document).on('change', '.eDate', function(){
            var commisionRate = parseFloat($('.comminsionRate').text());
            var calculateRate = 0.0;
            var price = 0;
            price = salePriceOrOrginalPrice();
            if(price > 0){
                calculateRate = price - ((price*commisionRate)/100);
            }
            $('.calculateProfit').text(calculateRate.toFixed(2));
            console.log(calculateRate);
        });

        function salePriceOrOrginalPrice(){
            var sDate = $('.sDate').val();
            var eDate = $('.eDate').val();
            var tDate = '<?=date('m/d/Y')?>';
            var onSale = $('#is_onsale').prop('checked');
            //console.log(sDate+' -> '+eDate+' -> '+tDate+' -> '+onSale);
            if(onSale == 1) {
                if(sDate != '' && eDate != '') {
                    if(tDate >= sDate && tDate <= eDate) {
                        return parseFloat($('#sale_price').val());
                    } else {
                         return parseFloat($('#prod_price').val());
                    }
                }else if(sDate != '') {
                    if(tDate >= sDate) {
                         return parseFloat($('#sale_price').val());
                    } else {
                         return parseFloat($('#prod_price').val());
                    }
                } else if(eDate != '') {
                    if(tDate <= eDate) {
                         return parseFloat($('#sale_price').val());
                    } else {
                         return parseFloat($('#prod_price').val());
                    }
                } else {
                     return parseFloat($('#sale_price').val());
                }
            } else {
                 return parseFloat($('#prod_price').val());
            }
        }

        $('#prod_price').keyup();
    });
    /**
    - End
    - Commision Fee Calculate
    */
</script>