<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard') ?>">My Account</a> </li>
                        <li class="breadcrumb-item active">Manage Products</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view("front/includes/seller-left-nav") ?>    
                <!--Left section starts here-->
                <!--right section starts here-->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">
                        <div class="mang-prod-mian">
                            <!--Manage product section-->	
                            <div class="mang-prod-mian-hed">
                                <h5>Manage Products</h5>
                                <div class="mang-prod-mian-hed-rg">
                                    <p>Total Amount Received:<span class="clor-ble">$<?php echo number_format(store_amount_received($store_id), 2, '.', '') ?></span></p>
                                    <p> | </p>
                                    <p class="pad-rgt-0">Amount Pending:<span class="clor-ble">$<?php echo number_format(store_amount_pending($store_id), 2, '.', '') ?></span></p>
                                </div>
                            </div>
                            <!--Manage product section-->
                            <div class="ualert-row">
                                <?php
                                if ($this->session->flashdata('msg') != "") {
                                    echo '<div id="message" class="alert alert-success message "><a href="javascript:;" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>' . $this->session->flashdata('msg') . '</div>';
                                }

                                if ($this->session->flashdata('valid_file') != "") {
                                    echo '<div id="message" class="alert alert-success message "><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>' . $this->session->flashdata('valid_file') . '</div>';
                                }                                
                                ?>
                                <div id="new_message" class="message hide"><a href="javascript:;" class="close" data-dismiss="alert" aria-label="close" title="close">×</a></div>
                            </div>
                            <!--Tabs section start here-->
                            <div class="add-product-tabs-sec">
                                <div class="bulk-proct">
                                    <ul>
                                        <li class="bulkBox"><a href="javascript:void(0);" class="bulk removefilename">Bulk Upload</a></li>
                                       	<li class="blsep-rp">|</li>
                                        <li><a href="javascript:;" id="copy_selected">Copy Listing</a></li>
                                        <li>|</li>
                                        <li>
                                            <?php echo form_open('', array("name" => "MyFormExport")); ?>
                                            <a href="<?= base_url() ?>export_product_csv" data-toggle="tooltip" data-placement="bottom" title="Download products in CSV format">Download CSV</a>
                                            <?php echo form_close(); ?>
                                        </li>
                                        <li>|</li>
                                        <li><a href="javascript:;" id="delete_selected">Delete Selected</a></li>
                                    </ul>
                                </div>
                                <div class="add-prd-btn">
                                    <a href="<?= base_url('create-new-product') ?>">Add New Product</a>	
                                </div>
                            </div>
                            <!--Tabs section start here--> 
                            <?php echo form_open_multipart(base_url('bluk-upload'),array('id'=>'bulkupload')); ?>
                            <!--Bulk product upload starts here-->
                            <div class="blk-prd-upld">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4>Bulk Product Upload</h4>
                                    </div>
                                    <div class="panel-body">
                                        <h5>Upload CSV File<span> * </span></h5>
                                        <input type="file" name="bulkFile" value="" id="bulkFile" />
                                            <div class="multi-fle-btn">
                                                <a href="<?=base_url('resources/bulk_file/file.csv')?>" download >Download CSV</a>
                                            </div>
                                        <div class="multi-fle-btn">
                                            <input type="submit"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Bulk product upload ends here-->
                            <?php echo form_close(); ?>
                            <!--Select option starts here-->
                            <div class="select-optn-sec">
                                <div class="my-account-right-2">
                                    <div class="well">
                                        <?php echo form_open_multipart('search_product', array('name' => 'search_product', 'id' => 'search_product_form')); ?>  
                                        <div class="mang-search-opt">
                                            <label>Search by:</label>
                                            <select class="selectpicker" id="select_search" name="select_search">
                                                <option  value="0" class="prod-id">Select</option>
                                                <option class="prod-id" <?php if(@$val == 1){ echo 'selected';}?> value="1">Product ID</option>
                                                <option class="prod-id" <?php if(@$val == 2){ echo 'selected';}?> value="2">Product Title</option>
                                                <option class="prod-id" <?php if(@$val == 3){ echo 'selected';}?> value="3">Quantity</option>
                                                <option class="prod-id" <?php if(@$val == 4){ echo 'selected';}?> value="4">Price</option>
                                            </select>
                                            <div class="error" id="select_search_validate"><?php echo form_error('select_search') ?></div>
                                        </div>
                                        <input type="hidden" value="<?=$user_id?>" name="user_id" id="user_id">
                                        <div class="search-product">
                                            <input type="text" id="search_value" value="<?=@$search?>" name="search_value"/>
                                            <input type="submit" value="Search" />
                                                <a href="<?=base_url('manage-products')?>" class="resetbutton">Reset</a>
                                            <div class="error" id="search_value_validate"><?php echo form_error('search_value') ?></div>
                                        </div>
                                        
                                        <?php echo form_close(); ?>
                                    </div>

                                    <div class="my-account-right-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="text-center" style="width:15%;"> Product ID</th>
                                                    <th style="width:1%;"></th>
                                                    <th class="pad-lft-0" style="width:16%;">Product Image</th>
                                                    <th style="width:18%;">Product Title</th>
                                                    <th class="text-center" style="width:9%;">Price</th>
                                                    <th style="width:12%;">Shipping</th>
                                                    <th style="width:7%;">Qty</th>
                                                    <th style="width:9%;">Status</th>
                                                    <th class="text-center pad-rgt-0" style="width:13%;">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if (isset($results)) { ?>
                                                    <?php foreach ($results as $data) {
                                                        $prod_image = $this->common_model->commonselect("tbl_product_images", "img_prod_id", $data->prod_id,'img_id','ASC')->row();
                                                        $url = 'javascript:void(0);';
                                                        if($data->prod_status == 1 && $data->qty > 0 && $prod_image != '' ){
                                                            $url = productDetailUrl($data->prod_id).'/'.$data->prod_url;
                                                        }
                                                    ?>
                                                        <tr id="<?= $data->prod_id ?>">
                                                            <td>
                                                                <input type="checkbox" id="id_checkbox" name="id_checkbox" value="<?= $data->prod_id ?>" class="proCheck" />
                                                                <p class="pull-left fnt-chnag"><?= $data->prod_id ?></p>
                                                            </td>
                                                            <td></td>
                                                            <td class="pad-lft-0">
                                                                <a href="<?=$url?>">
                                                                	<img src="<?= base_url() ?>resources/prod_images/thumb/<?= $prod_image->img_name ?>" alt="" title="" class="img-nset" />
                                                                </a>
                                                            </td>
                                                            <td>
                                                            	<a href="<?=$url?>">
																	<?= $data->prod_title ?>
                                                                </a>
                                                            </td>
                                                            <td class="text-center">
                                    <?php
                                    $productPrice = 0.0;
                                    $sDate = date('Y-m-d', strtotime($data->sale_start_date));
                                    $eDate = date('Y-m-d', strtotime($data->sale_end_date));
                                    $tDate = date('Y-m-d');
                                    if ($data->prod_onsale == 1) {
                                        if ($data->sale_start_date != '' && $data->sale_end_date != '') {
                                            if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                                                $productPrice = number_format($data->prod_price, 2, '.', '');
                                                echo '<span class="prc-p">$' . number_format($data->sale_price, 2, '.', '') . '</span> <span class="prc-cut r">$' . number_format($data->prod_price, 2, '.', '') . '</span>';
                                            } else {
                                                $productPrice = number_format($data->prod_price, 2, '.', '');
                                                echo '$' . number_format($data->prod_price, 2, '.', '') . '</span>';
                                            }
                                        } else if ($data->sale_start_date != '') {
                                            if ($tDate >= $sDate) {
                                                $productPrice = number_format($data->prod_price, 2, '.', '');
                                                echo '<span class="prc-p">$' . number_format($data->sale_price, 2, '.', '') . '</span> <span class="prc-cut r">$' . number_format($data->prod_price, 2, '.', '') . '</span>';
                                            } else {
                                                $productPrice = number_format($data->prod_price, 2, '.', '');
                                                echo '$' . number_format($data->prod_price, 2, '.', '') . '</span>';
                                            }
                                        } else if ($data->sale_end_date != '') {
                                            if ($tDate <= $eDate) {
                                                echo 'Price:<span class="prc-p">$' . number_format($data->sale_price, 2, '.', '') . '</span> <span class="prc-cut r">$' . number_format($data->prod_price, 2, '.', '') . '</span>';
                                            } else {
                                                echo 'Price:<span class="prc-p">$' . number_format($data->prod_price, 2, '.', '') . '</span>';
                                            }
                                        } else {
                                            $productPrice = number_format($data->prod_price, 2, '.', '');
                                            echo '<span class="prc-p">$' . number_format($data->sale_price, 2, '.', '') . '</span> <span class="prc-cut r">$' . number_format($data->prod_price, 2, '.', '') . '</span>';
                                        }
                                    } else {
                                        $productPrice = number_format($data->prod_price, 2, '.', '');
                                        echo '$' . number_format($data->prod_price, 2, '.', '');
                                    }
                                    ?>                                                                
                                                                <!--
                                                                <?php if (isset($data->sale_price)) { ?>
                                                                <p class="prc-p"><?php echo "$ " . number_format($data->sale_price, 2); ?></p>
                                                                <p class="prc-cut r"><?php echo "$ " . number_format($data->prod_price, 2); ?></p>
                                                                <?php }else { ?>
                                                                    <p class=""><?php echo "$ " . number_format($data->prod_price, 2); ?></p>
                                                               <?php } ?>
                                                                    -->
                                                            </td>
                                                            <td class="text-center">
                                                                <p>
                                                               $<?php echo ($data->shipping == "Charge for Shipping")?number_format($data->ship_price, 2, '.', ''):"00.00"; ?>
                                                                </p>
                                                            </td>
                                                            <td class="text-center"><?= $data->qty ?></td>
                                                            <td class="text-center">
                                                                <p><?php echo (($data->prod_status) == '1' ? "Active" : "Inactive")  ?></p>
                                                            </td>
                                                            <td class="pad-rgt-0 text-center">
                                                                 <?php 
                                                                $produrl = productDetailUrl($data->prod_id);
                                                                ?>
                                                                <a  class="up-fb" target="_blank" href='http://www.facebook.com/sharer.php?u=<?=$produrl.'/'.$data->prod_url?>'></a>
                                                                <a  class="up-twt" target="_blank" href='https://twitter.com/home?status=<?= $data->prod_title ?>%20%20<?= $produrl.'/'.$data->prod_url?>'></a>
                                                                <a href="<?= base_url() ?>edit-product/<?= $data->prod_url ?>" class="edt-btn"></a>
                                                                <a href="<?= base_url() ?>delete-product/<?= $data->prod_url ?>" class="delt-btn fl-lft"></a>	
                                                               
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--Select option ends here-->   

                            <?php if(count($results) > 0){?>
                            <div class="shop-right-third">
                                <div class="shop-right-third-lft">
                                    <p>
                                        <?=$start?> - <?php if($end > $total_records) {echo $total_records;} else {echo $end; }?> of <?=$total_records?><!--  in Category Name -->
                                    </p>
                                </div>
                                <div class="shop-right-third-rgt">
                                    <ul>
                                        <?php
                                        if (isset($links)) {
                                            echo $links;
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <!--right section starts here-->
            </div>
        </div>
    </div>
</section>
<!--Blog main ends  here-->
<script>

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $(document).ready(function () {
        $(".bulk").click(function () {
            $(".blk-prd-upld").toggle(1000);
        });
    });
    $(document).ready(function () {
        $.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg !== value;
        }, "This field is required.");
        $(function validate() {
            // body...
            var rules = {
                rules: {
                    search_value: {
                        required: true
                    },
                    select_search: {valueNotEquals: "0"}
                },
                errorPlacement: function (error, element) {
                    var name = $(element).attr("name");
                    error.appendTo($("#" + name + "_validate"));
                },
            };
            $('#search_product_form').validate(rules);
        });
    });
</script>
<script>

    $('#delete_selected').click(function () {
        var checked = $('input[name="id_checkbox"]:checked').length > 0;
        if (checked == false) {
            setTimeout(function () {
                $('#new_message').html('');
                $('#new_message').removeClass("alert alert-danger");
                $('#new_message').addClass("hide");
            }, 5000);
            $('#new_message').html('Please Select Product');
            $('#new_message').removeClass("hide");
            $('#new_message').addClass("alert alert-danger");

        } else {
            var checkedValues = $('input[name="id_checkbox"]:checked').map(function () {
                return this.value;
            }).get();
            var url = "<?= base_url() ?>bluk_product_delete";
            $.ajax({
                url: url,
                type: "POST",
                dataType: "json",
                data: {"delete_value": checkedValues,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                success: function (data) {
                    setTimeout(function () {
                        $('#new_message').html('');
                        $('#new_message').removeClass("alert alert-success");
                        $('#new_message').addClass("hide");
                    }, 5000);
                    $('input[name="id_checkbox"]:checked').each(function () {
                        var idd = this.value;
                        $('#' + idd).hide();
                    });
                    $('#new_message').html(data.msg);
                    $('#new_message').addClass("alert alert-success");
                    $('#new_message').removeClass("hide");
                    $('html, body').animate({
                        scrollTop: $("#message").offset().top
                    }, 2000);
                }
            });
        }
    });
    $(function () {
        $('a').filter('a[rel^="next"]').addClass('page-right');
        $('a').filter('a[rel^="prev"]').addClass('page-left');
    });

    $(function () {
        $('.message').append('<span class="close" title="Dismiss"></span>');
        //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
        $('.message .close').hover(
                function () {
                    $(this).addClass('hover');
                },
                function () {
                    $(this).removeClass('hover');
                }
        );

        $('.message .close').click(function () {
            $(this).parent().fadeOut('slow', function () {
                $(this).remove();
            });
        });
    });
    $(document).ready(function () {
        $('#copy_selected').click(function () {
            var checked = $('input[name="id_checkbox"]:checked').length > 0;
            if (checked == false) {
                setTimeout(function () {
                    $('#new_message').html('');
                    $('#new_message').removeClass("alert alert-danger");
                    $('#new_message').addClass("hide");
                }, 5000);
                $('#new_message').html('Please Select Product');
                $('#new_message').removeClass("hide");
                $('#new_message').addClass("alert alert-danger");
            } else {
                var checkedValues = $('input[name="id_checkbox"]:checked').map(function () {
                    return this.value;
                }).get();
                var url = "<?= base_url() ?>bluk_product_copy";
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: "json",
                    data: {"copy_value": checkedValues,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                    success: function (data) {
                        setTimeout(function () {
                            $('#new_message').html('');
                            $('#new_message').removeClass("alert alert-success");
                            $('#new_message').addClass("hide");
                            window.location.reload();
                        }, 5000);
                        $('#new_message').html(data.msg);
                        $('#new_message').addClass("alert alert-success");
                        $('#new_message').removeClass("hide");
                    }
                });
            }
        });

        $(document).on('click','.removefilename', function(){
            $('#bulkFile').val('');
        });
    });
</script>