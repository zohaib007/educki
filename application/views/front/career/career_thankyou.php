<!--banner  starts here-->
<section class="whted-bg">
	<div class="frg-layr"></div>
    	<!--Breadcrumbs starts here-->
        <div class="container">
            <div class="row">
                <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
                    <div class="auto-container">
                    	<div class="brd-whit">
                        <div class="brd-sec">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?= base_url()?>">Home</a></li>
                               <li class="breadcrumb-item"><a href="<?=base_url()?><?=get_page_url(12,'tbl_pages','pg_id')->pg_url;?>">Careers</a></li>
                              <li class="breadcrumb-item active">  Thank You</li>
                            </ol>
                        </div>
                        
                        <h2> Thank You</h2>	
                        </div>
                        
                    </div>
                </div>
            </div>
        
        </div>
        <!--Breadcrumbs ends  here-->

</section>
<!--banner  starts here-->



<!--main sec starts here-->
<section class="thnk-sec">
	<div class="container">
    	<div class="row">
        	<div class="auto-container"/>
        		
                <div class="thank-inner">
                	<h2>Thank You</h2>
                    <p><?=$message.'.'?></p>
                
                </div>
                
        	</div>
        </div>
    </div>
</section>

<!--main sec  ends here-->

<script>
	$('.collapse').on('shown.bs.collapse', function(){
		$(this).parent().find(".arrow-rgt").removeClass("arrow-rgt").addClass("arrow-down");
		
		}).on('hidden.bs.collapse', function(){
		$(this).parent().find(".arrow-down").removeClass("arrow-down").addClass("arrow-rgt");
	});
</script>

<script>
// trigger upload on space & enter
// = standard button functionality
$("#buttonlabel span[role=button]").bind('keypress keyup', function(e) {
  if(e.which === 32 || e.which === 13){
    e.preventDefault();
    $('#fileupload').click();
  }    
});

// return chosen filename to additional input
$('#fileupload').change(function(e) {
  var filename = $('#fileupload').val().split('\\').pop();
  $('#filename').val(filename);
  $('#filename').focus();
});
</script>
</body>


</html>