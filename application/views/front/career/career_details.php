<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?=base_url()?><?=get_page_url(12,'tbl_pages','pg_id')->pg_url;?>">Careers</a></li>
                        <li class="breadcrumb-item">Details</li>
                    </ol>
                </div>
                <div class="car-det-wrp">
                    <div class="crd-det-main">
                        <div class="crd-det-upr">
                            <h2><?= $title ?></h2>
                        </div>
                        <div class="crd-det-midd">
                            <p>
                                <?= clean_string($description) ?>
                            </p>
                        </div>
                        <div class="crd-det-btm">
                            <div class="jb-frmsec">
                                <div class="jobfrm-up">
                                    <h5>Apply for this Job</h5>
                                    <a href="<?=get_social_media_links()->linkedin?>" target="_blank">
                                        <img src="<?=base_url()?>front_resources/images/job-lnklnd.png" alt="" title=""/>
                                    </a>
                                    <span>(Optional)</span>
                                </div>
                                <?php echo form_open_multipart('apply_career', array('name' => 'apply_career', 'id' => 'applycareer_form', 'enctype' => 'multipart/form-data')); ?>
                                <div class="jobfrm-md">
                                    <div class="jobfrm-md-inr">
                                        <div class="jobfrm-md-inr-rw">
                                            <div class="jobfrm-md-inr-lft">
                                                <h6>First Name<span>*</span></h6>
                                            </div>
                                            <div class="jobfrm-md-inr-rgt">
                                                <input type="text" name="fname" value="<?php echo set_value('fname'); ?>" />
                                                <div class="error" id="fname_validate"></div>
                                            </div>
                                        </div>
                                        <div class="jobfrm-md-inr-rw">
                                            <div class="jobfrm-md-inr-lft">
                                                <h6>Last Name<span>*</span></h6>
                                            </div>
                                            <div class="jobfrm-md-inr-rgt">
                                                <input type="text" name="lname" value="<?php echo set_value('lname'); ?>" />
                                                <div class="error" id="lname_validate"></div>
                                            </div>
                                        </div>
                                        <div class="jobfrm-md-inr-rw">
                                            <div class="jobfrm-md-inr-lft">
                                                <h6>Email<span>*</span></h6>
                                            </div>
                                            <div class="jobfrm-md-inr-rgt">
                                                <input type="text" name="email" id="email_text" value="<?php echo set_value('email'); ?>" onkeyup="check_apply()"/>
                                                <div class="error" id="email_validate"></div>
                                            </div>
                                        </div>
                                        <div class="jobfrm-md-inr-rgt right-carbtn">
                                            <div class="input-group btn-upload uty-upload-1">
                                                <span class="input-group-btn uty-upload-2">
                                                    <div class="btn btn-upload-input uty-upload-3">
                                                        <span class="btn-upload-input-title">Upload Resume/CV</span>
                                                <input type="file" name="resume"/>
                                            </div>
                                            <div class="error" id="resume_validate"></div>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="jobfrm-md-inr-rgt right-carbtn">
                                        <div class="input-group btn-upload uty-upload-1">
                                            <span class="input-group-btn uty-upload-2">
                                                    <div class="btn btn-upload-input2 uty-upload-3">
                                                        <span class="btn-upload-input-title2">Upload Cover Letter</span>
                                            <input type="file" name="cover" />
                                            <!-- rename it -->
                                        </div>
                                        <div class="error" id="cover_validate"></div>
                                        </span>
                                    </div>   
                                </div>
                            </div>
                        </div>
                        <div class="jobfrm-thrd">
                            <div class="jobfrm-thrd-inr">
                                <div class="lnk-prof">
                                    <h6>LinkedIn Profile</h6>
                                    <input type="text" name="linkedin" value="<?php echo set_value('linkedin'); ?>" />
                                </div>
                                <div class="lnk-prof">
                                    <h6>Website</h6>
                                    <input type="text" name="website" value="<?php echo set_value('website'); ?>" />
                                </div>
                                <div class="lnk-prof">
                                    <h6>How did you hear about this job?</h6>
                                    <input type="text" name="hear" value="<?php echo set_value('hear'); ?>" />
                                </div>
                            </div>
                        </div>
                        <div class="jobfrm-lst">
                            <div class="jobfrm-thrd-inr">
                                <input type="submit" class="submit" id="apply" value="Submit Application" />
                            </div>
                        </div>
                        <input type="hidden" value="<?= $career_id ?>" id="career_id" name="career_id" />
                        <?php echo form_close(); ?>
                    </div>
                    <div class="jb-share-sec">
                        <div class="share-sec">
                            <p>Share this job:</p>
                            <div class="shr-img">
                                <ul>
                                    <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?=base_url()?>career_details/<?=$career_url?>" class="shr-fb" target="_blank"></a></li>
                                    <li><a href="https://twitter.com/home?status=<?=$title;?>%20%20%20<?=base_url()?>career_details/<?=$career_url?>" class="shr-fb2" target="_blank"></a></li>
                                    <li><a href="http://pinterest.com/pin/create/button/?url=<?=base_url('career_details/'.$career_url)?>&media=<?=base_url('front_resources/images/global.png')?>&description=<?=$title?>" class="shr-fb3" target="_blank">
                                    </a>
                                    </li>
                                    <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?=base_url()?>career_details/<?=$career_url?>&title=<?=$title?>&summary=" class="shr-fb4" target="_blank"></a></li>
                                    <li><a href="mailto:?Subject=<?=$title?>&body=<?=base_url()?>career_details/<?=$career_url?>" class="shr-fb5"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<script>
'use strict';
$(function() {
   var btnTitle = $(".btn-upload-input-title").html();
   var btnTitleHtml = $.parseHTML(btnTitle);
   $(".btn-upload-input input:file").change(function (){
      if( this.files && this.files.length >= 1 ) {
         var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
               $(".btn-upload-input-title").text(file.name);
            }
            reader.readAsDataURL(file);
      }
      else {
         $(".btn-upload-input-title").html(btnTitle);
      }
        
    });   
});
</script>


<script>
'use strict';
$(function() {
   var btnTitle = $(".btn-upload-input-title2").html();
   var btnTitleHtml = $.parseHTML(btnTitle);
   $(".btn-upload-input2 input:file").change(function (){
      if( this.files && this.files.length >= 1 ) {
         var file = this.files[0];
            var reader = new FileReader();
            // Set preview image into the popover data-content
            reader.onload = function (e) {
               $(".btn-upload-input-title2").text(file.name);
            }
            reader.readAsDataURL(file);
      }
      else {
         $(".btn-upload-input-title2").html(btnTitle);
      }
        
    });   
});
</script>
<script>
$(document).ready(function() {
    $.validator.addMethod("valueNotEquals", function(value, element, arg) {
        return arg !== value;
    }, "This field is required.");
    $(function validate() {
        // body...
        var rules = {
            rules: {
                fname: {
                    minlength: 2,
                    required: true
                },
                lname: {
                    minlength: 2,
                    required: true
                },
                email: {
                    minlength: 2,
                    maxlength: 50,
                    required: true,
                    email: true
                },
                resume: {
                  extension: "docx|doc|pdf|ppt|txt",
                  required: true
                },
                cover: {
                  extension: "docx|doc|pdf|ppt|txt",
                  required: true
                }
            },
            errorPlacement: function(error, element) {
                var name = $(element).attr("name");
                error.appendTo($("#" + name + "_validate"));
            },
        };
        $('#applycareer_form').validate(rules);
    });
});
</script>
<script type="text/javascript">
function check_apply(){
    var email = $('#email_text').val();
    var career_id = $('#career_id').val();
    if(email!=''){
        $.ajax({
            url: '<?=base_url('check-apply')?>',
            type: 'POST',
            data: { email: email, career_id: career_id, <?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>' },
        })
        .done(function(result){
            if (result == 1){
                //save succesfully
                $('#email_validate').html('You have already applied for this opening.');
                $('#apply').attr('disabled',true);
            }else{
                $('#email_validate').html('');
                $('#apply').attr('disabled',false);
            }
        })
    }else{
        $('#email_validate').html('');
        $('#apply').attr('disabled',false);
    }
}
</script>



