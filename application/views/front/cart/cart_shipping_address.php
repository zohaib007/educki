<!--Cart main section starts here-->
<div class="ovr-flw-hid">
    <div class="container">
        <div class="row">
            <div class="auto-container-hed2">
                <div class="col-md-8">
                    <div class="cart-left-mian">
                        <div class="cart-left-mian-hed">
                            <h2>Shopping Cart</h2>
                        </div>
                        <div class="brd-sec">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo ($this->uri->segment(1) == "cart") ? "javascript:;" : base_url() . "cart"; ?>">Cart</a></li>
                                <li class="breadcrumb-item"><a class="<?php
                                    if ($this->uri->segment(1) == "cart-shipping") {
                                        echo "active";
                                    }
                                    ?>" href="<?php
                                                               if ($this->uri->segment(1) == "cart-shipping") {
                                                                   echo "javascript:;";
                                                               }
                                                               ?>">Shipping Address</a></li>
                                <li class="breadcrumb-item"><a href="javascript:;"> Payment &amp; Billing</a></li>
                                <li class="breadcrumb-item"> <a href="javascript:;">Confirm Order</a></li>
                            </ol>
                        </div>

                        <div class="crt-shp-addr-main">
                            <div class="crt-shp-addr-hed">
                                <h3>Shipping Address</h3>
                            </div> 

                            <div class="crt-shp-addr-frm">
                                <?php echo form_open_multipart('', array('name' => 'shipping_address', 'id' => 'add_shipping_form')); ?>
                                <div class="crt-shp-addr-frm-rw">
                                    <div class="selct-ful-width">
                                        <select name="select_address" id="select_address">
                                            <option value="0" <?= set_select('select_address', '0') ?>>Select</option>
                                            <?php foreach ($address_list as $list) { ?>
                                                <option value="<?= $list->add_id ?>"><?=($default_address_list->nick_name == $list->nick_name) ? "Default Shipping Address" : $list->nick_name?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="use-diff-billng-addr-rw">
                                    <div class="selct-ful-width crt-shp-addr-frm-inr">
                                        <input type="text" name="nick_name" id="nick_name" value="<?=$current->shipping_nick_name?>" placeholder="Nickname *"/>
                                        <div class="error" id="nick_name_validate"><?php echo form_error('nick_name') ?></div>
                                    </div>
                                </div> 

                                <div class="crt-shp-addr-frm-rw">
                                    <div class="crt-shp-addr-frm-inr">
                                        <input type="text" name="first_name" id="first_name" value="<?=$current->shipping_first_name?>" placeholder="First Name *"/>
                                        <div class="error" id="first_name_validate"><?php echo form_error('first_name') ?></div>
                                    </div>

                                    <div class="crt-shp-addr-frm-inr mrg-rgt-0">
                                        <input type="text" name="last_name" id="last_name" value="<?=$current->shipping_last_name?>" placeholder="Last Name *"/>
                                        <div class="error" id="last_name_validate"><?php echo form_error('last_name') ?></div>
                                    </div>
                                </div>

                                <div class="crt-shp-addr-frm-rw">
                                    <div class="crt-shp-addr-frm-inr">
                                        <select name="country" id="country">
                                            <option value="0" <?= set_select('country', '0') ?>>Select Country</option>
                                            <?php foreach ($allcountries as $country) { ?>
                                            <option value="<?= $country['iso'] ?>"<?php echo ($current->shipping_country =='' && $country['iso']=='US') ? 'selected' : ""; ?> <?php echo ($current->shipping_country == $country['iso']) ? 'selected' : ""; ?>><?= $country['name'] ?></option>
                                            <!-- <option value="<?= $country['iso'] ?>"><?= $country['name'] ?></option> -->
                                            <?php } ?>
                                        </select>
                                        <div class="error" id="country_validate"><?php echo form_error('country') ?></div>
                                    </div>

                                    <div class="crt-shp-addr-frm-inr mrg-rgt-0">
                                        <div id="us-states" <?= ($current->shipping_country == "US") ? 'style="display: block"' : 'style="display: none"' ?> >
                                            <select name="state" id="state" class="hide">
                                                <option value="0" >Select State</option>
                                                <?php foreach ($allstate as $state) { ?>
                                                        <!--<option value="<?= $state['stat_id'] ?>" <?php echo (isset($default_address_list->state) ? $default_address_list->state == $state['stat_id'] ? 'selected' : '' : ""); ?>><?= $state['stat_name'] ?></option>-->
                                                    <option value="<?= $state['stat_id'] ?>" <?php echo ($current->shipping_state == $state['stat_id']) ? 'selected' : ""; ?>><?= $state['stat_name'] ?></option>
                                                <?php } ?>
                                            </select>
                                            <div class="error" id="state_validate"></div>
                                        </div>
                                        <div id="other-states" <?= ($current->shipping_country != "US") ? 'style="display: block"' : 'style="display: none"' ?> >
                                            <input type="text" id="other_state" name="other_state" value="<?=$current->shipping_state_other?>" placeholder="State *"/>
                                        </div>
                                        <div class="error" id="other_state_validate" ><?php echo form_error('other_state') ?></div> 
                                    </div>
                                </div>

                                <div class="crt-shp-addr-frm-rw">
                                    <div class="crt-shp-addr-frm-inr">
                                        <input type="text" name="street" id="street" value="<?=$current->shipping_street?>" placeholder="Street *"/>
                                        <div class="error" id="street_validate"><?php echo form_error('street') ?></div>
                                    </div>

                                    <div class="crt-shp-addr-frm-inr mrg-rgt-0">
                                        <input type="text" name="city" id="city" value="<?=$current->shipping_city?>" placeholder="City *"/>
                                        <div class="error" id="city_validate"><?php echo form_error('city') ?></div>
                                    </div>
                                </div>

                                <div class="crt-shp-addr-frm-rw">
                                    <div class="crt-shp-addr-frm-inr">
                                        <input type="text" name="zip_code" id="zip_code" value="<?=$current->shipping_zip?>" placeholder="Zip *"/>
                                        <div class="error" id="zip_code_validate"><?php echo form_error('zip_code') ?></div>
                                    </div>

                                    <div class="crt-shp-addr-frm-inr mrg-rgt-0">
                                        <input type="text"  name="phone" id="phone" value="<?=$current->shipping_phone?>" placeholder="Telephone"/>
                                        <!--<div class="error" id="phone_validate"><?php echo form_error('phone') ?></div>-->
                                    </div>
                                </div>                 

                                <div class="crt-shp-addr-frm-rw">
                                    <div class="crt-shp-addr-frm-inr">
                                        <input type="checkbox" id="save_address" name="save_address">
                                        <label for="save_address"><span></span><p>Save Address</p></label>
                                        <!-- <input type="hidden" id="nick_name"  name="nick_name" value=""> -->
                                        <input type="hidden" id="user_id" name="user_id" value="<?= $userid ?>">
                                        <input type="hidden" id="default_shipping" name="default_shipping" value="Y">
                                        <input type="submit" value="Next" id="address"/>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>               
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="cart-right-main">
                        <div class="boxgrey-rp"></div><!-- Grey Cover -->
                        <div class="cart-right-hed">
                            <h6>Order Details</h6>
                        </div>
                        <div class="cart-right-lowr">
                            <div class="cart-right-rw">
                                <div class="cart-right-rw-lft">
                                    Subtotal
                                </div>
                                <div class="cart-right-rw-rgt">
                                    $<?= number_format($cart_sub_total, 2, '.', '') ?>
                                </div>
                            </div>

                            <div class="cart-right-rw">
                                <div class="cart-right-rw-lft">
                                    Discount
                                </div>
                                <div class="cart-right-rw-rgt">
                                    $<?= number_format($discount_amount, 2, '.', '') ?>
                                </div>
                            </div>

                            <div class="cart-right-rw">
                                <div class="cart-right-rw-lft">
                                    Shipping Total
                                </div>
                                <div class="cart-right-rw-rgt">
                                    $<?= number_format($shipment_sub_total, 2, '.', '') ?>
                                </div>
                            </div>
                        </div>
                        <div class="cart-right-rw">
                            <div class="cart-right-rw-lft">
                                <h6>Total</h6>
                            </div>
                            <div class="cart-right-rw-rgt">
                                <h6>$<?= number_format($total_shipment_amount, 2, '.', '') ?></h6>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Cart main section ends here-->
<script>
    
    $(function () {
        $('#country').change(function () {
            if ($('#country').val() == 'US') {
                $('#us-states').show("slow");
                $('#state').removeClass('hide');
                $('#other_state').val('');
                $('#other-states').hide("slow");
            } else {
                $('#us-states').hide("slow");
                $('#other-states').show("slow");
            }
        });
    });
    $(function () {
        $('#select_address').change(function () {
            var value = $('#select_address').val();
            var url = "<?= base_url() ?>loadaddress";
            $.ajax({
                url: url,
                type: "POST",
                dataType: "json",
                data: {"addressId": value,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                success: function (data) {
                    console.log(data);
                    $("#first_name").val(data.first_name);
                    $("#last_name").val(data.last_name);
                    $("#street").val(data.street);
                    $("#city").val(data.city);
                    $("#country").val(data.country);
                    if (data.country == 'US') {                        
                        $('#us-states').show("slow");
                        $('#other_state').val('');
                        $('#other-states').hide("slow");
                        $("#country").click();
                    } else {                        
                        $('#us-states').hide("slow");
                        $('#other-states').show("slow");
                    }
                    /*$('#state').prop(data.state, true);*/
                    $("#state").val(data.state);
                    $('#country').change();
                    $("#other_state").val(data.state_other);
                    $("#zip_code").val(data.zip);
                    $("#phone").val(data.phone);
                    $("#nick_name").val(data.nick_name);

                }
            });
        });
    });
    $("#cat-code").change(function () {
        var value = this.value;
        if (value == "1") {
            $(".clk-shp-sho").show();
        }
        else {
            $(".clk-shp-sho").hide();
        }
    });
    $(document).ready(function () {
        $.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg !== value;
        }, "This field is required.");
        $(function validate() {
            // body...
            var rules = {
                rules: {
                    first_name: {
                        required: true
                    },
                    last_name: {
                        required: true
                    },
                    street: {
                        required: true
                    },
                    city: {
                        required: true
                    },
                    nick_name: {
                        required: true
                    },
                    zip_code: {
                        required: true
                    },
                    other_state: {
                        required: {
                            depends: function (element) {
                                if ($('#country').val() !== 'US') {
                                    return true;
                                }else{
                                    return false;
                                }
                            },
                        },
                    },
                    state: {valueNotEquals: "0"},
                    country: {valueNotEquals: "0"}
                },
                errorPlacement: function (error, element) {
                    var name = $(element).attr("name");
                    error.appendTo($("#" + name + "_validate"));
                },
            };

            $('#add_shipping_form').validate(rules);
        });
    });
</script>
<script>
    function price(id) {
        var qty = $("#" + id).val();
        var prod_price = $(".prod_price_" + id).val();
//        var total_price = $("#total_price_"+id).val();
        var total_price = qty * prod_price;
        $("#total_price_" + id).html("$" + total_price);
        $(".test .text-field").each(function () {
            alert($(this).val());
        });
    }

$(document).ready(function(){
    $('#country').change();
});
</script>