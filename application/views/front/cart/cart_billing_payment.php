<!--Cart main section starts here-->
<div class="ovr-flw-hid">
    <div class="container">
        <div class="row">
            <div class="auto-container-hed2">
                <div class="col-md-8 cart-col-md-8">
                    <div class="cart-left-mian">
                        <?php if ($this->session->flashdata('paypal_error') != '') { ?>
                            <div class="alert alert-danger fade in alert-dismissable">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                <?php echo $this->session->flashdata('paypal_error'); ?>
                            </div>
                        <?php } ?>
                        <div class="cart-left-mian-hed">
                            <h2>Shopping Cart</h2>
                        </div>
                        <div class="brd-sec">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo ($this->uri->segment(1) == "cart") ? "javascript:;" : base_url() . "cart"; ?>">Cart</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo ($this->uri->segment(1) == "cart-shipping") ? "javascript:;" : base_url() . "cart-shipping"; ?>">Shipping Address</a></li>
                                <li class="breadcrumb-item"><a class="<?php
                                    if ($this->uri->segment(1) == "cart-payment") {
                                        echo "active";
                                    }
                                    ?>" href="<?php
                                                               if ($this->uri->segment(1) == "cart-payment") {
                                                                   echo "javascript:;";
                                                               }
                                                               ?>">Payment &amp; Billing</a></li>
                                <li class="breadcrumb-item"> <a href="javascript:;">Confirm Order</a></li>
                            </ol>
                        </div>

                        <div class="crt-bil-pay-main">

                            <div class="crt-bil-pay-shp-addr">
                                <div class="well">
                                    <h4>Shipping address</h4>
                                    <span>Address goes here</span>
                                    <a href="<?= base_url() ?>cart-shipping">Edit</a>
                                </div>
                            </div>
                            <?php echo form_open_multipart('', array('name' => 'billing_address', 'id' => 'add_billing_form')); ?>
                            <div class="crt-bil-pay-pyinfo">
                                <h3>Payment Information</h3>
<!--                                <div class="well">
                                    <input type="radio" id="r003" name="payment_method" value="1" checked>
                                    <label for="r003"><span></span><p>Credit Card</p></label>

                                    <img src="<?= base_url() ?>front_resources/images/visa-master.png" alt="" title=""/>
                                </div>

                                <div class="crt-credt-crd">

                                    <div class="crd-row">
                                        <div class="crd-row-cdno">
                                            <input type="text" placeholder="Card Number *"/>
                                        </div>
                                    </div>

                                    <div class="crd-row">
                                        <div class="crd-nam">
                                            <input type="text" placeholder="Name on Card *"/>
                                        </div>
                                        <div class="crd-my-cv">
                                            <input type="text" placeholder="MM / YY *" id="creditCardDate"/>
                                        </div>
                                        <div class="crd-my-cv mrg-rgt-0">
                                            <input type="text" placeholder="CVC" id="cvcNumber"/>
                                        </div>
                                    </div>


                                </div>-->

                                <!--<div class="well brd-top-0">-->
                                <div class="well">
                                    <input type="radio" id="r004" name="payment_method" value="2" checked>
                                    <label for="r004"><span></span><p><img src="<?= base_url() ?>front_resources/images/paypal3.png" alt="" title="" /></p></label>

                                    <!-- <img src="<?= base_url() ?>front_resources/images/visa-master.png" alt="" title=""/> -->
                                </div>

                                <!--<div class="crt-bil-paypal" style="display:none;">-->
                                <div class="crt-bil-paypal">
                                    <div class="crt-bil-paypal-lft">
                                        <h6>PayPal is the safer, easir way to pay</h6>
                                        <p>No matter where you shop, we keep your financial information secure</p>
                                        <img src="<?= base_url() ?>front_resources/images/paypal-logo.png" alt="" title=""/>
                                    </div>

                                    <div class="crt-bil-paypal-right">
                                            <img src="<?= base_url() ?>front_resources/images/paypal-checkout.png" alt="" title=""/>
                                        <p>
                                            The safer, easier way to pay
                                        </p>
                                    </div>

                                </div>
                            </div>


                            <div class="crt-bil-pay-pyinfo">
                                <h3>Billing Address</h3>
                                <?php if ($default_address_list != '') { ?>
                                    <div class="well"> <!-- shw-btn -->
                                        <input type="radio" id="us1" value="1" <?=($restore=='1')?'checked':''?> name="us" class="shw-btn">
                                        <label for="us1"><span></span><p>Use default</p></label>
                                    </div>
                                <?php } ?>
                                <div class="well <?= ($default_address_list ? "brd-top-0" : "") ?>"> <!-- shw-btn -->
                                    <input type="radio" id="us2" value="2" <?=($restore=='2')?'checked':''?> name="us" class="shw-btn">
                                    <label for="us2"><span></span><p>Same as shipping address</p></label>
                                </div>

                                <div class="well brd-top-0">
                                    <input type="radio" id="us3" value="3" <?=($restore=='3')?'checked':''?> name="us">
                                    <label for="us3"><span></span><p>Use a different billing address</p></label>
                                </div>
                                    <div class="use-diff-billng-addr" id="default_billing_form">

                                        <div>
                                            <div class="use-diff-billng-addr-rw">
                                                <div class="selct-ful-width">
                                                    <select name="select_address" id="select_address">
                                                        <option value="0" <?= set_select('select_address', '0') ?>>Select</option>
                                                        <?php foreach ($address_list as $list) { ?>
                                                            <option value="<?= $list->add_id ?>"><?= ($default_address_list->nick_name == $list->nick_name) ? "Default Billing Address" : $list->nick_name ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="use-diff-billng-addr-rw">
                                                <div class="use-diff-billng-addr-bx selct-ful-width">
                                                    <input type="text" name="nick_name" id="nick_name" value="<?=($restore=='3')?$current->billing_nick_name:''?>" placeholder="Nickname *"/>
                                                    <div class="error" id="nick_name_validate"><?php echo form_error('nick_name') ?></div>
                                                </div>
                                            </div>

                                            <div class="use-diff-billng-addr-rw">
                                                <div class="use-diff-billng-addr-bx">
                                                    <input type="text" name="first_name" id="first_name" value="<?=($restore=='3')?$current->billing_first_name:''?>" placeholder="First Name *"/>
                                                    <div class="error" id="first_name_validate"><?php echo form_error('first_name') ?></div>
                                                </div>
                                                <div class="use-diff-billng-addr-bx mrg-rgt-0">
                                                    <input type="text" name="last_name" id="last_name" value="<?=($restore=='3')?$current->billing_last_name:''?>" placeholder="Last Name *"/>
                                                    <div class="error" id="last_name_validate"><?php echo form_error('last_name') ?></div>
                                                </div>
                                            </div>

                                            <div class="use-diff-billng-addr-rw">
                                                <div class="use-diff-billng-addr-bx">
                                                    <select name="country" id="country">
                                                        <option value="0" <?= set_select('country', '0') ?>>Select Country</option>
                                                        <?php foreach ($allcountries as $country) { ?>
                                                                <option value="<?= $country['iso'] ?>"<?php echo ($current->billing_country =='' && $country['iso']=='US') ? 'selected' : ""; ?> <?php echo ($current->billing_country == $country['iso'] && $restore=='3') ? 'selected' : ""; ?>><?= $country['name'] ?></option>
                                                            <!-- <option value="<?= $country['iso'] ?>"><?= $country['name'] ?></option> -->
                                                        <?php } ?>
                                                    </select>
                                                    <div class="error" id="country_validate"><?php echo form_error('country') ?></div>
                                                </div>
                                                <div class="use-diff-billng-addr-bx mrg-rgt-0">
                                                    <div id="us-states" <?= ($current->billing_country == "US") ? 'style="display: block"' : 'style="display: none"' ?> >
                                                        <select name="state" id="state">
                                                            <option value="0" <?= set_select('state', '0') ?>>Select State</option>
                                                            <?php foreach ($allstate as $state) { ?>
                                                                    <option value="<?= $state['stat_id'] ?>" <?php echo ($current->billing_state == $state['stat_id'] && $restore=='3') ? 'selected' : ""; ?>><?= $state['stat_name'] ?></option>
                                                            <?php } ?>
                                                        </select>
                                                        <div class="error" id="state_validate"></div> 
                                                    </div>
                                                    <div id="other-states" <?= ($current->billing_country != "US") ? 'style="display: block"' : 'style="display: none"' ?> >
                                                        <input type="text" id="other_state" name="other_state" value="<?=($restore=='3')?$current->billing_state_other:''?>" placeholder="State *"/>
                                                    </div>
                                                    <div class="error" id="other_state_validate"></div>
                                                </div>
                                            </div>

                                            <div class="use-diff-billng-addr-rw">
                                                <div class="use-diff-billng-addr-bx">
                                                    <input type="text" name="street" id="street" value="<?=($restore=='3')?$current->billing_street:''?>" placeholder="Street *"/>
                                                    <div class="error" id="street_validate"><?php echo form_error('street') ?></div>
                                                </div>
                                                <div class="use-diff-billng-addr-bx mrg-rgt-0">
                                                    <input type="text" name="city" id="city" value="<?=($restore=='3')?$current->billing_city:''?>" placeholder="City *"/>
                                                    <div class="error" id="city_validate"><?php echo form_error('city') ?></div>
                                                </div>
                                            </div>

                                            <div class="use-diff-billng-addr-rw">
                                                <div class="use-diff-billng-addr-bx">
                                                    <input type="text" name="zip_code" id="zip_code" value="<?=($restore=='3')?$current->billing_zip:''?>" placeholder="Zip *"/>
                                                    <div class="error" id="zip_code_validate"><?php echo form_error('zip_code') ?></div>
                                                </div>
                                                <div class="use-diff-billng-addr-bx mrg-rgt-0">
                                                    <input type="text"  name="phone" id="phone" value="<?=($restore=='3')?$current->billing_phone:''?>" placeholder="Telephone"/>
                                                    <!--<div class="error" id="phone_validate"><?php echo form_error('phone') ?></div>-->
                                                </div>
                                            </div>                                            

                                            <div class="use-diff-billng-addr-rw crt-shp-addr-frm-inr">
                                                <input type="checkbox" id="save_address" name="save_address">
                                                <label for="save_address"><span></span><p>Save Address</p></label>
                                            </div>
                                        </div>


                                    </div> 
                                <div class="use-diff-billng-addr-rw" id="form_submit" >
                                    <div class="use-diff-billng-addr-bx">
                                        <!-- <input type="hidden" id="nick_name" name="nick_name" value=""> -->
                                        <input type="hidden" id="user_id" name="user_id" value="<?= $userid ?>">
                                        <input type="hidden" id="default_billing" name="default_billing" value="Y">
                                        <input type="submit" value="Next"/>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 cart-col-md-4">
                    <div class="cart-right-main uty-cart-payment-1">
                        <div class="boxgrey-rp"></div><!-- Grey Cover -->
                        <div class="cart-right-hed">
                            <h6>Order Details</h6>
                        </div>
                        <div class="cart-right-lowr">
                            <div class="cart-right-rw">
                                <div class="cart-right-rw-lft">
                                    Subtotal
                                </div>
                                <div class="cart-right-rw-rgt">
                                    $<?= number_format($cart_sub_total, 2, '.', '') ?>
                                </div>
                            </div>

                            <div class="cart-right-rw">
                                <div class="cart-right-rw-lft">
                                    Discount
                                </div>
                                <div class="cart-right-rw-rgt">
                                    $<?= number_format($discount_amount, 2, '.', '') ?>
                                </div>
                            </div>

                            <div class="cart-right-rw">
                                <div class="cart-right-rw-lft">
                                    Shipping Total
                                </div>
                                <div class="cart-right-rw-rgt">
                                    $<?= number_format($shipment_sub_total, 2, '.', '') ?>
                                </div>
                            </div>
                        </div>
                        <div class="cart-right-rw">
                            <div class="cart-right-rw-lft">
                                <h6>Total</h6>
                            </div>
                            <div class="cart-right-rw-rgt">
                                <h6>$<?= number_format($total_shipment_amount, 2, '.', '') ?></h6>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Cart main section ends here-->
<script>
    $(function () {
        $('#country').change(function () {
            if ($('#country').val() === 'US') {
                $('#us-states').show("slow");
                $('#other_state').val('');
                $('#other-states').hide("slow");
            } else {
                $('#us-states').hide("slow");
                $('#other-states').show("slow");
            }
        });
    });
    $(function () {
        $('#select_address').change(function () {
            var value = $('#select_address').val();
            var url = "<?= base_url() ?>loadaddress";
            $.ajax({
                url: url,
                type: "POST",
                dataType: "json",
                data: {"addressId": value,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                success: function (data) {
                    $("#first_name").val(data.first_name);
                    $("#last_name").val(data.last_name);
                    $("#street").val(data.street);
                    $("#city").val(data.city);
                    $("#country").val(data.country);
                    if (data.country == 'US') {
                        $('#us-states').show("slow");
                        $('#other_state').val('');
                        $('#other-states').hide("slow");
                    } else {
                        $('#us-states').hide("slow");
                        $('#other-states').show("slow");
                    }
                    if (data.state != '') {
                        $("#state").val(data.state);
                    } else {
                        $("#state").val('0');
                    }
                    $("#other_state").val(data.state_other);
                    $("#zip_code").val(data.zip);
                    $("#phone").val(data.phone);
                    $("#nick_name").val(data.nick_name);

                }
            });
        });
    });
    $("#cat-code").change(function () {
        var value = this.value;
        if (value == "1") {
            $(".clk-shp-sho").show();
        }
        else {
            $(".clk-shp-sho").hide();
        }
    });
    $(document).ready(function () {
        $.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg !== value;
        }, "This field is required.");
        $(function validate() {
            // body...
            var rules = {
                rules: {
                    first_name: {
                        required: true
                    },
                    last_name: {
                        required: true
                    },
                    street: {
                        required: true
                    },
                    city: {
                        required: true
                    },
                    nick_name: {
                        required: true
                    },
                    zip_code: {
                        required: true
                    },
                    other_state: {
                        required: {
                            depends: function (element) {
                                if ($('#country').val() !== 'US') {
                                    return true;
                                }else{
                                    return false;
                                }
                            },
                        },
                    },
                    state: {valueNotEquals: "0"},
                    country: {valueNotEquals: "0"}
                },
                errorPlacement: function (error, element) {
                    var name = $(element).attr("name");
                    error.appendTo($("#" + name + "_validate"));
                },
            };

            $('#add_billing_form').validate(rules);
        });
    });
</script>
<script>
    function price(id) {
        var qty = $("#" + id).val();
        var prod_price = $(".prod_price_" + id).val();
//        var total_price = $("#total_price_"+id).val();
        var total_price = qty * prod_price;
        $("#total_price_" + id).html("$" + total_price);
        $(".test .text-field").each(function () {
            alert($(this).val());
        });
    }

</script>
<script>
    
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
//    console.log(charCode);
    if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105)) {
        return false;
    }
    return true;
}    
    
    $(document).ready(function () {
        
        jQuery("#cvcNumber").on("keydown", function(e){
            
            if ( !isNumber(e) ) {
                e.preventDefault();
                return false;
            }            
            
        });
        
        jQuery("#creditCardDate").on("keydown", function(e){
            
            var d = jQuery(this).val();

            if ( !isNumber(e) ) {
                e.preventDefault();
                return false;
            }
            
            var charCode = (e.which) ? e.which : e.keyCode;
            
            if ( d.length > 4 ) {
                
                
                if ( charCode > 36 ){
                    e.preventDefault();
                    return false;
                } 
                    
            }
            
            if ( d.length == 2 ) {
                
                if ( charCode > 36 ){
                    jQuery("#creditCardDate").val(d+"/");
                }
                
            }            
            
        });
        
        $('#r003').click(function () {
            $('.crt-bil-paypal').hide('');
            $('.crt-credt-crd').show('');

        });

        $('#r004').click(function () {
            $('.crt-bil-paypal').show('');
            $('.crt-credt-crd').hide('');

        });

        $('.shw-btn').click(function () {
            if ($('#default_billing_form').length) {
//                $("#without_submit").show('slow');
//                $("#form_submit").hide('slow');
                $("#default_billing_form").hide('slow');
            }
        });

        $('#us3').click(function () {
            $("#default_billing_form").show('slow');
//            $("#without_submit").hide('slow');
//            $("#form_submit").show('slow');

        });



    });
$(document).ready(function(){
    $('#country').change();
    var checked1 = $('#us1').val();
    var checked2 = $('#us2').val();
    var checked3 = $('#us3').val();
    var checked4 = '<?=$restore?>';
    if(checked3==1){
        /*if ($('#default_billing_form').length) {
            $("#default_billing_form").hide('slow');
        }*/
        $("#default_billing_form").show('slow');
    }

    if(checked1==1 || checked2==1){
        if ($('#default_billing_form').length) {
            $("#default_billing_form").hide('slow');
        }
    }
});
</script>