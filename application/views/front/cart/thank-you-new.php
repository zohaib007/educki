
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Thank you</title>

<meta name="Description" content="educki" />
<meta name="Keywords" content="educki" />
<meta name="author" content="Dotlogics" />

<!--header starts here-->
<?php $this->load->view("front/includes/js") ?>
<!--header ends here-->

<!-- Thankyou CSS Starts -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/thankyou.css" />
<!-- Thankyou CSS Ends -->



</head>

<body>

<!-- Thankyou Structure Starts -->
<div class="thankyou-bg">
	<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td>
				<span class="topicon"></span> <!--Top Tick Icon -->
				<h3>Thank You for Purchasing</h3>
				<p>
					Your order number <?=$order?> was completed successfully.
				</p>
				<div class="socialMedia">
					<a href="<?php echo get_social_media_links()->facebook; ?>" target="_blank" class="sociallk ufb" title="Facebook"></a>
					<a href="<?php echo get_social_media_links()->twitter; ?>" target="_blank" class="sociallk utwt" title="Twitter"></a>
					<a href="<?php echo get_social_media_links()->pinterest; ?>" target="_blank" class="sociallk upintrest" title="Pintrest"></a>
					<a href="<?php echo get_social_media_links()->linkedin; ?>" target="_blank" class="sociallk ulkin" title="LinkedIn"></a>
				</div>
				<a href="<?=base_url()?>" class="backhome">Back To Home Page</a>
				<!-- <a href="index.html" id="test">Go Back</a> -->
			</td>
		</tr>
	</table>
</div>
<!-- Thankyou Structure Ends -->


</body>
<?php
$cartId = $this->session->userdata('cartId');
if ($cartId == '') {
    $cartId = $_COOKIE['cartId'];
}

$this->common_model->commonDelete('tbl_cart_detail', 'cart_id', $cartId);
$this->common_model->commonDelete('tbl_cart', 'cart_id', $cartId);
$this->common_model->commonDelete('tbl_cart', 'cart_user_id', $this->session->userdata('userid'));


$this->session->unset_userdata('cartId');
$this->session->unset_userdata('isCart');

delete_cookie('isCart');
delete_cookie('cartId');
delete_cookie('educki_isCart');
delete_cookie('educki_cartId');
/*
$expires = time() - 50;
setcookie('isCart',"FALSE",$expires,$this->data['url'],'/','educki_');
setcookie('cartId',0,$expires,$this->data['url'],'/','educki_');
// //setcookie('userId',0,$expires,$this->data['url'],'/','educki_');
setcookie('educki_isCart', "FALSE", $expires, '/');
setcookie('educki_cartId', 0, $expires, '/');
//setcookie('educki_userId', 0, $expires, '/');
// setcookie('cookie_name', 0, $expires, '/');
// setcookie('eman_eikooc', 0, $expires, '/');*/

$this->session->unset_userdata('same_as_shipping');
$this->session->unset_userdata('cart_proceed');
$this->session->unset_userdata('use_default');
$this->session->unset_userdata('payment_method');
$this->session->unset_userdata('billing_add_user_id');
$this->session->unset_userdata('cart_process');
$this->session->unset_userdata('buyData');
$this->session->unset_userdata('paypal_error');
$this->session->unset_userdata('order_id');

$this->session->unset_userdata('paypal_order_id');
$this->session->unset_userdata('capture_id');
$this->session->unset_userdata('payment_id');
$this->session->unset_userdata('access_token');

?>
</html>