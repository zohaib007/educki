<!--banner  starts here-->
<section class="whted-bg">
    <div class="frg-layr"></div>
    <!--Breadcrumbs starts here-->
    <div class="container">
        <div class="row">
            <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
                <div class="auto-container">
                    <div class="brd-whit">
                        <div class="brd-sec">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                                <li class="breadcrumb-item active">  Thank You</li>
                            </ol>
                        </div>

                        <h2> Thank You</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Breadcrumbs ends  here-->
</section>
<!--banner  starts here-->

<!--main sec starts here-->
<section class="thnk-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container"/>

            <div class="thank-inner">
                <h2>Thank You</h2>
                <p><?= $message ?></p>

            </div>

        </div>
    </div>
</div>
</section>