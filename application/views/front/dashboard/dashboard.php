<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:;">My Account</a></li>
                        <li class="breadcrumb-item active">My Dashboard </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav'); ?>  
                <!--Left section starts here-->
                <!--right section starts here-->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">
                        <div class="ualert-row">
                                <?php
                                if ($this->session->flashdata('msg') != "") {
                                    echo '<div id="message" class="alert alert-success message "><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>' . $this->session->flashdata('msg') . '</div>';
                                }
                                ?>
                        </div>
                        <div class="my-account-right-head">
                            <h5>My Dashboard</h5>
                            <p>Hello <?=@$user->user_fname." ".@$user->user_lname?>! </p>
                            <p>
                            	Welcome to the eDucki Community.  Below you find a snapshot of your Account Dashboard, and everything you need to get started.  See recent account activity, billing, and contact information all at your fingertips.
                            </p>
                        </div>
                        <div class="my-account-right-2">
                            <div class="well">
                                <h4>Recent Orders</h4>
                                <?php if($orders){ ?>	
                                <a href="<?=base_url()?>my-orders">View All</a>
                                <?php } ?>
                            </div>
                            <?php if($orders){ ?>
                            <div class="my-account-right-table">
                                <table class="table">
                                    <colgroup>
                                        <col class="main-dashboard-c1" width="11%" />
                                        <col class="main-dashboard-c2" width="29%" />
                                        <col class="main-dashboard-c3" width="14%" />
                                        <col class="main-dashboard-c4" width="15%" />
                                        <col class="main-dashboard-c5" width="12%" />
                                        <col class="main-dashboard-c6" width="19%" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th class="pad-lft-0">Item</th>
                                            <th>Product Description</th>
                                            <th class="text-center">Date</th>
                                            <th>Order Total</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($orders as $key => $order) { ?>
                                        <?php $o_prod = getOrderProductByOrderId($order->order_id);?>
                                        <?php
                                        
                                        $url = base_url();
                                        //echo productDetailUrl($data['order_prod_id']);
                                        if(productDetailUrl($order->order_prod_id) != '0'){
                                            $url = productDetailUrl($order->order_prod_id).'/'.$order->order_prod_url;
                                        }
                                        ?>
                                        <tr>
                                            <td class="pad-lft-0 nitem-image"><a href="<?=$url?>"><img src="<?= base_url('resources/prod_images/thumb/'.getrodImage($order->order_prod_id))?>"  alt="" title="" class="img-nset" /></a></td>
                                            <td class="nitem-name"><a href="<?=$url?>"><?=$order->order_prod_name?></a></td>
                                            <td class="text-center"><?=date('m-d-Y', strtotime($order->order_date))?></td>
                                            <td class="text-center">$<?=number_format($order->order_grand_total, 2, '.', '')?></td>
                                            <td class="text-center"><?=$order->order_status?></td>
                                            <td class="pad-rgt-0 td-action text-center">
                                                <a href="<?=base_url()?>my-order/<?=$order->order_id?>">View Order</a> <!-- class="view-ord" -->
                                                <span class="lne2">|</span>
                                                <a id="re_order" href="javascript:void(0);" data-orderid="<?=$order->order_id?>" >Reorder</a> <!-- class="reord-ord" -->
                                                <!-- <a id="re_order" href="javascript:void(0);" data-orderid="<?=$order->order_id?>"  onclick="reorder(<?=$order->order_id?>)">Reorder</a> <!-- class="reord-ord" -->
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php }else{ ?>
                            <div id="message" class="alert alert-danger message my-account-right-table"><a href="javascript:;" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>No Data Found</div>
                            <?php } ?>
                            <div class="well">
                                <h4>Account Information</h4>	
                            </div>
                            <div class="accort-info">
                                <div class="accort-info-bx">
                                    <div class="accort-info-bx-tilt">
                                        <h5>Contact Information</h5>
                                        <a href="<?=base_url('my-profile');?>">Edit</a>
                                    </div>
                                    <div class="accort-info-bx-txt">
                                        <p><?=@$user->user_fname." ".@$user->user_lname?></p>
                                        <p><?=@$user->user_email?></p>
                                        <!-- <a href="<?= base_url('change-password'); ?>" class="chang-pawrd">Change Password</a> -->
                                        <?php if(!isset($user->user_facebook_id)){?>
                                        <div class="chng-paswrd">
                                            <input type="checkbox" id="cp1" name="cc">
                                            <label for="cp1"><span></span><p>Change Password</p></label>
                                        </div>
                                        <?php }?>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_open_multipart('check-password', array('name' => 'editprofile', 'id' => 'editprofile_form')); ?>
                            <?php if(!isset($user->user_facebook_id)){?>
                            <div class="change-pas-dv" id="ipt-hid">

                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>Current Password <span>*</span></h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="password" autocomplete="off" name="password" id="password" onblur="check_pass()" placeholder="" />
                                        <div class="error" id="password_validate"></div>
                                    </div>
                                </div>

                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>New Password <span>*</span></h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="password" autocomplete="off" name="new_password" id="new_password" placeholder="" />
                                        <div class="error" id="new_password_validate"></div>
                                    </div>
                                </div>

                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>Confirm New Password <span>*</span></h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="password" name="confirm_password" id="confirm_password" placeholder=""/>
                                        <div class="error" id="confirm_password_validate"></div>
                                    </div>
                                </div>
                                <div class="store-profile-row">
                                    <input class="upld-btn" type="submit" id="submit"/>
                                </div>
                            </div>                
                            <?php }?>
                            <input type="hidden" name="user_id" id="user_id" value="<?=$user->user_id?>">
                            <?php echo form_close(); ?>                            
                            <div class="well">
                                <h4>Address Book</h4>	
                                <a href="<?= base_url('my-address'); ?>">Manage Addresses</a>
                            </div>
                            <div class="accort-info">
                                <?php if(isset($default_billing->country) || isset($default_shipping->country)){?>
                                <div class="accort-info-bx">
                                    <div class="accort-info-bx-tilt">
                                        <h5>Default Billing Address</h5>
                                        <?php if(isset($default_billing->country)){?>
                                        <a data-id="dashboard" href="<?= base_url(); ?>edit-address/<?=$default_billing->add_id?>">Edit Address</a>
                                        <?php } ?>
                                                </div>
                                    <?php if(isset($default_billing->country)){?>
                                    <div class="accort-info-bx-txt">
                                        <p>
                                            <?=(isset($default_billing->nick_name) ? $default_billing->nick_name:"")?>
                                        </p>
                                        <p>
                                            <?=(isset($default_billing->first_name) ? $default_billing->first_name.' '.$default_billing->last_name :"")?>
                                        </p>
                                        <p>
                                            <?=(isset($default_billing->phone)? $default_billing->phone : "");?>
                                        </p>                                        
                                        <p>
                                            <?=(isset($default_billing->street)?$default_billing->street :"");?>
                                        </p>
                                        <p>
                                            <?=(isset($default_billing->city) ? $default_billing->city :"")?>
                                        </p>                                        
                                        <p>
                                            <?php if(isset($default_billing->country)){
                                                if ($default_billing->country == "US"){
                                                echo (isset($default_billing->state)?get_state_name($default_billing->state):"");
                                            } else {
                                                echo (isset($default_billing->state_other)?$default_billing->state_other:"");
                                            }}?>
                                        </p>
                                        <p>
                                             <?=(isset($default_billing->nicename) ? $default_billing->nicename :"")?>                                            
                                        </p>                                        
                                        <p>
                                             <?=(isset($default_billing->zip) ? $default_billing->zip :"")?>                                            
                                        </p>
                                    </div>
                                    <?php } else { ?>
                                    <div>No Address Found</div>
                                   <?php } ?>
                                </div>
                                <div class="accort-info-bx mrg-rgt-0">
                                    <div class="accort-info-bx-tilt">
                                        <h5>Default Shipping Address</h5>
                                        <?php if(isset($default_shipping->country)){?>
                                        <a href="<?= base_url(); ?>edit-address/<?=$default_shipping->add_id?>">Edit Address</a>
                                    <?php } ?>
                                    </div>
                                    <?php if(isset($default_shipping->country)){?>
                                    <div class="accort-info-bx-txt">
                                        <p>
                                            <?=(isset($default_shipping->nick_name) ? $default_shipping->nick_name:"")?>
                                        </p>
                                        <p>
                                            <?=(isset($default_shipping->first_name) ? $default_shipping->first_name.' '.$default_shipping->last_name :"")?>
                                        </p>
                                        <p>
                                            <?=(isset($default_shipping->phone)? $default_shipping->phone : "");?>
                                        </p>                                        
                                        <p>
                                            <?=(isset($default_shipping->street)?$default_shipping->street :"");?>
                                        </p>
                                        <p>
                                            <?=(isset($default_shipping->city) ? $default_shipping->city :"")?>
                                        </p>                                        
                                        <p>
                                            <?php if(isset($default_shipping->country)){
                                                if ($default_shipping->country == "US"){
                                                echo (isset($default_shipping->state)?get_state_name($default_shipping->state):"");
                                            } else {
                                                echo (isset($default_shipping->state_other)?$default_shipping->state_other:"");
                                            }}?>
                                        </p>
                                        <p>
                                             <?=(isset($default_shipping->nicename) ? $default_shipping->nicename :"")?>                                            
                                        </p>                                        
                                        <p>
                                             <?=(isset($default_shipping->zip) ? $default_shipping->zip :"")?>                                            
                                        </p>                                        
                                    </div>
                                    <?php } else { ?>
                                    <div>No Address Found</div>
                                   <?php } ?>
                                </div>
                                <?php } else { ?>
                                    <div>No Address Found</div>
                                   <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>                
                <!--right section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav-m-view'); ?>
            </div>
        </div>
    </div>
</section>
<!--Blog main ends  here-->



<!-- Popup Starts Here -->
<a data-toggle="modal" data-target="#myModal4" id="myAnchor" style="display: none;">Click HERE</a>
<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog popup-4" role="document">

        <div class="modal-content">
            <div class="modal-header pad-0">        
                <button type="button" style="z-index: 99;" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body pad-0">
                <div class="frgt-sec oneline">
                    <div class="fgt-tit">
                        <p class="mrg-botm-0" id="wishlist-text"></p>
                    </div>
                </div>
            </div>

        </div>

        <!-- Ended 2 divs below -->
    </div>
</div>
<!-- Popup Ends Here -->



<script>
    $(function(){
        $('.message').append('<span class="close" title="Dismiss"></span>');
        //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
        $('.message .close').hover(
                function () {
                    $(this).addClass('hover');
                },
                function () {
                    $(this).removeClass('hover');
                }
        );

        $('.message .close').click(function () {
            $(this).parent().fadeOut('slow', function () {
                $(this).remove();
            });
        });

        $(document).ready(function () {
            $('#cp1').click(function () {
                if(!($('#cp1').is(':checked'))){
                    $("#submit").removeAttr("disabled");
                    $('#password').val('');
                    $('#new_password').val('');
                    $('#confirm_password').val('');
                    $('#password_validate').html('');
                    $('#new_password_validate').html('');
                    $('#confirm_password_validate').html('');
                }else{
                    $('#password').val('');
                    $('#new_password').val('');
                    $('#confirm_password').val('');
                    $('#password_validate').html('');
                    $('#new_password_validate').html('');
                    $('#confirm_password_validate').html('');
                }
                $('#ipt-hid').slideToggle('');                
            });
        });
    });
</script>
<script type="text/javascript">

$(document).ready(function(){
    $("#password").keypress(check_pass).each(function() {
    check_pass();
  });
});
function check_pass(){
    var url = "<?=base_url()?>check-current-password";
    var value = $('#password').val();
    var user_id = $('#user_id').val();
    if (value == '') {
        $("#password").addClass('error');
        $("#password_validate").addClass('error');
        
        $("#password_validate").html('This field is required.');
    } else {
        $("#password_validate").removeClass('error');
        $("#password_validate").html('');
        $.ajax({
            url: url,
            type: "POST",
            dataType: "json",
            data: {"password": value, user_id: user_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function (data) {
                if (data.status == 'Ok') {
                    $("#submit").removeAttr("disabled");
                } else {
                    $("#submit").attr("disabled", true);
                    $("#password").removeClass('valid');
                    $("#password").addClass('error');
                    $("#password_validate").addClass('error');
                    $("#password_validate").html('Please Enter your correct password.');
                }
            }
        });
    }
}
</script>
<script>
    $(document).ready(function () {
        $.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg !== value;
        }, "This field is required.");
        $(function validate() {
            // body...
            var rules = {
                rules: {
                    password: {
                        required: true
                    },
                    new_password: {
                        required: true,
                        minlength: 6
                    },
                    confirm_password: {
                        required: true,
                        equalTo: "#new_password",
                        minlength: 6
                    },
                },
                messages: {
                    confirm_password: {
                        equalTo: "Your new password and confirmation password do not match."
                    }                        
                },
                
                errorPlacement: function (error, element) {
                    var name = $(element).attr("name");
                    //error.appendTo($("#" + name + "_validate"));
                    $("#" + name + "_validate").html(error);
                },
            };

            $('#editprofile_form').validate(rules);
        });
    });
</script>


<script type="text/javascript">
$(document).ready(function(){
    $(document).on('click', '#re_order', function(){
        var order_id = $(this).attr('data-orderid');
        var remove = 0;
        var items = 0;
        
        var total = 0.00;
        $.ajax({
            type: 'POST',
            url: '<?= base_url() . "addToCart" ?>',
            data: {order_id: order_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            async: false,
            success: function (res) {
                console.log(res);
                myObj = JSON.parse(res);
                console.log(myObj);
                items = parseInt(myObj.cart_items);
                total = myObj.cart_sub_total;
                if (myObj.status == 'not-ok') {
                    //$("#alert_msg").addClass('alert-danger');
                    //$('#alert_msg').html('Something went wrong.');
                    $("#wishlist-text").text('Item out of stock.');
                    // $('#myAnchor').trigger('click');

                } else if (myObj.status == 'ready-ok') {
                    // $("#wishlist-text").text('Item updated to cart successfully.');
                    $("#wishlist-text").text('');
                    $("#wishlist-text").html(myObj.message);
                    remove = 1;
                    //remove = 1;
                    // $('#myAnchor').trigger('click');

                   // $("#alert_msg").addClass('alert-success');
                    //$("#add_to_cart_msg").show();
                    $('.compl').attr('href', '<?= base_url('cart') ?>');
                    $('.compl').removeAttr('data-target')
                    if (myObj.cart_items > 1) {
                        $('#cart_items').html(myObj.cart_items + " items");
                    } else {
                        $('#cart_items').html(myObj.cart_items + " item");
                    }
                    $('#cart_sub_total').html(myObj.cart_sub_total);

                }  else if (myObj.status == "limit-ok") {
                    $("#wishlist-text").text('');
                    $("#wishlist-text").html(myObj.message);
                    //$('#myAnchor').trigger('click');
                    remove = 1;


                } else {
                    // $("#wishlist-text").text('Item added to cart successfully.');
                    $("#wishlist-text").text('');
                    $("#wishlist-text").html(myObj.message);
                    //$('#myAnchor').trigger('click');
                    remove = 1;

                   // $("#alert_msg").addClass('alert-success');
                    //$("#add_to_cart_msg").show();
                    $('.compl').attr('href', '<?= base_url('cart') ?>');
                    $('.compl').removeAttr('data-target')
                    if (myObj.cart_items > 1) {
                        $('#cart_items').html(myObj.cart_items + " items");
                    } else {
                        $('#cart_items').html(myObj.cart_items + " item");
                    }
                    $('#cart_sub_total').html(myObj.cart_sub_total);
                }
            },
            error: function () {
                console.log('Network error.');
            }
        });

        $('#cart_items').html(items+ " item");
        $('#cart_sub_total').html(total);
        if(items > 0){
            $('.compl').attr('href', '<?= base_url('cart') ?>');
            $('.compl').removeAttr('data-target')
            if (items > 1) {
                $('#cart_items').html(items + " items");
            } else {
                $('#cart_items').html(items + " item");
            }
            
        }
        $.ajax({
            url: '<?=base_url("whichProdNotAddCart")?>',
            type: 'POST',
            data: {order_id: order_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
        })
        .done(function(res) {
            if(remove == 0){
                $("#wishlist-text").text('');
            }

            $("#wishlist-text").append(res);
            $('#myAnchor').trigger('click');
            //console.log("success");
        })
        .fail(function() {
            //$("#wishlist-text").text('Something went wrong.');
            $('#myAnchor').trigger('click');
            console.log("error");
        });
        
    });
});
</script>