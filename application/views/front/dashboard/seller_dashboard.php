<!--Breadcrumbs starts here-->
<script src="<?=base_url()?>front_resources/js/Chart.js"></script>
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard'); ?>">My Account</a></li>
                        <li class="breadcrumb-item active">Seller Dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav'); ?>  
                <!--Left section starts here-->
                <!--right section starts here-->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">
                        <div class="sel-dashbrd-main">
                            <!--Seller dashboard starts here-->
                            <div class="sel-dashbrd-frst">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3>Seller Dashboard</h3>
                                        <p>
                                            Welcome, <?php echo $this->session->userdata('userfname') . ' ' . $this->session->userdata('userlname'); ?> | <a href="<?=base_url('store/'.$store_info->store_url)?>" target="_blank">Visit Store</a>
                                        </p>
                                    </div>
                                    <div class="panel-body">

                                        <div class="td-sale-bx">
                                            <div class="td-sale-bx-lft">
                                                <h5>Today’s Sales</h5>
                                                <h6>$<?=number_format($total_sales, 2, '.', '')?></h6>
                                            </div>
                                            <div class="td-sale-bx-rgt">
                                                <img src="<?=base_url()?>front_resources/images/trolly.png" alt="" title=""/> 
                                            </div>
                                        </div>

                                        <div class="td-sale-bx mrg-rgt-0">
                                            <div class="td-sale-bx-lft">
                                                <h5>Today’s Orders</h5>
                                                <h6><?= $todays_order->today_order ?></h6>
                                            </div>
                                            <div class="td-sale-bx-rgt">
                                                <img src="<?=base_url()?>front_resources/images/tag-btn.png" alt="" title=""/> 
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!--Seller dashboard ends here-->

                            <!--My Profit starts here-->
                            <div class="sel-dashbrd-frst uty-seller-dash-1">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3>My Profit</h3>

                                    </div>
                                    <div class="panel-body">

                                        <div class="prof-bx">
                                            <h5>$<?php if(!empty($store_profit['last_week']->last_week)){echo $store_profit['last_week']->last_week;}else{echo '0';}?></h5>
                                            <p>
                                                Last Week
                                            </p>
                                        </div>

                                        <div class="prof-bx">
                                            <h5>$<?php if(!empty($store_profit['last_month']->last_month)){echo $store_profit['last_month']->last_month;}else{echo '0';}?></h5>
                                            <p>
                                                Last Month
                                            </p>
                                        </div>


                                        <div class="prof-bx brd-right-0">
                                            <h5>$<?php if(!empty($store_profit['last_year']->last_year)){echo $store_profit['last_year']->last_year;}else{echo '0';}?></h5>
                                            <p>
                                                Last Year
                                            </p>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <!--My Profit ends here-->

                            <!--Product Views starts here-->
                            <div class="sel-dashbrd-frst">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3>Product Views</h3>

                                    </div>
                                    <div class="panel-body">

                                        <div class="pdvw-bx">
                                            <h5><?= ($counts['today']->today) ? $counts['today']->today : "0" ?></h5>
                                            <p>
                                                Today
                                            </p>
                                        </div>

                                        <div class="pdvw-bx">
                                            <h5><?= ($counts['last_week_views']->last_week) ? $counts['last_week_views']->last_week : "0" ?></h5>
                                            <p>
                                                Last Week
                                            </p>
                                        </div>


                                        <div class="pdvw-bx">
                                            <h5><?= ($counts['last_month_views']->last_month) ? $counts['last_month_views']->last_month : "0" ?></h5>
                                            <p>
                                                Last Month
                                            </p>
                                        </div>

                                        <div class="pdvw-bx brd-right-0">
                                            <h5><?= ($counts['last_year_views']->last_year) ? $counts['last_year_views']->last_year : "0" ?></h5>
                                            <p>
                                                Last Year
                                            </p>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <!--Product Views ends here-->

                            <!--Orders starts here-->
                            <div class="sel-dashbrd-frst">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3>Orders</h3>
                                    </div>
                                    <div class="panel-body pad-chang">
                                        <div class="ordrs-left">
                                            <div class="ordrs-left-row">
                                                <div class="ordrs-left-row-lf">
                                                    <p class="clor-red">Total</p>
                                                </div>
                                                <div class="ordrs-left-row-rg">
                                                    <p class="clor-red"><?=$orders_stats->total_orders?></p>
                                                </div>

                                            </div>

                                            <div class="ordrs-left-row">
                                                <div class="ordrs-left-row-lf">
                                                    <p class="clor-gren">Completed</p>
                                                </div>
                                                <div class="ordrs-left-row-rg">
                                                    <p class="clor-gren"><?=$orders_stats->completed?></p>
                                                </div>

                                            </div>

                                            <div class="ordrs-left-row">
                                                <div class="ordrs-left-row-lf">
                                                    <p class="clor-blck">Pending</p>
                                                </div>
                                                <div class="ordrs-left-row-rg">
                                                    <p class="clor-blck"><?=$orders_stats->pending?></p>
                                                </div>

                                            </div>

                                            <div class="ordrs-left-row">
                                                <div class="ordrs-left-row-lf">
                                                    <p class="clor-ble">Processing</p>
                                                </div>
                                                <div class="ordrs-left-row-rg">
                                                    <p class="clor-ble"><?=$orders_stats->processing?></p>
                                                </div>

                                            </div>

                                            <div class="ordrs-left-row">
                                                <div class="ordrs-left-row-lf">
                                                    <p class="clor-red">Cancelled</p>
                                                </div>
                                                <div class="ordrs-left-row-rg">
                                                    <p class="clor-red"><?=$orders_stats->cancel?></p>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="ordrs-right">
                                            <!--<img src="<?=base_url()?>front_resources/images/process-bar.jpg" alt="" title="" class="pull-right" />-->
                                            <canvas id="doughnutChart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Orders ends here-->

                            <!--Orders starts here-->
                            <div class="sel-dashbrd-frst">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3>Sales Report</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="salr-rept-bx">
                                            <canvas id="lineChart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Orders ends here-->


                            <!--Last five order starts here-->
                            <div class="my-account-right-2">
                                <div class="well">
                                    <h4>Last Five Orders</h4>	

                                </div>
                                <div class="my-account-right-table">
                                    <?php if (isset($last_orders)) { ?>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="pad-lft-0" style="width:11%;">Order #</th>
                                                    <th class="pad-lft-0" style="width:17%;">Item</th>
                                                    <th style="width:28%;">Product Description</th>
                                                    <th style="width:6%;"></th>
                                                    <th class="text-center" style="width:10%;">Price</th>
                                                    <th style="width:12%;">Shipping</th>
                                                    <th style="width:7%;">Qty</th>
                                                    <th class="text-center pad-rgt-0" style="width:9%;">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php // echo "<pre>";print_r($last_orders);die; ?>
                                                <?php foreach ($last_orders as $data) { ?>
                                                <?php
                                                $url = base_url();
                                                //echo productDetailUrl($data['order_prod_id']);
                                                if(productDetailUrl($data['order_prod_id']) != '0'){
                                                    $url = productDetailUrl($data['order_prod_id']).'/'.$data['order_prod_url'];
                                                }
                                                ?>
                                                    <tr>
                                                        <td class="pad-lft-0"><?= $data['order_id'] ?></td>
                                                        <td class="pad-lft-0"><a href="<?=$url?>"><img src="<?= base_url() ?>resources/prod_images/thumb/<?= $data['order_prod_image'] ?>" alt="" title="" class="img-nset"></a></td>
                                                        <td><a href="<?=$url?>"> <?= $data['order_prod_description'] ?> </a> </td>
                                                        <td></td>
                                                        <td class="text-center">$<?= number_format($data['order_prod_total_price'], 2, '.', '') ?></td>
                                                        <td class="text-center">$<?= number_format($data['order_prod_ship_price'], 2, '.', '') ?></td>
                                                        <td class="text-center"><?= $data['order_prod_qty'] ?></td>
                                                        <?php
                                                        $profit = calculateSellerProfit($data['order_prod_store_id'],$data['order_id']);?>
                                                        <td class="pad-rgt-0 text-center">$<?= number_format($profit->totalProfit, 2, '.', '') ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    <?php } else { ?>
                                        <div id="message" class="alert alert-danger message ">No Orders Found</div>
                                    <?php } ?>
                                </div>
                            </div>
                            <!--Last five order end here-->

                            <!--Last five order starts here-->
                            <div class="my-account-right-2">
                                <div class="well">
                                    <h4>Most Viewed Products</h4>	

                                </div>
                               	<div class="my-account-right-table">
                                    <?php if (isset($most_view_product) && count($most_view_product)) { ?>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="pad-lft-0" style="width:13%;">Item</th>
                                                <th style="width:31%;">Product Title</th>
                                                <th class="pad-lft-0" style="width:2%;"></th>
                                                <!-- <th style="width:10%;"></th> -->
                                                <th style="width:10%;"></th>
                                                <th style="width:18%;"></th>
                                                <th class="text-center" style="width:10%;">Price</th>
                                                <th class="pad-rgt-0 text-center" style="width:16%;">Total Views</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            <?php foreach ($most_view_product as $data) { ?>

                                            <?php

                                                $url = base_url();
                                                //echo productDetailUrl($data['order_prod_id']);
                                                if(productDetailUrl($data['product_id']) != '0'){
                                                    $url = productDetailUrl($data['product_id']).'/'.$data['prod_url'];
                                                }
                                                ?>
                                            <tr>
                                                
                                                <td class="pad-lft-0">
                                                	<a href="<?=$url?>">
                                                		<img src="<?= base_url() ?>resources/prod_images/thumb/<?= getrodImage($data['product_id'])?>" alt="" title="" class="" />
                                                    </a>
                                                </td>
                                                <td>
                                                	<a href="<?=$url?>">
														<?=$data['prod_title']?>
                                                    </a>
                                                </td>
                                                <td></td>
                                                <!-- <td></td> -->
                                                <td class="text-center"></td>
                                                <td class="text-center"></td>
                                                <td class="text-center">
                                                    <?php $prodPrice = 0.0;
                                                        $sDate = date('Y-m-d', strtotime($data['sale_start_date']));
                                                        $eDate = date('Y-m-d', strtotime($data['sale_end_date']));
                                                        $tDate = date('Y-m-d');
                                                        if ($data['prod_onsale']== 1) {
                                                            if ($data['sale_start_date'] != '' && $data['sale_end_date'] != '') {
                                                                if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                                                                    $prodPrice = number_format($data['sale_price'], 2, '.', '');                        
                                                                } else {
                                                                    $prodPrice = number_format($data['prod_price'], 2, '.', '');                        
                                                                }
                                                            } else if ($data['sale_start_date'] != '') {
                                                                if ($tDate >= $sDate) {                        
                                                                    $prodPrice = number_format($data['sale_price'], 2, '.', '');
                                                                } else {
                                                                    $prodPrice = number_format($data['prod_price'], 2, '.', '');
                                                                }
                                                            } else if($data['sale_end_date'] != '') {
                                                                if($tDate <= $eDate) {
                                                                    $prodPrice = number_format($data['sale_price'], 2, '.', '');
                                                                } else {
                                                                    $prodPrice = number_format($data['prod_price'], 2, '.', '');
                                                                }
                                                            }else {
                                                                $prodPrice = number_format($data['sale_price'], 2, '.', '');
                                                            }
                                                        } else {
                                                            $prodPrice = number_format($data['prod_price'], 2, '.', '');
                                                        } echo $prodPrice;?>
                                                    </td>
                                                <td class="text-center"><?=$data['prod_occurrence'] ?></td>
                                                
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <?php } else { ?>
                                        <div id="message" class="alert alert-danger message ">No Products Views</div>
                                    <?php } ?>
                                </div>
                            </div>
                            <!--Last five order end here-->
                        </div>
                    </div>	
                </div>  
                <!--right section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav-m-view'); ?>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
        Chart.pluginService.register({
        beforeDraw: function (chart) {
            if (chart.config.options.elements.center) {
        //Get ctx from string
        var ctx = chart.chart.ctx;
        
                //Get options from the center object in options
        var centerConfig = chart.config.options.elements.center;
        var fontStyle = centerConfig.fontStyle || 'Arial';
                var txt = centerConfig.text;
        var color = centerConfig.color || '#000';
        var sidePadding = centerConfig.sidePadding || 20;
        var sidePaddingCalculated = (sidePadding/100) * (chart.innerRadius * 2)
        //Start with a base font of 30px
        ctx.font = "30px " + fontStyle;
        
                //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
        var stringWidth = ctx.measureText(txt).width;
        var elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

        // Find out how much the font can grow in width.
        var widthRatio = elementWidth / stringWidth;
        var newFontSize = Math.floor(30 * widthRatio);
        var elementHeight = (chart.innerRadius * 2);

        // Pick a new font size so it will not be larger than the height of label.
        var fontSizeToUse = Math.min(newFontSize, elementHeight);

                //Set font settings to draw it correctly.
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        var centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
        var centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
        ctx.font = fontSizeToUse+"px " + fontStyle;
        ctx.fillStyle = color;
        
        //Draw text in center
        ctx.fillText(txt, centerX, centerY);
            }
        }
    });


        var config = {
            type: 'doughnut',
            data: {
                labels: [
                  "Completed",
                  "Pending",
                  "Processing",
                  "Cancelled"
                ],
                datasets: [{
                    data: [<?=$orders_stats->completed?>, <?=$orders_stats->pending?>, <?=$orders_stats->processing?>,<?=$orders_stats->cancel?>],
                    backgroundColor: [
                      "#9ac469",
                      "#000000",
                      "#008ac5",
                      "#ff0000"
                    ],
                    hoverBackgroundColor: [
                      "#9ac469",
                      "#000000",
                      "#008ac5",
                      "#ff0000"
                    ]
                }]
            },
        options: {
            elements: {
                center: {
                    text: <?=$orders_stats->total_orders?>,
          color: '#ff0000', // Default is #000000
          fontStyle: 'Arial', // Default is Arial
          sidePadding: 70 // Defualt is 20 (as a percentage)
                }
            }
        }
    };


        var ctx = document.getElementById("doughnutChart").getContext("2d");
        var myChart = new Chart(ctx, config);
       
</script>
<script>
    //line
var ctxL = document.getElementById("lineChart").getContext('2d');
var myLineChart = new Chart(ctxL, {
    type: 'line',
    data: {
        labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        datasets: [
            {
                label: "Sales Per Month",
                fillColor: "rgba(220,220,220,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [<?=$months[0]?>, <?=$months[1]?>, <?=$months[2]?>, <?=$months[3]?>, <?=$months[4]?>, <?=$months[5]?>, <?=$months[6]?>, <?=$months[7]?>, <?=$months[8]?>, <?=$months[9]?>, <?=$months[10]?>, <?=$months[11]?>]
            }
        ]
    },
    options: {
        responsive: true,
         scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true,
                    callback: function(value, index, values) {
                        return '$ ' + value.toFixed(2);
                    }
                }
            }]
        },
        tooltips: {
            callbacks: {
                label: function(tooltipItem, chart){
                    var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                    return datasetLabel + ': $ ' + tooltipItem.yLabel;
                }
            }
        }
    }    
});
</script>
<!--Blog main ends  here-->
