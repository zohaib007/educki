<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:;">My Account</a></li>
                        <li class="breadcrumb-item active">My Dashboard </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav'); ?>  
                <!--Left section starts here-->
                <!--right section starts here-->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">
                        <div class="ualert-row">
                                <?php
                                if ($this->session->flashdata('msg') != "") {
                                    echo '<div id="message" class="alert alert-success message "><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>' . $this->session->flashdata('msg') . '</div>';
                                }
                                ?>
                        </div>
                        <div class="my-account-right-head">
                            <h5>My Dashboard</h5>
                            <p>Hello <?=@$user->user_fname." ".@$user->user_lname?>! </p>
                            <p>
                            	Welcome to the eDucki Community.  Below you find a snapshot of your Account Dashboard, and everything you need to get started.  See recent account activity, billing, and contact information all at your fingertips.
                            </p>
                        </div>
                        <div class="my-account-right-2">
                            <div class="well">
                                <h4>Recent Orders</h4>
                                <?php if($orders){ ?>	
                                <a href="<?=base_url()?>my-orders">View All</a>
                                <?php } ?>
                            </div>
                            
                            <div class="my-account-right-table">
                                <?php if($orders){ ?>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="pad-lft-0" style="width:14%;">Item</th>
                                            <th style="width:30%;">Product Description</th>
                                            <th class="text-center" style="width:14%;">Date</th>
                                            <th style="width:15%;">Order Total</th>
                                            <th style="width:5%;">Status</th>
                                            <th class="text-center" style="width:18%;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($orders as $key => $order) { ?>
                                        <tr>
                                            <?php $o_prod = getOrderProductByOrderId($order->order_id);?>
                                            <td class="pad-lft-0"><img src="<?= base_url() ?>resources/prod_images/thumb/<?= $o_prod->order_prod_image ?>"  alt="" title="" class=""/></td>
                                            <td><?=$o_prod->order_prod_name?></td>
                                            <td class="text-center"><?=date('F d, Y', strtotime($order->order_date))?></td>
                                            <td class="text-center">$<?=number_format($order->order_grand_total, 2, '.', '')?></td>
                                            <td><?=$order->order_status?></td>
                                            <td class="pad-rgt-0">
                                                <a href="<?=base_url()?>my-order/<?=$order->order_id?>" class="view-ord">View Order </a>
                                                <span class="lne">|</span>
                                                <a id="re_order" href="javascript:void(0);" onclick="reorder(<?=$order_details->order_id?>)" class="reord-ord">Reorder </a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <?php }else{ ?>
                                <div id="message" class="alert alert-danger message "><a href="javascript:;" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>No Record Found</div>
                                <?php } ?>
                            </div>
                            
                            <div>
                                <div class="well">
                                    <h4>Account Information</h4>	
                                </div>
                                <div class="accort-info">
                                    <div class="accort-info-bx">
                                        <div class="accort-info-bx-tilt">
                                            <h5>Contact Information</h5>
                                            <a href="<?=base_url('my-profile')?>">Edit</a>
                                        </div>
                                        <div class="accort-info-bx-txt">
                                            <p><?=@$user->user_fname." ".@$user->user_lname?></p>
                                            <p><?=@$user->user_email?></p>
                                            <!-- <a href="<?= base_url('change-password'); ?>" class="chang-pawrd">Change Password</a> -->
                                            <?php if(!isset($user->user_facebook_id)){?>
                                            <div class="chng-paswrd">
                                                <input type="checkbox" id="cp1" name="cc">
                                                <label for="cp1"><span></span><p>Change Password</p></label>
                                            </div>
                                            <?php }?>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php if(!isset($user->user_facebook_id)){?>
                            <div class="change-pas-dv" id="ipt-hid">
                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>Current Password <span>*</span></h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="text" name="password" id="password" onblur="check()" placeholder="" />
                                        <div class="error" id="password_validate"></div>
                                    </div>
                                </div>

                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>New Password <span>*</span></h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="text" name="new_password" id="new_password" placeholder="" />
                                        <div class="error" id="new_password_validate"></div>
                                    </div>
                                </div>

                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>Confirm New Password <span>*</span></h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="text" name="confirm_password" id="confirm_password" placeholder=""/>
                                        <div class="error" id="confirm_password_validate"></div>
                                    </div>
                                </div>
                            </div>                            
                            <?php }?>
                            <div>
                                <div class="well">
                                    <h4>Address Book</h4>	
                                    <a href="<?= base_url('my-address'); ?>">Manage Addresses</a>
                                </div>
                                <div class="accort-info">
                                    <?php if(isset($default_billing->country) || isset($default_shipping->country)){?>
                                    <div class="accort-info-bx">
                                        <div class="accort-info-bx-tilt">
                                            <h5>Default Billing Address</h5>
                                            <?php if(isset($default_billing->country)){?>
                                            <a data-id="dashboard" href="<?= base_url(); ?>edit-address/<?=$default_billing->add_id?>">Edit Address</a>
                                            <?php } ?>
                                                    </div>
                                        <?php if(isset($default_billing->country)){?>
                                        <div class="accort-info-bx-txt">
                                            <p>
                                                <?=(isset($default_billing->first_name) ? $default_billing->first_name.' '.$default_billing->last_name :"")?>
                                            </p>
                                            <p>
                                                <?=(isset($default_billing->street)?$default_billing->street :"");?>
                                            </p>
                                            <p>
                                                <?php if(isset($default_billing->country)){
                                                    if ($default_billing->country == "US"){
                                                    echo (isset($default_billing->state)?$default_billing->state.' , '.$default_billing->zip:"");
                                                } else {
                                                    echo (isset($default_billing->state_other)?$default_billing->state_other.' , '.$default_billing->zip:"");
                                                }}?>
                                            </p>
                                            <p>
                                                <?=(isset($default_billing->city) ? $default_billing->city.','.$default_billing->nicename :"")?>
                                            </p>
                                            <p>
                                                T: <?=(isset($default_billing->phone)? $default_billing->phone : "");?>
                                            </p>
                                        </div>
                                        <?php } else { ?>
                                        <div>No Address Found</div>
                                       <?php } ?>
                                    </div>
                                    <div class="accort-info-bx mrg-rgt-0">
                                        <div class="accort-info-bx-tilt">
                                            <h5>Default Shipping Address</h5>
                                            <?php if(isset($default_shipping->country)){?>
                                            <a href="<?= base_url(); ?>edit-address/<?=$default_shipping->add_id?>">Edit Address</a>
                                        <?php } ?>
                                        </div>
                                        <?php if(isset($default_shipping->country)){?>
                                        <div class="accort-info-bx-txt">
                                            <p>
                                                <?=(isset($default_shipping->first_name) ? $default_shipping->first_name.' '.$default_shipping->last_name :"")?>
                                            </p>
                                            <p>
                                                <?=(isset($default_shipping->street)?$default_shipping->street :"");?>
                                            </p>
                                            <p>
                                                <?php if(isset($default_shipping->country)){
                                                    if ($default_shipping->country == "US"){
                                                    echo (isset($default_shipping->state)?$default_shipping->state.' , '.$default_shipping->zip:"");
                                                } else {
                                                    echo (isset($default_shipping->state_other)?$default_shipping->state_other.' , '.$default_shipping->zip:"");
                                                }}?>
                                            </p>
                                            <p>
                                                <?=(isset($default_shipping->city) ? $default_shipping->city.','.$default_shipping->nicename :"")?>
                                            </p>
                                            <p>
                                                T: <?=(isset($default_shipping->phone)? $default_shipping->phone : "");?>
                                            </p>
                                        </div>
                                        <?php } else { ?>
                                        <div>No Address Found</div>
                                       <?php } ?>
                                    </div>
                                    <?php } else { ?>
                                        <div>No Address Found</div>
                                       <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
                <!--right section starts here-->
            </div>
        </div>
    </div>
</section>
<!--Blog main ends  here-->
<script>
    $(document).ready(function () {
        $('#cp1').click(function () {
            $('#ipt-hid').slideToggle('');
        });
    });
    $(function () {
        $('.message').append('<span class="close" title="Dismiss"></span>');
        //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
        $('.message .close').hover(
                function () {
                    $(this).addClass('hover');
                },
                function () {
                    $(this).removeClass('hover');
                }
        );

        $('.message .close').click(function () {
            $(this).parent().fadeOut('slow', function () {
                $(this).remove();
            });
        });
    });
</script>
<script type="text/javascript">

    function reorder($order_id){
        $.ajax({
            type:'POST',
            url:'<?=base_url()."addToCart"?>',
            data:{order_id:$order_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function(res){
                myObj = JSON.parse(res);
                if(myObj.status == 'ok'){
                    window.location = "<?=base_url()."cart"?>";
                }
            },
            error: function(){
                console.log('Network error.');
            }
        });
    }

    </script>