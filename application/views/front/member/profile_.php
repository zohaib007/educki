<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard'); ?>">My Account </a></li>
                        <li class="breadcrumb-item active">My Profile</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->
<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav'); ?>
                <!--Left section starts here-->
                <!--right section starts here-->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">
                        <div class="my-account-right-head pad-btm-0">
                            <h5>My Profile</h5>
                        </div>
                        <?php echo form_open_multipart('', array('name' => 'editprofile', 'id' => 'editprofile_form')); ?>
                        <div class="store-profile">
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>First Name <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" id="first_name" name="first_name" value="<?= (isset($user->user_fname) ? $user->user_fname : "" ) ?>" placeholder="John" />
                                    <div class="error" id="first_name_validate"></div>
                                </div>
                            </div>
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Last Name <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" id="last_name" name="last_name" value="<?= (isset($user->user_lname) ? $user->user_lname : "" ) ?>" placeholder="Smith" />
                                    <div class="error" id="last_name_validate"></div>
                                </div>
                            </div>
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Zip Code <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" id="zip_code" name="zip_code" value="<?= (isset($user->user_zip) ? $user->user_zip : "" ) ?>" placeholder="Zip Code" />
                                    <div class="error" id="zip_code_validate"></div>
                                </div>
                            </div>

                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Age <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <select name="user_age">
                                        <option value="" <?= (isset($user->user_age) ? ($user->user_age == "" ? "selected" : "") : "") ?> >Select Age</option>
                                        <option value="13-17" <?= (isset($user->user_age) ? ($user->user_age == "13-17" ? "selected" : "") : "") ?> >13-17</option>
                                        <option value="13-17" <?= (isset($user->user_age) ? ($user->user_age == "18-24" ? "selected" : "") : "") ?> >18-24</option>
                                        <option value="25-34" <?= (isset($user->user_age) ? ($user->user_age == "25-34" ? "selected" : "") : "") ?> >25-34</option>
                                        <option value="35-44" <?= (isset($user->user_age) ? ($user->user_age == "35-44" ? "selected" : "") : "") ?> >35-44</option>
                                        <option value="45-54" <?= (isset($user->user_age) ? ($user->user_age == "45-54" ? "selected" : "") : "") ?> >45-54</option>
                                        <option value="55-64" <?= (isset($user->user_age) ? ($user->user_age == "55-64" ? "selected" : "") : "") ?> >55-64</option>
                                        <option value="65+" <?= (isset($user->user_age) ? ($user->user_age == "65+" ? "selected" : "") : "") ?> >65+</option>
                                    </select>
                                    <div class="error" id="user_age_validate"></div>   
                                </div>
                            </div>
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Gender <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <div class="gendr-sec">
                                        <div class="memb-gnd">
                                            <input type="radio" id="g1" name="user_sex" value="Male" <?= (isset($user->user_sex) ? ($user->user_sex == "Male" ? "checked" : "") : "") ?> >
                                            <label for="g1"><span></span><p>Male</p></label>
                                        </div>
                                        <div class="memb-gnd">
                                            <input type="radio" id="g2" name="user_sex" value="Female" <?= (isset($user->user_sex) ? ($user->user_sex == "Female" ? "checked" : "") : "") ?> >
                                            <label for="g2"><span></span><p>Female</p></label>
                                        </div>
                                        <div class="memb-gnd">
                                            <input type="radio" id="g3" name="user_sex" value="other" <?= (isset($user->user_sex) ? ($user->user_sex == "other" ? "checked" : "") : "") ?> >
                                            <label for="g3"><span></span><p>other</p></label>
                                        </div>
                                    </div>
                                    <div class="error" id="user_sex_validate"></div>   
                                </div>
                            </div>
                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Select Interests <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <select name="user_interests" id="user_interests" >
                                        <option value="">Select Interests</option>
                                        <option value="Fertility" <?= (isset($user->user_interests) ? ($user->user_interests == "Fertility" ? "selected" : "") : "") ?> >Fertility</option>
                                        <option value="Pregnancy" <?= (isset($user->user_interests) ? ($user->user_interests == "Pregnancy" ? "selected" : "") : "") ?> >Pregnancy</option>
                                        <option value="Baby names" <?= (isset($user->user_interests) ? ($user->user_interests == "Baby names" ? "selected" : "") : "") ?> >Baby Names</option>
                                        <option value="Baby" <?= (isset($user->user_interests) ? ($user->user_interests == "Baby" ? "selected" : "") : "") ?> >Baby</option>
                                        <option value="Toddler" <?= (isset($user->user_interests) ? ($user->user_interests == "Toddler" ? "selected" : "") : "") ?> >Toddler</option>
                                        <option value="Child" <?= (isset($user->user_interests) ? ($user->user_interests == "Child" ? "selected" : "") : "") ?> >Child</option>
                                        <option value="Family" <?= (isset($user->user_interests) ? ($user->user_interests == "Family" ? "selected" : "") : "") ?> >Family</option>
                                        <option value="Advice" <?= (isset($user->user_interests) ? ($user->user_interests == "Advice" ? "selected" : "") : "") ?> >Advice</option>
                                        <option value="Food" <?= (isset($user->user_interests) ? ($user->user_interests == "Food" ? "selected" : "") : "") ?> >Food</option>
                                        <option value="Other" <?= (isset($user->user_interests) ? ($user->user_interests == "Other" ? "selected" : "") : "") ?> >Other</option>
                                    </select>
                                    <div class="error" id="user_interests_validate"></div>
                                </div>
                            </div>
                            <div class="store-profile-row" id="otherinterest" <?= ($user->user_interests == "Other") ? 'style="display: block"' : 'style="display: none"' ?>>
                                <div class="store-profile-row-rgt">
                                    <input type="text" id="user_other_interest" name="user_other_interest" value="<?= (isset($user->user_other_interest) ? $user->user_other_interest : "" ) ?>" placeholder="" />
                                    <div class="error" id="user_other_interest_validate"></div>
                                </div>
                            </div>

                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Select Interests <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <select name="user_interests_second" id="user_interests_second" >
                                        <option value="">Select Interests</option>
                                        <option value="Fertility" <?= (isset($user->user_interests_second) ? ($user->user_interests_second == "Fertility" ? "selected" : "") : "") ?> >Fertility</option>
                                        <option value="Pregnancy" <?= (isset($user->user_interests_second) ? ($user->user_interests_second == "Pregnancy" ? "selected" : "") : "") ?> >Pregnancy</option>
                                        <option value="Baby names" <?= (isset($user->user_interests_second) ? ($user->user_interests_second == "Baby names" ? "selected" : "") : "") ?> >Baby Names</option>
                                        <option value="Baby" <?= (isset($user->user_interests_second) ? ($user->user_interests_second == "Baby" ? "selected" : "") : "") ?> >Baby</option>
                                        <option value="Toddler" <?= (isset($user->user_interests_second) ? ($user->user_interests_second == "Toddler" ? "selected" : "") : "") ?> >Toddler</option>
                                        <option value="Child" <?= (isset($user->user_interests_second) ? ($user->user_interests_second == "Child" ? "selected" : "") : "") ?> >Child</option>
                                        <option value="Family" <?= (isset($user->user_interests_second) ? ($user->user_interests_second == "Family" ? "selected" : "") : "") ?> >Family</option>
                                        <option value="Advice" <?= (isset($user->user_interests_second) ? ($user->user_interests_second == "Advice" ? "selected" : "") : "") ?> >Advice</option>
                                        <option value="Food" <?= (isset($user->user_interests_second) ? ($user->user_interests_second == "Food" ? "selected" : "") : "") ?> >Food</option>
                                        <option value="Other" <?= (isset($user->user_interests_second) ? ($user->user_interests_second == "Other" ? "selected" : "") : "") ?> >Other</option>
                                    </select>
                                    <div class="error" id="user_interests_second_validate"></div>
                                </div>
                            </div>
                            <div class="store-profile-row" id="otherinterestsecond" <?= ($user->user_interests_second == "Other") ? 'style="display: block"' : 'style="display: none"' ?>>
                                <div class="store-profile-row-rgt">
                                    <input type="text" id="user_other_interest_second" name="user_other_interest_second" value="<?= (isset($user->user_other_interest_second) ? $user->user_other_interest_second : "" ) ?>" placeholder="" />
                                    <div class="error" id="user_other_interest_second_validate"></div>
                                </div>
                            </div>

                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Are you a small business owner <span>*</span></h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <select name="user_business_owner">
                                        <option value="">Select</option>
                                        <option value="1" <?= (isset($user->user_business_owner) ? ($user->user_business_owner == "1" ? "selected" : "") : "") ?> >Yes</option>
                                        <option value="0" <?= (isset($user->user_business_owner) ? ($user->user_business_owner == "0" ? "selected" : "") : "") ?> >No</option>
                                    </select>
                                    <div class="error" id="user_business_owner_validate"></div>
                                </div>
                            </div>

                            <div class="store-profile-row">
                                <div class="store-profile-row-lft">
                                    <h4>Email</h4>
                                </div>
                                <div class="store-profile-row-rgt">
                                    <input type="text" id="email" name="email" disabled="disabled" value="<?= (isset($user->user_email) ? $user->user_email : "" ) ?>" placeholder="info@sampletexthere.com" />
                                    <input type="hidden" id="previous_email" name="previous_email" value="<?= (isset($user->user_email) ? $user->user_email : "" ) ?>" />
                                    <div class="chng-paswrd">
                                        <input type="checkbox" id="cp1" name="cc">
                                        <label for="cp1"><span></span><p>Change Password</p></label>
                                    </div>
                                </div>
                            </div>

                            <div class="change-pas-dv" id="ipt-hid">

                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>Current Password <span>*</span></h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="text" name="password" id="password" onblur="check()" placeholder="" />
                                        <div class="error" id="password_validate"></div>
                                    </div>
                                </div>

                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>New Password <span>*</span></h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="text" name="new_password" id="new_password" placeholder="" />
                                    </div>
                                </div>

                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>Confirm New Password <span>*</span></h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="text" name="confirm_password" id="confirm_password" placeholder=""/>
                                        <div class="error" id="confirm_password_validate"></div>
                                    </div>
                                </div>
                            </div>                            

                            <div class="store-profile-uplad border-bottom-0">
                                <div class="store-profile-row">
                                    <div class="store-logo">
                                        <h6>Upload a picture of your profile</h6>
                                        <input type='file' id="imgInpM" name="user_image" accept="image/*" class="imgList" />
                                        <h6>Upload image size 150 x 110</h6>
                                        <div id="imgList">
                                            <?php if(@$user->user_profile_picture != '') { ?>
                                            <a class="cross-btn" href="javascript:void(0);" style="display: block;" onclick="delete_gall_image(<?php echo @$user->user_id; ?>, 'imgList', 'user_profile_picture', 'user_image');"></a>
                                            <img src="<?= base_url() ?>/resources/user_profile_image/thumb/<?= $user->user_profile_picture?>" class="img-responsive" alt="" />
                                            <?php  } ?>
                                            <input type="hidden" name="user_image" id="logo_image" value="<?= @$user->user_profile_picture;?>"  />
                                        </div>
                                        <div class="newlogoimage">
                                            <button class="cross-btn logo"></button>
                                        </div>
                                        <div class="error" id="logo_img_validate"> <?php echo form_error('user_image') ?> </div>
                                    </div>
                                </div>
                                <div class="store-profile-row">
                                    <input class="upld-btn" type="submit"/>
                                </div>
                            </div>

                            <input type="hidden" name="user_id" id="user_id" value="<?= $user->user_id ?>"

                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>                <!--right section starts here-->
            </div>
        </div>
    </div>
</section>
<!--Blog main ends  here-->
<script>
    var base_url = '<?php echo base_url(); ?>';
    
    function readURLm(input) {
        if (input.files.length > 0) {
           var reader = new FileReader();
          $.each(input.files, function(key, val) {
            {
           
            reader.onload = function (e) {
                $('<img>').attr('src', e.target.result).appendTo('#imgList');
                $('.cross-btn').css('display','block')
            }
            reader.readAsDataURL(val);
          }
         });
       }
    }

    function readURL(input) {
        if (input.files.length > 0) {
           var reader = new FileReader();
          $.each(input.files, function(key, val) {
            {
           
            reader.onload = function (e) {
                $('<img>').attr('src', e.target.result).appendTo('#imgTwo');
                $('.cross-btn2').css('display','block')
            }
            reader.readAsDataURL(val);
          }
         });
       }
    }


    $("#imgInpM").change(function(){
        readURLm(this);
    });

    $("#imgInpN").change(function(){
        readURL(this);
    });


    function delete_gall_image(sel, hide, fieldname, name) {
        $('.' + hide).removeClass("hide");
        $('#' + hide).hide();
        $('#' + name).val('');
        $.ajax({
            url: base_url + 'users/delete_page_img',
            type: 'POST',
            data: {nId: sel, fieldname: fieldname,<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'},
        }).done(function () {
            console.log("success");
        }).fail(function () {
            console.log("error");
        }).always(function () {
            console.log("complete");
        });
    }

    function check() {


        var url = "<?= base_url() ?>check-current-password";
        var value = $('#password').val();
        var user_id = $('#user_id').val();
        if (value == '') {
            $("#password").addClass('error');
            $("#password_validate").html('This field is required');
        } else {
            $("#password_validate").removeClass('error');
            $("#password_validate").html('');
            $.ajax({
                url: url,
                type: "POST",
                dataType: "json",
                data: {"password": value, user_id: user_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                success: function (data) {
                    if (data.status == 'Ok') {
                        
                    } else {
                        $("#password").removeClass('valid');
                        $("#password").addClass('error');
                        $("#password_validate").addClass('error');
                        $("#password_validate").html('Please Enter your correct current password');
                    }
                }
            });
        }
    }
</script>

<script>
    $(function () {
        $('#user_interests').change(function () {
            if ($('#user_interests').val() == 'Other') {
                $('#otherinterest').show("slow");
            } else {
                $('#otherinterest').hide("slow");
            }

        });
    });
    $(function () {
        $('#user_interests_second').change(function () {
            if ($('#user_interests_second').val() == 'Other') {
                $('#otherinterestsecond').show("slow");
            } else {
                $('#otherinterestsecond').hide("slow");
            }

        });
    });
    $(document).ready(function () {
        $('#cp1').click(function () {
            $('#ipt-hid').slideToggle('');
        });
    });
</script>
<script>
    $(document).ready(function () {
        $.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg !== value;
        }, "This field is required.");
        $(function validate() {
            // body...
            var rules = {
                rules: {
                    first_name: {
                        required: true
                    },
                    last_name: {
                        required: true
                    },
                    zip_code: {
                        maxlength: 5,
                        required: true,
                        number: true
                    },
                    user_other_interest: {
                        required: true
                    },
                    user_other_interest_second: {
                        required: true
                    },
                    user_age: {valueNotEquals: ""},
                    user_interests: {valueNotEquals: ""},
                    user_interests_second: {valueNotEquals: ""},
                    user_business_owner: {valueNotEquals: ""},
                    password: {
                        required: true
                    },
                    new_password: {
                        required: true
                    },
                    confirm_password: {
                        equalTo: "#new_password"
                    }
                },
                errorPlacement: function (error, element) {
                    var name = $(element).attr("name");
                    error.appendTo($("#" + name + "_validate"));
                },
            };

            $('#editprofile_form').validate(rules);
        });
    });
</script>