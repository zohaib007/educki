<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard'); ?>">My Account </a></li>
                        <li class="breadcrumb-item active">My Wishlist</li>
                    </ol>
                </div>	
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->

<!--Shop Filters starts here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav'); ?>  
                <!--Left section starts here-->

                <div class="col-sm-9 col-xs-9">

                    <div class="shop-right pad-btm-22">
                        <div class="mang-prod-mian-hed">
                            <h5>My Wishlist</h5>
                        </div>
                        <div>
                            
                        <?php echo form_open('',array('name'=>'wishForm', 'method'=>'get', 'id'=>'wishForm')); ?>
                        <div class="shop-right-upr pad-tp-19">
                                <div class="comp-alert alert alert-danger alert-dismissable fade in hide" >
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span id="comp_msg">This alert box could indicate a dangerous or potentially negative action.</span>
                        </div>
                            <div class="show-itm">
                                    <h5><?php
                                        if ($total_rows > 0) {
                                            echo $per_page;
                                        } else {
                                            echo $total_rows;
                                        }
                                        ?> Item(s)</h5>
                                <h6>Show:</h6>
                                <select name="per_page" onchange="javascript: $('#wishForm').submit();">
                                        <option value="20" <?php
                                        if ($per_page == 20) {
                                            echo "selected='selected'";
                                        }
                                        ?>>20</option>
                                        <option value="40" <?php
                                        if ($per_page == 40) {
                                            echo "selected='selected'";
                                        }
                                        ?>>40</option>
                                        <option value="60" <?php
                                        if ($per_page == 60) {
                                            echo "selected='selected'";
                                        }
                                        ?>>60</option>
                                        <option value="80" <?php
                                        if ($per_page == 80) {
                                            echo "selected='selected'";
                                        }
                                        ?>>80</option>
                                        <option value="100" <?php
                                        if ($per_page == 100) {
                                            echo "selected='selected'";
                                        }
                                        ?>>100</option>
                                </select>
                            </div>
                        </div>
                        <?php echo form_close(); ?>

                        <div class="shop-right-lwr">
                            <div class="cloths pad-tp-33">
                                <?php if(count($wish_list) > 0) { ?>
                                <?php
                                $divStart = 0;
                                        for ($i = 0; $i < count($wish_list); $i++) {
                                            ?>
                                            <?php
                                            if ($divStart == 0) {
                                                echo '<div class="cloths-row">';
                                                $divStart = 1;
                                            }
                                            ?>
                                
                                            <div class="cloths-bx <?php
                                            if ((($i + 1) % 3) == 0) {
                                                echo 'mrg-rgt-0';
                                            }
                                            ?>" >
                                        <div class="cloths-bx-img-sec">
                                            <img src="<?=base_url('resources/prod_images/'.$wish_list[$i]['img_name'])?>" alt="" title="" class="img-responsive"/>
                                            <a href="<?=base_url('removeWishListItem/'.$wish_list[$i]['id'])?>"><span class="cross-absolt"></span></a>
                                        </div>

                                        <div class="cloths-bx-hed">
                                            <?php 
                                            $produrl = base_url($wish_list[$i]['catURL']);
                                            if($wish_list[$i]['subCatURL'] != '') {
                                                $produrl .= '/'.$wish_list[$i]['subCatURL'];
                                            }
                                            if($wish_list[$i]['subSubCatURL'] != '') {
                                                $produrl .= '/'.$wish_list[$i]['subSubCatURL'];
                                            }
                                            ?>

                                            <a href="<?=$produrl.'/'.$wish_list[$i]['prod_url']?>" data-toggle="tooltip" data-placement="top" title="Product Detail"><?=$wish_list[$i]['prod_title']?></a>								
                                        </div>

                                        <div class="cloths-bx-lowr1">
                                            <a href="<?=base_url('store/'.$wish_list[$i]['store_url'])?>" data-toggle="tooltip" data-placement="top" title="Seller Store"><?=$wish_list[$i]['store_name']?></a>
                                            <p>
                                                <?php
                                                $sDate = date('Y-m-d', strtotime($wish_list[$i]['sale_start_date']));
                                                $eDate = date('Y-m-d', strtotime($wish_list[$i]['sale_end_date']));
                                                $tDate = date('Y-m-d');
                                                if($wish_list[$i]['prod_onsale'] == 1) {
                                                    if($wish_list[$i]['sale_start_date'] != '' && $wish_list[$i]['sale_end_date'] != '') {
                                                        if($tDate >= $sDate && $tDate <= $eDate) {
                                                            echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['sale_price'], 2,'.','').'</span> <span class="sp2">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                        } else {
                                                            echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                        }
                                                    }else if($wish_list[$i]['sale_start_date'] != '') {
                                                        if($tDate >= $sDate) {
                                                            echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['sale_price'], 2,'.','').'</span> <span class="sp2">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                        } else {
                                                            echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                        }
                                                    } else if($wish_list[$i]['sale_end_date'] != '') {
                                                        if($tDate <= $eDate) {
                                                            echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['sale_price'], 2,'.','').'</span> <span class="sp2">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                        } else {
                                                            echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                        }
                                                    } else {
                                                        echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['sale_price'], 2,'.','').'</span> <span class="sp2">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                    }
                                                } else {
                                                    echo 'Price:<span class="sp1">$'.number_format($wish_list[$i]['prod_price'], 2,'.','').'</span>';
                                                }
                                                ?>
                                            </p>
                                        </div>

                                        <!-- <div class="cloths-bx-lowr2">
                                                    <input class="comp-checks" type="checkbox" id="comp-<?= $wish_list[$i]['prod_id'] ?>" name="cc" onclick="comp_prod(<?= $wish_list[$i]['prod_id'] ?>)"/>
                                                    <label for="comp-<?= $wish_list[$i]['prod_id'] ?>"><span></span><p>Compare</p></label>
                                        </div> -->
                                        <div class="cloths-bx-lowr2">
                                            <input class="comp-checks" type="checkbox" id="comp-<?= $wish_list[$i]['prod_id'] ?>" name="cc" onclick="comp_prod(<?= $wish_list[$i]['prod_id'] ?>)"/>
                                            <label for="comp-<?= $wish_list[$i]['prod_id'] ?>"><span></span><p>Compare</p></label>
                                        </div>
                                        <div class="shop-det-rgt-lowr-4">
                                            <div class="shr-img">
                                                <ul>
                                                    <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?=$produrl.'/'.$wish_list[$i]['prod_url']?>" class="shr-fb" target="_blank"></a></li>
                                                    <li><a href="https://twitter.com/home?status=<?= $wish_list[$i]['prod_title'] ?>%20%20<?=$produrl.'/'.$wish_list[$i]['prod_url']?>" class="shr-fb2" target="_blank"></a></li>
                                                    <li><a href="https://pinterest.com/pin/create/button/?url=<?=$produrl.'/'.$wish_list[$i]['prod_url']?>&media=<?=$share_image?>&description=<?= $wish_list[$i]['prod_description']?>" class="shr-fb3" target="_blank"></a></li>
                                                    <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?=$produrl.'/'.$wish_list[$i]['prod_url']?>&title=<?=$wish_list[$i]['prod_title'] ?>&summary=<?=$wish_list[$i]['prod_description'] ?>" class="shr-fb4" target="_blank"></a></li>
                                                    <li><a href="mailto:?Subject=<?= $wish_list[$i]['prod_title'] ?>&body=<?=$produrl.'/'.$wish_list[$i]['prod_url']?>" class="shr-fb5"></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                            <?php
                                            if ((($i + 1) % 3) == 0) {
                                                echo '</div>';
                                                $divStart = 0;
                                            }
                                            ?>
                                
                                            <?php
                                        }
                                        if ($divStart == 1) {
                                            echo "</div>";
                                        }
                                ?>
                                        <?php
                                    } else {
                                    echo '<div class="cloths-row">No item found.</div>';
                                    }
                                    ?>
                            </div>
                        </div>
                        <div class="shop-right-third">
                            <div class="shop-right-third-lft">
                                <p>
                                        <?php
                                        if ($total_rows > 0) {
                                            echo $to + 1;
                                        } else {
                                            echo $to;
                                        }
                                        ?> - <?php
                                        if ($from > $total_rows) {
                                            echo $total_rows;
                                        } else {
                                            echo $from;
                                        }
                                        ?> of <?= $total_rows ?><!--  in Category Name -->
                                </p>
                            </div>
                            <div class="shop-right-third-rgt">
                                <ul class="pageing">
                                    <?=$paginglink?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    

        <?php $compare_listing = getCompareList2(); ?>
        <div class="compare-bg <?php
        if (empty($compare_listing)) {
            echo 'hide';
        }
        ?>" id="top-pag1">
        <div class="container">
            <div class="row">
                <div class="auto-container">
                    <div class="compare-main">
                            <div id="compare_listing">
                                <?= $compare_listing ?>

                        </div>
                        <div class="col-md-3 pad-25">
                                <a id="comp_btn" href="<?= base_url() . 'compare-details' ?>" class="com-btn <?php
                                if (empty($compare_listing)) {
                                    echo 'hide';
                                }
                                ?>">Compare</a>
                                <a href="javascript:void(0);" onclick="clearcompare()" class="clear-btn">Clear List</a>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<a data-toggle="modal" data-target="#myModal4" id="myAnchor" style="display: none;">Click HERE</a>
    <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog popup-4" role="document">
    <div class="modal-content">
      <div class="modal-header pad-0">        
        <button type="button" style="z-index: 99;" class="close" data-dismiss="modal" aria-label="Close">
        </button>
      </div>
      <div class="modal-body pad-0">
       <p class="mrg-botm-0" id="wishlist-text">Your item has been deleted successfully.</p>
      </div>
      
    </div>
</section>
<?php $compare_listing = getCompareList2();?>
<div class="compare-bg <?php if(empty($compare_listing)){echo 'hide';}?>" id="top-pag1">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="compare-main">
                    <div id="compare_listing">
                        <?=$compare_listing?>
                        
                    </div>
                    <div class="col-md-3 pad-25">
                        <a id="comp_btn" href="<?=base_url().'compare-details'?>" class="com-btn <?php if(empty($compare_listing)){echo 'hide';}?>">Compare</a>
                        <a href="javascript:void(0);" onclick="clearcompare()" class="clear-btn">Clear List</a>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        /*$("li.prev > a" ).addClass('page-left');
        $("li.next > a" ).addClass('page-right');*/

       /* $('a').filter('a[rel^="next"]').addClass('page-right');
        $('a').filter('a[rel^="prev"]').addClass('page-left');*/
     });
    
</script>
<script type="text/javascript">
                    function comp_prod(prod_id) {
                        var element = $(this);
                        var bt_check = $('#comp-' + prod_id).is(':checked');
                        //alert(prod_id+' '+bt_check);
                        var url = "<?= base_url() ?>compare-prods-2";
                        $.ajax({
                            url: url,
                            type: "post",
                            data: {prod_id: prod_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                            success: function (data) {
                                data = JSON.parse(data);

                                if (data.status == 201) {
                                    $('#comp_msg').html('Compare list is full.');
                                    $('.comp-alert').removeClass('hide');
                                    $('html, body').animate({
                                        scrollTop: $(".shop-right").offset().top
                                    }, 2000);
                                } else if (data.status == 200 || data.status == 900) {
                                    if (data.html != '') {
                                        $('#compare_listing').html(data.html);
                                        $('.compare-bg').removeClass('hide');
                                        $('.shop-container').addClass('compare-set');
                                        if (data.status == 900) {
                                            $('#comp-' + prod_id).removeAttr("checked");
                                            $('#comp2-' + prod_id).removeAttr("checked");
                                            $('.prod-td-' + $prod_id).remove();
                                        }
                                        if (data.total_prods < 2) {
                                            $('#comp_btn').hide();
                                        } else if (data.total_prods >= 2) {
                                            $('#comp_btn').removeClass('hide');
                                            $('#comp_btn').attr('style', 'display:block;');
                                            $('.com-btn ').show();
                                        }
                                    } else {
                                        $('.comp-checks').removeAttr("checked");
                                        $('.compare-bg').addClass('hide');
                                        $('.shop-container').removeClass('compare-set');
                                    }
                                }
                                //}
                            }
                        });
                    }

                    function clearcompare() {
                        var url = "<?= base_url() ?>clear-compare-list";
                        $.ajax({
                            url: url,
                            type: "post",
                            data: {<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                            success: function (data) {
                                data = JSON.parse(data);
                                if (data.status == 200) {
                                    $('.close').click();
                                    $('.comp-checks').removeAttr("checked");
                                    $('.compare-bg').addClass('hide');
                                    $('.shop-container').removeClass('compare-set');
                                }
                            }
                        });
                        //alert('Compare List is now Clear!');
                    }
                    function removeslctitem(prod_id) {
                        $.ajax({
                            type: 'POST',
                            url: '<?= base_url() . "remove-compare-prod" ?>',
                            data: {prod_id: prod_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                            success: function (data) {
                                data = JSON.parse(data);
                                if (data.status == 200) {
                                    if (data.total_prods > 0) {
                                        console.log(data.msg);
                                        $('#comp-' + prod_id).removeAttr("checked");
                                        $('#comp2-' + prod_id).removeAttr("checked");
                                        $('.prod-td-' + prod_id).remove();
                                        console.log(data.compare_prods);
                                        if (data.total_prods < 2) {
                                            $('#comp_btn').hide();
                                        } else if (data.total_prods >= 2) {
                                            $('#comp_btn').removeClass('hide');
                                            $('#comp_btn').attr('style', 'display:block;');
                                            $('.com-btn ').show();
                                        }
                                    } else {
                                        $('.comp-checks').removeAttr("checked");
                                        $('.compare-bg').addClass('hide');
                                        $('.shop-container').removeClass('compare-set');
                                    }
                                }
                            },
                            error: function () {
                                console.log('Network error.');
                            }
                        });
                    }
                </script>
                <script type="text/javascript">
    $(document).ready(function(){
        var message = '<?=$this->session->flashdata('msg');?>';
        if(message!=''){
            $('#myAnchor').trigger('click'); 
        }
    });
</script>
<script>
    function comp_prod(prod_id){
        var element = $(this);
        var bt_check = $('#comp-'+prod_id).is(':checked');
        //alert(prod_id+' '+bt_check);
        var url = "<?= base_url() ?>compare-prods-2";
        $.ajax({
            url: url,
            type: "post",
            data: {prod_id: prod_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function (data) {
                data = JSON.parse(data);
                
                    if(data.status == 201){
                        $('#comp_msg').html('Compare list is full. Please select atmost 3 Products.');
                        $('.comp-alert').show();
                    }else if(data.status == 200 || data.status == 900){
                        if(data.html != ''){
                            $('#compare_listing').html(data.html);
                            $('.compare-bg').removeClass('hide');
                            $('.shop-container').addClass('compare-set');
                            if(data.status == 900){
                                $('#comp-'+prod_id).removeAttr("checked");
                                $('#comp2-'+prod_id).removeAttr("checked");
                                $('.prod-td-'+$prod_id).remove();
                            }
                            if(data.total_prods < 2){
                                $('#comp_btn').hide();
                            }else if(data.total_prods >= 2){
                                $('#comp_btn').removeClass('hide');
                                $('#comp_btn').attr('style','display:block;');
                                $('.com-btn ').show();
                            }
                        }else{
                            $('.comp-checks').removeAttr("checked");
                            $('.compare-bg').addClass('hide');
                            $('.shop-container').removeClass('compare-set');
                        }
                    }
                //}
            }
        });
    }

    function clearcompare(){
        var url = "<?= base_url() ?>clear-compare-list";
        $.ajax({
            url: url,
            type: "post",
            data: {<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function (data) {
                data = JSON.parse(data);
                if(data.status == 200 ){
                    $('.close').click();
                    $('.comp-checks').removeAttr("checked");
                    $('.compare-bg').addClass('hide');
                    $('.shop-container').removeClass('compare-set');
                }
            }
        });
        //alert('Compare List is now Clear!');
    }

    function removeslctitem(prod_id){
        $.ajax({
            type:'POST',
            url:'<?=base_url()."remove-compare-prod"?>',
            data:{prod_id:prod_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function(data){
                data = JSON.parse(data);
                if(data.status == 200 ){
                    if(data.total_prods > 0){
                        console.log(data.msg);
                        $('#comp-'+prod_id).removeAttr("checked");
                        $('#comp2-'+prod_id).removeAttr("checked");
                        $('.prod-td-'+prod_id).remove();
                        console.log(data.compare_prods);
                        if(data.total_prods < 2){
                            $('#comp_btn').hide();
                        }else if(data.total_prods >= 2){
                                $('#comp_btn').removeClass('hide');
                                $('#comp_btn').attr('style','display:block;');
                                $('.com-btn ').show();
                            }
                    }else{
                        $('.comp-checks').removeAttr("checked");
                        $('.compare-bg').addClass('hide');
                        $('.shop-container').removeClass('compare-set');
                    }
                }
            },
            error: function(){
                console.log('Network error.');
            }
        });
    }
</script>
<!--Shop Filters ends here-->
