<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard'); ?>">My Account</a></li>
                        <li class="breadcrumb-item active">Store Settings</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->
<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav'); ?>
                <!--Left section starts here-->
                <!-- Right Pannel Starts Here -->
                <div class="col-sm-9 col-xs-12">
                    <div class="ualert-row">
                    <?php
                    if ($this->session->flashdata('storeSetting') != "") {
                        echo '<div id="message" class="alert alert-success message "><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>' . $this->session->flashdata('storeSetting') . '</div>';
                    }
                    ?>
                    </div>
                    <div class="my-account-right">
                        <!-- Inner Section Starts -->
                        <div class="message-wrapper">
                            <!-- Heading Section Starts -->
                            <div class="my-account-right-head addbtmPad">
                                <div class="msg-hd-inrow">
                                    <h5>Store Settings</h5>
                                </div>
                                <!-- <p>Sample Text Here.</p> -->
                            </div>
                            <!-- Heading Section Ends -->
                            <!-- Bottom Section Starts -->
                            <div class="store-btmWrap">
                                <!-- Store Section Starts -->
                                <div class="store-mainsec">
                                    <!-- Top Grey Bar Starts -->
                                    <div class="topGrey-bar">
                                        <div class="topGrey-inner">
                                            <!-- Search By Form Starts -->
                                            <div class="store-tophdr">
                                                <!-- Left Heading Starts -->
                                                <div class="store-th">
                                                    <h3>Customer Discount</h3>
                                                </div>
                                                <!-- Left Heading Ends -->
                                            </div>
                                            <!-- Search By Form Ends -->
                                        </div>
                                    </div>
                                    <!-- Top Grey Bar Ends -->
                                    <?php echo form_open('', array('name'=>'storeForm', 'id'=>'storeForm')); ?>
                                    <!-- Bottom Grey Section Starts -->
                                    <div class="btm-boxwrap">
                                        <div class="btm-boxinner">
                                            <!-- Section Starts -->
                                            <div class="btm-boxfrm">
                                                <div class="btm-ftxt">
                                                    Offer your customers a discount if they buy more than one item from you!
                                                </div>
                                                <!-- Row Starts Here -->
                                                <div class="btm-frow">
                                                    <div class="bf-rleft">
                                                        <label>
                                                            Percent Discount
                                                        </label>
                                                    </div>
                                                    <div class="bf-rright">
                                                        <!-- Form Field Starts -->
                                                        <div class="base-txtbox">
                                                            <input type="text" name="perDiscount" value="<?=(set_value('perDiscount') != '')?set_value('perDiscount'):$page_data->store_discount?>" placeholder="5% off" />
                                                        </div>
                                                        <!-- Form Field Ends -->
                                                        <div class="error"><?=form_error('perDiscount')?></div>
                                                    </div>
                                                </div>
                                                <!-- Row Ends Here -->
                                                <!-- Row Starts Here -->
                                                <div class="btm-frow">
                                                    <div class="bf-rleft">
                                                        <label>
                                                            Number of Items
                                                        </label>
                                                    </div>
                                                    <div class="bf-rright">
                                                        <!-- Form Field Starts -->
                                                        <div class="base-txtbox">
                                                            <input type="text" name="numberItems" value="<?=(set_value('numberItems') != '')?set_value('numberItems'):$page_data->store_discount_item?>" placeholder="2 Listings" />
                                                        </div>
                                                        <!-- Form Field Ends -->
                                                        <div class="error"><?=form_error('numberItems')?></div>
                                                    </div>
                                                </div>
                                                <!-- Row Ends Here -->
                                                <!-- Row Starts Here -->
                                                <div class="btm-frow">
                                                    <div class="bf-btnright">
                                                        <div class="base-tbtn">
                                                            <input type="submit" name="SAVE" value="SAVE" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Row Ends Here -->
                                            </div>
                                            <!-- Section Ends -->
                                        </div>
                                    </div>
                                    <!-- Bottom Grey Section Ends -->
                                    <?php echo form_close(); ?>
                                </div>
                                <!-- Store Section Ends -->
                                <!-- Store Section Starts -->
                                <div class="store-mainsec">
                                    <!-- Top Grey Bar Starts -->
                                    <div class="topGrey-bar">
                                        <div class="topGrey-inner">
                                            <!-- Search By Form Starts -->
                                            <div class="store-tophdr">
                                                <!-- Left Heading Starts -->
                                                <div class="store-th">
                                                    <h3>vacation Mode</h3>
                                                </div>
                                                <!-- Left Heading Ends -->
                                            </div>
                                            <!-- Search By Form Ends -->
                                        </div>
                                    </div>
                                    <!-- Top Grey Bar Ends -->
                                    <!-- Bottom Grey Section Starts -->
                                    <div class="btm-boxwrap">
                                        <div class="btm-boxinner">
                                            <!-- Section Starts -->
                                            <div class="btm-boxfrm">
                                                <div class="btm-ftxt">
                                                    Want to take a break from selling?
                                                </div>
                                                <!-- Row Starts Here -->
                                                <div class="btm-frow">
                                                    <div class="bf-rleft">
                                                        <label>
                                                            Hide Store
                                                        </label>
                                                    </div>
                                                    <div class="bf-rright">
                                                        <!-- Form Field Starts -->
                                                        <div class="base-checkbox">
                                                            <label class="switch">
                                                                <input type="checkbox" class="uavailable" <?=($page_data->store_is_hide == '1'? 'checked':"")?> id="hide_store" name="uavailable" onclick="hidestore(this)" />
                                                                <div class="slider round" title="OFF"></div>
                                                            </label>
                                                        </div>
                                                        <!-- Form Field Ends -->
                                                        <div class="error" style="display:none;">
                                                            <p>This field is required.</p>
                                                        </div>
                                                        <input type="hidden" id="user_id" value="<?= $userid ?>">
                                                    </div>
                                                </div>
                                                <!-- Row Ends Here -->
                                            </div>
                                            <!-- Section Ends -->
                                        </div>
                                    </div>
                                    <!-- Bottom Grey Section Ends -->
                                </div>
                                <!-- Store Section Ends -->
                            </div>
                            <!-- Bottom Section Ends -->
                        </div>
                        <!-- Inner Section Ends -->
                    </div>
                </div>
                <!-- Right Pannel Starts Here -->
            </div>
        </div>
    </div>
</section>
<script>
function hidestore(checkbox) {
    var value = 0;
    var userid = $('#user_id').val();
    var url = "<?= base_url() ?>store-hide";
    if (checkbox.checked) {
        value = 1;
    }
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: { "store_hide": value, user_id: userid, <?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>' },
        success: function(data) {
            //                    if (data.status == 'Available') {
            //                    }
        }
    });
}
</script>