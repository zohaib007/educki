<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard'); ?>">My Account </a></li>
                        <li class="breadcrumb-item active"> Facebook Store</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav'); ?> 
                <!--Left section starts here-->

                <!--right section starts here-->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">
                        <div class="mang-prod-mian">
                            <!--Manage product section-->	
                            <div class="mang-prod-mian-hed">
                                <h5> Facebook Store</h5>
                            </div>
                            <!--Manage product section-->

                            <!--Tabs section start here-->
                            <div class="fb-stor">
                                <a href="#" class="fb-stbtn">FB Store</a>
                                <a href="#" class="fb-stbtn">Delete</a>
                            </div>
                            <!--Tabs section start here--> 

                            <!--Select option starts here-->
                            <div class="select-optn-sec">
                                <div class="my-account-right-2">
                                    <div class="my-account-right-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="text-center" style="width:14%;">ID</th>
                                                    <th style="width:1%;"></th>
                                                    <th class="pad-lft-0" style="width:13%;">Item</th>
                                                    <th style="width:31%;">Product Description</th>
                                                    <th class="text-center" style="width:14%;">Sale Price</th>
                                                    <th style="width:13%;" class="text-center">Facebook</th>
                                                    <th style="width:14%;" class="pad-rgt-0">Social Media</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox"/>
                                                        <p class="pull-right fnt-chnag">11178408</p>
                                                    </td>
                                                    <td></td>
                                                    <td class="pad-lft-0"><img src="<?=base_url()?>front_resources/images/product-slide-thumbl.jpg" alt="" title="" class=""></td>
                                                    <td>Sample Text Here Sample Text Here Sample Text Here Sample</td>
                                                    <td class="text-center">
                                                        $00.00
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                    <td class="pad-rgt-0">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox"/>
                                                        <p class="pull-right fnt-chnag">11178408</p>
                                                    </td>
                                                    <td></td>
                                                    <td class="pad-lft-0"><img src="<?=base_url()?>front_resources/images/product-slide-thumbl.jpg" alt="" title="" class=""></td>
                                                    <td>Sample Text Here Sample Text Here Sample Text Here Sample</td>
                                                    <td class="text-center">
                                                        $00.00
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                    <td class="pad-rgt-0">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox"/>
                                                        <p class="pull-right fnt-chnag">11178408</p>
                                                    </td>
                                                    <td></td>
                                                    <td class="pad-lft-0"><img src="<?=base_url()?>front_resources/images/product-slide-thumbl.jpg" alt="" title="" class=""></td>
                                                    <td>Sample Text Here Sample Text Here Sample Text Here Sample</td>
                                                    <td class="text-center">
                                                        $00.00
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                    <td class="pad-rgt-0">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox"/>
                                                        <p class="pull-right fnt-chnag">11178408</p>
                                                    </td>
                                                    <td></td>
                                                    <td class="pad-lft-0"><img src="<?=base_url()?>front_resources/images/product-slide-thumbl.jpg" alt="" title="" class=""></td>
                                                    <td>Sample Text Here Sample Text Here Sample Text Here Sample</td>
                                                    <td class="text-center">
                                                        $00.00
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                    <td class="pad-rgt-0">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox"/>
                                                        <p class="pull-right fnt-chnag">11178408</p>
                                                    </td>
                                                    <td></td>
                                                    <td class="pad-lft-0"><img src="<?=base_url()?>front_resources/images/product-slide-thumbl.jpg" alt="" title="" class=""></td>
                                                    <td>Sample Text Here Sample Text Here Sample Text Here Sample</td>
                                                    <td class="text-center">
                                                        $00.00
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                    <td class="pad-rgt-0">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox"/>
                                                        <p class="pull-right fnt-chnag">11178408</p>
                                                    </td>
                                                    <td></td>
                                                    <td class="pad-lft-0"><img src="<?=base_url()?>front_resources/images/product-slide-thumbl.jpg" alt="" title="" class=""></td>
                                                    <td>Sample Text Here Sample Text Here Sample Text Here Sample</td>
                                                    <td class="text-center">
                                                        $00.00
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                    <td class="pad-rgt-0">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox"/>
                                                        <p class="pull-right fnt-chnag">11178408</p>
                                                    </td>
                                                    <td></td>
                                                    <td class="pad-lft-0"><img src="<?=base_url()?>front_resources/images/product-slide-thumbl.jpg" alt="" title="" class=""></td>
                                                    <td>Sample Text Here Sample Text Here Sample Text Here Sample</td>
                                                    <td class="text-center">
                                                        $00.00
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                    <td class="pad-rgt-0">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox"/>
                                                        <p class="pull-right fnt-chnag">11178408</p>
                                                    </td>
                                                    <td></td>
                                                    <td class="pad-lft-0"><img src="<?=base_url()?>front_resources/images/product-slide-thumbl.jpg" alt="" title="" class=""></td>
                                                    <td>Sample Text Here Sample Text Here Sample Text Here Sample</td>
                                                    <td class="text-center">
                                                        $00.00
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                    <td class="pad-rgt-0">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox"/>
                                                        <p class="pull-right fnt-chnag">11178408</p>
                                                    </td>
                                                    <td></td>
                                                    <td class="pad-lft-0"><img src="<?=base_url()?>front_resources/images/product-slide-thumbl.jpg" alt="" title="" class=""></td>
                                                    <td>Sample Text Here Sample Text Here Sample Text Here Sample</td>
                                                    <td class="text-center">
                                                        $00.00
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                    <td class="pad-rgt-0">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="checkbox"/>
                                                        <p class="pull-right fnt-chnag">11178408</p>
                                                    </td>
                                                    <td></td>
                                                    <td class="pad-lft-0"><img src="images/product-slide-thumbl.jpg" alt="" title="" class=""></td>
                                                    <td>Sample Text Here Sample Text Here Sample Text Here Sample</td>
                                                    <td class="text-center">
                                                        $00.00
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                    <td class="pad-rgt-0">
                                                        <a href="#" class="tble-shre">Share</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--Select option ends here-->   

                            <div class="shop-right-third">
                                <div class="shop-right-third-lft">
                                    <p>
                                        1 - 10 of 14
                                    </p>
                                </div>
                                <div class="shop-right-third-rgt">
                                    <ul>
                                        <li>
                                            <a href="#" class="page-left"></a>
                                        </li>
                                        <li>
                                            <a href="#" class="active">1</a>
                                        </li>
                                        <li>
                                            <a href="#">2</a>
                                        </li>
                                        <li>
                                            <a href="#">3</a>
                                        </li>
                                        <li>
                                            <a href="#">4</a>
                                        </li>
                                        <li>
                                            <a href="#">5</a>
                                        </li>
                                        <li>
                                            <a href="#" class="page-right"></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--right section starts here-->
            </div>
        </div>
    </div>
</section>
<!--Blog main ends  here-->
<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });


    $(document).ready(function () {
        $(".bulk").click(function () {
            $(".blk-prd-upld").toggle(1000);
        });
    });

</script>