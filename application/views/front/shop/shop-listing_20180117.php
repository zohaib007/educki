<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item">Shop</li>
                        <?php if ($breadcrumbs_data != false) { ?>
                            <li class="breadcrumb-item"><a href="<?= base_url() ?><?= $breadcrumbs_data->catURL ?>"><?= $breadcrumbs_data->catName ?></a></li>
                            <?php if ($segment2 != '') { ?>
                                <li class="breadcrumb-item active"><a href="<?= base_url() ?><?= $breadcrumbs_data->catURL ?>/<?= $breadcrumbs_data->subCatURL ?>"><?= $breadcrumbs_data->subCatName ?></a></li>
                            <?php } if ($segment3 != '' && $segment4 != '') { ?>
                                <li class="breadcrumb-item active"><a href="<?= base_url() ?><?= $breadcrumbs_data->catURL ?>/<?= $breadcrumbs_data->subCatURL ?>/<?= $breadcrumbs_data->subSubCatURL ?>"><?= $breadcrumbs_data->subSubCatName ?></a></li>
                                <?php
                            } else {
                                if ($breadcrumbs_data->subSubCatName != '' && $segment3 != '') {
                                    ?>
                                    <li class="breadcrumb-item active"><?= $breadcrumbs_data->subSubCatName ?></li>
                                <?php } ?>                                
                            <?php } if ($segment4 != '' && !is_numeric($lastPart)) { ?>
                                <li class="breadcrumb-item active"><?= $segment4 ?></li>
                            <?php } ?>
                        <?php } ?>
                    </ol>
                </div>
                <div class="shop-tit">
                    <h2>Shop</h2>
                </div>         
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->

<!--Shop Filters starts here-->
<section class="shp-fltr-sec neg-margin">
    <div class="container shop-container">
        <div class="row">
            <div class="auto-container">
                <div class="col-sm-3 col-xs-3 pad-lft-0" >
                    <?php
                    $this->load->view('front/includes/shop-left-nav');
                    ?>  
                </div>
                <div class="col-sm-9 col-xs-9">
                    <div class="shop-right">
                        <div class="comp-alert alert alert-danger alert-dismissable fade in hide" >
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <span id="comp_msg">This alert box could indicate a dangerous or potentially negative action.</span>
                          </div>
                        <?php if ($category_details['product_lising'] != false) { ?>
                            <div class="shop-right-upr">
                                <?php echo form_open('', array('name' => 'shopsortForm', 'method' => 'get', 'id' => 'shopsortForm')); ?>
                                <div class="srt-sec">
                                    <h6>Sort By:</h6>
                                    <select name="sort_by" onchange="submit(shopsortForm)">
                                        <option value="1" <?php
                                        if ($category_details['sort_by'] == 1) {
                                            echo "selected='selected'";
                                        }
                                        ?>>Newest Arrivals</option>
                                        <option value="2" <?php
                                        if ($category_details['sort_by'] == 2) {
                                            echo "selected='selected'";
                                        }
                                        ?> >Featured</option>
                                        <option value="3" <?php
                                        if ($category_details['sort_by'] == 3) {
                                            echo "selected='selected'";
                                        }
                                        ?> >Price: Low to High</option>
                                        <option value="4" <?php
                                        if ($category_details['sort_by'] == 4) {
                                            echo "selected='selected'";
                                        }
                                        ?> >Price: High to Low</option>
                                    </select>
                                </div>
                                <?php echo form_close(); ?>
                                <div class="tb-sec">
                                    <ul class="nav nav-tabs">
                                        <li><a data-toggle="tab" href="#home" class="tab-1 active" onclick="toggleClass(this);"></a></li>
                                        <li><a data-toggle="tab" href="#menu1" class="tab-2" onclick="toggleClass(this);"></a></li>
                                    </ul>
                                </div>
                                <?php echo form_open('', array('name' => 'shopForm', 'method' => 'get', 'id' => 'shopForm')); ?>
                                <div class="show-itm">
                                    <h5><?php
                                        if ($category_details['total_rows'] > 0) {
                                            echo $category_details['per_page'];
                                        } else {
                                            echo $category_details['total_rows'];
                                        }
                                        ?> Item(s)</h5>
                                    <h6>Show:</h6>
                                    <select name="per_page" onchange="submit('shopForm');">
                                        <option value="10" <?php
                                        if ($category_details['per_page'] == 10) {
                                            echo "selected='selected'";
                                        }
                                        ?>>10</option>
                                        <option value="20" <?php
                                        if ($category_details['per_page'] == 20) {
                                            echo "selected='selected'";
                                        }
                                        ?>>20</option>
                                        <option value="30" <?php
                                        if ($category_details['per_page'] == 30) {
                                            echo "selected='selected'";
                                        }
                                        ?>>30</option>
                                        <option value="40" <?php
                                        if ($category_details['per_page'] == 40) {
                                            echo "selected='selected'";
                                        }
                                        ?>>40</option>
                                        <option value="50" <?php
                                        if ($category_details['per_page'] == 50) {
                                            echo "selected='selected'";
                                        }
                                        ?>>50</option>
                                    </select>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        <?php } ?>
                        <?php if ($category_details['product_lising'] != false) { ?>
                            <div class="shop-right-lwr">
                                <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active">
                                        <div class="cloths">
                                            <?php
                                            foreach (array_chunk($category_details['product_lising'], 3) as $newlist) {
                                                echo '</div><div class="cloths-row">';
                                                $count = 1;
                                                foreach ($newlist as $prod) {
                                                    ?>
                                                    <div class="cloths-bx <?= ($count % 3 == 0) ? 'mrg-rgt-0' : '' ?>">
                                                        <div class="cloths-bx-img-sec">
                                                            <img src="<?= base_url() ?>resources/prod_images/<?= $prod['img_name'] ?>" alt="" title="" class="img-responsive"/>
                                                            <!--<span class="featured-absolt"></span>-->
                                                        </div>

                                                        <div class="cloths-bx-hed">
                                                            <?php if ($category_level == 3) { ?>
                                                                <a href="<?= base_url() ?><?= $segment1 ?>/<?= $segment2 ?>/<?= $segment3 ?>/<?= $prod['prod_url'] ?>" data-toggle="tooltip" data-placement="right" title="Products Detail" class="left-tol"><?= $prod['prod_title'] ?></a>								
                                                            <?php } ?>
                                                            <?php if ($category_level == 2) { ?>
                                                                <a href="<?= base_url() ?><?= $segment1 ?>/<?= $segment2 ?>/<?= $prod['prod_url'] ?>" data-toggle="tooltip" data-placement="right" title="Products Detail" class="left-tol"><?= $prod['prod_title'] ?></a>								
                                                            <?php } ?>
                                                            <?php if ($category_level == 1) { ?>
                                                                <a href="<?= base_url() ?><?= $segment1 ?>/<?= $prod['prod_url'] ?>" data-toggle="tooltip" data-placement="right" title="Products Detail" class="left-tol"><?= $prod['prod_title'] ?></a>								
                                                            <?php } ?>
                                                        </div>

                                                        <div class="cloths-bx-lowr1">
                                                            <a href="<?= base_url() ?>store/<?= $prod['store_url'] ?>" data-toggle="tooltip" data-placement="top" title="Seller Store"><?= $prod['store_name']; ?></a>
                                                            <p>
                                                                   
                                                                    <?php
                                    $productPrice = 0.0;
                                    $sDate = date('Y-m-d', strtotime($prod['sale_start_date']));
                                    $eDate = date('Y-m-d', strtotime($prod['sale_end_date']));
                                    $tDate = date('Y-m-d');
                                    if ($prod['prod_onsale'] == 1) {
                                        if ($prod['sale_start_date'] != '' && $prod['sale_end_date'] != '') {
                                            if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                                                $productPrice = number_format($prod['prod_price'], 2, '.', '');
                                                echo '<span class="sp1">$ ' . number_format($prod['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod['prod_price'], 2, '.', '') . '</span>';
                                            } else {
                                                $productPrice = number_format($prod['prod_price'], 2, '.', '');
                                                echo '$' . number_format($prod['prod_price'], 2, '.', '') . '</span>';
                                            }
                                        } else if ($prod['sale_start_date'] != '') {
                                            if ($tDate >= $sDate) {
                                                $productPrice = number_format($prod['prod_price'], 2, '.', '');
                                                echo '<span class="sp1">$ ' . number_format($prod['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod['prod_price'], 2, '.', '') . '</span>';
                                            } else {
                                                $productPrice = number_format($prod['prod_price'], 2, '.', '');
                                                echo '$' . number_format($prod['prod_price'], 2, '.', '') . '</span>';
                                            }
                                        } else if($prod['sale_end_date'] != '') {
                                                        if($tDate <= $eDate) {
                                                            echo 'Price:<span class="sp1">$'.number_format($prod['sale_price'], 2,'.','').'</span> <span class="sp2">$'.number_format($prod['prod_price'], 2,'.','').'</span>';
                                                        } else {
                                                            echo 'Price:<span class="sp1">$'.number_format($prod['prod_price'], 2,'.','').'</span>';
                                                        }
                                                    }else {
                                            $productPrice = number_format($prod['prod_price'], 2, '.', '');
                                            echo '<span class="sp1">$' . number_format($prod['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod['prod_price'], 2, '.', '') . '</span>';
                                        }
                                    } else {
                                        $productPrice = number_format($prod['prod_price'], 2, '.', '');
                                        echo '$' . number_format($prod['prod_price'], 2, '.', '');
                                    }
                                    ?>
                                                            </p>
                                                        </div>

                                                        <div class="cloths-bx-lowr2">
                                                            <input class="comp-checks" type="checkbox" id="comp-<?= $prod['prod_id'] ?>" name="cc" onclick="comp_prod(<?= $prod['prod_id'] ?>, this)"/>
                                                            <label for="comp-<?= $prod['prod_id'] ?>"><span></span><p>Compare</p></label>
                                                            <?php if ($this->session->userdata('logged_in')) { ?>
                                                                <a href="javascript:;" data-id ="<?= $prod['prod_id'] ?>" class="wish <?php
                                                                if (check_wishlist_item($prod['prod_id'])) {
                                                                    echo 'active';
                                                                }
                                                                ?>" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"></a>
                                                               <?php } ?>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $count ++;
                                                }
                                                ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div id="menu1" class="tab-pane fade">
                                        <div class="cloths">
                                            <div class="cloths-row">
                                                <?php foreach ($category_details['product_lising'] as $prod) { ?>
                                                    <div class="cloths-bx-2">
                                                        <div class="cloths-bx-2-img">
                                                            <img src="<?= base_url() ?>resources/prod_images/<?= $prod['img_name'] ?>" alt="" title="" class="img-responsive"/>
                                                            <!--<span class="featured-absolt"></span>-->
                                                        </div>
                                                        <div class="cloths-bx-2-rgt">
                                                            <div class="cloths-bx-hed2"> 
                                                                <?php if ($category_level == 3) { ?>
                                                                    <a href="<?= base_url() ?><?= $segment1 ?>/<?= $segment2 ?>/<?= $segment3 ?>/<?= $prod['prod_url'] ?>" data-toggle="tooltip" data-placement="right" title="Products Detail" class="left-tol"><?= $prod['prod_title'] ?></a>								
                                                                <?php } ?>
                                                                <?php if ($category_level == 2) { ?>
                                                                    <a href="<?= base_url() ?><?= $segment1 ?>/<?= $segment2 ?>/<?= $prod['prod_url'] ?>" data-toggle="tooltip" data-placement="right" title="Products Detail" class="left-tol"><?= $prod['prod_title'] ?></a>								
                                                                <?php } ?>
                                                                <?php if ($category_level == 1) { ?>
                                                                    <a href="<?= base_url() ?><?= $segment1 ?>/<?= $prod['prod_url'] ?>" data-toggle="tooltip" data-placement="right" title="Products Detail" class="left-tol"><?= $prod['prod_title'] ?></a>								
                                                                <?php } ?>
                                                            </div>

                                                            <div class="cloths-bx-lowr3">
                                                                <a href="<?= base_url() ?>store/<?= $prod['store_url'] ?>" data-toggle="tooltip" data-placement="top" title="Seller Store"><?= $prod['store_name']; ?></a>
                                                                <p>
                                                                              
                                                                    <?php
                                    $productPrice = 0.0;
                                    $sDate = date('Y-m-d', strtotime($prod['sale_start_date']));
                                    $eDate = date('Y-m-d', strtotime($prod['sale_end_date']));
                                    $tDate = date('Y-m-d');
                                    if ($prod['prod_onsale'] == 1) {
                                        if ($prod['sale_start_date'] != '' && $prod['sale_end_date'] != '') {
                                            if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                                                $productPrice = number_format($prod['prod_price'], 2, '.', '');
                                                echo '<span class="sp1">$ ' . number_format($prod['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod['prod_price'], 2, '.', '') . '</span>';
                                            } else {
                                                $productPrice = number_format($prod['prod_price'], 2, '.', '');
                                                echo '$' . number_format($prod['prod_price'], 2, '.', '') . '</span>';
                                            }
                                        } else if ($prod['sale_start_date'] != '') {
                                            if ($tDate >= $sDate) {
                                                $productPrice = number_format($prod['prod_price'], 2, '.', '');
                                                echo '<span class="sp1">$ ' . number_format($prod['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod['prod_price'], 2, '.', '') . '</span>';
                                            } else {
                                                $productPrice = number_format($prod['prod_price'], 2, '.', '');
                                                echo '$' . number_format($prod['prod_price'], 2, '.', '') . '</span>';
                                            }
                                        } else if($prod['sale_end_date'] != '') {
                                                        if($tDate <= $eDate) {
                                                            echo 'Price:<span class="sp1">$'.number_format($prod['sale_price'], 2,'.','').'</span> <span class="sp2">$'.number_format($prod['prod_price'], 2,'.','').'</span>';
                                                        } else {
                                                            echo 'Price:<span class="sp1">$'.number_format($prod['prod_price'], 2,'.','').'</span>';
                                                        }
                                                    }else {
                                            $productPrice = number_format($prod['prod_price'], 2, '.', '');
                                            echo '<span class="sp1">$ ' . number_format($prod['sale_price'], 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod['prod_price'], 2, '.', '') . '</span>';
                                        }
                                    } else {
                                        $productPrice = number_format($prod['prod_price'], 2, '.', '');
                                        echo '$' . number_format($prod['prod_price'], 2, '.', '');
                                    }
                                    ?>
                                                           
                                                                </p>
                                                            </div>

                                                            <div class="cloths-bx-lowr4">
                                                                <input class="comp-checks" type="checkbox" id="comp2-<?= $prod['prod_id'] ?>" name="cc" onclick="comp_prod(<?= $prod['prod_id'] ?>, this)"/>
                                                            <label for="comp2-<?= $prod['prod_id'] ?>"><span></span><p>Compare</p></label>

                                                                <?php if ($this->session->userdata('logged_in')) { ?>
                                                                    <a href="javascript:;" data-id ="<?= $prod['prod_id'] ?>" class="wish <?php
                                                                    if (check_wishlist_item($prod['prod_id'])) {
                                                                        echo 'active';
                                                                    }
                                                                    ?>" data-toggle="tooltip" data-placement="bottom" title="Add to Wishlist"></a>
                                                                   <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="shop-right-third">
                            <div class="shop-right-third-lft">
                                <p>
                                    <?php
                                    if ($category_details['total_rows'] > 0) {
                                        echo $category_details['to'] + 1;
                                    } else {
                                        echo $category_details['to'];
                                    }
                                    ?> - <?php
                                    if ($category_details['from'] > $category_details['total_rows']) {
                                        echo $category_details['total_rows'];
                                    } else {
                                        echo $category_details['from'];
                                    }
                                    ?> of <?= $category_details['total_rows'] ?><!--  in Category Name -->
                                </p>
                            </div>
                            <div class="shop-right-third-rgt">
                                <ul>
                                    <?= $category_details['paginglink'] ?>
                                </ul>
                            </div>
                        </div>
                        <?php
                    } else {
                        echo '<div class="cloths-row">No item found.</div>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $compare_listing = getCompareList2();?>
<div class="compare-bg <?php if(empty($compare_listing)){echo 'hide';}?>" id="top-pag1">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="compare-main">
                    <div id="compare_listing">
                        <?=$compare_listing?>
                        
                    </div>
                    <div class="col-md-3 pad-25">
                        <a id="comp_btn" href="<?=base_url().'compare-details'?>" class="com-btn <?php if(empty($compare_listing)){echo 'hide';}?>">Compare</a>
                        <a href="javascript:void(0);" onclick="clearcompare()" class="clear-btn">Clear List</a>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<a data-toggle="modal" data-target="#myModal4" id="myAnchor" style="display: none;">Click HERE</a>
<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog popup-4" role="document">
        <div class="modal-content">
            <div class="modal-header pad-0">        
                <button type="button" style="z-index: 99;" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body pad-0">
                <p class="mrg-botm-0" id="wishlist-text"></p>
            </div>

        </div>
        </section>
        <!--Shop Filters ends here-->
        <script>
            $(document).ready(function () {

            if (typeof(Storage) !== "undefined") {
                var selectedTab = sessionStorage.getItem("shopListingTab");
                console.log(selectedTab);
                console.log( jQuery("a[href='"+selectedTab+"']").length && selectedTab != "" && selectedTab != null && selectedTab != "null");
                if ( jQuery("a[href='"+selectedTab+"']").length && selectedTab != "" && selectedTab != null && selectedTab != "null") {
                    setTimeout(function(){
                        jQuery("a[href='"+selectedTab+"']").trigger("click");
                    }, 1000);
                }
            } else {
                // Sorry! No Web Storage support..
            }

   // $('.crg-sub').addClass('collapse');
//    $('.crg-sub').attr('aria-expanded',false);
            });
            function submit(id) {
                $('#' + id).submit();
            }
            var acc = document.getElementsByClassName("accordion");
            var i;

            for (i = 0; i < acc.length; i++) {
                acc[i].onclick = function () {
                    /* Toggle between adding and removing the "active" class,
                     to highlight the button that controls the panel */
                    this.classList.toggle("active");

                    /* Toggle between hiding and showing the active panel */
                    var panel = this.nextElementSibling;
                    if (panel.style.display === "block") {
                        panel.style.display = "none";
                    } else {
                        panel.style.display = "block";
                    }
                }
            }
        </script>
        <?php if ($category_details['product_price']->product_count > 1) { ?>
            <script>

                var rangeOne = document.querySelector('input[name="rangeOne"]'),
                        rangeTwo = document.querySelector('input[name="rangeTwo"]'),
                        outputOne = document.querySelector('.outputOne'),
                        outputTwo = document.querySelector('.outputTwo'),
                        inclRange = document.querySelector('.incl-range'),
                        updateView = function () {
                            if (this.getAttribute('name') === 'rangeOne') {
                                outputOne.innerHTML = this.value;
                                outputOne.style.left = this.value / this.getAttribute('max') * 100 + '%';
                            } else {
                                outputTwo.style.left = this.value / this.getAttribute('max') * 100 + '%';
                                outputTwo.innerHTML = this.value
                            }
                            if (parseInt(rangeOne.value) > parseInt(rangeTwo.value)) {
                                inclRange.style.width = (rangeOne.value - rangeTwo.value) / this.getAttribute('max') * 100 + '%';
                                inclRange.style.left = rangeTwo.value / this.getAttribute('max') * 100 + '%';
                            } else {
                                inclRange.style.width = (rangeTwo.value - rangeOne.value) / this.getAttribute('max') * 100 + '%';
                                inclRange.style.left = rangeOne.value / this.getAttribute('max') * 100 + '%';
                            }
                        };

                document.addEventListener('DOMContentLoaded', function () {
                    updateView.call(rangeOne);
                    updateView.call(rangeTwo);
                    $('input[type="range"]').on('mouseup', function () {
                        this.blur();
                    }).on('mousedown input', function () {
                        updateView.call(this);
                    });
                });

            </script>
        <?php } ?>
        <script type="text/javascript">
            $(document).on('click', '.wish', function () {
                var element = $(this);
                var prod_id = element.attr("data-id");

                $.ajax({
                    url: '<?= base_url('add-product-wishlist') ?>',
                    type: 'POST',
                    data: {prod_id: prod_id, <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                })
                        .done(function (result) {
                            if (result == 1) {
                                //save succesfully
                                element.addClass('active');
                                $("#wishlist-text").text('Item added to your wishlist');
                                $('#myAnchor').trigger('click');
                            } else if (result == 2) {
                                //already exists
                                $("#wishlist-text").text('Item already exits in your wishlist');
                                $('#myAnchor').trigger('click');
                            } else {
                                error = 1;
                            }
                            console.log("success");
                        })
                        .fail(function () {
                            console.log("error");
                        })
                        .always(function () {
                            console.log("complete");
                        });
            });
        </script>
<script>
    function comp_prod(prod_id, this_ele){
        var element = $(this_ele);
//        var bt_check = $('#comp-'+prod_id).is(':checked');
        //alert(prod_id+' '+bt_check);
        
        var checked_element = element.attr("id");
        var other_element;

        if ( checked_element === "comp-"+prod_id ) {
            other_element = "comp2-"+prod_id;
        }
        
        if ( checked_element === "comp2-"+prod_id ) {
            other_element = "comp-"+prod_id;
        }

        if ( $("#"+checked_element).is(':checked') ) {
            $("#"+other_element).prop("checked", true);
        } else {
            $("#"+other_element).prop("checked", false);
        }
        
        var url = "<?= base_url() ?>compare-prods-2";
        $.ajax({
            url: url,
            type: "post",
            data: {prod_id: prod_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function (data) {
                data = JSON.parse(data);
                
                    if(data.status == 201){
                        $('#comp_msg').html('Compare list is full.');
                        $('.comp-alert').removeClass('hide');
                        $('html, body').animate({
                            scrollTop: $(".shop-right").offset().top
                        }, 2000);
                    }else if(data.status == 200 || data.status == 900){
                        if(data.html != ''){
                            $('#compare_listing').html(data.html);
                            $('.compare-bg').removeClass('hide');
                            $('.shop-container').addClass('compare-set');
                            if(data.status == 900){
                                $('#comp-'+prod_id).removeAttr("checked");
                                $('#comp2-'+prod_id).removeAttr("checked");
                                $('.prod-td-'+prod_id).remove();
                            }
                            if(data.total_prods < 2){
                                $('#comp_btn').hide();
                            }else if(data.total_prods >= 2){
                                $('#comp_btn').removeClass('hide');
                                $('#comp_btn').attr('style','display:block;');
                                $('.com-btn').show();
                            }
                        }else{
                            $('.comp-checks').removeAttr("checked");
                            $('.compare-bg').addClass('hide');
                            $('.shop-container').removeClass('compare-set');
                        }
                    }
                //}
            }
        });
    }

    function clearcompare(){
        var url = "<?= base_url() ?>clear-compare-list";
        $.ajax({
            url: url,
            type: "post",
            data: {<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function (data) {
                data = JSON.parse(data);
                if(data.status == 200 ){
                    $('.close').click();
                    $('.comp-checks').removeAttr("checked");
                    $('.compare-bg').addClass('hide');
                    $('.shop-container').removeClass('compare-set');
                }
            }
        });
        //alert('Compare List is now Clear!');
    }

    function removeslctitem(prod_id){
        $.ajax({
            type:'POST',
            url:'<?=base_url()."remove-compare-prod"?>',
            data:{prod_id:prod_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function(data){
                data = JSON.parse(data);
                if(data.status == 200 ){
                    if(data.total_prods > 0){
                        console.log(data.msg);
                        $('#comp-'+prod_id).removeAttr("checked");
                        $('#comp2-'+prod_id).removeAttr("checked");
                        $('.prod-td-'+prod_id).remove();
                        console.log(data.compare_prods);
                        if(data.total_prods < 2){
                            $('#comp_btn').hide();
                        }else if(data.total_prods >= 2){
                                $('#comp_btn').removeClass('hide');
                                $('#comp_btn').attr('style','display:block;');
                                $('.com-btn').show();
                            }
                    }else{
                        $('.comp-checks').removeAttr("checked");
                        $('.compare-bg').addClass('hide');
                        $('.shop-container').removeClass('compare-set');
                    }
                }
            },
            error: function(){
                console.log('Network error.');
            }
        });
    }
    
    function toggleClass(this_element) {
    
        if (typeof(Storage) !== "undefined") {
            sessionStorage.setItem("shopListingTab", jQuery(this_element).attr("href"));
        } else {
            // Sorry! No Web Storage support..
        }
    
        jQuery(this_element).parent('li').siblings('li').children('a').removeClass('active');
        jQuery(this_element).addClass("active");
        
    }
    
</script>
