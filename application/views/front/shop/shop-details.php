<!--Breadcrumbs starts here-->
<?php //echo "<pre>";print_r($this->session->userdata());?>
<link href="<?= base_url() ?>front_resources/css/pgwslideshow.css" rel="stylesheet" type="text/css" />
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item">Shop</li>
                        <li class="breadcrumb-item"><a href="<?= base_url() ?><?= $breadcrumbs_data->catURL ?>"><?= $breadcrumbs_data->catName ?></a></li>
                        <?php if ($segment2 != '' && $breadcrumbs_data->subCatURL == '') { ?>
                            <li class="breadcrumb-item active"><?= $breadcrumbs_data->prodTitle ?></li>
                        <?php } else { ?>
                            <li class="breadcrumb-item active"><a href="<?= base_url() ?><?= $breadcrumbs_data->catURL ?>/<?= $breadcrumbs_data->subCatURL ?>"><?= $breadcrumbs_data->subCatName ?></a></li>
                        <?php } if ($segment3 != '' && $breadcrumbs_data->subSubCatURL == '') { ?>
                            <li class="breadcrumb-item active"><?= $breadcrumbs_data->prodTitle ?></li>
                        <?php } elseif ($breadcrumbs_data->subCatURL != '') { ?>
                            <li class="breadcrumb-item active"><a href="<?= base_url() ?><?= $breadcrumbs_data->catURL ?>/<?= $breadcrumbs_data->subCatURL ?>/<?= $breadcrumbs_data->subSubCatURL ?>"><?= $breadcrumbs_data->subSubCatName ?></a></li>
                        <?php } if ($segment4 != '' && $breadcrumbs_data->subSubCatURL != '') { ?>
                            <li class="breadcrumb-item active"><?= $breadcrumbs_data->prodTitle ?></li>
                        <?php } ?>
                    </ol>
                </div>	
                <div class="shop-tit mrg-top-0 pad-10">
                    <h2>Shop</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->
<?php if ($prod_details['seller_data']->store_discount > 0 && $prod_details['seller_data']->store_discount_item > 0) { ?>
    <section class="pls-itm2">
        <div class="container">
            <div class="row">
                <div class="auto-container">
                    <div class="pls-itm-sec">
                        <h4>
                            <?= $prod_details['seller_data']->store_discount ?>% off when you buy <?= $prod_details['seller_data']->store_discount_item ?>+ Items
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>

<div class="container" id="add_to_cart_msg" style="display: none;">
    <div class="row">
        <div id="alert_msg" class="alert fade in alert-dismissable" style="clear: both; text-align: center; font-size: 15px;">
            Product <strong>added</strong> to cart.
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">&times;</a>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="comp-alert alert alert-danger alert-dismissable fade in hide" style="clear: both; text-align: center; font-size: 15px;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <span id="comp_msg"></span>
        </div>
    </div>
</div>
<div class="container" id="report_poduct_msg" style="display: none;">
    <div class="row">
        <div id="report_alert_msg" class="alert fade in alert-dismissable" style="clear: both; text-align: center; font-size: 15px;">
            Products reported successfully.
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">&times;</a>
        </div>
    </div>
</div>

<section class="shop-details-main">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="shop-detail-sec">
                    <div class="col-md-6 col-sm-6 col-xs-12 pd-lsec">
                        <div class="cro-slid">
                        	<div class="cro-slid-cvr">
                            <ul class="pgwSlideshow">
                                <?php foreach ($prod_details['product_images'] as $image) { ?>
                                    <li><img src="<?= base_url() ?>resources/prod_images/<?= $image['img_name'] ?>"/></li>
                                <?php } ?>
                            </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="shop-det-rgt">
                            <?php ?>
                            <div class="shop-det-rgt-upr">
                                <h4><?= $prod_details['product_data']->prod_title ?></h4>
                                <p><a href="<?= base_url() ?>store/<?= $prod_details['seller_data']->store_url ?>"><?php echo $prod_details['seller_data']->store_name; ?></a><span> (Visit their Shop)</span></p>
                            </div>

                            <?php if ($prod_details['required_filters'] != false && $prod_details['uniqueFilter'] == 0) { ?>
                            <!-- New Section Starts -->
                            <div class="pf-wrap">
                                <?php foreach ($prod_details['required_filters'] as $filters) {
                                    if($filters['cat_filter_is_conditional']!=1){ ?>
                                    <div class="row pf-row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <label><?= stripcslashes($filters['filter_title']) ?>:</label>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <select class="required-filters" data-name="<?= $filters['filter_title']?>" name="<?= $filters['filter_slug'] ?>" id="<?= $filters['filter_slug'] ?>" onchange="filtervalue(<?= $filters['prod_id'] ?>,'<?= $filters['filter_slug'] ?>')">
                                                <!-- <option value="">Choose <?= $filters['filter_title'] ?></option> -->
                                                <?php
                                                $options = explode(",", ltrim(rtrim($filters['cat_filter_values'], ','), ','));
//                                                foreach ($prod_details['filters_array'] as $op) {
                                                foreach ($prod_details['filters_array'] as $op) {
                                                    if($op['filter_id'] == $filters['filter_id']){
                                                    ?>
                                                    <option value="<?=$op['filter_value']?>"><?=stripcslashes($op['filter_value'])?></option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select>
                                            <input type="hidden" name="<?= $filters['prod_id'] ?>_<?= $filters['filter_slug'] ?>" data-name="<?=$filters['filter_title'] ?>" data-slug="<?= $filters['filter_slug'] ?>" class="required_filters" id="<?= $filters['prod_id'] ?>_<?= $filters['filter_slug'] ?>">
                                        </div>
                                    </div>
                                    <?php } else{ ?>
                                    <div class="row pf-row" id="<?=$filters['filter_slug']?>-html">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <label><?= stripcslashes($filters['filter_title']) ?>:</label>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <select class="required-filters" data-name="<?= $filters['filter_title']?>" name="<?= $filters['filter_slug'] ?>" id="<?= $filters['filter_slug'] ?>" onchange="filtervalue(<?= $filters['prod_id'] ?>,'<?= $filters['filter_slug'] ?>');load_con_filter($(this));">
                                                <?php
                                                $data['values'] = $this->db->query("SELECT f.filter_slug, fd.filter_title ,fd.filter_detail_id 
                                                                                    FROM tbl_product_filters f 
                                                                                    INNER JOIN tbl_cat_filter_detail fd ON f.filter_id = fd.filter_detail_title_id 
                                                                                    WHERE f.prod_id = ".$filters['prod_id']." AND f.filter_slug = '".$filters['filter_slug']."' 
                                                                                    GROUP BY fd.filter_detail_id 
                                                                                    ")->result_array();
                                                                                    ?>
                                                <?php foreach ($data['values'] as $op) { ?>
                                                   <option value="<?=$op['filter_title']?>" data-prod="<?=$filters['prod_id']?>" data-id="<?=$op['filter_detail_id']?>"><?=stripcslashes($op['filter_title'])?></option>
                                                <?php } ?>
                                            </select>
                                            <input type="hidden" name="<?= $filters['prod_id'] ?>_<?= $filters['filter_slug'] ?>" data-name="<?=$filters['filter_title'] ?>" data-slug="<?= $filters['filter_slug'] ?>" class="required_filters" id="<?= $filters['prod_id'] ?>_<?= $filters['filter_slug'] ?>">
                                        </div>
                                    </div>
                                <?php }
                                }?>
                            </div>
                            <!-- New Section Ends -->
                            <?php } ?>

                            <div class="shop-det-rgt-lowr-1">

                                <h5>
                                    <?php
                                    $productPrice = 0.0;
                                    $sDate = date('Y-m-d', strtotime($prod_details['product_data']->sale_start_date));
                                    $eDate = date('Y-m-d', strtotime($prod_details['product_data']->sale_end_date));
                                    $tDate = date('Y-m-d');
                                    if ($prod_details['product_data']->prod_onsale == 1) {
                                        if ($prod_details['product_data']->sale_start_date != '' && $prod_details['product_data']->sale_end_date != '') {
                                            if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                                                $productPrice = number_format($prod_details['product_data']->prod_price, 2, '.', '');
                                                echo '<span class="sp1">$' . number_format($prod_details['product_data']->sale_price, 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod_details['product_data']->prod_price, 2, '.', '') . '</span>';
                                            } else {
                                                $productPrice = number_format($prod_details['product_data']->prod_price, 2, '.', '');
                                                echo '$' . number_format($prod_details['product_data']->prod_price, 2, '.', '') . ''; //</span>
                                            }
                                        } else if ($prod_details['product_data']->sale_start_date != '') {
                                            if ($tDate >= $sDate) {
                                                $productPrice = number_format($prod_details['product_data']->prod_price, 2, '.', '');
                                                echo '<span class="sp1">$' . number_format($prod_details['product_data']->sale_price, 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod_details['product_data']->prod_price, 2, '.', '') . '</span>';
                                            } else {
                                                $productPrice = number_format($prod_details['product_data']->prod_price, 2, '.', '');
                                                echo '$' . number_format($prod_details['product_data']->prod_price, 2, '.', '') . ''; //</span>
                                            }
                                        } else if ($prod_details['product_data']->sale_end_date != '') {
                                            if ($tDate <= $eDate) {
                                                echo 'Price:<span class="sp1">$' . number_format($prod_details['product_data']->sale_price, 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod_details['product_data']->prod_price, 2, '.', '') . '</span>';
                                            } else {
                                                echo 'Price:<span class="sp1">$' . number_format($prod_details['product_data']->prod_price, 2, '.', '') . '</span>';
                                            }
                                        } else {
                                            $productPrice = number_format($prod_details['product_data']->prod_price, 2, '.', '');
                                            echo '<span class="sp1">$' . number_format($prod_details['product_data']->sale_price, 2, '.', '') . '</span> <span class="sp2">$' . number_format($prod_details['product_data']->prod_price, 2, '.', '') . '</span>';
                                        }
                                    } else {
                                        $productPrice = number_format($prod_details['product_data']->prod_price, 2, '.', '');
                                        echo '$' . number_format($prod_details['product_data']->prod_price, 2, '.', '');
                                    }
                                    ?>
                                </h5>
                                <p style="margin-top:-6px;">Available Quantity: <?=$prod_details['product_data']->qty?></p>

                                <p>
                                    <?php if ($prod_details['product_data']->shipping == "Charge for Shipping") { ?>
                                        + <?php echo "$" . number_format($prod_details['product_data']->ship_price, 2); ?> Shipping <?=(!empty($prod_details['product_data']->prod_shipping_methods))?'('.$prod_details['product_data']->prod_shipping_methods.')':''?><!-- <span>or</span> --> 
                                    <?php } else { ?>
                                        + <?= $prod_details['product_data']->shipping ?>
                                    <?php } ?>
                                </p>
                                <p>
                                    <?php if ($prod_details['product_data']->looking_for != "Sell") { ?>
                                        + <?php echo $prod_details['product_data']->looking_for; ?> 
                                    <?php }?>
                                </p>
                            </div>
                            <?php if ($prod_details['product_data']->qty > 0) { ?>
                                <div class="shop-det-rgt-lowr-2">
                                    <a href="javascript:void(0);" data-price = "<?= $productPrice ?>" class="adcrt" data-product="<?= $prod_details['product_data']->prod_id ?>">Add to cart</a>
                                    <!-- <a href="javascript:void(0);" class="paypal">Buy now with</a> -->
                                    <!-- <a href="javascript:;" title="Add to Wishlist"></a>
                                    <?php if($this->session->userdata('logged_in')){?>
                                        <a href="javascript:void(0);" class="wish adtowishlist" data-id="<?=$prod_details['product_data']->prod_id?>" title="Add to Wishlist">Add to Wishlist</a>
                                    <?php }?> -->
                                </div>
                            <?php } else { ?>
                                <div class="shop-det-rgt-lowr-2">
                                    <div class="out-of-stock">Out of stock.</div>
                                </div>
                            <?php } ?>
                            <div class="shop-det-rgt-lowr-3">
                                <span class="reprt-btn"><a href="mailto:info@educki.com?Subject=Report" onclick="report(<?= $prod_details['product_data']->prod_id ?>)">Report</a></span>
                                <button class="addcompr-btn aacomp" onclick="compare_prod(<?= $prod_details['product_data']->prod_id ?>)" title="Add to Compare"></button> <!-- Add to Compare -->
                                <a id="Modalcompare" style="display: none;" data-toggle="modal" data-target="#myModalcompare"></a>
                                <?php if ($prod_details['product_data']->qty > 0) { ?>
                                    <?php if($this->session->userdata('logged_in')){?>
                                        <a href="javascript:void(0);" class="wish wish-btnn <?php if (check_wishlist_item($prod_details['product_data']->prod_id)) {echo 'active';}?>" " data-id="<?=$prod_details['product_data']->prod_id?>" title="Add to Wishlist"></a> <!-- Add to Wishlist -->
                                    <?php }?>
                                <?php } ?>
                                <?php if(!empty($prod_details['product_data']->youtube_links)){?>
                                    <a href="<?php echo $prod_details['product_data']->youtube_links; ?>" target="_blank" class="watchvideo">Watch Video</a>
                                <?php }?>
                            </div>
                            <div class="shop-det-rgt-lowr-4">
                                <div class="shr-img">
                                    <ul>
                                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?= $share_url ?>" class="shr-fb" target="_blank"></a></li>
                                        <li><a href="https://twitter.com/home?status=<?= $prod_details['product_data']->prod_title ?>%20%20<?= $share_url ?>" class="shr-fb2" target="_blank"></a></li>
                                        <li><a href="https://pinterest.com/pin/create/button/?url=<?= $share_url ?>&media=<?= $share_image ?>&description=<?= $prod_details['product_data']->prod_description ?>" class="shr-fb3" target="_blank"></a></li>
                                        <li><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= $share_url ?>&title=<?= $prod_details['product_data']->prod_title ?>&summary=<?= $prod_details['product_data']->prod_description ?>" class="shr-fb4" target="_blank"></a></li>
                                        <li><a href="mailto:?Subject=<?= $prod_details['product_data']->prod_title ?>&body=<?= $share_url ?>" class="shr-fb5"></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="shop-det-rgt-lowr-5">
                                <?php if ($prod_details['product_data']->shipping == "Charge for Shipping") { ?>
                                    <h6>Product Ships within <?= $prod_details['product_data']->ship_days ?> business days</h6>
                                <?php } else if ($prod_details['product_data']->shipping == "Offer free Shipping") { ?>
                                    <h6>Product Ships within <?= $prod_details['product_data']->free_ship_days ?> business days</h6>
                                <?php } ?>
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                <p><?= $prod_details['product_data']->prod_description ?> </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="shop-details-tbsec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="shop-detail-tab-lowr">
                    <div id="exTab2">			
                        <ul class="nav nav-tabs">
                            <li><a href="#4" data-toggle="tab">Additional Information</a></li>
                            <li class="active"><a  href="#1" data-toggle="tab">More Products</a></li>
                            <li><a href="#2" data-toggle="tab">Comments</a></li>
                            <?php if ($this->session->userdata('logged_in')) { ?><li><a href="#3" data-toggle="tab">Ask a Question</a></li><?php } ?>
                            
                        </ul>
                        <div class="tab-content ">
                            
                            <!-- Addition Information Section Starts -->
                            <div class="tab-pane" id="4">
                                <div class="mre-prod addition-wrap">
                                    <div class="row addition-row">
                                        <div class="col-md-2 col-sm-4 col-xs-12 ad-lbl text-uppercase">
                                            Condition
                                        </div>
                                        <div class="col-md-10 col-sm-8 col-xs-12 ad-dtl">
                                            <?= $prod_details['product_data']->condition ?>
                                        </div>
                                    </div>
                                    <div class="row addition-row">
                                        <div class="col-md-2 col-sm-4 col-xs-12 ad-lbl text-uppercase">
                                            Return Policy
                                        </div>
                                        <div class="col-md-10 col-sm-8 col-xs-12 ad-dtl">
                                            <?= (($prod_details['product_data']->prod_return=='Returns Not Accepts')?'Returns Not Accepted':$prod_details['product_data']->prod_return) ?>
                                        </div>
                                    </div>
                                    <div class="row addition-row">
                                        <div class="col-md-2 col-sm-4 col-xs-12 ad-lbl text-uppercase">
                                            Available Quantity 
                                        </div>
                                        <div class="col-md-10 col-sm-8 col-xs-12 ad-dtl">
                                            <?= $prod_details['product_data']->qty ?>
                                        </div>
                                    </div>
                                    <div class="row addition-row">
                                        <div class="col-md-2 col-sm-4 col-xs-12 ad-lbl text-uppercase">
                                            Shipping 
                                        </div>
                                        <div class="col-md-10 col-sm-8 col-xs-12 ad-dtl">
                                            <?= $prod_details['product_data']->shipping ?>
                                        </div>
                                    </div>
                                    <div class="row addition-row">
                                        <div class="col-md-2 col-sm-4 col-xs-12 ad-lbl text-uppercase">
                                            Shipping Price
                                        </div>
                                        <?php $ship_price = "";
                                        if($prod_details['product_data']->shipping=="Charge for Shipping"){
                                            $ship_price = "$".number_format($prod_details['product_data']->ship_price, 2);
                                        }else if($prod_details['product_data']->shipping=="Offer free Shipping"){
                                            $ship_price = "Free";
                                        }else{
                                            $ship_price = "$0.00";
                                        }
                                        ?>
                                        <div class="col-md-10 col-sm-8 col-xs-12 ad-dtl">
                                            <?php echo $ship_price?>
                                        </div>
                                    </div>
                                    <?php if($prod_details['product_data']->shipping=="Charge for Shipping"){?>
                                    <div class="row addition-row">
                                        <div class="col-md-2 col-sm-4 col-xs-12 ad-lbl text-uppercase">
                                            Shipping Method
                                        </div>
                                        <div class="col-md-10 col-sm-8 col-xs-12 ad-dtl">
                                            <?php echo $prod_details['product_data']->prod_shipping_methods; ?>
                                        </div>
                                    </div>
                                    <?php }?>

                                    <?php
                                    $ops = '';
                                     if ($prod_details['required_filters'] != false) { ?>
                                        <?php foreach ($prod_details['required_filters'] as $filters) { 
                                            $ops = '';
                                            if($filters["cat_filter_values"]!=''){ ?>
                                            <div class="row addition-row">
                                                <div class="col-md-2 col-sm-4 col-xs-12 ad-lbl text-uppercase">
                                                    <?=$filters['filter_title']?>
                                                </div>
                                                <div class="col-md-10 col-sm-8 col-xs-12 ad-dtl">
                                                    <?php
                                                    foreach ($prod_details['filters_array'] as $op) {
                                                        if($op['filter_id'] == $filters['filter_id']){
                                                    ?>
                                                    <?php  $ops .= $op["filter_value"].', '; ?>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                    <?php if($ops != ''){ 
                                                        echo rtrim( $ops, ', ');
                                                    } ?>
                                                </div>
                                            </div>
                                    <?php }else{ 
                                        $data['value'] = $this->db->query("SELECT filter_value FROM tbl_product_filters_detail WHERE filter_id = ".$filters['filter_id']."")->result_array();
                                    ?>
                                        <div class="row addition-row">
                                            <div class="col-md-2 col-sm-4 col-xs-12 ad-lbl text-uppercase">
                                                <?=$filters['filter_title']?>
                                            </div>
                                            <div class="col-md-10 col-sm-8 col-xs-12 ad-dtl">
                                                <?php
                                                $ops = ''; 
                                                foreach ($data['value'] as $op) { 
                                                $ops .= $op["filter_value"].', ';
                                                }
                                                ?>
                                                <?php if($ops != ''){ 
                                                    echo rtrim( $ops, ', ');
                                                } ?>
                                            </div>
                                        </div>
                                <?php 
                                        }
                                    } 
                                }
                                ?>
                                    <?php
                                    $ops = '';
                                     if ($prod_details['required_filters2'] != false) { ?>
                                        <?php foreach ($prod_details['required_filters2'] as $filters) { 
                                            if($filters['cat_filter_values']!=''){
                                            $ops = '';
                                        ?>
                                            <div class="row addition-row">
                                                <div class="col-md-2 col-sm-4 col-xs-12 ad-lbl text-uppercase">
                                                    <?=$filters['filter_title']?>
                                                </div>
                                                <div class="col-md-10 col-sm-8 col-xs-12 ad-dtl">
                                                    <?php
                                                    // echo '<pre>';
                                                    // print_r($prod_details['filters_array2']);
                                                    // echo '</pre>';
                                                    foreach ($prod_details['filters_array2'] as $op) {
                                                        if($op['filter_id'] == $filters['filter_id']){
                                                    ?>
                                                        <?php  $ops .= $op["filter_value"].', '; ?>
                                                    <?php }
                                                    } ?>
                                                    <?php if($ops != ''){  echo rtrim( $ops, ', '); } ?>
                                                </div>
                                            </div>
                                    <?php }else{
                                        $data['value'] = $this->db->query("SELECT filter_value FROM tbl_product_filters_detail WHERE filter_id = ".$filters['filter_id']."")->result_array();?>

                                        <div class="row addition-row">
                                            <div class="col-md-2 col-sm-4 col-xs-12 ad-lbl text-uppercase">
                                                <?=$filters['filter_title']?>
                                            </div>
                                            <div class="col-md-10 col-sm-8 col-xs-12 ad-dtl">
                                                <?php
                                                $ops = ''; 
                                                foreach ($data['value'] as $op) { 
                                                    $ops .= $op["filter_value"].', ';
                                                }
                                                ?>
                                                <?php if($ops != ''){ 
                                                    echo rtrim( $ops, ', ');
                                                } ?>
                                            </div>
                                        </div>
                                      <?php }
                                       }
                                    }
                                    ?>
                                </div>
                            </div>
                            <!-- Addition Information Section Ends -->
                            <div class="tab-pane active" id="1">
                                <div class="mre-prod">
                                    <div class="mre-prod-tit">
                                        
                                        <a href="<?= base_url() ?>store/<?= $prod_details['seller_data']->store_url ?>"><img src="<?=base_url('resources/seller_account_image/logo/thumb/'.$prod_details['seller_data']->store_logo_image)?>" /></a>
                                        <h6>Seller: <a href="<?= base_url() ?>store/<?= $prod_details['seller_data']->store_url ?>"><?php echo $prod_details['seller_data']->store_name; ?></a></h6>
                                        <p>
                                            <?= $prod_details['seller_data']->store_city ?>, <?= $prod_details['seller_data']->store_state ?>
                                        </p>
                                    </div>
                                    <div class="mre-prod-map" id="googleMap" style="width:100%; height:300px;"></div>
                                    <div class="mre-prod-botm">
                                        <?php if ($prod_details['other_products'] != false) { ?>
                                            <p>Other Products from this seller</p>
                                            <?php foreach ($prod_details['other_products'] as $image) { ?>
                                                <div class="mre-prod-botm-bx">
                                                    <?php if ($image['subSubCatURL'] != '') { ?>
                                                        <a href="<?= base_url() ?><?= $image['catURL'] ?>/<?= $image['subCatURL'] ?>/<?= $image['subSubCatURL'] ?>/<?= $image['prod_url'] ?>">
                                                            <img src="<?= base_url() ?>resources/prod_images/thumb/<?= $image['img_name'] ?>" alt="" title="" class="img-responsive"/>
                                                        </a>
                                                    <?php } ?>
                                                    <?php if ($image['subCatURL'] != '' && $image['subSubCatURL'] == '') { ?>
                                                        <a href="<?= base_url() ?><?= $image['catURL'] ?>/<?= $image['subCatURL'] ?>/<?= $image['prod_url'] ?>">
                                                            <img src="<?= base_url() ?>resources/prod_images/thumb/<?= $image['img_name'] ?>" alt="" title="" class="img-responsive"/>
                                                        </a>
                                                    <?php } ?>
                                                    <?php if ($image['subCatURL'] == '' && $image['subSubCatURL'] == '') { ?>
                                                        <a href="<?= base_url() ?><?= $image['catURL'] ?>/<?= $image['prod_url'] ?>">
                                                            <img src="<?= base_url() ?>resources/prod_images/thumb/<?= $image['img_name'] ?>" alt="" title="" class="img-responsive"/>
                                                        </a>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="2">
                                <div class="detail-comm">
                                    <div id="disqus_thread"></div>
                                </div>
                            </div>
                            <?php if ($this->session->userdata('logged_in')) { ?>
                                <div class="tab-pane" id="3">
                                    <div class="ualert-row">
                                        <div id="new_message" class=" message hide ">
                                            <a href="javascript:;" class="close" data-dismiss="alert" aria-label="close" title="close">Ã—</a>
                                            No Content Avaliable
                                        </div>
                                    </div>
                                    <div class="detail-ques">
                                        <h6>Ask a Question</h6>
                                        <textarea id="message" name="message"></textarea>
                                        <div class="error askq_error"></div>
                                        <input type="hidden" name="msg_sender" id="msg_sender" value="<?=$this->session->userdata('userid')?>">
                                        <input type="hidden" name="msg_receiver" id="msg_receiver" value="<?=$prod_details['seller_data']->user_id?>">
                                        <input type="hidden" name="msg_email" id="msg_email" value="<?=$prod_details['seller_data']->user_email?>">
                                        <input type="hidden" name="msg_prod" id="msg_prod" value="<?=$prod_details['product_data']->prod_id?>">
                                        <input type="submit" id="ask_question"/>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div id="myModalcompare" class="modal fade" role="dialog">
    <div class="modal-dialog compare-popup">
        <?php $compare_listing = getCompareList2(); ?>
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"></button>
                <h4>The product <a href="<?= productDetailUrl($prod_details['product_data']->prod_id) ?>/<?= $prod_details['product_data']->prod_url ?>"><?= $prod_details['product_data']->prod_title ?></a> has been added to comparison list.</h4>
            </div>
            <div class="modal-body">
                <div class="compre-pop-rw" id="compare_listing">

                </div>
            </div>
            <div class="modal-footer">
                <a id="comp_btn" href="<?= base_url() . 'compare-details' ?>" class="com-btn2">Compare</a>
                <a href="javascript:;" onclick="clearcompare()" data-dismiss="modal" class="clear-btn2">Clear List</a>
            </div>
        </div>
        <input type="hidden" value="<?= $this->session->userdata('userid') ?>" id="user_id">
    </div>
</div>



<!-- Popup Starts Here -->
<a data-toggle="modal" data-target="#myModal4" id="myAnchor" style="display: none;">Click HERE</a>
<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog popup-4" role="document">

        <div class="modal-content">
            <div class="modal-header pad-0">        
                <button type="button" style="z-index: 99;" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body pad-0">
                <div class="frgt-sec oneline">
                    <div class="fgt-tit">
                        <p class="mrg-botm-0" id="wishlist-text"></p>
                        
                        <!-- Button Settings Starts -->
                        
                        <!-- Button Settings Ends -->
                        
                    </div>
                </div>
            </div>

        </div>

        <!-- Ended 2 divs below -->
    </div>
</div>
<!-- Popup Ends Here -->
<script src="<?= base_url() ?>front_resources/js/pgwslideshow.js" type="text/javascript"></script>
<script>
                    $(document).ready(function () {
                        $('.pgwSlideshow').pgwSlideshow(4);
                    });
                    $(document).ready(function () {
                        $("#ask_question").click(function (e) {
                            e.preventDefault();
                            var value = $('textarea#message').val();
                            if(value.length > 0)
                            {
                                var seller_id = $('#msg_receiver').val();
                                var prod_id = $('#msg_prod').val();
                                var message = $('#message').val();
                                var email = $('#msg_email').val();
                                var sender = $('#msg_sender').val();

                                $.ajax({
                                    type: 'POST',
                                    url: '<?= base_url() ?>submit-message',
                                    data: {email: email, seller_id: seller_id, prod_id: prod_id, message: message,sender:sender,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                                    success: function (res) {
                                        if (res == '200') {
                                            $('textarea#message').val('');
                                            setTimeout(function () {
                                                $('#new_message').html('');
                                                $('#new_message').removeClass("alert alert-success");
                                                $('#new_message').addClass("hide");
                                            }, 5000);
                                            $('#new_message').html('Message sent Successfully');
                                            $('#new_message').removeClass("hide");
                                            $('#new_message').addClass("alert alert-success");
                                        }
                                    },
                                    error: function () {
                                        console.log('Network error.');
                                    }
                                });
                                /*$('.askq_error').text('');
                                var url = "<?= base_url() ?>question";
                                var receiver_email = "<?= $prod_details['seller_data']->user_email ?>";
                                var prod_id = '<?=$prod_details['product_data']->prod_id?>';
                                var sender_info = $('#user_id').val();
                                $.ajax({
                                    url: url,
                                    type: "post",
                                    data: {question: value,prod_id:prod_id, receiver: receiver_email, sender: sender_info,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                                    success: function (data) {
                                        $('textarea#message').val('');
                                        setTimeout(function () {
                                            $('#new_message').html('');
                                            $('#new_message').removeClass("alert alert-success");
                                            $('#new_message').addClass("hide");
                                        }, 5000);
                                        $('#new_message').html('Message sent Successfully');
                                        $('#new_message').removeClass("hide");
                                        $('#new_message').addClass("alert alert-success");
                                    }
                                });*/
                            }else{
                                $('.askq_error').text('This field is required.');
                            }
                        });
                    });
</script>
<script>
    function removeslctitem(prod_id) {
        $.ajax({
            type: 'POST',
            url: '<?= base_url() . "remove-compare-prod" ?>',
            data: {prod_id: prod_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function (data) {
                data = JSON.parse(data);
                if (data.status == 200) {
                    if (data.total_prods > 0) {
                        console.log(data.msg);
                        $('.prod-td-' + prod_id).remove();
                        console.log(data.compare_prods);
                        if (data.total_prods < 2) {
                            $('#comp_btn').hide();
                        } else if (data.total_prods >= 2) {

//                                $('#comp_btn').attr('style','display:block;');
                            $('.com-btn2').show();
                        }
                    } else {
                        $('.close').click();
                    }
                }
            },
            error: function () {
                console.log('Network error.');
            }
        });
    }

    function compare_prod(prod_id) {
        var url = "<?= base_url() ?>compare-prods";
        $.ajax({
            url: url,
            type: "post",
            data: {prodId: prod_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function (data) {
                data = JSON.parse(data);
                if (data.status == 201) {
                    $('#comp_msg').html('Compare list is full.');
                    $('.comp-alert').removeClass('hide');
                    $('html, body').animate({
                        scrollTop: $(".comp-alert").offset().top
                    }, 2000);
                }
                else if (data.status == 200 || data.status == 900) {
                    $('#compare_listing').html(data.html);
                    $('#Modalcompare').click();
                    if (data.total_prods < 2) {
                        $('#comp_btn').hide();
                    } else if (data.total_prods >= 2) {

//                                $('#comp_btn').attr('style','display:block;');
                        $('.com-btn2').show();
                    }
                }
            }
        });
    }

    function clearcompare() {
        var url = "<?= base_url() ?>clear-compare-list";
        $.ajax({
            url: url,
            type: "post",
            data: {<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function (data) {
                data = JSON.parse(data);
                if (data.status == 200) {
                    $('.close').click();
//                    $('.comp-checks').removeAttr("checked");
                    $('.compare-popup').addClass('hide');

                }
            }
        });
        //alert('Compare List is now Clear!');
    }
    function report(prod_id) {
        var url = "<?= base_url() ?>productReport";
        $.ajax({
            url: url,
            type: "post",
            data: {prodId: prod_id,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
            success: function (data) {
                $("#wishlist-text").text('Product reported successfully.');
                $('#myAnchor').trigger('click');
            }
        });
    }
    
    function filtervalue(prod_id,filter_slug){
        var prod_id = prod_id;
        var filter_slug = filter_slug;
        var select_value = $("#"+filter_slug).val();
        $("#"+prod_id+"_"+filter_slug).val(select_value);
    }

    function load_con_filter(s){
        filter_id = s.find(':selected').attr('data-id');
        prod_id = s.find(':selected').attr('data-prod');
        title = s.find(':selected').val();
        id = s.attr('id');
        $('#'+id+'-filters').remove();

        if(title!=''){
            $.ajax({
                type: 'POST',
                url: '<?= base_url()."load-filters"?>',
                dataType: 'json',
                async:false,
                data: {id:id,prod_id: prod_id,filter_id:filter_id,title:title,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                success: function (res) {
                    if(res['status']==200){
                        $('#'+id+"-html").after(res['html']);
                    }
                },
                error: function () {
                    console.log('Network error.');
                }
            });
        }
    }

    function myMap() {
        var geocoder = new google.maps.Geocoder();
        var address = "<?= $prod_details['seller_data']->store_zip ?>";

        geocoder.geocode({'address': address}, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();

                var myCenter = new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude));
                var mapCanvas = document.getElementById("googleMap");
                var mapOptions = {center: myCenter, zoom: 5, zoomControl: true, scaleControl: true};
                var map = new google.maps.Map(mapCanvas, mapOptions);
                var marker = new google.maps.Marker({position: myCenter});
                marker.setMap(map);
            }
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAscDVsx1aPq_cm_9DaZAmj6zOIPjhLDpo&callback=myMap"></script>
<script id="dsq-count-scr" src="//www-educki-com.disqus.com/count.js" async></script>
<script>

    /**
     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
    /*
     var disqus_config = function () {
     this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
     this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
     };
     */
    (function () { // DON'T EDIT BELOW THIS LINE
        var d = document, s = d.createElement('script');
        s.src = 'https://www-educki-com.disqus.com/embed.js';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<!-- 
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
-->
<script>

    $(document).ready(function () {
        $('.required-filters').change();
        
        $('.adcrt').click(function (e) {
            $('.sub-filters').change();
            e.preventDefault();
            var v = [];
            $prod_id = $(this).attr('data-product');
            var filters = $(".required_filters");
            for(var i = 0; i < filters.length; i++){
                var neww = $(filters[i]).attr('data-slug');
                var name = $(filters[i]).attr('data-name');
                v.push({name: name, slug : name, value : $(filters[i]).val()});                
            }
            $.ajax({
                type: 'POST',
                url: '<?= base_url() . "addToCart" ?>',
                data: {prod_id: $prod_id,filter_data:v,<?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                success: function (res) {
                    console.log(res);
                    myObj = JSON.parse(res);
                    //console.log(myObj);
                    if (myObj.status == 'not-ok') {
                        //$("#alert_msg").addClass('alert-danger');
                        //$('#alert_msg').html('Something went wrong.');
                        $("#wishlist-text").text('Something went wrong.');
                        $('#myAnchor').trigger('click');

                    } else if (myObj.status == 'ready-ok') {
                        $("#wishlist-text").text('');
                        $("#wishlist-text").html('Item updated to cart successfully.<div class="nbtn-srow"><div class="btn-left"><a href="<?=$_SESSION['refererUrl']?>" class="nbtn-pop invert">Continue Shopping</a></div><div class="btn-right"><a href="<?=base_url('cart')?>" class="nbtn-pop">Check Out Now</a></div></div>');

                        $('#myAnchor').trigger('click');

                       // $("#alert_msg").addClass('alert-success');
                        //$("#add_to_cart_msg").show();
                        $('.compl').attr('href', '<?= base_url('cart') ?>');
                        $('.compl').removeAttr('data-target')
                        if (myObj.cart_items > 1) {
                            $('#cart_items').html(myObj.cart_items + " items");
                        } else {
                            $('#cart_items').html(myObj.cart_items + " item");
                        }
                        $('#cart_sub_total').html(myObj.cart_sub_total);

                    }  else if (myObj.status == "limit-ok") {
                        $("#wishlist-text").text('Item out of stock.');
                        $('#myAnchor').trigger('click');

                       

                    } else {
                        $("#wishlist-text").text('');
                        $("#wishlist-text").html('Item added to cart successfully.<div class="nbtn-srow"><div class="btn-left"><a href="<?=$_SESSION['refererUrl']?>" class="nbtn-pop invert">Continue Shopping</a></div><div class="btn-right"><a href="<?=base_url('cart')?>" class="nbtn-pop">Check Out Now</a></div></div>');
                        
                        $('#myAnchor').trigger('click');

                       // $("#alert_msg").addClass('alert-success');
                        //$("#add_to_cart_msg").show();
                        $('.compl').attr('href', '<?= base_url('cart') ?>');
                        $('.compl').removeAttr('data-target')
                        if (myObj.cart_items > 1) {
                            $('#cart_items').html(myObj.cart_items + " items");
                        } else {
                            $('#cart_items').html(myObj.cart_items + " item");
                        }
                        $('#cart_sub_total').html(myObj.cart_sub_total);
                    }
                },
                error: function () {
                    console.log('Network error.');
                }
            });
        });
    });
</script>


<script type="text/javascript">
    $(document).on('click', '.wish', function () {
        var element = $(this);
        var prod_id = $(this).attr("data-id");

        $.ajax({
            url: '<?= base_url('add-product-wishlist') ?>',
            type: 'POST',
            data: {prod_id: prod_id, <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
        })
        .done(function (result) {
            if (result == 1) {
                //save succesfully
                //$('.change'+prod_id).addClass('active');
                element.addClass('active');
                $("#wishlist-text").text('Item added to your wishlist');
                $('#myAnchor').trigger('click');
            } else if (result == 2) {
                //already exists
                $("#wishlist-text").text('Item already exists in your wishlist');
                $('#myAnchor').trigger('click');
            } else {
                error = 1;
            }
            console.log("success");
        })
        .fail(function () {
            console.log("error");
        })
        .always(function () {
            console.log("complete");
        });
    });
</script>