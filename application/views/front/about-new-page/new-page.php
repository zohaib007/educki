<?php
if (empty($pagedata->sub_pg_image)) {
    $path = base_url('resources/no_banner/no_banner.png');
} else {
    $path = base_url('resources/page_image/' . $pagedata->sub_pg_image);
}
?>
<!--banner  starts here-->
<section class="whted-bg" style="background-image: url(<?= $path ?>); !important">
    <div class="frg-layr"></div>
    <!--Breadcrumbs starts here-->
    <div class="container">
        <div class="row">
            <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
                <div class="auto-container">
                    <div class="brd-whit">
                        <div class="brd-sec">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?= base_url() ?><?= $pageurl->sub_pg_url?>">About</a></li>
                                <li class="breadcrumb-item active"> <?= $pagedata->sub_pg_title; ?></li>
                            </ol>
                        </div>

                        <h2><?= $pagedata->sub_pg_title; ?></h2>	
                    </div>

                </div>
            </div>
        </div>

    </div>
    <!--Breadcrumbs ends  here-->

</section>
<!--banner  starts here-->

<?php $this->load->view('front/about/about_side_nav'); ?>

<div class="col-md-9 col-lg-9 col-sm-9 col-xs-12 pad-rgt-0">
    <div class="whted-rgt-sec">
        <div class="community-sec">
            <?= clean_string($pagedata->sub_pg_description); ?>
        </div>
        <div class="sprt-sec">
            <p>
                Still need assistance? Check out our <a href="<?=base_url()?><?=get_page_url(15,'tbl_pages_sub','sub_id')->sub_pg_url;?>">Support Center</a>.
            </p>
        </div>
    </div>
</div>
</div>
</div>
</div>
</section>

<!--main sec  ends here-->