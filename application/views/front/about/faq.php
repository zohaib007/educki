<?php
if (empty($pagedata->sub_pg_image)) {
    $path = base_url('resources/no_banner/no_banner.png');
} else {
    $path = base_url('resources/page_image/' . $pagedata->sub_pg_image);
}
?>
<!--banner  starts here-->
<section class="whted-bg" style="background-image: url(<?= $path ?>); !important">
    <div class="frg-layr"></div>
    <!--Breadcrumbs starts here-->
    <div class="container">
        <div class="row">
            <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
                <div class="auto-container">
                    <div class="brd-whit">
                        <div class="brd-sec">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
                                <li class="breadcrumb-item"><a href="<?=base_url()?><?= $pageurl->sub_pg_url?>">About</a></li>
                                <li class="breadcrumb-item active"><?=$pagedata->sub_pg_title;?></li>
                            </ol>
                        </div>
                        <h2><?=$pagedata->sub_pg_title;?></h2>
                    </div>      
                </div>
            </div>
        </div>
    </div>        <!--Breadcrumbs ends  here-->
</section>  <!--banner  starts here-->
<!--main sec starts here-->
<section class="weht-main">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 pad-lft-0">
                    <div class="side-nav">
                        <ul>
                           <?php
                            $footermenu = about_side_menu();
                            foreach ($footermenu as $items) {
                                if($pagedata->sub_pg_url == $items['sub_pg_url']){
                                        $active = 'active'; 
                                    }else {
                                        $active = '';
                                    }
                                ?>
                                <li>
                                    <a class="<?=$active?>" href="<?= base_url() ?><?= $items['sub_pg_url'] ?>"><?= $items['sub_pg_title'] ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="faq-emal">
                        <a href="<?=base_url()?><?=get_page_url(15,'tbl_pages_sub','sub_id')->sub_pg_url?>">Email us</a>
                    </div>
                </div>
                <div class="col-md-9 col-lg-9 col-sm-9 col-xs-12 pad-rgt-0">
                    <div class="whted-rgt-sec">
                        <div class="faq-sec">
                            <?php echo form_open_multipart('faq-Search', array('name' => 'faq_search', 'id' => 'faq_search')); ?>
                            <div class="faq-srch">
                                <input type="text" name="search" placeholder="Ask or enter a search term here. "/>
                                <input type="submit" value="" />
                            </div>
                            <div class="ualert-row">
                                <?php
                                if ($this->session->flashdata('msg') != "") {
                                    echo '<div id="message" class="alert alert-success message "><a href="javascript:;" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>' . $this->session->flashdata('msg') . '</div>';
                                }
                                ?>
                            </div>
                            <?php echo form_close(); ?>
                            <div class="faq-accord">
                                <div class="panel-group" id="accordion">
                                    <?php $row_count = 1;
                                    foreach ($list as $page) { 
//                                        if($page['sub_sub_pg_status'] == '1'){
                                        ?>
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle only-lft" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $row_count ?>">
                                                        <span class="arrow-rgt"></span>
                                                        <?= $page['sub_sub_pg_title'] ?>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse<?php echo $row_count; ?>" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p><?= clean_string($page['sub_sub_pg_description']); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php //  }?>
                                        <?php $row_count++;
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  <!--main sec  ends here-->
<script>
    $(function () {
        $('.message').append('<span class="close" title="Dismiss"></span>');
//        setTimeout(function() { $(".message").fadeOut(1500); }, 4000);
        $('.message .close').hover(
                function () {
                    $(this).addClass('hover');
                },
                function () {
                    $(this).removeClass('hover');
                }
        );

        $('.message .close').click(function () {
            $(this).parent().fadeOut('slow', function () {
                $(this).remove();
            });
        });
    });
</script>
<script>
	$('.collapse').on('shown.bs.collapse', function(){
		$(this).parent().find(".arrow-rgt").removeClass("arrow-rgt").addClass("arrow-down");
		
		}).on('hidden.bs.collapse', function(){
		$(this).parent().find(".arrow-down").removeClass("arrow-down").addClass("arrow-rgt");
	});
</script>