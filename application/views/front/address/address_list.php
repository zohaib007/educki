<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard'); ?>">My Account</a></li>
                        <li class="breadcrumb-item active">Address Book</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav'); ?>
                <!--Left section starts here-->
                <!-- Right Pannel Starts Here -->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">
                        <!-- Inner Section Starts -->
                        <div class="message-wrapper">
                            <!-- Heading Section Starts -->
                            <div class="my-account-right-head addbtmPad">
                                <div class="msg-hd-inrow">
                                    <h5>Address Book</h5>
                                    <div class="adbtn-wrap">
                                        <div class="custombtn-large">
                                            <a href="<?= base_url('add-address'); ?>" title="Add New">Add New</a>
                                        </div>
                                    </div>
                                </div>                                <!-- <p>Sample Text Here.</p> -->
                            </div>                            <!-- Heading Section Ends -->
                            <div class="ualert-row">
                                <?php
                                if ($this->session->flashdata('msg') != "") {
                                    echo '<div id="message" class="alert alert-success message "><a href="javascript:;" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>' . $this->session->flashdata('msg') . '</div>';
                                }
                                ?>
                            </div>
                            <!-- Bottom Section Starts -->
                            <div class="store-btmWrap">
                                <!-- Main Section Starts -->
                                <div class="store-mainsec">
                                    <!-- Top Grey Bar Starts -->
                                    <div class="topGrey-bar">
                                        <div class="topGrey-inner">
                                            <!-- Search By Form Starts -->
                                            <div class="store-tophdr">
                                                <!-- Left Heading Starts -->
                                                <div class="store-th">
                                                    <h3>Default Addresses</h3>
                                                </div>  
                                                <!-- Left Heading Ends -->
                                                <!-- Right Heading Starts -->
                                                <?php if (isset($default_billing->country) || isset($default_shipping->country)) { 
                                                    if($default_billing->add_id == $default_shipping->add_id){ ?>
                                                <div class="store-th2" style="float: right;">
                                                    <a href="<?= base_url(); ?>edit-address/<?= $default_billing->add_id ?>" title="Edit">Edit</a>
                                                    <!-- <span>|</span>
                                                    <a href="<?= base_url(); ?>delete-address/<?= $default_billing->add_id ?>" title="Delete">Delete</a> -->
                                                </div>  
                                                <?php } } ?>
                                                <!-- Right Heading Ends -->
                                            </div>                                        <!-- Search By Form Ends -->
                                        </div>
                                    </div>

                                    <!-- Top Grey Bar Ends -->
                                    <!-- Bottom Grey Section Starts -->
                                    <div class="btm-boxwrap">
                                        <div class="btm-boxinner">
                                            <!-- Address Main Section Starts -->
                                            <div class="addressSec-wrap">
                                                <!-- Border Row Starts -->
                                                <div class="addressSec-bdr">
                                                    <!-- Address Row Starts -->
                                                    <div class="addressSec-row">
                                                        <?php if (isset($default_billing->country) || isset($default_shipping->country)) { ?>
                                                            <!-- Column Starts -->
                                                            <div class="addressSec-col">
                                                                <!-- Track Section Starts -->
                                                                <div class="inany-wrapper">
                                                                    <!-- Row Starts -->
                                                                    <div class="inany-row">
                                                                        <h3>Default Billing Address</h3>
                                                                    </div>
                                                                    <?php if (isset($default_billing->country)) { ?>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">Nickname:</div>
                                                                            <div class="inany-dtl"><?= (isset($default_billing->nick_name) ? $default_billing->nick_name : "") ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">Name:</div>
                                                                            <div class="inany-dtl"><?= (isset($default_billing->first_name) ? $default_billing->first_name . ' ' . $default_billing->last_name : "") ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">Telephone:</div>
                                                                            <div class="inany-dtl"><?= (!empty($default_billing->phone) ? $default_billing->phone : "N/A") ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">Street:</div>
                                                                            <div class="inany-dtl"><?= (isset($default_billing->street) ? $default_billing->street : "") ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">City:</div>
                                                                            <div class="inany-dtl"><?= (isset($default_billing->city) ? $default_billing->city : "") ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">State: </div>
                                                                            <div class="inany-dtl"><?php
                                                                                if (isset($default_billing->country)) {
                                                                                    echo ($default_billing->country == "US" ? get_state_name($default_billing->state) : $default_billing->state_other);
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">Country:</div>
                                                                            <div class="inany-dtl"><?= (isset($default_billing->country) ? get_country_name($default_billing->country) : "") ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">Zip:</div>
                                                                            <div class="inany-dtl"><?= (isset($default_billing->zip) ? $default_billing->zip : "") ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="adbtm-row">
                                                                            <?php if (isset($default_billing->add_id)) { 
                                                                                if($default_billing->add_id != $default_shipping->add_id){?>
                                                                                <a href="<?= base_url(); ?>edit-address/<?= $default_billing->add_id ?>" title="Edit">Edit</a>
                                                                                <!-- <span>|</span>
                                                                                <a href="<?= base_url(); ?>delete-address/<?= $default_billing->add_id ?>" title="Delete">Delete</a> -->
                                                                            <?php } } ?>
                                                                        </div>                                                              <!-- Row Starts -->
                                                                    <?php } else { ?>
                                                                        <div>No Data Found</div>
                                                                    <?php } ?>
                                                                </div>                                                            <!-- Track Section Ends -->
                                                            </div>                                                       <!-- Column Ends -->
                                                            <!-- Column Starts -->
                                                            <div class="addressSec-col right">
                                                                <!-- Track Section Starts -->
                                                                <div class="inany-wrapper">
                                                                    <!-- Row Starts -->
                                                                    <div class="inany-row">
                                                                        <h3>Default Shipping Address</h3>
                                                                    </div>
                                                                    <?php if (isset($default_shipping->country)) { ?>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">Nickname:</div>
                                                                            <div class="inany-dtl"><?= (isset($default_shipping->nick_name) ? $default_shipping->nick_name : "") ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">Name:</div>
                                                                            <div class="inany-dtl"><?= (isset($default_shipping->first_name) ? $default_shipping->first_name . ' ' . $default_shipping->last_name : "") ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">Telephone:</div>
                                                                            <div class="inany-dtl"><?= (!empty($default_shipping->phone) ? $default_shipping->phone : "N/A") ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">Street:</div>
                                                                            <div class="inany-dtl"><?= (isset($default_shipping->street) ? $default_shipping->street : "") ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">City:</div>
                                                                            <div class="inany-dtl"><?= (isset($default_shipping->city) ? $default_shipping->city : "") ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">State:</div>
                                                                            <div class="inany-dtl"><?php
                                                                                if (isset($default_shipping->country)) {
                                                                                    echo ($default_shipping->country == "US" ? get_state_name($default_shipping->state) : $default_shipping->state_other);
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">Country:</div>
                                                                            <div class="inany-dtl"><?= (isset($default_shipping->country) ? get_country_name($default_shipping->country) : "") ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="inany-row">
                                                                            <div class="inany-lbl">Zip:</div>
                                                                            <div class="inany-dtl"><?= (isset($default_shipping->zip) ? $default_shipping->zip : "") ?></div>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                        <!-- Row Starts -->
                                                                        <div class="adbtm-row">
                                                                            <?php if (isset($default_shipping->add_id)) { 
                                                                                if($default_billing->add_id != $default_shipping->add_id){?>
                                                                                <a href="<?= base_url(); ?>edit-address/<?= $default_shipping->add_id ?>" title="Edit">Edit</a>
                                                                                <!-- <span>|</span>
                                                                                <a href="<?= base_url(); ?>delete-address/<?= $default_shipping->add_id ?>" title="Delete">Delete</a> -->
                                                                            <?php } } ?>
                                                                        </div>
                                                                        <!-- Row Starts -->
                                                                    <?php } else { ?>
                                                                        <div>No Data Found</div>
                                                                    <?php } ?>
                                                                </div>
                                                                <!-- Track Section Ends -->
                                                            </div>
                                                            <!-- Column Ends -->
                                                        <?php } else { ?>
                                                            <div>No data found.</div>
                                                        <?php } ?>
                                                    </div>
                                                    <!-- Address Row Ends -->
                                                </div>
                                                <!-- Border Row Ends -->
                                            </div>
                                            <!-- Address Main Section Ends -->
                                        </div>
                                    </div>
                                    <!-- Bottom Grey Section Ends -->

                                </div>
                                <!-- Main Section Ends -->
                                <!-- Main Section Starts -->
                                <div class="store-mainsec">
                                    <!-- Top Grey Bar Starts -->
                                    <div class="topGrey-bar">
                                        <div class="topGrey-inner">
                                            <!-- Search By Form Starts -->
                                            <div class="store-tophdr">
                                                <!-- Left Heading Starts -->
                                                <div class="store-th">
                                                    <h3>Additional Addresses</h3>
                                                </div>
                                                <!-- Left Heading Ends -->
                                            </div>
                                            <!-- Search By Form Ends -->
                                        </div>
                                    </div>
                                    <!-- Top Grey Bar Ends -->
                                    <!-- Bottom Grey Section Starts -->
                                    <div class="btm-boxwrap">
                                        <div class="btm-boxinner">
                                            <!-- Address Main Section Starts -->
                                            <div class="addressSec-wrap">
                                                <!-- Border Row Starts -->
                                                <div class="addressSec-bdr">
                                                    <!-- Address Row Starts -->
                                                    <div class="addressSec-row">
                                                        <?php
                                                        if ($address_list != '') {
                                                            foreach (array_chunk($address_list, 2, true) as $newlist) {
                                                                echo '</div><div class="addressSec-row">';
                                                                $count = 1;
                                                                foreach ($newlist as $row) {
                                                                    ?>
                                                                    <!-- Column Starts -->
                                                                    <div class="addressSec-col <?= ($count % 2 == 0) ? 'right' : '' ?>">
                                                                        <!-- Track Section Starts -->
                                                                        <div class="inany-wrapper">
                                                                            <!-- Row Starts -->
                                                                            <div class="inany-row">
                                                                                <div class="inany-lbl">Nickname:</div>
                                                                                <div class="inany-dtl"><?= $row->nick_name ?></div>
                                                                            </div>
                                                                            <!-- Row Starts -->
                                                                            <!-- Row Starts -->
                                                                            <div class="inany-row">
                                                                                <div class="inany-lbl">Name:</div>
                                                                                <div class="inany-dtl"><?= $row->first_name . ' ' . $row->last_name ?></div>
                                                                            </div>
                                                                            <!-- Row Starts -->
                                                                            <!-- Row Starts -->
                                                                            <div class="inany-row">
                                                                                <div class="inany-lbl">Telephone:</div>
                                                                                <div class="inany-dtl"><?=(!empty($row->phone) ? $row->phone : "N/A")?></div>
                                                                            </div>
                                                                            <!-- Row Starts -->

                                                                            <!-- Row Starts -->
                                                                            <div class="inany-row">
                                                                                <div class="inany-lbl">Street:</div>
                                                                                <div class="inany-dtl"><?= $row->street ?></div>
                                                                            </div>
                                                                            <!-- Row Starts -->

                                                                            <!-- Row Starts -->
                                                                            <div class="inany-row">
                                                                                <div class="inany-lbl">City:</div>
                                                                                <div class="inany-dtl"><?= $row->city ?></div>
                                                                            </div>
                                                                            <!-- Row Starts -->

                                                                            <!-- Row Starts -->
                                                                            <div class="inany-row">
                                                                                <div class="inany-lbl">State:</div>
                                                                                <div class="inany-dtl"><?php
                                                                                    if (isset($row->country)) {
                                                                                        echo ($row->country == "US" ? get_state_name($row->state) : $row->state_other);
                                                                                    }
                                                                                    ?>
                                                                                </div>
                                                                            </div>
                                                                            <!-- Row Starts -->
                                                                            <!-- Row Starts -->
                                                                            <div class="inany-row">
                                                                                <div class="inany-lbl">Country:</div>
                                                                                <div class="inany-dtl"><?= get_country_name($row->country) ?></div>
                                                                            </div>
                                                                            <!-- Row Starts -->
                                                                            <!-- Row Starts -->
                                                                            <div class="inany-row">
                                                                                <div class="inany-lbl">Zip:</div>
                                                                                <div class="inany-dtl"><?= $row->zip ?></div>
                                                                            </div>
                                                                            <!-- Row Starts -->
                                                                            <!-- Row Starts -->
                                                                            <div class="adbtm-row">
                                                                                <a href="<?= base_url(); ?>edit-address/<?= $row->add_id ?>" title="Edit">Edit</a>
                                                                                <span>|</span>
                                                                                <a href="<?= base_url(); ?>delete-address/<?= $row->add_id ?>" title="Delete">Delete</a>
                                                                            </div>
                                                                            <!-- Row Starts -->
                                                                        </div>
                                                                        <!-- Track Section Ends -->
                                                                    </div>
                                                                    <?php
                                                                    /*echo ($count % 2 == 0) ? '</div></div><div class="addressSec-bdr"><div class="addressSec-row">' : '';*/
                                                                    $count ++;
                                                                }
                                                            }
                                                        } else {
                                                            ?>
                                                            <div>No data found.</div>
                                                        <?php }
                                                        ?>                                                        <!--<div class="addressSec-col right">-->
                                                    </div>                                                    <!-- Address Row Ends -->
                                                </div>                                                <!-- Border Row Ends -->
                                            </div>                                            <!-- Address Main Section Ends -->
                                        </div>
                                    </div>                                    <!-- Bottom Grey Section Ends -->
                                </div>                                <!-- Main Section Ends -->
                            </div>                            <!-- Bottom Section Ends -->
                        </div>                        <!-- Inner Section Ends -->
                    </div>
                </div>                <!-- Right Pannel Starts Here -->
            </div>
        </div>
    </div>
</section>
<!--Blog main ends  here-->
<script type="text/javascript">
    $(function () {
        $('.uavailable').click(function () {
            var id = $(this).val();
            var val = 0;
            var active = $(this).is(':checked');
            var title = 'OFF';
            if (active === true) {
                val = 1;
                title = 'ON'
            }
            $(this).parent().find('.round').attr('title', title);
            //$(this).parent().parent().find('.ulbl-txt').html(title); to change displayed text with checkbox
        });
    });
</script>
<script>
    $(function () {
        $('.message').append('<span class="close" title="Dismiss"></span>');
        //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
        $('.message .close').hover(
                function () {
                    $(this).addClass('hover');
                },
                function () {
                    $(this).removeClass('hover');
                }
        );

        $('.message .close').click(function () {
            $(this).parent().fadeOut('slow', function () {
                $(this).remove();
            });
        });
    });
</script>