<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url('my-dashboard'); ?>">My Account</a></li>
                        <li class="breadcrumb-item active">Change Password</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

</div>
<!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">

                <!--Left section starts here-->
                <?php $this->load->view('front/includes/seller-left-nav'); ?>
                <!--Left section starts here-->

                <!--right section starts here-->
                <div class="col-sm-9 col-xs-12">
                    <?php echo form_open_multipart('check-password', array('name' => 'changepassword', 'id' => 'changepassword_form')); ?>
                    <div class="my-account-right">
                        <div class="my-account-right-head pad-btm-0">
                            <h5>Change Password</h5>
                        </div>
                        <div class="store-profile">
                            <div class="change-pas-dv" style="display:block;">
<!--                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>Current Password</h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="text" name="password" id="password" placeholder="" />
                                        <div class="error" id="password_validate"></div>
                                    </div>
                                </div>-->

                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>New Password</h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="text" name="new_password" id="new_password" placeholder="" />
                                    </div>
                                </div>

                                <div class="store-profile-row">
                                    <div class="store-profile-row-lft">
                                        <h4>Confirm New Password</h4>
                                    </div>
                                    <div class="store-profile-row-rgt">
                                        <input type="text" name="confirm_password" id="confirm_password" placeholder=""/>
                                        <div class="error" id="confirm_password_validate"></div>
                                    </div>
                                </div>
                                <input type="hidden" name="user_id" id="user_id" value="<?= $userid ?>"/>
                                <div class="store-profile-row">
                                    <input class="upld-btn" type="submit" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>                <!--right section starts here-->
            </div>
        </div>
    </div>
</section>
<script>
    function check() {
        var url = "<?= base_url() ?>users/check_password";
        var password = $("#password").val();
        var user_id = $("#user_id").val();
        $.ajax({
            url: url,
            type: "POST",
            dataType: "json",
            data: {password: password, user_id: user_id},
            success: function (data) {
                console.log(data);
            }
        });
    }
</script>
<script>
    $(document).ready(function () {
        $(function validate() {
            // body...
            var rules = {
                rules: {
                    new_password: {
                        required: true,
                        minlength: 6
                    },
                    confirm_password: {
                        equalTo: "#new_password",
                        minlength: 6
                    }
                },
                errorPlacement: function (error, element) {
                    var name = $(element).attr("name");
                    error.appendTo($("#" + name + "_validate"));
                },
                onblur: true
            };

            $('#changepassword_form').validate(rules);
        });
    });
</script>