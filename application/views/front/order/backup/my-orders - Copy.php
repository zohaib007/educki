<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url()?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url()?>my-dashboard">My Account</a></li>
                        <li class="breadcrumb-item active">My Orders</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view("front/includes/seller-left-nav") ?> 
                <!--Left section starts here-->
                <!--right section starts here-->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">
                        <div class="mang-prod-mian">
                            <!--Manage product section-->	
                            <div class="mang-prod-mian-hed">
                                <h5>My Orders</h5>
                            </div>
                            <!--Manage product section-->
                            <!--Select option starts here-->
                            <div class="select-optn-sec">
                                <div class="my-account-right-2">
                                    <?php if ($results) { ?>
                                        <div class="well">
                                            <?php echo form_open_multipart('search_my_order', array('name' => 'search_my_order', 'id' => 'search_order_form')); ?> 
                                            <div class="mang-search-opt">
                                                <label>Search by:</label>
                                                <select class="selectpicker" id="select_search" name="select_search">
                                                    <option class="prod-id" value="0" >Select</option>
                                                    <option class="prod-id" value="3" >Product ID</option>
                                                    <option class="prod-id" value="4" >Product Title</option>
                                                </select>
                                                <div class="error" id="select_search_validate"><?php echo form_error('search_value') ?></div>
                                            </div>
                                            <div class="search-product">
                                                <input type="text" name="search_value" id="search_value" />
                                                <input type="hidden" value="3" name="step" />
                                                <input type="submit" value="Search" />
                                                <div class="error" id="search_value_validate"><?php echo form_error('search_value') ?></div>
                                            </div>                                            
                                            <?php echo form_close(); ?> 
                                        </div>
                                        <div class="pendshp-tb">	
                                            <div class="my-account-right-table">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th class="pad-lft-0" style="width:10%;">Date</th>
                                                            <th style="width:9%;">Order #</th>
                                                            <th  class="text-center" style="width:10%;">Item</th>
                                                            <th style="width:19%;"></th>
                                                            <th class="text-center" style="width:15%;">Tracking #</th>
                                                            <th style="width:18%;" class="text-center">Price + Shipping</th>
                                                            <th style="width:12%;" class="text-center">Status</th>

                                                            <th class="text-center pad-rgt-0" style="width:7%;">Detail</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>


                                                        <?php foreach ($results as $order) { ?>
                                                            <tr>
                                                                <td class="pad-lft-0"><?= date('m-d-Y', strtotime($order->order_date)) ?></td>
                                                                <td>
                                                                    <?= $order->order_id ?>
                                                                </td>
                                                                <?php
                                                                $o_prod = getOrderProductByOrderId($order->order_id);
                                                                ?>
                                                                <td class="text-center">
                                                                    <img src="<?= base_url() ?>resources/prod_images/thumb/<?= $order->order_prod_image ?>" alt="" title="" class="img-nset" />
                                                                </td>
                                                                <td>
                                                                    <a href="<?= productDetailUrl($order->order_prod_id) ?>/<?= $order->order_prod_url ?>" class="prt-btn">
                                                                        <?= $order->order_prod_name ?>
                                                                    </a>
                                                                </td>
                                                                <td class="text-center">
                                                                    <?php
                                                                    if($order->order_prod_tracking_number !=''){
                                                                        echo $order->order_prod_tracking_number;
                                                                    }elseif($order->order_prod_tracking_number =='' && $order->order_prod_shipping!='Local Pick Up' && $order->order_status!='cancel'){
                                                                        echo "Awaiting Tracking";
                                                                    }else{
                                                                        echo "Local Pick –Up";
                                                                    }?>
                                                                    <!-- Waiting -->
                                                                    <!-- N/A -->
                                                                </td>
                                                                <td class="text-center">
                                                                    <p>$<?= $order->order_sub_total ?> + $<?php
                                                                        if ($order->order_shipping_price == 0) {
                                                                            echo '0.00';
                                                                        } else {
                                                                            echo $order->order_shipping_price;
                                                                        }
                                                                        ?></p>
                                                                </td>
                                                                <?php $status = '';
                                                                if($order->order_status == 'cancel'){
                                                                    $status = 'Cancel';
                                                                }elseif($order->order_status == 'Awaiting Tracking' || $order->order_status == 'Awaiting Pickup'){
                                                                    $status = 'Processing';
                                                                }elseif($order->order_status == 'completed'){
                                                                    $status = 'Completed';
                                                                }elseif($order->order_status == 'In Process'){
                                                                    $status = 'Processing';
                                                                }
                                                                
                                                                ?>
                                                                <td class="text-center"><?=$status?></td>

                                                                <td class="text-center">
                                                                    <a href="<?= base_url() ?>my-order/<?= $order->order_id ?>" class="detl-btn"></a>

                                                                </td>
                                                            </tr>
                                                        <?php } ?>

                                                    </tbody>
                                                </table>

                                            </div>

                                        </div>
                                    <?php } else { ?>
                                        <div id="message" class="alert alert-danger message "><a href="javascript:;" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>No record found.</div>
                                    <?php } ?>
                                </div>
                            </div>

                            <!--Select option ends here-->   

                            <?php if (isset($results)) { ?>
                                <div class="shop-right-third">
                                    <div class="shop-right-third-lft">
                                        <p>
                                            <?php
                                            if ($total_records > 0) {
                                                echo $to + 1;
                                            } else {
                                                echo $to;
                                            }
                                            ?> - <?php
                                            if ($from > $total_records) {
                                                echo $total_records;
                                            } else {
                                                echo $from;
                                            }
                                            ?> of <?= $total_records ?><!--  in Category Name -->
                                        </p>
                                    </div>
                                    <div class="shop-right-third-rgt">
                                        <ul>
                                            <?php
                                            if (isset($links)) {
                                                echo $links;
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>

                        </div>



                    </div>	
                </div>  
                <!--right section starts here-->
            </div>
        </div>
    </div>
</section>
<!--Blog main ends  here-->
<script>
    $(function () {
        $('a').filter('a[rel^="next"]').addClass('page-right');
        $('a').filter('a[rel^="prev"]').addClass('page-left');
    });

</script>
<script>
    $(function () {
        $('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');
        //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
        $('.message .close').hover(
                function () {
                    $(this).addClass('hover');
                },
                function () {
                    $(this).removeClass('hover');
                }
        );

        $('.message .close').click(function () {
            $(this).parent().fadeOut('slow', function () {
                $(this).remove();
            });
        });
    });

    $(document).ready(function () {
        $.validator.addMethod("valueNotEquals", function (value, element, arg) {
            return arg !== value;
        }, "This field is required.");
        $(function validate() {
            // body...
            var rules = {
                rules: {
                    search_value: {
                        required: true
                    },
                    select_search: {valueNotEquals: "0"}
                },
                errorPlacement: function (error, element) {
                    var name = $(element).attr("name");
                    //error.html($("#" + name + "_validate"));
                    $("#" + name + "_validate").html(error);
                },
            };
            $('#search_order_form').validate(rules);
        });
    });
</script>