<!DOCTYPE html>
<html>
<head>
	<title><?=$title?></title>
	<style type="text/css">
		*{
			padding: 0;
			margin: 0;
		}
	</style>
</head>
<body>

	<table cellpadding="0" cellspacing="0" border="0" width="780" bgcolor="#FFFFFF" align="center" nowrap>
		
		<!-- Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="20px">&nbsp;</td>
		</tr>
		<!-- Row Ends -->

		<!-- Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;">
				<img src="<?= base_url() ?>resources/company_logo/<?=getSiteLogo()?>" alt="" />
			</td>
		</tr>
		<!-- Row Ends -->

		<!-- Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="30px">&nbsp;</td>
		</tr>
		<!-- Row Ends -->

		<!-- Heading Section Starts -->
		<tr>
			<td align="center" valign="middle">
		    	<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
		            <tr>
		                <td align="center" valign="middle" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-weight:bold; font-size:32px; color:#0c97c8; line-height:1.4;">
		                    ORDER DETAIL
		                </td>
		            </tr>
		        </table>
		    </td>
		</tr>
		<!-- Heading Section Ends -->

		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="23px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->

		<!-- Border Row Starts -->
		<tr>
			<td style="font-size: 1px; border-top: 2px solid #e8e6e8;" height="1px">&nbsp;</td>
		</tr>
		<!-- Border Row Ends -->
    	
		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="18px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->

		<!-- Table Row Starts -->
		<tr>
			<td align="center" valign="middle">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
			        <tr>
			        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
			            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
			                Order Date:
			            </td>
			            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:16px; color:#46494c; line-height:1.4;">
			            	January 26, 2018
			            </td>
			        </tr>

			        <!-- Spacing Row Starts -->
					<tr>
						<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="5px">&nbsp;</td>
					</tr>
					<!-- Spacing Row Ends -->

			        <tr>
			        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
			            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
			                Order ID:
			            </td>
			            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:16px; color:#46494c; line-height:1.4;">
			            	39
			            </td>
			        </tr>

			        <!-- Spacing Row Starts -->
					<tr>
						<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="5px">&nbsp;</td>
					</tr>
					<!-- Spacing Row Ends -->

			        <tr>
			        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
			            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
			                Order Total:
			            </td>
			            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:16px; color:#46494c; line-height:1.4;">
			            	$210.00
			            </td>
			        </tr>
			    </table>
			</td>
		</tr>
		<!-- Table Row Ends -->
    	
		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="18px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->

		<!-- Border Row Starts -->
		<tr>
			<td style="font-size: 1px; border-top: 2px solid #e8e6e8;" height="1px">&nbsp;</td>
		</tr>
		<!-- Border Row Ends -->
    	
		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="22px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->

		<!-- Table Row Starts -->
		<tr>
			<td align="center" valign="middle">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
					
			        <tr>
			        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
			            <td align="left" valign="top" bgcolor="#FFF" colspan="2" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
			                Shipping Address
			            </td>
			        </tr>

			        <!-- Spacing Row Starts -->
					<tr>
						<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="14px">&nbsp;</td>
					</tr>
					<!-- Spacing Row Ends -->

			        <tr>
			        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
			            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
			                Name:
			            </td>
			            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
			            	test test last
			            </td>
			        </tr>

			        <!-- Spacing Row Starts -->
					<tr>
						<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
					</tr>
					<!-- Spacing Row Ends -->


			        <tr>
			        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
			            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
			                Address:
			            </td>
			            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
			            	test Street
			            </td>
			        </tr>

			        <!-- Spacing Row Starts -->
					<tr>
						<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
					</tr>
					<!-- Spacing Row Ends -->


			        <tr>
			        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
			            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
			                City:
			            </td>
			            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
			            	test city
			            </td>
			        </tr>

			        <!-- Spacing Row Starts -->
					<tr>
						<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
					</tr>
					<!-- Spacing Row Ends -->


			        <tr>
			        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
			            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
			                State:
			            </td>
			            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
			            	test state
			            </td>
			        </tr>

			        <!-- Spacing Row Starts -->
					<tr>
						<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
					</tr>
					<!-- Spacing Row Ends -->


			        <tr>
			        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
			            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
			                Zip:
			            </td>
			            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
			            	44545
			            </td>
			        </tr>
					
			    </table>
			</td>
		</tr>
		<!-- Table Row Ends -->

		
    	
		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="33px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->


		<!-- Table Row Starts -->
		<tr>
			<td align="center" valign="middle">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
					<tr>
						
						<!-- Col Starts -->
						<td align="left" valign="top" bgcolor="#FFF" width="50%">
							<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
								
						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" colspan="2" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
						                Payment Method
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="14px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->

						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" colspan="2" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	Paypal
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->
								
						    </table>
						</td>
						<!-- Col Ends -->

						<!-- Col Starts -->
						<td align="left" valign="top" bgcolor="#FFF" width="50%">
							<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
								
						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" colspan="2" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
						                Order Summary
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="14px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->

						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                Order Sub Total:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	$200.00
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->


						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                Order Discount:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	- $0.00
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->


						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:15px; color:#46494c; line-height:1.4;">
						                Shipping & Handing:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	$10.00
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="30px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->


						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
						                Order Total
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
						            	$220.00
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->
								
						    </table>
						</td>
						<!-- Col Ends -->
						
					</tr>
				
				</table>
			</td>
		</tr>
		<!-- Table Row Ends -->


    	
		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="25px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->

		<!-- Border Row Starts -->
		<tr>
			<td style="font-size: 1px; border-top: 2px solid #e8e6e8;" height="1px">&nbsp;</td>
		</tr>
		<!-- Border Row Ends -->

		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="25px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->



		<!-- REPEATABLE SECTION STARTS -->

		<!-- Product Row Starts -->
		<tr>
			<td align="center" valign="middle">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
					<tr>
						
						<!-- Col Starts -->
						<td align="left" valign="top" bgcolor="#FFF" width="58%">
							<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">

						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="110">
						            	<img src="<?= base_url() ?>resources/company_logo/<?=getSiteLogo()?>" alt="" width="90" />
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<p style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            		Addition product<br /> 
						            		asAddition product<br /> 
						            		asAddition product as
						            	</p>
						            	<p>&nbsp;</p>
						            	<p style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            		Accept Returns<br />
						            		Qty: 1
						            	</p>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->
								
						    </table>
						</td>
						<!-- Col Ends -->

						<!-- Col Starts -->
						<td align="left" valign="top" bgcolor="#FFF" width="42%">
							<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
								
						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" colspan="2" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
						                Shipping Method
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="14px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->

						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-size:15px; color:#46494c; line-height:1.4;">
						                Charge for Shipping (0)
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	&nbsp;
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->


						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-size:15px; color:#46494c; line-height:1.4;">
						                Item:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	$200.00
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->


						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-size:15px; color:#46494c; line-height:1.4;">
						                Shipping & Handing:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	$20.00
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->
								
						    </table>
						</td>
						<!-- Col Ends -->
						
					</tr>
				
				</table>
			</td>
		</tr>
		<!-- Product Row Ends -->

		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="14px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->

		<!-- Border Row Starts -->
		<tr>
			<td style="font-size: 1px; border-top: 1px solid #e8e6e8;" height="1px">&nbsp;</td>
		</tr>
		<!-- Border Row Ends -->

		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="14px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->

		<!-- REPEATABLE SECTION ENDS -->




		<!-- REPEATABLE SECTION STARTS -->

		<!-- Product Row Starts -->
		<tr>
			<td align="center" valign="middle">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
					<tr>
						
						<!-- Col Starts -->
						<td align="left" valign="top" bgcolor="#FFF" width="58%">
							<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">

						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="110">
						            	<img src="<?= base_url() ?>resources/company_logo/<?=getSiteLogo()?>" alt="" width="90" />
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	<p style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            		Addition product<br /> 
						            		asAddition product<br /> 
						            		asAddition product as
						            	</p>
						            	<p>&nbsp;</p>
						            	<p style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            		Accept Returns<br />
						            		Qty: 1
						            	</p>
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->
								
						    </table>
						</td>
						<!-- Col Ends -->

						<!-- Col Starts -->
						<td align="left" valign="top" bgcolor="#FFF" width="42%">
							<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
								
						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" colspan="2" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-weight:bold; font-size:17px; color:#46494c; line-height:1.4;">
						                Shipping Method
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="14px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->

						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-size:15px; color:#46494c; line-height:1.4;">
						                Charge for Shipping (0)
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	&nbsp;
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->


						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-size:15px; color:#46494c; line-height:1.4;">
						                Item:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	$200.00
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->


						        <tr>
						        	<td align="left" valign="top" bgcolor="#FFF" width="19">&nbsp;</td>
						            <td align="left" valign="top" bgcolor="#FFF" width="276" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif;  font-size:15px; color:#46494c; line-height:1.4;">
						                Shipping & Handing:
						            </td>
						            <td align="left" valign="top" bgcolor="#FFF" style="font-family:'Helvetica Neue Light',Helvetica,Arial,sans-serif; font-size:14px; color:#46494c; line-height:1.4;">
						            	$20.00
						            </td>
						        </tr>

						        <!-- Spacing Row Starts -->
								<tr>
									<td align="center" valign="top" colspan="3" style="font-size: 1px;" height="4px">&nbsp;</td>
								</tr>
								<!-- Spacing Row Ends -->
								
						    </table>
						</td>
						<!-- Col Ends -->
						
					</tr>
				
				</table>
			</td>
		</tr>
		<!-- Product Row Ends -->

		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="14px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->

		<!-- Border Row Starts -->
		<tr>
			<td style="font-size: 1px; border-top: 1px solid #e8e6e8;" height="1px">&nbsp;</td>
		</tr>
		<!-- Border Row Ends -->

		<!-- Spacing Row Starts -->
		<tr>
			<td align="center" valign="top" style="font-size: 1px;" height="14px">&nbsp;</td>
		</tr>
		<!-- Spacing Row Ends -->

		<!-- REPEATABLE SECTION ENDS -->
    	



    	

	</table>

</body>
</html>