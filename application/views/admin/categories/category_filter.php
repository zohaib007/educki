<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
            <?php echo $title; ?>
        </title>
        <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
        <?php include(ADMIN_INCLUDE_PATH . "includes/js.php"); ?>
        <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>js/toastr/build/toastr.min.css" />
        <script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/toastr/build/toastr.min.js"></script>
        <script>
            $(function () {
                // Open
                $(document).on('click', '.nonfilter', function (e) {

                    var act = $(this).attr('act');
                    $('#nontitle').val('');
                    $('#nondetail').val('');
                    if (act == 'add') {
                        $('#nonact').val('add');
                        $('#add-row-non').attr('act', 'add');
                        $('#add-row-non').text('Add');
                        $('#nonheading').text('Add Filter');
                    } else {
                        $('#nonact').val('edit');
                        var id = $(this).attr('data-id');
                        var cid = $(this).attr('data-cid');
                        $('#tid').val(id);
                        $('#nontid').val(id);
                        $('#add-row-non').attr('act', 'edit');
                        $('#add-row-non').text('Update');
                        $('#nonheading').text('Edit Filter');
                        $.ajax({
                            type: 'POST',
                            url: base_url + "admin/categories/get_nonfilter",
                            data: {id: cid, tid: id, <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                            success: function (data) {
                                data = JSON.parse(data);
                                $('#nontitle').val(data.data.title);
                                $('#nondetail').val(data.data.value);
                            },
                        });

                    }
                    var targeted_popup_class = 'popup-1';
                    $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
                    e.preventDefault();
                });

                $(document).on('click', '.confilter', function (e) {
                    var act = $(this).attr('act');
                    $('#contitle').val('');
                    $('#convaluetitle').val('');
                    $('#convalue').val('');
                    $('#contitle').prop('disabled', false);
                    if (act == 'add') {
                        $('#conact').val('add');
                        $('#add-row-condata').text('Add');
                        $('#confilterdata').hide();
                        $('#detail-btn').hide();
                        $('#add-row-condata').text('Add');
                        $('#conheading').text('Add Conditional Filter');
                        $('#conadd').show();
                    } else if (act == 'edit') {
                        $('#conact').val('edit');
                        var id = $(this).attr('data-id');
                        var catId = $(this).attr('data-cat');
                        var dId = $(this).attr('data-did');
                        $('#contid').val(id);
                        $('#contdid').val(dId);
                        $('#contitle').prop('disabled', true);
                        $('#confilterdata').show();
                        $('#detail-btn').show();
                        $('#conadd').hide();
                        $('#add-row-condata').text('Update');
                        $('#conheading').text('Edit Conditional Filter');
                        $.ajax({
                            type: 'POST',
                            url: base_url + "admin/categories/getdata_confilter",
                            data: {id: catId, tid: id, did: dId, <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                            success: function (data) {
                                data = JSON.parse(data);
                                $('#contitle').val(data.data.title);
                                $('#convaluetitle').val(data.dataDetail.filter_title)
                                $('#convalue').val(data.dataDetail.filter_detail)

                            },
                        });
                    } else {
                        $('#conact').val('addm');
                        var id = $(this).attr('data-id');
                        var catId = $(this).attr('data-cat');
                        var dId = $(this).attr('data-did');
                        $('#contid').val(id);
                        $('#contdid').val(dId);
                        $('#contitle').prop('disabled', true);
                        $('#confilterdata').show();
                        $('#detail-btn').show();
                        $('#conadd').hide();
                        $('#add-row-condata').text('Add');
                        $('#conheading').text('Add Conditional Filter');
                        $.ajax({
                            type: 'POST',
                            url: base_url + "admin/categories/getdata_confilter",
                            data: {id: catId, tid: id, did: dId, <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                            success: function (data) {
                                data = JSON.parse(data);
                                $('#contitle').val(data.data.title);


                            },
                        });
                    }
                    var targeted_popup_class = 'popup-2';
                    $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);
                    e.preventDefault();
                });
                // Close
                $('[data-popup-close]').on('click', function (e) {
                    var targeted_popup_class = $(this).attr('data-popup-close');
                    $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
                    e.preventDefault();
                });
            });
        </script>
    </head>

    <body>
        <div class="container_p">
            <!-- Dashboard Left Side Begins Here -->
            <div class="left_wrp">
                <?php include(ADMIN_INCLUDE_PATH . "includes/dash-left.php"); ?>
            </div>
            <!-- Dashboard Left Side Ends Here -->
            <div class="right-rp">
                <!-- Top Green Bar Section Begins Here -->
                <?php include(ADMIN_INCLUDE_PATH . "includes/top_green_bar.php"); ?>
                <!-- Top Green Bar Section Ends Here -->
                <!-- Section Starts -->
                <div class="mu-contnt-wrp">
                    <div class="mu-contnt-hdng">
                        <h1>Categories Filters</h1>
                    </div>
                    <!-- Bread Crumbs Starts -->
                    <div class="n-crums">
                        <ul>
                            <li> <a href="<?php echo base_url('admin/categories'); ?>">Categories</a> </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li> <a href="javascript:void(0);">Filters (<?php echo $categoryData->cat_name; ?>)</a> </li>
                        </ul>
                    </div>
                    <!-- Bread Crumbs Ends -->
                    <?php echo form_open_multipart('', array('name' => 'add-category')); ?>
                    <!-- Filter Section Starts -->
                    <div class="mu-contnt-outer">
                        <!-- Add Structure Starts -->
                        <div class="ntop-btn-row">
                            <div class="ntop-btn">
                                <a href="javascript:void(0);" title="Add Category Filter" class="confilter" act="add" data-popup-open="popup-2">Add Conditional Filter</a>
                            </div>
                            <div class="ntop-btn">
                                <a href="javascript:void(0);" title="Add Filter" class="nonfilter" act="add">Add Filter</a>
                            </div>
                        </div>
                        <!-- Add Structure Ends -->
                        <div id="html">
                            <?php
                            if ($filters->num_rows() > 0) {
                                foreach ($filters->result() as $row) {
                                    ?>
                                    <?php if ($row->cat_filter_is_conditional == 0) { ?>
                                        <div class="conbox-row clhide" id="row-<?php echo $row->filter_title_id; ?>">
                                            <div class="mu-contnt-lfLbl ad-size">
                                                <h2>Filter Information</h2>
                                                <p>Set up the filter title and details. </p>
                                            </div>
                                            <div class="mu-contntBx-wrp">
                                                <div class="contntTop-row">
                                                    <!-- Repeatable Structure Starts -->
                                                    <div class="add_cat_filters">
                                                        <div class="customFilter-wrap">
                                                            <!-- Remove Structure Starts -->
                                                            <div class="nfull-row ar-space3">
                                                                <a href="javascript:void(0);" title="Remove" data-filter="filter_no" data-id="<?php echo $row->filter_title_id; ?>" class="addremove-lk remove_filter nonremove" data-cat="<?php echo $categoryData->cat_id; ?>">Remove Filter</a>
                                                            </div>
                                                            <!-- Remove Structure Ends -->
                                                            <!-- Custom Table Structure Starts -->
                                                            <div class="cfmain-tbl">
                                                                <!-- Left Starts -->
                                                                <div class="cfmain-tbl-left">
                                                                    <!-- Row Starts -->
                                                                    <div class="nanylist-row">
                                                                        <div class="nanylist-lbl">Filter Title:</div>
                                                                        <div class="nanylist-dtl">
                                                                            <?php echo stripcslashes($row->filter_title);?>
                                                                        </div>
                                                                    </div>
                                                                    <!-- Row Ends -->
                                                                    <!-- Row Starts -->
                                                                    <div class="nanylist-row">
                                                                        <div class="nanylist-lbl">Filter Details:</div>
                                                                        <div class="nanylist-dtl">
                                                                            <?php echo stripcslashes($row->cat_filter_values); ?>
                                                                        </div>
                                                                    </div>
                                                                    <!-- Row Ends -->
                                                                </div>
                                                                <!-- Left Ends -->
                                                                <!-- Right Starts -->
                                                                <div class="cfmain-tbl-right">
                                                                    <a title="Edit" href="javascript:void(0);" class="p-edt-btn-n non-conditional-edit nonfilter" data-id="<?php echo $row->filter_title_id; ?>" data-cat="<?php echo $categoryData->cat_id; ?>" act="edit"></a>
                                                                    <!--<a title="Delete" href="#" class="p-del-btn-n"></a>-->
                                                                </div>
                                                                <!-- Right Ends -->
                                                                <!-- Is Required Row Starts -->
                                                                <div class="nanylist-row isreq-set">
                                                                    <div class="nany-chkrp">
                                                                        <input <?php
                                                                        if ($row->is_required == '1') {
                                                                            echo 'checked="checked"';
                                                                        }
                                                                        ?> type="checkbox" name="requiredfld" value="" data-id="
                                                                            <?php echo $row->filter_title_id; ?>" class="requirefield" />
                                                                    </div>
                                                                    <div class="nany-chktxt">Is required filter?</div>
                                                                </div>
                                                                <!-- Is Required Row Ends -->
                                                            </div>
                                                            <!-- Custom Table Structure Ends -->
                                                            <div class="cat-sptr" style="display:none;"></div>
                                                            <!-- Seperator Line -->
                                                        </div>
                                                    </div>
                                                    <!-- Repeatable Structure Ends -->
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    } else {
                                        $contionalFilters = getConditionalFilter($row->filter_title_id);
                                        ?>
                                        <div class="conbox-row" id="row-<?php echo $row->filter_title_id; ?>">
                                            <div class="mu-contnt-lfLbl ad-size">
                                                <h2></h2>
                                                <p></p>
                                            </div>
                                            <div class="mu-contntBx-wrp">
                                                <div class="contntTop-row">
                                                    <!-- Repeatable Structure Starts -->
                                                    <div class="add_cat_filters">
                                                        <div class="customFilter-wrap">
                                                            <!-- Remove Structure Starts -->
                                                            <div class="nfull-row ar-space3">
                                                                <a href="javascript:void(0);" title="Remove" data-filter="filter_no" class="addremove-lk remove_filter comremove" data-id="<?php echo $row->filter_title_id; ?>" data-cat="<?php echo $categoryData->cat_id; ?>">Remove Filter</a>
                                                            </div>
                                                            <!-- Remove Structure Ends -->
                                                            <!-- Conditional Strcuture Starts -->
                                                            <div class="customFilter-subsec">
                                                                <!-- Left Starts -->
                                                                <div class="cfmain-tbl-left">
                                                                    <!-- Row Starts -->
                                                                    <div class="nanylist-row">
                                                                        <div class="nanylist-lbl">Conditional Filter Title:</div>
                                                                        <div class="nanylist-dtl">
                                                                            <div class="mu-flds-wrp">
                                                                                <input type="text" name="cft-txt" value="<?php echo stripcslashes($row->filter_title); ?>" placeholder="Conditional Filter Title" class="contitleedit" data-id="<?php echo $row->filter_title_id; ?>" data-cat="<?php echo $categoryData->cat_id; ?>" autocomplete="off" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- Row Ends -->
                                                                </div>
                                                                <!-- Left Ends -->
                                                            </div>
                                                            <!-- Conditional Structure Ends -->
                                                            <?php
                                                            if ($contionalFilters->num_rows() > 0) {

                                                                foreach ($contionalFilters->result() as $con) {
                                                                    ?>
                                                                    <!-- Custom Revisable Table Structure Starts -->
                                                                    <div class="cfmain-tbl">
                                                                        <!-- Left Starts -->
                                                                        <div class="cfmain-tbl-left">
                                                                            <!-- Row Starts -->
                                                                            <div class="nanylist-row">
                                                                                <div class="nanylist-lbl">Filter Title:</div>
                                                                                <div class="nanylist-dtl">
                                                                                    <?php echo stripcslashes($con->filter_title); ?>
                                                                                </div>
                                                                            </div>
                                                                            <!-- Row Ends -->
                                                                            <!-- Row Starts -->
                                                                            <div class="nanylist-row">
                                                                                <div class="nanylist-lbl">Filter Details:</div>
                                                                                <div class="nanylist-dtl">
                                                                                    <?php echo stripcslashes($con->filter_detail); ?>
                                                                                </div>
                                                                            </div>
                                                                            <!-- Row Ends -->
                                                                        </div>
                                                                        <!-- Left Ends -->
                                                                        <!-- Right Starts -->
                                                                        <div class="cfmain-tbl-right">
                                                                            <a title="Edit" href="javascript:void(0);" class="p-edt-btn-n confilter" data-id="<?php echo $row->filter_title_id; ?>" act="edit" data-cat="<?php echo $categoryData->cat_id; ?>" data-did="<?php echo $con->filter_detail_id; ?>"></a>
                                                                            <a title="Delete" href="javascript:void(0)" class="p-del-btn-n consignledelete" data-id="<?php echo $row->filter_title_id; ?>" data-cat="<?php echo $categoryData->cat_id; ?>" data-did="<?php echo $con->filter_detail_id; ?>"></a>
                                                                        </div>
                                                                        <!-- Right Ends -->
                                                                    </div>
                                                                    <!-- Custom Revisable Table Structure Ends -->
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                            <div class="btn01 cta-row">
                                                                <!-- Is Required Row Starts -->
                                                                <div class="nanylist-row isreq-set">
                                                                    <div class="nany-chkrp">
                                                                        <input <?php
                                                                        if ($row->is_required == 1) {
                                                                            echo 'checked="checked"';
                                                                        }
                                                                        ?> type="checkbox" name="requiredfld" value="1" data-id="
                                                                            <?php echo $row->filter_title_id; ?>" class="requirefield" />
                                                                    </div>
                                                                    <div class="nany-chktxt">Is required filter?</div>
                                                                </div>
                                                                <!-- Is Required Row Ends -->
                                                                <a href="javascript:void(0);" id="add-row" class="add-more-record confilter" data-id="<?php echo $row->filter_title_id; ?>" act="addm" data-cat="<?php echo $categoryData->cat_id; ?>" data-did="0">Add More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Repeatable Structure Ends -->
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php
                                }
                            } else {
                                ?>
                                <div class="conbox-row clhide">
                                    <div class="mu-contnt-lfLbl ad-size">
                                        <h2>Filter Information</h2>
                                        <p>Set up the filter title and details. </p>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <!-- Filter Section Ends -->
                    <!-- Submit Button Section Starts -->
                    <div class="mu-fld-sub">
                        <a href="<?= base_url() ?>admin/categories">Back</a>
                    </div>
                    <!-- Submit Button Section Ends -->
                    <!-- Add Filter Popup Starts -->
                    <div class="popup" data-popup="popup-1">
                        <div class="popup-inner">
                            <div class="pricing">
                                <h1 id="nonheading">Add Filter</h1>
                                <div class="my-form-record">
                                    <div class="mu-frmFlds_long" id="popup-data">
                                        <div class="catFilters filter-mwrap">
                                            <form id="nonfilter" method="post" action="" name="non">
                                                <input type="hidden" name="id" id="nonid" value="<?php echo $categoryData->cat_id; ?>" />
                                                <input type="hidden" name="act" id="nonact" value="add" />
                                                <input type="hidden" name="tid" id="nontid" value="" />
                                                <div class="mu-flds-wrp">
                                                    <div class="mu-frmFlds_long">
                                                        <label>Filter Title*</label>
                                                        <input type="text" value="" name="nontitle" id="nontitle" class="filterTitle" autocomplete="off" />
                                                        <div class="error" id="errnon"></div>
                                                    </div>
                                                </div>
                                                <div class="mu-flds-wrp">
                                                    <div class="mu-frmFlds_long">
                                                        <label>Filter Details</label>
                                                        <textarea class="text_meta" name="nondetail" id="nondetail" placeholder="Enter comma Seperated value (e.g. blue,red,green....)" ></textarea>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    <div class="btn01 btn-setting">
                                        <a href="javascript:void(0);" id="add-row-non" act="add" class="add-more-record">Add</a>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <a href="#" class="popup-close" data-popup-close="popup-1">x</a>
                        </div>
                    </div>
                    <!-- Add Filter Popup Ends -->
                    <!-- Add Category Filter Popup Starts -->
                    <div class="popup" data-popup="popup-2">
                        <div class="popup-inner">
                            <div class="custom-nfrm">
                                <h1 id="conheading">Add Conditional Filter</h1>
                                <div class="scrollable">
                                    <div class="my-form-record">
                                        <div class="mu-frmFlds_long" id="popup-data">
                                            <div class="catFilters filter-mwrap">
                                                <form id="confilter" method="post" action="" name="con">
                                                    <input type="hidden" name="id" id="conid" value="<?php echo $categoryData->cat_id; ?>" />
                                                    <input type="hidden" name="act" id="conact" value="add" />
                                                    <input type="hidden" name="tid" id="contid" value="" />
                                                    <input type="hidden" name="tdid" id="contdid" value="" />
                                                    <div class="mu-flds-wrp">
                                                        <div class="mu-frmFlds_long">
                                                            <label>Conditional Filter Title</label>
                                                            <input type="text" value="" class="filterTitle" id="contitle" name="title" autocomplete="off" />
                                                            <div class="error" id="errcon"></div>
                                                        </div>
                                                    </div>
                                                    <div class="btn01 ncat-btn" id="conadd">
                                                        <a href="javascript:void(0);" id="con-add-filter" class="add-more-record">Add Filter Details</a>
                                                    </div>
                                                    <!-- Titles detial Structure Starts -->
                                                    <div class="nfilter-detail" id="confilterdata" style="display:none;">
                                                        <!-- Repeatable Section Starts -->
                                                        <div class="cus-repeatsec">
                                                            <div class="mu-flds-wrp">
                                                                <div class="mu-frmFlds_long">
                                                                    <label>Filter Title</label>
                                                                    <input type="text" value="" class="filterTitle" id="convaluetitle" name="convaluetitle" />
                                                                    <div class="error" id="errcont"></div>
                                                                </div>
                                                            </div>
                                                            <div class="mu-flds-wrp">
                                                                <div class="mu-frmFlds_long">
                                                                    <label>Filter Details</label>
                                                                    <textarea class="text_meta" id="convalue" name="convalue" placeholder="Enter comma Seperated value (e.g. blue,red,green....)"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Repeatable Section Ends -->
                                                    </div>
                                                    <!-- Titles detial Structure Ends -->
                                            </div>
                                        </div>
                                        <div class="btn01 btn-setting" id="detail-btn" style="display:none;">
                                            <a href="javascript:void(0);" id="add-row-condata" class="add-more-record">Add</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="popup-close" data-popup-close="popup-2">x</a>
                        </div>
                    </div>
                    <!-- Add Category Filter Popup Ends -->
                    <?php echo form_close(); ?>
                </div>
                <!-- Section Ends -->
            </div>
        </div>
    </body>
    <script>
        var tit = '';
        $(document).ready(function (e) {

            //$('[data-popup="popup-1"]').fadeOut(0);
            //$('[data-popup="popup-2"]').fadeOut(0);
            $(document).on('click', '#add-row-non', function () {
                var val = $('#nontitle').val();
                $('#errnon').html('');
                var title = val;
                var detail = $('#nondetail').val();
                var id = $('#nonid').val();
                var act = $('#nonact').val();
                var tid = $('#nontid').val();
                if (val.length > 0) {
                    $('[data-popup="popup-1"]').fadeOut(0);
                    $.ajax({
                        type: 'POST',
                        url: base_url + "admin/categories/add_nonfilter",
                        data: {title: title, detail: detail, id: id, act: act, tid: tid, <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                        success: function (data) {
                            data = JSON.parse(data);
                            toastr.options.closeMethod = 'fadeOut';
                            toastr.options.closeDuration = 5000;
                            toastr.options.closeEasing = 'swing';
                            $('#html').html(data.html);

                            if (act == 'add') {
                                toastr.success('Filter added successfully!');
                                $("html, body").animate({scrollTop: $(document).height()}, 1000);
                            } else {
                                toastr.success('Filter updated successfully!');
                            }
                            $('#nontitle').val('');
                            $('#nondetail').val('');
                        },
                    });
                } else {
                    $('#errnon').html('This field is required.');
                }
            });
            $(document).on('click', '.nonremove', function () {
                var confirmValue = confirm('Are you sure you want to delete the selected item(s)? ');
                if (confirmValue == true) {
                    var id = $(this).attr('data-cat');
                    var tid = $(this).attr('data-id');
                    $.ajax({
                        type: 'POST',
                        url: base_url + "admin/categories/delete_nonfilter",
                        data: {id: id, tid: tid, <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                        success: function (data) {
                            data = JSON.parse(data);
                            toastr.options.closeMethod = 'fadeOut';
                            toastr.options.closeDuration = 5000;
                            toastr.options.closeEasing = 'swing';
                            $('#html').html(data.html);
                            toastr.success('Filter deleted successfully!');
                        },
                    });
                }
            });

            $(document).on('click', '#con-add-filter', function () {
                var title = $('#contitle').val();
                $('#errcon').html('');
                if (title.length > 0) {
                    $('#conadd').hide();
                    var id = $('#conid').val();
//                    alert(id);
                    $.ajax({
                        type: 'POST',
                        url: base_url + "admin/categories/add_confilter",
                        data: {id: id, title: title, <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                        success: function (data) {
                            data = JSON.parse(data);
                            toastr.options.closeMethod = 'fadeOut';
                            toastr.options.closeDuration = 5000;
                            toastr.options.closeEasing = 'swing';
                            $('#html').html(data.html);
                            $('#contid').val(data.id);
                            toastr.success('Filter added successfully!');
                            $("html, body").animate({scrollTop: $(document).height()}, 1000);
                            $('#contitle').prop('disabled', true);
                            $('#confilterdata').show();
                            $('#detail-btn').show();
                            $('#contitle').val();
                            $('#convaluetitle').val();
                            $('#convalue').val();
                        },
                    });
                } else {
                    $('#errcon').html('This field is required.');
                }
            });

            $(document).on('click', '#add-row-condata', function () {
                var filterId = $('#contid').val();
                var dfilterId = $('#contdid').val();
                var action = $('#conact').val();
                var categoryId = $('#conid').val();
                var title = $('#convaluetitle').val();
                var value = $('#convalue').val();
                $('#errcont').html('');
                if (title.length > 0) {
                    $('[data-popup="popup-2"]').fadeOut(0);
                    $.ajax({
                        type: 'POST',
                        url: base_url + "admin/categories/add_confilterdata",
                        data: {id: categoryId, fid: filterId, did: dfilterId, act: action, title: title, value: value, <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                        success: function (data) {
                            data = JSON.parse(data);
                            toastr.options.closeMethod = 'fadeOut';
                            toastr.options.closeDuration = 5000;
                            toastr.options.closeEasing = 'swing';
                            $('#html').html(data.html);
                            $('#contid').val(data.id);

                            //toastr.success('Filter detail added successfully!');
                            //$("html, body").animate({ scrollTop: $(document).height() }, 1000);
                            $('#contitle').prop('disabled', false);
                            $('#contitle').val('');
                            $('#convaluetitle').val('');
                            $('#convalue').val('');
                            $('#confilterdata').hide();
                            $('#detail-btn').hide();
                            $('#conadd').show();
                            if (action == 'add') {
                                toastr.success('Filter detail added successfully!');
                                $("html, body").animate({scrollTop: $(document).height()}, 1000);
                            } else if (action == 'edit') {
                                toastr.success('Filter detail updated successfully!');
                            } else {
                                toastr.success('Filter detail added successfully!');
                            }
                        },
                    });
                } else {
                    $('#errcont').html('This field is required.');
                }
            });

            $(document).on('click', '.comremove', function () {
                var confirmValue = confirm('Are you sure you want to delete the selected item(s)? ');
                if (confirmValue == true) {
                    var id = $(this).attr('data-cat');
                    var tid = $(this).attr('data-id');
                    $.ajax({
                        type: 'POST',
                        url: base_url + "admin/categories/delete_confilter",
                        data: {id: id, tid: tid, <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                        success: function (data) {
                            data = JSON.parse(data);
                            toastr.options.closeMethod = 'fadeOut';
                            toastr.options.closeDuration = 5000;
                            toastr.options.closeEasing = 'swing';
                            $('#html').html(data.html);
                            toastr.success('Filter deleted successfully!');
                        },
                    });
                }
            });

            $(document).on('click', '.consignledelete', function () {
                var confirmValue = confirm('Are you sure you want to delete the selected item(s)? ');
                if (confirmValue == true) {
                    var id = $(this).attr('data-cat');
                    var tid = $(this).attr('data-id');
                    var did = $(this).attr('data-did');
                    $.ajax({
                        type: 'POST',
                        url: base_url + "admin/categories/delete_confiltersingle",
                        data: {id: id, tid: tid, did: did, <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                        success: function (data) {
                            data = JSON.parse(data);
                            toastr.options.closeMethod = 'fadeOut';
                            toastr.options.closeDuration = 5000;
                            toastr.options.closeEasing = 'swing';
                            $('#html').html(data.html);
                            toastr.success('Filter deleted successfully!');
                        },
                    });
                }
            });
            $(document).on('focus', '.contitleedit', function () {
                tit = $(this).val();
            });
            $(document).on('keyup', '.contitleedit', function () {
                var catId = $(this).attr('data-cat');
                var id = $(this).attr('data-id');
                var val = $(this).val();
                if (val.length > 0) {

                } else {
                    $(this).val(tit);
                    val = tit;
                }
                $.ajax({
                    type: 'POST',
                    url: base_url + "admin/categories/update_contitle",
                    data: {id: id, cat: catId, val: val, <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                    success: function (data) {

                    },
                });
            });

            $(document).on('click', '.requirefield', function () {
                var id = $(this).attr('data-id');
                var checked = $(this).is(':checked');
                var val = 0;
                if (checked === true) {
                    val = 1;
                }
                $.ajax({
                    type: 'POST',
                    url: base_url + "admin/categories/is_required",
                    data: {id: id, val: val, <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                    success: function (data) {
                    }
                });
            });
        });
    </script>

</html>