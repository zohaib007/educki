<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Subcategory</title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />

<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>

<link rel="stylesheet" href="<?=base_url()?>js/tablesort/style.css" type="text/css" media="all" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/tablesort/jquery-latest.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/tablesort/jquery.tablesorter.js"></script>


<script>
$(function () {
	$('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');
	//setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
	$('.message .close').hover(
		function() { $(this).addClass('hover'); },
		function() { $(this).removeClass('hover'); }
	);
		
	$('.message .close').click(function() {
		$(this).parent().fadeOut('slow', function() { $(this).remove(); });
	});
});

$(document).ready(function() { 
    $("#mytable").tablesorter({ 
        // pass the headers argument and assing a object 
        headers: { 
            // assign the secound column (we start counting zero) 
           
			1: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            },
			2: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            },
			3: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            },
			4: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            }
        
        } 
    }); 
});

    
</script>

<style>
#display_tr {
	display: table-row !important;
}
</style>
</head>

<body>
<div class="container"> 
  
  <!-- Dashboard Left Side Begins Here -->
  <div class="left_wrp">
    <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
  </div>
  <!-- Dashboard Left Side Ends Here -->
  
  <div class="right-rp"> 
    
    <!-- Top Green Bar Section Begins Here -->
    <?php
include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
?>
    <!-- Top Green Bar Section Ends Here -->
   
    <div class="col-continer">
		<?php	
        if($this->session->flashdata('msg') != "")
        {
        echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
        }
        ?>
        
        <!-- Bread Crumbs Starts -->
        <div class="n-crums">
            <ul>
                <li> <a href="<?php echo base_url('admin/categories'); ?>">Category Management</a> </li>
                 <li>
                    <div class="crms-sep">&gt;</div>
                </li>
                <li> <a href="javascript:void(0);"><?=$cat_data['cat_name']?></a> </li>
                
            </ul>
        </div>
        <!-- Bread Crumbs Ends -->
        
      <div class="col-3 cat-box">
        <div class="contntTop-row">
          <div class="actvity-hd">
            <div class="act-tab b-ad-tb">
                <table cellspacing="0" border="0">
                    <colgroup>
                    <col width="35%" />
                    <col width="35%" />
                    <col width="42%" />
                    <!-- <col width="3%" />
                        <col width="3%" />
                        <col width="3%" />-->
                    </colgroup>
                    <tr>
                        <td class="no-bdr">
                        	<div class="cat-tbl-tp-lf">
                                <h1><?=$cat_data['cat_name']?></h1> <span class="n-of-cat"><?=$total_records?></span>
							</div>
						</td>
                        <td class="no-bdr">&nbsp;</td>
                        <td  class="no-bdr" align="right">
                            <a href="<?= base_url('admin/categories/add_subcategory/'.$cat_data['cat_id'])?>" class="pro-addNew"> Add Subcategory </a>
                        </td>
                    </tr>
                </table>
              
            </div>
          </div>
          <div class="b-prd-tbl align-top">
            <table border="0" cellspacing="0"  id="mytable" class='tablesorter'>
              <colgroup>
              <col width="67%" />
              <col width="15%" />
              <col width="18%" />
              </colgroup>
              <thead>
                <tr>
                  <th align="left">Category Title</th>
                  <th align="center">Status</th>
                  <th align="center">Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php
				if(count($list) > 0) { 
				for ($n = 0; $n < count($list); $n++) {
				?>
                <tr <?php #if (($n % 2) == 0) echo "class='odd'"; ?>>
                  <td class="td-content" align="left"><?=limited_text($list[$n]['cat_name'] , 40) ?></td>
                  <td class="td-content" align="center">
                    <?php
					if($list[$n]['cat_status'] == 1)
					{
						echo "Active";
					}
					else
					{
						echo "Inactive";
					}
					?>
                  </td>
                  
                  <td align="center"><table border="0" cellspacing="0" cellpadding="0">
                      <colgroup>
                      <col width="30%" />
                      <col width="20%" />
                      <col width="20%" />
                      <col width="30%" />
                      </colgroup>
                      <tr id="display_tr">
                        <td>&nbsp;</td>
                        <td align="center"><a title="Edit" href="<?= base_url() ?>admin/categories/edit_subcategory/<?= $list[$n]['cat_parent_id'] ?>/<?= $list[$n]['cat_id'] ?>" class="tik-cross-btns p-edt-btn-n"></a></td>
                        <td align="center"><a title="Delete" onclick="return confirm('Are you sure you want to delete the selected item(s)? ')" href="<?= base_url() ?>admin/categories/delete_subcategory/<?= $list[$n]['cat_parent_id'] ?>/<?= $list[$n]['cat_id'] ?>" class="tik-cross-btns p-del-btn-n"></a></td>
                        <td>&nbsp;</td>
                      </tr>
                    </table></td>
                </tr>
                <?php
					}
				}else { 
					echo "<tr><td colspan='3' align='center'><strong>No data found.</strong></td></tr>";
				}
				?>
              </tbody>
            </table>
            <div class="pagination-row" style="width:100%">
				<?=$paginglink ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--
<script>
$(document).ready(function() {
$('table').filterTable({ // apply filterTable to all tables on this page
quickList: [] // add some shortcut searches
});
});
</script> 
<script src="<?= base_url() ?>/js/jquery.filtertable.min.js"></script>
-->
</body>
</html>