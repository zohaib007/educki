<?php if($filters->num_rows()>0){
						foreach($filters->result() as $row){?>	
                    <?php if($row->cat_filter_is_conditional == 0){?>				
					<div class="conbox-row clhide" id="row-<?php echo $row->filter_title_id;?>">
						
						<div class="mu-contnt-lfLbl ad-size">
							<h2>Filter Information</h2>
							<p>Set up the filter title and details. </p>
						</div>
						
						<div class="mu-contntBx-wrp">
							<div class="contntTop-row">
								
								<!-- Repeatable Structure Starts -->
								<div class="add_cat_filters">
									
									<div class="customFilter-wrap">
										
	
										<!-- Remove Structure Starts -->
										<div class="nfull-row ar-space3">
											<a href="javascript:void(0);" title="Remove" data-filter="filter_no" data-id="<?php echo $row->filter_title_id;?>" class="addremove-lk remove_filter nonremove" data-cat="<?php echo $categoryData->cat_id;?>">Remove Filter</a>
										</div>
										<!-- Remove Structure Ends -->
										
										<!-- Custom Table Structure Starts -->
										<div class="cfmain-tbl">
											
											<!-- Left Starts -->
											<div class="cfmain-tbl-left">
												
												<!-- Row Starts -->
												<div class="nanylist-row">
													<div class="nanylist-lbl">Filter Title:</div>
													<div class="nanylist-dtl"><?php echo $row->filter_title;?></div>
												</div>
												<!-- Row Ends -->
												
												<!-- Row Starts -->
												<div class="nanylist-row">
													<div class="nanylist-lbl">Filter Details:</div>
													<div class="nanylist-dtl"><?php echo $row->cat_filter_values;?></div>
												</div>
												<!-- Row Ends -->
												
											</div>
											<!-- Left Ends -->
											
											<!-- Right Starts -->
											<div class="cfmain-tbl-right">
												<a title="Edit" href="javascript:void(0);" class="p-edt-btn-n non-conditional-edit nonfilter" data-id="<?php echo $row->filter_title_id;?>" data-cat="<?php echo $categoryData->cat_id;?>" act="edit"></a>
												<!--<a title="Delete" href="#" class="p-del-btn-n"></a>-->
											</div>
											<!-- Right Ends -->
                                            
                                                
                                            <!-- Is Required Row Starts -->
                                            <div class="nanylist-row isreq-set">
                                                <div class="nany-chkrp">
                                                    <input <?php if($row->is_required == '1' ){ echo 'checked="checked"';}?> type="checkbox" name="requiredfld" value="" data-id="<?php echo $row->filter_title_id;?>" class="requirefield" />
                                                </div>
                                                <div class="nany-chktxt">Is required filter?</div>
                                            </div>
                                            <!-- Is Required Row Ends -->
											
										</div>
										<!-- Custom Table Structure Ends -->
										
										
										<div class="cat-sptr" style="display:none;"></div> <!-- Seperator Line -->
										
										
									</div>
									
								</div>
								<!-- Repeatable Structure Ends -->
								
								
							</div>
						</div>
                        
						
					</div>
					<?php }else{
						$contionalFilters = getConditionalFilter($row->filter_title_id);
						?>
					<div class="conbox-row" id="row-<?php echo $row->filter_title_id;?>">
						
						<div class="mu-contnt-lfLbl ad-size">
							<h2></h2>
							<p></p>
						</div>
						
						<div class="mu-contntBx-wrp">
							<div class="contntTop-row">
								
								<!-- Repeatable Structure Starts -->
								<div class="add_cat_filters">
									
									<div class="customFilter-wrap">
										
										<!-- Remove Structure Starts -->
										<div class="nfull-row ar-space3">
											<a href="javascript:void(0);" title="Remove" data-filter="filter_no" class="addremove-lk remove_filter comremove" data-id="<?php echo $row->filter_title_id;?>" data-cat="<?php echo $categoryData->cat_id;?>">Remove Filter</a>
										</div>
										<!-- Remove Structure Ends -->
										
										<!-- Conditional Strcuture Starts -->
										<div class="customFilter-subsec">
											
											<!-- Left Starts -->
											<div class="cfmain-tbl-left">
												
												<!-- Row Starts -->
												<div class="nanylist-row">
													<div class="nanylist-lbl">Conditional Filter Title:</div>
													<div class="nanylist-dtl">
														<div class="mu-flds-wrp">
															<input type="text" name="cft-txt" value="<?php echo $row->filter_title;?>" placeholder="Conditional Filter Title" class="contitleedit" data-id="<?php echo $row->filter_title_id;?>" data-cat="<?php echo $categoryData->cat_id;?>" autocomplete="off"/>
														</div>
													</div>
												</div>
												<!-- Row Ends -->
												
											</div>
											<!-- Left Ends -->
											
										</div>
										<!-- Conditional Structure Ends -->
										<?php if($contionalFilters->num_rows() > 0){
											
											foreach($contionalFilters->result() as $con){?>
                                           
										<!-- Custom Revisable Table Structure Starts -->
										<div class="cfmain-tbl">
											
											<!-- Left Starts -->
											<div class="cfmain-tbl-left">
												
												<!-- Row Starts -->
												<div class="nanylist-row">
													<div class="nanylist-lbl">Filter Title:</div>
													<div class="nanylist-dtl"><?php echo $con->filter_title;?></div>
												</div>
												<!-- Row Ends -->
												
												<!-- Row Starts -->
												<div class="nanylist-row">
													<div class="nanylist-lbl">Filter Details:</div>
													<div class="nanylist-dtl"><?php echo $con->filter_detail;?></div>
												</div>
												<!-- Row Ends -->
												
											</div>
											<!-- Left Ends -->
											
											<!-- Right Starts -->
											<div class="cfmain-tbl-right">
												<a title="Edit" href="javascript:void(0);" class="p-edt-btn-n confilter" data-id="<?php echo $row->filter_title_id;?>" act="edit" data-cat="<?php echo $categoryData->cat_id;?>" data-did="<?php echo $con->filter_detail_id;?>"></a>
												<a title="Delete" href="javascript:void(0)" class="p-del-btn-n consignledelete" data-id="<?php echo $row->filter_title_id;?>" data-cat="<?php echo $categoryData->cat_id;?>" data-did="<?php echo $con->filter_detail_id;?>"></a>
											</div>
											<!-- Right Ends -->
											
										</div>
										<!-- Custom Revisable Table Structure Ends -->
										
										<?php }
										}
										?>
                                        
										
										<div class="btn01 cta-row">
                                        	
                                            <!-- Is Required Row Starts -->
                                            <div class="nanylist-row isreq-set">
                                                <div class="nany-chkrp">
                                                    <input <?php if($row->is_required == '1' ){ echo 'checked="checked"';}?> type="checkbox" name="requiredfld" value="" data-id="<?php echo $row->filter_title_id;?>" class="requirefield" />
                                                </div>
                                                <div class="nany-chktxt">Is required filter?</div>
                                            </div>
                                            <!-- Is Required Row Ends -->
                                            
											<a href="javascript:void(0);" id="add-row" class="add-more-record confilter" data-id="<?php echo $row->filter_title_id;?>" act="addm" data-cat="<?php echo $categoryData->cat_id;?>" data-did="0">Add More</a>
                                            
										</div>
										
										
									</div>
									
								</div>
								<!-- Repeatable Structure Ends -->
								
								
							</div>
						</div>
						
					</div>
                    <?php } ?>
				<?php }
				}else{?>
                <div class="conbox-row clhide" >
						
						<div class="mu-contnt-lfLbl ad-size">
							<h2>Filter Information</h2>
							<p>Set up the filter title and details. </p>
						</div>
						
						
                        
						
					</div>
                <?php } ?>