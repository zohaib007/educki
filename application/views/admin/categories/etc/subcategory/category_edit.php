<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title; ?></title>
<link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php");?>
<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
<script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>


<!-- tinymce configration start here.-->
<script>
var base_url='<?php echo base_url();?>';
var ser='<?php echo $_SERVER['DOCUMENT_ROOT'];?>educki/resources/tiny_upload';
</script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.js"></script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>tiny/common.js"></script>
<!-- tinymce configration end here.-->


</head>

<body>
<div class="container_p"> 
    
    <!-- Dashboard Left Side Begins Here -->
    <div class="left_wrp">
        <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
    </div>
    <!-- Dashboard Left Side Ends Here -->
    
    <div class="right-rp"> 
        
        <!-- Top Green Bar Section Begins Here -->
        <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
        <!-- Top Green Bar Section Ends Here --> 
        
        <!-- Section Starts -->
        <div class="mu-contnt-wrp">
            <div class="mu-contnt-hdng">
                <h1>Edit Category</h1>
            </div>
            
            <!-- Bread Crumbs Starts -->
            <div class="n-crums">
                <ul>
                    <li> <a href="<?php echo base_url('admin/categories'); ?>">Categories</a> </li>
                    <li>
                        <div class="crms-sep">&gt;</div>
                    </li>
                    <li> <a href="<?=base_url('admin/categories/sub_category/'.$main_cat_data['cat_id'])?>"><?=$main_cat_data['cat_name']?></a> </li>
                    <li>
                        <div class="crms-sep">&gt;</div>
                    </li>
                    <li> <a href="javascript:void(0);">Edit Subcategory (<?=$cat_data['cat_name']?>)</a> </li>
                </ul>
            </div>
            <!-- Bread Crumbs Ends --> 
            
            <?php echo form_open_multipart('', array('name'=>'edit-category')); ?> 
            
            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Category</h2>
                    <p>Manage your category.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Name*</label>
                                <input type="text" name="name" value="<?=set_value('name')!= ''?set_value('name'):$cat_data['cat_name']?>" placeholder="" />
                            </div>
                        </div>
                        <div class="error"> <?php echo form_error('name')?> </div>
                        <div class="mu-flds-wrp">
                            <label class="lone-set">Description*</label>
                            <textarea class="myeditor" name="cat_desc" ><?=set_value('cat_desc')?set_value('cat_desc'):$cat_data['cat_desc']?></textarea>
                        </div>
                        <div class="error"> <?php echo form_error('cat_desc')?> </div>
                    </div>
                </div>
            </div>
            
            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Image</h2>
                    <p> Upload an image for this blog. </p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds frmFlds-new">
                                <div class="my-form">
                                    <label for="box1">Image*</label>
                                    <div class="fileUp img-er">
                                        <p class="text-box my-form"><img src="<?=base_url('resources/cat_image/'.$cat_data['cat_image'])?>" width="auto" height="80px" /></p>
                                        <div class="fileUp">
                                            <input type="file" name="file1" value="<?=$cat_data['cat_image']?>" accept="image/*" id="box1" onchange="javascript: document.getElementById('box1-txt').value = this.value" />
                                            <label for="box1"></label>
                                            <input type="text" id="box1-txt" value="<?=$cat_data['cat_image']?>" name="hidden_file1" readonly="true" />
                                            <div class="error"><?=form_error('hidden_file1')?><?=$this->session->flashdata('msg_err0')?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds frmFlds-new">
                                <div class="my-form">
                                    <label for="box2">Thumb Image*</label>
                                    <div class="fileUp img-er">
                                        <p class="text-box my-form"><img src="<?=base_url('resources/cat_image/'.$cat_data['cat_image_thumb'])?>" width="auto" height="80px" /></p>
                                        <div class="fileUp">
                                            <input type="file" name="file2" value="<?=$cat_data['cat_image_thumb']?>" accept="image/*" id="box2" onchange="javascript: document.getElementById('box2-txt').value = this.value" />
                                            <label for="box2"></label>
                                            <input type="text" id="box2-txt" value="<?=$cat_data['cat_image_thumb']?>" name="hidden_file2" readonly="true" />
                                            <div class="error"><?=form_error('hidden_file2')?><?=$this->session->flashdata('msg_err1')?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Meta Information</h2>
                    <p> Set up the category title, meta description. These helps define how this category shows up on search engines. </p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Meta title</label>
                                <input type="text" value="<?=set_value('meta_title')?set_value('meta_title'):$cat_data['meta_title']?>" name="meta_title" />
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Meta keywords</label>
                                <input type="text" value="<?=set_value('meta_keyword')?set_value('meta_keyword'):$cat_data['meta_keyword']?>" name="meta_keyword" />
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Meta description</label>
                                <textarea class="text_meta" name="meta_desc"><?=set_value('')?set_value('meta_desc'):$cat_data['meta_desc']?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Box along with label section ends here -->
            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Status</h2>
                    <p>Manage your category status.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Status*</label>
                                <select name="cat_status">
                                    <option value="" >Select</option>
                                    <option value="1" <?=set_select('cat_status','1',$cat_data['cat_status']==1?TRUE:FALSE)?>>Active</option>
                                    <option value="0" <?=set_select('cat_status','0',$cat_data['cat_status']==0?TRUE:FALSE)?>>Inactive</option>
                                </select>
                                
                            </div>
                        </div>
                        <div class="error"><?=form_error('cat_status')?></div>
                    </div>
                </div>
            </div>
            
            <!-- Box wihout label section begins here -->
            <div class="mu-fld-sub">
                <input type="submit" value="Submit" />
                <a href="<?=base_url('admin/categories/sub_category/'.$main_cat_data['cat_id'])?>">cancel</a>
            </div>
            <!-- Box wihout label section ends here --> 
            
            <?php echo form_close(); ?> </div>
        <!-- Section Ends --> 
        
    </div>
</div>
</body>


</html>