<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title; ?></title>
<link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php");?>
<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
<script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
<?php include(ADMIN_INCLUDE_PATH."includes/summernote.php"); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<script>
$(function() {
  // Open
  $('[data-popup-open]').on('click', function(e) {
    var targeted_popup_class = $(this).attr('data-popup-open');    
    $('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);    
    e.preventDefault();
  });
  
  // Close
  $('[data-popup-close]').on('click', function(e) {
    var targeted_popup_class = $(this).attr('data-popup-close');
    $('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
    e.preventDefault();
  }); 
});
</script>


</head>

<body>
<div class="container_p"> 
    
    <!-- Dashboard Left Side Begins Here -->
    <div class="left_wrp">
        <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
    </div>
    <!-- Dashboard Left Side Ends Here -->
    
    <div class="right-rp"> 
        
        <!-- Top Green Bar Section Begins Here -->
        <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
        <!-- Top Green Bar Section Ends Here --> 
        
        <!-- Section Starts -->
        <div class="mu-contnt-wrp">
            <div class="mu-contnt-hdng">
                <h1>Add Category</h1>
            </div>
            
            <!-- Bread Crumbs Starts -->
            <div class="n-crums">
                <ul>
                    <li> <a href="<?php echo base_url('admin/categories'); ?>">Categories</a> </li>
                    <li>
                        <div class="crms-sep">&gt;</div>
                    </li>
                    <li> <a href="javascript:void(0);">Add Category</a> </li>
                </ul>
            </div>
            <!-- Bread Crumbs Ends --> 
            
            <?php echo form_open_multipart('', array('name'=>'add-category')); ?> 
            
            
            <!-- Filter Section Starts -->
            <div class="mu-contnt-outer">
            	
            	<!-- Add Structure Starts -->
                <div class="ntop-btn-row">
                    <div class="ntop-btn">
                        <a href="javascript:void(0);" title="Add Filter" data-popup-open="popup-1">Add Filter</a>
                    </div>
                    <div class="ntop-btn">
                        <a href="javascript:void(0);" title="Add Category Filter" data-popup-open="popup-2">Add Category Filter</a>
                    </div>
                </div>
                <!-- Add Structure Ends -->
                
                <div class="conbox-row">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Filter Information</h2>
                        <p>Set up the filter title and details. </p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            
                            <!-- Repeatable Structure Starts -->
                            <div class="add_cat_filters">
                                <input type="hidden" name="total_filter" value="" />
                                
                                <div class="catFilters filter-mwrap">
                                    
                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds_long">
                                            <label>Filter Title</label>
                                            <input type="text" value="" class="filterTitle" name="filter_title" />
                                        </div>
                                    </div>
                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds_long">
                                            <label>Filter Details</label>
                                            <textarea class="text_meta" name="filter_details"></textarea>
                                        </div>
                                    </div>
                                    
                                    <!-- Remove Structure Starts -->
                                    <div class="nfull-row ar-space2">
                                        <a href="javascript:void(0);" title="Remove" data-filter="filter_no" class="addremove-lk remove_filter">Remove</a>
                                    </div>
                                    <!-- Remove Structure Ends -->
                                    
                                    <div class="cat-sptr"></div> <!-- Seperator Line -->
                                    
                                    
                                </div>
                                
                            </div>
                            <!-- Repeatable Structure Ends -->
                            
                            
                        </div>
                    </div>
                </div>
                
                <div class="conbox-row">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2></h2>
                        <p></p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            
                            <!-- Repeatable Structure Starts -->
                            <div class="add_cat_filters">
                                
                                <div class="catFilters filter-mwrap">
                                    
                                    <div class="custom-nfrm">
                                        <!-- <h1>Add Category Filter</h1> -->
                                        <div class="scrollable">
                                            <div class="my-form-record">
                                                <div class="mu-frmFlds_long">
                                                    
                                                    <div class="catFilters filter-mwrap">
                                                        
                                                        <div class="mu-flds-wrp">
                                                            <div class="mu-frmFlds_long">
                                                                <label>Category Filter Title</label>
                                                                <input type="text" value="" class="filterTitle" name="cat-filter" />
                                                            </div>
                                                        </div>
                                                        
                                                        <!-- All Titles Structure Starts -->
                                                        <div class="nfilter-wrap">
                                                            
                                                            <!-- Repeatable Section Starts -->
                                                            <div class="mu-flds-wrp">
                                                                <div class="mu-frmFlds_long">
                                                                    <label>Filter Title</label>
                                                                    <div class="custom-fld">
                                                                        <div class="cus-num">1</div>
                                                                        <input type="text" value="" class="filterTitle medium-txt2" name="cat-filter-title" />
                                                                        <div class="nadd-btn minus" style="display:none;">
                                                                            <a href="#"></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Repeatable Section Ends -->
                                                            
                                                            <!-- Repeatable Section Starts -->
                                                            <div class="mu-flds-wrp">
                                                                <div class="mu-frmFlds_long">
                                                                    <label>Category Filter Title</label>
                                                                    <div class="custom-fld">
                                                                        <div class="cus-num">2</div>
                                                                        <input type="text" value="" class="filterTitle medium-txt2" name="cat-filter-title" />
                                                                        <div class="nadd-btn minus" style="display:none;">
                                                                            <a href="#"></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Repeatable Section Ends -->
                                                            
                                                            <div class="btn01 ncat-btn" style="display:none;">
                                                                <a href="javascript:void(0);" id="add-row" class="add-more-record">Add</a> 
                                                            </div>
                                                            
                                                        </div>
                                                        <!-- All Titles Structure Ends -->
                                                        
                                                        <!-- Titles detial Structure Starts -->
                                                        <div class="nfilter-detail">
                                                            
                                                            <!-- Repeatable Section Starts -->
                                                            <div class="cus-repeatsec">
                                                                
                                                                <div class="cat-sptr">
                                                                    <div class="step-num">1</div>
                                                                </div> <!-- Seperator Line -->
                                                                
                                                                <div class="mu-flds-wrp">
                                                                    <div class="mu-frmFlds_long">
                                                                        <label>Filter Title</label>
                                                                        <input type="text" value="" class="filterTitle" name="cat-filter" />
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="mu-flds-wrp">
                                                                    <div class="mu-frmFlds_long">
                                                                        <label>Filter Details</label>
                                                                        <textarea class="text_meta" name="filter_details"></textarea>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                            <!-- Repeatable Section Ends -->
                                                            
                                                            <!-- Repeatable Section Starts -->
                                                            <div class="cus-repeatsec">
                                                                
                                                                <div class="cat-sptr">
                                                                    <div class="step-num">2</div>
                                                                </div> <!-- Seperator Line -->
                                                                
                                                                <div class="mu-flds-wrp">
                                                                    <div class="mu-frmFlds_long">
                                                                        <label>Filter Title</label>
                                                                        <input type="text" value="" class="filterTitle" name="cat-filter" />
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="mu-flds-wrp">
                                                                    <div class="mu-frmFlds_long">
                                                                        <label>Filter Details</label>
                                                                        <textarea class="text_meta" name="filter_details"></textarea>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                            <!-- Repeatable Section Ends -->
                                                            
                                                        </div>
                                                        <!-- Titles detial Structure Ends -->
                                                        
                                                    </div>
                                                    
                                                </div>
                                                <div class="btn01 btn-setting">
                                                    <a href="javascript:void(0);" id="add-row" class="add-more-record">Remove</a> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            <!-- Repeatable Structure Ends -->
                            
                            
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- Filter Section Ends -->
            
            <!-- Submit Button Section Starts -->
            <div class="mu-fld-sub">
                <input type="submit" value="Submit" />
                <a href="<?=base_url()?>admin/categories">cancel</a>
            </div>
            <!-- Submit Button Section Ends -->
            
            <!-- Add Filter Popup Starts -->
            <div class="popup" data-popup="popup-1">
                <div class="popup-inner">
                    
                    <div class="pricing">
                        <h1>Add Filter</h1>
                        <div class="my-form-record">
                            <div class="mu-frmFlds_long" id="popup-data">
                                
                                <div class="catFilters filter-mwrap">
                                    
                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds_long">
                                            <label>Filter Title</label>
                                            <input type="text" value="" class="filterTitle" name="filter_title" />
                                        </div>
                                    </div>
                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds_long">
                                            <label>Filter Details</label>
                                            <textarea class="text_meta" name="filter_details"></textarea>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            <div class="btn01 btn-setting" style="display:none;">
                                <a href="javascript:void(0);" id="add-row" class="add-more-record">Add</a> 
                            </div>
                        </div>
                    </div>
                    
                    <a href="#" class="popup-close" data-popup-close="popup-1">x</a>
                    
                </div>
            </div>
            <!-- Add Filter Popup Ends -->
            
            <!-- Add Category Filter Popup Starts -->
            <div class="popup" data-popup="popup-2">
                <div class="popup-inner">
                    
                    <div class="custom-nfrm">
                        <h1>Add Category Filter</h1>
                        <div class="scrollable">
                            <div class="my-form-record">
                                <div class="mu-frmFlds_long" id="popup-data">
                                    
                                    <div class="catFilters filter-mwrap">
                                        
                                        <div class="mu-flds-wrp">
                                            <div class="mu-frmFlds_long">
                                                <label>Category Filter Title</label>
                                                <input type="text" value="" class="filterTitle" name="cat-filter" />
                                            </div>
                                        </div>
                                        
                                        <!-- All Titles Structure Starts -->
                                        <div class="nfilter-wrap">
                                            
                                            <!-- Repeatable Section Starts -->
                                            <div class="mu-flds-wrp">
                                                <div class="mu-frmFlds_long">
                                                    <label>Filter Title</label>
                                                    <div class="custom-fld">
                                                        <div class="cus-num">1</div>
                                                        <input type="text" value="" class="filterTitle medium-txt" name="cat-filter-title" />
                                                        <div class="nadd-btn minus">
                                                            <a href="#"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Repeatable Section Ends -->
                                            
                                            <!-- Repeatable Section Starts -->
                                            <div class="mu-flds-wrp">
                                                <div class="mu-frmFlds_long">
                                                    <label>Category Filter Title</label>
                                                    <div class="custom-fld">
                                                        <div class="cus-num">2</div>
                                                        <input type="text" value="" class="filterTitle medium-txt" name="cat-filter-title" />
                                                        <div class="nadd-btn minus">
                                                            <a href="#"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Repeatable Section Ends -->
                                            
                                            <div class="btn01 ncat-btn">
                                                <a href="javascript:void(0);" id="add-row" class="add-more-record">Add</a> 
                                            </div>
                                            
                                        </div>
                                        <!-- All Titles Structure Ends -->
                                        
                                        <!-- Titles detial Structure Starts -->
                                        <div class="nfilter-detail">
                                            
                                            <!-- Repeatable Section Starts -->
                                            <div class="cus-repeatsec">
                                                
                                                <div class="cat-sptr">
                                                    <div class="step-num">1</div>
                                                </div> <!-- Seperator Line -->
                                                
                                                <div class="mu-flds-wrp">
                                                    <div class="mu-frmFlds_long">
                                                        <label>Filter Title</label>
                                                        <input type="text" value="" class="filterTitle" name="cat-filter" />
                                                    </div>
                                                </div>
                                                
                                                <div class="mu-flds-wrp">
                                                    <div class="mu-frmFlds_long">
                                                        <label>Filter Details</label>
                                                        <textarea class="text_meta" name="filter_details"></textarea>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <!-- Repeatable Section Ends -->
                                            
                                            <!-- Repeatable Section Starts -->
                                            <div class="cus-repeatsec">
                                                
                                                <div class="cat-sptr">
                                                    <div class="step-num">2</div>
                                                </div> <!-- Seperator Line -->
                                                
                                                <div class="mu-flds-wrp">
                                                    <div class="mu-frmFlds_long">
                                                        <label>Filter Title</label>
                                                        <input type="text" value="" class="filterTitle" name="cat-filter" />
                                                    </div>
                                                </div>
                                                
                                                <div class="mu-flds-wrp">
                                                    <div class="mu-frmFlds_long">
                                                        <label>Filter Details</label>
                                                        <textarea class="text_meta" name="filter_details"></textarea>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <!-- Repeatable Section Ends -->
                                            
                                        </div>
                                        <!-- Titles detial Structure Ends -->
                                        
                                    </div>
                                    
                                </div>
                                <div class="btn01 btn-setting" style="display:none;">
                                    <a href="javascript:void(0);" id="add-row" class="add-more-record">Add</a> 
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <a href="#" class="popup-close" data-popup-close="popup-2">x</a>
                    
                </div>
            </div>
            <!-- Add Category Filter Popup Ends -->
            
            <?php echo form_close(); ?>
            
            </div>
        <!-- Section Ends --> 
        
    </div>
</div>
 
</body>

<script>
$(document).ready(function(e) {
	
	/* ADD CATEGORY FILTER START */
    $(document).on('click', '.add_more', function(f){		
		var total_filter = $("input[name='total_filter']").val();
		var html = '<div class="catFilters filter_no'+total_filter+' filter-mwrap" ><div class="mu-flds-wrp"><div class="mu-frmFlds_long"><label>Filter Title</label><input type="text" value="" class="filterTitle" name="filter_title'+total_filter+'" /></div></div><div class="mu-flds-wrp"><div class="mu-frmFlds_long"><label>Filter Details</label><textarea class="text_meta filterArea" name="filter_details'+total_filter+'"></textarea></div></div><!-- Remove Structure Starts --><div class="nfull-row ar-space2"><a href="javascript:void(0);" title="Remove" data-filter="filter_no'+total_filter+'" class="addremove-lk remove_filter">Remove</a></div><!-- Remove Structure Ends --><div class="cat-sptr"></div></div></div>';
		$('.add_cat_filters').append(html);
		$("input[name='total_filter']").val((parseInt(total_filter)+1));
	});
	/* ADD CATEGORY FILTER END */
	
// :)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:) \\

	/* REMOVE CATEGORY FILTER START */
	$(document).on('click', '.remove_filter', function(g){
		var total_filter = $("input[name='total_filter']").val();
		var filter_no = $(this).attr('data-filter');
		//$('.'+filter_no).fadeOut("fast", function(){
			$('.'+filter_no).remove();
			$("input[name='total_filter']").val((parseInt(total_filter)-1));
			var all_filters = $('.add_cat_filters').find('.catFilters');
			
			$('.catFilters').each(function(index){
				$(this).removeClass();
				$(this).addClass("catFilters filter-mwrap filter_no"+index);
				$(this).find('.filterTitle').attr('name','filter_title'+index);
				$(this).find('.filterArea').attr('name','filter_details'+index);
				$(this).find('.remove_filter').attr('data-filter','filter_no'+index);
			});
		//});
	});
	/* REMOVE CATEGORY FILTER END */
});
</script>

</html>