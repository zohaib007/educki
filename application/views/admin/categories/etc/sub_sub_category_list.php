<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Categories</title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />

<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>

<!--<link rel="stylesheet" href="<?=base_url()?>js/tablesort/style.css" type="text/css" media="all" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/tablesort/jquery-latest.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/tablesort/jquery.tablesorter.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.tablednd.0.7.min.js"></script>

<script>
$(function () {
	$('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');
	//setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
	$('.message .close').hover(
		function() { $(this).addClass('hover'); },
		function() { $(this).removeClass('hover'); }
	);
		
	$('.message .close').click(function() {
		$(this).parent().fadeOut('slow', function() { $(this).remove(); });
	});
});


    
</script>

<style>
#display_tr {
	display: table-row !important;
}
</style>

</head>

<body>
<div class="container"> 
  
  <!-- Dashboard Left Side Begins Here -->
  <div class="left_wrp">
    <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
  </div>
  <!-- Dashboard Left Side Ends Here -->
  
  <div class="right-rp"> 
    
    <!-- Top Green Bar Section Begins Here -->
    <?php
include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
?>
    <!-- Top Green Bar Section Ends Here -->
   
    <div class="col-continer">
      <?php	

if($this->session->flashdata('msg') != "")
{
echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
}
?>
      <div class="col-3 cat-box dragable-cat">
        <div class="contntTop-row">
          <div class="actvity-hd">
            <div class="act-tab b-ad-tb">
                <table cellspacing="0" border="0">
                    <colgroup>
                    <col width="35%" />
                    <col width="35%" />
                    <col width="42%" />
                    <!-- <col width="3%" />
                        <col width="3%" />
                        <col width="3%" />-->
                    </colgroup>
                    <tr>
                        <td class="no-bdr">
                        	<div class="cat-tbl-tp-lf">
                                <h1>Categories</h1> <span class="n-of-cat"><?=$total_records?></span>
							</div>
						</td>
                        <td class="no-bdr">&nbsp;</td>
                        <td  class="no-bdr" align="right">
                            <a href="<?= base_url() ?>admin/categories/add_category/" class="pro-addNew"> Add Category </a>
                        </td>
                    </tr>
                </table>
              
            </div>
          </div>
          <div class="b-prd-tbl align-top">
          	            
            <!-- New Lisitng Structure Starts -->
            <div class="ul-listing-wrap">
                <div id="debugArea" style="float: right">&nbsp;</div>
                <!-- Listing Header Starts -->
                <div class="ul-lihdr">
                    
                    <div class="ul-liman-rp ul-col-one">
                        <h3>Category Title</h3>
                    </div>
                    
                    <div class="ul-liman-rp ul-col-two">
                        <h3>Status</h3>
                    </div>
                    
                    <div class="ul-liman-rp ul-col-three">
                        <h3>Actions</h3>
                    </div>
                    
                </div>
                <!-- Lisiting Header Ends -->
                
                <!-- Listing Bottom Section Starts -->
                <div class="ul-libtm">
                    
                    <div class="sort-wrap">
                        
                        <ul class="sortable">
                        	
                        	<?php
							if(count($list) > 0) { 
							for ($n = 0; $n < count($list); $n++) {
							?>
                            
                            <li class="p" id="<?=$list[$n]['cat_id']?>" sid="<?php echo $list[$n]['cat_sort'];?>">
                                <!-- Row Starts -->
                                <div class="ul-lirow">
                                    
                                    <div class="ul-limain-list">
                                        
                                        <div class="ul-liman-rp ul-col-one">
                                        	<?=limited_text($list[$n]['cat_name'] , 40) ?>
                                        </div>
                                        
                                        <div class="ul-liman-rp ul-col-two">
                                        	<?php
											if($list[$n]['cat_status'] == 1)
											{
												echo "Active";
											}
											else
											{
												echo "Inactive";
											}
											?>
                                        </div>
                                        
                                        <div class="ul-liman-rp ul-col-three">
                                            <div class="action-btn">
                                            	<a href="<?= base_url() ?>admin/categories/edit_category/<?= $list[$n]['cat_id'] ?>" title="Edit" class="tik-cross-btns p-edt-btn-n"></a>
                                            </div>
                                            <div class="action-btn">
                                            	<a href="<?= base_url() ?>admin/categories/delete_category/<?= $list[$n]['cat_id'] ?>" title="Delete" class="tik-cross-btns p-del-btn-n" onclick="return confirm('Are you sure you want to delete selected item(s)?');"></a>
                                            </div>
                                            <div class="action-btn">
                                            	<a href="<?=base_url('admin/categories/filters/'.$list[$n]['cat_id'] )?>" title="Filters Listing" class="tik-cross-btns dtlbtn-n"></a>
                                            </div>
                                            <div class="action-btn" style="display:none;">
                                            	<a href="#" title="View Detail" class="tik-cross-btns p-srch-btn-n"></a>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                <!-- Row Ends -->
                            </li>
                            
                            <?php
								}
							}else { 
								echo "<div class='not-found'><strong>No category found.</strong></div>";
							}
							?>
                            
                        </ul>
                        
                        <div class="pagination-row" style="width:100%">
							<?=$paginglink ?>
                        </div>
                        
                    </div>
                    
                </div>
                <!-- Listing Bottom Section Ends -->
                
            </div>
            <!-- New Lisitng Structure Ends -->
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--
<script>
$(document).ready(function() {
$('table').filterTable({ // apply filterTable to all tables on this page
quickList: [] // add some shortcut searches
});
});
</script> 
<script src="<?= base_url() ?>/js/jquery.filtertable.min.js"></script>
-->

<script src="<?=base_url()?>/js/uisortable/jquery-ui.js" type="text/javascript"></script>
<script type="application/javascript">
$(".sortable").sortable({
		revert: true,
		stop: function( event, ui ) {
			//console.log(event);
			var lis = $(this).find('.p');
			//console.log(lis);
			var formData = [];
			var i = 1;
			$.each(lis,function(){
				formData.push({
					'id' : $(this).attr('id'),
					'ord' : i,
				});
				i++;
			});
			//console.log(formData);
			$.ajax({
				type : 'POST',
				url  :  base_url+'admin/categories/sorting/',
				data : {data : formData, <?php echo $this->security->get_csrf_token_name(); ?>:'<?php echo $this->security->get_csrf_hash(); ?>'},
				success: function(data){
					//console.log(data);	
				},
			});
			//console.log(formData)	
		},
	});
	//$(".sortable").disableSelection();
 </script>
</body>
</html>