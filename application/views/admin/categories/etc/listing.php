<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title;?></title>
<link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
<?php $this->load->view("includes/js.php");?>

</head>

<body>
<div class="container"> 
    <!-- Dashboard Left Side Begins Here -->
    <div class="left_wrp">
        <?php $this->load->view("includes/dash-left.php");?>
    </div>
    <!-- Dashboard Left Side Ends Here -->

    <div class="right-rp"> 
        <!-- Top Green Bar Section Begins Here -->
       <?php $this->load->view("includes/top_green_bar.php"); ?>
        <!-- Top Green Bar Section Ends Here -->
        <div class="col-continer">
            <div class="col-3 cat-box">
                <div class="contntTop-row">
                    <div class="actvity-hd nobdr">
                    	
                        <!-- Top Horizontal Navigation Starts -->
                        <div class="top-nav-wrap">
                            <div class="topnav-list">
                                
                                <a title="Products" class="" href="<?=base_url('products/')?>">Products</a>
                                <span>|</span>
                                <a title="Categories" class="active" href="<?php echo base_url('categories/en/1');?>" >Categories</a>
                                <span>|</span>
                                <a title="Translate" class="" href="<?php echo base_url('product_translate');?>">Translate</a>
                            </div>
                         </div>
                    	<!-- Top Horizontal Navigation Ends -->
                        
                        <div class="act-tab b-ad-tb">
                            <table cellspacing="0" border="0">
                                <colgroup>
                                    <col width="61%" />
                                    <col width="20%" />
                                    <col width="19%" />
                                </colgroup>
                                <tr>
                                	
                                    <td class="no-bdr">
                                    	<div class="cat-tbl-tp-lf" style="display:none;">
                                            <h1>Category Management</h1>
                                        </div>
                                    </td>
                                    
                                    <td class="no-bdr">
                                    	<select name="site-language" class="selLanguage">
                                            <option value="">Select Language</option>
                                            <?php if($languages->num_rows()>0){
												foreach($languages->result() as $l){?>
                                            <option value="<?php echo $l->iso;?>" <?php if($l->iso == $lang){ echo 'selected="selected"';}?>><?php echo $l->name;?></option>
                                            <?php } } ?>
                                        </select>
                                    </td>
                                    
                                    <td align="right">
										<?php if($this->session->userdata('admin_type') == 1) { ?>
                                        <?php if($lang == 'en'){?>
                                        	<a href="<?=base_url()?>category_add/en" class="pro-addNew" title="Create a Category">Create a Category</a>
                                        <?php } ?>
                                        <?php } ?>
                                    </td>
                                    
                                </tr>
                            </table>
                        </div>
                        
                        <?php 
						if($this->session->flashdata('msg') != "") {
								echo '<div id="message" class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
						}
						?>
                        
                        <div class="b-srch-bl-sec">
                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                <colgroup>
                                    <col width="10%" />
                                    <col width="18%" />
                                    <col width="20%" />
                                    <col width="10%" />
                                    <col width="42%" />
                                </colgroup>
                                <tr>
                                	
									<?php echo form_open_multipart('', array('name'=>"myForm")); ?>
                                    <td align="center">
                                        <span>Search by</span>
                                    </td>
                                    <td align="center">
                                        <select name="srchOp" class="catName">
                                            <option value="1" >Category Title</option>
                                        </select>
                                    </td>
                                    <td align="center">
                                        <input type="text" name="searchText" class="catName" value="<?php echo $txt;?>" />
                                    </td>
                                    <td align="center">
                                        <input type="submit" value="" class="catSrch" title="Search" />
                                    </td>
                                    <?php echo form_close();?>
                                    
                                    <td align="left">&nbsp;</td>
                                    
                                </tr>
                            </table>
                        </div>
                        
                        <!-- New Lisitng Structure Starts -->
                        <div class="ul-listing-wrap">
                            
                            <!-- Listing Header Starts -->
                            <div class="ul-lihdr">
                            	
                            	<div class="ul-liman-rp ul-col-one">
                                	<h3>Category Title</h3>
                                </div>
                                
                            	<div class="ul-liman-rp ul-col-two">
                                	<h3><?php if($lang == 'en'){?>Status<?php }else{?> &nbsp; <?php } ?></h3>
                                </div>
                                
                            	<div class="ul-liman-rp ul-col-three">
                                	<h3><?php if($this->session->userdata('admin_type') == 1) { echo 'Actions';}else{ echo 'Action';}?></h3>
                                </div>
                                
                            </div>
                            <!-- Lisiting Header Ends -->
                            
                            <!-- Listing Bottom Section Starts -->
                            <div class="ul-libtm">
                            	
                                <div class="sort-wrap">
                                    
                                    <ul class="sortable" id="p">
                                        <?php if($data->num_rows()>0){
											foreach($data->result() as $row){
												
												$subCategories = getChildCategories($row->mId,$lang);
												?>
                                        <li class="p" id="<?php echo $row->Id;?>" sid="<?php echo $row->SortOrder;?>">
                                            <!-- Row Starts -->
                                            <div class="ul-lirow">
                                                
                                                <div class="ul-limain-list">
                                                    
                                                    <div class="ul-liman-rp ul-col-one">
                                                    	<?php 
												
												if($subCategories->num_rows()>0){
												?>
                                                        <div class="ul-ico-sub"> <!-- ul-ico-empty -->
                                                        
                                                            <a href="javascript:void(0);"></a>
                                                            
                                                        </div>
                                                        <?php }?>
                                                        <a href="<?php echo $url;?>category_edit/<?php echo $lang;?>/<?php echo $row->mId;?>"><?php echo $row->Name;?></a>
                                                    </div>
                                                    
                                                    <div class="ul-liman-rp ul-col-two">
                                                    <?php if($lang == 'en'){?>
                                                       <?php if($row->IsVisible == 0){
														   echo 'Inactive';
													   }else{
														   echo 'Active';
													   }?>
                                                       <?php }else{?>
                                                       &nbsp;
                                                       <?php } ?>
                                                    </div>
                                                    <?php if($this->session->userdata('admin_type') == 1) { ?>
                                                    <div class="ul-liman-rp ul-col-three">
                                                        <div class="action-btn">
                                                            <a href="<?php echo $url;?>category_edit/<?php echo $lang;?>/<?php echo $row->mId;?>" title="Edit" class="tik-cross-btns p-edt-btn-n"></a>
                                                        </div>
                                                        <div class="action-btn">
                                                            <a href="<?php echo $url;?>category_delete/<?php echo $lang;?>/<?php echo $row->mId;?>" title="Delete" class="tik-cross-btns p-del-btn-n" onclick="return confirm('Are you sure you want to delete selected item(s)?');"></a>
                                                        </div>
                                                    </div>
                                                    <?php }else{?>
                                                    	<div class="ul-liman-rp ul-col-three">
                                                        <div class="action-btn">
                                                            <a href="<?php echo $url;?>category_preview/<?php echo $lang;?>/<?php echo $row->mId;?>" title="View Detail" class="tik-cross-btns p-srch-btn-n"></a>
                                                        </div>
                                                       
                                                    </div>
                                                    <?php }?>
                                                </div>
                                                <?php 
												
												if($subCategories->num_rows()>0){
												?>
                                                <!-- Show Hide Container Starts -->
                                                <div class="ul-lisub-list" style="display:none;">
                                                	
                                                    <ul class="sortable">
                                                        <?php foreach($subCategories->result() as $category){?>
                                                        <li class="c" id="<?php echo $category->Id;?>" sid="<?php echo $category->SortOrder;?>">
                                                            <!-- Sub Row Starts -->
                                                            <div class="ul-lisub-lrow">
                                                                <div class="ul-liman-rp ul-col-one">
                                                                    <div class="ul-ico-subrep"></div> <!-- Sub Icon -->
                                                                    <a href="<?php echo $url;?>category_edit/<?php echo $lang;?>/<?php echo $category->mId;?>"><?php echo $category->Name;?></a>
                                                                </div>
                                                                
                                                                <div class="ul-liman-rp ul-col-two">
                                                                 <?php if($lang == 'en'){?>
                                                                    <?php if($category->IsVisible == 0){
														   echo 'Inactive';
													   }else{
														   echo 'Active';
													   }?>
                                                        <?php }else{?>
                                                       &nbsp;
                                                       <?php } ?>
                                                                </div>
                                                                 <?php if($this->session->userdata('admin_type') == 1) { ?>
                                                                <div class="ul-liman-rp ul-col-three">
                                                                    <div class="action-btn">
                                                                        <a href="<?php echo $url;?>category_edit/<?php echo $lang;?>/<?php echo $category->mId;?>" title="Edit" class="tik-cross-btns p-edt-btn-n"></a>
                                                                    </div>
                                                                    <div class="action-btn">
                                                                        <a href="<?php echo $url;?>category_delete/<?php echo $lang;?>/<?php echo $category->mId;?>" title="Delete" class="tik-cross-btns p-del-btn-n" onclick="return confirm('Are you sure you want to delete selected item(s)?');"></a>
                                                                    </div>
                                                                </div>
                                                                <?php }else{?>
                                                                	 <div class="ul-liman-rp ul-col-three">
                                                                    <div class="action-btn">
                                                                        <a href="<?php echo $url;?>category_preview/<?php echo $lang;?>/<?php echo $category->mId;?>" title="View Detail" class="tik-cross-btns p-srch-btn-n"></a>
                                                                    </div>
                                                                   
                                                                </div>
                                                                <?php } ?>
                                                            </div>
                                                            <!-- Sub Row Ends -->
                                                        </li>
                                                        <?php } ?>
                                                        
                                                   
                                                        
                                                    </ul>
                                                    
                                                </div>
                                                <!-- Show Hide Container Ends -->
                                                <?php } ?>
                                            </div>
                                            <!-- Row Ends -->
                                        </li>
                                        <?php } }else{ ?>
                                       <div class="not-found">No category found.</div>
                                       <?php } ?>
                                        
                                    </ul>
                                    
                            	</div>
                                
                            </div>
                            <!-- Listing Bottom Section Ends -->
                            
                            
                        </div>
                        <!-- New Lisitng Structure Ends -->
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
<script type="text/javascript">
var lang = '<?php echo $lang;?>'; 
</script>
<script src="<?=base_url()?>/resources/uisortable/jquery-ui.js" type="text/javascript"></script>
<script src="<?=base_url()?>/js/categorieslisting.js" type="text/javascript"></script>
</html>