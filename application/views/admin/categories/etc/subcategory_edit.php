<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title; ?></title>
<link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php");?>
<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
</head>

<body>
<div class="container_p"> 
    
    <!-- Dashboard Left Side Begins Here -->
    <div class="left_wrp">
        <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
    </div>
    <!-- Dashboard Left Side Ends Here -->
    
    <div class="right-rp"> 
        
        <!-- Top Green Bar Section Begins Here -->
        <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
        <!-- Top Green Bar Section Ends Here --> 
        
        <!-- Section Starts -->
        <div class="mu-contnt-wrp">
            <div class="mu-contnt-hdng">
                <h1>Edit Subcategory</h1>
            </div>
            
            <!-- Bread Crumbs Starts -->
            <div class="n-crums">
                <ul>
                    <li> <a href="<?php echo base_url('admin/categories'); ?>">Category Management</a> </li>
                     <li>
                        <div class="crms-sep">&gt;</div>
                    </li>
                    <li> <a href="<?=base_url('admin/categories/subcategory/'.$main_cat_data['cat_id'])?>"><?=$main_cat_data['cat_name']?></a> </li>
                    <li>
                        <div class="crms-sep">&gt;</div>
                    </li>
                    <li> <a href="javascript:void(0);">Edit Category (<?=$cat_data['cat_name']?>)</a> </li>
                </ul>
            </div>
            <!-- Bread Crumbs Ends --> 
            
            <?php echo form_open_multipart('', array('name'=>'edit-category')); ?> 
            
            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Subcategory</h2>
                    <p>Manage your category.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Subcategory Name*</label>
                                <input type="text" name="name" value="<?=set_value('name')?set_value('name'):$cat_data['cat_name']?>" placeholder="" />
                            </div>
                        </div>
                        <div class="error"> <?php echo form_error('name')?> </div>
                    </div>
                </div>
            </div>
            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Meta Information</h2>
                    <p> Set up the subcategory title, meta description. These helps define how this subcategory shows up on search engines. </p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Meta title</label>
                                <input type="text" value="<?=set_value('meta_title')?set_value('meta_title'):$cat_data['cat_meta_title']?>" name="meta_title" />
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Meta keywords</label>
                                <input type="text" value="<?=set_value('meta_keyword')?set_value('meta_keyword'):$cat_data['cat_meta_keyword']?>" name="meta_keyword" />
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Meta description</label>
                                <textarea class="text_meta" name="meta_desc"><?=set_value('')?set_value('meta_desc'):$cat_data['cat_meta_desc']?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Box along with label section ends here -->
            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Status</h2>
                    <p>Manage your subcategory status.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Status*</label>
                                <select name="cat_status">
                                    <option value="" >Select</option>
                                    <option value="1" <?=set_select('cat_status','1',$cat_data['cat_status']==1?TRUE:FALSE)?>>Active</option>
                                    <option value="0" <?=set_select('cat_status','0',$cat_data['cat_status']==0?TRUE:FALSE)?>>Inactive</option>
                                </select>
                                
                            </div>
                        </div>
                        <div class="error"><?=form_error('cat_status')?></div>
                    </div>
                </div>
            </div>
            
            <!-- Box wihout label section begins here -->
            <div class="mu-fld-sub">
                <input type="submit" value="Submit" />
                <a href="<?=base_url('admin/categories/subcategory/'.$main_cat_data['cat_id'])?>">cancel</a>
            </div>
            <!-- Box wihout label section ends here --> 
            
            <?php echo form_close(); ?> </div>
        <!-- Section Ends --> 
        
    </div>
</div>
</body>
</html>