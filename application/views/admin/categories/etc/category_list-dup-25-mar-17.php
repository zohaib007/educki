<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Categories</title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />

<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>

<!--<link rel="stylesheet" href="<?=base_url()?>js/tablesort/style.css" type="text/css" media="all" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/tablesort/jquery-latest.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/tablesort/jquery.tablesorter.js"></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.tablednd.0.7.min.js"></script>

<script>
$(function () {
	$('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');
	//setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
	$('.message .close').hover(
		function() { $(this).addClass('hover'); },
		function() { $(this).removeClass('hover'); }
	);
		
	$('.message .close').click(function() {
		$(this).parent().fadeOut('slow', function() { $(this).remove(); });
	});
});

$(document).ready(function() { 
    /*$("#mytable").tablesorter({ 
        // pass the headers argument and assing a object 
        headers: { 
            // assign the secound column (we start counting zero) 
           
			1: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            },
			2: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            },
			3: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            },
			4: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            }
        
        } 
    });*/
	
	// re-ordering table rows
	$("#mytable tr:even").addClass("alt");
	// Initialise the second table specifying a dragClass and an onDrop function that will display an alert
	$("#mytable").tableDnD({
	    onDragClass: "myDragClass",
	    onDrop: function(table, row) {
            var rows = table.tBodies[0].rows;
			//console.log(rows);
            //var debugStr = "Row dropped was "+row.id+". New order: ";
		    var formData = [];
            for (var i=0; i < rows.length; i++) {
				//debugStr += rows[i].id+" ";
				formData.push({
					'id' : rows[i].id,
					'ord' : i,
				});
            }
			
			
			$.ajax({
				type : 'POST',
				url  :  base_url+'admin/categories/sorting/',
				data : {data : formData, <?php echo $this->security->get_csrf_token_name(); ?>:'<?php echo $this->security->get_csrf_hash(); ?>'},
				success: function(data){
					//console.log(data);	
				},
			});
	        //$("#debugArea").html(debugStr);
	    },
		onDragStart: function(table, row) {
			//$("#debugArea").html("Started dragging row "+row.id);
			
		}
	}); 
	// end re-ordering
	
});

    
</script>

<style>
#display_tr {
	display: table-row !important;
}
</style>

</head>

<body>
<div class="container"> 
  
  <!-- Dashboard Left Side Begins Here -->
  <div class="left_wrp">
    <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
  </div>
  <!-- Dashboard Left Side Ends Here -->
  
  <div class="right-rp"> 
    
    <!-- Top Green Bar Section Begins Here -->
    <?php
include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
?>
    <!-- Top Green Bar Section Ends Here -->
   
    <div class="col-continer">
      <?php	

if($this->session->flashdata('msg') != "")
{
echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
}
?>
      <div class="col-3 cat-box dragable-cat">
        <div class="contntTop-row">
          <div class="actvity-hd">
            <div class="act-tab b-ad-tb">
                <table cellspacing="0" border="0">
                    <colgroup>
                    <col width="35%" />
                    <col width="35%" />
                    <col width="42%" />
                    <!-- <col width="3%" />
                        <col width="3%" />
                        <col width="3%" />-->
                    </colgroup>
                    <tr>
                        <td class="no-bdr">
                        	<div class="cat-tbl-tp-lf">
                                <h1>Categories</h1> <span class="n-of-cat"><?=$total_records?></span>
							</div>
						</td>
                        <td class="no-bdr">&nbsp;</td>
                        <td  class="no-bdr" align="right">
                            <a href="<?= base_url() ?>admin/categories/add_category/" class="pro-addNew"> Add Category </a>
                        </td>
                    </tr>
                </table>
              
            </div>
          </div>
          <div class="b-prd-tbl align-top">
          	<div class="tableDemo">
			<div id="debugArea" style="float: right">&nbsp;</div>
            <table border="0" cellspacing="0" id="mytable" class='tablesorter'>
              <colgroup>
              <col width="67%" />
              <col width="15%" />
              <col width="18%" />
              </colgroup>
              <thead>
                <tr>
                  <th align="left">Category Title</th>
                  <th align="center">Status</th>
                  <th align="center">Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php
				if(count($list) > 0) { 
				for ($n = 0; $n < count($list); $n++) {
				?>
                <tr class="ui-state-default ui-sortable-handle" <?php #if (($n % 2) == 0) echo "class='odd'"; ?> id="<?=$list[$n]['cat_id']?>">
                  <td class="td-content" align="left"><?=limited_text($list[$n]['cat_name'] , 40) ?></td>
                  <td class="td-content" align="center">
                    <?php
					if($list[$n]['cat_status'] == 1)
					{
						echo "Active";
					}
					else
					{
						echo "Inactive";
					}
					?>
                  </td>
                  
                  <td align="center"><table border="0" cellspacing="0" cellpadding="0">
                      <colgroup>
                      <col width="20%" />
                      <col width="20%" />
                      <col width="20%" />
                      <col width="20%" />
                      <col width="20%" />
                      </colgroup>
                      <tr id="display_tr">
                        <td>&nbsp;</td>
                        <!--<td align="center"><a title="Listing" href="<?= base_url() ?>admin/categories/subcategory/<?= $list[$n]['cat_id'] ?>" class="tik-cross-btns dtlbtn-n"></a></td>-->
                        <td align="center"><a title="Edit" href="<?= base_url() ?>admin/categories/edit_category/<?= $list[$n]['cat_id'] ?>" class="tik-cross-btns p-edt-btn-n"></a></td>
                        <td align="center"><a title="Delete" onclick="return confirm('Are you sure you want to delete the selected item(s)? ')" href="<?= base_url() ?>admin/categories/delete_category/<?= $list[$n]['cat_id'] ?>" class="tik-cross-btns p-del-btn-n"></a></td>
                        <td align="center"><a title="Filters Listing" href="<?=base_url('admin/categories/filters/'.$list[$n]['cat_id'] )?>" class="tik-cross-btns dtlbtn-n"> </span></a></td>
                        <td>&nbsp;</td>
                      </tr>
                    </table></td>
                </tr>
                <?php
					}
				}else { 
					echo "<tr><td colspan='5' align='center'><strong>No data found.</strong></td></tr>";
				}
				?>
              </tbody>
            </table>
            </div>
            <div class="pagination-row" style="width:100%">
				<?=$paginglink ?>
            </div>
            
            
            
            <!-- New Structure Starts -->
            
                        
            <!-- New Lisitng Structure Starts -->
            <div class="ul-listing-wrap">
                
                <!-- Listing Header Starts -->
                <div class="ul-lihdr">
                    
                    <div class="ul-liman-rp ul-col-one">
                        <h3>Category Title</h3>
                    </div>
                    
                    <div class="ul-liman-rp ul-col-two">
                        <h3>Status</h3>
                    </div>
                    
                    <div class="ul-liman-rp ul-col-three">
                        <h3>Actions</h3>
                    </div>
                    
                </div>
                <!-- Lisiting Header Ends -->
                
                <!-- Listing Bottom Section Starts -->
                <div class="ul-libtm">
                    
                    <div class="sort-wrap">
                        
                        <ul class="sortable">
                        	
                            <li>
                                <!-- Row Starts -->
                                <div class="ul-lirow">
                                    
                                    <div class="ul-limain-list">
                                        
                                        <div class="ul-liman-rp ul-col-one">
                                            <div class="ul-ico-sub"> <!-- ul-ico-empty -->
                                            	<a href="javascript:void(0);"></a>
                                            </div>
                                        	<a href="#">Row Name</a>
                                        </div>
                                        
                                        <div class="ul-liman-rp ul-col-two">
                                        	Active
                                        </div>
                                        
                                        <div class="ul-liman-rp ul-col-three">
                                            <div class="action-btn">
                                            	<a href="#" title="Edit" class="tik-cross-btns p-edt-btn-n"></a>
                                            </div>
                                            <div class="action-btn">
                                            	<a href="#" title="Delete" class="tik-cross-btns p-del-btn-n" onclick="return confirm('Are you sure you want to delete selected item(s)?');"></a>
                                            </div>
                                        </div>
                                        
                                        <div class="ul-liman-rp ul-col-three" style="display:none;">
                                            <div class="action-btn">
                                            	<a href="#" title="View Detail" class="tik-cross-btns p-srch-btn-n"></a>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                    <!-- Show Hide Container Starts -->
                                    <div class="ul-lisub-list" style="display:none;">
                                        
                                        <ul class="sortable">
                                            <li>
                                                <!-- Sub Row Starts -->
                                                <div class="ul-lisub-lrow">
                                                    <div class="ul-liman-rp ul-col-one">
                                                        <div class="ul-ico-subrep"></div> <!-- Sub Icon -->
                                                        <a href="#>">Sub Category Name</a>
                                                    </div>
                                                    
                                                    <div class="ul-liman-rp ul-col-two">
                                                    	Active
                                                    </div>
                                                    
                                                    <div class="ul-liman-rp ul-col-three">
                                                        <div class="action-btn">
                                                        	<a href="#" title="Edit" class="tik-cross-btns p-edt-btn-n"></a>
                                                        </div>
                                                        <div class="action-btn">
                                                        	<a href="#" title="Delete" class="tik-cross-btns p-del-btn-n" onclick="return confirm('Are you sure you want to delete selected item(s)?');"></a>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="ul-liman-rp ul-col-three" style="display:none;">
                                                        <div class="action-btn">
                                                        	<a href="#" title="View Detail" class="tik-cross-btns p-srch-btn-n"></a>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <!-- Sub Row Ends -->
                                            </li>
                                        </ul>
                                        
                                    </div>
                                    <!-- Show Hide Container Ends -->
                                    
                                </div>
                                <!-- Row Ends -->
                            </li>
                            
                            <li>
                                <!-- Row Starts -->
                                <div class="ul-lirow">
                                    
                                    <div class="ul-limain-list">
                                        
                                        <div class="ul-liman-rp ul-col-one">
                                        	<a href="#">Row Name</a>
                                        </div>
                                        
                                        <div class="ul-liman-rp ul-col-two">
                                        	Active
                                        </div>
                                        
                                        <div class="ul-liman-rp ul-col-three">
                                            <div class="action-btn">
                                            	<a href="#" title="Edit" class="tik-cross-btns p-edt-btn-n"></a>
                                            </div>
                                            <div class="action-btn">
                                            	<a href="#" title="Delete" class="tik-cross-btns p-del-btn-n" onclick="return confirm('Are you sure you want to delete selected item(s)?');"></a>
                                            </div>
                                            <div class="action-btn">
                                            	<a title="Filters Listing" href="#" class="tik-cross-btns dtlbtn-n"></a>
                                            </div>
                                        </div>
                                        
                                        <div class="ul-liman-rp ul-col-three" style="display:none;">
                                            <div class="action-btn">
                                            	<a href="#" title="View Detail" class="tik-cross-btns p-srch-btn-n"></a>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                <!-- Row Ends -->
                            </li>
                            
                            <div class="not-found">
                            	<strong>No category found.</strong>
                            </div>
                            
                        </ul>
                        
                    </div>
                    
                </div>
                <!-- Listing Bottom Section Ends -->
                
            </div>
            <!-- New Lisitng Structure Ends -->
            
            <!-- New Structure Ends -->
            
            
            
            
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--
<script>
$(document).ready(function() {
$('table').filterTable({ // apply filterTable to all tables on this page
quickList: [] // add some shortcut searches
});
});
</script> 
<script src="<?= base_url() ?>/js/jquery.filtertable.min.js"></script>
-->

<script src="<?=base_url()?>/js/uisortable/jquery-ui.js" type="text/javascript"></script>
<script src="<?=base_url()?>/js/categorieslisting.js" type="text/javascript"></script>
</body>
</html>