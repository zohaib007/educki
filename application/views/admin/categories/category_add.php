<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title; ?></title>
<link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php");?>
<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
<script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>


<!-- tinymce configration start here.-->
<script>
var base_url='<?php echo base_url();?>';
var ser='<?php echo $_SERVER['DOCUMENT_ROOT'];?>dev/resources/tiny_upload';
</script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.js"></script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>tiny/common.js"></script>
<!-- tinymce configration end here.-->

<script type="text/javascript">
function convertToSlug(Text) {
    url = Text
        .toLowerCase()
        .replace(/ /g, '-').replace(/[^\w -]+/g, '');
    document.getElementById("my_url").value = url;
}

function convertToSlug2(Text) {
    url = Text
        .toLowerCase()
        .replace(/ /g, '-').replace(/[^\w -]+/g, '');
    document.getElementById("my_url").value = url;
}
</script>
</head>

<body>
<div class="container_p"> 
    
    <!-- Dashboard Left Side Begins Here -->
    <div class="left_wrp">
        <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
    </div>
    <!-- Dashboard Left Side Ends Here -->
    
    <div class="right-rp"> 
        
        <!-- Top Green Bar Section Begins Here -->
        <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
        <!-- Top Green Bar Section Ends Here --> 
        
        <!-- Section Starts -->
        <div class="mu-contnt-wrp">
            <div class="mu-contnt-hdng">
                <h1>Add Category</h1>
            </div>
            
            <!-- Bread Crumbs Starts -->
            <div class="n-crums">
                <ul>
                    <li> <a href="<?php echo base_url('admin/categories'); ?>">Categories</a> </li>
                    <li>
                        <div class="crms-sep">&gt;</div>
                    </li>
                    <li> <a href="javascript:void(0);">Add Category</a> </li>
                </ul>
            </div>
            <!-- Bread Crumbs Ends --> 
            
            <?php echo form_open_multipart('', array('name'=>'add-category')); ?> 
            
            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Category</h2>
                    <p>Manage your category.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Category Name*</label>
                                <input type="text" name="name" value="<?=stripcslashes(set_value('name'))?>" placeholder="" onfocusout="convertToSlug2($(this).val())"/>
                            </div>
                            <div class="error"> <?php echo form_error('name')?> </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>URL*</label>
                                <input type="text" name="url" id="my_url" value="<?=stripcslashes(set_value('url'))?>" placeholder="" onkeyup="convertToSlug($(this).val())"/>
                            </div>
                            <div class="error"> <?php echo form_error('url')?> </div>
                        </div>
                        <!-- <div class="mu-flds-wrp">
                            <label class="lone-set">Description*</label>
                            <textarea class="myeditor" name="cat_desc" ><?=stripcslashes(set_value('cat_desc'))?></textarea>
                            <div class="error"> <?php echo form_error('cat_desc')?> </div>
                        </div>  -->                       
                    </div>
                </div>
            </div>

            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Parent Category</h2>
                    <p>The parent category under which this category will be displayed.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Parent Category*</label>
                                <select name="cat_parent_id" id="cat_parent">
                                    <option value="">Select Parent Category</option>
                                    <option value="0" <?=set_select('cat_parent_id','0')?>>No Parent Category</option>
                                    <?php foreach($pCategories as $pCat) { ?>
                                    <option value="<?=$pCat['cat_id']?>" <?=set_select('cat_parent_id', $pCat['cat_id'])?>><?=$pCat['cat_name']?></option>
                                    <?php } ?>
                                </select>                                
                            </div>
                            <div class="error"><?=form_error('cat_parent_id')?></div>
                        </div>                        
                    </div>
                </div>
            </div>            
            
            <div class="mu-contnt-outer" id="image" >
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Image</h2>
                    <p> Upload an image for this category. </p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds">
                                <div class="my-form">
                                    <p class="text-box">
                                        <label for="box1">Image*</label>
                                    <div class="fileUp">
                                        <input type="file" name="file1" value="" accept="image/*" id="box1" onchange="javascript: document.getElementById('box1-txt').value = this.value" />
                                        <label for="box1"></label>
                                        <input type="text" id="box1-txt" placeholder="Upload File" value="" name="hidden_file" readonly="true" />
                                    </div>
                                    </p>
                                </div>
                                
                            </div>
                            <div class="error"><?=form_error('hidden_file')?><?=$this->session->flashdata('msg_err0')?></div>
                        </div>                        

                        <!-- <div class="mu-flds-wrp">
                            <div class="mu-frmFlds">
                                <div class="my-form">
                                    <p class="text-box">
                                        <label for="box2">Thumb Image*</label>
                                    <div class="fileUp">
                                        <input type="file" name="file2" value="" accept="image/*" id="box2" onchange="javascript: document.getElementById('box2-txt').value = this.value" />
                                        <label for="box2"></label>
                                        <input type="text" id="box2-txt" placeholder="Upload File" value="" name="hidden_file2" readonly="true" />
                                    </div>
                                    </p>
                                </div>                            
                            </div>
                            <div class="error"><?=form_error('hidden_file2')?><?=$this->session->flashdata('msg_err1')?></div>
                        </div>  -->                       
                    </div>
                </div>
            </div>            
            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Meta Information</h2>
                    <p> Set up the category title, meta description. These helps define how this category shows up on search engines. </p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Meta title</label>
                                <input type="text" value="<?php echo set_value('meta_title'); ?>" name="meta_title" />
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Meta keywords</label>
                                <input type="text" value="<?php echo set_value('meta_keyword'); ?>" name="meta_keyword" />
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Meta description</label>
                                <textarea class="text_meta" name="meta_desc"><?php echo set_value('meta_desc'); ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Box along with label section ends here -->
            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Status</h2>
                    <p>Manage your category status.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Status*</label>
                                <select name="cat_status">
                                    <option value="" >Select</option>
                                    <option value="1" <?=set_select('cat_status','1')?>>Active</option>
                                    <option value="0" <?=set_select('cat_status','0')?>>Inactive</option>
                                </select>                                
                            </div>
                            <div class="error"><?=form_error('cat_status')?></div>                     
                        </div>                        
                    </div>
                </div>
            </div>
            
            <!-- Box wihout label section begins here -->
            <div class="mu-fld-sub">
                <input type="submit" value="Submit" />
                <a href="<?=base_url()?>admin/categories">cancel</a>
            </div>
            <!-- Box wihout label section ends here --> 
            
            <?php echo form_close(); ?> </div>
        <!-- Section Ends --> 
        
    </div>
</div>
</body>
<script>
//  $("#cat_parent").change(function(){
//     var option = $('#cat_parent').val();
//     if(option==0){
//         $("#image").css("display","block");
//     }else{
//         $("#image").css("display","none");
//     }
// });
//  $(document).ready(function(){
//     var option = '<?=@$_POST['cat_parent_id']?>';
//     if(option=='0'){
//         $("#image").css("display","block");
//     }else{
//         $("#image").css("display","none");
//     }
//  });
</script>
<script>
$(document).ready(function(e) {
	
	/* ADD CATEGORY FILTER START */
    $(document).on('click', '.add_more', function(f){		
		var total_filter = $("input[name='total_filter']").val();
		var html = '<div class="catFilters filter_no'+total_filter+' filter-mwrap" ><div class="mu-flds-wrp"><div class="mu-frmFlds_long"><label>Filter Title</label><input type="text" value="" class="filterTitle" name="filter_title'+total_filter+'" /></div></div><div class="mu-flds-wrp"><div class="mu-frmFlds_long"><label>Filter Details</label><textarea class="text_meta filterArea" name="filter_details'+total_filter+'"></textarea></div></div><!-- Remove Structure Starts --><div class="nfull-row ar-space2"><a href="javascript:void(0);" title="Remove" data-filter="filter_no'+total_filter+'" class="addremove-lk remove_filter">Remove</a></div><!-- Remove Structure Ends --><div class="cat-sptr"></div></div></div>';
		$('.add_cat_filters').append(html);
		$("input[name='total_filter']").val((parseInt(total_filter)+1));
	});
	/* ADD CATEGORY FILTER END */
	
// :)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:)|:) \\

	/* REMOVE CATEGORY FILTER START */
	$(document).on('click', '.remove_filter', function(g){
		var total_filter = $("input[name='total_filter']").val();
		var filter_no = $(this).attr('data-filter');
		//$('.'+filter_no).fadeOut("fast", function(){
			$('.'+filter_no).remove();
			$("input[name='total_filter']").val((parseInt(total_filter)-1));
			var all_filters = $('.add_cat_filters').find('.catFilters');
			
			$('.catFilters').each(function(index){
				$(this).removeClass();
				$(this).addClass("catFilters filter-mwrap filter_no"+index);
				$(this).find('.filterTitle').attr('name','filter_title'+index);
				$(this).find('.filterArea').attr('name','filter_details'+index);
				$(this).find('.remove_filter').attr('data-filter','filter_no'+index);
			});
		//});
	});
	/* REMOVE CATEGORY FILTER END */
});
</script>

</html>