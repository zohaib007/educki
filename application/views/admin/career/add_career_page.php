<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Page</title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
<!--
<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
<script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
-->
<!-- tinymce configration start here.-->
<script>
var base_url='<?php echo base_url();?>';
var ser='<?php echo $_SERVER['DOCUMENT_ROOT'];?>dev/resources/tiny_upload';
</script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.js"></script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>tiny/common.js"></script>
<!-- tinymce configration end here.-->

<script type="text/javascript">
  jQuery(document).ready(function($){
    $('.my-form .add-box').click(function(){
      var n = $('.text-box').length + 1;
        if( 5 < n ) {
            alert('Stop it!');
            return false;
        }
           var box_html = $('<p class="text-box"><label for="box' + n + '">image <span class="box-number">' + n + '</span></label> <input type="file" name="file[]" value="" id="box' + n + '" /> <a href="#" class="remove-box">Remove</a></p>');
        box_html.hide();
        $('.my-form p.text-box:last').after(box_html);
        box_html.fadeIn('slow');
        return false;
    });
    $('.my-form').on('click', '.remove-box', function(){
       
        $(this).parent().fadeOut("slow", function() {
            $(this).remove();
            $('.box-number').each(function(index){
                $(this).text( index + 1 );
            });
        });
        return false;
    });
});
</script>
<script>
  $(document).ready(function(){
    $("#hide").click(function(){
      $('#datepicker-example1').val('');
      $('#timeid').val('');
      $(".show_hide").hide();
    });
    $("#show").click(function(){
      $(".show_hide").show();
    });
    
  });
</script>
</head>
<body>
<div class="container_p">
    
    <!-- Dashboard Left Side Begins Here -->
    <div class="left_wrp">
        <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
    </div>
    <!-- Dashboard Left Side Ends Here -->
    
    <div class="right-rp">
        
        <!-- Top Green Bar Section Begins Here -->
        <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
        <!-- Top Green Bar Section Ends Here -->
        <div class="mu-contnt-wrp">
            <div class="mu-contnt-hdng">
                <h1>Add New Page</h1>
            </div>
            <!-- Bread crumbs starts here -->
            <div class="n-crums">
                <ul>
                    <li>
                        <a href="<?php echo base_url() ?>admin/career">Career</a>
                    </li>
                    <li>
                        <div class="crms-sep">
                            &gt;
                        </div>
                    </li>
                    <li>
                        <a href="javascript:voide(0);"> Add Page</a>
                    </li>
                </ul>
            </div>
            <!-- Bread crumbs ends here -->
            
            <?php echo form_open_multipart('admin/career/career_page',array('name'=>'add page')); ?>
            <!-- Box along with label section begins here -->
            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Write your page</h2>
                    <p>Give your page a title, and add your page content.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Title*</label>
                                <input type="text" value="<?php echo set_value('pg_title'); ?>" name="pg_title" placeholder="" />
                            </div>
                            <div class="error">
                                <?php echo form_error('pg_title')?>
                            </div>
                        </div>
                        
                        <div class="mu-flds-wrp">
                            <label class="lone-set">Description*</label>
                            <textarea  class="myeditor" name="pg_description" id="desc" ><?php echo set_value('pg_description'); ?></textarea>
                            <div class="error">
                                <?php echo form_error('pg_description')?>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Banner Image</h2>
                    <p>Upload an banner image for this page.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Banner Title*</label>
                                <input type="text" value="<?php echo set_value('banner_title'); ?>" name="banner_title" placeholder="" />
                            </div>
                            <div class="error">
                                <?php echo form_error('banner_title')?>
                            </div>
                        </div>
                        
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds">
                                <div class="my-form1">
                                    <div class="my-form1">
                                        <div id="testdiv">
                                        </div>
                                        <p class="text-box">
                                            <label for="box1">Banner Image
                                                <span class="sub-text x-set">Image size: 715 x 372</span>
                                            </label>
                                            <!--<input type="file" name="image" value="" id="box1" />-->
                                        <div class="fileUp">
                                            <input type="file" name="banner_image" value="" id="box1" onchange="javascript: document.getElementById('box1-txt').value = this.value" />
                                            <label for="box1"></label>
                                            <input type="text" id="box1-txt" value="Upload File" readonly="true" />
                                        </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="error">
                                <?=$banner_image_error?>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>First Page Image</h2>
                    <p>Upload an First image for this page.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>First Image Title*</label>
                                <input type="text" value="<?php echo set_value('first_image_title'); ?>" name="first_image_title" placeholder="" />
                            </div>
                            <div class="error">
                                <?php echo form_error('first_image_title')?>
                            </div>
                        </div>
                        
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>First Image description* </label>
                                <textarea class="text_meta" name="first_image_description"><?php echo set_value('first_image_description'); ?></textarea>
                                <div class="error"><?php echo form_error('first_image_description')?></div>
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds">
                                <div class="my-form1">
                                    <div class="my-form1">
                                        <div id="testdiv">
                                        </div>
                                        <p class="text-box">
                                            <label for="box1">First Image
                                                <span class="sub-text x-set">Image size: 715 x 372</span>
                                            </label>
                                            <!--<input type="file" name="image" value="" id="box1" />-->
                                        <div class="fileUp">
                                            <input type="file" name="first_image" value="" id="box1" onchange="javascript: document.getElementById('first_box1-txt').value = this.value" />
                                            <label for="box1"></label>
                                            <input type="text" id="first_box1-txt" value="Upload File" readonly="true" />
                                        </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="error">
                                <?=$first_image_error?>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Second Image</h2>
                    <p>Upload an Second image for this page.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Second Image Title*</label>
                                <input type="text" value="<?php echo set_value('second_image_title'); ?>" name="second_image_title" placeholder="" />
                            </div>
                            <div class="error">
                                <?php echo form_error('second_image_title')?>
                            </div>
                        </div>                        
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Second Image description* </label>
                                <textarea class="text_meta" name="second_image_description"><?php echo set_value('second_image_description'); ?></textarea>
                                <div class="error"><?php echo form_error('second_image_description')?></div>
                            </div>
                        </div>                        
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds">
                                <div class="my-form1">
                                    <div class="my-form1">
                                        <div id="testdiv">
                                        </div>
                                        <p class="text-box">
                                            <label for="box1">Second Image
                                                <span class="sub-text x-set">Image size: 715 x 372</span>
                                            </label>
                                            <!--<input type="file" name="image" value="" id="box1" />-->
                                        <div class="fileUp">
                                            <input type="file" name="second_image" value="" id="box1" onchange="javascript: document.getElementById('second_box1-txt').value = this.value" />
                                            <label for="box1"></label>
                                            <input type="text" id="second_box1-txt" value="Upload File" readonly="true" />
                                        </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="error"><?=$second_image_error?></div>
                        </div>                        
                    </div>
                </div>
            </div>
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Third Image</h2>
                    <p>Upload an third image for this page.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Third Image Title*</label>
                                <input type="text" value="<?php echo set_value('third_image_title'); ?>" name="third_image_title" placeholder="" />
                            </div>
                        </div>
                        <div class="error">
                            <?php echo form_error('third_image_title')?>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Third Image description* </label>
                                <textarea class="text_meta" name="third_image_description"><?php echo set_value('third_image_description'); ?></textarea>
                                <div class="error"><?php echo form_error('third_image_description')?></div>
                            </div>
                        </div>                        
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds">
                                <div class="my-form1">
                                    <div class="my-form1">
                                        <div id="testdiv">
                                        </div>
                                        <p class="text-box">
                                            <label for="box1">Third Image
                                                <span class="sub-text x-set">Image size: 715 x 372</span>
                                            </label>
                                            <!--<input type="file" name="image" value="" id="box1" />-->
                                        <div class="fileUp">
                                            <input type="file" name="third_image" value="" id="box1" onchange="javascript: document.getElementById('third_box1-txt').value = this.value" />
                                            <label for="box1"></label>
                                            <input type="text" id="third_box1-txt" value="Upload File" readonly="true" />
                                        </div>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="error"><?=$third_image_error?></div>
                        </div>                        
                    </div>
                </div>
            </div>
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Meta Information</h2>
                    <p> Set up the meta description. These help define how this product shows up on search engines. </p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Meta title</label>
                                <input type="text" value="<?php echo set_value('meta_title'); ?>" name="meta_title" />
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Meta Keyword</label>
                                <input type="text" value="<?php echo set_value('meta_keywords'); ?>" name="meta_keywords" />
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Meta description </label>
                                <textarea class="text_meta" name="meta_description"><?php echo set_value('meta_description'); ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mu-fld-sub">
                <input type="submit" value="Submit" />
                <a href="<?=base_url() ?>admin/pages">cancel</a>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
</body>
</html>