<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Dashboard</title>
    <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all"/>
    <?php include(ADMIN_INCLUDE_PATH . "includes/js.php"); ?>

    <!--
<script src="<?= base_url() ?>js/charts/highcharts.js"></script>
<script src="<?= base_url() ?>js/charts/modules/exporting.js"></script>
-->

    


    <!-- Chart CSS Starts -->
    <!-- Font Awesome c -->
    <link rel="stylesheet" href="<?= base_url() ?>js/chart-files/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?= base_url() ?>js/chart-files/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url() ?>js/chart-files/chart-css.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Chart CSS Ends -->

    <!-- Date Range CSS Starts -->
    <link rel="stylesheet" href="<?= base_url() ?>css/daterangepicker.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.16.0/moment.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/date-range/demo.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>js/date-range/jquery.daterangepicker.js" type="text/javascript"></script>
    <style type="text/css">
        #display_tr {
            display: table-row !important;
        }
    </style>
    <!-- Date Range CSS Ends -->

</head>
<body>
<div class="container">

    <!-- Dashboard Left Side Begins Here -->
    <div class="left_wrp">

        <?php include(ADMIN_INCLUDE_PATH . "includes/dash-left.php") ?>

    </div>
    <!-- Dashboard Left Side Ends Here -->
    <div class="right-rp">
        <!-- Top Green Bar Section Begins Here -->
        <?php include(ADMIN_INCLUDE_PATH . "includes/top_green_bar.php"); ?>
        <!-- Top Green Bar Section Ends Here -->
        <div class="mu-contnt-hdng">
            <h1>Dashboard</h1>
        </div>
        <div class="col-continer">
            <div class="col-3 cat-box">
                <div class="contntTop-row"> <!-- col-box1 -->

                    <!-- Top Header Bar Starts -->
                    <div class="actvity-hd">
                        <div class="mtr-hdr">

                            <h6>Market Place Dashboard Summary</h6>

                            <!-- Filter By Dropdown Structure Starts -->
                            <div class="ndpdSort newsortdrop-set ad1-pad">
                                <button class="pro-addNew nsortBy">Filter By<span class="dn-angl"></span></button>
                                <!--  id="nsortBy" -->
                                <div class="dropval"></div>
                                <div class="ndp-nx nsortDp" style="display:none;"> <!-- id="nsortDp" -->
                                    <ul>
                                        <li>
                                            <div class="usr-srt-dp subDpd"> Select date range <span class="sub-dn-angl"
                                                                                                    style="top: 10px;"></span>
                                            </div>

                                            <!-- Regular Text Box Starts -->
                                            <input value="" type="text" name="searchText" class="catName"
                                                   style="display:none;"/>
                                            <!-- Regular Text Box Ends -->

                                            <!-- Default Setting Do not remove starts -->
                                            <div class="demo" style="display:none;">
                                                Default settings: <input class="date-range0" size="40" value=""/>
                                                <!-- id="date-range0" -->
                                            </div>
                                            <div class="demo">
                                                <input class="date-range-01 catName" id="marketplaceValue"
                                                       name="dateRange1" size="60" value="<?= @$_POST['dateRange'] ?>"
                                                       placeholder="Please select a date range"/>
                                                <!-- id="date-range1" -->
                                            </div>
                                            <!-- Default Setting Do not remove starts -->
                                            <input type="submit" value="Submit" id="marketplaceSubmit"
                                                   class="nbtn-nrp"/>

                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Filter By Dropdown Structure Ends -->

                        </div>
                    </div>
                    <!-- Top Header Bar Starts -->

                    <!-- Bottom Section Starts -->
                    <div class="ndash-wrap">

                        <!-- First Section Starts -->
                        <div class="ndash-firstsec">

                            <!-- Repeatable Row Starts with 27px margin -->
                            <div class="ndash-frow">

                                <!-- Repeatable Column Starts -->
                                <div class="ndash-fcol">
                                    <div class="ndash-fcinn">

                                        <!-- Top Text Starts -->
                                        <div class="ndfc-ttxt">
                                            <p>
                                                Gross Merchandise Volume (GMV)
                                            </p>
                                        </div>
                                        <!-- Top Text Ends -->

                                        <!-- Bottom Text Starts -->
                                        <div class="ndfc-btxt">
                                            <p>Total Sales</p>
                                            <p><strong class="red-txt"
                                                       id="GMV">$<?= (null == $marketplace_data['GMV']) ? '0.00' : number_format($marketplace_data['GMV'],2, '.', ' '); ?></strong>
                                            </p>
                                        </div>
                                        <!-- Bottom Text Ends -->

                                    </div>
                                </div>
                                <!-- Repeatable Column Ends -->

                                <!-- Repeatable Column Starts -->
                                <div class="ndash-fcol">
                                    <div class="ndash-fcinn">

                                        <!-- Top Text Starts -->
                                        <div class="ndfc-ttxt">
                                            <p>
                                                Total Number of Transactions Completed on Site
                                            </p>
                                        </div>
                                        <!-- Top Text Ends -->

                                        <!-- Bottom Text Starts -->
                                        <div class="ndfc-btxt">
                                            <p>Total Number</p>
                                            <p><strong class="red-txt"
                                                       id="transactions_number"><?= (null == $marketplace_data["transactions_number"]) ? '0.00' : number_format($marketplace_data["transactions_number"],2, '.', ' '); ?></strong>
                                            </p>
                                        </div>
                                        <!-- Bottom Text Ends -->

                                    </div>
                                </div>
                                <!-- Repeatable Column Ends -->

                                <!-- Repeatable Column Starts -->
                                <div class="ndash-fcol">
                                    <div class="ndash-fcinn">

                                        <!-- Top Text Starts -->
                                        <div class="ndfc-ttxt">
                                            <p>
                                                Average order value ($) from Cart
                                            </p>
                                        </div>
                                        <!-- Top Text Ends -->

                                        <!-- Bottom Text Starts -->
                                        <div class="ndfc-btxt">
                                            <p>Average Value</p>
                                            <p><strong class="red-txt"
                                                       id="avg_order_value"><?= (null == $marketplace_data["avg_order_value"]) ? '0.00' : number_format($marketplace_data["avg_order_value"],2, '.', ' '); ?></strong>
                                            </p>
                                        </div>
                                        <!-- Bottom Text Ends -->

                                    </div>
                                </div>
                                <!-- Repeatable Column Ends -->

                            </div>
                            <!-- Repeatable Row Ends -->

                        </div>
                        <!-- First Section Ends -->

                        <!-- Second Section Starts -->
                        <div class="ndash-secondsec">

                            <!-- scol Starts -->
                            <div class="ndash-chartrp">

                                <!-- Chart Image Section Starts -->
                                <div class="ndash-chartimg">
                                    <!-- <img src="<?= base_url() ?>images/ndash-chart.jpg" alt="" /> -->
                                    <!-- Sales Chart Canvas Starts -->
                                    <div class="chart">
                                        <canvas class="salesChart" style="height: 180px;"></canvas>
                                        <!-- id="salesChart" -->
                                    </div>
                                    <!-- Sales Chart Canvas Ends -->
                                </div>
                                <!-- Chart Image Section Ends -->

                                <!-- Chart Text Starts Here -->
                                <div class="chart-text">
                                    GMV Growth Rate, M-o-M (%)
                                </div>
                                <!-- Chart Text Ends Here -->

                            </div>
                            <!-- scol Ends -->


                            <!-- scol Starts -->
                            <div class="ndash-chartrp">

                                <!-- Chart Image Section Starts -->
                                <div class="ndash-chartimg">
                                    <!-- <img src="<?= base_url() ?>images/ndash-chart.jpg" alt="" /> -->
                                    <!-- Sales Chart Canvas Starts -->
                                    <div class="chart">
                                        <canvas class="salesChart2" style="height: 180px;"></canvas>
                                        <!-- id="salesChart" -->
                                    </div>
                                    <!-- Sales Chart Canvas Ends -->
                                </div>
                                <!-- Chart Image Section Ends -->

                                <!-- Chart Text Starts Here -->
                                <div class="chart-text">
                                    GMV Growth Rate, Y-o-Y (%)
                                </div>
                                <!-- Chart Text Ends Here -->

                            </div>
                            <!-- scol Ends -->

                        </div>
                        <!-- Second Section Ends -->

                        <!-- Sub Section Starts -->
                        <div class="dash-ssection">

                            <!-- Heading Starts -->
                            <div class="actvity-hd">
                                <div class="mtr-hdr">
                                    <h6>Total Revenue ($)</h6>
                                </div>
                            </div>
                            <!-- Heading Ends -->

                            <!-- Bottom Table Section Starts -->
                            <div class="nbtm-row sixeighty">
                                <div class="totsub-tbl">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="75%"/>
                                            <col width="25%"/>
                                        </colgroup>

                                        <tr>
                                            <td align="left">
                                                Transaction Fee Non Subscribers
                                            </td>
                                            <td align="center">
                                                <strong>$<?=($tier_revenue['tier_1']>0)?number_format($tier_revenue['tier_1'],2,'.',''):'0.00'?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Transaction Fee Tier 1 - (<?=getTierMonths(2);?> months) Subscription
                                            </td>
                                            <td align="center">
                                                <strong>$<?=($tier_revenue['tier_2']>0)?number_format($tier_revenue['tier_2'],2,'.',''):'0.00'?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Transaction Fee Tier 2 - (<?=getTierMonths(3);?> months) Subscription
                                            </td>
                                            <td align="center">
                                                <strong>$<?=($tier_revenue['tier_3']>0)?number_format($tier_revenue['tier_3'],2,'.',''):'0.00'?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Transaction Fee Tier 3 - (<?=getTierMonths(4);?> months) Subscription
                                            </td>
                                            <td align="center">
                                                <strong>$<?=($tier_revenue['tier_4']>0)?number_format($tier_revenue['tier_4'],2,'.',''):'0.00'?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Average Transaction Revenue
                                            </td>
                                            <td align="center">
                                                <?php $average = ($tier_revenue['tier_1']+$tier_revenue['tier_2']+$tier_revenue['tier_3']+$tier_revenue['tier_4'])/4;?>
                                                <strong>$<?=($average>0)?number_format($average,2,'.',''):'0.00'?></strong>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                            <!-- Bottom Table Section Ends -->

                        </div>
                        <!-- Sub Section Ends -->

                        <!-- Sub Section Starts -->
                        <div class="dash-ssection">

                            <!-- Heading Starts -->
                            <div class="actvity-hd">
                                <div class="mtr-hdr">
                                    <h6>Subscription FEE Revenue TOTAL (MRR) Monthly Recurring Revenue</h6>
                                </div>
                            </div>
                            <!-- Heading Ends -->

                            <!-- Bottom Table Section Starts -->
                            <div class="nbtm-row sixeighty">
                                <div class="totsub-tbl">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="75%"/>
                                            <col width="25%"/>
                                        </colgroup>

                                        <tr>
                                            <td align="left">
                                                Subscription Revenue Tier 1 (<?=getTierMonths(2);?> months)
                                            </td>
                                            <td align="center">
                                                <strong>$<?= ($_revenue_data['t1'] == null) ? '0.00' : $_revenue_data['t1']; ?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Subscription Revenue Tier 2 (<?=getTierMonths(3);?> months)
                                            </td>
                                            <td align="center">
                                                <strong>$<?= ($_revenue_data['t2'] == null) ? '0.00' : $_revenue_data['t2']; ?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Subscription Revenue Tier 3 (<?=getTierMonths(4);?> months)
                                            </td>
                                            <td align="center">
                                                <strong>$<?= ($_revenue_data['t3'] == null) ? '0.00' : $_revenue_data['t3']; ?></strong>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                            <!-- Bottom Table Section Ends -->

                        </div>
                        <!-- Sub Section Ends -->

                        <!-- Sub Section Starts -->
                        <div class="dash-ssection">

                            <!-- Heading Starts -->
                            <div class="actvity-hd">
                                <div class="mtr-hdr">
                                    <h6>Subscribers [Total, Existing, Renewed, New, Churn Count ]</h6>
                                </div>
                            </div>
                            <!-- Heading Ends -->

                            <!-- Bottom Table Section Starts -->
                            <div class="nbtm-row onedzero">
                                <div class="totsub-tbl">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="40%" />
                                            <col width="12%" />
                                            <col width="12%" />
                                            <col width="12%" />
                                            <col width="12%" />
                                            <col width="12%" />
                                        </colgroup>
                                        <tr>
                                            <th align="left">Total Subscribers</th>
                                            <th align="center">Total</th>
                                            <th align="center">Existing</th>
                                            <th align="center">Renewed</th>
                                            <th align="center">New</th>
                                            <th align="center" class="last">Churn</th>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                #  of Subscribers in Tier 0  Free
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_1['existing']+$tier_1['Renewal']+$tier_1['New']+$tier_1['churn']?></strong>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_1['existing']?></strong>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_1['Renewal']?></strong>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_1['New']?></strong>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_1['churn']?></strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                #  of Subscribers in  Tier 1   (<?=getTierMonths(2);?> months) - $<?=getTierAmount(2);?>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_2['existing']+$tier_2['Renewal']+$tier_2['New']+$tier_2['churn']?></strong>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_2['existing']?></strong>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_2['Renewal']?></strong>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_2['New']?></strong>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_2['churn']?></strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                #  of Subscribers in  Tier 2 (<?=getTierMonths(3);?> months) - $<?=getTierAmount(3);?>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_3['existing']+$tier_3['Renewal']+$tier_3['New']+$tier_3['churn']?></strong>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_3['existing']?></strong>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_3['Renewal']?></strong>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_3['New']?></strong>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_3['churn']?></strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                #  of Subscribers in  Tier 3 (<?=getTierMonths(4);?> months) - $<?=getTierMonths(4);?>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_4['existing']+$tier_4['Renewal']+$tier_4['New']+$tier_4['churn']?></strong>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_4['existing']?></strong>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_4['Renewal']?></strong>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_4['New']?></strong>
                                            </td>
                                            <td align="center">
                                                <strong><?=$tier_4['churn']?></strong>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- Bottom Table Section Ends -->

                        </div>
                        <!-- Sub Section Ends -->

                        <!-- Sub Section Starts -->

                        <!-- Sub Section Starts Add Drop -->
                        <div class="dash-ssection">

                            <!-- Heading Starts -->
                            <div class="actvity-hd">
                                <div class="mtr-hdr">

                                    <h6>Subscription Cancellation Request</h6>

                                    <!-- Filter By Dropdown Structure Starts -->
                                    <div class="ndpdSort newsortdrop-set ad1-pad">
                                        <button class="pro-addNew nsortBy">Filter By<span class="dn-angl"></span>
                                        </button> <!--  id="nsortBy" -->
                                        <div class="dropval"></div>
                                        <div class="ndp-nx nsortDp" style="display:none;"> <!-- id="nsortDp" -->
                                            <ul>
                                                <li>
                                                    <div class="usr-srt-dp subDpd"> Select date range <span
                                                                class="sub-dn-angl" style="top: 10px;"></span></div>

                                                    <!-- Regular Text Box Starts -->
                                                    <input value="" type="text" name="searchText" class="catName"
                                                           style="display:none;"/>
                                                    <!-- Regular Text Box Ends -->

                                                    <!-- Default Setting Do not remove starts -->
                                                    <div class="demo" style="display:none;">
                                                        Default settings: <input class="date-range0" size="40"
                                                                                 value=""/> <!-- id="date-range0" -->
                                                    </div>
                                                    <div class="demo">
                                                        <input class="date-range-02 catName" id="subscriber_time"
                                                               name="dateRange2" size="60"
                                                               value="<?= @$_POST['dateRange'] ?>"
                                                               placeholder="Please select a date range"/>
                                                        <!-- id="date-range1" -->
                                                    </div>
                                                    <!-- Default Setting Do not remove starts -->
                                                    <input type="submit" value="Submit" id="subscriber_submit"
                                                           class="nbtn-nrp"/>

                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Filter By Dropdown Structure Ends -->

                                </div>
                            </div>
                            <!-- Heading Ends -->

                            <!-- Bottom Table Section Starts -->
                            <div class="nbtm-row sixeighty">
                                <div class="totsub-tbl">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="75%"/>
                                            <col width="25%"/>
                                        </colgroup>

                                        <tr>
                                            <td align="left" id="subscriber_test">
                                                Cancellation Request Today
                                            </td>
                                            <td align="center" id="subscriber_value">
                                                <strong><?= ($cancel_subscriber['number'] == null) ? '0' : $cancel_subscriber['number']; ?></strong>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                            <!-- Bottom Table Section Ends -->

                        </div>
                        <!-- Sub Section Ends -->

                        <!-- Sub Section Starts -->
                        <div class="dash-ssection">

                            <!-- Heading Starts -->
                            <div class="actvity-hd">
                                <div class="mtr-hdr">
                                    <h6>% of Subscribers that renewed after their last Subscription month</h6>
                                </div>
                            </div>
                            <!-- Heading Ends -->

                            <!-- Bottom Table Section Starts -->
                            <div class="nbtm-row sixeighty">
                                <div class="totsub-tbl">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="75%"/>
                                            <col width="25%"/>
                                        </colgroup>

                                        <tr>
                                            <td align="left">
                                                % of Subscribers in Tier 1 (<?=getTierMonths(2);?> months)
                                            </td>
                                            <td align="center">
                                                <strong><?= number_format(($subscribers['renew_percentage_subscriber_t1'] == null) ? '0' : $subscribers['renew_percentage_subscriber_t1'], 2, '.', '') ?>%</strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                % of Subscribers in Tier 2 (<?=getTierMonths(3);?> months)
                                            </td>
                                            <td align="center">
                                                <strong><?= number_format(($subscribers['renew_percentage_subscriber_t2'] == null) ? '0' : $subscribers['renew_percentage_subscriber_t2'], 2, '.', '') ?>%</strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                % of Subscribers in Tier 3 (<?=getTierMonths(4);?> months)
                                            </td>
                                            <td align="center">
                                                <strong><?= number_format(($subscribers['renew_percentage_subscriber_t3'] == null) ? '0' : $subscribers['renew_percentage_subscriber_t3'], 2, '.', '') ?>%</strong>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                            <!-- Bottom Table Section Ends -->

                        </div>
                        <!-- Sub Section Ends -->

                        <!-- Sub Section Starts -->
                        <div class="dash-ssection">

                            <!-- Heading Starts -->
                            <div class="actvity-hd">
                                <div class="mtr-hdr">
                                    <h6>Existing Monthly Recurring Revenue (MRR) $$$ Total</h6>
                                </div>
                            </div>
                            <!-- Heading Ends -->

                            <!-- Bottom Table Section Starts -->
                            <div class="nbtm-row sixeighty">
                                <div class="totsub-tbl">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="75%"/>
                                            <col width="25%"/>
                                        </colgroup>

                                        <tr>
                                            <td align="left">
                                                Existing MRR $$$ Subscribers Tier 1 (<?=getTierMonths(2);?> months)
                                            </td>
                                            <td align="center">
                                                <strong>$ <?=($EMRR['EMRR_1']==null)?'00.00':$EMRR['EMRR_1']?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Existing MRR $$$ Subscribers Tier 2 (<?=getTierMonths(3);?> months)
                                            </td>
                                            <td align="center">
                                                <strong>$ <?=($EMRR['EMRR_2']==null)?'00.00':$EMRR['EMRR_2']?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Existing MRR $$$ Subscribers Tier 3 (<?=getTierMonths(4);?> months)
                                            </td>
                                            <td align="center">
                                                <strong>$ <?=($EMRR['EMRR_3']==null)?'00.00':$EMRR['EMRR_3']?></strong>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                            <!-- Bottom Table Section Ends -->

                        </div>
                        <!-- Sub Section Ends -->

                        <!-- Sub Section Starts -->
                        <div class="dash-ssection">

                            <!-- Heading Starts -->
                            <div class="actvity-hd">
                                <div class="mtr-hdr">
                                    <h6>Expansion MRR (Monthly Recurring Revenue) New and Renewed $$$ TOTAL</h6>
                                </div>
                            </div>
                            <!-- Heading Ends -->

                            <!-- Bottom Table Section Starts -->
                            <div class="nbtm-row sixeighty">
                                <div class="totsub-tbl">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="75%"/>
                                            <col width="25%"/>
                                        </colgroup>

                                        <tr>
                                            <td align="left">
                                                Expansion (MRR) NEW & Renewed Subscribers Tier 1 (<?=getTierMonths(2);?> months)
                                            </td>
                                            <td align="center">
                                                <strong>$ <?=($EXMRR['EXMRR_1']==null)?'00.00':$EXMRR['EXMRR_1']?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Expansion (MRR) NEW & Renewed Subscribers Tier 2 (<?=getTierMonths(3);?> months)
                                            </td>
                                            <td align="center">
                                                <strong>$ <?=($EXMRR['EXMRR_2']==null)?'00.00':$EXMRR['EXMRR_2']?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Expansion (MRR) NEW & Renewed Subscribers Tier 3 (<?=getTierMonths(4);?> months)
                                            </td>
                                            <td align="center">
                                                <strong>$ <?=($EXMRR['EXMRR_3']==null)?'00.00':$EXMRR['EXMRR_3']?></strong>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                            <!-- Bottom Table Section Ends -->

                        </div>
                        <!-- Sub Section Ends -->

                        <!-- Sub Section Starts -->
                        <div class="dash-ssection">

                            <!-- Heading Starts -->
                            <div class="actvity-hd">
                                <div class="mtr-hdr">
                                    <h6>Churned (MRR) Monthly Subscription Revenue $$ Dollars fallen off Total</h6>
                                </div>
                            </div>
                            <!-- Heading Ends -->

                            <!-- Bottom Table Section Starts -->
                            <div class="nbtm-row sixeighty">
                                <div class="totsub-tbl">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="75%"/>
                                            <col width="25%"/>
                                        </colgroup>

                                        <tr>
                                            <td align="left">
                                                Churned (MRR) Subscribers Tier 1 (<?=getTierMonths(2);?> Months)
                                            </td>
                                            <td align="center">
                                                <strong>$ <?=($CMRR['CMRR_1']==null)?'00.00':$CMRR['CMRR_1']?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Churned (MRR) Subscribers Tier 2 (<?=getTierMonths(3);?> Months)
                                            </td>
                                            <td align="center">
                                                <strong>$ <?=($CMRR['CMRR_2']==null)?'00.00':$CMRR['CMRR_2']?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Churned (MRR) Subscribers Tier 3 (<?=getTierMonths(4);?> Months)
                                            </td>
                                            <td align="center">
                                                <strong>$ <?=($CMRR['CMRR_3']==null)?'00.00':$CMRR['CMRR_3']?></strong>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                            <!-- Bottom Table Section Ends -->

                        </div>
                        <!-- Sub Section Ends -->

                        <!-- Sub Section Starts
                        <div class="dash-ssection">

                            Heading Starts
                            <div class="actvity-hd">
                                <div class="mtr-hdr">
                                    <h6>Net Monthly MRR (Monthly Reocurring Revenue Total)</h6>
                                </div>
                            </div>
                            Heading Ends

                            Bottom Table Section Starts
                            <div class="nbtm-row sixeighty">
                                <div class="totsub-tbl">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="75%"/>
                                            <col width="25%"/>
                                        </colgroup>

                                        <tr>
                                            <td align="left">
                                                Net (MRR) Subscribers Tier 1 (3 Months)
                                            </td>
                                            <td align="center">
                                                <strong>$00.00</strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Net (MRR) Subscribers Tier 2 (6 Months)
                                            </td>
                                            <td align="center">
                                                <strong>$00.00</strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Net (MRR) Subscribers Tier 3 (12 Months)
                                            </td>
                                            <td align="center">
                                                <strong>$00.00</strong>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                            Bottom Table Section Ends -->

                        </div>
                        <!-- Sub Section Ends -->

                        <!-- Sub Section Starts  add drop down -->
                        <div class="dash-ssection">

                            <!-- Heading Starts -->
                            <div class="actvity-hd">
                                <div class="mtr-hdr">

                                    <h6>Seller Metrics</h6>

                                    <!-- Filter By Dropdown Structure Starts -->
                                    <div class="ndpdSort newsortdrop-set ad1-pad">
                                        <button class="pro-addNew nsortBy">Filter By<span class="dn-angl"></span>
                                        </button> <!--  id="nsortBy" -->
                                        <div class="dropval"></div>
                                        <div class="ndp-nx nsortDp" style="display:none;"> <!-- id="nsortDp" -->
                                            <ul>
                                                <li>
                                                    <div class="usr-srt-dp subDpd"> Select date range <span
                                                                class="sub-dn-angl" style="top: 10px;"></span></div>

                                                    <!-- Regular Text Box Starts -->
                                                    <input value="" type="text" name="searchText" class="catName"
                                                           style="display:none;"/>
                                                    <!-- Regular Text Box Ends -->

                                                    <!-- Default Setting Do not remove starts -->
                                                    <div class="demo" style="display:none;">
                                                        Default settings: <input class="date-range0" size="40"
                                                                                 value=""/> <!-- id="date-range0" -->
                                                    </div>
                                                    <div class="demo">
                                                        <input class="date-range-03 catName" id="seller_time" name="dateRange3" size="60"
                                                               value="<?= @$_POST['dateRange'] ?>"
                                                               placeholder="Please select a date range"/>
                                                        <!-- id="date-range1" -->
                                                    </div>
                                                    <!-- Default Setting Do not remove starts -->
                                                    <input type="submit" value="Submit" id="seller_submit" class="nbtn-nrp"/>

                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Filter By Dropdown Structure Ends -->

                                </div>
                            </div>
                            <!-- Heading Ends -->

                            <!-- Bottom Table Section Starts -->
                            <div class="nbtm-row sixeighty">
                                <div class="totsub-tbl">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="75%"/>
                                            <col width="25%"/>
                                        </colgroup>

                                        <tr>
                                            <td align="left">
                                                Total Number of Profiles on the Site
                                            </td>
                                            <td align="center">
                                                <strong id="total_users">
                                                    <?= ($seller_metrics['total_users'] == null) ? '0' : $seller_metrics['total_users']; ?>
                                                </strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Total number of Sellers
                                            </td>
                                            <td align="center">
                                                <strong  id="total_sellers">
                                                    <?= ($seller_metrics['total_sellers'] == null) ? '0' : $seller_metrics['total_sellers']; ?>
                                                </strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Total Number of New sellers Added
                                            </td>
                                            <td align="center">
                                                <strong id="new_sellers">
                                                    <?= ($seller_metrics['new_sellers'] == null) ? '0' : $seller_metrics['new_sellers']; ?>
                                                </strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Seller Growth Rate Month over Month (%)
                                            </td>
                                            <td align="center">
                                                <strong id="seller_MOM">
                                                    <?= ($seller_metrics['MOM'] == null) ? '0' : number_format($seller_metrics['MOM'],2,'.',''); ?>%
                                                </strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Seller Growth Rate Year over Year (%) Same month Prev year
                                            </td>
                                            <td align="center">
                                                <strong id="seller_YOY">
                                                    <?= ($seller_metrics['YOY'] == null) ? '0' : $seller_metrics['YOY']; ?>%
                                                </strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Percentage of Sellers still active over 1 month (%)
                                            </td>
                                            <td align="center">
                                                <strong>
                                                    <?= ($seller_metrics['monthly'] <=0) ? '0.00' : $seller_metrics['monthly']; ?> %
                                                </strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Percentage of Sellers Still active over 1 year
                                            </td>
                                            <td align="center">
                                                <strong>
                                                    <?= ($seller_metrics['yearly'] <=0) ? '0.00' : $seller_metrics['yearly']; ?> %
                                                </strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Average Revenue generated per seller
                                            </td>
                                            <td align="center">
                                                <strong id="avg_rev">
                                                    <?= number_format(($seller_metrics['avg_rev'] == null) ? '0' : $seller_metrics['avg_rev'],2, '.', ','); ?>
                                                </strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Percentage of Revenue generated by top 20% Sellers
                                            </td>
                                            <td align="center">
                                                <strong id="per_rev" >
                                                    <?= number_format(($seller_metrics['per_rev'] == null) ? '0' : $seller_metrics['per_rev'],2, '.', ''); ?>%
                                                </strong>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                            <!-- Bottom Table Section Ends -->

                        </div>
                        <!-- Sub Section Ends -->

                        <!-- Sub Section Starts add drop down -->
                        <div class="dash-ssection">

                            <!-- Heading Starts -->
                            <div class="actvity-hd">
                                <div class="mtr-hdr">

                                    <h6>Total Number of listings on Site</h6>

                                    <!-- Filter By Dropdown Structure Starts -->
                                    <div class="ndpdSort newsortdrop-set ad1-pad">
                                        <button class="pro-addNew nsortBy">Filter By<span class="dn-angl"></span>
                                        </button> <!--  id="nsortBy" -->
                                        <div class="dropval"></div>
                                        <div class="ndp-nx nsortDp" style="display:none;"> <!-- id="nsortDp" -->
                                            <ul>
                                                <li>
                                                    <div class="usr-srt-dp subDpd"> Select date range <span
                                                                class="sub-dn-angl" style="top: 10px;"></span></div>

                                                    <!-- Regular Text Box Starts -->
                                                    <input value="" type="text" name="searchText" class="catName"
                                                           style="display:none;"/>
                                                    <!-- Regular Text Box Ends -->

                                                    <!-- Default Setting Do not remove starts -->
                                                    <div class="demo" style="display:none;">
                                                        Default settings: <input class="date-range0" size="40"
                                                                                 value=""/> <!-- id="date-range0" -->
                                                    </div>
                                                    <div class="demo">
                                                        <input class="date-range-04 catName" name="dateRange4" id="prod_listing" size="60"
                                                               value="<?= @$_POST['dateRange'] ?>"
                                                               placeholder="Please select a date range"/>
                                                        <!-- id="date-range1" -->
                                                    </div>
                                                    <!-- Default Setting Do not remove starts -->
                                                    <input type="submit" value="Submit" class="nbtn-nrp" id="prod_listing_submit" />

                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Filter By Dropdown Structure Ends -->

                                </div>
                            </div>
                            <!-- Heading Ends -->

                            <!-- Bottom Table Section Starts -->
                            <div class="nbtm-row sixeighty">
                                <div class="totsub-tbl">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="75%"/>
                                            <col width="25%"/>
                                        </colgroup>

                                        <tr>
                                            <td align="left">
                                                Listed for Sale
                                            </td>
                                            <td align="center">
                                                <strong id="sell"><?=$products['sell']?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Listed for Trade
                                            </td>
                                            <td align="center">
                                                <strong id="trade"><?=$products['trade']?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Listed for Donate
                                            </td>
                                            <td align="center">
                                                <strong id="donate"><?=$products['donate']?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Listings Growth Rate (%)
                                            </td>
                                            <td align="center">
                                                <strong id="rate"><?=($products['growth_rate']>0)?$products['growth_rate']:'0.00'?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Average Listing Price
                                            </td>
                                            <td align="center">
                                                <strong id="avg"><?=$products['avg']?></strong>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                            <!-- Bottom Table Section Ends -->

                        </div>
                        <!-- Sub Section Ends -->

                        <!-- Sub Section Starts add drop down -->
                        <div class="dash-ssection">

                            <!-- Heading Starts -->
                            <div class="actvity-hd">
                                <div class="mtr-hdr">

                                    <h6>Flagged Sellers</h6>

                                    <!-- Filter By Dropdown Structure Starts -->
                                    <div class="ndpdSort newsortdrop-set ad1-pad">
                                        <button class="pro-addNew nsortBy">Filter By<span class="dn-angl"></span>
                                        </button> <!--  id="nsortBy" -->
                                        <div class="dropval"></div>
                                        <div class="ndp-nx nsortDp" style="display:none;"> <!-- id="nsortDp" -->
                                            <ul>
                                                <li>
                                                    <div class="usr-srt-dp subDpd"> Select date range <span
                                                                class="sub-dn-angl" style="top: 10px;"></span></div>

                                                    <!-- Regular Text Box Starts -->
                                                    <input value="" type="text" name="searchText" class="catName"
                                                           style="display:none;"/>
                                                    <!-- Regular Text Box Ends -->

                                                    <!-- Default Setting Do not remove starts -->
                                                    <div class="demo" style="display:none;">
                                                        Default settings: <input class="date-range0" size="40"
                                                                                 value=""/> <!-- id="date-range0" -->
                                                    </div>
                                                    <div class="demo">
                                                        <input class="date-range-05 catName" name="dateRange5" id="sellervalue" size="60"
                                                               value="<?= @$_POST['dateRange'] ?>"
                                                               placeholder="Please select a date range"/>
                                                        <!-- id="date-range1" -->
                                                    </div>
                                                    <!-- Default Setting Do not remove starts -->
                                                    <input type="submit" value="Submit" class="nbtn-nrp" id="sellerSubmit" />

                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Filter By Dropdown Structure Ends -->

                                </div>
                            </div>
                            <!-- Heading Ends -->

                            <!-- Bottom Table Section Starts -->
                            <div class="nbtm-row sixeighty">
                                <div class="totsub-tbl">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="75%"/>
                                            <col width="25%"/>
                                        </colgroup>

                                        <tr>
                                            <td align="left">
                                                Sellers that were flagged
                                            </td>
                                            <td align="center">
                                                <strong id="seller_reported"><?=$reported_user_item['user_reported']?></strong>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                            <!-- Bottom Table Section Ends -->

                        </div>
                        <!-- Sub Section Ends -->

                        <!-- Sub Section Starts -->
                        <div class="dash-ssection">

                            <!-- Heading Starts -->
                            <div class="actvity-hd">
                                <div class="mtr-hdr">
                                    <h6>Buyers</h6>
                                </div>
                            </div>
                            <!-- Heading Ends -->

                            <!-- Bottom Table Section Starts -->
                            <div class="nbtm-row sixeighty">
                                <div class="totsub-tbl">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="75%"/>
                                            <col width="25%"/>
                                        </colgroup>

                                        <tr>
                                            <td align="left">
                                                Total Number of Buyers
                                            </td>
                                            <td align="center">
                                                <strong><?=$buyers['total_buyers']?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                Buyer Growth Rate month over month (%)
                                            </td>
                                            <td align="center">
                                                <strong><?=number_format($buyers['growth_rate'],2,'.','')?></strong>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                            <!-- Bottom Table Section Ends -->

                        </div>
                        <!-- Sub Section Ends -->

                        <!-- Sub Section Starts add drop down -->
                        <div class="dash-ssection">

                            <!-- Heading Starts -->
                            <div class="actvity-hd">
                                <div class="mtr-hdr">

                                    <h6>Reported / Flagged</h6>

                                    <!-- Filter By Dropdown Structure Starts -->
                                    <div class="ndpdSort newsortdrop-set ad1-pad">
                                        <button class="pro-addNew nsortBy">Filter By<span class="dn-angl"></span>
                                        </button> <!--  id="nsortBy" -->
                                        <div class="dropval"></div>
                                        <div class="ndp-nx nsortDp" style="display:none;"> <!-- id="nsortDp" -->
                                            <ul>
                                                <li>
                                                    <div class="usr-srt-dp subDpd"> Select date range <span
                                                                class="sub-dn-angl" style="top: 10px;"></span></div>

                                                    <!-- Regular Text Box Starts -->
                                                    <input value="" type="text" name="searchText" class="catName"
                                                           style="display:none;"/>
                                                    <!-- Regular Text Box Ends -->

                                                    <!-- Default Setting Do not remove starts -->
                                                    <div class="demo" style="display:none;">
                                                        Default settings: <input class="date-range0" size="40"
                                                                                 value=""/> <!-- id="date-range0" -->
                                                    </div>
                                                    <div class="demo">
                                                        <input class="date-range-06 catName" name="dateRange6" id='reportvalue' size="60"
                                                               value="<?= @$_POST['dateRange'] ?>"
                                                               placeholder="Please select a date range"/>
                                                        <!-- id="date-range1" -->
                                                    </div>
                                                    <!-- Default Setting Do not remove starts -->
                                                    <input type="submit" value="Submit" class="nbtn-nrp" id="reportSubmit"/>

                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Filter By Dropdown Structure Ends -->

                                </div>
                            </div>
                            <!-- Heading Ends -->

                            <!-- Bottom Table Section Starts -->
                            <div class="nbtm-row sixeighty">
                                <div class="totsub-tbl">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="75%"/>
                                            <col width="25%"/>
                                        </colgroup>

                                        <tr>
                                            <td align="left">
                                                # of Items Reported / Flagged
                                            </td>
                                            <td align="center">
                                                <strong id="item_reported"><?=$reported_user_item['item_reported']?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                # of Users Reported / Flagged
                                            </td>
                                            <td align="center">
                                                <strong id="user_reported"><?=$reported_user_item['user_reported']?></strong>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                            <!-- Bottom Table Section Ends -->

                        </div>
                        <!-- Sub Section Ends -->

                        <!-- Sub Section Starts add drop down -->
                        <div class="dash-ssection">

                            <!-- Heading Starts -->
                            <div class="actvity-hd">
                                <div class="mtr-hdr">

                                    <h6>Top 10 Selling Stores</h6>

                                    <!-- Filter By Dropdown Structure Starts -->
                                    <div class="ndpdSort newsortdrop-set ad1-pad">
                                        <button class="pro-addNew nsortBy">Filter By<span class="dn-angl"></span>
                                        </button> <!--  id="nsortBy" -->
                                        <div class="dropval"></div>
                                        <div class="ndp-nx nsortDp" style="display:none;"> <!-- id="nsortDp" -->
                                            <ul>
                                                <li>
                                                    <div class="usr-srt-dp subDpd"> Select date range <span
                                                                class="sub-dn-angl" style="top: 10px;"></span></div>

                                                    <!-- Regular Text Box Starts -->
                                                    <input value="" type="text" name="searchText" class="catName"
                                                           style="display:none;"/>
                                                    <!-- Regular Text Box Ends -->

                                                    <!-- Default Setting Do not remove starts -->
                                                    <div class="demo" style="display:none;">
                                                        Default settings: <input class="date-range0" size="40"
                                                                                 value=""/> <!-- id="date-range0" -->
                                                    </div>
                                                    <div class="demo">
                                                        <input class="date-range-07 catName" name="dateRange7" id="topsellingvalue" size="60"
                                                               value="<?= @$_POST['dateRange'] ?>"
                                                               placeholder="Please select a date range"/>
                                                        <!-- id="date-range1" -->
                                                    </div>
                                                    <!-- Default Setting Do not remove starts -->
                                                    <input type="submit" value="Submit" class="nbtn-nrp" id="topsellingsubmit" />

                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Filter By Dropdown Structure Ends -->

                                </div>
                            </div>
                            <!-- Heading Ends -->

                            <!-- Bottom Table Section Starts -->
                            <div class="nbtm-row onedzero">
                                <div class="totsub-tbl" id="stores">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="52%"/>
                                            <col width="24%"/>
                                            <col width="24%"/>
                                        </colgroup>

                                        <tr>
                                            <th align="left">Stores Name</th>
                                            <th align="center">Sales</th>
                                            <th align="center" class="last"># Products Sold</th>
                                        </tr>
                                        <?php foreach ($stores['top_stores'] as $i => $store) {?>
                                            <tr>                                            
                                                <td align="left"><?=$store->store_name?></td>
                                                <td align="center">
                                                    <strong>$ <?= (null == $store->revenue) ? '0.00' : number_format($store->revenue,2, '.', ' '); ?></strong>
                                                </td>
                                                <td align="center">
                                                    <strong><?=$store->sold?></strong>
                                                </td>
                                            </tr>
                                        <?php }?>
                                    </table>
                                </div>
                            </div>
                            <!-- Bottom Table Section Ends -->

                        </div>
                        <!-- Sub Section Ends -->

                        <!-- Sub Section Starts -->
                        <div class="dash-ssection">

                            <!-- Heading Starts -->
                            <div class="actvity-hd">
                                <div class="mtr-hdr">
                                    <h6>Searched For</h6>
                                </div>
                            </div>
                            <!-- Heading Ends -->

                            <!-- Bottom Table Section Starts -->
                            <div class="nbtm-row nintysix">
                                <div class="totsub-tbl">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="43%"/>
                                            <col width="11.4%"/>
                                            <col width="11.4%"/>
                                            <col width="11.4%"/>
                                            <col width="11.4%"/>
                                            <col width="11.4%"/>
                                        </colgroup>

                                        <tr>                                            
                                            <td align="left">
                                                Top 5 Categories Looked For
                                            </td>
                                            <?php foreach ($search_result['search_categories'] as $i => $cat) {?>
                                            <td align="center">
                                                <strong>
                                                    <?=$cat->cat_name?>
                                                </strong>
                                            </td>
                                            <?php }?>
                                        </tr>


                                    </table>
                                </div>
                            </div>
                            <!-- Bottom Table Section Ends -->

                        </div>
                        <!-- Sub Section Ends -->

                        <!-- Sub Section Starts -->
                        <div class="dash-ssection">

                            <!-- Heading Starts -->
                            <div class="actvity-hd">
                                <div class="mtr-hdr">
                                    <h6>Shipping Summary</h6>
                                </div>
                            </div>
                            <!-- Heading Ends -->

                            <!-- Bottom Table Section Starts -->
                            <div class="nbtm-row sixeighty">
                                <div class="totsub-tbl">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="75%"/>
                                            <col width="25%"/>
                                        </colgroup>

                                        <tr>
                                            <th align="left">List</th>
                                            <th align="center" class="last">Total</th>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                # of Items Listed (Charging Shipping)
                                            </td>
                                            <td align="center">
                                                <strong><?=$shipping_summary['charge_shipping']?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                # of Items Listed (Free Shipping)
                                            </td>
                                            <td align="center">
                                                <strong><?=$shipping_summary['free_shipping']?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left">
                                                # of Products Listed (Local Pick up)
                                            </td>
                                            <td align="center">
                                                <strong><?=$shipping_summary['local_shipping']?></strong>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                            <!-- Bottom Table Section Ends -->

                        </div>
                        <!-- Sub Section Ends -->

                    </div>
                    <!-- Bottom Section Ends -->


                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $('.nsortBy').click(function () {
        //$('.nsortDp').slideUp();
		var currentObject = $(this).parent().find('.nsortDp');
		$('.nsortDp').not(currentObject).each(function(){
			 $(this).slideUp();
		 });
        currentObject.slideToggle();
        //$('#nsortDp').slideToggle();
    });

    /*
    $('.subDpd').click(function() {
        $('.dp-dt-pik').slideToggle();
    });

    $('.valclick').click(function() {
        $('.dropval').html($(this).attr("title"));
        $('#nsortDp').slideToggle();
    });

    */


    $(document).on('click', '#marketplaceSubmit', function () {
        var date = $('#marketplaceValue').val();
        if (date != "") {

            var arrayDate = date.split("~");
            var start = arrayDate[0].trim();
            var end = arrayDate[1].trim();
            console.log(start);
            console.log(end);
            $.ajax({
                url: '<?= base_url('admin/dashboard/ajax_marketplace_dashboard') ?>/' + start + '/' + end,
                type: 'POST',
                data: {<?php echo $this->security->get_csrf_token_name(); ?>:
            '<?php echo $this->security->get_csrf_hash(); ?>'
        },
        })
        .
            done(function (res) {
                if (res != 0) {
                    //  $('.' + subFilterClass).html(res);
                    var json = JSON.parse(res);
                    // console.log(res);
                    $('#GMV').html((json.GMV == null) ? 0 : json.GMV.toFixed(2));
                    $('#transactions_number').html(json.transactions_number.toFixed(2));
                    $('#avg_order_value').html((json.avg_order_value == null) ? 0 : json.avg_order_value.toFixed(2));
                    console.log("success");
                }

            })
                .fail(function () {
                    console.log("error");
                });
        }

    });

    $(document).on('click', '#reportSubmit', function () {
        var date = $('#reportvalue').val();
        if (date != "") {

            var arrayDate = date.split("~");
            var start = arrayDate[0].trim();
            var end = arrayDate[1].trim();
            console.log(start);
            console.log(end);
            $.ajax({
                url: '<?= base_url('admin/dashboard/ajax_user_and_item_reported') ?>/' + start + '/' + end,
                type: 'POST',
                data: {<?php echo $this->security->get_csrf_token_name(); ?>:
            '<?php echo $this->security->get_csrf_hash(); ?>'
        },
        })
        .
            done(function (res) {
                if (res != 0) {
                    //  $('.' + subFilterClass).html(res);
                    var json = JSON.parse(res);
                    console.log(json);                    
                    $('#user_reported').html((json.user_reported == null) ? 0 : json.user_reported);
                    $('#item_reported').html((json.item_reported == null) ? 0 : json.item_reported);
                    console.log("success");
                }

            })
                .fail(function () {
                    console.log("error");
                });
        }

    });

    $(document).on('click', '#sellerSubmit', function () {
        var date = $('#sellervalue').val();
        if (date != "") {

            var arrayDate = date.split("~");
            var start = arrayDate[0].trim();
            var end = arrayDate[1].trim();
            console.log(start);
            console.log(end);
            $.ajax({
                url: '<?= base_url('admin/dashboard/ajax_user_and_item_reported') ?>/' + start + '/' + end,
                type: 'POST',
                data: {<?php echo $this->security->get_csrf_token_name(); ?>:
            '<?php echo $this->security->get_csrf_hash(); ?>'
        },
        })
        .
            done(function (res) {
                if (res != 0) {
                    //  $('.' + subFilterClass).html(res);
                    var json = JSON.parse(res);
                    console.log(json);                    
                    $('#seller_reported').html((json.user_reported == null) ? 0 : json.user_reported);
                    console.log("success");
                }

            })
                .fail(function () {
                    console.log("error");
                });
        }

    });

    $(document).on('click', '#prod_listing_submit', function () {
        var date = $('#prod_listing').val();
        if (date != "") {

            var arrayDate = date.split("~");
            var start = arrayDate[0].trim();
            var end = arrayDate[1].trim();
            console.log(start);
            console.log(end);
            $.ajax({
                url: '<?= base_url('admin/dashboard/ajax_product_listing') ?>/' + start + '/' + end,
                type: 'POST',
                data: {<?php echo $this->security->get_csrf_token_name(); ?>:
            '<?php echo $this->security->get_csrf_hash(); ?>'
        },
        })
        .
            done(function (res) {
                if (res != 0) {
                    //  $('.' + subFilterClass).html(res);
                    var json = JSON.parse(res);
                    console.log(json);                    
                    $('#sell').html((json.sell == null) ? 0 : json.sell);
                    $('#trade').html((json.trade == null) ? 0 : json.trade);
                    $('#donate').html((json.donate == null) ? 0 : json.donate);
                    $('#rate').html((json.growth_rate == null) ? '0.00' : json.growth_rate);
                    $('#avg').html((json.avg == null) ? '0.00' : json.avg);
                    console.log("success");
                }

            })
                .fail(function () {
                    console.log("error");
                });
        }

    });

    $(document).on('click', '#topsellingsubmit', function () {
        var date = $('#topsellingvalue').val();
        if (date != "") {

            var arrayDate = date.split("~");
            var start = arrayDate[0].trim();
            var end = arrayDate[1].trim();
            console.log(start);
            console.log(end);
            $.ajax({
                url: '<?= base_url('admin/dashboard/ajax_top_stores') ?>/' + start + '/' + end,
                type: 'POST',
                data: {<?php echo $this->security->get_csrf_token_name(); ?>:
            '<?php echo $this->security->get_csrf_hash(); ?>'
        },
        })
        .
            done(function (res) {
                console.log(res);
                if (res != 0) {
                    $('#stores').html(res);
                    console.log("success");
                }

            })
                .fail(function () {
                    console.log("error");
                });
        }

    });

    $(document).on('click', '#subscriber_submit', function () {
        var date = $('#subscriber_time').val();
        if (date != "") {

            var arrayDate = date.split("~");
            var start = arrayDate[0].trim();
            var end = arrayDate[1].trim();
            console.log(start);
            console.log(end);
            $.ajax({
                url: '<?= base_url('admin/dashboard/ajax_cancel_subscription') ?>/' + start + '/' + end,
                type: 'POST',
                data: {<?php echo $this->security->get_csrf_token_name(); ?>:
            '<?php echo $this->security->get_csrf_hash(); ?>'
        },
        })
        .
            done(function (res) {
                if (res != 0) {
                    //  $('.' + subFilterClass).html(res);
                    var json = JSON.parse(res);
                    // console.log(res);
                    $('#subscriber_value').html((json.number == null) ? 0 : json.number);
                    $('#subscriber_text').html("Cancellation Request");

                    console.log("success");
                }

            })
                .fail(function () {
                    console.log("error");
                });
        }

    });

    $(document).on('click', '#seller_submit', function () {
        var date = $('#seller_time').val();
        if (date != "") {

            var arrayDate = date.split("~");
            var start = arrayDate[0].trim();
            var end = arrayDate[1].trim();
            console.log(start);
            console.log(end);
            $.ajax({
                url: '<?= base_url('admin/dashboard/ajax_seller_metrics') ?>/' + start + '/' + end,
                type: 'POST',
                data: {<?php echo $this->security->get_csrf_token_name(); ?>:
            '<?php echo $this->security->get_csrf_hash(); ?>'
        },
        })
        .
            done(function (res) {
                if (res != 0) {
                    //  $('.' + subFilterClass).html(res);
                    var json = JSON.parse(res);
                    // console.log(res);
                    $('#total_users').html((json.total_users == null) ? 0 : json.total_users);
                    $('#total_sellers').html((json.total_sellers == null) ? 0 : json.total_sellers);
                    $('#new_sellers').html((json.new_sellers == null) ? 0 : json.new_sellers);
                    $('#seller_MOM').html(((json.MOM == null) ? 0 : json.MOM).toFixed(2));
                    $('#seller_YOY').html(((json.YOY == null) ? 0 : json.YOY).toFixed(2));
                    $('#avg_rev').html(((json.avg_rev == null) ? 0 : json.avg_rev).toFixed(2));
                    $('#per_rev').html(((json.per_rev == null) ? 0 : json.per_rev).toFixed(2));

                    console.log("success");
                }

            })
                .fail(function () {
                    console.log("error");
                });
        }

    });

    $(document).click(function (e) {
        var target = e.target;
        if (!$(target).is('.ndpdSort') && !$(target).parents().is('.ndpdSort')) {
            $('.nsortDp').slideUp();
            // $('#nsortDp').slideUp();
        }
    });

var months = [<?=$marketplace_data['months']?>];
var sales = [<?=$marketplace_data['Chart_1']?>];
var years = [<?=$marketplace_data['years']?>];
var sales_2 = [<?=$marketplace_data['Chart_2']?>];
</script>


<!-- jQuery 3 -->
<!-- <script src="<?= base_url() ?>js/chart-files/jquery/dist/jquery.min.js"></script> -->
<!-- Sparkline -->
<script src="<?= base_url() ?>js/chart-files/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- ChartJS -->
<script src="<?= base_url() ?>js/chart-files/chart.js/Chart.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?= base_url() ?>js/chart-files/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->

</body>

</html>