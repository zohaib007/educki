<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Dashboard</title>
<link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php");?> 

<script src="<?=base_url()?>js/charts/highcharts.js"></script>
<script src="<?=base_url()?>js/charts/modules/exporting.js"></script>

<!-- Date Range CSS Starts -->
<link rel="stylesheet" href="<?= base_url() ?>css/daterangepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.16.0/moment.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/demo.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/date-range/jquery.daterangepicker.js" type="text/javascript"></script>
<style type="text/css">
#display_tr {
    display: table-row !important;
}
</style>
<!-- Date Range CSS Ends -->
        
</head>
<body>
<div class="container">

	<!-- Dashboard Left Side Begins Here -->
	<div class="left_wrp">
    	
    	<?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
        
    </div>
    <!-- Dashboard Left Side Ends Here -->
    
    <div class="right-rp"> 
                <!-- Top Green Bar Section Begins Here -->
                <?php include(ADMIN_INCLUDE_PATH . "includes/top_green_bar.php"); ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="mu-contnt-hdng">
                    <h1>Dashboard</h1>
                </div>
                <div class="col-continer">
                    <div class="col-3 cat-box">
                        <div class="contntTop-row"> <!-- col-box1 -->

                            <!-- Top Header Bar Starts -->
							<div class="actvity-hd">
                            	<div class="mtr-hdr">

                                	<h6>Market Place Dashboard Summary</h6>

                                    <!-- Filter By Dropdown Structure Starts -->
                                    <div class="ndpdSort newsortdrop-set ad1-pad">
                                        <button class="pro-addNew" id="nsortBy">Filter By<span class="dn-angl"></span> </button>
                                        <div class="dropval"></div>
                                        <div class="ndp-nx" id="nsortDp" style="display:none;">
                                            <ul>
                                                <li>
                                                    <div class="usr-srt-dp subDpd"> Select date range <span class="sub-dn-angl" style="top: 10px;"></span> </div>
                                                    
                                                    <!-- Regular Text Box Starts -->
                                                    <input value="" type="text" name="searchText" class="catName" style="display:none;" />
                                                    <!-- Regular Text Box Ends -->
                                                    
                                                    <!-- Default Setting Do not remove starts -->
                                                    <div class="demo" style="display:none;">
                                                        Default settings: <input id="date-range0" size="40" value="" />
                                                    </div>
                                                    <div class="demo">
                                                        <input id="date-range1" class="catName" name="dateRange" size="60" value="<?=@$_POST['dateRange']?>" placeholder="Please select a date range" />
                                                    </div>
                                                    <!-- Default Setting Do not remove starts -->
                                                    <input type="submit" value="Submit" class="nbtn-nrp" />

                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Filter By Dropdown Structure Ends -->
                                    
                                </div>    
                            </div>
                            <!-- Top Header Bar Starts -->
                            
                            <!-- Bottom Section Starts -->
                            <div class="ndash-wrap">
                                
                            </div>
                            <!-- Bottom Section Ends -->



                            <!-- Bottom Rough Section -->
                            <div class="nsec-btm">

                                <!-- Section Starts -->
                                <div class="mtr-blk-001">
                                    <div class="mtr-hdr">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tbody><tr>
                                                <td>
                                                    Gross Merchandise Volume (GMV)
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </div>
                                    <div class="mtr-btm">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tbody><tr>
                                                <td>

                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </div>
                                </div>
                                <!-- Section Ends -->

                                <!-- Section Starts -->
                                <div class="mtr-blk-001">
                                    <div class="mtr-hdr">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tbody><tr>
                                                <td>
                                                    Total Number of Transactions Completed on Site
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </div>
                                    <div class="mtr-btm">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tbody><tr>
                                                <td>

                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </div>
                                </div>
                                <!-- Section Ends -->

                                <!-- Section Starts -->
                                <div class="mtr-blk-001">
                                    <div class="mtr-hdr">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tbody><tr>
                                                <td>
                                                    Average Order Value
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </div>
                                    <div class="mtr-btm">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tbody><tr>
                                                <td>

                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </div>
                                </div>
                                <!-- Section Ends -->

                                <!-- Section Starts -->
                                <div class="mtr-blk-001">
                                    <div class="mtr-hdr">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tbody><tr>
                                                <td>
                                                    eDucki Commissions Today
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </div>
                                    <div class="mtr-btm">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tbody><tr>
                                                <td>
                                                    $0.00                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </div>
                                </div>
                                <!-- Section Ends -->

                            </div>
                            <!-- Bottom Rough Section -->

                        </div>
                    </div>
                </div>
            </div>
</div>


<script type="text/javascript">
$('#nsortBy').click(function() {
    $('#nsortDp').slideToggle();
});

$('.subDpd').click(function() {
    $('.dp-dt-pik').slideToggle();
});

$('.valclick').click(function() {
    $('.dropval').html($(this).attr("title"));
    $('#nsortDp').slideToggle();
});

$(document).click(function(e) {
  var target = e.target;
  if (!$(target).is('.ndpdSort') && !$(target).parents().is('.ndpdSort')) {
    $('#nsortDp').slideUp();
  }
});

</script>

</body>

</html>