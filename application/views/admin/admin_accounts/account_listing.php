<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin Accounts</title>
<link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/jquery1.12.4.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/data_table.min.js"></script>
<script>
$(function () {
	$('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');
	//setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
	$('.message .close').hover(
		function() { $(this).addClass('hover'); },
		function() { $(this).removeClass('hover'); }
	);
		
	$('.message .close').click(function() {
		$(this).parent().fadeOut('slow', function() { $(this).remove(); });
	});
});
</script>
</head>

<body>
<div class="container">
    
    <!-- Dashboard Left Side Begins Here -->
    <div class="left_wrp">
        <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
    </div>
    <!-- Dashboard Left Side Ends Here -->
    
    <div class="right-rp">
        
        <!-- Top Green Bar Section Begins Here -->
        <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
        
        <!-- Top Green Bar Section Ends Here -->
        
        <div class="col-continer">
            <?php
			if($this->session->flashdata('msg') != "")
			{
			echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
			}
      		?>
            <div class="col-3 cat-box">
                <div class="contntTop-row">
                    <div class="actvity-hd">
                        <div class="act-tab b-ad-tb">
                            <table cellspacing="0" border="0">
                                <colgroup>
                                <col width="35%" />
                                <col width="35%" />
                                <col width="42%" />
                                
                                </colgroup>
                                <tr>
                                    <td class="no-bdr">
                                        <div class="cat-tbl-tp-lf">
                                            <h1>Admin Accounts</h1>
                                        </div>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td align="right">
                                        <a href="<?=base_url()?>admin/admin_accounts/add" class="pro-addNew"> Create New Admin </a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <?php 	
						if($this->session->flashdata('msg') != "") {
							//echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
						} else {
						?>
						<div id="message"></div>
						<?php } ?>
                    </div>
                    <div class="cat-tbl-contnt">
                        <table border="0" cellspacing="0" id='mytable'>
                            <colgroup>
                            <col width="10%" />
                            <col width="25%" />
                            <col width="30%" />
                            <col width="15%" />
                            <col width="20%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th align="left">Date</th>
                                    <th align="left">Name</th>
                                    <th align="left">Email</th>
                                    <th align="center">Status</th>
                                    <th align="center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
								for($n=0;$n<count($admin_list);$n++)
								{
								?>
                                <tr id="row<?=$admin_list[$n]['admin_id']?>">
                                     <td align="left">
                                        <span>
                                        <?=date('m-d-Y', strtotime($admin_list[$n]['admin_date']))?>
                                        </span>
                                    </td>
                                    <td align="left">
                                        <span>
                                        <?=$admin_list[$n]['admin_fname'].' '.$admin_list[$n]['admin_lname']?>
                                        </span>
                                    </td>
                                    <td align="left">
                                        <?=$admin_list[$n]['admin_email']?>
                                    </td>
                                    <td align="center">
                                        <?=($admin_list[$n]['admin_status'] == 0)?"Inactive":"Active" ?>
                                    </td>
                                    <td align="center">
                                        <table border="0" cellspacing="0" cellpadding="0" class="nbdr-new">
                                            <colgroup>
                                            <col width="25%" />
                                            <col width="25%" />
                                            <col width="25%" />
                                            <col width="25%" />
                                            </colgroup>
                                            <tr id="display_tr">
                                                <td>&nbsp;</td>
                                                <td id="hide_<?=$admin_list[$n]['admin_id']?>" align="center">
                                                    <a title="Edit" href="<?=base_url()?>admin/admin_accounts/edit/<?=$admin_list[$n]['admin_id']?>" class="tik-cross-btns p-edt-btn-n"><!--<img src="<?=base_url()?>images/b-edt-icon.png" alt="" />--></a>
                                                </td>
                                                <td align="center" id="delete_<?=$admin_list[$n]['admin_id']?>">
                                                    <?php if($admin_list[$n]['admin_is_superadmin'] == "0" || $admin_list[$n]['admin_id'] != 1 ) {  ?>
                                                    <a title="Delete" href="<?=base_url('admin/admin_accounts/delete/'.$admin_list[$n]['admin_id'])?>" onclick="return  confirm('Are you sure you want to delete the selected item(s)? ');"  class="tik-cross-btns  p-del-btn-n"><!--<img src="<?=base_url()?>images/b-del-icon.png" alt="" />--></a>
                                                    <?php } ?>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>
$(document).ready(function(){
    $('#mytable').DataTable({
        "paging":   true,
        "ordering": true,
        "info":     false,
        "order": [[ 0, "desc" ]],
        "columnDefs": [
           { orderable: false, targets: -1 }
        ],
        "oLanguage": { "sZeroRecords": "No records found." },
    });
});
</script>
</html>
