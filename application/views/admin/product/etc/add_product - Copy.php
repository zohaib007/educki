<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Dashboard</title>
        <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
        <?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?> 
        
        <script src="<?= base_url() ?>js/charts/highcharts.js"></script>
        <script src="<?= base_url() ?>js/charts/modules/exporting.js"></script>
        <link rel="stylesheet" href="<?php echo base_url() ?>examples/css/bootstrap.min.css" /> 
        <script type="text/javascript" src="<?php echo base_url() ?>examples/bootstrap.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url() ?>examples/css/font-awesome.min.css" />  
        <link rel="stylesheet" href="<?php echo base_url() ?>dist/summernote.css">
        <script type="text/javascript" src="<?php echo base_url() ?>dist/summernote.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery-1.9.1.min.js" type="text/javascript"></script>
        
        <script type="text/javascript" src="<?php echo base_url() ?>js/core.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/zebra_datepicker.js"></script>
        <link rel="stylesheet" href="<?= base_url() ?>css/defualt.css" type="text/css" media="all" />
        
        <script type="text/javascript">
			jQuery(document).ready(function($){
				$('.my-form .add-box').click(function(){
					var n = $('.text-box').length + 1;
					if( 5 < n ) {
						alert('Stop it!');
						return false;
					}
					   var box_html = $('<p class="text-box"><label for="box' + n + '">image <span class="box-number">' + n + '</span></label> <input type="file" name="file[]" value="" id="box' + n + '" /> <a href="#" class="remove-box">Remove</a></p>');
					box_html.hide();
					$('.my-form p.text-box:last').after(box_html);
					box_html.fadeIn('slow');
					return false;
				});
				$('.my-form').on('click', '.remove-box', function(){
				   
					$(this).parent().fadeOut("slow", function() {
						$(this).remove();
						$('.box-number').each(function(index){
							$(this).text( index + 1 );
						});
					});
					return false;
				});
			});
		</script>
            
		<script>
            $(document).ready(function(){
                $("#hide").click(function(){
                    $('#datepicker-example1').val('');
                    $('#timeid').val('');
                    $(".show_hide").hide();
                });
                $("#show").click(function(){
                    $(".show_hide").show();
                });
            });
        </script>

		<script type="text/javascript">
            $(function() {
                $.noConflict();
                $('.summernote').summernote({
                    height: 200
                });
        
                $('form').on('submit', function (e) {
                    // alert($('.summernote').code());
                });
            });
        
		
        function getmiddleCategory(sel)
        {
        
        $("#middlecategoryDiv").load(base_url+'admin/product/getmiddleCategory', {"nId":sel} );      
        }    
            
        
		function getsubCategory(sel)
		{
		
		 $("#subcategoryDiv").load(base_url+'admin/product/get_sub_cat', {"nId":sel} );      
		}    
            
        
		function hiddenpublishdate()
		{
		alert("test");
		 $(document).ready(function(){
			 
		   $("#radiohidden").click(function(){
				$('#datepicker-example1').val('');
							$('#timeid').val('');
			   
			   $(".show_hide").hide();
			   
		   });  
			 
			 
			 
		 });
		
			 
		}    

		 $(document).ready(function(){
		   $("#create_vender").click(function(){          
			   $("#show_vender_div").show();
		   });  
		 });
		
		  $(document).ready(function(){
		   $("#create_vender_1").click(function(){          
			   $("#show_vender_div_1").show();
		   });  
		 });
		   $(document).ready(function(){
		   $("#create_vender_2").click(function(){          
			   $("#show_vender_div_2").show();
		   });  
		 });
    
		</script>
	</head>
	<body>
        <div class="container_p">
        <!-- Dashboard Left Side Begins Here -->
            <div class="left_wrp">

                <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>

            </div>
            <!-- Dashboard Left Side Ends Here -->

            <div class="right-rp">

                <!-- Top Green Bar Section Begins Here -->
                <?php
                include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
                ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="mu-contnt-wrp">
                    <div class="mu-contnt-hdng">
                        <h1>Add New Product</h1>
                    </div>
  <!-- Bread crumbs starts here -->
        <div class="n-crums">
            <ul>
                <li>
                    <a href="<?php echo base_url() ?>admin/product">Products</a>
                </li>
                <li>
                    <div class="crms-sep">&gt;</div>
                </li>
                <li>
              
                    <a href="#">  Add Product</a>
                </li>
            </ul>
        </div>
        <!-- Bread crumbs ends here -->
                    <form method="post" name="form" action="<?php echo base_url();?>admin/product/prod_add" enctype="multipart/form-data">
			<?php //echo form_open(); ?>



                        <!-- Box along with label section begins here -->
                        
                        <div class="mu-contnt-outer">
                            <div class="mu-contnt-lfLbl">
                                <h2>Product details</h2>
                                <p>
                                   Provide Product ID, Product Name
                                    Select Category, Sub-Category,
                                    Description, Content and Shipping.
                                </p>
                            </div>
                            <div class="mu-contntBx-wrp">
                                <div class="contntTop-row">
                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds">
                                            <label>Product ID*</label>
                                            <input type="text" placeholder="0.00" value="<?php echo set_value('prod_user_id'); ?>" name="prod_user_id" />
                                            <div class="error">  
                                        <?php echo form_error('prod_user_id')?>
                                        </div>
                                        </div>
                                        
                                        
                                    </div>
                                        <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds_long">
                                            <label>Product Name*</label>
                                            <input type="text" value="<?php echo set_value('prod_title'); ?>" name="prod_title" placeholder="e.g Unicorn crest short sleeve tee" />
                                        </div>
                                        
                                        </div>
                                    <div class="error">  
                                    <?php echo form_error('prod_title')?>
                                    </div>
                                    
                                    
                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds_long">
                                            <label>Select category*</label>                                                                               
                                            <select name="main_cat" onchange="getmiddleCategory(this.value);">
                                                <option value="">Select  </option>
                                            <?php   for($n=0;$n<count($main_cat);$n++)
                                                { ?>
                                                    <option <?php if(set_value('main_cat')==$main_cat[$n]['cat_id']) { ?> selected <?php }?> value="<?=$main_cat[$n]['cat_id']?>"><?=$main_cat[$n]['cat_name']?></option>
                                                 <?php } ?>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="error">  
										<?php echo form_error('main_cat'); ?>
                                    </div>
                                    
                                    
                                    <div id="middlecategoryDiv"></div>
                                    <div id="subcategoryDiv"></div>
                                    
                                    <div class="mu-flds-wrp">
                                    	<div class="nlink-div">
                                        	<a href="#" title="Add More">Add More</a>
                                        </div>
                                    </div>
                                    
                                    <div class="mu-flds-wrp">
                                        <label>Description</label>
                                        <textarea  class="summernote" name="prod_des" id="desc" ><?php echo set_value('prod_des'); ?></textarea>   
                                    </div>
                                    <div class="mu-flds-wrp">
                                        <label>Content</label>
                                        <textarea  class="summernote" name="prod_content" id="desc" ><?php echo set_value(''); ?></textarea>   
                                    </div>
                                      <div class="mu-flds-wrp">
                                        <label>Shipping</label>
                                        <textarea  class="summernote" name="prod_ship_detail" id="desc" ></textarea>   
                                    </div>
                               
                                </div>
                            </div>
                        </div>
                        
                        
                        <!-- Images Section Starts -->
                        <div class="mu-contnt-outer">
                            <div class="mu-contnt-lfLbl">
                                <h2>Images</h2>
                                <p>
                                    Upload images.
                                </p>
                            </div>
                            <div class="mu-contntBx-wrp">
                                <div class="contntTop-row">
                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds">


                                                <div class="my-form">
                                                
                                                <p class="text-box">
                                                    
                                                    <label for="box1">Image* <span class="box-number">1</span></label>
                                                    <input type="file" name="file[]" value="<?php echo form_error('file[]')?>" id="box1" />
                                                    
                                                </p>
                                                      <a class="add-box" href="#">Add More</a>
                                                  
                                                    <div class="error">                                 
                                                <?php echo form_error('file')?> 
                                                    </div>
                                            </div>  
        
                                        </div>
                                            
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- Images Section Ends -->
                        
                        
                        <div class="mu-contnt-outer">
                            <div class="mu-contnt-lfLbl">
                                <h2>Prices &amp; variants</h2>
                                <p>
                                    Manage inventory, and configure the options for selling this product.
                                </p>
                            </div>
                            <div class="mu-contntBx-wrp">
                                <div class="contntTop-row">
                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds">
                                            <label>Price*</label>
                                            <input type="text" placeholder="0.00" value="<?php echo set_value('prod_price'); ?>" name="prod_price" />
                                            <div class="error">  
                                        <?php echo form_error('prod_price')?>
                                        </div>
                                        </div>
                                        <div class="mu-frmFlds mu-flt-rght">
                                            <label>Inventory*</label>
                                            <input type="text" placeholder="0.0" value="<?php echo set_value('prod_quantity'); ?>" name="prod_quantity" />
                                            <div class="error">  
                                            <?php echo form_error('prod_quantity')?>
                                        </div>
                                        </div>
                                        
                                    </div>
                                    

                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds">
                                            <label>Sale*</label>
                                            <select name="prod_sale_chk">
                                                <option value="">Select </option>                                                                                                                     
                                                <option id="create_vender"  value="1">Active</option>
                                                <option  value="0">Inactive</option>
                                                    
                                            </select>
                                          
                                       
                                            
                                            <div id="show_vender_div" style="display:block;" class="mu-flds-wrp ntxtbox">
                                                <input type="text" value="<?php echo set_value(''); ?>" placeholder="Enter Sale Price" name="sale_price" />
                                            </div>
                                        
                                        </div>
                                    	
                                           <div class="mu-frmFlds mu-flt-rght">
                                            <label>Hot*</label>
                                            <select name="prod_hot_chk">
                                                <option value="">Select </option>                                                                                                                     
                                                <option id="create_vender_1"  value="1">Active</option>
                                                <option  value="0">Inactive</option>
                                                    
                                            </select>
                                          
                                            <div class="error">  
                                            	<?php echo form_error('prod_vender')?>                            
                                        	</div>
                                            
                                            <div id="show_vender_div_1" style="display:block;" class="mu-flds-wrp ntxtbox">
                                            	<input value="<?php echo set_value(''); ?>" type="text" placeholder="Enter Hot Price" name="hot_price" />
                                            </div>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                    
                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds">
                                            <label>Ribbon*</label>
                                            <select name="prod_ribben_chk">
                                                <option value="">Select </option>                                                                                                                     
                                                <option id="create_vender_2"  value="1">Active</option>
                                                <option  value="0">Inactive</option>
                                                    
                                            </select>
                                          
                                        
                                            
                                            <div id="show_vender_div_2" style="display:block;" class="mu-flds-wrp ntxtbox space-adjust">
                                            <label>Red Ribbon Price</label>
                                            <input value="<?php echo set_value(''); ?>" type="text" placeholder="Enter Red Ribbon Price" name="red_rib_price" />
                                             <label>White Ribbon Price</label>
                                             <input value="<?php echo set_value(''); ?>" type="text" placeholder="Enter White Ribbon Price" name="white_rib_price" />
                                             <label>Black Ribbon Price</label>
                                             <input value="<?php echo set_value(''); ?>" type="text" placeholder="Enter Black Ribbon Price" name="black_rib_price" />
                                            </div>
                                        
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="mu-contnt-outer">
                            <div class="mu-contnt-lfLbl">
                                <h2>Shipping information</h2>
                                <p>
                                    Provide standard shipping price for US and Canada.
                                </p>
                            </div>
                            <div class="mu-contntBx-wrp">
                                <div class="contntTop-row">
                                	
                                    
                                    <div class="mu-flds-wrp">
                                    	
                                        <div class="mu-frmFlds"> <!--  p-rel dol -->
                                            <label>Standard ship price for US*</label>
                                            <!--<span class="dollar">$</span>-->
                                            <input type="text" value="" name="" placeholder="" />
                                            
                                            <div class="error">  
                                            	<?php echo form_error('')?>
                                        	</div>
                                        </div>
                                        
                                        <div class="mu-frmFlds mu-flt-rght">
                                            <label>Standard ship price for Canada*</label>
                                            <input type="text" value="" name="" placeholder="" />
                                            
                                            <div class="error">  
                                            	<?php echo form_error('')?>
                                        	</div>
                                        </div>
                                        
                                    </div>
                                    
                                    
                                </div>

                            </div>
                        </div>
                        
                        <div class="mu-contnt-outer">
                            <div class="mu-contnt-lfLbl">
                                <h2>Ship now information</h2>
                                <p>
                                    Provide ship now price for US and Canada.
                                </p>
                            </div>
                            <div class="mu-contntBx-wrp">
                                <div class="contntTop-row">
                                	
                                    
                                    <div class="mu-flds-wrp">
                                    	
                                        <div class="mu-frmFlds">
                                            <label>Ship Now*</label>
                                            <select name="">
                                                <option value="">Select </option>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                            
                                            <div class="error">  
                                            	<?php echo form_error('')?>
                                        	</div>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="mu-flds-wrp" style="display:block;">
                                    	
                                        <div class="mu-frmFlds"> <!--  p-rel dol -->
                                            <label>Ship now price for US</label>
                                            <!-- <span class="dollar">$</span> -->
                                            <input type="text" value="" name="" placeholder="" />
                                            
                                            <div class="error">  
                                            	<?php echo form_error('')?>
                                        	</div>
                                        </div>
                                        
                                        <div class="mu-frmFlds mu-flt-rght">
                                            <label>Ship now price for Canada</label>
                                            <input type="text" value="" name="" placeholder="" />
                                            
                                            <div class="error">  
                                            	<?php echo form_error('')?>
                                        	</div>
                                        </div>
                                        
                                    </div>
                                    
                                    
                                </div>

                            </div>
                        </div>
                        
                        
                        <!-- Reginal Table Section Starts -->
                        <div class="mu-contnt-outer">
                            <div class="mu-contnt-lfLbl">
                                <h2>Shipping table*</h2>
                                <p>
                                    Provide price for Overnight, 2<sup>nd</sup> Day, 3<sup>rd</sup> Day.
                                </p>

                            </div>
                            <div class="mu-contntBx-wrp">
                                <div class="contntTop-row">
									
                                    <!-- Table Starts Here -->
                                    <div class="ntbl-rp">
                                    	<table cellpadding="0" cellspacing="0" border="0">
                                        	<colgroup>
                                            	<col width="16%" />
                                            	<col width="12%" />
                                            	<col width="12%" />
                                            	<col width="12%" />
                                            	<col width="12%" />
                                            	<col width="12%" />
                                            	<col width="12%" />
                                            	<col width="12%" />
                                            </colgroup>
                                            
                                        	<tr>
                                            	<th>&nbsp;</th>
                                            	<th>Region 1</th>
                                            	<th>Region 2</th>
                                            	<th>Region 3</th>
                                            	<th>Region 4</th>
                                            	<th>Region 5</th>
                                            	<th>Region 6</th>
                                            	<th>Region 7</th>
                                            </tr>
                                            
                                        	<tr>
                                            	<td>
                                                	<strong>Overnight</strong>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            </tr>
                                            
                                        	<tr>
                                            	<td>
                                                	<strong>2<sup>nd</sup> Day</strong>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            </tr>
                                            
                                        	<tr>
                                            	<td>
                                                	<strong>3<sup>rd</sup> Day</strong>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            	<td>
                                                	<div class="mu-frmFlds_long">
                                                        <input type="text" value="" name="" placeholder="" />
                                                    </div>
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </div>
                                    <!-- Table Ends Here -->
                                    
                                    <!-- Error Div Starts Here -->
                                    <div class="nerror-rp">
                                    	Error comes here. <br />
                                    	Error comes here. <br />
                                    	Error comes here.
                                    </div>
                                    <!-- Error Div Ends Here -->
                                    
                                    
                                </div>

                            </div>
                        </div>
                        <!-- Reginal Table Section Ends -->
                        
                        
                        <div class="mu-contnt-outer">
                            <div class="mu-contnt-lfLbl">
                                <h2>May we suggest</h2>
                                <p>
                                    Provide name of products.
                                </p>
                            </div>
                            <div class="mu-contntBx-wrp">
                                <div class="contntTop-row">
                                	
                                    <!-- Product 1 Starts -->
                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds_long">
                                            <label>First Product</label>
                                            <input type="text" value="<?php echo set_value(''); ?>" name="" placeholder="Product Name" />
                                        </div>
                                    </div>
                                    <div class="error">  
                                    	<?php echo form_error('')?>
                                    </div>
                                    <!-- Product 1 Ends -->
                                    
                                    <!-- Product 2 Starts -->
                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds_long">
                                            <label>Second Product</label>
                                            <input type="text" value="<?php echo set_value(''); ?>" name="" placeholder="Product Name" />
                                        </div>
                                    </div>
                                    <div class="error">  
                                    	<?php echo form_error('')?>
                                    </div>
                                    <!-- Product 2 Ends -->
                                    
                                    <!-- Product 3 Starts -->
                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds_long">
                                            <label>Third Product</label>
                                            <input type="text" value="<?php echo set_value(''); ?>" name="" placeholder="Product Name" />
                                        </div>
                                    </div>
                                    <div class="error">  
                                    	<?php echo form_error('')?>
                                    </div>
                                    <!-- Product 3 Ends -->
                                    
                                    <!-- Product 4 Starts -->
                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds_long">
                                            <label>Fourth Product</label>
                                            <input type="text" value="<?php echo set_value(''); ?>" name="" placeholder="Product Name" />
                                        </div>
                                    </div>
                                    <div class="error">  
                                    	<?php echo form_error('')?>
                                    </div>
                                    <!-- Product 4 Ends -->
                                    
                                    <!-- Product 5 Starts -->
                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds_long">
                                            <label>Fifth Product</label>
                                            <input type="text" value="<?php echo set_value(''); ?>" name="" placeholder="Product Name" />
                                        </div>
                                    </div>
                                    <div class="error">  
                                    	<?php echo form_error('')?>
                                    </div>
                                    <!-- Product 5 Ends -->
                                    
                                    <!-- Product 6 Starts -->
                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds_long">
                                            <label>Sixth Product</label>
                                            <input type="text" value="<?php echo set_value(''); ?>" name="" placeholder="Product Name" />
                                        </div>
                                    </div>
                                    <div class="error">  
                                    	<?php echo form_error('')?>
                                    </div>
                                    <!-- Product 6 Ends -->
                                    
                                    
                                </div>

                            </div>
                        </div>
                        
                        
                        <div class="mu-contnt-outer">
                            <div class="mu-contnt-lfLbl">
                                <h2>Search engines</h2>
                                <p>
                                   Set up the meta title, meta keyword and meta description. These help define how this product shows up on 
                                   search engines.
                                </p>

                            </div>
                            <div class="mu-contntBx-wrp">
                                <div class="contntTop-row">


                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds_long">
                                            <label>Meta title 0 of 70 characters used</label>
                                            <input type="text" value="<?php echo form_error('prod_page_title')?>" name="prod_page_title" />
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds_long">
                                            <label>Meta Keyword</label>
                                            <input type="text" value="" name="" />
                                        </div>
                                    </div>
                                    

                                    <div class="mu-flds-wrp">

                                        <div class="mu-frmFlds_long">
                                            <label>Meta description 0 of 160 characters used</label>
                                            <textarea class="text_meta" value="<?php echo form_error('prod_meta_des')?>" name="prod_meta_des"><?php echo form_error('prod_meta_des')?></textarea>

                                        </div>


                                    </div>

                                 
                                </div>

                            </div>
                        </div>
                        
                        
                        <div class="mu-contnt-outer">
                            <div class="mu-contnt-lfLbl">
                                <h2>Product status</h2>
                                <p>
                                    Select product status active or inactive.
                                </p>

                            </div>
                            <div class="mu-contntBx-wrp">
                                <div class="contntTop-row">
									
                                    
                                    <div class="mu-flds-wrp">
                                        <div class="mu-frmFlds_long">
                                            <label>Status*</label>
                                            <select name="">
                                                <option value="">Select</option>
                                                <option value="1">Active</option>
                                                <option value="0">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    
                                </div>

                            </div>
                        </div>
                        
                        
                        <!-- Box along with label section ends here -->
						
                        <!-- Box wihout label section begins here -->

                        <div class="mu-fld-sub">
                            <input type="submit" value="Submit" />
                            <a href="<?=base_url()?>admin/product">cancel</a>
                        </div>

                        <!-- Box wihout label section ends here -->

                    </form>
                </div>

            </div>
        </div>
    </body>

</html>