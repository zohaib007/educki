<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Dashboard</title>
		<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
        
        <script type="text/javascript" src="<?=base_url()?>js/jquery.min.js"></script>
        
		<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
        <script src="<?= base_url() ?>js/charts/highcharts.js"></script>
		<script src="<?= base_url() ?>js/charts/modules/exporting.js"></script>
		<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
		<script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="<?php echo base_url() ?>js/editor/examples/css/font-awesome.min.css" />
		<link rel="stylesheet" href="<?php echo base_url() ?>js/editor/dist/summernote.css">
		<script type="text/javascript" src="<?php echo base_url() ?>js/editor/dist/summernote.js"></script>
		<script src="<?php echo base_url(); ?>js/jquery-1.9.1.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>js/core.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>js/zebra_datepicker.js"></script>
		<link rel="stylesheet" href="<?= base_url() ?>css/defualt.css" type="text/css" media="all" />
		
		<script type="text/javascript">
		
			
			/****************************************************************************************/
			jQuery(document).ready(function($){
				$('.my-form .add-box').click(function(){
					var n = $('.text-box').length + 1;
					if( 3 < n )
					{
						alert('Stop it!');
						return false;
					}
					var box_html = $('<p class="text-box"><label for="box' + n + '">image <span class="box-number">' + n + '</span></label> <input type="file" name="file[]" value="" id="box' + n + '" /> <a href="#" class="remove-box">Remove</a></p>');
					box_html.hide();
					$('.my-form p.text-box:last').after(box_html);
					box_html.fadeIn('slow');
					return false;
				});
				$('.my-form').on('click', '.remove-box', function(){
				   
					$(this).parent().fadeOut("slow", function() {
						$(this).remove();
						$('.box-number').each(function(index){
							$(this).text( index + 1 );
						});
					});
					return false;
				});
			
			
			
			/************  [Multi category append]  *************/
			
				$('.add_more_cat').click(function(){
					//alert ( $('.cat_box').length);
					var n = $('.cat_box').length + 1;
					/*if( 3 < n )
					{
						alert('Stop it!');
						return false;
					}*/
					var box_html = $('<div class="cat_box" ><div class="mu-flds-wrp"><div class="mu-frmFlds_long"><label>Select category*</label><select name="main_cat[]" onchange="getmiddleCategory(this.value,'+ n +');"><option value="">Select </option><?php foreach($main_cat as $val) { ?><option <?php if(set_value('main_cat') == $val['cat_id']) { ?> selected <?php }?> value="<?=$val['cat_id']?>"><?php echo $val['cat_name'];?></option><?php } ?></select><div class="error"> <?php echo form_error('main_cat[]'); ?> </div></div></div><div id="middlecategoryDiv'+ n +'"></div><a href="javascript:void(0)" class="remove_box_cat">Remove</a></p>');
					box_html.hide();
					$('.cat_box:last').after(box_html);
					box_html.fadeIn('slow');
					return false;
				});
				
				
				$('.cat_form').on('click', '.remove_box_cat', function(){
				   
					$(this).parent().fadeOut("slow", function() {
						$(this).remove();
						/*$('.box-number').each(function(index){
							$(this).text( index + 1 );
						});*/
					});
					return false;
				});
			});
			
			
		</script>
		<script>
			
			
			 
			
			
            $(document).ready(function(){
                $("#hide").click(function(){
                    $('#datepicker-example1').val('');
                    $('#timeid').val('');
                    $(".show_hide").hide();
                });
                $("#show").click(function(){
                    $(".show_hide").show();
                });
            });
        </script>
		<script type="text/javascript">
            $(function() {
                $.noConflict();
                $('.summernote').summernote({
                    height: 200
                });
        
                $('form').on('submit', function (e) {
                    // alert($('.summernote').code());
                });
            });
        
		
		function getmiddleCategory(sel, display_div)
		{
			
			//alert(display_div);
			$("#middlecategoryDiv"+display_div).load(base_url+'admin/product/getmiddleCategory', {"nId":sel, <?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'} );
		}    
            
        
		/*function getsubCategory(sel)
		{
		
		 $("#subcategoryDiv").load(base_url+'admin/product/get_sub_cat', {"nId":sel, <?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'} );
		}  */  
            
        
		function hiddenpublishdate()
		{
			//alert("test");
			$(document).ready(function(){
				$("#radiohidden").click(function(){
					$('#datepicker-example1').val('');
					$('#timeid').val('');
					$(".show_hide").hide();
				});  
			});
		}    

		</script>
		<script>
		
			$(document).ready(function(){
				
				var sale_val = $("select[name='prod_onsale']").val();
				var hot_val = $("select[name='prod_meltable']").val();
				var ship_val = $("select[name='ship_now']").val();
				var ribbon_val = $("select[name='prod_ribbon']").val();
				
				if(sale_val == '1')
				{
					$('#on_sale_id').css('display', 'block');
				}
				else
				{
					$('#on_sale_id').css('display', 'none');
				}
				
				if(hot_val == '1')
				{
					$('#hot_id').css('display', 'block');
				}
				else
				{
					$('#hot_id').css('display', 'none');
				}
				
				if(ship_val == '1')
				{
					$('#shipnow_id').css('display', 'block');
				}
				else
				{
					$('#shipnow_id').css('display', 'none');
				}
				
				if(ribbon_val == '1')
				{
					$('#ribbon_id').css('display', 'block');
				}
				else
				{
					$('#ribbon_id').css('display', 'none');
				}
			});
			
			//onchange="show_div(this.value);"
			function show_div(n, id)
			{
				//alert(n+" --> "+id);
				if(n==1)
				{
					$('#'+id).css('display', 'block');
				}
				else
				{
					$('#'+id).css('display', 'none');
				}
			}
		


	function related_product_data(field_name, id_name)
	{
		//alert(field_name+" -->"+id_name)
		var prod1, prod2, prod3, prod4, prod5, prod6;
		
		$("input[name='"+field_name+"']").keyup(function(){
			//alert(field_name);
			//return false;
			if(field_name == 'one')
			{
				prod1 = document.getElementById('one').value;
				prod2 = document.getElementById('three_id').value;
				prod3 = document.getElementById('five_id').value;
				prod4 = document.getElementById('two_id').value;
				prod5 = document.getElementById('four_id').value;
				prod6 = document.getElementById('six_id').value;
			}
			else if(field_name == 'two')
			{
				prod1 = document.getElementById('two').value;
				prod2 = document.getElementById('one_id').value;
				prod3 = document.getElementById('three_id').value;
				prod4 = document.getElementById('four_id').value;
				prod5 = document.getElementById('five_id').value;
				prod6 = document.getElementById('six_id').value;
			}
			else if(field_name == 'three')
			{
				prod1 = document.getElementById('three').value;
				prod2 = document.getElementById('one_id').value;
				prod3 = document.getElementById('two_id').value;
				prod4 = document.getElementById('four_id').value;
				prod5 = document.getElementById('five_id').value;
				prod6 = document.getElementById('six_id').value;
			}
			else if(field_name == 'four')
			{
				prod1 = document.getElementById('four').value;
				prod2 = document.getElementById('one_id').value;
				prod3 = document.getElementById('two_id').value;
				prod4 = document.getElementById('three_id').value;
				prod5 = document.getElementById('five_id').value;
				prod6 = document.getElementById('six_id').value;
			}
			else if(field_name == 'five')
			{
				prod1 = document.getElementById('five').value;
				prod2 = document.getElementById('one_id').value;
				prod3 = document.getElementById('two_id').value;
				prod4 = document.getElementById('three_id').value;
				prod5 = document.getElementById('four_id').value;
				prod6 = document.getElementById('six_id').value;
			}
			else if(field_name == 'six')
			{
				prod1 = document.getElementById('six').value;
				prod2 = document.getElementById('one_id').value;
				prod3 = document.getElementById('two_id').value;
				prod4 = document.getElementById('three_id').value;
				prod5 = document.getElementById('four_id').value;
				prod6 = document.getElementById('five_id').value;
			}
			
			
			//var input_name = "'"+field_name+"'";
			
			if(prod1.length >= 2)
			{
				
				$.ajax({
					type: "POST",
					url: base_url + "admin/product/related_products/"+field_name,
					//data:{field_name : variable_name},
					data:{prod1:prod1,prod2:prod2, prod3:prod3, prod4:prod4, prod5:prod5, prod6:prod6, <?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'},			
					success: function(res)
					{
						//$("#thelist").html('');
						if(res == '')
						{
							$("."+field_name).hide();
						}
						else
						{
							$("."+field_name).show();
							
							$("#"+id_name).html(res);
							
						}
					},
					error: function(){
						//alert('failure');
					}
				});
			}
			else if(prod1.length <= 1)
			{
				$("."+field_name).hide();
				//$("input[name='"+field_name+"']").val('');
				$("input[name='"+field_name+"_id']").val('');
			}
			//$(".fleft").show();
		});
		/* First Left Ends */
	}

	function get_related_product(field_name, prod_id, prod_name)
	{
		//alert(field_name+'-->'+prod_id+'-->'+prod_name);
		$("input[name='"+field_name+"']").val(prod_name);
		$("input[name='"+field_name+"_id']").val(prod_id);
		$("."+field_name).hide();
	}


	function get_prod_name(value)
	{
		//alert(value);
		$("input[name='name']").val(value);	
		$(".dyn-listing").hide();
	}
	
	
	
	$("#box1").change(function(){
			var x = $("#box1").val();
			alert (x);
			
		});
		
	/*
	Sir Ali new code 10-11-2015
	$(document).ready(function(){
		
		
		
		$("input[name='fleft']").keyup(function(){
			$(".fleft").show();
		});
		
		$("input[name='fleft']").focusout(function(){
			$(".fleft").hide();
		});
		//first Left end
		
		
	});
	
	function get_prod_name(value)
	{
		//alert(value);
		$("input[name='name']").val(value);	
		$(".dyn-listing").hide();
	}
	*/

</script>

		</head>
		<body>
        <div class="container_p"> 
            <!-- Dashboard Left Side Begins Here -->
            <div class="left_wrp">
                <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
            </div>
            <!-- Dashboard Left Side Ends Here -->
            
            <div class="right-rp"> 
                
                <!-- Top Green Bar Section Begins Here -->
                <?php
                include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
                ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="mu-contnt-wrp">
                    <div class="mu-contnt-hdng">
                        <h1>Add New Product</h1>
                    </div>
                    <!-- Bread crumbs starts here -->
                    <div class="n-crums">
                        <ul>
                            <li> <a href="<?php echo base_url() ?>admin/product">Products</a> </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li> <a href="#"> Add Product</a> </li>
                        </ul>
                    </div>
                    <!-- Bread crumbs ends here --> 
                    <!--      <form method="post" name="form" action="<?php echo base_url();?>admin/product/prod_add" enctype="multipart/form-data">--> 
                    <?php echo form_open_multipart('', array('name'=>"form", 'id'=>"form","onsubmit"=>"return confirm('Please confirm your information. Are you ready to submit?')")); ?> 
                    
                    <!-- Box along with label section begins here -->
                    
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl">
                            <h2>Product details</h2>
                            <p> Provide Product ID, Product Name
                                Select Category, Sub-Category,
                                Description, Content and Shipping. </p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <label>Enter SKU*</label>
                                        <input type="text" placeholder="0.00" value="<?php echo set_value('prod_sku'); ?>" name="prod_sku" />
                                        <div class="error"> <?php echo form_error('prod_sku')?> </div>
                                    </div>
                                </div>
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Enter Product Name*</label>
                                        <input type="text" value="<?php echo set_value('prod_name'); ?>" name="prod_name" placeholder="e.g Unicorn crest short sleeve tee" />
                                    </div>
                                </div>
                                <div class="error"> <?php echo form_error('prod_name')?> </div>
                                
                                
								<div class="cat_form">
                                    <div class="cat_box">
                                        <div class="mu-flds-wrp">
                                        	
                                            <div class="mu-frmFlds">
                                                <label>Select category*</label>
                                                <select name="main_cat[]" onchange="getmiddleCategory(this.value,1);" size="7" multiple="multiple" class="multi-select">
                                                    <?php foreach($main_cat as $val) { ?>
                                                    <option <?php if(set_value('main_cat') == $val['cat_id']) { ?> selected <?php }?> value="<?=$val['cat_id']?>">
                                                    <?php echo $val['cat_name'];?>
                                                    </option>
                                                    <?php } ?>
                                                </select>
                                                <div class="error"> <?php echo form_error('main_cat[]'); ?> </div>
                                            </div>
                                            
                                            <div class="mu-frmFlds mu-flt-rght">
                                                <label>Select sub category*</label>
                                                <select size="7" multiple="multiple" class="multi-select">
                                                    <option>Sub category 1</option>
                                                    <option>Sub category 2</option>
                                                    <option>Sub category 3</option>
                                                    <option>Sub category 4</option>
                                                    <option>Sub category 5</option>
                                                    <option>Sub category 6</option>
                                                    <option>Sub category 7</option>
                                                    <option>Sub category 8</option>
                                                </select>
                                                <div class="error"> <?php echo form_error(''); ?> </div>
                                            </div>
                                            
                                        </div>
                                        
                                        <div id="middlecategoryDiv1"></div>
                                    </div>
                                <!--<div id="subcategoryDiv"></div>-->
                                </div>
                                
                                
                                
                                <div class="mu-flds-wrp" style="display:none;">
                                    <div class="nlink-div"> <a href="javascript:void(0)" class="add_more_cat" title="Add More">Add More</a> </div>
                                </div>
                                
                                <div class="mu-flds-wrp">
                                    <label>Description</label>
                                    <textarea  class="summernote" name="prod_dec" id="desc" ><?php echo set_value('prod_dec'); ?></textarea>
                                </div>
                                <div class="error"> <?php echo form_error('prod_dec'); ?> </div>
                                <div class="mu-flds-wrp">
                                    <label>Content Details</label>
                                    <textarea  class="summernote" name="prod_content" id="desc" ><?php echo set_value('prod_content'); ?></textarea>
                                </div>
                                <div class="error"> <?php echo form_error('prod_content'); ?> </div>
                                <div class="mu-flds-wrp">
                                    <label>Shipping Details</label>
                                    <textarea  class="summernote" name="prod_shipping" id="desc" ><?php echo set_value('prod_shipping'); ?></textarea>
                                </div>
                                <div class="error"> <?php echo form_error('prod_shipping'); ?> </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Images Section Starts -->
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl">
                            <h2>Images</h2>
                            <p> Upload images. </p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <div class="my-form">
                                            <p class="text-box">
                                                <label for="box1">Image* <span class="box-number">1</span></label>
                                                <input type="file" name="file[]" value="" id="box1" />
                                                <input type="text" name="hidden_file" value="" id="hidden_file" />
                                            </p>
                                            
                                            <a class="add-box" href="javascript:void(0)">Add More</a>
                                            <div class="error"> <?php echo form_error('file[]'); ?>
											<?php if($this->session->flashdata('msg_err')!= ''){ echo $this->session->flashdata('msg_err'); }?> </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Images Section Ends -->
                    
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl">
                            <h2>Prices &amp; variants</h2>
                            <p> Manage inventory, and configure the options for selling this product. </p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <label>Enter Price*</label>
                                        <input type="text" placeholder="0.00" value="<?php echo set_value('prod_price'); ?>" name="prod_price" />
                                        <div class="error"> <?php echo form_error('prod_price')?> </div>
                                    </div>
                                    <div class="mu-frmFlds mu-flt-rght">
                                        <label>Enter Inventory*</label>
                                        <input type="text" placeholder="0.0" value="<?php echo set_value('prod_qty'); ?>" name="prod_qty" />
                                        <div class="error"> <?php echo form_error('prod_qty')?> </div>
                                    </div>
                                </div>
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <label>Choose Sale*</label>
                                        <select name="prod_onsale" id="prod_onsale" onchange="show_div(this.value,'on_sale_id');">
                                            <option value="">Select </option>
                                            <option value="1" <?php echo set_select('prod_onsale', '1'); ?> >Active</option>
                                            <option value="0" <?php echo set_select('prod_onsale', '0'); ?>>Inactive</option>
                                        </select>
                                        <div class="error"> <?php echo form_error('prod_onsale')?> </div>
                                        
                                        <!--<div id="show_vender_div" style="display:block;" class="mu-flds-wrp ntxtbox">-->
                                        <div id="on_sale_id" style="display:none;" class="mu-flds-wrp ntxtbox">
                                            <input type="text" value="<?php echo set_value('prod_sale_price'); ?>" placeholder="Enter Sale Price of on-sale" name="prod_sale_price" />
                                            <div class="error"> <?php echo form_error('prod_sale_price');?> </div>
                                        </div>
                                    </div>
                                    <div class="mu-frmFlds mu-flt-rght">
                                        <label>Meltable*</label>
                                        <select name="prod_meltable" onchange="show_div(this.value,'hot_id');">
                                            <option value="">Select </option>
                                            <option value="1" <?php echo set_select('prod_meltable', '1'); ?> >Active</option>
                                            <option value="0" <?php echo set_select('prod_meltable', '0'); ?>>Inactive</option>
                                        </select>
                                        <div class="error"> <?php echo form_error('prod_meltable')?> </div>
                                        <div id="hot_id" style="display:none;" class="mu-flds-wrp ntxtbox">
                                            <input value="<?php echo set_value('prod_meltable_price'); ?>" type="text" placeholder="Enter addition price for hot" name="prod_meltable_price" />
                                        	<div class="error"> <?php echo form_error('prod_meltable_price');?> </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <label>Ribbon*</label>
                                        <select name="prod_ribbon" onchange="show_div(this.value,'ribbon_id');">
                                            <option value="">Select </option>
                                            <option value="1" <?php echo set_select('prod_ribbon', '1'); ?> >Active</option>
                                            <option value="0" <?php echo set_select('prod_ribbon', '0'); ?>>Inactive</option>
                                        </select>
                                        <div class="error"> <?php echo form_error('prod_ribbon')?> </div>
                                        <div id="ribbon_id" style="display:none;" class="mu-flds-wrp ntxtbox space-adjust">
                                            <label>Red Ribbon Price</label>
                                            <input value="<?php echo set_value('prod_red_ribbon_price'); ?>" type="text" placeholder="Enter Red Ribbon Price" name="prod_red_ribbon_price" />
                                            <div class="error"><?php echo form_error('prod_red_ribbon_price')?></div>
                                            <label>White Ribbon Price</label>
                                            <input value="<?php echo set_value('prod_white_ribbon_price'); ?>" type="text" placeholder="Enter White Ribbon Price" name="prod_white_ribbon_price" />
                                            <div class="error"><?php echo form_error('prod_white_ribbon_price')?></div>
                                            <label>Black Ribbon Price</label>
                                            <input value="<?php echo set_value('prod_black_ribbon_price'); ?>" type="text" placeholder="Enter Black Ribbon Price" name="prod_black_ribbon_price" />
                                            <div class="error"><?php echo form_error('prod_black_ribbon_price')?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl">
                            <h2>Shipping information</h2>
                            <p> Provide standard shipping price for US and Canada. </p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds"> <!--  p-rel dol -->
                                        <label>Enter Standard shipping price USA*</label>
                                        <!--<span class="dollar">$</span>-->
                                        <input type="text" value="<?php echo set_value('prod_stnd_ship_us'); ?>" name="prod_stnd_ship_us" placeholder="0.00" />
                                        <div class="error"> <?php echo form_error('prod_stnd_ship_us')?> </div>
                                    </div>
                                    <div class="mu-frmFlds mu-flt-rght">
                                        <label>Enter Standard shipping price Canada*</label>
                                        <input type="text" value="<?php echo set_value('prod_stnd_ship_ca'); ?>" name="prod_stnd_ship_ca" placeholder="0.00" />
                                        <div class="error"> <?php echo form_error('prod_stnd_ship_ca')?> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl">
                            <h2>Ship now information</h2>
                            <p> Provide ship now price for US and Canada. </p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <label>Ship Now*</label>
                                        <select name="ship_now" onchange="show_div(this.value,'shipnow_id');">
                                            <option value="">Select </option>
                                            <option value="1" <?php echo set_select('ship_now', '1'); ?> >Active</option>
                                            <option value="0" <?php echo set_select('ship_now', '0'); ?>>Inactive</option>
                                        </select>
                                        <div class="error"> <?php echo form_error('ship_now')?> </div>
                                    </div>
                                </div>
                                <div class="mu-flds-wrp" style="display:none;" id="shipnow_id" >
                                    <div class="mu-frmFlds"> <!--  p-rel dol -->
                                        <label>Enter Ship now price USA*</label>
                                        <!-- <span class="dollar">$</span> -->
                                        <input type="text" value="<?php echo set_value('prod_ship_now_us'); ?>" name="prod_ship_now_us" placeholder="0.00" />
                                        <div class="error"> <?php echo form_error('prod_ship_now_us')?> </div>
                                    </div>
                                    <div class="mu-frmFlds mu-flt-rght">
                                        <label>Enter Ship now price Canada*</label>
                                        <input type="text" value="<?php echo set_value('prod_ship_now_ca'); ?>" name="prod_ship_now_ca" placeholder="0.00" />
                                        <div class="error"> <?php echo form_error('prod_ship_now_ca')?> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Reginal Table Section Starts -->
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl">
                            <h2>Shipping table*</h2>
                            <p> Provide price for Overnight, 2<sup>nd</sup> Day, 3<sup>rd</sup> Day. </p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row"> 
                                
                                <!-- Table Starts Here -->
                                <div class="ntbl-rp">
                                    <table cellpadding="0" cellspacing="0" border="0">
                                        <colgroup>
                                        <col width="16%" />
                                        <col width="12%" />
                                        <col width="12%" />
                                        <col width="12%" />
                                        <col width="12%" />
                                        <col width="12%" />
                                        <col width="12%" />
                                        <col width="12%" />
                                        </colgroup>
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>Region 1</th>
                                            <th>Region 2</th>
                                            <th>Region 3</th>
                                            <th>Region 4</th>
                                            <th>Region 5</th>
                                            <th>Region 6</th>
                                            <th>Region 7</th>
                                        </tr>
                                        <tr>
                                            <td><strong>Overnight</strong></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r1_1'); ?>" name="prod_r1_1" placeholder="0.00" />
                                                </div></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r2_1'); ?>" name="prod_r2_1" placeholder="0.00" />
                                                </div></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r3_1'); ?>" name="prod_r3_1" placeholder="0.00" />
                                                </div></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r4_1'); ?>" name="prod_r4_1" placeholder="0.00" />
                                                </div></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r5_1'); ?>" name="prod_r5_1" placeholder="0.00" />
                                                </div></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r6_1'); ?>" name="prod_r6_1" placeholder="0.00" />
                                                </div></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r7_1'); ?>" name="prod_r7_1" placeholder="0.00" />
                                                </div></td>
                                        </tr>
                                        <tr>
                                            <td><strong>2<sup>nd</sup> Day</strong></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r1_2'); ?>" name="prod_r1_2" placeholder="0.00" />
                                                </div></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r2_2'); ?>" name="prod_r2_2" placeholder="0.00" />
                                                </div></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r3_2'); ?>" name="prod_r3_2" placeholder="0.00" />
                                                </div></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r4_2'); ?>" name="prod_r4_2" placeholder="0.00" />
                                                </div></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r5_2'); ?>" name="prod_r5_2" placeholder="0.00" />
                                                </div></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r6_2'); ?>" name="prod_r6_2" placeholder="0.00" />
                                                </div></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r7_2'); ?>" name="prod_r7_2" placeholder="0.00" />
                                                </div></td>
                                        </tr>
                                        <tr>
                                            <td><strong>3<sup>rd</sup> Day</strong></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r1_3'); ?>" name="prod_r1_3" placeholder="0.00" />
                                                </div></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r2_3'); ?>" name="prod_r2_3" placeholder="0.00" />
                                                </div></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r3_3'); ?>" name="prod_r3_3" placeholder="0.00" />
                                                </div></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r4_3'); ?>" name="prod_r4_3" placeholder="0.00" />
                                                </div></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r5_3'); ?>" name="prod_r5_3" placeholder="0.00" />
                                                </div></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r6_3'); ?>" name="prod_r6_3" placeholder="0.00" />
                                                </div></td>
                                            <td><div class="mu-frmFlds_long">
                                                    <input type="text" value="<?php echo set_value('prod_r7_3'); ?>" name="prod_r7_3" placeholder="0.00" />
                                                </div></td>
                                        </tr>
                                    </table>
                                </div>
                                <!-- Table Ends Here --> 
                                
                                <!-- Error Div Starts Here -->
                                <div class="nerror-rp"> 
									<?php echo form_error('prod_r1_1');?>
                                    <?php echo form_error('prod_r2_1');?>
                                    <?php echo form_error('prod_r3_1');?>
                                    <?php echo form_error('prod_r4_1');?>
                                    <?php echo form_error('prod_r5_1');?>
                                    <?php echo form_error('prod_r6_1');?>
                                    <?php echo form_error('prod_r7_1');?>
                                    
									<?php echo form_error('prod_r1_2');?>
                                    <?php echo form_error('prod_r2_2');?>
                                    <?php echo form_error('prod_r3_2');?>
                                    <?php echo form_error('prod_r4_2');?>
                                    <?php echo form_error('prod_r5_2');?>
                                    <?php echo form_error('prod_r6_2');?>
                                    <?php echo form_error('prod_r7_2');?>
                                    
									<?php echo form_error('prod_r1_3');?>
                                    <?php echo form_error('prod_r2_3');?>
                                    <?php echo form_error('prod_r3_3');?>
                                    <?php echo form_error('prod_r4_3');?>
                                    <?php echo form_error('prod_r5_3');?>
                                    <?php echo form_error('prod_r6_3');?>
                                    <?php echo form_error('prod_r7_3');?>
								</div>
                                <!-- Error Div Ends Here --> 
                                
                            </div>
                        </div>
                    </div>
                    <!-- Reginal Table Section Ends -->
                    
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl">
                            <h2>May we suggest</h2>
                            <p> Provide name of products. </p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row"> 
                                
                                
                                <!-- Product 1 Starts -->
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>First Product</label>
                                        <input type="text" name="one" id="one" autocomplete="off"  value="" placeholder="Product Name" onkeyup="related_product_data('one','one_list');"/>
										<input type="hidden" name="one_id" id="one_id" value="" />
                                        <!-- Drop Starts -->
                                        <div class="dyn-list2 one">
                                            <div class="dyn-scroll">
                                                <ul id="one_list">
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Drop Ends -->
                                    </div>
                                </div>
                                <!-- Product 1 Ends --> 
                                
                                <!-- Product 2 Starts -->
                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Second Product</label>
                                        <input type="text" name="two" id="two" autocomplete="off"  value="" placeholder="Product Name" onkeyup="related_product_data('two','two_list');"/>
										<input type="hidden" name="two_id" id="two_id" value="" />
                                        <!-- Drop Starts -->
                                        <div class="dyn-list2 two">
                                            <div class="dyn-scroll">
                                                <ul id="two_list">
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Drop Ends -->
                                    </div>
                                </div>
                                <!-- Product 2 Ends --> 
                                
                                <!-- Product 3 Starts -->
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Third Product</label>
                                         <input type="text" name="three" id="three" autocomplete="off"  value="" placeholder="Product Name" onkeyup="related_product_data('three','three_list');"/>
										<input type="hidden" name="three_id" id="three_id" value="" />
                                        <!-- Drop Starts -->
                                        <div class="dyn-list2 three">
                                            <div class="dyn-scroll">
                                                <ul id="three_list">
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Drop Ends -->
                                    </div>
                                </div>
                                <!--<div class="error"> <?php echo form_error('relp_prod_three_id')?> </div>
                                 Product 3 Ends --> 
                                
                                <!-- Product 4 Starts -->
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Fourth Product</label>
                                         <input type="text" name="four" id="four" autocomplete="off"  value="" placeholder="Product Name" onkeyup="related_product_data('four','four_list');"/>
										<input type="hidden" name="four_id" id="four_id" value="" />
                                        <!-- Drop Starts -->
                                        <div class="dyn-list2 four">
                                            <div class="dyn-scroll">
                                                <ul id="four_list">
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Drop Ends -->
                                    </div>
                                </div>
                                <!--<div class="error"> <?php echo form_error('relp_prod_four_id')?> </div>
                                 Product 4 Ends --> 
                                
                                <!-- Product 5 Starts -->
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Fifth Product</label>
                                         <input type="text" name="five" id="five" autocomplete="off"  value="" placeholder="Product Name" onkeyup="related_product_data('five','five_list');"/>
										<input type="hidden" name="five_id" id="five_id" value="" />
                                        <!-- Drop Starts -->
                                        <div class="dyn-list2 five">
                                            <div class="dyn-scroll">
                                                <ul id="five_list">
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Drop Ends -->
                                    </div>
                                </div>
                                 <!--<div class="error"> <?php echo form_error('relp_prod_five_id')?> </div>
                                Product 5 Ends --> 
                                
                                <!-- Product 6 Starts -->
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Sixth Product</label>
                                         <input type="text" name="six" id="six" autocomplete="off"  value="" placeholder="Product Name" onkeyup="related_product_data('six','six_list');"/>
										<input type="hidden" name="six_id" id="six_id" value="" />
                                        <!-- Drop Starts -->
                                        <div class="dyn-list2 six">
                                            <div class="dyn-scroll">
                                                <ul id="six_list">
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- Drop Ends -->
                                    </div>
                                </div>
                               <!-- <div class="error"> <?php echo form_error('relp_prod_six_id')?> </div>
                                 Product 6 Ends --> 
                                
                            </div>
                        </div>
                    </div>
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl">
                            <h2>Search engines</h2>
                            <p> Set up the meta title, meta keyword and meta description. These help define how this product shows up on 
                                search engines. </p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Meta title 0 of 70 characters used</label>
                                        <input type="text" value="<?php echo set_value('prod_meta_title');?>" name="prod_page_title" />
                                        <div class="error"> <?php echo form_error('prod_meta_title')?> </div>
                                    </div>
                                </div>
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Meta Keyword</label>
                                        <input type="text" value="<?php echo set_value('prod_keyword');?>" name="prod_keyword" />
                                        <div class="error"> <?php echo form_error('prod_keyword')?> </div>
                                    </div>
                                </div>
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Meta description 0 of 160 characters used</label>
                                        <textarea class="text_meta" value="<?php echo set_value('prod_meta_des');?>" name="prod_meta_des"></textarea>
                                        <div class="error"> <?php echo form_error('prod_meta_des')?> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl">
                            <h2>Product status</h2>
                            <p> Select product status active or inactive. </p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Status*</label>
                                        <select name="prod_status">
                                            <option value="">Select</option>
                                            <option value="1" <?php echo set_select('prod_status', '1'); ?> >Active</option>
                                            <option value="0" <?php echo set_select('prod_status', '0'); ?>>Inactive</option>
                                        </select>
                                    </div>
                                    <div class="error"> <?php echo form_error('prod_status')?> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Box along with label section ends here --> 
                    
                    <!-- Box wihout label section begins here -->
                    
                    <div class="mu-fld-sub">
                        <input type="submit" value="Submit" />
                        <a href="<?=base_url()?>admin/product">cancel</a> </div>
                    
                    <!-- Box wihout label section ends here --> 
                    
                    <?php echo form_close(); ?> </div>
            </div>
        </div>
</body>
</html>