<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <?php echo $title;?>
    </title>
    <link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
    <?php include(ADMIN_INCLUDE_PATH."includes/js.php");?>
</head>

<body>
    <div class="container">
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
            <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        <div class="right-rp">
            <!-- Top Green Bar Section Begins Here -->
            <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
            <!-- Top Green Bar Section Ends Here -->
            <div class="col-continer">
                <div class="col-3 cat-box">
                    <!-- Breadcrumbs Starts Here -->
                    <div class="n-crums">
                        <ul>
                            <li> <a href="<?php echo base_url() ?>admin/product">Product Management</a> </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li> <a href="javascript:void(0);"> Edit Product</a> </li>
                        </ul>
                    </div>
                    <!-- Breadcrumbs Ends Here -->
                    <?php echo form_open_multipart('', array('name'=>"form", 'id'=>"myForm")); //"onsubmit"=>"return confirm('Please confirm your information. Are you ready to submit?')" ?>
                    <!-- White Box Starts -->
                    <div class="contntTop-row">
                        <!-- Listing Detail Section Starts -->
                        <div class="listmain-wrap">
                            <!-- Border Row Starts -->
                            <div class="listmain-bdr">
                                <!-- Section Starts -->
                                <div class="listsec-main">
                                    <h3>General Information:</h3>
                                    <!-- Inner Wrap Starts -->
                                    <div class="listsec-in">
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Date of Upload: </div>
                                            <div class="listsec-detail">
                                                <p>
                                                    <?=date('m-d-Y', strtotime($prod_data->prod_created_date))?>
                                                </p>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Product ID: </div>
                                            <div class="listsec-detail">
                                                <?=$prod_data->prod_id?>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Product Name: </div>
                                            <div class="listsec-detail">
                                                <?=$prod_data->prod_title?>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Product For: </div>
                                            <div class="listsec-detail">
                                                <?=$prod_data->looking_for?>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Condition: </div>
                                            <div class="listsec-detail">
                                                <?=$prod_data->condition?>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Quantity: </div>
                                            <div class="listsec-detail">
                                                <?=$prod_data->qty?>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Short Description: </div>
                                            <div class="listsec-detail">
                                                <?=$prod_data->prod_description?>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label liimg-lbl"> Product Image(s): </div>
                                            <div class="listsec-detail">
                                                <?php foreach($prodImg as $img){ ?>
                                                <div class="listsec-img"> <a href="<?=base_url('resources/prod_images/'.$img->img_name)?>" target="_blank" title="Image"> <img src="<?=base_url('resources/prod_images/thumb/'.$img->img_name)?>" alt="" /> </a> </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Youtube Video: </div>
                                            <div class="listsec-detail">
                                                <?=( trim($prod_data->youtube_links) != '' && $prod_data->youtube_links != NULL)?'<a target="_blank" href="'.$prod_data->youtube_links.'">'.$prod_data->youtube_links.'</a>':'N/A'?>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                    </div>
                                    <!-- Inner Wrap Ends -->
                                </div>
                                <div class="pro-m-sprtr"></div>
                                <!-- Section Ends -->
                                <!-- Section Starts -->
                                <div class="listsec-main">
                                    <h3>Category & Filters:</h3>
                                    <!-- Inner Wrap Starts -->
                                    <div class="listsec-in">
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Category: </div>
                                            <div class="listsec-detail">
                                                <p>
                                                    <?=$prod_data->catName?>
                                                </p>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <?php
                                        $purl = $prod_data->catURL;
                                        if($prod_data->subCatName != ''){
                                             $purl .= '/'.$prod_data->subCatURL;
                                        ?>
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Sub Categoy: </div>
                                            <div class="listsec-detail">
                                                <p>
                                                    <?=$prod_data->subCatName?>
                                                </p>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <?php } ?>
                                        <?php if($prod_data->subSubCatName != '') {
                                            $purl .= '/'.$prod_data->subSubCatURL;
                                        ?>
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Sub Sub Categoy: </div>
                                            <div class="listsec-detail">
                                                <p>
                                                    <?=$prod_data->subSubCatName?>
                                                </p>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <?php
                                        $i = 0;
                                        foreach($prodFilters as $filter) {
                                        ?>
                                            <!-- Row Starts -->
                                            <div class="listsec-row">
                                                <div class="listsec-label">
                                                    <?php if($i == 0) { echo "Category Filters:"; }else { echo "&nbsp;"; }?>
                                                </div>
                                                <div class="listsec-detail">
                                                    <div class="listsec-ul">
                                                        <h4><?=$filter->filter_title?></h4>
                                                        <ul>
                                                            <?php
                                                            $fDetail = $this->db->query("
                                                                            SELECT *
                                                                            FROM tbl_product_filters_detail s
                                                                            INNER JOIN tbl_cat_filter_detail m ON m.filter_slug = s.filter_slug
                                                                            WHERE 
                                                                            s.prod_id = ".$prod_data->prod_id."
                                                                            AND
                                                                            s.prod_filter_id = ".$filter->id."
                                                                        ")->row();
                                                            ?>
                                                            <?php 
                                                            $Fil = $this->db->query("
                                                                        SELECT *
                                                                        FROM tbl_product_filters s
                                                                        WHERE 
                                                                        s.prod_id = ".$prod_data->prod_id."
                                                                        AND s.filter_id = ".$filter->filter_id."
                                                                        ORDER BY id ASC
                                                                        ")->result();
                                                            if(empty($fDetail)) {
                                                            ?>
                                                                <?php foreach($Fil as $fff) { ?>
                                                            <li><?=$fff->filter_value?></li>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                    <?php
                                                    if(count($Fil) > 0) { 
                                                        foreach($Fil as $fil2) {
                                                            $fDetail = $this->db->query("
                                                                            SELECT *
                                                                            FROM tbl_product_filters_detail s
                                                                            INNER JOIN tbl_cat_filter_detail m ON m.filter_slug = s.filter_slug
                                                                            WHERE 
                                                                            s.prod_id = ".$prod_data->prod_id."
                                                                            AND
                                                                            s.prod_filter_id = ".$fil2->id."
                                                                        ")->result();
                                                            if(!empty($fDetail)) {
                                                    ?>
                                                    <div class="listsec-ul">
                                                        <h4><?=$fDetail[0]->filter_title?></h4>
                                                        <ul>
                                                            <?php foreach($fDetail as $fdet) { ?>
                                                            <li><?=$fdet->filter_value?></li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                            <?php } ?>
                                                    <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <?php if(count($prodFilters) < $i) { ?>
                                            <div class="pro-sprtr"></div>
                                            <?php } ?>
                                            <!-- Row Ends -->
                                            <?php
                                        $i++;
                                        }
                                        ?>
                                        <?php } ?>
                                    </div>
                                    <!-- Inner Wrap Ends -->
                                </div>
                                <div class="pro-m-sprtr"></div>
                                <!-- Section Ends -->
                                <div class="listsec-main">
                                    <h3>Product Price & Shipping Info:</h3>
                                    <!-- Inner Wrap Starts -->
                                    <div class="listsec-in">
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Price: </div>
                                            <div class="listsec-detail">
                                                $
                                                <?=number_format($prod_data->prod_price, '2', '.', '')?>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> On Sale: </div>
                                            <div class="listsec-detail">
                                                <?=($prod_data->prod_onsale==1)?'Yes':'No'?>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <?php if($prod_data->prod_onsale==1){?>
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Sale Price: </div>
                                            <div class="listsec-detail">
                                                $
                                                <?=number_format($prod_data->sale_price, '2', '.', '')?>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <?php if($prod_data->prod_start_date != ''){?>
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Sale Start Date: </div>
                                            <div class="listsec-detail">
                                                <?=date('m-d-Y',strtotime($prod_data->sale_price))?>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <?php } ?>
                                        <?php if($prod_data->prod_end_date != ''){?>
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Sale End Date: </div>
                                            <div class="listsec-detail">
                                                <?=date('m-d-Y', strtotime($prod_data->sale_price))?>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <?php } ?>
                                        <?php } ?>
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Shipping: </div>
                                            <div class="listsec-detail">
                                                <?=$prod_data->shipping?>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <?php if($prod_data->shipping == "Charge for Shipping") { ?>
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Shipping Method: </div>
                                            <div class="listsec-detail">
                                                <?=$prod_data->prod_shipping_methods?>
                                            </div>
                                        </div>

                                        <div class="listsec-row">
                                            <div class="listsec-label"> Shipping Price: </div>
                                            <div class="listsec-detail">
                                                $<?=$prod_data->ship_price?>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Shipping Days: </div>
                                            <div class="listsec-detail">
                                                <?=$prod_data->ship_days?>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                        <?php } else if($prod_data->shipping == "Offer free Shipping" ){ ?>
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Shipping Days: </div>
                                            <div class="listsec-detail">
                                                <?=$prod_data->free_ship_days?>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <!-- Row Starts -->
                                        <div class="listsec-row">
                                            <div class="listsec-label"> Returns: </div>
                                            <div class="listsec-detail">
                                                <?=$prod_data->prod_return?>
                                            </div>
                                        </div>
                                        <!-- Row Ends -->
                                    </div>
                                    <!-- Inner Wrap Ends -->
                                </div>
                                <div class="pro-m-sprtr"></div>
                                <!-- Section Ends -->
                            </div>
                            <!-- Border Row Ends -->
                            <div class="status-btm">
                                <div class="mu-cm-frmFlds">
                                    <label>Status*:</label>
                                    <select name="prod_status">
                                        <option value="">Select</option>
                                        <option value="1" <?php if (set_select( 'prod_status', '1') !='' ) { echo set_select( 'prod_status', '1');} else if($prod_data->prod_status == '1' ){echo "selected"; }?> >Active</option>
                                        <option value="0" <?php if (set_select( 'prod_status', '0') !='' ) { echo set_select( 'prod_status', '0');} else if($prod_data->prod_status == '0' ){echo "selected"; }?>>Inactive</option>
                                    </select>
                                    <div class="error">
                                        <?=form_error('prod_status')?>
                                    </div>
                                </div>
                                <div class="mu-cm-frmFlds">
                                    <label>Is Featured*:</label>
                                    <select name="prod_feature">
                                        <option value="">Select</option>
                                        <option value="1" <?php if (set_select( 'prod_feature', '1') !='' ) { echo set_select( 'prod_feature', '1');} else if($prod_data->prod_feature == '1' ){echo "selected"; }?> >Yes</option>
                                        <option value="0" <?php if (set_select( 'prod_feature', '0') !='' ) { echo set_select( 'prod_feature', '0');} else if($prod_data->prod_feature == '0' ){echo "selected"; }?>>No</option>
                                    </select>
                                    <div class="error">
                                        <?=form_error('prod_feature')?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Listing Detail Section Ends -->
                    </div>
                    <!-- White Box Ends -->
                    <!-- Buttons begins here -->
                    <div class="mu-fld-sub">
                        <input type="submit" value="Submit" />
                        <a target="_blank" href="<?php echo base_url($purl.'/'.$prod_data->prod_url)?>">Preview</a>
                        <a href="<?php echo base_url()?>admin/product">Cancel</a>
                    </div>
                    <!-- Buttons ends here -->
                    <?php echo form_close(); ?> </div>
            </div>
        </div>
    </div>
    </div>
</body>

</html>