<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <?php echo $title;?>
    </title>
    <link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
    <?php include(ADMIN_INCLUDE_PATH."includes/js.php");?>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/jquery1.12.4.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/data_table.min.js"></script>
    <script>
    $(function() {
        $('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');
        //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
        $('.message .close').hover(
            function() { $(this).addClass('hover'); },
            function() { $(this).removeClass('hover'); }
        );

        $('.message .close').click(function() {
            $(this).parent().fadeOut('slow', function() { $(this).remove(); });
        });
    });

    function randomNumber() {
        return Math.floor(Math.random() * 101)
    }
    </script>
</head>

<body>
    <div class="container">
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
            <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        <div class="right-rp">
            <!-- Top Green Bar Section Begins Here -->
            <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
            <!-- Top Green Bar Section Ends Here -->
            <div class="mu-contnt-hdng">
                <h1>Manage Catalog</h1>
            </div>
            <div class="col-continer">
                <div class="col-3 cat-box">
                    <div class="contntTop-row">
                        <?php include(ADMIN_INCLUDE_PATH."includes/catalog.php"); ?>
                        <div class="actvity-hd">
                            <div class="act-tab b-ad-tb">
                                <table cellspacing="0" border="0">
                                    <colgroup>
                                        <col width="30%" />
                                        <col width="55%" />
                                        <col width="15%" />
                                    </colgroup>
                                    <tr>
                                        <td class="no-bdr">
                                            <div class="cat-tbl-tp-lf">
                                                <h1><?=$title;?></h1><span class="n-of-cat"><?=$total_records?></span>
                                            </div>
                                        </td>
                                        <td align="right" class="no-bdr"></td>
                                        <td align="right" class="expo-cell">
                                          <span>Export to:</span> 
                                          <form method="post" name="MyFormExport">
                                            <?php echo form_open('',array("name"=>"MyFormExport"));?>
                                            <span>
                                              &nbsp;&nbsp;&nbsp;
                                              <a href="<?=base_url()?>admin/product/exportProductCSV">CSV</a>
                                            </span>
                                          <?php echo form_close();?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <?php 
                        if($this->session->flashdata('msg') != "") {
                                echo '<div id="message" class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
                        }
                        ?>
                            <div class="b-prd-tbl align-top">
                                <input type="hidden" name="delIds" id="delIds" />
                                <div class="mn-tblrp">
                                    <table border="0" cellspacing="0" id="mytable">
                                        <colgroup>
                                            <col width="10%" />
                                            <col width="10%" />
                                            <col width="20%" />
                                            <col width="20%" />
                                            <col width="10%" />
                                            <col width="12%" />
                                            <col width="10%" />
                                            <col width="8%" />
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th align="center">Date</th>
                                                <th align="center">Product ID</th>
                                                <th align="left">Product Name</th>
                                                <th align="left">Store Name</th>
                                                <th align="left">Inventory</th>
                                                <th align="center">Price</th>
                                                <th align="center">Status</th>
                                                <th align="center">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if(count($resultsChannel) > 0){
                                                for($n = 0; $n < count($resultsChannel); $n++){
                                            ?>
                                                <tr>
                                                    <td align="center">
                                                        <?=date('m-d-Y', strtotime($resultsChannel[$n]['prod_created_date']))?>
                                                    </td>
                                                    <td align="center">
                                                        <?=$resultsChannel[$n]['prod_id']?>
                                                    </td>
                                                    <td align="left">
                                                        <?=$resultsChannel[$n]['prod_title']?>
                                                    </td>
                                                    <td align="left">
                                                        <?=$resultsChannel[$n]['store_name']?>
                                                    </td>
                                                    <td align="left">
                                                        <?=$resultsChannel[$n]['qty']?> in stock</td>
                                                    <td align="center">
                                                        <?php
                                                            $productPrice = 0.0;
                                                            $sDate = date('Y-m-d', strtotime($resultsChannel[$n]['sale_start_date']));
                                                            $eDate = date('Y-m-d', strtotime($resultsChannel[$n]['sale_end_date']));
                                                            $tDate = date('Y-m-d');
                                                            if ($resultsChannel[$n]['prod_onsale'] == 1) {
                                                                if ($resultsChannel[$n]['sale_start_date'] != '' && $resultsChannel[$n]['sale_end_date'] != '') {
                                                                    if (strtotime($tDate) >= strtotime($sDate) && strtotime($tDate) <= strtotime($eDate)) {
                                                                        $productPrice = number_format($resultsChannel[$n]['sale_price'], 2, '.', '');
                                                                    } else {
                                                                        $productPrice = number_format($resultsChannel[$n]['prod_price'], 2, '.', '');
                                                                    }
                                                                } else if ($resultsChannel[$n]['sale_start_date'] != '') {
                                                                    if ($tDate >= $sDate) {
                                                                        $productPrice = number_format($resultsChannel[$n]['sale_price'], 2, '.', '');
                                                                    } else {
                                                                        $productPrice = number_format($resultsChannel[$n]['prod_price'], 2, '.', '');
                                                                    }
                                                                } else if ($resultsChannel[$n]['sale_end_date'] != '') {
                                                                    if ($tDate <= $eDate) {
                                                                        $productPrice = number_format($resultsChannel[$n]['sale_price'], 2, '.', '');
                                                                    } else {
                                                                        $productPrice = number_format($resultsChannel[$n]['prod_price'], 2, '.', '');
                                                                    }
                                                                } else {
                                                                    $productPrice = number_format($resultsChannel[$n]['sale_price'], 2, '.', '');
                                                                }
                                                            } else {
                                                                $productPrice = number_format($resultsChannel[$n]['prod_price'], 2, '.', '');
                                                            }
                                                            echo "$ ".$productPrice;
                                                        ?>                                                        
                                                    </td>
                                                    <td align="center">
                                                        <?php if($resultsChannel[$n]['prod_status'] == 0 ){ ?>Inactive
                                                        <?php } else { ?> Active <?php } ?>
                                                    </td>
                                                    <td align="center">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <colgroup>
                                                                <col width="20%" />
                                                                <col width="30%" />
                                                                <col width="30%" />
                                                                <col width="20%" />
                                                            </colgroup>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td align="center"><a title="Edit" href="<?=base_url()?>admin/product/edit/<?=$resultsChannel[$n]['prod_id']?>" class="tik-cross-btns p-edt-btn-n"><!--<img src="<?=base_url()?>images/b-edt-icon.png" alt="" />--></a></td>
                                                                <td align="center"><a title="Delete" onclick="return confirm('Are you sure you want to delete selected item(s)?');" href="<?=base_url()?>admin/product/delete/<?=$resultsChannel[$n]['prod_id']?>" class="tik-cross-btns p-del-btn-n"><!--<img src="<?=base_url()?>images/b-del-icon.png " alt="" />--></a></td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
$(document).ready(function() {
    $('#mytable').DataTable({
        "paging": true,
        "ordering": true,
        "info": false,
        "order": [
            [1, "desc"]
        ],
        "columnDefs": [
            { orderable: false, targets: -1 }
        ],
        "oLanguage": { "sZeroRecords": "No records found." },
    });
});
</script>

</html>