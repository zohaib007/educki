<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$title;?></title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
<script>
$(function () {
  $('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');
  //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
  $('.message .close').hover(
    function() { $(this).addClass('hover'); },
    function() { $(this).removeClass('hover'); }
  );
    
  $('.message .close').click(function() {
    $(this).parent().fadeOut('slow', function() { $(this).remove(); });
  });
});
</script>
</head>

<body>
<div class="container_p">

<!-- Dashboard Left Side Begins Here -->

<div class="left_wrp">
    <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
</div>

<!-- Dashboard Left Side Ends Here -->

<div class="right-rp">
    
    <!-- Top Green Bar Section Begins Here -->
    
    <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
    
    <!-- Top Green Bar Section Ends Here -->
    
    <div class="mu-contnt-wrp">
        <!--<div class="mu-contnt-hdng">
            <h1><?=$title;?></h1>
        </div>-->
        <div class="n-crums">
            <ul>
                <li>
                    <a href="<?php echo base_url() ?>admin/marketplace">Marketplace Billing</a>
                </li>
                <li>
                    <div class="crms-sep">
                        &gt;
                    </div>
                </li>
                <li>
                    <a href="javascritp:void(0);"><?=$title;?></a>
                </li>
            </ul>
        </div>
        <?php echo form_open('', array('name'=>'editAdmin') ); ?>
        <!-- Box along with label section begins here -->
        
        	<div class="mu-contnt-outer">
            <?php 
			if($this->session->flashdata('msg') != "")
			{
				echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
			}
			?>
            <div class="mu-contntBx-wrp mrkt-plc" style="width: 100%;">
                    
                <div class="contntTop-row rdtl-pg">
                	<div class="cont-half">
                        <div class="review-tphdr">
                            <div class="reviews-hd">
                                <h1>Billing</h1>
                            </div>
                        </div>
                        <?php if($default_billing){ ?>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Name:</label>
                                <div class="review-rdtl"><?=$default_billing->nick_name?></div>
                            </div>
                        </div>
                        
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Phone:</label>
                                <div class="review-rdtl">
                                    <div class="review-rdtl"><?=$default_billing->phone?></div>      
                                </div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Billing Street:</label>
                                <div class="review-rdtl"><?=$default_billing->street?></div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>City:</label>
                                <div class="review-rdtl"><?=$default_billing->city?></div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>State:</label>
                                <div class="review-rdtl"><?=$default_billing->state?></div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Zip:</label>
                                <div class="review-rdtl"><?=$default_billing->zip?></div>
                            </div>
                        </div>
                        <?php }else{ ?>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <p>No default billing address available.</p>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    
                    <div class="cont-half" style="float:right !important">
                        <div class="review-tphdr">
                            <div class="reviews-hd">
                                <h1>Shipping</h1>
                            </div>
                        </div>
                        <?php if($default_shipping){ ?>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Name:</label>
                                <div class="review-rdtl"><?=$default_shipping->nick_name?></div>
                            </div>
                        </div>
                        
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Phone:</label>
                                <div class="review-rdtl">
                                    <div class="review-rdtl"><?=$default_shipping->phone?></div>      
                                </div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Shipping Street:</label>
                                <div class="review-rdtl"><?=$default_shipping->street?></div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>City:</label>
                                <div class="review-rdtl"><?=$default_shipping->city?></div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>State:</label>
                                <div class="review-rdtl"><?=$default_shipping->state?></div>
                            </div>
                        </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Zip:</label>
                                <div class="review-rdtl"><?=$default_shipping->zip?></div>
                            </div>
                        </div>
                        <?php }else{ ?>
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <p>No default shipping address available.</p>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                        
   			</div>
                    
            </div>
            
        	<div class="mu-contnt-outer">

            <div class="mu-contntBx-wrp store-info" style="width: 100%;">
                    
                <div class="contntTop-row rdtl-pg">
                
                        <div class="review-tphdr">
                            <div class="reviews-hd">
                                <h1>Personal Information</h1>
                            </div>
                        </div>
                        
                        <div class="cont-half">
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Last Log in:</label>
                                    <div class="review-rdtl"><?=date_format(date_create($personal_information->user_last_active),"M d, Y")?></div>
                                </div>
                            </div>
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Seller Since:</label>
                                    <div class="review-rdtl"><?=date_format(date_create($personal_information->user_register_date),"M d, Y")?></div>
                                </div>
                            </div>
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Account Status:</label>
                                    <div class="review-rdtl">
                                        <div class="review-rdtl"><?php if($personal_information->user_status == 1){echo 'Active';}else{echo 'InActive';}?></div>      
                                    </div>
                                </div>
                            </div>
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Subscription Tier:</label>
                                    <div class="review-rdtl"><?=getTierNameBySellerId($personal_information->user_id)?></div>
                                </div>
                            </div>

                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label># of Months left in Subscription:</label>
                                    <div class="review-rdtl"><?php $tier_info = getTierInfoBySellerId($personal_information->user_id);
                                    echo dateDifference($tier_info->sub_ends,date("Y-m-d H:i:s"),"%m");?> Month</div>
                                </div>
                            </div>
                        </div>

                        <div class="cont-half" style="float:right !important">
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label># of People Reffered:</label>
                                    <div class="review-rdtl">0</div>
                                </div>
                            </div>

                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Paypal ID:</label>
                                    <div class="review-rdtl"><?=$tier_info->store_paypal_id?></div>
                                </div>
                            </div>

                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label># of times User Reported / Flagged:</label>
                                    <div class="review-rdtl">0</div>
                                </div>
                            </div>

                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label># of times their Product Reported / Flagged:</label>
                                    <div class="review-rdtl">0</div>
                                </div>
                            </div>
                        </div>
                  
                    
                   
                </div>
                        
   			</div>
                    
            </div>
            
            <div class="mu-contnt-outer">
            <?php 
			if($this->session->flashdata('msg') != "")
			{
				echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
			}
			?>
            <div class="mu-contntBx-wrp store-info" style="width: 100%;">
                    
                <div class="contntTop-row rdtl-pg">
                
                        <div class="review-tphdr">
                            <div class="reviews-hd">
                                <h1>Store Information</h1>
                            </div>
                        </div>
                        
                        <div class="cont-half">
                        
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Date:</label>
                                    <div class="review-rdtl">08-31-2017</div>
                                </div>
                            </div>
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Submitted By:</label>
                                    <div class="review-rdtl">mike </div>
                                </div>
                            </div>
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Email:</label>
                                    <div class="review-rdtl">
                                        <div class="review-rdtl">mike </div>      
                                    </div>
                                </div>
                            </div>
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Review Type:</label>
                                    <div class="review-rdtl">Product Review</div>
                                    <!--
                                    <div class="review-rdtl">Product Review</div>
                                    -->
                                </div>
                            </div>
                  
                    	</div>
                   
                </div>
                        
   			</div>
                    
            </div>
            
            <div class="mu-contnt-outer">
            <?php 
			if($this->session->flashdata('msg') != "")
			{
				echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
			}
			?>
            <div class="mu-contntBx-wrp store-info" style="width: 100%;">
                    
                <div class="contntTop-row rdtl-pg">
                
                        <div class="review-tphdr">
                            <div class="reviews-hd">
                                <h1>Subscription History</h1>
                            </div>
                        </div>

                        <div class="cont-half">
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Date:</label>
                                    <div class="review-rdtl">08-31-2017</div>
                                </div>
                            </div>
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Submitted By:</label>
                                    <div class="review-rdtl">mike </div>
                                </div>
                            </div>
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Email:</label>
                                    <div class="review-rdtl">
                                        <div class="review-rdtl">mike </div>      
                                    </div>
                                </div>
                            </div>
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-cm-frmFlds">
                                    <label>Review Type:</label>
                                    <div class="review-rdtl">Product Review</div>
                                    <!--
                                    <div class="review-rdtl">Product Review</div>
                                    -->
                                </div>
                            </div>

                        </div>
                    
                   
                </div>
                        
   			</div>
                    
            </div>
            
            <div class="mu-contnt-outer">
            <?php 
			if($this->session->flashdata('msg') != "")
			{
				echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
			}
			?>
            <div class="mu-contntBx-wrp store-info" style="width: 100%;">
                    
                <div class="contntTop-row rdtl-pg2">
                
                        <div class="review-tphdr">
                        	<div class="mu-cm-frmFlds">
                                <label>Comments:</label>
                                <div class="review-rdtl">Sample text here Sample text here Sample text here Sample text here Sample text here </div>
                            </div>
                        </div>
                        
                        <div class="mu-flds-wrp">
                            <div class="mu-cm-frmFlds">
                                <label>Comments:</label>
                                <div class="review-rdtl">
                                <textarea class="txtarea">
                                </textarea></div>
                            </div>
                        </div>
                                           
                </div>
                        
   			</div>
                    
            </div>
            
        </div>
        
        <!-- Box along with label section ends here -->
        <!-- Box wihout label section begins here -->
        
        <div class="mu-fld-sub">
            
            <input type="submit" value="Submit" />
            <a href="">Cancel</a> </div>
            
            <!-- Box wihout label section ends here -->
            
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
</body>
</html>