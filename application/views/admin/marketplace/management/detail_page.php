<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$title?></title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>


<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
<script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

<!--<script src="<?= base_url() ?>js/charts/highcharts.js"></script>
<script src="<?= base_url() ?>js/charts/modules/exporting.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>js/editor/examples/css/font-awesome.min.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>js/editor/dist/summernote.css">
<script type="text/javascript" src="<?php echo base_url() ?>js/editor/dist/summernote.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-1.9.1.min.js" type="text/javascript"></script>-->



<script type="text/javascript" src="<?php echo base_url() ?>js/core.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/zebra_datepicker.js"></script>
<link rel="stylesheet" href="<?= base_url() ?>css/defualt.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/summernote.php"); ?>
</head>
<body>
<div class="container_p"> 
  
  <!-- Dashboard Left Side Begins Here -->
  <div class="left_wrp">
    <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
  </div>
  <!-- Dashboard Left Side Ends Here -->
  
  <div class="right-rp"> 
    
    <!-- Top Green Bar Section Begins Here -->
    <?php
    include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
    ?>
    <!-- Top Green Bar Section Ends Here -->
    <div class="mu-contnt-wrp">
      <div class="mu-contnt-hdng">
        <h1>Details Page</h1>
      </div>
      <!-- Bread Crumbs Starts -->
      <div class="n-crums">
        <ul>
          <li> <a href="<?php echo base_url() ?>admin/marketplace/management_listing">Marketplace Management</a> </li>
          <li>
            <div class="crms-sep">&gt;</div>
          </li>
          <li> <a href="javascript:">Details Page</a></li>
        </ul>
      </div>
      <!-- Bread Crumbs Ends --> 
      
      <?php echo form_open_multipart(base_url().'admin/communication/emailtrigger_add'); ?> 
      
      <!-- Box along with label section begins here -->
      
      <div class="mu-contnt-outer">
        <div class="mu-contnt-lfLbl ad-size">
          <h2>Page Details</h2>
          <p>View seller details.</p>
        </div>
        <div class="mu-contntBx-wrp">          
          <div class="contntTop-row">
            <p><label>Customer ID:&nbsp;</label><?=$seller->user_id?></p>
          	<div class="mu-flds-wrp">
            	<div class="mu-frmFlds_long">
                  <label>Store Name</label>
                  <input type="text" style="width: 40%;" value="<?=$seller->store_name?>" name="store_name" readonly/>
              </div>
            </div>

            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                  <label>Email</label>
                  <input type="text" style="width: 40%;" value="<?=$seller->user_email?>" name="email" readonly/>
              </div>
            </div>

            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                  <label>Seller Name</label>
                  <input type="text" style="width: 40%;" value="<?=$seller->user_fname.' '.$seller->user_lname?>" name="seller_name" readonly/>
              </div>
            </div>

            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                  <label>Phone</label>
                  <input type="text" style="width: 40%;" value="<?=(!empty($seller->store_number))?$seller->store_number:'N/A'?>" name="phone" readonly/>
              </div>
            </div>

            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                  <label>Customer Start Date</label>
                  <input type="text" style="width: 40%;" value="<?= date('m-d-Y', strtotime($seller->user_register_date))?>" name="register_date" readonly/>
              </div>
            </div>
            <?php
              $date1 = date_create(date("Y-m-d", strtotime($seller->user_last_active)));
              $date2 = date_create(date("Y-m-d"));
              $diff = date_diff($date1,$date2);
            ?>
            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                  <label># Of Days Since Logged In</label>
                  <input type="text" style="width: 40%;" value="<?=$diff->format("%a days")?>" name="last_active" readonly/>
              </div>
            </div>

            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                  <label># Of Products In Store</label>
                  <input type="text" style="width: 40%;" value="<?=$no_of_products->total_products?>" name="no_of_products" readonly/>
              </div>
            </div>

            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                  <label>Gross Sales GMV</label>
                  <input type="text" style="width: 40%;" value="$<?=(!empty($GMV))?number_format($GMV, 2, '.', ''):'00.00'?>" name="gross_sales" readonly/>
              </div>
            </div>

            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                  <label>Transaction Fees</label>
                  <input type="text" style="width: 40%;" value="$<?=(!empty($fees))?number_format($fees, 2, '.', ''):'00.00'?>" name="fees" readonly/>
              </div>
            </div>

            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                  <label>Account Type</label>
                  <input type="text" style="width: 40%;" value="<?=$seller->user_type?>" name="type" readonly/>
              </div>
            </div>
            <?php  $tier = '';
            if($seller->store_tier_id==1){
              $tier= "Free";
            }
            else if($seller->store_tier_id==2){
              $tier= "Basic";
            }else if($seller->store_tier_id==3){
              $tier= "Pro";
            }else if($seller->store_tier_id==4){
              $tier= "Deluxe";
            }
            ?>
            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                  <label>Current Subscription</label>
                  <input type="text" style="width: 40%;" value="<?=$tier?>" name="tier" readonly/>
              </div>
            </div>

            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                  <label>Payment Schedule</label>
                  <input type="text" style="width: 40%;" value="<?=$next_payment?>" name="next_payment" readonly/>
              </div>
            </div>

            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                  <label>Lifetime Seller Value</label>
                  <input type="text" style="width: 40%;" value="$<?=(!empty($lifetime_value))?number_format($lifetime_value, 2, '.', ''):'00.00'?>" name="life_value" readonly/>
              </div>
            </div>

            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                  <label># Of Times Renewed Subscription</label>
                  <input type="text" style="width: 40%;" value="<?=$renewal?>" name="renewed" readonly/>
              </div>
            </div>

            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                  <label>Total # Of Months Subscribed</label>
                  <input type="text" style="width: 40%;" value="<?=($subscribed>0)?number_format($subscribed, 2, '.', ''):'0'?> months" name="total_months" readonly/>
              </div>
            </div>

            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                  <label># Of Months Left In Current Subscription</label>
                  <input type="text" style="width: 40%;" value="<?=$months_left?>" name="subscription_months" readonly/>
              </div>
            </div>

            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                  <label># Of Times User Reported</label>
                  <input type="text" style="width: 40%;" value="<?=$user_reported?>" name="user_reported" readonly/>
              </div>
            </div>

            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
                  <label># Of Times Their Product Reported</label>
                  <input type="text" style="width: 40%;" value="<?=(!empty($prod_reported))?$prod_reported:'0'?>" name="prod_reported" readonly/>
              </div>
            </div>

          </div>
        </div>

      </div>
      
      <!-- Box along with label section ends here --> 
      
      <!-- Box wihout label section begins here -->
      
      <div class="mu-fld-sub">
        <!-- <input type="submit" value="Submit" /> -->
        <a href="<?=base_url()?>admin/marketplace/management_listing">cancel</a> </div>
      
      <!-- Box wihout label section ends here --> 
      
      <?php echo form_close(); ?> 
    </div>
  </div>
</div>
</body>
</html>