<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?=$title?></title>
    <link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
    <?php include(ADMIN_INCLUDE_PATH."includes/js.php");?>
</head>

<body>
    <div class="container">
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
            <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        <div class="right-rp">
            <!-- Top Green Bar Section Begins Here -->
            <?php 
            $strTopGreenText1="Catalog / Add Sub Category";
            $strTopGreenText2="Catalog";
            include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
            ?>
            <!-- Top Green Bar Section Ends Here -->            
            <div class="col-continer">
                <div class="col-3 cat-box">
                    <!-- Bread crumbs starts here -->
                    <div class="n-crums">
                        <ul>
                            <li> <a href="<?php echo base_url() ?>admin/marketplace/management_listing">Marketplace Management</a> </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li> <a href="javascript:"><?=$title?></a></li>
                        </ul>
                    </div>
                    <!-- Bread crumbs ends here -->
                    <div class="contntTop-row"> 
                        <div class="actvity-hd">
                            <div class="act-tab b-ad-tb">
                                <table cellspacing="0" border="0">
                                    <colgroup>
                                        <col width="70%" />
                                        <col width="30%" />
                                    </colgroup>
                                    <tr>
                                        <td class="no-bdr">
                                            <div class="cat-tbl-tp-lf">
                                                <h1><?=$title?></h1>
                                            </div>
                                        </td>
                                        <td align="right" class="no-bdr">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div style="display:block">
                            <div class="add-frm-container">
                                <?php echo form_open('');?>
                                    <div class="add-frm-wrp">
                                        <label>Cancel Requested:</label>
                                        <div class="frm-s-sel">
                                            <input type="text" name="email" readonly="readonly" value="<?=$seller->user_email?>">
                                        </div>
                                    </div>                                    

                                   <div class="add-frm-wrp">
                                        <label>Reason for cancellation:</label>
                                        <div class="frm-s-sel">
                                            <select name="other_reason" id="other_reason">
                                                <option value="">Select</option>
                                                <option value="1" <?php echo set_select('other_reason',1);?>>No more items to sell</option>
                                                <option value="2" <?php echo set_select('other_reason',2);?>>I have been unsuccessful selling</option>
                                                <option value="3" <?php echo set_select('other_reason',3);?>>Using a different site</option>
                                                <option value="4" <?php echo set_select('other_reason',4);?>>No longer using service</option>                                                
                                                <option value="5" <?php echo set_select('other_reason',5);?>>I found a better deal</option>
                                                <option value="6" <?php echo set_select('other_reason',6);?>>Fees are too expensive</option>
                                                <option value="7" <?php echo set_select('other_reason',7);?>>Other</option>
                                             </select>
                                         </div>
                                         <div class="error lable-errorpad"><?=form_error('other_reason');?></div>
                                   </div>

                                    <div class="add-frm-wrp" id="test" style="display: none;">
                                        <label>Other:</label>
                                        <div class="frm-s-txtarea">
                                            <textarea cols="45" rows="10" name="other"></textarea>
                                        </div>
                                        <div class="error lable-errorpad"><?=form_error('other');?></div>
                                    </div>

                                    <div class="add-frm-wrp">
                                        <div class="mu-fld-sub ns01">
                                            <label>&nbsp;</label>
                                            <div class="nbtn-nrow">
                                                <input type="submit" value="Submit" />
                                                <a href="<?=base_url()?>admin/marketplace/management_listing/">cancel</a> 
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('select').on('change',function(){
            var value = $('#other_reason').val();
            /*alert(value);*/
            if(value!=7){
                $('#test').hide();
            }else{
                $('#test').show();
            }
        });
        $(document).ready(function(){
            $('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');
            
            $('.message .close').hover(
                function() { $(this).addClass('hover'); },
                function() { $(this).removeClass('hover'); }
            );

            $('.message .close').click(function() {
                $(this).parent().fadeOut('slow', function() { $(this).remove(); });
            });
        });
        $(document).ready(function(){
            var value = '<?=@$_POST['other_reason']?>';
            if(value == 7){
              $('#test').show("slow"); 
            }
            else {
                $('#test').hide();
            }
        });
    </script>
</body>

</html>