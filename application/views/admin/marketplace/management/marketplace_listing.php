<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?= $title; ?></title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />

<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/jquery1.12.4.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/data_table.min.js"></script>

<script>
$(function () {
    $('.message').append('<span class="close" title="Dismiss"></span>');
    //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
    $('.message .close').hover(
        function() { $(this).addClass('hover'); },
        function() { $(this).removeClass('hover'); }
    );
        
    $('.message .close').click(function() {
        $(this).parent().fadeOut('slow', function() { $(this).remove(); });
    });
});
</script>

<style>
#display_tr {
    display: table-row !important;
}
</style>
</head>

<body>
<div class="container">
    
    <!-- Dashboard Left Side Begins Here -->
    <div class="left_wrp">
        <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
    </div>
    <!-- Dashboard Left Side Ends Here -->
    
    <div class="right-rp">
        
        <!-- Top Green Bar Section Begins Here -->
        <?php
        include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
        ?>
        <!-- Top Green Bar Section Ends Here-->
        <div class="col-continer">
            <div class="col-3 cat-box">
                <div class="contntTop-row" style="width: 95.5%;padding-left: 1%;padding-right: 1%;">
                    <div class="actvity-hd">
                        <div class="act-tab b-ad-tb">
                            <table cellspacing="0" border="0">
                                <colgroup>
                                <col width="30%" />
                                <col width="40%" />
                                <col width="30%" />
                                </colgroup>
                                <tr>
                                    <td class="no-bdr">
                                        <div class="cat-tbl-tp-lf">
                                            <h1><?=$title;?></h1><span class="n-of-cat">5</span>
                                        </div>
                                    </td>
                                    <td id="" class="pr-msg-bx"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?php   
                    if($this->session->flashdata('msg') != ""){
                       echo '<div id="message" class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
                    }
                    ?>
                    <div class="b-prd-tbl align-top">
                        <!--<form name="myForm" method="post" action="<?= base_url() ?>admin/product/del/">-->
                            <table border="0" cellspacing="0" id='mytable'>
                                <colgroup>
                                    <col width="10%"/>
                                    <col width="12%"/>
                                    <col width="17%"/>
                                    <col width="12%"/>
                                    <col width="10%"/>
                                    <col width="10%"/>
                                    <col width="8%"/>
                                    <col width="8%"/>
                                    <col width="8%"/>
                                    <col width="4%"/>
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th align="left">Customer ID</th>
                                        <th align="left">Store Name</th>
                                        <th align="left">Email</th>
                                        <th align="left">Seller Name</th>
                                        <th align="left">Phone</th>
                                        <th align="left">Start Date</th>
                                        <th align="left">Wishlist Item(s)</th>
                                        <th align="left"># Of Products</th>
                                        <th align="left">Status</th>
                                        <th align="center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if($sellers){ 
                                        foreach ($sellers as $seller) { 
                                        $phone = getStoreInfoByUserId($seller['user_id'])->store_number; ?>
                                        
                                    <tr>
                                        <td class="td-content" align="center" ><?=$seller['user_id']?></td>
                                        <td class="td-content" ><?=$seller['store_name']?></td>
                                        <td class="td-content" ><?=$seller['user_email']?></td>
                                        <td class="td-content" align="center" ><?=$seller['user_fname']?> <?=$seller['user_lname']?></td>
                                        <td class="td-content" align="center" ><?= (trim($seller['store_number'])!='')?$seller['store_number']:"N/A"?></td>
                                        <td class="td-content" align="center" ><?=date_format(date_create($seller['user_register_date']),"m-d-Y")?></td>
                                        <td class="td-content" align="center" ><?=countWishListItem($seller['user_id'])?></td>
                                        <td class="td-content" align="center" ><?php if(getTotalProductsByUserId($seller['user_id']) > 0){echo getTotalProductsByUserId($seller['user_id']);}else{ echo '0';} ?></td>
                                        <td class="td-content" align="center" ><?php if($seller['user_status'] == 1){echo 'Active';}else{echo 'InActive';}?></td>
                                        <td align="center">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <colgroup>
                                                    <col width="2%" />
                                                    <col width="48%" />
                                                    <col width="48%" />
                                                    <col width="2%" />
                                                </colgroup>
                                                <tr id="display_tr">
                                                    <td>&nbsp;</td>
                                                    <td align="center">
                                                        <a title="Edit" href="<?=base_url()?>admin/marketplace/management_detail/<?=$seller['user_id']?>" class="tik-cross-btns p-view-btn-n"></a>
                                                    </td>
                                                    <?php if($seller['store_tier_id']!=1){?>
                                                    <td align="center">                                                        
                                                        <a title="Cancel Request" href="<?=base_url()?>admin/marketplace/cancel_subscription/<?=$seller['user_id']?>" class="tik-cross-btns p-cancelr-btn-n"></a>                                                        
                                                    </td>
                                                    <?php }else{?>
                                                    <td>&nbsp;</td>
                                                    <?php }?>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <?php }
                                    }else{
                                        echo '<tr><td colspan = 9 align="center" >No data found.</td></tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        <!--</form>-->
                        <div class="pagination-row" style="width:100%">
                            <?php //$paginglink ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $('#mytable').DataTable({
        "paging":   true,
        "ordering": true,
        "info":     false,
        "aaSorting": [],
        "columnDefs": [
           { orderable: false, targets: -1 }
        ],
        "oLanguage": { "sZeroRecords": "No records found." },
    });
});
</script>
</body>
</html>