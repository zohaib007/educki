<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>User Orders</title>
    <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
    <link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/jquery-ui-1.8.13.custom.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/fullcalendar.css" />
    <link rel="stylesheet" href="<?=base_url()?>js/tablesort/style.css" type="text/css" media="all" />
    <?php include(ADMIN_INCLUDE_PATH."includes/js.php");?>

    <script src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/jquery1.12.4.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/data_table.min.js"></script>

    <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.daterange.js"></script>
    <!--//////////////////////////////table sorter///////////////////////////////////////// ///////-->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/tablesort/jquery-latest.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/tablesort/jquery.tablesorter.js"></script>
    <script>
    $(function() {
        $('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');
        //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
        $('.message .close').hover(
            function() { $(this).addClass('hover'); },
            function() { $(this).removeClass('hover'); }
        );

        $('.message .close').click(function() {
            $(this).parent().fadeOut('slow', function() { $(this).remove(); });
        });
    });

    /*  $(function() {
            $("#mytable").tablesorter({debug: true})
            
            $("a.append").click(appendData);
        });*/
    $(document).ready(function() {
        $("#mytable").tablesorter({
            // pass the headers argument and assing a object 
            headers: {
                // assign the sixth column (we start counting zero) 
                6: {
                    // disable it by setting the property sorter to false 
                    sorter: false
                },
                7: {
                    // disable it by setting the property sorter to false 
                    sorter: false
                },
            }
        });
    });

    var lastStudent = 23;
    var limit = 500;

    function appendData() {

        var tdTagStart = '<td>';
        var tdTagEnd = '</td>';
        var sex = ['male', 'female'];
        var major = ['Mathematics', 'Languages'];


        for (var i = 0; i < limit; i++) {
            var rnd = i % 2;
            var row = '<tr>';
            row += tdTagStart + 'student' + (lastStudent++) + tdTagEnd;
            row += tdTagStart + major[rnd] + tdTagEnd;
            row += tdTagStart + sex[rnd] + tdTagEnd;

            row += tdTagStart + randomNumber() + tdTagEnd;
            row += tdTagStart + randomNumber() + tdTagEnd;
            row += tdTagStart + randomNumber() + tdTagEnd;
            row += tdTagStart + randomNumber() + tdTagEnd;

            row += '</tr>';

            $("table/tbody:first").append(row);

        };


        $("table").trigger('update');
        return false;
    }

    function randomNumber() {
        return Math.floor(Math.random() * 101)
    }
    </script>
    <!--//////////////////////////////table sorter end////////////////////////////////////////////////-->
    <script>
    $(function() {

        $('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');

        //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)

        $('.message .close').hover(

            function() { $(this).addClass('hover'); },

            function() { $(this).removeClass('hover'); }

        );



        $('.message .close').click(function() {

            $(this).parent().fadeOut('slow', function() { $(this).remove(); });

        });

    });

    $(document).ready(function() {

        $("#showMessage").css('display', 'none');



    });


    $(document).ready(function() {

        $("#showMessage").css('display', 'none');

        $("a.userchangeStatus1").click(function(e) {



            var parent = $(this).parent();

            var myarray = parent.attr('id').split("_");



            var con = confirm("Are you sure you want to change the status?");

            if (con == true)

            {

                var parent = $(this).parent();

                var myarray = parent.attr('id').split("_");
                if (myarray[0] == 1) {

                    $("#img" + myarray[1]).attr('src', base_url + 'images/tick-btn.png');

                    $("#imag" + myarray[1]).attr('src', base_url + 'images/visible.png');
                    parent.attr('id', '0_' + myarray[1]);

                } else {
                    $("#img" + myarray[1]).attr('src', base_url + 'images/cross-btn.png');

                    $("#imag" + myarray[1]).attr('src', base_url + 'images/invisible.png');

                    parent.attr('id', '1_' + myarray[1]);

                }

                e.preventDefault();

                $("#showMessage").css('display', 'block');

                $("#showMessage").load(base_url + 'admin/user/changeStatus', { "nId": myarray[1], "bStatus": myarray[0], <?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>' });
            }
        });

    });

    $(document).ready(function() {

        $("#showMessage").css('display', 'none');

        $("a.userchangeStatus2").click(function(e) {
            var con = confirm("Are you sure you want to change the status (V/I?");
            if (con == true) {
                var parent = $(this).parent();
                var myarray = parent.attr('id').split("_");
                if (myarray[0] == 1) {

                    $("#imag" + myarray[1]).attr('src', base_url + 'images/visible.png');

                    $("#img" + myarray[1]).attr('src', base_url + 'images/tick-btn.png');

                    parent.attr('id', '2_' + myarray[1]);

                } else if (myarray[0] == 2) {
                    $("#imag" + myarray[1]).attr('src', base_url + 'images/invisible.png');
                    parent.attr('id', '1_' + myarray[1]);
                }
                e.preventDefault();

                $("#showMessage").css('display', 'block');
                $("#showMessage").load(base_url + 'admin/user/changeStatus', { "nId": myarray[1], "bStatus": myarray[0], <?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>' });
            }

        });

    });
    </script>
</head>

<body>
    <div class="container">
        <div class="left_wrp" id="menu-top">
            <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        <div class="right-rp">
            <!-- Top Green Bar Section Begins Here -->
            <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");?>
            <!-- Top Green Bar Section Ends Here -->
            <div class="col-continer">
                <div class="col-3 cat-box">
                    <div class="contntTop-row">
                        <!-- col-box1 -->
                        <div class="actvity-hd">
                            <div class="act-tab b-ad-tb">
                                <table cellspacing="0" border="0">
                                    <colgroup>
                                        <col width="34%" />
                                        <col width="33%" />
                                        <col width="33%" />
                                    </colgroup>
                                    <tr>
                                        <td class="no-bdr">
                                            User Orders
                                        </td>
                                        <td align="right" class="no-bdr"></td>
                                        <td align="right" class="no-bdr"></td>
                                    </tr>
                                </table>
                            </div>
                            <?php
                        if($this->session->flashdata('msg') != "")
                        {
                            echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
                        }
                        ?>
                        </div>
                        <div class="b-srch-bl-sec">
                            <table border="0" width="100%" cellspacing="0">
                                <colgroup>
                                    <col width="10%" />
                                    <col width="15%" />
                                    <col width="20%" />
                                    <col width="10%" />
                                    <col width="45%" />
                                </colgroup>
                                <tr>
                                    <?php echo form_open('',array('name'=>'myForm','method'=>'get'));?>
                                    <td align="center"><span>Search by</span></td>
                                    <td align="center">
                                        <select name="srchOp" class="catName">
                                            <option <?php if(@$_GET['srchOp']=='status' ) { ?> selected="selected"
                                                <?php } ?> value="status">Status</option>
                                            <option <?php if(@$_GET['srchOp']=='id' ) { ?> selected="selected"
                                                <?php } ?> value="id">Order ID</option>
                                        </select>
                                    </td>
                                    <td align="center">
                                        <input value="<?php if(isset($_GET['searchText'])) { echo $_GET['searchText']; } ?>" type="text" name="searchText" class="catName" />
                                    </td>
                                    <td align="center">
                                        <input type="submit" value="" class="catSrch" />
                                    </td>
                                    <?php echo form_close();?>
                                    <td align="left">
                                        <div class="ndpdSort" style="width:auto; float:right; margin-right: 274px; width: auto;">
                                            <button class="pro-addNew" id="nsortBy">Sort By<span class="dn-angl"></span> </button>
                                            <div class="dropval"></div>
                                            <div class="ndp-nx" id="nsortDp" style="display:none; width: 135px;">
                                                <ul>
                                                    <li>
                                                        <div class="usr-srt-dp subDpd"> Select date range <span class="sub-dn-angl" style="top: 10px;"></span> </div>
                                                        <div class="dp-dt-pik" style="display:none;">
                                                            <?php echo form_open('', array('name'=>'sort_form'));?>
                                                            <input name="date_range" style="width: 100%;" placeholder="Select date to view report" id="d" class="form__daterange catName" />
                                                            <input type="submit" value="Submit" />
                                                            <?php echo form_close();?>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="b-prd-tbl">
                            <table border="0" cellspacing="0" id='mytable' class='tablesorter'>
                                <colgroup>
                                    <col width="12%" />
                                    <col width="14%" />
                                    <col width="20%" />
                                    <col width="18%" />
                                    <col width="16%" />
                                    <col width="12%" />
                                    <col width="8%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th align="center">Date</th>
                                        <th align="center">Order ID</th>
                                        <th align="center">Item</th>
                                        <th align="center">Tracking Number</th>
                                        <th align="center">Order Total</th>
                                        <th align="center">Status</th>
                                        <th align="center">Actions</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if(count($rstCategories) > 0)
                                    {
                                        foreach($rstCategories as $n => $rstCategorie)
                                        {
                                    ?>
                                        <tr id="row<?=$rstCategorie->buyer_id?>" <?php if(($n%2)==1){           echo "class='odd'";} ?> >
                                            <td align="center">
                                                <?=date('m-d-Y', strtotime($rstCategorie->order_date))?>
                                            </td>
                                            <td align="center">
                                                <?=$rstCategorie->order_id?>
                                            </td>
                                            <?php $o_prod = getOrderProductByOrderId($rstCategorie->order_id);?>
                                            <td align="center">

                                                <div class="subimg">
                                                    <img src="<?= base_url() ?>resources/prod_images/<?= $o_prod->order_prod_image ?>" alt="" title="" class="" />
                                                </div>
                                                <div class="subtxtn">
                                                    <?=$o_prod->order_prod_name?>
                                                </div>
                                            </td>
                                            <td align="center">
                                                <?=($o_prod->order_prod_tracking_number!='')?$o_prod->order_prod_tracking_number:'N/A'?>
                                            </td>
                                            <td align="center">
                                                $ <?=number_format($rstCategorie->order_grand_total, 2, '.', '')?>
                                            </td>
                                            <td align="center">
                                                <?=$rstCategorie->order_status?>
                                            </td>
                                            <td align="center">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <colgroup>
                                                        <col width="15%">
                                                        <col width="70%">
                                                        <col width="15%">
                                                    </colgroup>
                                                    <tr>
                                                        <td></td>
                                                        <td align="center">
                                                            <a title="View Order Detail" href="<?php echo base_url();?>view_order/<?=$rstCategorie->order_id?>" class="tik-cross-btns p-view-btn-n"></a>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <?php
                                            } 
                                        }else{
                                            echo "<tr><td colspan='7' align='center'><strong>No data found.</strong></td></tr>";
                                        }
                                    ?>
                                </tbody>
                            </table>
                            <div class="pagination-row">
                                <?php if(isset($paginglink)) { echo $paginglink; }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(function() {
        jQuery.noConflict();
        $("#d").daterange({
            dateFormat: "yy-mm-dd",
            maxDate: 0,
        });
    });

    $(document).ready(function() {
        $('#nsortBy').click(function() {
            $('#nsortDp').slideToggle();
        });

        $('.subDpd').click(function() {
            $('.dp-dt-pik').slideToggle();
        });

        $('.valclick').click(function() {
            $('.dropval').html($(this).attr("title"));
            $('#nsortDp').slideToggle();
        });
    });
    </script>
    <script>
        $(document).ready(function() {
            $('#mytable').DataTable({
                "paging": true,
                "ordering": true,
                "info": false,
                "lengthChange": false,
                "searching": false,
                "order": [
                    [1, "desc"]
                ],
                "columnDefs": [
                    { orderable: false, targets: -1 }
                ],
                "oLanguage": { "sZeroRecords": "No records found." },
            });
        });
    </script>
    <script src="<?= base_url() ?>/js/jquery.filtertable.min.js"></script>
</body>

</html>