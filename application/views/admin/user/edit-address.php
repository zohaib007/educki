<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$title;?></title>
<?php include(ADMIN_INCLUDE_PATH."includes/js.php");?>
<link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />

<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />

<script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
<script src="<?=base_url()?>js/jquery-1.10.1.min.js"></script>
<script src="<?php echo base_url()?>js/jquery.liveaddress.js"></script>
<script type="text/javascript" src="<?=base_url()?>js/jquery.validate.min.js"></script>
 </head>
 <body>
 <div class="container_p"> <!-- container --> 
   <!-- Dashboard Left Side Begins Here -->
   <div class="left_wrp">
     <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
   </div>
   <!-- Dashboard Left Side Ends Here -->
   <div class="right-rp"> 
     <!-- Top Green Bar Section Begins Here -->
     <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
     <div class="mu-contnt-hdng">
          <h1><?=$title;?></h1>
      </div>
      <div class="n-crums">
          <ul>
              <li> <a href="<?=base_url()?>admin/user/statistics">Manage Users</a> </li>
              <li><div class="crms-sep">&gt;</div></li>
              <li> <a href="<?=base_url()?>admin/user/list_addresses/<?=$user_id?>" >Address Book</a></li>
              <li><div class="crms-sep">&gt;</div></li>
              <li> <a href="javascript:void(0)"><?=$title;?></a></li>
          </ul>
      </div>
     <!-- Top Green Bar Section Ends Here -->
     <div class="mu-contnt-wrp">
       <!-- <div class="mu-contnt-hdng">
         <h1<?=$title?></h1>
         <a href="<?php echo base_url()?>admin/user" class="c-b-s-l" style="display:none;"> Back <img src="<?=base_url()?>images/back0icon.jpg" alt="" /> </a> </div>
       <div class="n-crums">
         <ul>
           <li> <a href="<?php echo base_url() ?>admin/user">User Accounts</a> </li>
           <li> <div class="crms-sep">&gt;</div> </li>
           <li> <a href="#"> Add New User</a> </li>
         </ul>
       </div> -->
       <?php echo form_open_multipart('', array('id'=>'edit_user')); ?> 
       <!-- Box along with label section begins here -->
       <div class="mu-contnt-outer">
         <div class="mu-contnt-lfLbl ad-size">
           <h2>Address Details</h2>
           <p>Manage your Address Details.</p>
         </div>
         <div class="mu-contntBx-wrp">
           <div class="contntTop-row">
            <div class="mu-flds-wrp">
               <div class="mu-frmFlds_long">
                 <label>Nick Name*</label>
                 <input type="text" name="user_nname"  value="<?php echo set_value('user_nname')!=''?set_value('user_nname'):$address_data->nick_name; ?>" />
               </div>
             </div>
             <div class="error"> <?php echo form_error('user_nname')?> </div>
             <div class="mu-flds-wrp">
               <div class="mu-frmFlds_long">
                 <label>First Name*</label>
                 <input type="text" name="user_fname"  value="<?php echo set_value('user_fname')!=''?set_value('user_fname'):$address_data->first_name; ?>" />
               </div>
             </div>
             <div class="error"> <?php echo form_error('user_fname')?> </div>
             <div class="mu-flds-wrp">
               <div class="mu-frmFlds_long">
                 <label>Last Name*</label>
                 <input type="text" name="user_lname"  value="<?php echo set_value('user_lname')!=''?set_value('user_lname'):$address_data->last_name; ?>" />
               </div>
             </div>
             <div class="error"> <?php echo form_error('user_lname')?> </div>
             <div class="mu-flds-wrp">
               <div class="mu-frmFlds_long">
                 <label>Street*</label>
                 <input type="text" name="street"  value="<?php echo set_value('street')!=''?set_value('street'):$address_data->street; ?>" />
               </div>
             </div>
             <div class="error"> <?php echo form_error('street')?> </div>
             <div class="mu-flds-wrp">
               <div class="mu-frmFlds_long">
                 <label>City*</label>
                 <input type="text" name="city"  value="<?php echo set_value('city')!=''?set_value('city'):$address_data->city; ?>" />
               </div>
             </div>
             <div class="error"> <?php echo form_error('city')?> </div>
             <div class="mu-flds-wrp">
               <div class="mu-frmFlds_long">
                 <label>Country*</label>
                 <select name="user_country" id="user_country">
                 <option value="">Select</option>
                   <?php foreach($country as $val) { ?>
                   <option value="<?php echo $val['iso']; ?>" <?php if($address_data->country==$val['iso']){echo "selected"; } ?> ><?php echo $val['name'];?></option>
                   <?php } ?>
                 </select>
               </div>
             </div>
             <div class="error"> <?php echo form_error('user_country')?> </div>

             <div class="mu-flds-wrp country-us" id ="us-states" <?= ($address_data->country=="US") ? 'style="display: block"' : 'style="display: none"' ?> >
               <div class="mu-frmFlds_long">
                 <label>State*</label>
                 <select name="state">
                   <option value="">Select</option>
                   <?php foreach($states as $val) { ?>
                   <option value="<?php echo $val['stat_id']; ?>" <?php if (set_select('state', $val['stat_id']) != '') { echo set_select('state', $val['stat_id']);} elseif($address_data->state == $val['stat_id'] ){echo "selected"; } ?>><?php echo $val['stat_name'];?></option>
                       <?php } ?>
                 </select>
               </div>
             </div>
             <div class="error country-us"> <?php echo form_error('state')?> </div>

             <div class="mu-flds-wrp" id="other-states" <?= ($address_data->country!="US") ? 'style="display: block"' : 'style="display: none"' ?> >
               <div class="mu-frmFlds_long">
                 <label>State*</label>
                 <input type="text" id="state_other" name="state_other" value="<?php if(set_value('state_other') != ''){ echo set_value('state_other');} else { echo htmlentities($address_data->state_other);}?>" />
               </div>
             </div>
             <div class="error"> <?php echo form_error('state_other'); ?> </div>

             <div class="mu-flds-wrp">
               <div class="mu-frmFlds_long">
                 <label id="zip_postal">Zip*</label>
                 <input type="text" name="user_zip" id="zip" value="<?php echo set_value('user_zip')!=''?set_value('user_zip'):$address_data->zip;?>"/>
               </div>
             </div>
             <div class="error"> <?php echo form_error('user_zip')?> </div>
             <div class="mu-flds-wrp">
               <div class="mu-frmFlds_long">
                 <label>Telephone</label>
                 <input type="text" name="user_phone" value="<?php echo set_value('user_phone')!=''?set_value('user_phone'):$address_data->phone;?>"/>
               </div>
             </div>
             <div class="error"> <?php echo form_error('user_phone')?> </div>
             <div class="mu-flds-wrp">
               <?php if($address_data->default_billing_check=='Y') {?>
                  <input type="checkbox" name="Billing" value="1" checked>Use as default billing address<br>
               <?php } else {?>
                  <input type="checkbox" name="Billing" value="1">Use as default billing address<br>
               <?php }?>
               <?php if($address_data->default_shipping_check=='Y') {?>
                  <input type="checkbox" name="shipping" value="2" checked>Use as default shipping address<br>
               <?php } else {?>
                  <input type="checkbox" name="shipping" value="2">Use as default shipping address<br>
               <?php }?>
             </div>
           </div>
         </div>
       </div>
       <!-- Box along with label section ends here --> 
       <!-- Box wihout label section begins here -->
       <div class="mu-fld-sub">
         <input type="submit" value="Submit" />
         <a href="<?php echo base_url()?>admin/user/list_addresses/<?=$user_id;?>">cancel</a> </div>
       <!-- Box wihout label section ends here --> 
       <?php echo form_close(); ?> </div>
   </div>
 </div>
<script type="text/javascript">
         $(function() {
            $('#user_country').change(function(){
                if($('#user_country').val() === 'US') {
                    $('#us-states').show("slow"); 
                    $('#other_state').val('');
                    $('#other-states').hide("slow"); 
                } else {
                    $('#us-states').hide("slow");
                    $('#other-states').show("slow");
                } 
            });
         });
         $(function() {
            $('#user_interests_second').change(function(){
                if($('#user_interests_second').val() == 'other') {
                    $('#otherinterestsecond').show("slow"); 
                } else {
                    $('#otherinterestsecond').hide("slow");
                }
                 
            });
         });
     </script>

    <script type="text/javascript">
      $(document).ready(function(){
        var country = '<?=@$_POST['user_country']?>';
        if(country!=''){
          if(country == 'US'){
            $('#us-states').show("slow"); 
            $('#other-states').hide("slow"); 
          }
          else {
            $('#us-states').hide("slow");
            $('#other-states').show("slow");
          }
        }
      });
    </script>

</body>
</html>