<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <?=$title;?>
    </title>
    <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
    <?php include(ADMIN_INCLUDE_PATH . "includes/js.php"); ?>
    <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
    <script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
    <!-- tinymce configration start here.-->
    <script>
    var base_url = '<?php echo base_url(); ?>';
    var ser = '<?php echo $_SERVER['
    DOCUMENT_ROOT '];?>educki/resources/tiny_upload';
    </script>
    <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.js"></script>
    <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
    <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>tiny/common.js"></script>
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/main-dev2.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/main.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/web-fonts/ufonts.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/animate.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/bootstrap-dropdownhover.min.css" />
    <script type="text/javascript" src="<?=base_url()?>front_resources/js/jquery-3.2.1.min.js"></script>
    <!-- <link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/bootstrap.min.css" /> -->
    <!-- tinymce configration end here.-->
    <script>
    var base_url = '<?php echo base_url();?>';

    function check() {
        $("#check_availability").click();
    }

    function delete_gall_image(sel, hide, fieldname, name) {
        //e.preventDefault();
        $('#' + hide).hide();
        $('#' + name).val('');
        $.ajax({
            url: base_url + 'admin/user/delete_page_img',
            type: 'POST',
            data: { nId: sel, fieldname: fieldname, <?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>' },
        }).done(function() {
            console.log("success");
        }).fail(function() {
            console.log("error");
        }).always(function() {
            console.log("complete");
        });
    }

    $(document).ready(function() {
        $("#check_availability").click(function(e) {
            e.preventDefault();
            var value = $('#store_name').val();
            var old_value = $('#old_store').val();
            if(value!=''){
                if (value != old_value && value!='') {
                    var url = "<?= base_url() ?>admin/user/check_availability";
                    $.ajax({
                        url: url,
                        type: "POST",
                        data: { "store_name": value, <?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>' },
                        success: function(data) {
                            data = jQuery.parseJSON(data);
                            if (data.status == 'Available') {
                                $("#Available").removeClass("hide");
                                $("#not_available").addClass("hide");
                                $("#rnd_store_name").addClass("hide");
                                $('#store_name_validate').html('');
                            } else {
                                $("#not_available").removeClass("hide");
                                $("#Available").addClass("hide");
                                $("#rnd_store_name").removeClass("hide");
                                $("#rnd_store_name").html("Store Name Not Avaliable try this " + data.new_name);
                                $('#store_name_validate').html('');
                            }
                        }
                    });
                }
            }else{
                $('#store_name_validate').html('This field is required');
                $("#not_available").addClass("hide");
                $("#Available").addClass("hide");
            }
        });
    });
    </script>
</head>

<body>
    <div class="container_p">
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
            <?php include(ADMIN_INCLUDE_PATH . "includes/dash-left.php"); ?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        <div class="right-rp">
            <!-- Top Green Bar Section Begins Here -->
            <?php
            include(ADMIN_INCLUDE_PATH . "includes/top_green_bar.php");
            ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="mu-contnt-wrp">
                    <div class="mu-contnt-hdng">
                        <h1><?=$title;?></h1>
                    </div>
                    <!-- Bread crumbs starts here -->
                    <div class="n-crums">
                        <ul>
                            <li> <a href="<?=base_url()?>admin/user">Manage Users</a> </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <?=$title;?>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- Bread crumbs ends here -->
                    <!-- Box along with label section begins here -->
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2><?=$title?></h2>
                            <p>Manage your store Details.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="col-sm-12 col-xs-12 custom-boxset">
                                    <div class="my-account-right">
                                        <div class="my-account-right-head">
                                            <h5>Name Your Shop <span>*</span></h5>
                                            <p>Choose a memorable name that reflects your style.</p>
                                        </div>
                                        <?php echo form_open_multipart('', array('name' => 'create_store', 'id' => 'createstore_form')); ?>
                                        <div class="become-seller-sec">
                                            <div class="chk-avalb">
                                                <div class="chk-avalb-lft">
                                                    <input type="text" name="store_name" id="store_name" value="<?php echo set_value('store_name'); ?>" onblur="check()" placeholder="Store name here" />
                                                    <span id="Available" class="g1 hide">Availabe</span>
                                                    <span id="not_available" class="rd1 hide">Not Availabe</span>
                                                </div>
                                                <div class="chk-avalb-rgt">
                                                    <input type="submit" id="check_availability" value="Check Availability" />
                                                </div>
                                                <p>Your shop name will appear in your shop and next to each of your listings throughout eDucki.</p>
                                                <div class="optionl-struct">
                                                    <p id="rnd_store_name"></p>
                                                </div>
                                                <div class="error" id="store_name_validate">
                                                    <?php echo form_error('store_name') ?>
                                                </div>
                                            </div>
                                            <div class="best-descb">
                                                <div class="best-descb-lft">
                                                    <h5>Which of these best describes you? <span>*</span></h5>
                                                </div>
                                                <div class="best-descb-rgt">
                                                    <div class="best-descb-rgt-row">
                                                        <input type="radio" checked id="r01" name="describe" value="1" <?php echo set_radio( 'describe', '1'); ?>>
                                                        <label for="r01"><span></span>
                                                            <p>Selling is my full-time job</p>
                                                        </label>
                                                    </div>
                                                    <div class="best-descb-rgt-row">
                                                        <input type="radio" id="r02" name="describe" value="2" <?php echo set_radio( 'describe', '2'); ?>>
                                                        <label for="r02"><span></span>
                                                            <p>I sell part-time but hope to sell full-time</p>
                                                        </label>
                                                    </div>
                                                    <div class="best-descb-rgt-row">
                                                        <input type="radio" id="r03" name="describe" value="3" <?php echo set_radio( 'describe', '3'); ?>>
                                                        <label for="r03"><span></span>
                                                            <p>I sell part-time and that’s how I like it</p>
                                                        </label>
                                                    </div>
                                                    <div class="best-descb-rgt-row">
                                                        <input type="radio" id="r04" name="describe" value="4" <?php echo set_radio( 'describe', '4'); ?>>
                                                        <label for="r04"><span></span>
                                                            <p>Other</p>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="error">
                                                    <?php echo form_error('describe') ?>
                                                </div>
                                            </div>
                                            <div class="best-descb">
                                                <div class="best-descb-lft">
                                                    <h5>Select your plan <span>*</span></h5>
                                                </div>
                                                <div class="best-descb-rgt">
                                                    <?php foreach($tiers as $i => $tir) { ?>
                                                        <div class="best-descb-rgt-row">
                                                            <input type="radio" id="r_<?=$i?>" class="tierclass" name="tier" disabled value="<?=$i+1?>" <?=($tir->tier_id==1)?'checked':''?>>
                                                            <label for="r_<?=$i?>"><span></span><p>Tier <?=$i?>  <?=($tir->tier_months > 0)?'('.$tir->tier_months.' months)':'Free'?> – <?=($tir->tier_amount > 0)?'$'.number_format($tir->tier_amount,2,'.','').'/month,':''?> <?=$tir->tier_fee?>% transaction fee</p>
                                                                <?php if($i == 2){?>
                                                                <div class="mst-poplr"><h6>Most Popular</h6></div>
                                                                <?php } ?>
                                                            </label>
                                                        </div>
                                                    <?php } ?>                                                                                                
                                                </div>
                                                <div class="error">
                                                    <?php echo form_error('tier') ?>
                                                </div>
                                            </div>
                                            <div class="get-paid-sec">
                                                <div class="get-paid-sec-hed">
                                                    <h4>How you’ll get paid</h4>
                                                    <span class="protected">Protected</span>
                                                    <img src="<?= base_url() ?>front_resources/images/paypal3.png" alt="" title="" class="img-responsive" />
                                                </div>
                                                <div class="get-paid-sec-lowr">
                                                    <img src="<?= base_url() ?>front_resources/images/brands-img.png" alt="" title="" />
                                                    <p>
                                                        eDucki Payment believes simple is better . We use Paypal for all your online sales so you can manage all of your accounts in one place. Don’t have a Paypal account? Don’t wory, you can create one here .<a href="#"> Learn more</a>
                                                    </p>
                                                </div>            
                                                <div class="get-paid-sec-proces-paymnt process-set">
                                                    <h6>Process Payment via Paypal Account<span>*</span></h6>
                                                    <div class="best-descb-rgt-row">
                                                        <input type="radio" id="r005" name="cc3" checked>
                                                        <label for="r005"><span></span>
                                                            <p>Paypal Id</p>
                                                        </label>
                                                    </div>
                                                    <input type="text" name="paypal_id" value="<?php echo set_value('paypal_id'); ?>" id="paypal_id" placeholder="JohnSmith@gmail.com" />
                                                    <div class="error">
                                                        <?php echo form_error('paypal_id') ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-profile-row">
                                                <div class="store-profile-row-lft">
                                                    <h4>Street Address</h4>
                                                </div>
                                                <div class="store-profile-row-rgt">
                                                    <input type="text" name="store_address" id="store_address" value="<?php echo set_value('store_address'); ?>" placeholder="Sample Text Here Sample Text Here Sample Text Here" />
                                                    <h6>Recommended if you have a physical store</h6>
                                                </div>
                                            </div>
                                            <div class="store-profile-row">
                                                <div class="store-profile-row-lft">
                                                    <h4>City <span>*</span></h4>
                                                </div>
                                                <div class="store-profile-row-rgt">
                                                    <input type="text" name="store_city" value="<?php echo set_value('store_city'); ?>" id="store_city"/>
                                                    <div class="error">
                                                        <?php echo form_error('store_city') ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-profile-row">
                                                <div class="store-profile-row-lft">
                                                    <h4>Country <span>*</span></h4>
                                                </div>
                                                <div class="store-profile-row-rgt">
                                                    <select name="store_country" id="store_country">
                                                        <option value="">Select Country</option>
                                                        <?php foreach ($allcountries as $country) { ?>
                                                        <option value="<?= $country['iso'] ?>" <?php echo set_select( 'store_country', $country[ 'iso'], (isset($page_data->store_country) ? $page_data->store_country == $country['iso'] ? true:'' : "")); ?> >
                                                            <?= $country['name'] ?>
                                                        </option>
                                                        <?php } ?>
                                                    </select>
                                                    <div class="error">
                                                        <?php echo form_error('store_country') ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-profile-row" id="us-states" style="display: none">
                                                <div class="store-profile-row-lft">
                                                    <h4>State <span>*</span></h4>
                                                </div>
                                                <div class="store-profile-row-rgt">
                                                    <select name="store_state">
                                                        <option value=""> Select State</option>
                                                        <?php foreach ($allstate as $state) { ?>
                                                        <option value="<?= $state['stat_id'] ?>" <?php echo set_select( 'store_state', $state[ 'stat_id'], (isset($page_data->store_state) ? $page_data->store_state == $state['stat_id'] ? true:'': "")); ?>>
                                                            <?= $state['stat_name'] ?>
                                                        </option>
                                                        <?php } ?>
                                                    </select>
                                                    <div class="error">
                                                        <?php echo form_error('store_state') ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-profile-row" id="other-states" style="display: none">
                                                <div class="store-profile-row-lft">
                                                    <h4>State <span>*</span></h4>
                                                </div>
                                                <div class="store-profile-row-rgt">
                                                    <input type="text" id="other_state" value="<?php echo set_value('other_state'); ?>" name="other_state" value="" />
                                                    <div class="error">
                                                        <?php echo form_error('other_state') ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="store-profile-row">
                                                <div class="store-profile-row-lft">
                                                    <h4>Zip <span>*</span></h4>
                                                </div>
                                                <div class="store-profile-row-rgt">
                                                    <input type="text" name="store_zip" id="store_zip" onblur="myMap()" value="<?php echo set_value('store_zip'); ?>" />
                                                    <div class="error">
                                                        <?php echo form_error('store_zip') ?> </div>
                                                </div>
                                            </div>
                                            <div class="store-profile-row">
                                                <div class="store-profile-row-lft">
                                                    <h4>Contact Number</h4>
                                                </div>
                                                <div class="store-profile-row-rgt">
                                                    <input type="text" id="store_number" name="store_number" placeholder="" value="<?php echo set_value('store_number'); ?>" onkeypress="return isNumber(event)" />
                                                    <h6>Will be displayed publically. Recommended if you have a physical store.</h6>
                                                </div>
                                            </div>
                                            <div class="store-profile-uplad">
                                                <div class="store-profile-row">
                                                    <div class="store-profile-row-lft">
                                                    </div>
                                                    <div class="store-profile-row-rgt">
                                                        <div class="store-logo">
                                                            <h6>Store Logo or a picture of yourself*</h6>
                                                            <input type='file' id="imgInpM" name="logo_img" onchange="javascript:$('#logo_image').val($(this).val())" accept="image/*" class="imgList" />
                                                            <h6>Upload image size 150 x 110</h6>
                                                            <div id="imgList">
                                                                <input type="hidden" name="logo_image" id="logo_image" value="" />
                                                            </div>
                                                            <div class="error"><?php echo form_error('logo_image') ?></div>
                                                        </div>
                                                        <div class="store-banner">
                                                            <h6>Store Banner*</h6>
                                                            <input type='file' id="imgInpN" accept="image/*" name="store_banner_img" onchange="javascript:$('#banner_image').val($(this).val())"  class="imgTwo" />
                                                            <h6 class="mrg-botm-0">Upload image size 1100 x 234</h6>
                                                            <div id="imgTwo">
                                                                <input type="hidden" name="store_banner_image" id="banner_image" value="" />
                                                            </div>
                                                            <div class="error"><?php echo form_error('store_banner_image') ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="store-profile-row">
                                                    <div class="store-profile-row-lft">
                                                        <h4>Store Description</h4>
                                                    </div>
                                                    <div class="store-profile-row-rgt">
                                                        <textarea name="store_description" id="store_description" placeholder="Great items with a lot of love attached"><?php echo set_value('store_description'); ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gve-othr">
                                                <h5>Give others the opprtunity to connect with you</h5>
                                                <div class="store-profile-row">
                                                    <div class="store-profile-row-lft">
                                                        <h4>Facebook Page Url</h4>
                                                    </div>
                                                    <div class="store-profile-row-rgt">
                                                        <input type="text" name="store_facebook_url" id="store_facebook_url" value="<?php echo set_value('store_facebook_url'); ?>" placeholder="https://www.facebook.com/example" />
                                                        <h6 class="mrg-botm-5">Ex: https://www.facebook.com/example</h6>
                                                    </div>
                                                </div>
                                                <div class="store-profile-row">
                                                    <div class="store-profile-row-lft">
                                                        <h4>Twitter Page Url</h4>
                                                    </div>
                                                    <div class="store-profile-row-rgt">
                                                        <input type="text" name="store_twitter_url" id="store_twitter_url" value="<?php echo set_value('store_twitter_url'); ?>" placeholder="https://www.twitter.com/example" />
                                                        <h6 class="mrg-botm-5">Ex: https://www.twitter.com/example</h6>
                                                    </div>
                                                </div>
                                                <div class="store-profile-row">
                                                    <div class="store-profile-row-lft">
                                                        <h4>Instagram Page Url</h4>
                                                    </div>
                                                    <div class="store-profile-row-rgt">
                                                        <input type="text" name="store_instagram_url" id="store_instagram_url" value="<?php echo set_value('store_instagram_url'); ?>" placeholder="https://www.instagram.com/example" />
                                                        <h6 class="mrg-botm-5">Ex: https://www.instagram.com/example</h6>
                                                    </div>
                                                </div>
                                                <div class="store-profile-row">
                                                    <div class="store-profile-row-lft">
                                                        <h4>Pintrest Page Url</h4>
                                                    </div>
                                                    <div class="store-profile-row-rgt">
                                                        <input type="text" name="store_pintrest_url" id="store_pintrest_url" value="<?php echo set_value('store_pintrest_url'); ?>" placeholder="https://www.pintrest.com/example" />
                                                        <h6>Ex: https://www.pintrest.com/example</h6>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="store_latitude" id="store_latitude">
                                                <input type="hidden" name="store_longitude" id="store_longitude">
                                                <input type="hidden" name="user_id" value="<?=$userid?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mu-fld-sub">
                        <?php if ($this->session->userdata('admin_status') == 1) { ?>
                        <input type="submit" value="Submit" />
                        <?php } ?>
                        <a href="<?php echo base_url() ?>admin/user">cancel</a>
                    </div>
                    <!-- Box wihout label section ends here -->
                </div>
                <?php echo form_close(); ?>
        </div>
</body>

</html>
<script>
function readURLm(input) {
    if (input.files.length > 0) {
        var reader = new FileReader();
        $.each(input.files, function(key, val) {
            {

                reader.onload = function(e) {
                    $('<img>').attr('src', e.target.result).appendTo('#imgList');
                    $('.cross-btn').css('display', 'block')
                }
                reader.readAsDataURL(val);
            }
        });
    }
}

function readURL(input) {
    if (input.files.length > 0) {
        var reader = new FileReader();
        $.each(input.files, function(key, val) {
            {

                reader.onload = function(e) {
                    $('<img>').attr('src', e.target.result).appendTo('#imgTwo');
                    $('.cross-btn2').css('display', 'block')
                }
                reader.readAsDataURL(val);
            }
        });
    }
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

$("#logo_img").change(function() {
    readURLm(this);
});

$("#store_banner_img").change(function() {
    readURL(this);
});

$(function() {
    $('#store_country').change(function() {
        if ($('#store_country').val() === 'US') {
            $('#us-states').show("slow");
            $('#other_state').val('');
            $('#other-states').hide("slow");
        } else {
            $('#us-states').hide("slow");
            $('#other-states').show("slow");
        }
    });
});

$(document).ready(function(){
    var country = '<?=@$_POST['store_country']?>';
    if(country == 'US'){
      $('#us-states').show("slow"); 
      $('#other-states').hide("slow"); 
    }
    else {
      $('#us-states').hide("slow");
      $('#other-states').show("slow");
    }
});
</script>
<script>
function myMap() {
    var zipcode = $("#store_zip").val();
    var geocoder = new google.maps.Geocoder();

    geocoder.geocode({ 'address': zipcode }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            $("#store_latitude").val(latitude);
            $("#store_longitude").val(longitude);
        }
    });
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAscDVsx1aPq_cm_9DaZAmj6zOIPjhLDpo&callback=myMap"></script>