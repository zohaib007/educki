<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title; ?></title>

<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>

<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.daterange.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/core.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/zebra_datepicker.js"></script>

<link rel="stylesheet" href="<?= base_url() ?>css/defualt.css" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/jquery-ui-1.8.13.custom.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/fullcalendar.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />

</head>
<body>
<div class="container_p"> 
  
  <!-- Dashboard Left Side Begins Here -->
  <div class="left_wrp">
    <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
  </div>
  <!-- Dashboard Left Side Ends Here -->
  
  <div class="right-rp"> 
    
    <!-- Top Green Bar Section Begins Here -->
    <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
    <!-- Top Green Bar Section Ends Here -->
    <div class="mu-contnt-wrp">
      <div class="mu-contnt-hdng">
        <h1><?php echo $title; ?></h1>
      </div>
      <!-- Bread crumbs starts here -->
      <div class="n-crums">
        <ul>
          <li> <a href="<?php echo base_url() ?>admin/user">User Management</a> </li>
          <li>
            <div class="crms-sep">&gt;</div>
          </li>
          <li> <a href="javascript:void(0);">User Orders</a> </li>
        </ul>
      </div>
      <!-- Bread crumbs ends here --> 
      
      <!-- Box along with label section begins here -->
      
      <div class="mu-contnt-outer">
        <div class="mu-contnt-lfLbl ad-size">
          <h2>User Orders</h2>
          <p>Download user order details.</p>
        </div>
        <?php echo form_open_multipart('', array('name'=>'myForm')); ?>
        <div class="mu-contntBx-wrp">
          <div class="contntTop-row">
            
            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
              	<label>Get User Order Details by Date Range</label>
                <div class="ndpdSort" style="width:auto; float:left; margin-right: 274px; width: auto;" >
                    <button class="pro-addNew" id="nsortBy">Download Date Range Orders <span class="dn-angl"></span></button>
                    <div class="dropval"></div>
                    <div class="ndp-nx" id="nsortDp" style="display:none; width: 135px;">
                    	<ul>
                            <li>
                                <div class="usr-srt-dp subDpd"> Select date range <span class="sub-dn-angl" style="top: 10px;"></span> </div>
                                <div class="dp-dt-pik" style="display:none;">
                                    <input name="date_range" style="width: 100%;" placeholder="Select date to download order" id="d" class="form__daterange catName" autocomplete="off" readonly="readonly" />
                                    <input type="submit" value="Submit" class="date-range"/>
                                </div>
                            </li>
                        </ul>
                    </div>
            	</div>
              </div>
              <div class="error"><?php //echo form_error('gift_message')?> </div>
            </div>
            
            
            <div class="mu-flds-wrp">
              <div class="mu-frmFlds_long">
              	<label>Download Last Year's Orders</label>
                <div class="ndpdSort" style="width:auto; float:left; margin-right: 274px; width: auto;" >
              		<input type="hidden" name="last_year" value=""/>
                    <button class="pro-addNew" id="last-year" style="padding-right:5px;">Download Last Year's Orders</button>
            	</div>
              </div>
            </div>
            
            
          </div>
          
        </div>
        <?php echo form_close(); ?> </div>
    </div>
    
    <!-- Box along with label section ends here --> 
    
</div>
</div>
<script>
$(function () {
	jQuery.noConflict();
	jQuery("#d").daterange({
		dateFormat: "yy-mm-dd",
		maxDate: 0,
	});
});

$(document).ready(function() {
	jQuery('#nsortBy').click(function(e){
		e.preventDefault();
		jQuery('#nsortDp').slideToggle();
	});
	
	jQuery('.subDpd').click(function(){
		jQuery('.dp-dt-pik').slideToggle();
	});

	jQuery('.valclick').click(function(){
		jQuery('.dropval').html($(this).attr("title"));
		jQuery('#nsortDp').slideToggle();
	});
	
	jQuery("#last-year").click(function(e){
		e.preventDefault();
		jQuery("input[name='last_year']").val('1');
		jQuery("form").submit();		
	});
	
	jQuery(".date-range").click(function(){
		var date_range = jQuery("input[name='date_range']").val();
		if(date_range=='')
		{
			return false;
		}
		else
		{
			jQuery('#nsortDp').slideToggle();
			return true;
		}
	});
});

</script>
</body>
</html>