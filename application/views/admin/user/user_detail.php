<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?= $title ?></title>
        <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
        <?php include(ADMIN_INCLUDE_PATH . "includes/js.php"); ?>
    </head>
    <body>
        <div class="container">
            <!-- Dashboard Left Side Begins Here -->
            <div class="left_wrp">
                <?php include(ADMIN_INCLUDE_PATH . "includes/dash-left.php"); ?>
            </div>
            <!-- Dashboard Left Side Ends Here -->
            <div class="right-rp">
                <!-- Top Green Bar Section Begins Here -->
                <?php
                include(ADMIN_INCLUDE_PATH . "includes/top_green_bar.php");
                ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="col-continer">
                    <div class="col-3 cat-box">
                        <!-- Bread crumbs starts here -->
                        <div class="n-crums">
                            <ul>
                                <li>
                                    <a href="<?php echo base_url() ?>admin/user">Manage User</a>
                                </li>
                                <li>
                                    <div class="crms-sep">&gt;</div>
                                </li>
                                <li>

                                    <a href="javascript:void(0);">Applied User Details</a>
                                </li>
                            </ul>
                        </div>
                        <!-- Bread crumbs ends here -->
                        <div class="contntTop-row"> <!-- col-box1 --> 
                            <!-- Top Navigation Starts Here -->
                            
                            <!-- Top Navigation Ends Here -->
                            <!-- 01-5-1 Page Starst Here -->
                            <div style="display:block">
                                    <div class="add-frm-container">
                                            <div class="add-frm-wrp">
                                                <label>Client ID:</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="clientid" readonly="readonly" value="<?php echo $results['user_id']; ?>"/>  
                                                </div>
                                            </div>
                                            <div class="add-frm-wrp">
                                                <label>Name:</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="name" readonly="readonly" value="<?php echo $results['user_fname'] .' '. $results['user_lname'] ; ?>"/>  
                                                </div>
                                            </div>
                                            <div class="add-frm-wrp">
                                                <label>Activity(# of days  since last active):</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="activity last login" readonly="readonly" value="<?php echo $results['last_active_difference']; ?>"/>  
                                                </div>
                                            </div>
                                            <div class="add-frm-wrp">
                                                <label>Customer Since:</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="lname" readonly="readonly" value="<?php echo date('Y-m-d', strtotime($results['user_register_date'])); ?>"/>  
                                                </div>
                                            </div>
                                            <div class="add-frm-wrp">
                                                <label>Gross Merchandise(volume) Sales:</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="created_date" readonly="readonly" value="<?php echo $results['user_gross_sales']; ?>">  
                                                </div>
                                            </div>
                                            <div class="add-frm-wrp">
                                                <label>Subscription Revenue:</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="phone" readonly="readonly" value="<?php echo $results['subscription_revenue']; ?>"/>  
                                                </div>
                                            </div>
                                            <div class="add-frm-wrp">
                                                <label>Subscription Type(Tier):</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="email" readonly="readonly" value="<?php echo $results['subscription_type']; ?>"/>  
                                                </div>
                                            </div>

                                            <div class="add-frm-wrp">
                                                <label>Number of Months Left in Subscription:</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="status" readonly="readonly" value="<?php echo $results['subscription_date']; ?>"/>  
                                                </div>
                                            </div>
                                            <div class="add-frm-wrp">
                                                <label>Number of Times Subscribed:</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="status" readonly="readonly" value="<?php echo $results['subscription_count']; ?>"/>  
                                                </div>
                                            </div>
                                            <div class="add-frm-wrp">
                                                <label>Total #  of Months Subscribed:</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="status" readonly="readonly" value="<?php echo $results['subscription_date']; ?>"/>  
                                                </div>
                                            </div>
                                            <div class="add-frm-wrp">
                                                <label>New or Renewal:</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="status" readonly="readonly" value="<?= $results['user_renewal']== 0 ? 'First Time' : 'Renewal' ?>"/>  
                                                </div>
                                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>