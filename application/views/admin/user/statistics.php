<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>User Accounts</title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
<link rel="shortcut icon" type="image/x-icon" href="./favicon.ico" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/jquery-ui-1.8.13.custom.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/fullcalendar.css" />
<link rel="stylesheet" href="<?=base_url()?>js/tablesort/style.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php");?>
<script src="<?php echo base_url(); ?>js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.daterange.js"></script>

<!-- <script type="text/javascript" src="<?= base_url() ?>js/commonAjax.js"></script>-->

<!--  <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.min.js"></script>

<script type="text/javascript" src="<?= base_url() ?>js/fullcalendar.min.js"></script>

<script type="text/javascript" src="<?= base_url() ?>js/gcal.js"></script>

<script type="text/javascript" src="<?= base_url() ?>js/jquery.wysiwyg.js"></script>-->

<!--<script type="text/javascript" src="<?= base_url() ?>js/ws.init.js"></script>-->

<!--//////////////////////////////table sorter////////////////////////////////////////////////-->
<script type="text/javascript" src="<?php echo base_url(); ?>js/tablesort/jquery-latest.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/tablesort/jquery.tablesorter.js"></script>
<script>

$(function () {
    $('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');
    //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
    $('.message .close').hover(
        function() { $(this).addClass('hover'); },
        function() { $(this).removeClass('hover'); }
    );
        
    $('.message .close').click(function() {
        $(this).parent().fadeOut('slow', function() { $(this).remove(); });
    });
});
    
/*	$(function() {
        $("#mytable").tablesorter({debug: true})
        
        $("a.append").click(appendData);
    });*/
$(document).ready(function() { 
    $("#mytable").tablesorter({ 
        // pass the headers argument and assing a object 
        headers: { 
            // assign the sixth column (we start counting zero) 
            6: { 
                // disable it by setting the property sorter to false 
                sorter: false 
            }, 
        } 
    }); 
});
    
    var lastStudent = 23;
    var limit = 500;
    
    function appendData() {
        
        var tdTagStart = '<td>';
        var tdTagEnd = '</td>';
        var sex = ['male','female'];
        var major = ['Mathematics','Languages'];
        
        
        for(var i = 0; i < limit; i++) { 
            var rnd = i % 2;
            var row = '<tr>';	
            row += tdTagStart + 'student' + (lastStudent++) + tdTagEnd;
            row += tdTagStart + major[rnd] + tdTagEnd;
            row += tdTagStart + sex[rnd] + tdTagEnd;
            
            row += tdTagStart +  randomNumber() + tdTagEnd;
            row += tdTagStart +  randomNumber() + tdTagEnd;
            row += tdTagStart +  randomNumber() + tdTagEnd;
            row += tdTagStart +  randomNumber() + tdTagEnd;
            
            row += '</tr>';
            
            $("table/tbody:first").append(row);
            
        };
        
        
        $("table").trigger('update');
        return false;
    }
    
    function randomNumber() {
        return Math.floor(Math.random()*101)
    }
</script>
<!--//////////////////////////////table sorter end////////////////////////////////////////////////-->

<script>

$(function () {

    $('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');

    //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)

    $('.message .close').hover(

        function() { $(this).addClass('hover'); },

        function() { $(this).removeClass('hover'); }

    );

        

    $('.message .close').click(function() {

        $(this).parent().fadeOut('slow', function() { $(this).remove(); });

    });

});

$(document).ready(function() {

    $("#showMessage").css('display','none');



});


$(document).ready(function() {

    $("#showMessage").css('display','none');

    $("a.userchangeStatus1").click(function(e){

        

        var parent = $(this).parent();

        var myarray = parent.attr('id').split("_");

    

        var con =confirm("Are you sure you want to change the status?");

        if(con == true)

        {

            var parent = $(this).parent();

            var myarray = parent.attr('id').split("_");
            if(myarray[0] == 1){

                $("#img"+myarray[1]).attr('src', base_url+'images/tick-btn.png');

                $("#imag"+myarray[1]).attr('src', base_url+'images/visible.png');
                parent.attr('id', '0_'+myarray[1]);

            }else{
                $("#img"+myarray[1]).attr('src', base_url+'images/cross-btn.png');

                $("#imag"+myarray[1]).attr('src', base_url+'images/invisible.png');

                parent.attr('id', '1_'+myarray[1]);

            }

            e.preventDefault();

            $("#showMessage").css('display','block');

            $("#showMessage").load(base_url+'admin/user/changeStatus', {"nId":myarray[1],"bStatus":myarray[0], <?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'} );
        }
    });

});

$(document).ready(function() {

    $("#showMessage").css('display','none');

    $("a.userchangeStatus2").click(function(e){
        var con =confirm("Are you sure you want to change the status (V/I?");
        if(con == true)
        {
            var parent = $(this).parent();
            var myarray = parent.attr('id').split("_");
            if(myarray[0] == 1){

                $("#imag"+myarray[1]).attr('src', base_url+'images/visible.png');

                $("#img"+myarray[1]).attr('src', base_url+'images/tick-btn.png');

                parent.attr('id', '2_'+myarray[1]);

            }
			else if (myarray[0] == 2)
			{
				$("#imag"+myarray[1]).attr('src', base_url+'images/invisible.png');
				parent.attr('id', '1_'+myarray[1]);
			}
			e.preventDefault();
			
			$("#showMessage").css('display','block');
		    $("#showMessage").load(base_url+'admin/user/changeStatus', {"nId":myarray[1],"bStatus":myarray[0], <?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'} );
        }

    });

});
</script>
</head>

<body>
<div class="container">
    
    <div class="left_wrp" id="menu-top">
        <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
    </div>
    
    <!-- Dashboard Left Side Ends Here -->
    <div class="right-rp"> 
    <!-- Top Green Bar Section Begins Here -->
    <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");?>
    <!-- Top Green Bar Section Ends Here -->
        <div class="mu-contnt-hdng">
            <h1>Manage Users</h1>
        </div>
        <div class="col-continer">
            <div class="col-3 cat-box">
                <div class="contntTop-row"> <!-- col-box1 -->
                    
                    <div class="actvity-hd">
                        <div class="act-tab b-ad-tb">
                            <table cellspacing="0" border="0">
                                <colgroup>
                                <col width="50%" />
                                <col width="50%" />
                                </colgroup>
                                <tr>
                                    <td class="no-bdr">
                                        <div class="tnav-list">
                                            <a href="<?=base_url()?>admin/user/statistics" class="active" title="User Statistics">User Statistics</a>
                                            <span>|</span>
                                            <a href="<?=base_url()?>admin/user" class="" title="Manage Users">Manage Users</a>
                                        </div>
                                    </td>
                                    <!-- <td align="right" class="expo-cell" style="float: right;width: 190px;border-left: 0 !important;">
                                            <span>Export to:</span> 
                                            <!--<form method="post" name="MyFormExport">->
                                            <?php echo form_open('',array("name"=>"MyFormExport"));?>
                                            <span>
                                                &nbsp;&nbsp;
                                                <a href="<?=base_url()?>admin/user/exportcsv">CSV</a>
                                                <!-- &nbsp;&nbsp;|&nbsp;&nbsp;
                                                <a href="<?=base_url()?>admin/user/exportexcelxml">Excel XML</a>-> 
                                            </span>
                                            <?php echo form_close();?>
                                        </td> -->
                                </tr>
                            </table>
                        </div>
                        <?php
                        if($this->session->flashdata('msg') != "")
                        {
                            echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
                        }
                        ?>
                    </div>
                    
                    <!-- Bottom Section Starts -->
                    <div class="nsec-btm">
                        
                        <!-- Section Starts -->
                        <div class="mtr-blk-001">
                        	<div class="mtr-hdr">
                        		<table cellpadding="0" cellspacing="0" border="0">
                                	<tr>
                                    	<td>
                                        	# Active Sellers
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="mtr-btm">
                        		<table cellpadding="0" cellspacing="0" border="0">
                                	<tr>
                                    	<td>
                                        	<?=$activeSellers['ActiveSellers']?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- Section Ends -->
                        
                        <!-- Section Starts -->
                        <div class="mtr-blk-001">
                        	<div class="mtr-hdr">
                        		<table cellpadding="0" cellspacing="0" border="0">
                                	<tr>
                                    	<td>
                                        	# Sellers on Vacation
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="mtr-btm">
                        		<table cellpadding="0" cellspacing="0" border="0">
                                	<tr>
                                    	<td>
                                        	<?=$SellersOnVacation['SellersOnVacation']?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- Section Ends -->
                        
                        <!-- Section Starts -->
                        <div class="mtr-blk-001">
                        	<div class="mtr-hdr">
                        		<table cellpadding="0" cellspacing="0" border="0">
                                	<tr>
                                    	<td>
                                        	Total Registered User
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="mtr-btm">
                        		<table cellpadding="0" cellspacing="0" border="0">
                                	<tr>
                                    	<td>
                                        	<?=$TotalRegisteredUsers['TotalRegisteredUsers']?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- Section Ends -->
                        
                        <!-- Section Starts -->
                        <div class="mtr-blk-001">
                        	<div class="mtr-hdr">
                        		<table cellpadding="0" cellspacing="0" border="0">
                                	<tr>
                                    	<td>
                                        	Total Number of Sellers
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="mtr-btm">
                        		<table cellpadding="0" cellspacing="0" border="0">
                                	<tr>
                                    	<td>
                                        	<?=$TotalSellers['TotalSellers']?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- Section Ends -->
                        
                        <!-- Section Starts -->
                        <div class="mtr-blk-001">
                        	<div class="mtr-hdr">
                        		<table cellpadding="0" cellspacing="0" border="0">
                                	<tr>
                                    	<td>
                                        	Total Buyers
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="mtr-btm">
                        		<table cellpadding="0" cellspacing="0" border="0">
                                	<tr>
                                    	<td>
                                        	<?=$TotalBuyers['TotalBuyers']?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- Section Ends -->
                        
                        <!-- Section Starts -->
                        <div class="mtr-blk-001">
                        	<div class="mtr-hdr">
                        		<table cellpadding="0" cellspacing="0" border="0">
                                	<tr>
                                    	<td>
                                        	# of Users Reported/Flagged
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="mtr-btm">
                        		<table cellpadding="0" cellspacing="0" border="0">
                                	<tr>
                                    	<td>
                                        	<?=$TotalFlaggedUsers['TotalFlaggedUsers']?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- Section Ends -->
                        
                    </div>
                    <!-- Bottom Section Ends -->
                    
                    <!-- Bottom Table Section Starts -->
                    <div class="nbtm-row">
                    	<div class="totsub-tbl">
                        	<table cellpadding="0" cellspacing="0" border="0">
                            	<colgroup>
                                	<col width="40%" />
                                	<col width="12%" />
                                	<col width="12%" />
                                	<col width="12%" />
                                	<col width="12%" />
                                	<col width="12%" />
                                </colgroup>
                            	<tr>
                                	<th align="left">Total Subscribers</th>
                                	<th align="center">Total</th>
                                	<th align="center">Exisiting</th>
                                	<th align="center">Renewed</th>
                                	<th align="center">New</th>
                                	<th align="center" class="last">Churn</th>
                                </tr>
                                <?php                                    
                                    $total_e = $tier_1['existing']+$tier_2['existing']+$tier_3['existing'];
                                    $total_n = count($tier_1['New'])+count($tier_2['New'])+count($tier_3['New']);
                                    $total_c = $tier_1['churn']+$tier_2['churn']+$tier_3['churn'];
                                    $total_r = $tier_1['Renewal']+$tier_2['Renewal']+$tier_3['Renewal'];
                                    $total_1 = $total_e+$total_n+$total_r;//+$total_c
                                ?>
                            	<tr>
                                	<td align="left">
                                    	<strong>Total Subscribers</strong>
                                    </td>
                                	<td align="center">
                                    	<strong><?php echo $total_1;?></strong>
                                    </td>
                                	<td align="center">
                                    	<strong><?php  echo $total_e;?></strong>
                                    </td>
                                	<td align="center">
                                    	<strong><?php echo $total_r;?></strong>
                                    </td>
                                	<td align="center">
                                    	<strong><?php echo $total_n;?></strong>
                                    </td>
                                	<td align="center">
                                    	<strong><?php echo $total_c;?></strong>
                                    </td>
                                </tr>
                            	<?php                                    
                                    $total_1 = $tier_1['existing']+$tier_1['New']+$tier_1['Renewal'];
                                ?>
                                <tr>
                                    <td align="left">
                                        #  of Subscribers in Tier 1 (<?=getTierMonths(2);?> months) - $<?=getTierAmount(2);?>
                                    </td>
                                    <td align="center">
                                        <strong><?=$total_1?></strong>
                                    </td>
                                    <td align="center">
                                        <strong><?=$tier_1['existing']?></strong>
                                    </td>
                                    <td align="center">
                                        <strong><?=$tier_1['Renewal']?></strong>
                                    </td>
                                    <td align="center">
                                        <strong><?=count($tier_1['New'])?></strong>
                                    </td>
                                    <td align="center">
                                        <strong><?=$tier_1['churn']?></strong>
                                    </td>
                                </tr>
                            	<?php                                    
                                    $total_2 = $tier_2['existing']+$tier_2['New']+$tier_2['Renewal'];
                                ?>
                                <tr>
                                    <td align="left">
                                        #  of Subscribers in Tier 2 (<?=getTierMonths(3);?> months) - $<?=getTierAmount(3);?>
                                    </td>
                                    <td align="center">
                                        <strong><?=$total_2?></strong>
                                    </td>
                                    <td align="center">
                                        <strong><?=$tier_2['existing']?></strong>
                                    </td>
                                    <td align="center">
                                        <strong><?=$tier_2['Renewal']?></strong>
                                    </td>
                                    <td align="center">
                                        <strong><?=count($tier_2['New'])?></strong>
                                    </td>
                                    <td align="center">
                                        <strong><?=$tier_2['churn']?></strong>
                                    </td>
                                </tr>
                                <?php                                    
                                    $total_3 = $tier_3['existing']+$tier_3['New']+$tier_3['Renewal'];
                                ?>
                            	<tr>
                                	<td align="left">
                                        #  of Subscribers in Tier 3 (<?=getTierMonths(4);?> months) - $<?=getTierAmount(4);?>
                                    </td>
                                    <td align="center">
                                        <strong><?=$total_3?></strong>
                                    </td>
                                    <td align="center">
                                        <strong><?=$tier_3['existing']?></strong>
                                    </td>
                                    <td align="center">
                                        <strong><?=$tier_3['Renewal']?></strong>
                                    </td>
                                    <td align="center">
                                        <strong><?=count($tier_3['New'])?></strong>
                                    </td>
                                    <td align="center">
                                        <strong><?=$tier_3['churn']?></strong>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- Bottom Table Section Ends -->
                    
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

	$(function () {
		jQuery.noConflict();
		$("#d").daterange({
			dateFormat: "yy-mm-dd",
			maxDate: 0,
		});
	});

	$(document).ready(function() {
		$('#nsortBy').click(function(){
			$('#nsortDp').slideToggle();
		});
		
		$('.subDpd').click(function(){
			$('.dp-dt-pik').slideToggle();
		});

		$('.valclick').click(function(){
			$('.dropval').html($(this).attr("title"));
			$('#nsortDp').slideToggle();
		});
	});

</script> 
<script src="<?= base_url() ?>/js/jquery.filtertable.min.js"></script>
</body>
</html>
