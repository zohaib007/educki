<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        <?php echo $title; ?>
    </title>
    <?php include ADMIN_INCLUDE_PATH . "includes/js.php";?>
    <link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
    <!-- <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" /> -->
    <script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>js/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>js/jquery.validate.min.js"></script>
</head>

<body>
    <div class="container">
        <!-- container  -->
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
            <?php include ADMIN_INCLUDE_PATH . "includes/dash-left.php";?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        <div class="right-rp">
            <!-- Top Green Bar Section Begins Here -->
            <?php include ADMIN_INCLUDE_PATH . "includes/top_green_bar.php";?>
            <!-- Top Green Bar Section Ends Here -->
            <div class="mu-contnt-wrp">
                <div class="mu-contnt-hdng">
                    <h1>
           <?=$title?>
         </h1>
                    <a href="<?php echo base_url() ?>admin/user" class="c-b-s-l" style="display:none;"> Back <img src="<?=base_url()?>images/back0icon.jpg" alt="" /> </a> </div>
                <div class="n-crums">
                    <ul>
                        <li> <a href="<?php echo base_url() ?>admin/user">Manage Users</a> </li>
                        <li>
                            <div class="crms-sep">&gt;</div>
                        </li>
                        <li> <a href="javascript:void(0)">Edit User</a> </li>
                    </ul>
                </div>
                <?php echo form_open_multipart('', array('id' => 'edit_user')); ?>
                <!-- Box along with label section begins here -->
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Account Details</h2>
                        <p>Manage the registered accounts on your website.</p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <p><label>Customer ID:&nbsp;<span style="font-family: Arial, Helvetica, sans-serif;font-size: 13px;color: #000;font-weight: normal;"><?=$user_data['user_id']?></span></label></p>
                                    <p><label>Activity(days since last active):&nbsp;<span style="font-family: Arial, Helvetica, sans-serif;font-size: 13px;color: #000;font-weight: normal;"><?=$user_data['last_active_difference']?> day(s)</span></label></p>
                                    <p><label>Customer Since:&nbsp;<span style="font-family: Arial, Helvetica, sans-serif;font-size: 13px;color: #000;font-weight: normal;"><?=date('m-d-y',strtotime($user_data['user_register_date']))?></span></label></p>
                                    <p><label>Number of items in wishlist:&nbsp;<span style="font-family: Arial, Helvetica, sans-serif;font-size: 13px;color: #000;font-weight: normal;"><?=countWishListItem($user_data['user_id'])?></span></label></p>
                                    <?php if($user_data['user_type']=='Seller'){ ?>
                                        <p><label># Number of times user flagged:&nbsp;<span style="font-family: Arial, Helvetica, sans-serif;font-size: 13px;color: #000;font-weight: normal;"><?=user_reported($user_data['user_id'])?></span></label></p>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>First Name*</label>
                                    <input type="text" name="user_fname" value="<?php if (isset($_POST['user_fname']) && $_POST['user_fname'] != '') {echo set_value('user_fname');} else {echo htmlentities($user_data['user_fname']);}?>" />
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_fname') ?> </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Last Name*</label>
                                    <input type="text" name="user_lname" value="<?php if (isset($_POST['user_lname']) && $_POST['user_lname'] != '') {echo set_value('user_lname');} else {echo htmlentities($user_data['user_lname']);}?>" />
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_lname') ?> </div>
                            </div>
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Email*</label>
                                    <input type="text" name="user_email" readonly value="<?php if (set_value('user_email') != '') {echo set_value('user_email');} else {echo htmlentities($user_data['user_email']);}?>" />
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_email') ?> </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Gender*</label>
                                    <select name="user_sex" id="user_sex">
                                        <option value="Male" <?php if(set_select('user_sex','Male')!='' ) {echo set_select( 'user_sex', 'Male');} else if ($user_data[ 'user_sex']=="Male" ) {echo "selected";}?>>Male</option>
                                        <option value="Female" <?php if (set_select( 'user_sex', 'Female') !='' ) {echo set_select( 'user_sex', 'Female');} else if ($user_data[ 'user_sex']=="Female" ) {echo "selected";}?>>Female</option>
                                        <option value="Other" <?php if (set_select( 'user_sex', 'Other') !='' ) {echo set_select( 'user_sex', 'Other');} else if ($user_data[ 'user_sex']=="Other" ) {echo "selected";}?>>Other</option>
                                    </select>
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_sex') ?> </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Interest*</label>
                                    <select name="user_interests" id="user_interests">
                                        
                                        <option value="">Interests</option>                                                                                
                                        <option value="Fertility" <?php if ($user_data[ 'user_interests']=="Fertility" ) {echo "selected";}?>>Fertility</option>
                                        <option value="Pregnancy" <?php if ($user_data[ 'user_interests']=="Pregnancy" ) {echo "selected";}?>>Pregnancy</option>
                                        <option value="Baby names" <?php if ($user_data[ 'user_interests']=="Baby names" ) {echo "selected";}?>>Baby Names</option>
                                        <option value="Baby" <?php if ($user_data[ 'user_interests']=="Baby" ) {echo "selected";}?>>Baby</option>
                                        <option value="Toddler" <?php if ($user_data[ 'user_interests']=="Toddler" ) {echo "selected";}?>>Toddler</option>
                                        <option value="Child" <?php if ($user_data[ 'user_interests']=="Child" ) {echo "selected";}?>>Child</option>
                                        <option value="Family" <?php if ($user_data[ 'user_interests']=="Family" ) {echo "selected";}?>>Family</option>
                                        <option value="Advice" <?php if ($user_data[ 'user_interests']=="Advice" ) {echo "selected";}?>>Advice</option>
                                        <option value="Food" <?php if ($user_data[ 'user_interests']=="Food" ) {echo "selected";}?>>Food</option>
                                        <option value="Other" <?php if ($user_data[ 'user_interests']=="Other" ) {echo "selected";}?>>Other</option>
                                    </select>
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_interests') ?> </div>
                            </div>
                            <div class="mu-flds-wrp" id="otherinterest" <?=($user_data[ 'user_interests']=="Other" ) ? 'style="display: block"' : 'style="display: none"'?> >
                                <div class="mu-frmFlds_long">
                                    <input type="text" name="user_other_interest" value="<?php if (isset($_POST['user_other_interest']) && $_POST['user_other_interest'] != '') {echo set_value('user_other_interest');} else {echo htmlentities($user_data['user_other_interest']);}?>" />
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_other_interest') ?> </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Interest*</label>
                                    <select name="user_interests_second" id="user_interests_second">
                                        <option value="">Interests</option>
                                        <option value="Fertility" <?php if ($user_data[ 'user_interests_second']=="Fertility" ) {echo "selected";}?>>Fertility</option>
                                        <option value="Pregnancy" <?php if ($user_data[ 'user_interests_second']=="Pregnancy" ) {echo "selected";}?>>Pregnancy</option>
                                        <option value="Baby names" <?php if ($user_data[ 'user_interests_second']=="Baby names" ) {echo "selected";}?>>Baby Names</option>
                                        <option value="Baby" <?php if ($user_data[ 'user_interests_second']=="Baby" ) {echo "selected";}?>>Baby</option>
                                        <option value="Toddler" <?php if ($user_data[ 'user_interests_second']=="Toddler" ) {echo "selected";}?>>Toddler</option>
                                        <option value="Child" <?php if ($user_data[ 'user_interests_second']=="Child" ) {echo "selected";}?>>Child</option>
                                        <option value="Family" <?php if ($user_data[ 'user_interests_second']=="Family" ) {echo "selected";}?>>Family</option>
                                        <option value="Advice" <?php if ($user_data[ 'user_interests_second']=="Advice" ) {echo "selected";}?>>Advice</option>
                                        <option value="Food" <?php if ($user_data[ 'user_interests_second']=="Food" ) {echo "selected";}?>>Food</option>
                                        <option value="Other" <?php if ($user_data[ 'user_interests_second']=="Other" ) {echo "selected";}?>>Other</option>
                                    </select>
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_interests_second') ?> </div>
                            </div>
                            <div class="mu-flds-wrp" id="otherinterestsecond" <?=($user_data[ 'user_interests_second']=="Other" ) ? 'style="display: block"' : 'style="display: none"'?> >
                                <div class="mu-frmFlds_long">
                                    <input type="text" name="user_other_interest_second" value="<?php if (isset($_POST['user_other_interest_second']) && $_POST['user_other_interest_second'] != '') {echo set_value('user_other_interest_second');} else {echo htmlentities($user_data['user_other_interest_second']);}?>" />
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_other_interest_second') ?> </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Age*</label>
                                    <select name="user_age">
                                        <option value="" <?php if ($user_data[ 'user_age']=="" ) {echo "selected";}?>>Select Age</option>
                                        <option value="13-17" <?php if ($user_data[ 'user_age']=="13-17" ) {echo "selected";}?>>13-17</option>
                                        <option value="18-24" <?php if ($user_data[ 'user_age']=="18-24" ) {echo "selected";}?>>18-24</option>
                                        <option value="25-34" <?php if ($user_data[ 'user_age']=="25-34" ) {echo "selected";}?>>25-34</option>
                                        <option value="35-44" <?php if ($user_data[ 'user_age']=="35-44" ) {echo "selected";}?>>35-44</option>
                                        <option value="45-54" <?php if ($user_data[ 'user_age']=="45-54" ) {echo "selected";}?>>45-54</option>
                                        <option value="55-64" <?php if ($user_data[ 'user_age']=="55-64" ) {echo "selected";}?>>55-64</option>
                                        <option value="65+" <?php if ($user_data[ 'user_age']=="65+" ) {echo "selected";}?>>65+</option>
                                    </select>
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_age') ?> </div>
                            </div>

                            <?php if($user_data['user_facebook_id']==''){?>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Password*</label>
                                    <input type="password" name="password" value="<?php if (set_value('password') != '') {echo set_value('password');} else {echo $user_data['user_password'];}?>" placeholder="password" />
                                </div>
                                <div class="error">
                                    <?php echo form_error('password') ?> </div>
                            </div>
                            <?php }?>
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label id="zip_postal">Zip Code*</label>
                                    <input type="text" name="user_zip" id="zip" value="<?php if (set_value('user_zip') != '') {echo set_value('user_zip');} else {echo $user_data['user_zip'];}?>" placeholder="Zip Code" />
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_zip')?> </div>
                            </div>
                            <!-- <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Phone*</label>
                                    <input type="text" name="user_phone" value="<?php if (set_value('user_phone') != '') {echo set_value('user_phone');} else {echo htmlentities($user_data['user_phone']);}?>" placeholder="Phone" />
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_phone') ?> </div>
                            </div> -->
                            
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Flagged Status*</label>
                                    <select name="user_flagged_status">
                                        <option value="">Select</option>
                                        <option value="0" <?php if(set_select('user_flagged_status', '0') !=''){echo set_select('user_flagged_status', '0');} elseif($user_data['user_flagged_status']=='0'){ echo "selected"; } ?> >Unflagged</option>
                                        <option value="1" <?php if(set_select('user_flagged_status', '1') !=''){echo set_select('user_flagged_status', '1');} elseif($user_data['user_flagged_status']=='1'){ echo "selected"; } ?> >Flagged</option>
                                    </select>
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_flagged_status'); ?>
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Are you a small business owner?</label>
                                    <select name="user_business_owner">
                                        <option value="">Select</option>
                                        <option value="0" <?php if (set_select( 'user_business_owner', '0') !='' ) {echo set_select( 'user_business_owner', '0');} else if($user_data['user_business_owner']=='0' ) { echo "selected"; }?>>No</option>
                                        <option value="1" <?php if (set_select( 'user_business_owner', '1') !='' ) {echo set_select( 'user_business_owner', '1');} else if($user_data['user_business_owner']=='1' ) { echo "selected"; }?>>Yes</option>
                                    </select>
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_business_owner'); ?>
                                </div>
                            </div>
                            <?php if($user_data['user_type'] == 'Seller'){?>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Is Featured</label>
                                    <select name="user_is_featured" id="user_is_featured">
                                        <option value="0" <?php if (set_select('user_is_featured', '0') != '') {echo set_select('user_is_featured', '0');} elseif ($user_data['user_is_featured'] == '0') {echo "selected";}?>>No</option>
                                        <option value="1" <?php if (set_select('user_is_featured', '1') != '') {echo set_select('user_is_featured', '1');} elseif ($user_data['user_is_featured'] == '1') {echo "selected";}?>>Yes</option>                                        
                                    </select>
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_is_featured'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Status*</label>
                                    <select name="user_status" id="user_status">
                                        <option value="">Select</option>
                                        <option value="0" <?php if (set_select( 'user_status', '0') !='' ) {echo set_select( 'user_status', '0');} elseif ($user_data[ 'user_status']=='0' ) {echo "selected";}?>>Inactive</option>
                                        <option value="1" <?php if (set_select( 'user_status', '1') !='' ) {echo set_select( 'user_status', '1');} elseif ($user_data[ 'user_status']=='1' ) {echo "selected";}?>>Active</option>
                                        <option value="3" <?php if (set_select( 'user_status', '3') !='' ) {echo set_select( 'user_status', '3');} elseif ($user_data[ 'user_status']=='3' ) {echo "selected";}?>>Delete Account</option>
                                    </select>
                                </div>
                                <div class="error">
                                    <?php echo form_error('user_status'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Box along with label section ends here -->
                <!-- Box wihout label section begins here -->
                <div class="mu-fld-sub">
                    <input type="submit" value="Submit" />
                    <a href="<?php echo base_url() ?>admin/user">cancel</a> </div>
                <!-- Box wihout label section ends here -->
                <?php echo form_close(); ?> </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#user_country').change(function() {
            if ($('#user_country').val() === 'US') {
                $('#us-states').show("slow");
                $('#other_state').val('');
                $('#other-states').hide("slow");
            } else {
                $('#us-states').hide("slow");
                $('#other-states').show("slow");
            }
        });
    });
    $(document).ready(function() {
        $('#user_interests').change(function() {
            if ($('#user_interests').val() == 'Other') {
                $('#otherinterest').show("slow");
            } else {
                $('#otherinterest').hide("slow");
            }

        });
    });
    $(document).ready(function() {
        $('#user_interests_second').change(function() {
            if ($('#user_interests_second').val() == 'Other') {
                $('#otherinterestsecond').show("slow");
            } else {
                $('#otherinterestsecond').hide("slow");
            }

        });
    });
    $(document).ready(function() {
        $('#user_status').change(function() {
            if ($('#user_status').val() == '3') {
                alert("Are you sure you want to delete selected item(s)?")
            }

        });
        $('#user_interests_second').change();
        $('#user_interests').change();
    });
    </script>
</body>

</html>