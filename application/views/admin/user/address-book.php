<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>
            Address Book
        </title>
        <link rel="stylesheet" href="<?= base_url()?>css/new.css" type="text/css" media="all"/>
        <?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/jquery1.12.4.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/data_table.min.js"></script>
        <style>
        #display_tr {
            display: table-row !important;
        }
        </style>
        <script>
            $(function() {
                $('.message').append('<span class="close" title="Dismiss"></span>');
                //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
                $('.message .close').hover(
                    function() { $(this).addClass('hover'); },
                    function() { $(this).removeClass('hover'); }
                );
        
                $('.message .close').click(function() {
                    $(this).parent().fadeOut('slow', function() { $(this).remove(); });
                });
            });
            </script>
    </head>
    <body>
        <div class="container">
            <!-- Dashboard Left Side Begins Here -->
            <div class="left_wrp">
                <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
            </div>
            <!-- Dashboard Left Side Ends Here -->
            <div class="right-rp">
                <!-- Top Green Bar Section Begins Here -->
                <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
                <div class="mu-contnt-hdng">
                    <h1>
                        Manage Users
                    </h1>
                </div>
                <div class="n-crums">
                    <ul>
                        <li> <a href="<?=base_url()?>admin/user/">Manage Users</a> </li>
                        <li><div class="crms-sep">&gt;</div></li>
                        <li> <a href="javascript:void(0);" >Address Book</a></li>
                    </ul>
                </div>
                <!-- Top Green Bar Section Ends Here -->
                <div class="col-continer">
                    <div class="col-3 cat-box">
                        <div class="contntTop-row">
                            <div class="actvity-hd">
                                <div class="act-tab b-ad-tb">
                                    <table cellspacing="0" border="0">
                                        <colgroup/>
                                        <col width="30%"/>
                                        <col width="40%"/>
                                        <col width="30%"/>
                                    </colgroup>
                                    <tr>
                                        <td class="no-bdr">
                                            <div class="cat-tbl-tp-lf">
                                                <h1>
                                                    Address Book
                                                </h1>
                                                <span class="n-of-cat">
                                                    <?=count($pageList)?>
                                                </span>
                                            </div>
                                        </td>
                                        <td id="" class="pr-msg-bx no-bdr">
                                        </td>
                                        <td align="right" class="no-bdr">
                                            <a href="<?=base_url()?>/admin/user/add_new_address/<?=$user_id;?>" class="pro-addNew">Add an Address</a>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <?php    if($this->session->flashdata('msg') != ""){ 
                                    echo '<div id="message" class="message info">
                                        <p>'.$this->session->flashdata('msg').'</p>
                                        </div>';
                                }
                        ?>
                        <div class="b-prd-tbl align-top">
                            <table border="0" cellspacing="0" id='mytable' class='display'>
                                <colgroup/>
                                <col width="15%"/>
                                <col width="15%"/>
                                <col width="30%"/>
                                <col width="30%"/>
                                <col width="10%"/>
                            </colgroup>
                            <thead>
                                <tr>
                                    <th align="left" style="padding-left:13px;">
                                        Nickname
                                    </th>
                                    <th align="left" style="padding-left:13px;">
                                        Phone
                                    </th>
                                    <th align="left" style="padding-left:13px;">
                                        Address
                                    </th>
                                    <th align="left" style="padding-left:13px;">
                                        Address Type
                                    </th>
                                    <!-- <th align="center">Status</th> -->
                                    <th align="center">
                                        Actions
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    if(count($pageList)>0){ 
                                        foreach($pageList as $page) { $count=0; ?>
                                <tr>
                                    <td><?= $page->nick_name; ?></td>
                                    <td><?php if(strlen($page->phone)>0){echo $page->phone;}else{ echo "N/A";} ?></td>
                                    <?php $address=''; 
                                        if(strlen($page->street)>0){
                                            $address .= $page->street.', ';
                                        }
                                        if(strlen($page->city)>0){
                                            $address .= $page->city.', ';
                                        }
                                        if(strlen($page->state)>0){
                                            $address .= $page->state.', ';
                                        }
                                        else if(strlen($page->state_other)>0){
                                            $address .= $page->state_other.', ';
                                        }
                                        if(strlen($page->zip)>0){
                                            $address .= $page->zip;
                                        }
                                    ?>
                                    <td><?=$address;?></td>
                                    <td>
                                        <?php 
                                            if($page->default_billing_check=='Y' && $page->default_shipping_check=='Y') {
                                                echo "Default Billing & shipping Address";
                                            } else if($page->default_billing_check=='Y'){
                                                echo "Default Billing Address";
                                            } else if($page->default_shipping_check=='Y') {
                                                echo "Default Shipping Address";
                                            } else {
                                                echo "Additional Address";
                                            }
                                        ?>
                                    </td>
                                    <td align="center">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <colgroup/>
                                            <col width="25%"/>
                                            <col width="25%"/>
                                            <col width="25%"/>
                                            <col width="25%"/>
                                        </colgroup>
                                        <tr id="display_tr">
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td align="center">
                                                <a title="Edit" href="<?=base_url()?>admin/user/edit_address/<?=$page->add_user_id?>/<?=$page->add_id?>" class="tik-cross-btns p-edt-btn-n">
                                                </a>
                                            </td>
                                            <td align="center">
                                                <a title="Delete" href="<?=base_url()?>admin/user/delete_address/<?=$page->add_id?>/<?=$page->add_user_id?>" onclick="return confirm('Are you sure you want to delete selected item(s)?');" class="tik-cross-btns p-del-btn-n">
                                                </a>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <?php } 
                            }else{
                                            echo "<tr><td colspan='5' align='center'><strong>No Address found.</strong></td></tr>";
                                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script>
$(document).ready(function() {
    $('#mytable').DataTable({
        "paging": true,
        "ordering": true,
        "info": false,
        "order": [
            [0, "asc"]
        ],
        "columnDefs": [
            { orderable: false, targets: -1 }
        ],
        "oLanguage": { "sZeroRecords": "No records found." },
    });
});
</script>
</body>
</html>
