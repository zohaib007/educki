<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?= $title ?></title>
        <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
        <?php include(ADMIN_INCLUDE_PATH . "includes/js.php"); ?>
    </head>
    <body>
        <div class="container">
            <!-- Dashboard Left Side Begins Here -->
            <div class="left_wrp">
                <?php include(ADMIN_INCLUDE_PATH . "includes/dash-left.php"); ?>
            </div>
            <!-- Dashboard Left Side Ends Here -->
            <div class="right-rp">
                <!-- Top Green Bar Section Begins Here -->
                <?php
                include(ADMIN_INCLUDE_PATH . "includes/top_green_bar.php");
                ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="col-continer">
                    <div class="col-3 cat-box">
                        <!-- Bread crumbs starts here -->
                        <div class="n-crums">
                            <ul>
                                <li>
                                    <a href="<?php echo base_url() ?>admin/communication/appliedusers">Career Forms</a>
                                </li>
                                <li>
                                    <div class="crms-sep">&gt;</div>
                                </li>
                                <li>

                                    <a href="javascript:void(0);">Applied User Details</a>
                                </li>
                            </ul>
                        </div>
                        <!-- Bread crumbs ends here -->
                        <div class="contntTop-row"> <!-- col-box1 --> 
                            <!-- Top Navigation Starts Here -->
                            <?php include(ADMIN_INCLUDE_PATH . "includes/commnuication.php"); ?>
                            <!-- Top Navigation Ends Here -->
                            <!-- 01-5-1 Page Starst Here -->
                            <div style="display:block">
                                <?php
                                foreach ($results as $rstRow) {
                                    ?>
                                    <div class="add-frm-container">
                                        <form method="post" name="myForm">
                                            <div class="add-frm-wrp">
                                                <label>Career Title:</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="fname" readonly="readonly" value="<?php echo $rstRow['career_title']; ?>"/>  
                                                </div>
                                            </div>
                                            <div class="add-frm-wrp">
                                                <label>First Name:</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="fname" readonly="readonly" value="<?php echo $rstRow['career_user_first_name']; ?>"/>  
                                                </div>
                                            </div>
                                            <div class="add-frm-wrp">
                                                <label>Last Name:</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="lname" readonly="readonly" value="<?php echo $rstRow['career_user_last_name']; ?>"/>  
                                                </div>
                                            </div>
                                            <div class="add-frm-wrp">
                                                <label>Email:</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="email" readonly="readonly" value="<?php echo $rstRow['career_user_email']; ?>"/>  
                                                </div>
                                            </div>
                                            <div class="add-frm-wrp">
                                                <label>Date Applied:</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="created_date" readonly="readonly" value="<?php echo date('m-d-Y', strtotime($rstRow['career_user_applied_date'])); ?>">  
                                                </div>
                                            </div>
                                            <div class="add-frm-wrp">
                                                <label>LinkedIn Profile:</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="status" readonly="readonly" value="<?php echo $rstRow['career_user_linkedin_profile']; ?>"/>  
                                                </div>
                                            </div>
                                            <div class="add-frm-wrp">
                                                <label>Website:</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="phone" readonly="readonly" value="<?php echo $rstRow['career_user_website']; ?>"/>  
                                                </div>
                                            </div>
                                            <div class="add-frm-wrp">
                                                <label>How did you hear about this Job:</label>
                                                <div class="frm-s-sel">
                                                    <input type="text" name="status" readonly="readonly" value="<?php echo $rstRow['career_user_hear_from']; ?>"/>  
                                                </div>
                                            </div>
                                            <div class="add-frm-wrp">
                                                <label>Resume/CV:</label>
                                                <?php $resume_file_ico = check_extension($rstRow['career_user_resume']);?>
                                                <div class="fileUp img-er">
                                                    <p class="text-box my-form"><a <?php if($resume_file_ico!="file_thumbnail.jpg"){ echo "target='_blank'";}?> title="click to view" href="<?php if($rstRow['career_user_resume']!=''){ echo base_url('resources/applied_user_career_images/resume/'.$rstRow['career_user_resume']); }else{ echo "javascript:void(0)";}?>"><img src="<?= base_url('resources/applied_user_career_images/'.$resume_file_ico) ?>" width="30px" height="30px" /></a></p>
                                                </div>
                                            </div>
                                            <div class="add-frm-wrp">
                                                <label>Cover Letter:</label>
                                                <?php $cover_file_ico = check_extension($rstRow['career_user_cover']);?>
                                                <div class="fileUp img-er">
                                                    <p class="text-box my-form"><a <?php if($cover_file_ico!="file_thumbnail.jpg"){ echo "target='_blank'";}?> title="click to view" href="<?php if($rstRow['career_user_cover']!=''){ echo base_url('resources/applied_user_career_images/cover/'.$rstRow['career_user_cover']); }else{ echo "javascript:void(0)";}?>"><img src="<?= base_url('resources/applied_user_career_images/'.$cover_file_ico) ?>" width="30px" height="30px" /></a></p>
                                                </div>
                                            </div>
                                        </form>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>