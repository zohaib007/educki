<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Careers</title>
        <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
        <?php include(ADMIN_INCLUDE_PATH . "includes/js.php"); ?>
    </head>
    <body>
        <div class="container">
            <!-- Dashboard Left Side Begins Here -->
            <div class="left_wrp">
                <?php include(ADMIN_INCLUDE_PATH . "includes/dash-left.php"); ?>
            </div>
            <!-- Dashboard Left Side Ends Here -->
            <div class="right-rp">
                <!-- Top Green Bar Section Begins Here -->
                <?php
                include(ADMIN_INCLUDE_PATH . "includes/top_green_bar.php");
                ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="col-continer">
                    <div class="col-3 cat-box">
                        <div class="contntTop-row">
                            <!-- Top Nav Starts Here -->
                            <?php include(ADMIN_INCLUDE_PATH . "includes/commnuication.php"); ?>
                            <!-- Top Nav Ends Here -->
                            <div class="actvity-hd">
                                <div class="act-tab b-ad-tb">
                                    <table cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="30%" />
                                            <col width="40%" />
                                            <col width="30%" />
                                        </colgroup>
                                        <tr>
                                            <td class="no-bdr">
                                                <div class="cat-tbl-tp-lf">
                                                    <h1>
                                                        <?= $title ?>
                                                    </h1>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="b-prd-tbl">
                                <table border="0" cellspacing="0" cellpadding="0" id='mytable' class='tablesorter'>
                                    <colgroup>
                                        <col width="15%" />
                                        <col width="20%" />
                                        <col width="40%" />
                                        <col width="25%" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <!--<th align="center" style="padding-left:13px;">Created Date</th>-->
                                            <th align="left">Date</th>
                                            <th align="left">Name</th>
                                            <th align="left">Email</th>
                                            <th align="left">Career</th>
                                            <th align="center">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($list as $page) {
                                            ?>
                                            <tr>
                                                <td align="left" >
                                                    <?= date('m-d-Y', strtotime($page['career_user_applied_date'])) ?>
                                                </td>
                                                <td align="center" style="padding-left:13px;">
                                                    <?php echo $page['career_user_first_name'] .' '. $page['career_user_last_name']; ?>
                                                </td>
                                                <td align="left">
                                                    <?= $page['career_user_email'] ?>
                                                </td>
                                                <td align="left">
                                                    <?= $page['career_title'] ?>
                                                </td>
                                                <td align="center"><table cellpadding="0" cellspacing="0" border="0">
                                                        <colgroup>
                                                            <col width="24%" />
                                                            <col width="26%" />
                                                            <col width="26%" />
                                                            <col width="24%" />
                                                        </colgroup>
                                                        <tr>
                                                            <td align="center">&nbsp;</td>
                                                            <td align="center"><a href="<?= base_url() ?>admin/communication/appliedusers_details/<?= $page['career_user_id'] ?>" class="tik-cross-btns dtlbtn-n" title="View Details"></a></td>
                                                            <td align="center">
                                                            <a title="Delete" href="<?= base_url() ?>admin/communication/appliedusers_delete/<?= $page['career_user_id'] ?>" onclick="return confirm('Are you sure you want to delete the selected item(s)? ')"  class="tik-cross-btns prodDelete p-del-btn-n"><!--<img src="<?= base_url() ?>images/b-del-icon.png " alt="" />--></a>
                                                            </td>
                                                            <td align="center">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/jquery1.12.4.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/data_table.min.js"></script>
        <script>
        $(function () {
            $('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');
            //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
            $('.message .close').hover(
                    function () {
                        $(this).addClass('hover');
                    },
                    function () {
                        $(this).removeClass('hover');
                    }
            );

            $('.message .close').click(function () {
                $(this).parent().fadeOut('slow', function () {
                    $(this).remove();
                });
            });
        });

        $(document).ready(function () {
            $('#mytable').DataTable({
                "paging": true,
                "ordering": true,
                "info": false,
                "aaSorting": [],
                "columnDefs": [
                    {orderable: false, targets: -1}
                ],
                "oLanguage": {"sZeroRecords": "No records found."},
            });
        });
        </script>
    </body>
</html>