<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Contact Details</title>
    <link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
    <?php include(ADMIN_INCLUDE_PATH."includes/js.php");?>
</head>

<body>
    <div class="container">
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
            <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        <div class="right-rp">
            <!-- Top Green Bar Section Begins Here -->
            <?php 
            $strTopGreenText1="Catalog / Add Sub Category";
            $strTopGreenText2="Catalog";
            include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
         ?>
            <!-- Top Green Bar Section Ends Here -->
            <div class="col-continer">
                <div class="col-3 cat-box">
                    <!-- Bread crumbs starts here -->
                    <div class="n-crums">
                        <ul>
                            <li>
                                <a href="<?php echo base_url() ?>admin/communication/contact">Contact Forms</a>
                            </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li>
                                <a href="#">Contact Details</a>
                            </li>
                        </ul>
                    </div>
                    <!-- Bread crumbs ends here -->
                    <div class="contntTop-row">
                        <!-- col-box1 -->
                        <!-- Top Navigation Starts Here -->
                        <?php  include(ADMIN_INCLUDE_PATH."includes/commnuication.php");?>
                        <!-- Top Navigation Ends Here -->
                        <!-- 01-5-1 Page Starst Here -->
                        <div style="display:block">
                            <?php
                        foreach($results->result() as $rstRow){     
                    ?>
                                <div class="add-frm-container">
                                    <form method="post" name="myForm">
                                        <div class="add-frm-wrp">
                                            <label>Date:</label>
                                            <div class="frm-s-sel">
                                                <input type="text" name="fname" readonly="readonly" value="<?php echo date('m-d-Y', strtotime($rstRow->date_created));  ?>">
                                            </div>
                                        </div>
                                        <div class="add-frm-wrp">
                                            <label>Name:</label>
                                            <div class="frm-s-sel">
                                                <input type="text" name="fname" readonly="readonly" value="<?php echo stripcslashes($rstRow->fname);?>">
                                            </div>
                                        </div>

                                        <div class="add-frm-wrp">
                                            <label>Email:</label>
                                            <div class="frm-s-sel">
                                                <input type="text" name="fname" readonly="readonly" value="<?php echo stripcslashes($rstRow->email);?>">
                                            </div>
                                        </div>
                                        
                                        <div class="add-frm-wrp">
                                            <label>Subject:</label>
                                            <div class="frm-s-sel">
                                                <input type="text" name="fname" readonly="readonly" value="<?php echo (!empty($rstRow->subject))?stripcslashes($rstRow->subject):'N/A'?>">
                                            </div>
                                        </div>
                                        <div class="add-frm-wrp">
                                            <label>Category:</label>
                                            <div class="frm-s-sel">
                                                <input type="text" name="fname" readonly="readonly" value="<?php echo (!empty($rstRow->cat_name))?stripcslashes($rstRow->cat_name):'N/A'?>">
                                            </div>
                                        </div>
                                        
                                        <div class="add-frm-wrp">
                                            <label>Message:</label>
                                            <div class="frm-s-txtarea">
                                                <textarea cols="45" rows="10" readonly="readonly"><?=stripcslashes($rstRow->comment);?></textarea>
                                            </div>
                                        </div>
                                        <div class="add-frm-wrp">
                                            <label>File Attachment:</label>
                                            <?php $resume_file_ico = check_extension($rstRow->filename);?>
                                            <div class="fileUp img-er">
                                                <p class="text-box my-form"><a <?php if($resume_file_ico!="file_thumbnail.jpg" ){ echo "target='_blank'";}?> title="click to view" href="<?php if($rstRow->filename!=''){ echo base_url('resources/contact_form_files/'.$rstRow->filename); }else{ echo "javascript:void(0)";}?>"><img src="<?= base_url('resources/applied_user_career_images/'.$resume_file_ico) ?>" width="30px" height="30px" /></a></p>
                                            </div>
                                        </div>
                                    </form>
                                    <?php } ?>
                                </div>
                        </div>
                    </div>

                    <div class="contntTop-row" style="margin-top: 10px;">
                        <div style="display:block">
                            <div class="add-frm-container">
                                <?php echo form_open(base_url('admin/communication/chnage_contact_status/'.$rstRow->c_id), array('id' => 'edit_user')); ?>
                                    <div class="add-frm-wrp" style="width: 60%;">
                                        <div class="mu-frmFlds_long">
                                            <label>Status*</label>
                                            <select name="status" id="status">
                                                <option value="Pending" <?php if(set_select('status','Pending')!='' ) {echo set_select( 'status', 'Pending');} else if ($rstRow->status=="Pending" ) {echo "selected";}?>>Pending</option>
                                                <option value="Review Further" <?php if (set_select( 'status', 'Review Further') !='' ) {echo set_select( 'status', 'Review Further');} else if ($rstRow->status=="Review Further" ) {echo "selected";}?>>Review Further</option>
                                                <option value="Resolved" <?php if (set_select( 'status', 'Resolved') !='' ) {echo set_select( 'status', 'Resolved');} else if ($rstRow->status=="Resolved" ) {echo "selected";}?>>Resolved</option>
                                            </select>
                                        </div>
                                        <div class="error">
                                            <?php echo form_error('status') ?>
                                        </div>
                                    </div>
                                    <div class="mu-fld-sub">
                                        <input type="submit" value="Submit" />
                                        <a href="<?php echo base_url() ?>admin/communication/contact">cancel</a> 
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>