<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Email Subscribers</title>
<link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php");?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/jquery1.12.4.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/data_table.min.js"></script>

<script>
$(function () {
	$('.message').append('<span class="close" title="Dismiss"></span>');
	//setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
	$('.message .close').hover(
		function() { $(this).addClass('hover'); },
		function() { $(this).removeClass('hover'); }
	);
		
	$('.message .close').click(function() {
		$(this).parent().fadeOut('slow', function() { $(this).remove(); });
	});
	
});
</script>
</head>

<body>
<div class="container"> 
  
  <!-- Dashboard Left Side Begins Here -->
  <div class="left_wrp">
    <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php");?>
  </div>
  <!-- Dashboard Left Side Ends Here -->
  
  <div class="right-rp"> 
    
    <!-- Top Green Bar Section Begins Here -->
    <?php 
			 
			include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
		 ?>
    <!-- Top Green Bar Section Ends Here -->
    <div id="showMessage" style="display:none;" class="message info">
      <p>Item deleted successfully.</p>
    </div>
      <?php
		if($this->session->flashdata('msg') != "") {
			echo '<div class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
		} 
	?>
    <div class="col-continer">
      <div class="col-3 cat-box"> 
        
        <!-- Crumbs Starts -->
        <div class="n-crums">
          <ul>
            <li> </li>
            <!--
                <li>
                    <div class="crms-sep">></div>
                </li>
                -->
          </ul>
        </div>
        <!-- Crumbs Ends -->
        
        <div class="contntTop-row"> 
          
          <!-- Top Navigation Starts Here -->
          <?php  include(ADMIN_INCLUDE_PATH."includes/commnuication.php");?>
          <!-- Top Navigation Ends Here -->
          <!--
          <div class="b-srch-bl-sec">
                <table border="0" width="100%" cellspacing="0" >
                    <colgroup>
                    <col width="10%" />
                    <col width="15%" />
                    <col width="20%" />
                    <col width="10%" />
                    <col width="43%" />
                    <col width="2%" />
                    </colgroup>
                    <tr>
                        <?php echo form_open('',array('name'=>'myForm'));?>
                            <td align="center"><span>Search by</span></td>
                            <td align="center" >
                                <select name="srchOp" class="catName">
                                    <option <?php if(@$_POST['srchOp'] == 2) { ?> selected="selected" <?php } ?> value="2">Email</option>
                                </select>
                             </td>
                            <td align="center"><input value="<?php if(isset($_POST['searchText'])) { echo $_POST['searchText']; } ?>" type="text" name="searchText" class="catName" /></td>
                            <td align="center"><input type="submit" value="" class="catSrch" /></td>
                            <?php echo form_close();?>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        
                    </tr>
                </table>
          </div>
          -->
          
          <div class="actvity-hd">
            <div class="act-tab b-ad-tb">
              <table cellspacing="0" border="0">
                <colgroup>
                    <col width="30%">
                    <col width="40%">
                    <col width="15%">
                    <col width="15%">
                </colgroup>
                <tbody>
                  <tr>
                    <td class="no-bdr">
                      <div class="cat-tbl-tp-lf">
                          <h1>Newsletter Subscribers</h1>
                      </div>
                    </td>
                    <td id="" class="pr-msg-bx no-bdr"></td>                                            
                    <td align="right" class="no-bdr"></td>
                    <td align="right" class="expo-cell">
                      <span>Export to:</span> 
                      <form method="post" name="MyFormExport">
                        <?php echo form_open('',array("name"=>"MyFormExport"));?>
                        <span>
                          &nbsp;&nbsp;&nbsp;
                          <a href="<?=base_url()?>admin/communication/exportsubscribercsv">CSV</a>
                        </span>
                      <?php echo form_close();?>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="b-prd-tbl"> 
           <!-- <form name="myForm" method="post" action="<?=base_url()?>admin/product/del/">-->
              <input type="hidden" name="delIds" id="delIds" />
              <table border="0" cellspacing="0" id='mytable' class='tablesorter' >
                <colgroup>
                  <col width="20%" />
                  <col width="60%" />
                  <col width="20%" />
                </colgroup>
                <thead>
                    <tr>
                      
                      <th align="left">Date</th>
                      <th align="left">Email</th>
                        <th align="center">Action</th>
                        
                      
                    </tr>
                </thead>
                <tbody>
                <?php
				if(!empty($sub_data)) 
				{
					for($n = 0; $n < count($sub_data); $n++)
					{
				?>
                <tr id="row<?=$sub_data[$n]['sub_id']?>">
                  
                  <td align="left"><?=date('m-d-Y',strtotime($sub_data[$n]['sub_created_date']))?></td>
                  <td align="left"><?=$sub_data[$n]['sub_email']?></td>
					<?php 
						$strImg2="tick-btn.png";
						$status2="InActive";
						$bStats2=1;
						if($sub_data[$n]['sub_is_active']==0)
						{
							$strImg2="cross-btn.png";
							$status2="Active";
							$bStats2=0;
						}
                    ?>
                  <td align="center" id="delete_<?=$sub_data[$n]['sub_id']?>">
                  <a title="Delete" onclick="return confirm('Are you sure you want to delete selected item(s)?')" href="<?php echo base_url(); ?>admin/communication/deltesub/<?=$sub_data[$n]['sub_id']?>" id="<?=$sub_data[$n]['sub_id']?>" class="tik-cross-btns  p-del-btn-n"> 
                  </a>
                    
                    </td>
                </tr>
                <?php
					}
				}
				else
				{
					echo "<tr><td colspan='7' align='center'><strong>No data found.</strong></td></tr>";
				}
				?>
                </tbody>
              </table>
            <!--</form>-->
           
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
    $('#mytable').DataTable({
        "paging":   true,
        "ordering": true,
        "info":     false,
        "aaSorting": [],
        "columnDefs": [
           { orderable: false, targets: -1 }
        ],
        "oLanguage": { "sZeroRecords": "No records found." },
    });
});
</script>
</body>
</html>