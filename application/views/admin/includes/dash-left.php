<!-- Left menu begins here -->
<!--<?php //include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>-->
<script type="text/javascript" >
    $(document).ready(function () {


        jQuery('.unactive').click(function () {
            var objthis = jQuery(this);
            jQuery('.unactive').each(function () {
                jQuery(this).removeClass('current');
                jQuery(this).addClass('unactive');
            });
            objthis.addClass('current');

        });
    });
</script>
<?php
$dashboard = $admin_accounts = $product = $channels = $user = $communication = $sub = "";
$pages = $orders = $promocode = $setting = $managecomunication = $blog = $marketplace = $career = $promotion = "";
$order = "";
$urlSegeent = $this->uri->segment(2);
$urlSegeent3 = $this->uri->segment(3);
if ($urlSegeent == "dashboard")
    $dashboard = " current";
elseif ($urlSegeent == "orders")
    $orders = " current";
elseif ($urlSegeent == "categories" || $urlSegeent == "Categories")
    $channels = " current";
elseif ($urlSegeent == "catalog")
    $channels = " current";
elseif ($urlSegeent == "product")
    $channels = " current";
elseif ($urlSegeent == "user")
    $user = " current";
elseif ($urlSegeent == "communication")
    $communication = " current";
elseif ($urlSegeent3 == "subscriber")
    $sub = "current";
elseif ($urlSegeent == "admin_accounts")
    $admin_accounts = "current";
elseif ($urlSegeent == "pages" || $urlSegeent == "tier_management")
    $pages = "current";
elseif ($urlSegeent == "promocode")
    $promocode = "current";
elseif ($urlSegeent == "setting")
    $setting = "current";
elseif ($urlSegeent == "blog")
    $blog = "current";
elseif ($urlSegeent == "career")
    $career = "current";
elseif ($urlSegeent3 == "management_listing" || $urlSegeent3 == "management_detail" || $urlSegeent3 == "cancel_subscription")
    $marketplace = "current";
elseif ($urlSegeent3 == "billing" || $urlSegeent3 == "billing_detail")
    $marketplace_billing = "current";
elseif ($urlSegeent == "order")
    $order = "current";
elseif ($this->uri->segment(1) == "buyer_orders")
    $user = "current";
elseif ($this->uri->segment(1) == "view_order")
    $user = "current";
?>

<div class="lf-menu">
    <ul>

        <li> <a href="<?= base_url('admin/dashboard') ?>" class="unactive <?= $dashboard ?>"> <span class="m-image01"></span> <span class="m-text">Dashboard</span> </a> </li>
        <li> <a href="<?= base_url('admin/pages') ?>" class="unactive <?= $pages ?>"> <span class="m-image12"></span> <span class="m-text">Content Management</span> </a> </li>
        <li> <a href="<?= base_url('admin/catalog') ?>" class="unactive <?= $channels ?>"> <span class="m-image02"></span> <span class="m-text">Manage Catalog</span> </a> </li>
        <li> <a href="<?= base_url('admin/user/statistics') ?>" class="unactive <?= $user ?>"> <span class="m-image10"></span> <span class="m-text">Manage Users</span> </a> </li>
        <li> <a href="<?= base_url('admin/order') ?>" class="unactive <?= $order ?>"> <span class="m-image18"></span> <span class="m-text">Manage Orders</span> </a> </li>
        <li> <a href="<?= base_url('admin/admin_accounts') ?>" class="unactive <?= $admin_accounts ?>"> <span class="m-image16"></span> <span class="m-text">Admin Account</span> </a> </li>

        <li> <a href="<?= base_url('admin/blog') ?>" class="unactive <?= $blog ?>"> <span class="m-image17"></span> <span class="m-text">News &amp; Advice</span> </a> </li>
        <li> <a href="<?= base_url('admin/career') ?>" class="unactive <?= $career ?>"> <span class="m-image03"></span> <span class="m-text">Career</span> </a> </li>
        
        <!--<li><a href="<?php echo base_url('admin/promotions'); ?>" class="unactive <?= $promotion ?>"><span class="m-image08"></span><span class="m-text">Promotions</span></a></li>-->
        <li> <a href="<?php echo base_url('admin/communication/emailtrigger_list'); ?>" class="unactive <?= $communication ?>"> <span class="m-image06"></span> <span class="m-text">Manage Communication</span> </a> </li>
        <!-- <li> <a href="<?= base_url('admin/product') ?>"  class="unactive <?= $product ?>"> <span class="m-image02b"></span> <span class="m-text">Product Management</span> </a> </li> -->
        <li> <a href="<?php echo base_url('admin/marketplace/management_listing'); ?>" class="unactive <?= $marketplace ?>"> <span class="m-image19"></span> <span class="m-text">Marketplace Management</span></a> </li>
        <li> <a href="<?php echo base_url('admin/marketplace/billing'); ?>" class="unactive <?= $marketplace_billing ?>"> <span class="m-image20"></span> <span class="m-text">Marketplace Billing</span></a> </li>
        <li> <a href="<?php echo base_url('admin/setting'); ?>" class="unactive <?= $setting ?>"> <span class="m-image04"></span> <span class="m-text">Settings</span> </a> </li>
    </ul>
</div>
<!-- Left menu ends here -->