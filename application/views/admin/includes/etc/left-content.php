<div class="col-1">
    <div class="col-box">
    
        <div class="actvity-hd">
            <div class="act-tab">
                <table cellspacing="0" border="0">
                    <colgroup>
                        <col width="45%" />
                        <col width="15%" />
                        <col width="15%" />
                    </colgroup>
                    <tr>
                        <td class="no-bdr">
                            <div class="activity-cl">
                                <h1>Activity overview</h1>
                            </div>
                        </td>
                        <td class="tab-pad">
                            <a href="#">
                                <img src="images/setng-icon.png" alt="" />
                            </a>
                        </td>
                        <td class="tab-pad">
                            <a href="#">
                                <img src="images/refrsh-icon.png" alt="" />
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        
        <div class="activity-reslt">
            <table border="0" cellspacing="0">
                <colgroup>
                    <col width="85%" />
                    <col width="15%" />
                </colgroup>
                <tr>
                    <td>
                        <h2>Online Visitors</h2>
                        <span>in the last 30 minutes </span>
                    </td>
                    <td align="center">
                        <h1>41</h1>
                    </td>
                </tr>
                 <tr>
                    <td>
                        <h2>Active Shopping Carts</h2>
                        <span>in the last 30 minutes </span>
                    </td>
                    <td align="center">
                        <h1>41</h1>
                    </td>
                </tr>
            </table>
        </div>
        
        <div class="pndng-bx">
            <h1 class="pndng-hd">Currently Pending</h1>
            <div class="pndng-tabl">
                <table border="0" cellspacing="0">
                    <colgroup>
                        <col width="80%" />
                        <col width="10%" />
                    </colgroup>
                    <tr>
                        <td class="p-tabl-sps">
                            <h3>Orders</h3>
                        </td>
                        <td align="center">
                            <h2>5</h2>
                        </td>
                    </tr>
                    <tr>
                        <td class="p-tabl-sps">
                            <h3>Return/Exchanges</h3>
                        </td>
                        <td align="center">
                            <h2>1</h2>
                        </td>
                    </tr>
                     <tr>
                        <td class="p-tabl-sps">
                            <h3>Abandoned Carts</h3>
                        </td>
                        <td align="center">
                            <h2>6</h2>
                        </td>
                    </tr>
                     <tr>
                        <td class="p-tabl-sps p-n-bdr">
                            <h3>Out of Stock Products</h3>
                        </td>
                        <td align="center" class="p-n-bdr">
                            <h2>6</h2>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        
        <div class="pndng-bx">
            <h1 class="pndng-hd">Currently Pending</h1>
            <div class="pndng-tabl">
                <table border="0" cellspacing="0">
                    <colgroup>
                        <col width="80%" />
                        <col width="10%" />
                    </colgroup>
                    <tr>
                        <td class="p-tabl-sps">
                            <h3>Orders</h3>
                        </td>
                        <td align="center">
                            <h2>5</h2>
                        </td>
                    </tr>
                    <tr>
                        <td class="p-tabl-sps">
                            <h3>Return/Exchanges</h3>
                        </td>
                        <td align="center">
                            <h2>1</h2>
                        </td>
                    </tr>
                     <tr>
                        <td class="p-tabl-sps">
                            <h3>Abandoned Carts</h3>
                        </td>
                        <td align="center">
                            <h2>6</h2>
                        </td>
                    </tr>
                     <tr>
                        <td class="p-tabl-sps p-n-bdr">
                            <h3>Out of Stock Products</h3>
                        </td>
                        <td align="center" class="p-n-bdr">
                            <h2>6</h2>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        
        <div class="pndng-bx">
            <h1 class="notify-hd">Notifications</h1>
            <div class="notify-tabl">
                <table border="0" cellspacing="0">
                    <colgroup>
                        <col width="50%" />
                        <col width="50%" />
                    </colgroup>
                    <tr>
                        <td align="center">
                            <a href="#.">
                                New Messages
                                <br />
                                <br />
                                <span>10</span>
                            </a>
                        </td>
                        <td align="center" class="nt-n-sps">
                            <a href="#.">
                                New Messages
                                <br />
                                <br />
                                <span>10</span>
                            </a>
                        </td>
                    </tr>
                    
                </table>
            </div>
        </div>
        
        <div class="pndng-bx">
            <h1 class="custNews-hd">
                Customers & Newsletters<br />(from 2014-05-07 to 2014-05-07)
            </h1>
            <div class="pndng-tabl">
                <table border="0" cellspacing="0">
                    <colgroup>
                        <col width="80%" />
                        <col width="10%" />
                    </colgroup>
                    <tr>
                        <td class="p-tabl-sps">
                            <h3>Orders</h3>
                        </td>
                        <td align="center">
                            <h2>5</h2>
                        </td>
                    </tr>
                    <tr>
                        <td class="p-tabl-sps">
                            <h3>Return/Exchanges</h3>
                        </td>
                        <td align="center">
                            <h2>1</h2>
                        </td>
                    </tr>
                     <tr>
                        <td class="p-tabl-sps">
                            <h3>Abandoned Carts</h3>
                        </td>
                        <td align="center">
                            <h2>6</h2>
                        </td>
                    </tr>
                     <tr>
                        <td class="p-tabl-sps p-n-bdr">
                            <h3>Out of Stock Products</h3>
                        </td>
                        <td align="center" class="p-n-bdr">
                            <h2>6</h2>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        
    </div>
</div>