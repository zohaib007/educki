<div class="col-1">
                <div class="add-p-menu">
                    <ul>
                        <li>
                            <a href="<?=base_url()?>admin/attributes/index/size"  class="<?php if($strType=="size") echo "current";?>">
                                <img src="<?=base_url()?>images/p-menu-tp.jpg" alt="" class="p-menu-tp" />
                                sizes
                                <img src="<?=base_url()?>images/p-memu-tp-h.jpg" alt="" class="p-menu-tp-h" />
                            </a>
                            <a href="<?=base_url()?>admin/attributes/index/color" class="<?php if($strType=="color") echo "current";?>">colors</a>
                            <a href="<?=base_url()?>admin/attributes/other/name-and-number" class="<?php if($strType=="name-and-number") echo "current";?>">Name&Number</a>
                            <a href="<?=base_url()?>admin/attributes/other/weight" class="<?php if($strType=="weight") echo "current";?>">Weight</a>
                            <!--<a href="<?=base_url()?>admin/attributes/other/team-prices-available" class="<?php if($strType=="team-prices-available") echo "current";?>">Team Prices Available</a>-->
                            <a href="<?=base_url()?>admin/attributes/other/minimum-required" class="<?php if($strType=="minimum-required") echo "current";?>">minimum required</a>
                            <a href="<?=base_url()?>admin/attributes/other/origin" class="<?php if($strType=="origin") echo "current";?>">Origin</a>
                            <a href="<?=base_url()?>admin/attributes/other/qty-per-package" class="<?php if($strType=="qty-per-package") echo "current";?>">Qty Per Package</a>
                            <a href="<?=base_url()?>admin/attributes/other/ship-out-in" class="<?php if($strType=="ship-out-in") echo "current";?>">Ship Out In</a>
                            <a href="<?=base_url()?>admin/attributes/other/export" class="<?php if($strType=="export") echo "current";?>">Export</a>
                            <!--<a href="<?=base_url()?>admin/attributes/other/qty-included" class="<?php if($strType=="qty-included") echo "current";?>">Qty Included</a>-->
                            <a href="<?=base_url()?>admin/attributes/other/quality" class="<?php if($strType=="quality") echo "current";?>">Quality</a>
                            <a href="<?=base_url()?>admin/attributes/other/brand" class="<?php if($strType=="brand") echo "current";?>">Brand</a>
                            <!--<a href="<?=base_url()?>admin/attributes/other/available" class="<?php if($strType=="available") echo "current";?>">Available</a>-->
                            <a href="<?=base_url()?>admin/attributes/other/components"   class="p-n-bdr <?php if($strType=="components") echo "current";?>">
                                <img src="<?=base_url()?>images/p-menu-btm.jpg" alt="" class="p-menu-btm"  />
                                Components
                                <img src="<?=base_url()?>images/p-menu-btm-h.jpg" alt="" class="p-menu-btm-h" />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>