<?php
	$emailmanager = "";
	$subscriber = "";
	$contact = "";
	$career = "";
	
	$urlSegeent= $this->uri->segment(3);
	$urlSegeentid= $this->uri->segment(4);
	if($urlSegeent=="emailtrigger_list")
	{
		$emailmanager=" active";
	}
	elseif($urlSegeent=="subscriber")
	{
		$subscriber="active";
	}
	elseif($urlSegeent == "appliedusers")
	{
		$career="active";
	}
	elseif($urlSegeent == "appliedusers_details")
	{
		$career="active";
	}
	elseif($urlSegeent == "contact" || ($urlSegeent = "contact_detail" && $urlSegeentid != ""))
	{
		$contact="active";
	}
?>
<div class="tnav-hd">
	<div class="tnav-list">
		<a href="<?=base_url()?>admin/communication/emailtrigger_list" class="<?=$emailmanager?>" title="Email Triggers">Email Triggers</a>
		<span>|</span>
		<a href="<?=base_url()?>admin/communication/contact" class="<?=$contact?>" title="Contact Forms">Contact Forms</a>
		<span>|</span>
		<a href="<?=base_url()?>admin/communication/subscriber" class="<?=$subscriber?>" title="Newsletter Subscribers">Newsletter Subscribers</a>
		<span>|</span>
		<a href="<?=base_url()?>admin/communication/appliedusers" class="<?=$career?>" title="Careers">Careers</a>
	</div>
</div>