<link rel="stylesheet" href="<?php echo base_url() ?>js/summernote/dist/summernote.css">
<link rel="stylesheet" href="<?php echo base_url() ?>js/summernote/examples/css/font-awesome.min.css" />  
<script type="text/javascript" src="<?php echo base_url() ?>js/summernote/dist/summernote.js"></script>

   <script type="text/javascript">
                $(function() {
                    //$.noConflict();
                    $('.summernote').summernote({
                        height: 200,
						focus: true,
						toolbar: [
								// [groupName, [list of button]]
								['style', ['style']],
								['font', ['bold', 'italic', 'underline', 'clear']],
								['fontsize', ['fontsize']],
								['fontname', ['fontname']],
								['color', ['color']],
								['para', ['ul', 'ol', 'paragraph']],
								['height', ['height']],
								['table', ['table']],
								['insert', ['link', 'picture', 'video']],
								['view', ['fullscreen', 'codeview']],
								['help', ['help']]
							  ]
                    });

                    $('form').on('submit', function (e) {
                        // alert($('.summernote').code());
                    });
                });
            </script>