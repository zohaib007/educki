<!--Pricing & Benefits starts  here-->
<section class="blog-sec">
	<div class="container">
		<div class="row">
        	<div class="auto-container">
        	<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="banfit-wrp">
                	
                    <div class="banfit-upr">
                    	<div class="banfit-upr-tit">
                        	<h2><?= $page_title; ?></h2>
                        </div>
                        
                        <div class="banfit-upr-txt"><?= stripcslashes($page_description);?></div>
                    </div>
                	<div class="banfit-frst-row">
                        	<div class="banfit-low-bx">
                            	<table>
                                	<tr>
                                    	<td>
                                        	<img src="<?=$image_1?>" alt="" title=""/>
                                        </td>
                                    </tr>
                                </table>
                            	
                                <?=stripcslashes($img_description_1);?>
                            
                            </div>
                            
                            <div class="banfit-low-bx">
                            	<table>
                                	<tr>
                                    	<td>
                                        	<img src="<?=$image_2?>" alt="" title="" />
                                        </td>
                                    </tr>
                                </table>
                            	
                                <?=stripcslashes($img_description_2);?>       
                            
                            </div>
                            
                            <div class="banfit-low-bx mrg-rgt-0">
                            	<table>
                                	<tr>
                                    	<td>
                                        	<img src="<?=$image_3?>" alt="" title="" />
                                        </td>
                                    </tr>
                                </table>
                            	
                                <?=stripcslashes($img_description_3);?>

                            </div>
                        
                        </div>
                        
                  	<div class="get-frnd">
                    	<p><?=stripcslashes($offer_line);?></p>
                    </div> 
                    
                    <div class="plan-sec">
                    	
                        <div class="plan-sec-inner">
                        	<div class="plan-free">
                            	<div class="plan-free-hed">
                                	<table>
                                    	<tr>
                                        	<td>
                                            	<h3><?=stripcslashes($tierdata[0]->tier_name);?></h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                
                                <div class="plan-free-feature">
                                	<h4><?= stripcslashes($tierdata[0]->tier_title); ?></h4>
                                </div>
                                
                                <div class="transc-fee-sec">
                                	
                                    <p>
                                        <?=$tierdata[0]->tier_fee;?>% Transaction Fee<br>
                                        <?php if($tierdata[0]->is_pickup==1) {?>
                                            Local Pick Up Option<br>
                                        <?php } if($tierdata[0]->is_fb_store==1) {?>
                                            Facebook Store<br>
                                        <?php } if($tierdata[0]->tier_months>0) {?>
                                            <?= $tierdata[0]->tier_months; ?> Months
                                        <?php }?>
                                    </p>
                                    
                                    <div class="plan-amount">
                                    	<?php if($tierdata[0]->tier_amount>0){?>
                                           <h5>$<?=$tierdata[0]->tier_amount;?>/month</h5>
                                        <?php }else {?>
                                            <h5>FREE</h5>
                                        <?php }?>
                                    </div>
                                    
                                    <div class="plan-lst">
                                    	<a href="javascript:void(0)">Sign up!</a>
                                    </div>
                                
                                </div>
                        
                        	</div>
                            
                            <div class="plan-free border-lft">
                            	<div class="plan-free-bacs">
                                	<table>
                                    	<tr>
                                        	<td>
                                            	<h3><?=stripcslashes($tierdata[1]->tier_name);?></h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                
                                <div class="plan-free-feature">
                                	<h4><?=stripcslashes($tierdata[1]->tier_name);?></h4>
                                </div>
                                
                                <div class="transc-fee-sec">
                                	<p>
                                        <?=$tierdata[1]->tier_fee;?>% Transaction Fee<br>
                                        <?php if($tierdata[1]->is_pickup==1) {?>
                                            Local Pick Up Option<br>
                                        <?php } if($tierdata[1]->is_fb_store==1) {?>
                                            Facebook Store<br>
                                        <?php } if($tierdata[1]->tier_months>0) {?>
                                            <?= $tierdata[1]->tier_months; ?> Months
                                        <?php }?>
                                    </p>
                                    
                                    <div class="plan-amount">
                                        <?php if($tierdata[1]->tier_amount>0){?>
                                    	   <h5>$<?=$tierdata[1]->tier_amount;?>/month</h5>
                                        <?php }else {?>
                                            <h5>FREE</h5>
                                        <?php }?>
                                    </div>
                                    
                                    <div class="plan-lst">
                                    	<a href="javascript:void(0)">Sign up!</a>
                                    </div>
                                
                                </div>
                        
                        	</div>
                            
                            <div class="plan-free border-lft">
                            	<div class="plan-free-pro">
                                	<table>
                                    	<tr>
                                        	<td>
                                            	<h3><?=stripcslashes($tierdata[2]->tier_name); ?></h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                
                                <div class="plan-free-feature pad-tp-0">
                                	<h4><?= stripcslashes($tierdata[2]->tier_title); ?></h4>
                                </div>
                                
                                <div class="transc-fee-sec">
                                	<p>
                                        <?=$tierdata[2]->tier_fee;?>% Transaction Fee<br>
                                        <?php if($tierdata[2]->is_pickup==1) {?>
                                            Local Pick Up Option<br>
                                        <?php } if($tierdata[2]->is_fb_store==1) {?>
                                            Facebook Store<br>
                                        <?php } if($tierdata[2]->tier_months>0) {?>
                                            <?= $tierdata[2]->tier_months; ?> Months
                                        <?php }?>
                                    </p>
                                    
                                    <div class="plan-amount">
                                        <?php if($tierdata[2]->tier_amount>0){?>
                                           <h5>$<?=$tierdata[2]->tier_amount;?>/month</h5>
                                        <?php }else {?>
                                            <h5>FREE</h5>
                                        <?php }?>
                                        <?php $saves = ((($base_value*$tierdata[2]->tier_months)-($tierdata[2]->tier_amount*$tierdata[2]->tier_months))/($base_value*$tierdata[2]->tier_months))*100 ?>
                                        <h6>Save <?= round($saves)?>%</h6>
                                    </div>
                                    
                                    <div class="plan-lst">
                                    	<a href="javascript:void(0)">Sign up!</a>
                                    </div>
                                
                                </div>
                        
                        	</div>
                            
                            <div class="plan-free border-lft">
                            	<div class="plan-free-delx">
                                	<table>
                                    	<tr>
                                        	<td>
                                            	<h3><?=stripcslashes($tierdata[3]->tier_name); ?></h3>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                
                                <div class="plan-free-feature">
                                	<h4><?= stripcslashes($tierdata[3]->tier_title); ?></h4>
                                </div>
                                
                                <div class="transc-fee-sec">
                                	<p>
                                    	<?=$tierdata[3]->tier_fee;?>% Transaction Fee<br>
                                        <?php if($tierdata[3]->is_pickup==1) {?>
                                            Local Pick Up Option<br>
                                        <?php } if($tierdata[3]->is_fb_store==1) {?>
                                            Facebook Store<br>
                                        <?php } if($tierdata[3]->tier_months>0) {?>
                                            <?= $tierdata[3]->tier_months; ?> Months
                                        <?php }?>
                                    </p>
                                    
                                    <div class="plan-amount">
                                    	<?php if($tierdata[3]->tier_amount>0){?>
                                           <h5>$<?=$tierdata[3]->tier_amount;?>/month</h5>
                                        <?php }else {?>
                                            <h5>FREE</h5>
                                        <?php }?>
                                        <?php $saves = ((($base_value*$tierdata[3]->tier_months)-($tierdata[3]->tier_amount*$tierdata[3]->tier_months))/($base_value*$tierdata[3]->tier_months))*100 ?>
                                        <h6>Save <?= round($saves)?>%</h6>
                                    </div>
                                    
                                    <div class="plan-lst">
                                    	<a href="javascript:void(0)">Sign up!</a>
                                    </div>
                                
                                </div>
                        
                        	</div>
                        </div>                            
                    </div>  

                    <div class="evry-plan">
                        <div><?= stripcslashes($benefits_description);?></div>
                    </div>   
                
                </div>
            </div>
            </div>
		</div>
    </div>
</section>


<!--Pricing & Benefits ends  here-->