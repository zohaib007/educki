<!--banner  starts here-->
<section class="carer-bg" style="background-image:url('<?=$banner?>') !important ;">
    <div class="frg-layr"></div>
    <!--Breadcrumbs starts here-->
    <div class="container">
        <div class="row">
            <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
                <div class="auto-container">
                    <div class="brd-whit">
                        <h2><?=stripcslashes($banner_title)?></h2>	
                    </div>
                </div>
            </div>
        </div>
    </div>    <!--Breadcrumbs ends  here-->
</section>
<!--banner  starts here-->
<!--main sec starts here-->
<section class="carrr-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="carer-wrp">
                    <div class="carr-tit">
                        <h3><?=stripcslashes($page_title)?></h3>
                        <p><?=stripcslashes($page_description)?></p>
                    </div>
                    <div class="carr-bx">
                        <img src="<?=$image_1?>" alt="" title="" class="img-responsive"/>
                        <h5><?=stripcslashes($image_1_title)?></h5>
                        <div class="carr-bx-lwr">
                            <p><?=stripcslashes($img_description_1)?></p>
                        </div>
                    </div>
                    <div class="carr-bx">
                        <img src="<?= $image_2?>" alt="" title="" class="img-responsive"/>
                        <h5><?=stripcslashes($image_2_title)?></h5>
                        <div class="carr-bx-lwr">
                            <p><?=stripcslashes($img_description_2)?></p>
                        </div>
                    </div>
                    <div class="carr-bx mrg-rgt-0">
                        <img src="<?= $image_3?>" alt="" title="" class="img-responsive"/>
                        <h5><?=stripcslashes($image_3_title)?></h5>
                        <div class="carr-bx-lwr">
                            <p><?=stripcslashes($img_description_3)?></p>
                        </div>
                    </div>
                    <div class="prk-sec">
                        <h5>The Perks</h5>
                        <div class="prk-rw">
                            <div class="prk-rw-bx">
                                <img src="<?=base_url()?>front_resources/images/global.png" alt="" title="" class="img-responsive center-block" />
                                <p>
                                    Sample Text Here Sample Text Here Sample
                                </p>
                            
                            </div>
                            <div class="prk-rw-bx">
                                <img src="<?=base_url()?>front_resources/images/global.png" alt="" title="" class="img-responsive center-block" />
                                <p>
                                    Sample Text Here Sample Text Here Sample
                                </p>
                            
                            </div>
                            <div class="prk-rw-bx">
                                <img src="<?=base_url()?>front_resources/images/global.png" alt="" title="" class="img-responsive center-block" />
                                <p>
                                    Sample Text Here Sample Text Here Sample
                                </p>
                            
                            </div>
                            <div class="prk-rw-bx mrg-rgt-0">
                                <img src="<?=base_url()?>front_resources/images/global.png" alt="" title="" class="img-responsive center-block" />
                                <p>
                                    Sample Text Here Sample Text Here Sample
                                </p>
                            
                            </div>
                        </div>
                        <div class="prk-rw">
                            <div class="prk-rw-bx">
                                <img src="<?=base_url()?>front_resources/images/global.png" alt="" title="" class="img-responsive center-block" />
                                <p>
                                    Sample Text Here Sample Text Here Sample
                                </p>
                            
                            </div>
                            <div class="prk-rw-bx">
                                <img src="<?=base_url()?>front_resources/images/global.png" alt="" title="" class="img-responsive center-block" />
                                <p>
                                    Sample Text Here Sample Text Here Sample
                                </p>
                            
                            </div>
                            <div class="prk-rw-bx">
                                <img src="<?=base_url()?>front_resources/images/global.png" alt="" title="" class="img-responsive center-block" />
                                <p>
                                    Sample Text Here Sample Text Here Sample
                                </p>
                            
                            </div>
                            <div class="prk-rw-bx mrg-rgt-0">
                                <img src="<?=base_url()?>front_resources/images/global.png" alt="" title="" class="img-responsive center-block" />
                                <p>
                                    Sample Text Here Sample Text Here Sample
                                </p>
                            
                            </div>
                        </div>
                    
                    </div>
                    <div class="crr-wrp-2">
                        <div class="opng-sec">
                            <h5>Current Openings</h5>
                        </div>
                        <div class="op-lnk">
                            <?php foreach ($alphabetical_list as $page) { ?>
                                <div class="op-lnk-rw">
                                    <a href="javascript:void(0)"><?=stripcslashes($page['career_title'])?></a>
                                    <p><?= $page['stat_name'] ?>, <?=$page['career_state'] ?></p>
                                </div>
<?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
