<!--Main banner Slider starts here-->
<div class="main-bannr">
    <ul class="bxslider">
        <li><img src="<?= base_url() ?>front_resources/images/banner-1.jpg"  alt="" />
            <div class="auto-container">
                <div class="wdth-50">
                    <p>
                        Destination for Clothes & Kidâ€™s Accessories    
                    </p>
                    <ul>
                        <li>
                            <a href="">Buy</a>
                        </li>
                        <li>
                            <span></span>
                        </li>
                        <li>
                            <a href="">Sell</a>
                        </li>
                        <li>
                            <span></span>
                        </li>
                        <li>
                            <a href="">Trade</a>
                        </li>
                        <li>
                            <span></span>
                        </li>
                        <li>
                            <a href="">Donate</a>
                        </li>
                    </ul>
                    <div class="bnr-sec">
                        <a href="#" class="bnr-fb"></a>
                        <a href="#" class="bnr-eml"></a>
                    </div>
                </div>
            </div>
        </li>
        <li><img src="<?= base_url() ?>front_resources/images/banner-1.jpg" alt="" />
            <div class="auto-container">
                <div class="wdth-50">
                    <p>
                        Destination for Clothes & Kidâ€™s Accessories    
                    </p>
                    <ul>
                        <li>
                            <a href="">Buy</a>
                        </li>
                        <li>
                            <span></span>
                        </li>
                        <li>
                            <a href="">Sell</a>
                        </li>
                        <li>
                            <span></span>
                        </li>
                        <li>
                            <a href="">Trade</a>
                        </li>
                        <li>
                            <span></span>
                        </li>
                        <li>
                            <a href="">Donate</a>
                        </li>
                    </ul>
                    <div class="bnr-sec">
                        <a href="#" class="bnr-fb"></a>
                        <a href="#" class="bnr-eml"></a>
                    </div>
                </div>
            </div>
        </li>
        <li><img src="<?= base_url() ?>front_resources/images/banner-1.jpg" alt="" /></li>
    </ul>
</div>    
<!--Main banner Slider ends here-->
<!--home catgry starts here-->
<section class="catgr-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="cgt-tit">
                    <h4>Shop By Categories</h4>    
                </div>
                <div class="slk-sec">
                    <div class="slider-for">
                        <div>
                            <img src="<?= base_url() ?>front_resources/images/catg-1.jpg" alt="" title="" class="img-responsive"/>
                            <a href="#" class="accr-bt">Accessories</a>
                        </div>
                        <div> 
                            <img src="<?= base_url() ?>front_resources/images/catg-2.jpg" class="img-responsive"/>
                            <a href="#" class="accr-bt">Accessories</a>
                        </div>
                        <div>
                            <img src="<?= base_url() ?>front_resources/images/catg-3.jpg" class="img-responsive"/>
                            <a href="#" class="accr-bt">Accessories</a>
                        </div>
                        <div>
                            <img src="<?= base_url() ?>front_resources/images/catg-4.jpg" class="img-responsive"/>
                            <a href="#" class="accr-bt">Accessories</a>
                        </div>
                        <div>
                            <img src="<?= base_url() ?>front_resources/images/catg-2.jpg" alt="" title="" class="img-responsive"/>
                            <a href="#" class="accr-bt">Accessories</a>
                        </div>
                        <div> 
                            <img src="<?= base_url() ?>front_resources/images/catg-1.jpg" class="img-responsive"/>
                            <a href="#" class="accr-bt">Accessories</a>
                        </div>
                        <div>
                            <img src="<?= base_url() ?>front_resources/images/catg-3.jpg" class="img-responsive"/>
                            <a href="#" class="accr-bt">Accessories</a>
                        </div>
                        <div>
                            <img src="<?= base_url() ?>front_resources/images/catg-4.jpg" class="img-responsive"/>
                            <a href="#" class="accr-bt">Accessories</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--home catgry ends here-->
<!--Featured Sellers starts here-->
<section class="hm-sel-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="feat-tit">
                    <h4>Featured Sellers</h4>
                    <span class="brd-lne">
                        <img src="<?= base_url() ?>front_resources/images/featred-bdr.png"/>
                    </span>
                </div>
                <div class="featrd-row">
                    <div class="featrd-bx">
                        <img src="<?= base_url() ?>front_resources/images/catg-8.jpg" alt="" title="" class="img-responsive"/>
                        <a href="">Sellers store name goes here</a>
                    </div>
                    <div class="featrd-bx">
                        <img src="<?= base_url() ?>front_resources/images/catg-5.jpg" alt="" title="" class="img-responsive"/>
                        <a href="">Sellers store name goes here</a>
                    </div>
                    <div class="featrd-bx">
                        <img src="<?= base_url() ?>front_resources/images/catg-6.jpg" alt="" title="" class="img-responsive"/>
                        <a href="">Sellers store name goes here</a>
                    </div>
                    <div class="featrd-bx mrg-rgt-0">
                        <img src="<?= base_url() ?>front_resources/images/catg-7.jpg" alt="" title="" class="img-responsive"/>
                        <a href="">Sellers store name goes here</a>
                    </div>
                </div>
                <div class="featrd-row">
                    <div class="featrd-bx">
                        <img src="<?= base_url() ?>front_resources/images/catg-8.jpg" alt="" title="" class="img-responsive"/>
                        <a href="">Sellers store name goes here</a>
                    </div>
                    <div class="featrd-bx">
                        <img src="<?= base_url() ?>front_resources/images/catg-5.jpg" alt="" title="" class="img-responsive"/>
                        <a href="">Sellers store name goes here</a>
                    </div>
                    <div class="featrd-bx">
                        <img src="<?= base_url() ?>front_resources/images/catg-6.jpg" alt="" title="" class="img-responsive"/>
                        <a href="">Sellers store name goes here</a>
                    </div>
                    <div class="featrd-bx mrg-rgt-0">
                        <img src="<?= base_url() ?>front_resources/images/catg-7.jpg" alt="" title="" class="img-responsive"/>
                        <a href="">Sellers store name goes here</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Featured Sellers ends here-->
<!--Map sec starts here-->
<section class="hm-mp-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="srch-itm-ar">
                        <div class="itm-ar-tit">
                            <h5>Search Items in your Area</h5>
                            <span class="brd-lne">
                                <img src="<?= base_url() ?>front_resources/images/featred-bdr.png"/>
                            </span>
                        </div>
                        <div class="itm-ar-txt">
                            <p>
                                Sample Text Here Sample Text Here Sample Text Here
                                Sample Text Here Sample Text Here Sample Text
                                Here Sample Text Here Sample Text Here Sample Text
                                Here Sample Text Here Sample Text Here Sample
                                Text Here
                            </p>
                            <p>
                                City, State, Zip
                            </p>
                            <input type="button" value="Search"/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <img src="<?= base_url() ?>front_resources/images/map.jpg" alt="" title="" style="width:100%; height:360px;"/>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Map sec ends here-->
<!--Getting starts here-->
<section class="hm-sel-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="feat-tit">
                    <h4>Getting Started is as Easy as 1-2-3.</h4>
                    <span class="brd-lne">
                        <img src="<?= base_url() ?>front_resources/images/featred-bdr.png"/>
                    </span>
                </div>
                <div class="grt-strt-sec">
                    <div class="stp-bx-1">
                        <div class="stp-1">
                            <table>
                                <tr>
                                    <td>
                                        <img src="<?= base_url() ?>front_resources/images/get-step-1.png" alt="" title=""/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="stp-txt">
                            <h5>Add Listing</h5>
                            <p>
                                Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.
                            </p>
                        </div>
                    </div>
                    <div class="stp-bx-2">
                        <div class="stp-1">
                            <table>
                                <tr>
                                    <td>
                                        <img src="<?= base_url() ?>front_resources/images/step-right-arrow.png" alt="" title="" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="stp-bx-1">
                        <div class="stp-1">
                            <table>
                                <tr>
                                    <td>
                                        <img src="<?= base_url() ?>front_resources/images/get-step-2.png" alt="" title="" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="stp-txt">
                            <h5>Send/Receive Inquiries</h5>
                            <p>
                                Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.
                            </p>
                        </div>
                    </div>
                    <div class="stp-bx-2">
                        <div class="stp-1">
                            <table>
                                <tr>
                                    <td>
                                        <img src="<?= base_url() ?>front_resources/images/step-right-arrow.png" alt="" title="" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="stp-bx-1">
                        <div class="stp-1">
                            <table>
                                <tr>
                                    <td>
                                        <img src="<?= base_url() ?>front_resources/images/get-step-3.png" alt="" title=""/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="stp-txt">
                            <h5>Make Extra Money</h5>
                            <p>
                                Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here Sample Text Here.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="join-btn">
                <a href="">Join Now</a>
            </div>
        </div>
    </div>
</section>
<!--Getting starts ends here-->
<!--Save and average sec starts here-->
<section class="sve-arg-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="sve-tit">
                    <h5>Save an Average of 40% Buying from Members Like You</h5>
                </div>
                <div class="hm-avrg-sec">
                    <div class="col-md-6 col-lg-6 col-sm-6 sol-xs-6">
                        <div class="only-for-lft">
                            <ul>
                                <li>
                                    <span><img src="<?= base_url() ?>front_resources/images/save-1.png" alt="" title=""/></span>
                                    <table>
                                        <tr>
                                            <td>
                                                Make extra cash for your kids items taking up space
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                                <li>
                                    <span><img src="<?= base_url() ?>front_resources/images/save-2.png" alt="" title=""/></span>
                                    <table>
                                        <tr>
                                            <td>
                                                List your kids items for free
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                                <li>
                                    <span><img src="<?= base_url() ?>front_resources/images/save-3.png" alt="" title=""/></span>
                                    <table>
                                        <tr>
                                            <td>
                                                Find small businesses locally and globally
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6 sol-xs-6">
                        <div class="only-for-rgt">
                            <ul>
                                <li>
                                    <span><img src="<?= base_url() ?>front_resources/images/save-4.png" alt="" title=""/></span>
                                    <table>
                                        <tr>
                                            <td>
                                                Contact members like you locally and globally
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                                <li>
                                    <span><img src="<?= base_url() ?>front_resources/images/save-5.png" alt="" title=""/></span>
                                    <table>
                                        <tr>
                                            <td>
                                                Get more money for your outgrown kidâ€™s items			
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                                <li>
                                    <span><img src="<?= base_url() ?>front_resources/images/save-6.png" alt="" title=""/></span>
                                    <table>
                                        <tr>
                                            <td>
                                                Get 24/7 Member Support
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Save and average sec ends here-->