<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo (isset($title) ? $title : '') ; ?></title>
<meta name="Description" content="<?php echo (isset($meta_description) ? $meta_description : '');?>" />
<meta name="Keywords" content="<?php echo (isset($meta_keyword) ? $meta_keyword : '');?>" />
<meta name="author" content="Dotlogics" />
<!-- Google Chrome Frame for IE -->
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<link rel="shortcut icon" href="<?php echo base_url(); ?>front_resources/images/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo base_url(); ?>front_resources/images/favicon.ico" type="image/x-icon">
<link rel="icon" type="image/png" href="<?php echo base_url(); ?>front_resources/images/favicon.png">
<!-- <meta http-equiv="X-UA-Compatible" content="IE=edge" /> -->
<!-- mobile meta (hooray!) -->
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" /> 
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no" />
<!--Bootstrap and custom css start-->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/main.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/web-fonts/ufonts.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/main-dev2.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/animate.min.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url()?>front_resources/css/bootstrap-dropdownhover.min.css" />
<script type="text/javascript" src="<?=base_url()?>front_resources/js/jquery-3.2.1.min.js"></script>

<!--Bootstrap and custom css end-->
</head>
<body>
	<?php $this->load->view("admin/preview/".$content_view); ?>
</body>

<script type="text/javascript" src="<?=base_url()?>front_resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>front_resources/js/bootstrap-dropdownhover.min.js"></script>
<!--Bootstrap and custom js start-->
</html>