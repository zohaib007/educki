<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item active">Blog</li>
                    </ol>
                </div>	
            </div>
        </div>
    </div>
</div>  <!--Breadcrumbs ends  here-->
<!--Blog main starts  here-->
<section class="blog-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="col-md-2 col-lg-2 col-sm-3 col-xs-12 pad-lft-0">
                    <div class="blog-aside">
                        <h3>News & Trends</h3>
                        <ul>
                            <li><a href="#">Most Popular</a></li>
                            <li><a href="#">Fertility</a></li>
                            <li><a href="#">Pregnancy</a></li>
                            <li><a href="#">Baby Names</a></li>
                            <li><a href="#">Baby</a></li>
                            <li><a href="#">Toddler</a></li>
                            <li><a href="#">Child</a></li>
                            <li><a href="#">Family</a></li>
                            <li><a href="#">Advice</a></li>
                            <li><a href="#">Food</a></li>
                        </ul>
                        <div class="discus-sec">
                            <a href="#">eDucki Discussions &amp; Forum </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 col-lg-10 col-sm-9 col-xs-12">
                    <div class="blog-main">
                        <?php if (isset($results)) { ?>
                            <?php foreach ($results as $page) { ?>
                                <div class = "blog-list-bx">
                                    <div class = "blog-list-inner pad-tp-0">
                                        <div class = "blog-lst-wrp">
                                            <div class = "blog-list-tit">
                                                <h2><?= $page->blog_name ?></h2>
                                            </div>
                                            <div class = "blog-list-date">
                                                <p>By <a href = "#"><?= $page->blog_author ?></a> on <?= date('F d, Y', strtotime($page->blog_created_date)) ?></p>
                                            </div>
                                        </div>
                                        <div class="blog-lst-img">
                                            <img src="<?= base_url() ?>resources/blog_image/<?= $page->blog_image ?>" alt="" title="" class="img-responsive"/> 
                                        </div>
                                        <div class = "blog-lst-wrp2">
                                            <div class = "blog-list-txt">
                                                <p><?= $page->blog_description ?></p>
                                            </div>
                                            <div class = "blog-lst-btn">
                                                <a href = "blog/blog_details/<?= $page->blog_id ?>">Read<span>&gt;</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                        <div class="pagnation-start">
                            <ul>
                                <!-- Show pagination links -->
                                <?php
                                if (isset($links)) {
                                    echo "<li>" . $links . "</li>";
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Blog main ends  here-->