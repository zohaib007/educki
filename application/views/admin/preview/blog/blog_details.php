<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?= base_url() ?>blog">Blog</a></li>
                        <li class="breadcrumb-item active">Details</li>
                    </ol>
                </div>	
            </div>
        </div>
    </div>
</div>      <!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="blog-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="col-md-2 col-lg-2 col-sm-3 col-xs-12 pad-lft-0">
                    <div class="blog-aside">
                        <h3>News & Trends</h3>
                        <ul>
                            <li><a href="#">Most Popular</a></li>
                            <li><a href="#">Fertility</a></li>
                            <li><a href="#">Pregnancy</a></li>
                            <li><a href="#">Baby Names</a></li>
                            <li><a href="#">Baby</a></li>
                            <li><a href="#">Toddler</a></li>
                            <li><a href="#">Child</a></li>
                            <li><a href="#">Family</a></li>
                            <li><a href="#">Advice</a></li>
                            <li><a href="#">Food</a></li>
                        </ul>
                        <div class="discus-sec">
                            <a href="#">eDucki Discussions &amp; Forum</a>
                        </div>
                    </div>
                </div>  
                <div class="col-md-10 col-lg-10 col-sm-9 col-xs-12">
                    <div class="blog-main">
                        <div class="blog-list-bx">
                            <div class="blog-list-inner pad-tp-0">
                                <div class="blog-lst-wrp">
                                    <div class="blog-list-tit">
                                        <h2><?= $blog_name ?></h2>
                                    </div>
                                    <div class="blog-list-date">
                                        <p>By <a href="#"><?= $blog_author ?></a> on <?= date('F d, Y', strtotime($blog_name)) ?></p>
                                    </div>
                                </div>
                                <div class="blog-lst-img">
                                    <img src="<?= base_url() ?>resources/blog_image/<?= $blog_image ?>" alt="" title="" class="img-responsive"/> 
                                </div>
                                <div class="blog-lst-wrp2">
                                    <div class="blog-detail-txt">
                                        <p><?= $blog_description ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="blog-detail-comment">
                            <div class="blw-commt">   
                                <div id="disqus_thread"></div>
                                <!--<img src="<?= base_url() ?>front_resources/images/comment-img.jpg" class="img-responsive"/>-->
                            </div>
                            <div class="share-sec">
                                <p>Share this job:</p>
                                <img src="<?= base_url() ?>front_resources/images/share.png" alt="" title="" />
                                <img src="<?= base_url() ?>front_resources/images/fb-like.jpg" alt="" title=""/>
                            </div>
                        </div>

                    </div>

                </div>  

            </div>
        </div>
    </div>
</section>
<script id="dsq-count-scr" src="//eduki.disqus.com/count.js" async></script>
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://eduki.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<!--Blog main ends  here-->
