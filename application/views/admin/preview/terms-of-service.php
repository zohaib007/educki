<!--Breadcrumbs starts here-->
<div class="container">
	<div class="row">
    	<div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
    		<div class="auto-container">	
                <div class="prvc-tit">
                	<h2><?=stripcslashes($pg_title);?></h2>
                </div>
                
                <div class="prvcy-txt"><?=stripcslashes($pg_description);?></div>
            </div>
    	</div>
    </div>

</div>
<!--Breadcrumbs ends  here-->