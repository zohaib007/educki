<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
                        <li class="breadcrumb-item">My Account </li>
                        <li class="breadcrumb-item active">Message Center</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <!--Left section starts here-->
                <?php $this->load->view('front/includes/left-navsec2'); ?>
                <!--Left section starts here-->
                <!-- Right Pannel Starts Here -->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">
                        <!-- Inner Section Starts -->
                        <div class="message-wrapper">
                            <!-- Heading Section Starts -->
                            <div class="my-account-right-head addbtmPad">
                                <div class="msg-hd-inrow">
                                    <h5>Message Center</h5>
                                </div>                                <!-- <p>Sample Text Here.</p> -->
                            </div>                            <!-- Heading Section Ends -->
                            <!-- Bottom Section Starts -->
                            <div class="msg-center-btm">
                                <!-- Top Message Rely Section Starts -->
                                <div class="msgreply-top">
                                    <div class="msgreply-box">
                                        <div class="msgreply-inner">
                                            <form name="msgReply" method="post">
                                                <div class="msgreplyFrm">
                                                    <div class="msgreply-row">
                                                        <label>Message</label>
                                                        <div class="msgreply-txtarea">
                                                            <textarea placeholder="Message"></textarea>
                                                            <div class="error" style="display:none;">
                                                                <p>This field is required.</p>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="msgreply-row">
                                                        <div class="msgreply-btnsec">
                                                            <div class="msgreply-btn">
                                                                <a href="#" title="Cancel">Cancel</a>
                                                            </div>
                                                            <div class="msgreply-btn">
                                                                <input type="submit" value="REPLY" title="Reply" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- Top Message Rely Section Ends -->

                                <!-- Bottom Message Section Starts -->
                                <div class="msgreply-btm">
                                    <div class="msgreply-tbl">
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <th valign="top" align="left" class="firstcol msgcol1">Date</th>
                                                <th valign="top" align="left" class="msgcol2">Sender</th>
                                                <th valign="top" align="left" class="msgcol3">Product ID</th>
                                                <th valign="top" align="left" class="msgcol4">Message</th>
                                            </tr>
                                            <?php
                                            if (count($conversation) > 0) {
                                                $count = 0;
                                                foreach ($conversation as $msg) {
                                                    $count++;
                                                    ?>
                                                    <tr <?php
                                                    if ($count % 2 == 0) {
                                                        echo 'class=even';
                                                    }
                                                    ?>>
                                                        <td valign="top" align="left" class="firstcol">
                                                            <?= date("m/d/Y", strtotime($msg->timestamp)); ?>
                                                        </td>
                                                        <td valign="top" align="left">
                                                            <?php
                                                            if ($msg->sender == $this->session->userdata('userid')) {
                                                                echo "Me";
                                                            } else {
                                                                echo $msg->sendername . ' ' . $msg->sender_lname;
                                                            }
                                                            ?>
                                                        </td>
                                                        <td valign="top" align="left">
                                                            10001
                                                        </td>
                                                        <td valign="top" align="left"><?= $msg->message; ?></td>
                                            </tr>
                                            <?php }}?>
                                        </table>
                                    </div>
                                </div>
                                <!-- Bottom Message Section Ends -->
                            </div>
                            <!-- Bottom Section Ends -->
                        </div>
                        <!-- Inner Section Ends -->
                    </div>
                </div>
                <!-- Right Pannel Starts Here -->
            </div>
        </div>
    </div>
</section>