<!--Breadcrumbs starts here-->
<div class="container">
    <div class="row">
        <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
            <div class="auto-container">
                <div class="brd-sec pad-9">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="<?=base_url();?>">Home</a></li>
                      <li class="breadcrumb-item">My Account</li>
                      <li class="breadcrumb-item active">Message Center</li>

                    </ol>
                </div>
            </div>
        </div>
    </div>

</div>
<!--Breadcrumbs ends  here-->

<!--Blog main starts  here-->
<section class="seller-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">

                <!--Left section starts here-->
                  <?php //include("includes/left-navsec2.php") ?>
                  <?php $this->load->view('front/includes/left-navsec2');?>
                <!--Left section starts here-->


                <!-- Right Pannel Starts Here -->
                <div class="col-sm-9 col-xs-12">
                    <div class="my-account-right">

                        <!-- Inner Section Starts -->
                        <div class="message-wrapper">

                            <!-- Heading Section Starts -->
                            <div class="my-account-right-head addbtmPad">
                                <div class="msg-hd-inrow">

                                    <h5>Message Center</h5>

                                    <div class="del-btnright">
                                        <div class="custombtn-large">
                                            <a href="#">DELETE MESSAGES</a>
                                        </div>
                                    </div>

                                </div>
                                <!-- <p>Sample Text Here.</p> -->
                            </div>
                            <!-- Heading Section Ends -->

                            <!-- Bottom Section Starts -->
                            <div class="msg-center-btm">

                                <!-- Top Grey Bar Starts -->
                                <div class="topGrey-bar">
                                    <div class="topGrey-inner">

                                        <!-- Search By Form Starts -->
                                        <div class="searchby-wrap">
                                            <form name="searchby" method="post">
                                                <div class="searchby-row">
                                                    <label>Search by:</label>
                                                    <div class="sselect-rp">
                                                        <select id="searchType">
                                                            <option value="0">Select</option>
                                                            <option value="1">First Name</option>
                                                            <option value="2">Product ID</option>
                                                        </select>
                                                    </div>
                                                    <div class="stextbox-rp">
                                                        <input type="text" id="utxtbox" placeholder="Sample Text" value=""/>
                                                    </div>
                                                    <div class="search-btn">
                                                        <div class="custombtn-large">
                                                            <input type="button" value="SEARCH" title="SEARCH" id="search"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- Search By Form Ends -->

                                    </div>
                                </div>
                                <!-- Top Grey Bar Ends -->

                                <!-- Message Inner Bottom Listing Starts -->
                                <div class="msg-secbtm-rp">

                                    <form name="messageListing" method="post">
                                    <!-- Table Section Starts -->
                                    <div class="msg-sectbl">
                                        <table cellpadding="0" cellspacing="0" border="0" id='my_table'>
                                            <tr>
                                                <th class="colOne">&nbsp;</th>
                                                <th class="colTwo">Date</th>
                                                <th class="colThree">Name</th>
                                                <th class="colFour">Product ID</th>
                                                <th class="colFive">Message</th>
                                                <th class="colSix">Actions</th>
                                            </tr>
                                            <?php if (count($Allconversation) > 0) {
                                                $class = '';
                                                foreach ($Allconversation as $chat) {
                                                    if ($chat->seller_unread > 0) {$class = 'class=unread';} else { $class = '';}
                                                    ?>
                                            <tr <?=$class;?>>
                                                <td><input type="checkbox" name="ulist1" value="ulist1" /></td>
                                                <td><?=date("m/d/Y", strtotime($chat->timestamp));?></td>
                                                <td><?php if ($chat->buyer == $this->session->userdata('userid')) {echo "Me";} else {echo $chat->buyername . ' ' . $chat->buyer_lname;}?></td>
                                                <td>10001</td>
                                                <td><?=$chat->latest_message;?></td>
                                                <td class="msgaction">
                                                    <div class="msg-actionrp">
                                                        <ul>
                                                            <li>
                                                                <div class="viewmsg-btn">
                                                                    <?php if ($chat->seller_unread > 0) {?>
                                                                    <div class="viewBubble"><?=$chat->seller_unread;?></div>
                                                                    <?php }?>
                                                                    <a href="<?=base_url()?>messages/<?=$chat->conversation_id?>" title="View Messages"></a>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="delmsg-btn">
                                                                    <a href="" title="Delete Message"></a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php }} else {?>
                                            <tr class="unread" align="center"><td colspan="6">No Messages found</td></tr>
                                            <?php }?>
                                            <tr>
                                                <td colspan="6">
                                                <!-- <td colspan="6" style="padding: 1px 0px;" -->
                                                    <div class="msb-lastfrow">

                                                        <!-- Left Section Starts 
                                                        <div class="showing-rp">
                                                            1 - 10 of 14
                                                        </div>
                                                        <! Left Section Ends -->

                                                        <!-- Right Section Starts -->
                                                        <div class="msg-bright">
                                                            <!-- Pagination Section Starts -->
                                                            <div class="pagination-rp">
                                                              <!-- <?php if (isset($paginglink)) {echo $paginglink;}?> -->
                                                               
                                                                <a href="#" class="preBtn"></a>
                                                                <div class="paginationInner">
                                                                    
                                                                    <a href="#" class="active">1</a>
                                                                    <a href="#">2</a>
                                                                    <a href="#">3</a>
                                                                    <a href="#">4</a>
                                                                    <a href="#">5</a>
                                                                    
                                                                </div>
                                                                <a href="#" class="nextBtn"></a>

                                                                <!-- <div class="paginationInner">
                                                                    <p href="#" class="preBtn"></p>
                                                                    <a href="#" class="active">1</a>
                                                                    <a href="#">2</a>
                                                                    <a href="#">3</a>
                                                                    <a href="#">4</a>
                                                                    <a href="#">5</a>
                                                                    <p href="#" class="nextBtn"></p>
                                                                </div> -->

                                                            </div>
                                                            <!-- Pagination Section Ends -->
                                                        </div>
                                                        <!-- Right Section Ends -->

                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!-- Table Section Ends -->

                                    </form>

                                </div>
                                <!-- Message Inner Bottom Listing Ends -->

                            </div>
                            <!-- Bottom Section Ends -->

                        </div>
                        <!-- Inner Section Ends -->


                    </div>
                </div>
                <!-- Right Pannel Starts Here -->


            </div>
        </div>
    </div>
</section>
<!--Blog main ends  here-->

<script>
$(document).ready(function(){
    $("#search").click(function(){
        if($('#searchType').val()==1 || $('#searchType').val()==2){
            var input, filter, table, tr, td, i;
            input = document.getElementById("utxtbox");
            filter = input.value.toUpperCase();
            table = document.getElementById("my_table");
            column=0;
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                if($('#searchType').val()==1){column=2;}else{column=3;}
                  td = tr[i].getElementsByTagName("td")[column];
                  if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                      tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    else{
        alert('Please select a filter from dropdown');
    }
    });
});
</script>