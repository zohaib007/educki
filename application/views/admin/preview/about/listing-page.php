<!--banner  starts here-->
<?php if (empty($banner)) {
    $path = base_url('resources/no_banner/no_banner.png');
} else {
    $path = $banner;
}
?>
<section class="whted-bg" style="background-image:url(<?=$path?>) !important;">
    <div class="frg-layr"></div>
    <!--Breadcrumbs starts here-->
    <div class="container">
        <div class="row">
            <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
                <div class="auto-container">
                    <div class="brd-whit">
                        <div class="brd-sec">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item">About</li>
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Guide to eDucki</a></li>
                                <li class="breadcrumb-item active"><?=stripcslashes($title);?></li>
                            </ol>
                        </div>
                        <h2><?=stripcslashes($banner_title);?></h2>	
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Breadcrumbs ends  here-->
</section>
<!--banner  starts here-->
<!--main sec starts here-->
<section class="weht-main">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 pad-lft-0">
                    <div class="side-nav">
                        <ul>
                            <li>
                                <a href="javascript:void(0);">What is eDucki</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="active">Guide To eDucki</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">Community Guidelines</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">eDucki Protect</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">FAQs</a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12 pad-rgt-0">
                    <div class="whted-rgt-sec">
                        <?php if ($id == '7') { ?>
                            <div class="abt-get-start">
                                <?php
                                if(!empty($id)) {  
                                foreach ($list as $page) { ?>
                                    <div class="abt-get-stp">
                                        <div class="hng">
                                            <img src="<?= base_url() ?>resources/page_image/sub_listing/<?= $page->lisintg_image ?>" alt="" title=""/>
                                        </div>
                                        <div class="abt-get-txt">
                                            <?= stripcslashes($page->listing_description);?>
                                        </div>

                                    </div>
                                <?php } }?>
                                <p><?= stripcslashes($description)?></p>
                            </div>
                        <?php } else { ?>
                            <div class="frst-sale">
                                <?php
                                if(!empty($id)) { 
                                foreach ($list as $page) { ?>
                                    <div class="frst-sale-bx">
                                        <div class="frst-sale-bx-lft">
                                            <img src="<?= base_url() ?>resources/page_image/sub_listing/<?= $page->lisintg_image ?>" alt="" title="" class="img-responsive"/>
                                        </div>
                                        <div class="frst-sale-bx-rgt">
                                            <?= stripcslashes($page->listing_description);?>
                                        </div>
                                    </div>
                                <?php } }?>
                                <p><?= stripcslashes($description)?></p>
                            </div>
                        <?php } ?>
                        <div class="sprt-sec">
                            <p>
                                Still need assistance? Check out our <a href="javascript:void(0);">Support Center</a>.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--main sec  ends here-->