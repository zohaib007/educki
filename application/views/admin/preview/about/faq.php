<?php
if (empty($banner)) {
    $path = base_url('resources/no_banner/no_banner.png');
} else {
    $path = $banner;
}
?>
<!--banner  starts here-->
<section class="whted-bg" style="background-image: url('<?= $path ?>'); !important">
    <div class="frg-layr"></div>
    <!--Breadcrumbs starts here-->
    <div class="container">
        <div class="row">
            <div class="col-lg-12-col-md-12 col-sm-12 col-xs-12">
                <div class="auto-container">
                    <div class="brd-whit">
                        <h2><?= stripcslashes($pg_title); ?></h2>
                    </div>      
                </div>
            </div>
        </div>
    </div>        <!--Breadcrumbs ends  here-->
</section>  <!--banner  starts here-->
<!--main sec starts here-->
<section class="weht-main">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 pad-lft-0">
                    <div class="side-nav">
                        <ul>
                            <li><a href="javascript:void(0);">What is eDucki</a></li>
                            <li><a href="javascript:void(0);">Guide To eDucki</a></li>
                            <li><a href="javascript:void(0);">Community Guidelines</a></li>
                            <li><a href="javascript:void(0);">eDucki Protect</a></li>
                            <li><a href="javascript:void(0);" class="active"><?= stripcslashes($pg_title); ?></a></li>
                            <li><a href="javascript:void(0);">Contact</a></li>
                        </ul>
                    </div>
                    <div class="faq-emal">
                        <a href="javascript:void(0);">Email us</a>
                    </div>
                </div>
                <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12 pad-rgt-0">
                    <div class="whted-rgt-sec">
                        <div class="faq-sec">
                            <div class="faq-srch">
                                <input type="text" name="search" placeholder="Ask or enter a search term here. "/>
                                <input type="submit" value="" />
                            </div>
                            <div class="faq-accord">
                                <div class="panel-group" id="accordion">
                                    <?php
                                    $row_count = 1;
                                    foreach ($list as $page) {
                                        ?>
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle only-lft" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $row_count ?>">
                                                        <span class="arrow-rgt"></span>
                                                        <?= stripcslashes($page['sub_sub_pg_title']);?>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse<?php echo $row_count; ?>" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p><?= stripcslashes($page['sub_sub_pg_description']); ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $row_count++;
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  <!--main sec  ends here-->
<script>
    $('.collapse').on('shown.bs.collapse', function(){
        $(this).parent().find(".arrow-rgt").removeClass("arrow-rgt").addClass("arrow-down");
        
        }).on('hidden.bs.collapse', function(){
        $(this).parent().find(".arrow-down").removeClass("arrow-down").addClass("arrow-rgt");
    });

</script>

 