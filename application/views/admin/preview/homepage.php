<!--Main banner Slider starts here-->
<div class="main-bannr">
    <ul class="bxslider">
        <?php
        if(count($home_banner) > 0){
            foreach($home_banner as $banner){
        ?>
        <li><img src="<?= base_url() ?>resources/home_image/<?=$banner->banner_image?>"  alt="" />
            <div class="auto-container">
                <div class="wdth-50">
                    <p>
                        Destination for Clothes & Kidâ€™s Accessories    
                    </p>
                    <ul>
                        <li>
                            <a href="">Buy</a>
                        </li>
                        <li>
                            <span></span>
                        </li>
                        <li>
                            <a href="">Sell</a>
                        </li>
                        <li>
                            <span></span>
                        </li>
                        <li>
                            <a href="">Trade</a>
                        </li>
                        <li>
                            <span></span>
                        </li>
                        <li>
                            <a href="">Donate</a>
                        </li>
                    </ul>
                    <div class="bnr-sec">
                        <a href="<?php // echo $this->facebook->login_url(); ?>" class="bnr-fb"></a>
                        <a href="#" class="bnr-eml"></a>
                    </div>
                </div>
            </div>
        </li>
        <?php
            } 
        } else {
        ?>
        <li><img src="<?= base_url() ?>resources/home_image/no-banner.jpg"  alt="" />
            <div class="auto-container">
                <div class="wdth-50">
                    <p>
                        Destination for Clothes & Kidâ€™s Accessories    
                    </p>
                    <ul>
                        <li>
                            <a href="">Buy</a>
                        </li>
                        <li>
                            <span></span>
                        </li>
                        <li>
                            <a href="">Sell</a>
                        </li>
                        <li>
                            <span></span>
                        </li>
                        <li>
                            <a href="">Trade</a>
                        </li>
                        <li>
                            <span></span>
                        </li>
                        <li>
                            <a href="">Donate</a>
                        </li>
                    </ul>
                    <div class="bnr-sec">
                        <a href="#" class="bnr-fb"></a>
                        <a href="#" class="bnr-eml"></a>
                    </div>
                </div>
            </div>
        </li>
        <?php
        }
        ?>
    </ul>
</div>    
<!--Main banner Slider ends here-->
<?php if(count($home_category ) > 0 ){?>
<!--home catgry starts here-->
<section class="catgr-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="cgt-tit">
                    <h4><?=$home_data->category_title?></h4>    
                </div>
                <div class="slk-sec">
                    <div class="slider-for">
                        <?php foreach($home_category as $category) { ?>
                        <div>
                            <img src="<?= base_url() ?>resources/cat_image/<?=$category->cat_image_thumb?>" alt="" title="" class="img-responsive"/>
                            <a href="#" class="accr-bt"><?=stripcslashes($category->cat_name)?></a>
                        </div>
                        <?php } ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--home catgry ends here-->
<?php } ?>
<!--Featured Sellers starts here-->
<section class="hm-sel-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="feat-tit">
                    <h4><?=stripcslashes($home_data->feaSeller)?></h4>
                    <span class="brd-lne">
                        <img src="<?= base_url() ?>front_resources/images/featred-bdr.png"/>
                    </span>
                </div>
                <div class="featrd-row">
                    <div class="featrd-bx">
                        <img src="<?= base_url() ?>front_resources/images/catg-8.jpg" alt="" title="" class="img-responsive"/>
                        <a href="">Sellers store name goes here</a>
                    </div>
                    <div class="featrd-bx">
                        <img src="<?= base_url() ?>front_resources/images/catg-5.jpg" alt="" title="" class="img-responsive"/>
                        <a href="">Sellers store name goes here</a>
                    </div>
                    <div class="featrd-bx">
                        <img src="<?= base_url() ?>front_resources/images/catg-6.jpg" alt="" title="" class="img-responsive"/>
                        <a href="">Sellers store name goes here</a>
                    </div>
                    <div class="featrd-bx mrg-rgt-0">
                        <img src="<?= base_url() ?>front_resources/images/catg-7.jpg" alt="" title="" class="img-responsive"/>
                        <a href="">Sellers store name goes here</a>
                    </div>
                </div>
                <div class="featrd-row">
                    <div class="featrd-bx">
                        <img src="<?= base_url() ?>front_resources/images/catg-8.jpg" alt="" title="" class="img-responsive"/>
                        <a href="">Sellers store name goes here</a>
                    </div>
                    <div class="featrd-bx">
                        <img src="<?= base_url() ?>front_resources/images/catg-5.jpg" alt="" title="" class="img-responsive"/>
                        <a href="">Sellers store name goes here</a>
                    </div>
                    <div class="featrd-bx">
                        <img src="<?= base_url() ?>front_resources/images/catg-6.jpg" alt="" title="" class="img-responsive"/>
                        <a href="">Sellers store name goes here</a>
                    </div>
                    <div class="featrd-bx mrg-rgt-0">
                        <img src="<?= base_url() ?>front_resources/images/catg-7.jpg" alt="" title="" class="img-responsive"/>
                        <a href="">Sellers store name goes here</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Featured Sellers ends here-->
<!--Map sec starts here-->
<section class="hm-mp-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="srch-itm-ar">
                        <div class="itm-ar-tit">
                            <h5><?=stripcslashes($home_data->searchtitle)?></h5>
                            <span class="brd-lne">
                                <img src="<?= base_url() ?>front_resources/images/featred-bdr.png"/>
                            </span>
                        </div>
                        <div class="itm-ar-txt">
                            <?=stripcslashes($home_data->searchtext)?>
                            <input type="button" value="<?=$home_data->searchButton?>"/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <img src="<?= base_url() ?>front_resources/images/map.jpg" alt="" title="" style="width:100%; height:360px;"/>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Map sec ends here-->
<!--Getting starts here-->
<section class="hm-sel-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="feat-tit">
                    <h4><?=stripcslashes($home_data->gettingStartTitle)?></h4>
                    <span class="brd-lne">
                        <img src="<?= base_url() ?>front_resources/images/featred-bdr.png"/>
                    </span>
                </div>
                <div class="grt-strt-sec">
                    <div class="stp-bx-1">
                        <div class="stp-1">
                            <table>
                                <tr>
                                    <td>
                                        <img src="<?= base_url('resources/home_image/'.$home_data->start_image1) ?>" alt="" title=""/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="stp-txt">
                            <?=stripcslashes($home_data->startTitle1)?>
                            
                        </div>
                    </div>
                    <div class="stp-bx-2">
                        <div class="stp-1">
                            <table>
                                <tr>
                                    <td>
                                        <img src="<?= base_url() ?>front_resources/images/step-right-arrow.png" alt="" title="" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="stp-bx-1">
                        <div class="stp-1">
                            <table>
                                <tr>
                                    <td>
                                        <img src="<?= base_url('resources/home_image/'.$home_data->start_image2) ?>" alt="" title=""/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="stp-txt">
                            <?=stripcslashes($home_data->startTitle2)?>
                        </div>
                    </div>
                    <div class="stp-bx-2">
                        <div class="stp-1">
                            <table>
                                <tr>
                                    <td>
                                        <img src="<?= base_url() ?>front_resources/images/step-right-arrow.png" alt="" title="" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="stp-bx-1">
                        <div class="stp-1">
                            <table>
                                <tr>
                                    <td>
                                        <img src="<?= base_url('resources/home_image/'.$home_data->start_image3) ?>" alt="" title=""/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="stp-txt">
                            <?=stripcslashes($home_data->startTitle3)?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="join-btn">
                <a href="javascript:void(0);">Join Now</a>
            </div>
        </div>
    </div>
</section>
<!--Getting starts ends here-->
<?php if(count($home_member) > 0) { ?>
<!--Save and average sec starts here-->
<section class="sve-arg-sec">
    <div class="container">
        <div class="row">
            <div class="auto-container">
                <div class="sve-tit">
                    <h5><?=stripcslashes($home_data->membertitle)?></h5>
                </div>
                <div class="hm-avrg-sec">
                    <?php 
                    $rowStart = 0;
                    $div = 1;
                    foreach($home_member as $mem ) { 
                    ?>
                    <?php
                    if( $rowStart== 0 ){?> 
                    <div class="hm-avrg-sec-row">
                    <?php
                        $rowStart = 1;
                    } ?>
                        <div class="<?=(($div%2) != 0 )?'only-for-lft':'only-for-rgt'?>">
                            <ul>
                                <li>
                                    <span><img src="<?=base_url('resources/member_image/thumb/thumb_'.$mem->mem_image)?>" alt="" title=""/></span>
                                    <table>
                                        <tr>
                                            <td>
                                                <?=stripcslashes($mem->mem_description)?>
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                            </ul>
                        </div>
                    <?php
                    if($div%2 == 0){
                    ?>
                    </div>
                    <?php
                    $rowStart = 0;
                    } 
                    ?>
                    <?php } ?>
                    <?php if($rowStart == 1){ echo "</div"; }?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Save and average sec ends here-->
<?php } ?>