<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Add Promo Code</title>
        <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
        <?php include(ADMIN_INCLUDE_PATH . "includes/js.php"); ?>

        <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
        <script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>

        <script src="<?php echo base_url(); ?>js/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/core.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>js/zebra_datepicker.js"></script>
        <link rel="stylesheet" href="<?= base_url() ?>css/defualt.css" type="text/css" media="all" />
        <script>
            function hide_percentage(id)
            {
                if (id == 0)
                {
                    //alert('doller')
                    $("#percentage").val('');
                    $("#doller").show();
                    $("#percentage").hide();
                }
                if (id == 1)
                {
                    $("#doller").hide();
                    $("#doller").val('');
                    $("#percentage").show();
                }
            }


            $(document).ready(function () {
                /*  $(".cus-sc-outr").click(function(){
                 $(".cus-scrl-wrp").toggle();
                 });
                 */
                var id = $('#promo_discount_type').val();
                //alert(id);
                if (id == 0)
                {
                    //alert('doller')
                    $("#percentage").val('');
                    $("#doller").show();
                    $("#percentage").hide();
                }
                if (id == 1)
                {
                    $("#doller").hide();
                    $("#doller").val('');
                    $("#percentage").show();
                }
                var promo_type = $('#promo_type_id').val();
                if (promo_type == 1)
                {
                    $(".cus-hdn-contaier").show();
                }
            });

            $(document).ready(function () {
                $('#date_check').on('click', function () {
                    //alert('asdf');
                    var disableField = $(this).attr('dis');
                    if (disableField == 0) {
                        $(this).attr('dis', 1);
                        $("#expiredate").addClass("important");
                        $('#expiredate').prop('disabled', true);
                    } else {
                        $(this).attr('dis', 0);
                        $('#expiredate').prop('disabled', false);
                    }
                });
            });

            $(document).ready(function () {
                $('#limit').on('click', function () {
                    //alert('asdf');
                    var disableField = $(this).attr('dis');
                    if (disableField == 0) {
                        $(this).attr('dis', 1);
                        $('#limit_dis').prop('disabled', true);
                    } else {
                        $(this).attr('dis', 0);
                        $('#limit_dis').prop('disabled', false);
                    }
                });
            });

            $(document).ready(function ()
            {
                $('#rand_string').on('click', function () {
                    $.ajax({
                        type: "post",
                        url: base_url + "admin/promocode/randstring",
                        data: {<?php echo $this->security->get_csrf_token_name(); ?>: '<?php echo $this->security->get_csrf_hash(); ?>'},
                        success: function (res)
                        {
                            $("#rand").attr('value', res);
                        }
                    });
                });
            });
        </script>

    </head>
    <body>
        <div class="container_p"> 
            <!-- Dashboard Left Side Begins Here -->
            <div class="left_wrp">
                <?php include(ADMIN_INCLUDE_PATH . "includes/dash-left.php"); ?>
            </div>
            <!-- Dashboard Left Side Ends Here -->
            <div class="right-rp"> 
                <!-- Top Green Bar Section Begins Here -->
                <?php
                include(ADMIN_INCLUDE_PATH . "includes/top_green_bar.php");
                ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="mu-contnt-wrp">
                    <div class="mu-contnt-hdng">
                        <h1>Add Promo code</h1>
                    </div>
                    <!-- Bread crumbs starts here -->
                    <div class="n-crums">
                        <ul>
                            <li> <a href="<?php echo base_url() ?>admin/promocode">Promo Code Management</a> </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li> <a href="#"> Add Promo Code</a> </li>
                        </ul>
                    </div>
                    <!-- Bread crumbs ends here --> 
                    <?php echo form_open(base_url() . 'admin/promocode/add'); ?> 

                    <!-- Box along with label section begins here -->

                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Date</h2>
                            <p> </p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Date</label>
                                        <input id="system_date" type="text" value="<?php echo date("m-d-y"); ?>" name="system_date" readonly="readonly" class="spaceset-small2" />
                                       </div>
                                </div>
                                <div class="error">
                                    <?php echo form_error('promo_code') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Generate Promo Code</h2>
                            <p> </p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long gcode">
                                        <label>Promo code*</label>
                                        <input id="rand" type="text" value="<?php echo set_value('promo_code'); ?>" name="promo_code" placeholder="Promo Code" class="spaceset-small2" />
                                        <div class="gcode-txt"> <a href="javascript:void(0)" id="rand_string">Generate Code</a> </div>
                                    </div>
                                </div>
                                <div class="error">
                                    <?php echo form_error('promo_code') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Discount Type</h2>
                            <p> Select promo code discount type.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <label class="nlbl-set">Select Type*</label>
                                    <div class="cus-ad-fld-sel">
                                        <select  onchange="hide_percentage(this.value)" name="promo_discount_type" id="promo_discount_type">
                                            <option value="0" <?php echo set_select('promo_discount_type', '0'); ?>>$USD</option>
                                            <option value="1" <?php echo set_select('promo_discount_type', '1'); ?>>%Discount</option>
                                        </select>
                                    </div>
                                    <div id="doller" class="cus-ad-tk-dis">
                                        <label></label>
                                        <div class="cus-tk-dis-fld "> <span class="dol-line">$</span>
                                            <input type="text" style="width:66%; important" value="<?php echo set_value('promo_price_usd'); ?>" name="promo_price_usd" />
                                        </div>
                                    </div>
                                    <div style="display: none;" id="percentage" class="cus-ad-tk-dis">
                                        <label></label>
                                        <div class="cus-tk-dis-fld">
                                            <input type="text" value="<?php echo set_value('promo_price_percentage'); ?>" name="promo_price_percentage" />
                                            <span>%</span> 
                                        </div>
                                    </div>
                                </div>
                                <div class="error"> <?php echo form_error('promo_price_usd') ?> <?php echo form_error('promo_price_percentage') ?> </div>
                            </div>
                        </div>
                    </div>
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Promo Code Validity</h2>
                            <p> Set promo code validity range. </p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <label id="show">Promo code start date*</label>
                                        <input type="text" value="<?php echo set_value('promo_start_date'); ?>"  name="promo_start_date"  id="datepicker-example7-start" placeholder="From" />
                                        <div class="error set">
                                            <?php
                                            echo form_error('promo_start_date');
                                            echo $this->session->flashdata('msg');
                                            ?>
                                        </div>
                                    </div>
                                    <div class="mu-frmFlds mu-flt-rght">
                                        <label id="show">Promo code end date*</label>
                                        <input type="text" value="<?php echo set_value('promo_end_date'); ?>"   name="promo_end_date"  id="datepicker-example7-end" placeholder="To" />
                                        <div class="error set"><?php echo form_error('promo_end_date') ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Box along with label section ends here -->
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Promo Code Status</h2>
                            <p>Manage your promo code status.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-frmFlds_long ">
                                    <label>Status*</label>
                                    <select   name="promo_status">
                                        <option value="">Select</option>
                                        <option value="1" <?php echo set_select('promo_status', '1'); ?>>Active</option>
                                        <option value="0" <?php echo set_select('promo_status', '0'); ?>>Expired</option>
                                    </select>
                                </div>
                                <div class="error set"><?php echo form_error('promo_status') ?></div>
                                <!-- New Div Starts -->
                                <div class="mu-flds-wrp"> </div>
                            </div>
                        </div>
                    </div>
                    <!-- Box wihout label section begins here -->

                    <div class="mu-fld-sub">
                        <input type="submit" value="Submit" />
                        <a href="<?php echo base_url(); ?>admin/promocode">cancel</a>
                    </div>
                    <!-- Box wihout label section ends here --> 
                    <?php echo form_close(); ?> </div>
            </div>
        </div>
        <script>
            $('#datepicker-example7-start').Zebra_DatePicker({
                direction: true,
                pair: $('#datepicker-example7-end')
            });
            $('#datepicker-example7-end').Zebra_DatePicker({
                direction: true
            });
        </script> 
        <script>
            /*$(document).ready(function(e) {
             $("input[name='promo_start_date']").change(function(){
             alert('sdfs');
             $("input[name='promo_end_date']").val("");
             });
             });*/
        </script>
        <style type="text/css" media="all">
            #scrollWrapper {
                position:absolute; z-index:1;
                top:32px; bottom:0; left:0;
                width:100%;
                overflow:auto;
            }
            #scroller {
                position:absolute; z-index:1;
                /*	-webkit-touch-callout:none;*/
                -webkit-tap-highlight-color:rgba(0,0,0,0);
                width:100%;
                padding:0;
            }

            #scroller ul {
                list-style:none;
                padding:0;
                margin:0;
                width:100%;
                text-align:left;
            }

            #scroller li {
                width:96%;
                height:auto;
                margin:0 2%;
                border-bottom:1px solid #f1f1f1;
            }
            #scroller li a{
                width:96%;
                padding:0 2%;
                display:block;
                font-family:Arial, Helvetica, sans-serif;
                font-size:13px;
                color:#767676;
                text-decoration:none;
                line-height:30px;

                background-color:#FFF;
            }
            #scroller li a:hover{
                background-color:#52ca63;
                color:#FFF;
            }
        </style>
        <!-- Scroll bar css and js ends here -->
    </body>
</html>