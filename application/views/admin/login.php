<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
<link rel="stylesheet" href="<?=base_url()?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php");?>
</head>

<body>
<!--<script src="<?=base_url()?>js/jquery.min.js" type="text/javascript"></script>
 <link rel="stylesheet" href="<?=base_url()?>js/bootstrap.min.css" />
  <script type="text/javascript" src="<?=base_url()?>js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="<?=base_url()?>js/font-awesome.min.css" />  
  <link rel="stylesheet" href="<?=base_url()?>js/summernote.css">
  <script type="text/javascript" src="<?=base_url()?>js/summernote.js"></script>
	<textarea name="d" class="summernote"></textarea> <script type="text/javascript">
        $(function() {
          $('.summernote').summernote({
            height: 200
          });
    
          $('form').on('submit', function (e) {
           // alert($('.summernote').code());
          });
        });
    </script>-->
<div class="container">
  <div class="login-bg">
    <div class="login-content">
      <div class="login-hd">
      <?php $site_data = $this->common_model->getCombox('tbl_site_setting')->row_array();?>
        <h1><?=$site_data['name']?> ADMIN</h1>
      </div>
      <div class="log-frm-wrp"> 
        <!--<form method="post" name="myForm">--> 
        <?php echo form_open('',array('name'=>'myForm', 'method'=>'post'));?>
        <div class="log-fld">
          <label>Username</label>
          <div class="email-bg">
            <input type="text" name="log" required="required" autocomplete="off"/>
          </div>
          <span class="errorMessage"><?php echo form_error('log')?></span> </div>
        <div class="log-fld">
          <label>Password</label>
          <div class="pass-bg">
            <input type="password" name="pwd" required="required"  autocomplete="off"/>
          </div>
          <div class="errorMessage"><?php echo form_error('pwd')?></div>
        </div>
        <div class="log-fld"> 
          <!-- <div class="log-chk">
                            <input type="checkbox" value="login" />
                            <span>Keep me logged in.</span>
                        </div>--> 
          <a href="<?php echo base_url()?>admin/login/forget_password" class="passLos">Lost password</a> </div>
          
          
        
        <div class="log-sub">
          <input type="submit" value="Sign in" />
        </div>
        <?php echo form_close();?> 
        
        <div class="errorMessage"><?php echo $this->session->flashdata('msg');?></div>
        </div>
        
    </div>
  </div>
</div>
</body>
</html>