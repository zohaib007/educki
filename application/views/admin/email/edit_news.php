<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit Trigger</title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>

<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
<script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/core.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>js/zebra_datepicker.js"></script>
<link rel="stylesheet" href="<?= base_url() ?>css/defualt.css" type="text/css" media="all" />

<!-- tinymce configration start here.-->
<script>
var base_url='<?php echo base_url();?>';
var ser='<?php echo $_SERVER['DOCUMENT_ROOT'];?>/resources/tiny_upload';
</script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.js"></script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>tiny/common.js"></script>
<!-- tinymce configration end here.-->

</head>
<body>
<div class="container_p"> 
  
  <!-- Dashboard Left Side Begins Here -->
  <div class="left_wrp">
    <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
  </div>
  <!-- Dashboard Left Side Ends Here -->
  
  <div class="right-rp"> 
    
    <!-- Top Green Bar Section Begins Here -->
    <?php
    include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
    ?>
    <!-- Top Green Bar Section Ends Here -->
    <div class="mu-contnt-wrp">

      <div class="mu-contnt-hdng">
        <h1>Edit Trigger</h1>
      </div>
      <div class="n-crums">
        <ul>
          <li> <a href="<?php echo base_url() ?>admin/communication/emailtrigger_list"> Email Triggers </a> </li>
          <li>
            <div class="crms-sep">&gt;</div>
          </li>
          <li> <a href="javascript:void(0);">Edit Trigger</a> </li>
        </ul>
      </div>
      <!-- Bread Crumbs Ends -->
      <?php echo form_open_multipart('', array("name"=>'edit email')); ?> 
      
      <!-- Box along with label section begins here -->
      
      <div class="mu-contnt-outer">
        <div class="mu-contnt-lfLbl ad-size">
          <h2>Trigger Details</h2>
          <p>Manage your trigger details.</p>
        </div>
        <div class="mu-contntBx-wrp">
          <div class="contntTop-row">
          
              <div class="mu-flds-wrp">
                  <div class="mu-frmFlds">
                    <label>Date : <span class="last-updateby"><?php echo date('m-d-Y', strtotime($resultsArray->date_created));  ?></span></label>
                  </div>
                </div>
                
            <!--<div class="mu-flds-wrp">
              <div class="mu-frmFlds">
                <label>Email Identifier</label>
                <input readonly="readonly" type="text" name="" value="<?php echo $resultsArray->e_email_slug ?>" />
               
              </div>
            </div>
            -->
            <div class="mu-flds-wrp">
              <div class="mu-frmFlds">
                <label>Title*</label>
                <input type="text" name="nl_title" value="<?php echo $resultsArray->e_email_title; ?>" />
              </div>
            </div>
            <div class="error"> <?php echo form_error('nl_title')?> </div>
            
            
            <div class="mu-flds-wrp">
              <div class="mu-frmFlds">
                <label>Subject*</label>
                <input type="text" value="<?php echo $resultsArray->e_email_subject; ?>"  name="nl_subject" />
              </div>
            </div>
            <div class="error"> <?php echo form_error('nl_subject')?> </div>
            
            <div class="mu-flds-wrp">
              <div class="mu-frmFlds">
                <label>To</label>
                <input type="text" value="<?=set_value('nl_to')?>"  name="nl_to" />
              </div>
            </div>
            <div class="error"> <?php echo form_error('nl_to')?> </div>
            
            <div class="mu-flds-wrp">
                <label class="lone-set">Description*</label>
                <textarea class="myeditor" name="nl_text" ><?php echo $resultsArray->e_email_text ?></textarea>
                <div class="error"> <?php echo form_error('nl_text')?> </div>
            </div>
           
          </div>
        </div>
      </div>
      
      <!-- Box along with label section ends here --> 
      
      <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Email Status</h2>
                    <p>Manage your email status.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Status*</label>
                                <select name="email_status">
                                    <option value="">Select</option>
                                    <option value="1" <?php echo set_select('email_status', '1', $resultsArray->is_active == 1?true:''); ?> >Active</option>
                                    <option value="0" <?php echo set_select('email_status', '0',$resultsArray->is_active == 0?true:''); ?> >Inactive</option>
                                
                                </select>
                            </div>
                            <div class="error"> <?php echo form_error('email_status')?> </div>

                        </div>
                    </div>
                </div>
            </div>

      <!-- Box wihout label section begins here -->
      
      <div class="mu-fld-sub">
        <input type="submit" value="Submit" class="submit_button"/>
        <a href="<?=base_url()?>admin/communication/emailtrigger_list">cancel</a> </div>
      
      <!-- Box wihout label section ends here --> 
      
      <?php echo form_open(); ?> </div>
  </div>
</div>

</body>
</html>