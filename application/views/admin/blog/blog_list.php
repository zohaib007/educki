<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>News &amp; Advice</title>
    <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
    <?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/jquery1.12.4.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/data_table.min.js"></script>
    <script>
    $(function () {
        $('.message').hide().append('<span class="close" title="Dismiss"></span>').fadeIn('slow');
        //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
        $('.message .close').hover(
            function() { $(this).addClass('hover'); },
            function() { $(this).removeClass('hover'); }
        );
            
        $('.message .close').click(function() {
            $(this).parent().fadeOut('slow', function() { $(this).remove(); });
        });
    });
        
    </script>
    <style>
    #display_tr {
        display: table-row !important;
    }
    </style>
</head>

<body>
    <div class="container">
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
            <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        <div class="right-rp">
            <!-- Top Green Bar Section Begins Here -->
            <?php
        include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
        ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="col-continer">
                    <div class="col-3 cat-box">
                        <div class="contntTop-row">
                            <!-- Top Nav Starts Here -->
                            <?php  include(ADMIN_INCLUDE_PATH."includes/blog.php");?>
                            <!-- Top Nav Ends Here -->
                            <div class="actvity-hd">
                                <div class="act-tab b-ad-tb">
                                    <table cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="30%" />
                                            <col width="40%" />
                                            <col width="30%" />
                                        </colgroup>
                                        <tr>
                                            <td class="no-bdr">
                                                <div class="cat-tbl-tp-lf">
                                                    <h1>News &amp; Advice</h1><span class="n-of-cat"><?=count($list)?></span>
                                                </div>
                                            </td>
                                            <td id="" class="pr-msg-bx no-bdr"></td>
                                            <td align="right" class="no-bdr">
                                                <a href="<?= base_url() ?>admin/blog/add_blog/" class="pro-addNew">Add News</a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <?php   
                            if($this->session->flashdata('msg') != ""){
                               echo '<div id="message" class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
                            }
                            ?>
                            <div class="b-prd-tbl">
                                <table border="0" cellspacing="0" id='mytable' class='display'>
                                    <colgroup>
                                        <col width="15%" />
                                        <col width="30%" />
                                        <col width="20%" />
                                        <col width="15%" />
                                        <col width="20%" />
                                    </colgroup>
                                    <thead>
                                        <tr>
                                            <th align="center" style="padding-left:13px;">Date</th>
                                            <th align="left">Name</th>
                                            <th align="left">Author Name</th>
                                            <th align="center">Status</th>
                                            <th align="center">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach($list as $page) {
                                        ?>
                                            <tr>
                                                <td align="center" style="padding-left:13px;">
                                                    <?=date('m-d-Y', strtotime($page->blog_created_date))?>
                                                </td>
                                                <td align="left" >
                                                    <?=$page->blog_name?>
                                                </td>
                                                <td align="left">
                                                    <?=$page->blog_author?>
                                                </td>
                                                <td align="center">
                                                    <?=$page->blog_status==1?'Active':'Inactive'?>
                                                </td>
                                                <td align="center">
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <colgroup>
                                                            <col width="26%" />
                                                            <col width="16%" />
                                                            <col width="16%" />
                                                            <col width="16%" />
                                                            <col width="26%" />
                                                        </colgroup>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td align="center">
                                                                <a title="Edit" href="<?= base_url() ?>admin/blog/edit_blog/<?= $page->blog_id ?>" class="tik-cross-btns p-edt-btn-n"></a>
                                                            </td>
                                                            <td align="center">
                                                                <a onclick="return confirm('Are you sure you want to delete the selected item(s)? ');" href="<?php echo base_url() ?>admin/blog/delete_blog/<?=$page->blog_id ?>" class="tik-cross-btns p-del-btn-n"><img src="<?= base_url() ?>images/b-del-icon.png " alt="" /></a>
                                                            </td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <script>
    $(document).ready(function() {
        $('#mytable').DataTable({
            "paging": true,
            "ordering": true,
            "info": false,
            "order": [
                [0, "desc"]
            ],
            "columnDefs": [
                { orderable: false, targets: -1 }
            ],
            "oLanguage": { "sZeroRecords": "No records found." },
        });
    });
    </script>
</body>

</html>