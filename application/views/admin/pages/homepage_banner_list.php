<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Homepage Banner</title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= base_url() ?>js/filer/css/jquery.filer.css" type="text/css" media="all" />
<link rel="stylesheet" href="<?= base_url() ?>js/filer/css/themes/jquery.filer-dragdropbox-theme.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
<script>
$(function () {
    $('.message').append('<span class="close" title="Dismiss"></span>');
    //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
    $('.message .close').hover(
        function() { $(this).addClass('hover'); },
        function() { $(this).removeClass('hover'); }
    );
    
    $('.message .close').click(function() {
        $(this).parent().fadeOut('slow', function() { $(this).remove(); });
    });
});
</script>
</head>
<body>

    <div class="container_p"> 
        
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
        	<?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        
        <div class="right-rp"> 
            
            <!-- Top Green Bar Section Begins Here -->
            <?php include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php"); ?>
            <!-- Top Green Bar Section Ends Here -->
            
            <div class="mu-contnt-wrp">
                
                <!-- Bread Crumbs starts -->
                <div class="n-crums">
                    <ul>
                        <li>
                            <a href="<?php echo base_url() ?>admin/pages">
                                Content Management
                            </a>
                        </li>
                        <li>
                            <div class="crms-sep">&gt;</div>
                        </li>                       
                        <li>
                            <a href="javascript:void(0);">
                                Homepage Banner
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- Bread Crumbs Ends -->
                 <?php 
						if($this->session->flashdata('msg') != "") {
								echo '<div id="message" class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
						}
						?>
                <div class="mu-contnt-outer">
                    <div class="mu-cm-contntBx-wrp">
                    	
                        <!-- Inner Section Starts -->
                        <div class="contntTop-row">
                        	<!-- Select Bulk Images Section Starts -->
                            <div class="bulkimg-selector" id="upload">
                            	<input type="file" name="files[]" id="filer_input2" multiple>
                            </div>
							<!-- Select Bulk Images Section Ends -->
                            
                            <!-- Image Listing Starts -->
                            <div class="b-prd-tbl">
                            
                                <div class="mn-tblrp">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <colgroup>
                                            <col width="1%">
                                            <col width="87%">
                                            <col width="12%">
                                        </colgroup>
                                        <thead>
                                        <tr>
                                        	<th align="left">&nbsp;</th>
                                            <th align="left">Images</th>
                                            <th align="center">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="imagesdata">
                                        <?php if($images->num_rows()>0){
											$i = 1;
											foreach($images->result() as $image){?>
                                        <tr>
                                            <td align="left">&nbsp;</td>
                                            <td align="left"><img src="<?php
												$im = $image->banner_image;
												$im = explode('/',$im);
												if(count($im)>0){
													$im = end($im);
												}else{
													$im = $image->image_file;
												}
											 echo base_url().'resources/home_image/thumb/'.$im;?>" alt="<?php echo $image->banner_image;?>" /></td>
                                            <td align="center">
                                            	<table border="0" cellspacing="0" cellpadding="0">
                                                    <colgroup>
                                                        <col width="100%">
                                                    </colgroup>
                                                    
                                                    <tr>
                                                        <td align="center">
                                                        	<a title="Delete" href="<?php echo base_url();?>admin/pages/image_delete/<?php echo $image->banner_id;?> " onclick="return confirm('Are you sure you want to delete selected item(s)?');"  class="tik-cross-btns p-del-btn-n"></a>
                                                        </td>
                                                    </tr>
                                                    
                                                </table>
												
                                            </td>
                                            
                                        </tr>
                                        <?php 
										$i++;
										} 
										}else{?>
                                        <tr class="remo"> <td colspan="4" align="center">No image found.</td>
                                        <?php } ?>
                                        </tbody>
                                        
                                    </table>
                                </div>
                            
                            </div>
                            <!-- Image Listing Ends -->
                            
                        </div>
                        <!-- Inner Section Ends -->
                        
                    </div>
                </div>
                
            </div>
            
        </div>
        
    </div>

</body>
<script type="text/javascript">
var root_url = '<?php echo base_url();?>';
</script>
<script src="<?=base_url()?>/js/filer/js/jquery.filer.min.js" type="text/javascript"></script>

<?php include('js/file_filer.php'); ?>
</html>