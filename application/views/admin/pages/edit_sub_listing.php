<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?=$title?></title>
        <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
        <?php include(ADMIN_INCLUDE_PATH . "includes/js.php"); ?>
        <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
         <script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
        <!-- tinymce configration start here.-->
        <script>
                var base_url = '<?php echo base_url(); ?>';
                var ser = '<?php echo $_SERVER['DOCUMENT_ROOT']; ?>educki/resources/tiny_upload';
        </script>
        <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.js"></script>
        <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
        <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/tinymce.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>tiny/common.js"></script>
        <!-- tinymce configration end here.-->
    </head>
    <body>
        <div class="container_p"> 
            <!-- Dashboard Left Side Begins Here -->
            <div class="left_wrp"><?php include(ADMIN_INCLUDE_PATH . "includes/dash-left.php"); ?></div>
            <!-- Dashboard Left Side Ends Here -->
            <div class="right-rp"> 
                <!-- Top Green Bar Section Begins Here -->
                <?php include(ADMIN_INCLUDE_PATH . "includes/top_green_bar.php"); ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="mu-contnt-wrp">
                    <div class="mu-contnt-hdng">
                        <h1>Edit Sub Listing</h1>
                    </div>
                    <!-- Bread crumbs starts here -->
                    <div class="n-crums">
                        <ul>
                            <li> <a href="<?=base_url("admin/pages")?>" >Content Management</a> </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li> <a href="<?=base_url()?>admin/pages/sub_page_listing/<?=$main_pg->pg_id?>" ><?=$main_pg->pg_title?></a> </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li> <a href="<?=base_url()?>admin/pages/sub_sub_listing/<?=$main_pg->pg_id?>/<?=$listing_pg->pg_listing_id?>"><?=$listing_pg->listing_title?></a> </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li> <a href="<?=base_url()?>admin/pages/list_sub_listing/<?=$main_pg->pg_id?>/<?=$listing_pg->pg_listing_id?>/<?=$listing_pg->listing_id?>"><?=$listing_pg->listing_title." "?><?=$sub_title?></a> </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li> <a href="javascript:void(0);"><?=$title?></a> </li>
                        </ul>
                    </div>
                    <!-- Bread crumbs ends here -->
                    <?php echo form_open_multipart('', array('name' => 'add sub page','enctype' => 'multipart/form-data')); ?>  
                    <!-- Box along with label section begins here -->
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Write your Listing</h2>
                            <p>Give your listing a description and image.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row" >
                                <!--<div id="clonediv">-->
                                <div class="mu-flds-wrp">
                                    <label class="lone-set">Description*</label>
                                    <?php $desc = set_value('pg_description') == false ? $page->listing_description : set_value('pg_description'); ?>
                                    <textarea  class="myeditor" name="pg_description" id="desc" ><?php echo $desc; ?></textarea>
                                    <div class="error"><?php echo form_error('pg_description')?></div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Status*</label>
                                        <select name="pg_status">
                                            <option value="">Select</option>
                                            <option value="1" <?php echo set_select('pg_status', '1', $page->listing_status == 1?true:''); ?> >Active</option>
                                            <option value="0" <?php echo set_select('pg_status', '0',$page->listing_status == 0?true:''); ?> >Inactive</option>
                                        </select>
                                    </div>
                                    <div class="error"> <?php echo form_error('pg_status')?> </div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <div class="my-form1">
                                            <div class="my-form1">
                                                <div id="testdiv"></div>
                                                <p class="text-box">
                                                    <label for="box1">Image*</label>
                                                    <img src="<?=base_url()?>resources/page_image/sub_listing/thumb/<?=$page->lisintg_image?>">
                                                    <!-- <input type="text" style="border: none;" name="old-txt" value="<?=$page->lisintg_image?>" readonly="true" /> -->
                                                    <div class="fileUp">
                                                        <input type="file" name="image" value="" id="box1" onchange="javascript: document.getElementById('box1-txt').value = this.value" />
                                                        <label for="box1"></label>
                                                        <input type="text" id="box1-txt" name="hidden_file" value="<?=$page->lisintg_image?>" readonly="true" />
                                                    </div>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="error"> <?php echo form_error('hidden_file')?> </div>
                                </div>                                            
                        </div>                       
                    </div>
                    <div class="mu-fld-sub">
                        <input type="submit" value="Submit" />
                        <a href="<?=base_url()?>admin/pages/list_sub_listing/<?=$main_pg->pg_id?>/<?=$listing_pg->pg_listing_id?>/<?=$page->list_listing_id?>">cancel</a>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </body>
</html>