
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Content Management</title>
    <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
    <?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/jquery1.12.4.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/data_table.min.js"></script>
    <style>
    #display_tr {
        display: table-row !important;
    }
    </style>

    <script>
$(function () {
    $('.message').append('<span class="close" title="Dismiss"></span>');
    //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
    $('.message .close').hover(
        function() { $(this).addClass('hover'); },
        function() { $(this).removeClass('hover'); }
    );
        
    $('.message .close').click(function() {
        $(this).parent().fadeOut('slow', function() { $(this).remove(); });
    });
});
</script>

</head>

<body>
    <div class="container">
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
            <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        <div class="right-rp">
            <!-- Top Green Bar Section Begins Here -->
            <?php
        include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
        ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="col-continer">
                    <div class="col-3 cat-box">
                        <div class="contntTop-row">
                            <div class="actvity-hd">
                                <div class="act-tab b-ad-tb">
                                    <table cellspacing="0" border="0">
                                        <colgroup>
                                            <col width="30%" />
                                            <col width="40%" />
                                            <col width="30%" />
                                        </colgroup>
                                        <tr>
                                            <td class="no-bdr">
                                                <div class="cat-tbl-tp-lf">
                                                    <h1>Content Management</h1><span class="n-of-cat"><?=count($pageList)?></span>
                                                </div>
                                            </td>
                                            <td id="" class="pr-msg-bx no-bdr"></td>
                                            <td align="right" class="no-bdr">
                                                <!-- <a href="<?= base_url() ?>admin/pages/add_page/" class="pro-addNew">Add New Page</a> -->
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <?php   
                            if($this->session->flashdata('msg') != ""){
                               echo '<div id="message" class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
                            }
                            ?>
                                <div class="b-prd-tbl align-top">
                                    <table border="0" cellspacing="0" id='mytable' class='display'>
                                        <colgroup>
                                            <col width="15%" />
                                            <col width="55%" />
                                            <col width="15%" />
                                            <col width="15%" />
                                        </colgroup>
                                        <thead>
                                            <tr>
                                                <th align="center" style="padding-left:13px;">Created Date</th>
                                                <th align="left">Page Title</th>
                                                <th align="center">Update On</th>
                                                <th align="center">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach($pageList as $page) { $count=0;
                                            ?>
                                                <tr>
                                                    <td align="center" class="td-content" style="padding-left:13px;">
                                                        <?=date('m-d-Y', strtotime($page->date))?>
                                                    </td>
                                                    <td class="td-content">
                                                        <h3><?= limited_text($page->pg_title , 40) ?></h3>
                                                    </td>
                                                    <td align="center" class="td-content">
                                                        <?=$page->update!=''?date('m-d-Y', strtotime($page->update)):'N/A'?>
                                                    </td>
                                                    <td align="center">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <colgroup>
                                                                <col width="5%" />
                                                                <col width="20%" />
                                                                <col width="35%" />
                                                                <col width="35%" />
                                                                <col width="5%" />
                                                            </colgroup>
                                                            <tr id="display_tr">
                                                                <td>&nbsp;</td>
                                                                <?php if($page->pg_id == 1){?>
                                                                <td align="center">
                                                                    <a title="Edit" href="<?= base_url() ?>admin/pages/homepage/<?= $page->pg_id ?>" class="tik-cross-btns p-edt-btn-n"></a>
                                                                </td>
                                                                <td align="center">
                                                                    <a title="Homepage Banner" href="<?= base_url() ?>admin/pages/homepage_banner" class="tik-cross-btns dtlbtn-g"></a>
                                                                </td>
                                                                <td align="center">
                                                                    <a title="Homepage Member" href="<?= base_url() ?>admin/pages/homepage_member" class="tik-cross-btns dtlbtn-m-l"></a>
                                                                </td>
                                                                <?php } else if($page->pg_id == 10){ ?>
                                                                <td align="center">
                                                                    <a title="Edit" href="<?= base_url() ?>admin/pages/edit_pricing_benefits_page/" class="tik-cross-btns p-edt-btn-n"></a>
                                                                </td>
                                                                <td align="center">
                                                                    <a title="Tier Management" href="<?= base_url() ?>admin/tier_management/" class="tik-cross-btns dtlbtn-p-s"></a>
                                                                </td>
                                                                <td>&nbsp;</td>
                                                                <?php } else {?>
                                                                <td align="center">
                                                                    <a title="Edit" href="<?= base_url() ?>admin/pages/edit_page/<?= $page->pg_id ?>" class="tik-cross-btns p-edt-btn-n"></a>
                                                                </td>
                                                                <?php } if($page->pg_is_pages){ ?>
                                                                <td align="center">
                                                                    <a title="View Pages" href="<?= base_url() ?>admin/pages/sub_page_listing/<?= $page->pg_id ?>" class="tik-cross-btns dtlbtn-n"></a>
                                                                </td>
                                                                <td>&nbsp;</td>
                                                                <?php } else if($page->pg_id != 10 && $page->pg_id != 1){?>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                                <?php }?>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <?php
                                    }
                                    ?>
                                        </tbody>
                                    </table>
                                    <!--
                        <div class="pagination-row" style="width:100%">
                            <?php //$paginglink ?>
                        </div>
                        -->
                                </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <script>
    $(document).ready(function() {
        $('#mytable').DataTable({
            "paging": true,
            "ordering": true,
            "info": false,
            "order": [
                [0, "asc"]
            ],
            "columnDefs": [
                { orderable: false, targets: -1 }
            ],
            "oLanguage": { "sZeroRecords": "No records found." },
        });
    });
    </script>
</body>

</html>