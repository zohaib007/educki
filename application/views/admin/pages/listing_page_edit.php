<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Edit <?=$listing_pg->listing_title?></title>
    <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
    <?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
    <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
    <script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
    <!-- tinymce configration start here.-->
    <script>
    var base_url = '<?php echo base_url();?>';
    var ser = '<?php echo $_SERVER['DOCUMENT_ROOT'];?>educki/resources/tiny_upload';
    </script>
    <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.js"></script>
    <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
    <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>tiny/common.js"></script>
    <!-- tinymce configration end here.-->
    <script>
    function delete_gall_image(sel, name, fieldname) {
        //  alert(sel);
        $('.' + name).hide();
        $('#' + name).val('');
        $.ajax({
            url: base_url + 'admin/pages/delete_listing_img',
            type: 'POST',
            data: { nId: sel, fieldname: fieldname, <?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>' },
        }).done(function() {
            console.log("success");
        }).fail(function() {
            console.log("error");
        }).always(function() {
            console.log("complete");
        });
    }

    $('.my-form').on('click', '.remove-box', function() {

        $(this).parent().fadeOut("slow", function() {
            $(this).remove();
            $('.box-number').each(function(index) {
                $(this).text(index + 1);
            });
        });
        return false;
    });

    function convertToSlug(Text) {
        url = Text
            .toLowerCase()
            .replace(/ /g, '-').replace(/[^\w -]+/g, '');
        document.getElementById("my_url").value = url;
    }

    function convertToSlug2(Text) {
        url = Text
            .toLowerCase()
            .replace(/ /g, '-').replace(/[^\w -]+/g, '');
        document.getElementById("my_url").value = url;
    }

    $(document).ready(function() {
        $(".submit_blog_form").on('click', function(e) {
            var data_attr = $(this).attr('data_attr');
            var sub_url = $(this).attr('submit_url');
            if (data_attr == 1) {
                $('#myForm').attr('action', sub_url);
                $("#myForm").attr('target', '_blank');
                $('#myForm').submit();

            } else {

                $('#myForm').attr('action', sub_url);
                $("#myForm").attr('target', '_self');
                $('#myForm').submit();
            }
        });
    });
    </script>
</head>

<body>
    <div class="container_p">
        <!-- Dashboard Left Side Begins Here -->
        <div class="left_wrp">
            <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
        </div>
        <!-- Dashboard Left Side Ends Here -->
        <div class="right-rp">
            <!-- Top Green Bar Section Begins Here -->
            <?php
        include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
        ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="mu-contnt-wrp">
                    <div class="mu-contnt-hdng">
                        <h1>Edit <?=$listing_pg->listing_title?></h1>
                    </div>
                    <!-- Bread crumbs starts here -->
                    <div class="n-crums">
                        <ul>
                            <li> <a href="<?php echo base_url() ?>admin/pages">Content Management</a> </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li> <a href="<?php echo base_url('admin/pages/sub_page_listing/' . $main_pg->pg_id) ?>"><?= $main_pg->pg_title ?></a> </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li> <a href="<?= base_url() ?>admin/pages/sub_sub_listing/<?= $main_pg->pg_id ?>/<?= $sub_pg->sub_id ?>"><?= $sub_pg->sub_pg_title ?></a> </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li> <a href="javascript:voide(0);"> Edit <?=$listing_pg->listing_title?></a> </li>
                        </ul>
                    </div>
                    <!-- Bread crumbs ends here -->
                    <?php echo form_open_multipart('',array('name'=>'edit listing','id'=>'myForm')); ?>
                    <!-- Box along with label section begins here -->
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Write your Listing</h2>
                            <p>Manage the content on this Listing.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Listing Title*</label>
                                        <input type="text" value="<?php echo $listing_pg->listing_title; ?>" name="listing_title" onfocusout="convertToSlug2($(this).val())" id="my_title" />
                                    </div>
                                    <div class="error"><?php echo form_error('listing_title')?> </div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>URL*</label>
                                        <input type="text" value="<?php echo $listing_pg->listing_url; ?>" name="listing_url" id="my_url" onkeyup="convertToSlug($(this).val())" />
                                        <input type="hidden" name="old_url" value="<?php echo $listing_pg->listing_url; ?>" />
                                    </div>
                                    <div class="error"><?php echo form_error('listing_url')?></div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <div class="my-form1">
                                            <div class="my-form1">
                                                <!-- <div id="testdiv"></div> -->
                                                <?php  if(@$listing_pg->lisintg_image){?>
                                                <label for="box1">Image*<span class="box-number"></span><span class="sub-text x-set">Image size: 715 x 372</span></label>
                                                <p class="text-box my-form image">
                                                    <img src="<?= base_url() ?>resources/page_image/listing/banner/thumb/<?=$listing_pg->lisintg_image; ?>"></img>
                                                    <input type="hidden" name="image" value="<?=@$listing_pg->lisintg_image; ?>" />
                                                </p>
                                                <!-- <a class="remove-box image" href="javascript:void(0)" onclick="delete_gall_image(<?php echo @$listing_pg->listing_id; ?>,'image','lisintg_image');">Remove</a> -->
                                                <!-- </div> -->
                                                <div class="fileUp">
                                                    <input type="file" name="image" value="" id="box1" onchange="javascript: document.getElementById('listingbox1-txt').value = this.value" />
                                                    <label for="box1"></label>
                                                    <input type="text" id="listingbox1-txt" name="hidden_file1" value="<?=@$listing_pg->lisintg_image; ?>" readonly="true" />
                                                </div>
                                                <?php } else {?>
                                                <p class="text-box">
                                                    <label for="box1">Image*<span class="sub-text x-set">Image size: 715 x 372</span></label>
                                                    <!--<input type="file" name="image" value="" id="box1" />-->
                                                    <div class="fileUp">
                                                        <input type="file" name="image" value="" id="box1" onchange="javascript: document.getElementById('listingbox1-txt').value = this.value" />
                                                        <label for="box1"></label>
                                                        <input type="text" id="listingbox1-txt" name="hidden_file1" value="Upload File" readonly="true" />
                                                    </div>
                                                </p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="error"><?php echo form_error('hidden_file1')?> </div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <div class="my-form1">
                                            <div class="my-form1">
                                                <div id="testdiv"></div>
                                                <?php if(@$listing_pg->lisintg_image2){?>
                                                <label for="box1">Hover Image*<span class="box-number"></span><span class="sub-text x-set">Image size: 715 x 372</span></label>
                                                <p class="text-box my-form hover_image">
                                                    <img src="<?= base_url() ?>resources/page_image/listing/banner/thumb/<?=$listing_pg->lisintg_image2; ?>"></img>
                                                    <input type="hidden" name="hover_image" value="<?=@$listing_pg->lisintg_image2; ?>" />
                                                </p>
                                                <!-- <a class="remove-box hover_image" href="javascript:void(0)" onclick="delete_gall_image(<?php echo @$listing_pg->listing_id; ?>,'hover_image','lisintg_image2');">Remove</a> -->
                                                <!-- </div> -->
                                                <div class="fileUp">
                                                    <input type="file" name="hover_image" value="" id="box1" onchange="javascript: document.getElementById('listingbox2-txt').value = this.value" />
                                                    <label for="box1"></label>
                                                    <input type="text" id="listingbox2-txt" name="hidden_file2" value="<?=@$listing_pg->lisintg_image; ?>" readonly="true" />
                                                </div>
                                                <?php } else {?>
                                                <p class="text-box">
                                                    <label for="box1">Hover Image*<span class="sub-text x-set">Image size: 715 x 372</span></label>
                                                    <!--<input type="file" name="image" value="" id="box1" />-->
                                                    <div class="fileUp">
                                                        <input type="file" name="hover_image" value="" id="box1" onchange="javascript: document.getElementById('listingbox2-txt').value = this.value" />
                                                        <label for="box1"></label>
                                                        <input type="text" id="listingbox2-txt" name="hidden_file2" value="Upload File" readonly="true" />
                                                    </div>
                                                </p>
                                                <?php } ?>
                                            </div>
                                            <!--  <a class="add-box" href="#">Add More</a>-->
                                        </div>
                                    </div>
                                    <div class="error"><?php echo form_error('hidden_file2')?> </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Image</h2>
                            <p>Upload an image for this page.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Banner Title*</label>
                                        <input type="text" value="<?php echo $listing_pg->listing_banner_title; ?>" name="banner_title" />
                                    </div>
                                    <div class="error"><?php echo form_error('banner_title')?> </div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <div class="my-form1">
                                            <div class="my-form1">
                                                <div id="testdiv"></div>
                                                <?php if(@$listing_pg->listing_banner_image) {?>
                                                <label for="box1">Banner Image*<span class="box-number"></span><span class="sub-text x-set">Image size: 715 x 372</span></label>
                                                <p class="text-box my-form banner_image">
                                                    <img style="width: 150px;height: 60px;" src="<?= base_url() ?>resources/page_image/listing/banner/thumb/<?=$listing_pg->listing_banner_image; ?>"></img>
                                                    <input type="hidden" name="banner_image" value="<?=@$listing_pg->listing_banner_image; ?>" />
                                                </p>
                                                <!-- <a class="remove-box banner_image" href="javascript:void(0)" onclick="delete_gall_image(<?php echo @$listing_pg->listing_id; ?>,'banner_image','listing_banner_image');">Remove</a> -->
                                                <!-- </div> -->
                                                <div class="fileUp">
                                                    <input type="file" name="banner_image" value="" id="box1" onchange="javascript: document.getElementById('bannerbox1-txt').value = this.value" />
                                                    <label for="box1"></label>
                                                    <input type="text" id="bannerbox1-txt" name="hidden_file3" value="<?=@$listing_pg->lisintg_image; ?>" readonly="true" />
                                                </div>
                                                <?php } else {?>
                                                <p class="text-box">
                                                    <label for="box1">Banner Image*<span class="sub-text x-set">Image size: 715 x 372</span></label>
                                                    <!--<input type="file" name="image" value="" id="box1" />-->
                                                    <div class="fileUp">
                                                        <input type="file" name="banner_image" value="" id="box1" onchange="javascript: document.getElementById('bannerbox1-txt').value = this.value" />
                                                        <label for="box1"></label>
                                                        <input type="text" id="bannerbox1-txt" name="hidden_file3" value="Upload File" readonly="true" />
                                                    </div>
                                                </p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="error"> <?php echo form_error('hidden_file3')?> </div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <label class="lone-set">Page description</label>
                                    <textarea class="myeditor" name="pg_description" id="desc"><?php echo $listing_pg->listing_description; ?></textarea>
                                    <div class="error"><?php echo form_error('pg_description')?> </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Meta Information</h2>
                        <p> Set up the meta description. These help define how this product shows up on search engines. </p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Meta title</label>
                                    <input type="text" value="<?php echo $listing_pg->meta_title; ?>" name="meta_title" />
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Meta Keyword</label>
                                    <input type="text" value="<?php echo $listing_pg->meta_keywords; ?>" name="meta_keywords" />
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Meta description </label>
                                    <textarea class="text_meta" name="meta_description"><?php echo $listing_pg->meta_description; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Listing Status</h2>
                        <p>Manage your Listing status.</p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Status*</label>
                                    <select name="listing_status">
                                        <option value="">Select</option>
                                        <option value="1" <?php echo set_select( 'listing_status', '1', $listing_pg->listing_status == 1?true:''); ?> >Active</option>
                                        <option value="0" <?php echo set_select( 'listing_status', '0',$listing_pg->listing_status == 0?true:''); ?> >Inactive</option>
                                    </select>
                                </div>
                                <div class="error"><?php echo form_error('listing_status')?> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="mu-fld-sub">
                <input type="submit" value="Submit" />
                <a href="<?=base_url('admin/pages/sub_sub_listing/'.$main_pg->pg_id.'/'.$sub_pg->sub_id)?>">cancel</a> 
            </div> -->
                <div class="mu-fld-sub">
                    <?php if($this->session->userdata('admin_status') == 1 ) { ?>
                    <a data_attr="0" submit_url="<?=base_url()?>admin/pages/edit_listing/<?=$main_pg->pg_id?>/<?=$sub_pg->sub_id?>/<?=$listing_pg->listing_id?>" class="submit_blog_form" href="javascript:void(0);" id="asd">Submit</a>
                    <a data_attr="1" submit_url="<?=base_url()?>admin/preview/sub_page_preview/<?=$listing_pg->listing_id?>" class="submit_blog_form" href="javascript:void(0);" id="preview">Preview</a>
                    <a href="<?=base_url('admin/pages/sub_sub_listing/'.$main_pg->pg_id.'/'.$sub_pg->sub_id)?>">cancel</a>
                    <?php } ?>
                </div>
                <!-- Box wihout label section ends here -->
                <?php echo form_close(); ?> </div>
    </div>
    </div>
</body>

</html>