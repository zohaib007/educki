<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?= $sub_title; ?></title>
        <link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
        <?php include(ADMIN_INCLUDE_PATH . "includes/js.php"); ?>
        <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
         <script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
        <!-- tinymce configration start here.-->
        <script>
                var base_url = '<?php echo base_url(); ?>';
                var ser = '<?php echo $_SERVER['DOCUMENT_ROOT']; ?>educki/resources/tiny_upload';
        </script>
        <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.js"></script>
        <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
        <script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/tinymce.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>tiny/common.js"></script>
        <!-- tinymce configration end here.-->
    </head>
    <body>
        <div class="container_p"> 
            <!-- Dashboard Left Side Begins Here -->
            <div class="left_wrp"><?php include(ADMIN_INCLUDE_PATH . "includes/dash-left.php"); ?></div>
            <!-- Dashboard Left Side Ends Here -->
            <div class="right-rp"> 
                <!-- Top Green Bar Section Begins Here -->
                <?php include(ADMIN_INCLUDE_PATH . "includes/top_green_bar.php"); ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="mu-contnt-wrp">
                    <div class="mu-contnt-hdng">
                        <h1>Edit <?= $tier_data->tier_name; ?> Tier</h1>
                    </div>
                    <!-- Bread crumbs starts here -->
                    <div class="n-crums">
                        <ul>
                            <li> <a href="<?=base_url("admin/pages")?>" >Content Management</a> </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li> <a href="<?=base_url()?>admin/tier_management/" ><?=$title?></a> </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li> <a href="javascript:void(0);"><?=$sub_title?></a> </li>
                        </ul>
                    </div>
                    <!-- Bread crumbs ends here -->
                    <?php echo form_open_multipart('', array('name' => 'add sub page','enctype' => 'multipart/form-data')); ?>  
                    <!-- Box along with label section begins here -->
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Tier Details</h2>
                            <p>Provide details about the tier.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row" >
                                <!--<div id="clonediv">-->
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Tier Name*</label>
                                        <input type="text" name="tier_name" value="<?php echo set_value('tier_name')!=''?set_value('tier_name'):$tier_data->tier_name; ?>"/>
                                    </div>
                                    <div class="error"> <?php echo form_error('tier_name')?> </div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Tier Title*</label>
                                        <input type="text" name="tier_title" value="<?php echo set_value('tier_title')!=''?set_value('tier_title'):$tier_data->tier_title; ?>"/>
                                    </div>
                                    <div class="error"> <?php echo form_error('tier_title')?> </div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Transaction Fee*</label>
                                        <input type="text" style="width: 30%;" name="fee" value="<?php echo set_value('fee')!=''?set_value('fee'):$tier_data->tier_fee; ?>"/>
                                        <span>%</span>
                                    </div>
                                    <div class="error"> <?php echo form_error('fee')?> </div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Subscription duration*</label>
                                        <select name="months">
                                            <option value="">Select</option>
                                            <option value="1">One Month</option>
                                            <option value="3">Three Months </option>
                                            <option value="6">Six Months </option>
                                        </select>
<!--                                        <input type="text" name="months" value="--><?php //echo set_value('months')!=''?set_value('months'):$tier_data->tier_months; ?><!--"/>-->
                                    </div>
                                    <div class="error"> <?php echo form_error('months')?> </div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Amount/month*</label>
                                        <input type="text" name="amount" value="<?php echo set_value('amount')!=''?set_value('amount'):$tier_data->tier_amount; ?>"/>
                                    </div>
                                    <div class="error"> <?php echo form_error('amount')?> </div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Local Pickup*</label>
                                        <select name="pickup">
                                            <option value="">Select</option>
                                            <option value="1" <?php echo set_select('pickup', '1', $tier_data->is_pickup == 1?true:''); ?> >Available</option>
                                            <option value="0" <?php echo set_select('pickup', '0',$tier_data->is_pickup == 0?true:''); ?> >Not Available</option>
                                        </select>
                                    </div>
                                    <div class="error"> <?php echo form_error('pickup')?> </div>
                                </div>                                
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Facebook Store*</label>
                                        <select name="store">
                                            <option value="">Select</option>
                                            <option value="1" <?php echo set_select('store', '1', $tier_data->is_fb_store == 1?true:''); ?> >Available</option>
                                            <option value="0" <?php echo set_select('store', '0',$tier_data->is_fb_store == 0?true:''); ?> >Not Available</option>
                                        </select>
                                    </div>
                                    <div class="error"> <?php echo form_error('store')?> </div>
                                </div>                                
                            </div>
                            
                        </div>
                       
                    </div>
                    <div class="mu-fld-sub">
                        <input type="submit" value="Submit" />
                        <a href="<?=base_url()?>admin/tier_management/">cancel</a>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </body>
</html>