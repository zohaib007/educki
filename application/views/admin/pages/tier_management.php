<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?= $title; ?></title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />

<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/jquery1.12.4.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/data_table/data_table.min.js"></script>

<script>
$(function () {
    $('.message').append('<span class="close" title="Dismiss"></span>');
    //setTimeout(function() { $(".message info").fadeOut(1500); }, 4000)
    $('.message .close').hover(
        function() { $(this).addClass('hover'); },
        function() { $(this).removeClass('hover'); }
    );
        
    $('.message .close').click(function() {
        $(this).parent().fadeOut('slow', function() { $(this).remove(); });
    });
});
</script>

<style>
#display_tr {
    display: table-row !important;
}
</style>
</head>

<body>
<div class="container">
    
    <!-- Dashboard Left Side Begins Here -->
    <div class="left_wrp">
        <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
    </div>
    <!-- Dashboard Left Side Ends Here -->
    
    <div class="right-rp">
        
        <!-- Top Green Bar Section Begins Here -->
        <?php
        include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
        ?>
        <!-- Top Green Bar Section Ends Here -->
        
        <div class="n-crums">
                <ul>
                    <li> <a href="<?=base_url("admin/pages")?>" >Content Management</a> </li>
                    <li>
                        <div class="crms-sep">&gt;</div>
                    </li>
                    <li> <a href="javascript:void(0);"><?=$title?></a> </li>
                </ul>
            </div>

        <div class="col-continer">
            <div class="col-3 cat-box">
                <div class="contntTop-row">
                    <div class="actvity-hd">
                        <div class="act-tab b-ad-tb">
                            <table cellspacing="0" border="0">
                                <colgroup>
                                <col width="30%" />
                                <col width="40%" />
                                <col width="30%" />
                                </colgroup>
                                <tr>
                                    <td class="no-bdr">
                                        <div class="cat-tbl-tp-lf">
                                            <h1>Tier Listing</h1><span class="n-of-cat"><?=count($listing_list)?></span>
                                        </div>
                                    </td>
                                    <td id="" class="pr-msg-bx"></td>
                                    <!--<td align="center">
                                        <!--<a href="<?= base_url("admin/pages/add_listing/".$listing_pg->pg_listing_id) ?>" class="pro-addNew">Add New Listing Page</a>
                                        <a href="" class="pro-addNew">Add New Sub Listing</a>
                                    </td>-->
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?php   
                    if($this->session->flashdata('msg') != ""){
                       echo '<div id="message" class="message info"><p>'.$this->session->flashdata('msg').'</p></div>';
                    }
                    ?>
                    <div class="b-prd-tbl align-top">
                        <!--<form name="myForm" method="post" action="<?= base_url() ?>admin/product/del/">-->
                            <table border="0" cellspacing="0" id='mytable'>
                                <colgroup>
                                    <col width="12.5%" />
                                    <col width="12.5%" />
                                    <col width="17%" />
                                    <col width="11%" />
                                    <col width="11%" />
                                    <col width="11%" />
                                    <col width="13%" />
                                    <col width="12%" />
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th align="center" style="padding-left:13px;" >Created Date</th>
                                        <th align="center" >Update On</th>
                                        <th align="left" >Tier Name</th>
                                        <th align="center" >Tier Fee</th>
                                        <th align="center" >Tier Months</th>
                                        <th align="center" >Amount</th>
                                        <th align="center" >Facebook Store</th>
                                        <th align="center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (count($listing_list) > 0){
                                         foreach($listing_list as $page) {?>
                                    <tr>
                                        
                                        <td align="center" class="td-content" style="padding-left:13px;">
                                            <?=date('m-d-Y', strtotime($page->date))?>
                                        </td>
                                        <td align="center" class="td-content" >
                                            <?=$page->update!=''?date('m-d-Y', strtotime($page->update)):'N/A'?>
                                        </td>
                                        <td calign="left" class="td-content" >
                                            <?= $page->tier_name; ?>
                                        </td>
                                        <td align="center" class="td-content" >
                                            <?= $page->tier_fee; ?>
                                        </td>
                                        <td align="center" class="td-content" >
                                            <?= $page->tier_months; ?>
                                        </td>
                                        <td align="center" class="td-content" >
                                            <?= $page->tier_amount; ?>
                                        </td>
                                        <td align="center" class="td-content" >
                                            <?php if($page->is_fb_store==1){echo "Available";}else{echo "Not Available";} ?>
                                        </td>
                                        <td align="center">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <colgroup>
                                                    <col width="100%" />
                                                </colgroup>
                                                <tr id="display_tr">
                                                    <td align="center">
                                                        <a title="Edit" href="<?= base_url() ?>admin/tier_management/edit_tier/<?=$page->tier_id?>/" class="tik-cross-btns p-edt-btn-n"></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <?php
                                        }}else{
                                        echo '<tr><td colspan="6" align="center" >No data found.</td></tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        <!--</form>-->
                        <div class="pagination-row" style="width:100%">
                            <?php //$paginglink ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){
    $('#mytable').DataTable({
        "paging":   true,
        "ordering": true,
        "info":     false,
        "order": [[ 0, "asc" ]],
        "columnDefs": [
           { orderable: false, targets: -1 }
        ],
        "oLanguage": { "sZeroRecords": "No records found." },
    });
});
</script>
</body>
</html>