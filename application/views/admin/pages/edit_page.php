<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit <?php echo $page_data->pg_title; ?></title>

<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH . "includes/js.php"); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
<script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
<!-- tinymce configration start here.-->
<script>
var base_url = '<?php echo base_url(); ?>';
var ser = '<?php echo $_SERVER['DOCUMENT_ROOT']; ?>educki/resources/tiny_upload';
</script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.js"></script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?= base_url() ?>tiny/common.js"></script>
<!-- tinymce configration end here.-->

        <script>
            function delete_gall_image(sel) {
                //  alert(sel);
                $(".text-box").hide();
                $(".remove-box").hide();
                $("input[name='image']").val('');
                $.ajax({
                    url: base_url + 'admin/pages/delete_page_img',
                    type: 'POST',
                    data: {nId: sel, <?= $this->security->get_csrf_token_name() ?>: '<?= $this->security->get_csrf_hash() ?>'},
                }).done(function () {
                    console.log("success");
                }).fail(function () {
                    console.log("error");
                }).always(function () {
                    console.log("complete");
                });
            }

            $('.my-form').on('click', '.remove-box', function () {

                $(this).parent().fadeOut("slow", function () {
                    $(this).remove();
                    $('.box-number').each(function (index) {
                        $(this).text(index + 1);
                    });
                });
                return false;
            });


        </script>

<script>
   $(document).ready(function(){
        $(".submit_blog_form").on('click',function(e){
        var data_attr = $(this).attr('data_attr');
        var sub_url = $(this).attr('submit_url');
        if(data_attr==1)
            {
                $('#myForm').attr('action',sub_url);
                $("#myForm").attr('target', '_blank');
                $('#myForm').submit();
                
            }else{
                
                $('#myForm').attr('action',sub_url);
                $("#myForm").attr('target', '_self');
                $('#myForm').submit();
            }
        });
    });
    
    function convertToSlug(Text)
    {
        url = Text.toLowerCase().replace(/ /g,'-').replace(/[^\w -]+/g,'');
        document.getElementById("my_url").value = url;
    }

    function convertToSlug2(Text)
    {
        var turl = Text.toLowerCase().replace(/ /g,'-').replace(/[^\w -]+/g,'');
        document.getElementById("my_url").value = turl;
    }

</script>

    </head>
    <body>

        <div class="container_p"> 

            <!-- Dashboard Left Side Begins Here -->
            <div class="left_wrp">
                <?php include(ADMIN_INCLUDE_PATH . "includes/dash-left.php"); ?>
            </div>
            <!-- Dashboard Left Side Ends Here -->

            <div class="right-rp"> 

                <!-- Top Green Bar Section Begins Here -->
                <?php
                include(ADMIN_INCLUDE_PATH . "includes/top_green_bar.php");
                ?>
                <!-- Top Green Bar Section Ends Here -->
                <div class="mu-contnt-wrp">
                    <div class="mu-contnt-hdng">
                        <h1>Edit <?php echo $page_data->pg_title; ?></h1>
                    </div>
                    <!-- Bread crumbs starts here -->
                    <div class="n-crums">
                        <ul>
                            <li> <a href="<?php echo base_url() ?>admin/pages">Content Management</a> </li>
                            <li>
                                <div class="crms-sep">&gt;</div>
                            </li>
                            <li> <a href="javascript:void(0);"> Edit <?php echo $page_data->pg_title; ?> </a> </li>
                        </ul>
                    </div>
                    <!-- Bread crumbs ends here --> 

                    <?php echo form_open_multipart('', array('name' => 'myForm', 'id' => 'myForm')); ?> 

                    <!-- Box along with label section begins here -->

                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Page Details</h2>
                            <p>Manage the content on this page.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">

                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>Title*</label>
                                        <input type="text" value="<?=$page_data->pg_title?>" name="pg_title" id="my_title" onfocusout="convertToSlug2($(this).val())"/>
                                    </div>
                                    <div class="error"> <?php echo form_error('pg_title') ?> </div>
                                </div>                                

                                <?php if($page_data->pg_id!=4) { ?>

                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <label>URL*</label>
                                            <input type="text" id="my_url" value="<?php echo $page_data->pg_url; ?>" name="pg_url" onkeyup = "convertToSlug($(this).val())"/> 
                                    </div>
                                    <div class="error"> <?php echo form_error('pg_url') ?> </div>
                                </div>                                

                                <?php } else {?>

                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds_long">
                                        <!-- <label type="hidden">URL*</label> -->
                                            <input  type="hidden" value="<?php echo $page_data->pg_url; ?>" name="pg_url" /> 
                                    </div>
                                </div>

                                <?php }?>

                                <?php if($page_data->pg_id!=4) {?>
                                <div class="mu-flds-wrp">
                                    <label class="lone-set">Description* </label>
                                    <textarea class="myeditor" name="pg_description" id="desc" ><?php echo $page_data->pg_description; ?></textarea>
                                    <div class="error"> <?php echo form_error('pg_description') ?> </div>
                                </div>                                
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <?php if($page_data->pg_id!=8 && $page_data->pg_id!=9 && $page_data->pg_id!=4){?>
                    <div class="mu-contnt-outer">
                        <div class="mu-contnt-lfLbl ad-size">
                            <h2>Image</h2>
                            <p>Upload an image for this page.</p>
                        </div>
                        <div class="mu-contntBx-wrp">
                            <div class="contntTop-row">
                                <div class="mu-flds-wrp">
                                    <div class="mu-frmFlds">
                                        <div class="my-form1">
                                            <div class="my-form1">
                                                <div id="testdiv"></div>
                                                <?php
                                                if (@$page_data->pg_image) {
                                                    ?>
                                                    <label for="box1">Image <!-- <span class="box-number"></span><span class="sub-text x-set">Image size: 715 x 372</span> --></label>
                                                    <p class="text-box my-form">
                                                        <img style="width: 150px;height: 50px;" src="<?= base_url() ?>/resources/page_image/thumb/<?= $page_data->pg_image; ?>"></img>
                                                        <input type="hidden" name="image" value="<?= @$page_data->pg_image; ?>"  />
                                                    </p>
                                                    <a class="remove-box" href="javascript:void(0)" onclick="delete_gall_image(<?php echo @$page_data->pg_id; ?>);">Remove</a> </div>
                                                <div class="fileUp">
                                                    <input type="file" name="image" value="" id="box1" onchange="javascript: document.getElementById('box1-txt').value = this.value" />
                                                    <label for="box1"></label>
                                                    <input type="text" id="box1-txt" value="Upload File" readonly="true" />
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <p class="text-box">
                                                    <label for="box1">Image<span class="sub-text x-set">Image size: 715 x 372</span></label>
                                                    <!--<input type="file" name="image" value="" id="box1" />-->
                                                    <div class="fileUp">
                                                        <input type="file" name="image" value="" id="box1" onchange="javascript: document.getElementById('box1-txt').value = this.value" />
                                                        <label for="box1"></label>
                                                        <input type="text" id="box1-txt" value="Upload File" readonly="true" />
                                                    </div>
                                                </p>
                                            <?php } ?>
                                            <!--  <a class="add-box" href="#">Add More</a>--> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>

                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Meta Information</h2>
                        <p> Set up the meta description. These help define how this product shows up on search engines. </p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Meta title</label>
                                    <input type="text" value="<?php echo $page_data->meta_title; ?>" name="meta_title" />
                                    <input type="hidden" value="<?php echo $page_data->pg_id; ?>" name="pg_id" />
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Meta Keyword</label>
                                    <input type="text" value="<?php echo $page_data->meta_keywords; ?>" name="meta_keywords" />
                                </div>
                            </div>
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Meta description </label>
                                    <textarea class="text_meta" name="meta_description"><?php echo $page_data->meta_description; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<!-- 
                <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Page Status And Listing</h2>
                        <p>Manage your page status and sub page listing.</p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Status*</label>
                                    <select name="pg_status">
                                        <option value="">Select</option>
                                        <option value="1" <?php echo set_select('pg_status', '1', $page_data->pg_status == 1 ? true : ''); ?> >Active</option>
                                        <option value="0" <?php echo set_select('pg_status', '0', $page_data->pg_status == 0 ? true : ''); ?> >Inactive</option>
                                    </select>
                                </div>
                                <div class="error"> <?php echo form_error('pg_status') ?> </div>
                            </div>
<                          <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Is Listing*</label>
                                    <select name="pg_is_listing">
                                        <option value="">Select</option>
                                        <option value="1" <?php echo set_select('pg_is_listing', '1', $page_data->pg_is_listing == 1 ? true : ''); ?> >Yes</option>
                                        <option value="0" <?php echo set_select('pg_is_listing', '0', $page_data->pg_is_listing == 0 ? true : ''); ?> >No</option>
                                    </select>
                                </div>
                                <div class="error"> <?php echo form_error('pg_is_listing') ?> </div>
                            </div>->
                        </div>
                    </div>
                </div> 

                 <!-- <div class="mu-fld-sub">
                    <?php if ($this->session->userdata('admin_status') == 1) { ?>
                        <input type="submit" value="Submit" />
                    <?php } ?>
                    <a href="<?php echo base_url() ?>admin/pages">cancel</a> </div> -->

                <div class="mu-fld-sub">
                    <?php if($this->session->userdata('admin_status') == 1 ) { ?>
                        <a data_attr="0" submit_url="<?=base_url()?>admin/pages/edit_page/<?=$page_data->pg_id?>" class="submit_blog_form" href="javascript:void(0);" id="asd">Submit</a>
                     <?php if($page_data->pg_id == 1222 ){ ?>
                        <a data_attr="1" submit_url="<?=base_url()?>admin/pages/preview" class="submit_blog_form" href="javascript:void(0);" id="preview">Preview</a>
                    <?php } else if($page_data->pg_id == 9 ){ ?> 
                        <a data_attr="1" submit_url="<?=base_url()?>admin/preview/terms_of_service_preview" class="submit_blog_form" href="javascript:void(0);" id="preview">Preview</a>
                    <?php } else if($page_data->pg_id == 8){ ?> 
                        <a data_attr="1" submit_url="<?=base_url()?>admin/preview/privacy_policy_preview" class="submit_blog_form" href="javascript:void(0);" id="preview">Preview</a>
                    <?php } ?> 
                    <a href="<?=base_url()?>admin/pages">cancel</a>
                    <?php } ?>
                </div>


                <!-- Box wihout label section ends here -->

                <?php echo form_close(); ?> </div>
        </div>
        </div>
    </body>
</html>