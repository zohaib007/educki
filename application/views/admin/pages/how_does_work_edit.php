<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit <?= $work_data->work_page_title; ?></title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
<script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
<!-- tinymce configration start here.-->
<script>
var base_url='<?php echo base_url();?>';
var ser='<?php echo $_SERVER['DOCUMENT_ROOT'];?>dev/resources/tiny_upload';
</script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.js"></script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>tiny/common.js"></script>
<!-- tinymce configration end here.-->

<script>
    function delete_gall_image(sel){
        //  alert(sel);
        $(".text-box").hide();
        $(".remove-box").hide();
        $("input[name='image']").val('');
        $.ajax({
            url:  base_url+'admin/pages/delete_page_img',
            type: 'POST',
            data: {nId: sel, <?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>'},
        }).done(function() {
            console.log("success");
        }).fail(function() {
            console.log("error");
        }).always(function() {
            console.log("complete");
        });
    }
             
    $('.my-form').on('click', '.remove-box', function(){
       
        $(this).parent().fadeOut("slow", function() {
            $(this).remove();
            $('.box-number').each(function(index){
                $(this).text( index + 1 );
            });
        });
        return false;
    });


</script>

<script>
   $(document).ready(function(){
        $(".submit_blog_form").on('click',function(e){
        var data_attr = $(this).attr('data_attr');
        var sub_url = $(this).attr('submit_url');
        if(data_attr==1)
            {
                $('#myForm').attr('action',sub_url);
                $("#myForm").attr('target', '_blank');
                $('#myForm').submit();
                
            }else{
                
                $('#myForm').attr('action',sub_url);
                $("#myForm").attr('target', '_self');
                $('#myForm').submit();
            }
        });
    });
    
</script>

</head>
<body>

<div class="container_p"> 
    
    <!-- Dashboard Left Side Begins Here -->
    <div class="left_wrp">
        <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
    </div>
    <!-- Dashboard Left Side Ends Here -->
    
    <div class="right-rp"> 
        
        <!-- Top Green Bar Section Begins Here -->
        <?php
        include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
        ?>
        <!-- Top Green Bar Section Ends Here -->
        <div class="mu-contnt-wrp">
            <div class="mu-contnt-hdng">
                <h1>Edit <?= $work_data->work_page_title; ?></h1>
            </div>
            <!-- Bread crumbs starts here -->
            <div class="n-crums">
                <ul>
                    <li> <a href="<?php echo base_url() ?>admin/pages">Content Management</a> </li>
                    <li>
                        <div class="crms-sep">&gt;</div>
                    </li>
                    <li> <a href="<?php echo base_url('admin/pages/sub_page_listing/' . $main_pg->pg_id) ?>"><?= $main_pg->pg_title ?></a></li>
                    <li>
                        <div class="crms-sep">&gt;</div>
                    </li>
                    <li> <a href="javascript:void(0);">Edit <?= $work_data->work_page_title; ?></a></li>
                </ul>
            </div>
            <!-- Bread crumbs ends here --> 
            
            <?php echo form_open_multipart('',array('name'=>'Editwork','id'=>'myForm')); ?> 
            
            <!-- Box along with label section begins here -->
            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Page Details</h2>
                    <p>Manage the content on  How Does it Work page.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Description Title*</label>
                                <!--value="<?php echo set_value('title')!=''?set_value('title'):$home_data->category_title; ?>"-->
                                <input type="text" name="title" value="<?php echo set_value('title')!=''?set_value('title'):$work_data->work_title; ?>"/>
                            </div>
                            <div class="error"> <?php echo form_error('title')?> </div>
                        </div>                        

                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Page Title*</label>
                                <input type="text" name="page_title" value="<?php echo set_value('page_title')!=''?set_value('page_title'):$work_data->work_page_title; ?>"/>
                            </div>
                            <div class="error"> <?php echo form_error('page_title')?> </div>
                        </div>
                        <?php if (empty($work_data->work_banner)) {
                            $path = base_url('resources/page_image/no-banner-small.png');
                        } else {
                            $path = base_url('resources/work_page_images/'.$work_data->work_banner);
                        }
                        ?>                        
                        <!-- <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Banner Title*</label>
                                <input type="text" name="banner_title" value="<?php echo set_value('banner_title')!=''?set_value('banner_title'):$work_data->banner_title; ?>"/>
                            </div>
                        </div>
                        <div class="error"> <?php echo form_error('banner_title')?> </div> -->
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds">
                                <div class="my-form1">
                                    <div class="my-form1">
                                        <label for="box5">Banner Image</label>
                                        <p class="text-box my-form">
                                            <img  src="<?= $path ?>" style="height: 80px;width: 200px;"></img>
                                        </p>
                                        <div class="fileUp">
                                            <input type="file" name="file5" value="" id="box5" onchange="javascript: document.getElementById('box5-txt').value = this.value" />
                                            <label for="box5"></label>
                                            <input type="text" id="box5-txt" name="hidden_file5" value="<?=@$work_data->work_banner; ?>" readonly="true" />
                                            <!-- <input type="hidden" id="box5old-txt" name="hidden_file5old" value="<?=@$work_data->work_banner; ?>" readonly="true" /> -->
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="error"> <?php echo form_error('hidden_file5')?></div>
                        </div>                        
                    </div>
                </div>
            </div>
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Section 1</h2>
                    <p>Manage the section at page.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">                        
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds">
                                <div class="my-form1">
                                    <div class="my-form1">
                                        <label for="box1">Image*</label>
                                        <p class="text-box my-form">
                                            <img  src="<?=base_url()?>resources/work_page_images/thumb/thumb_<?= $work_data->work_image_1; ?>" style="height: 150px;width: 100px;"></img>
                                        </p>
                                        <div class="fileUp">
                                            <input type="file" name="file1" value="" id="box1" onchange="javascript: document.getElementById('box1-txt').value = this.value" />
                                            <label for="box1"></label>
                                            <input type="text" id="box1-txt" name="hidden_file1" value="<?=@$work_data->work_image_1; ?>" readonly="true" />
                                            <!-- <input type="hidden" id="box1old-txt" name="hidden_file1old" value="<?=@$work_data->work_image_1; ?>" readonly="true" /> -->
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="error"> <?php echo form_error('hidden_file1')?> </div>
                        </div>                        

                        <div class="mu-flds-wrp">
                            <label>Description*</label>
                            <textarea  class="myeditor" name="desc1"><?php echo set_value('desc1')!=''?set_value('desc1'):$work_data->work_desc_1; ?></textarea>
                            <div class="error"> <?php echo form_error('desc1')?> </div>                        
                        </div>                        
                    </div>
                </div>
            </div>

            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2> Section 2</h2>
                    <p>Manage the section at page.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">                        
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds">
                                <div class="my-form1">
                                    <div class="my-form1">
                                        <label for="box2">Image*</label>
                                        <p class="text-box my-form">
                                            <img  src="<?=base_url()?>resources/work_page_images/thumb/thumb_<?= $work_data->work_image_2; ?>" style="height: 150px;width: 100px;"></img>
                                        </p>
                                        <div class="fileUp">
                                            <input type="file" name="file2" value="" id="box2" onchange="javascript: document.getElementById('box2-txt').value = this.value" />
                                            <label for="box2"></label>
                                            <input type="text" id="box2-txt" name="hidden_file2" value="<?=@$work_data->work_image_2; ?>" readonly="true" />
                                            <!-- <input type="hidden" id="box2old-txt" name="hidden_file2old" value="<?=@$work_data->work_image_2; ?>" readonly="true" /> -->
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="error"> <?php echo form_error('hidden_file2')?> </div>
                        </div>                        

                        <div class="mu-flds-wrp">
                            <label>Description*</label>
                            <textarea  class="myeditor" name="desc2"><?php echo set_value('desc2')!=''?set_value('desc2'):$work_data->work_desc_2; ?></textarea>
                            <div class="error"> <?php echo form_error('desc2')?></div>
                        </div>                        
                    </div>
                </div>
            </div>

            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Section 3</h2>
                    <p>Manage the section at page.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">                        
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds">
                                <div class="my-form1">
                                    <div class="my-form1">
                                        <label for="box3">Image*</label>
                                        <p class="text-box my-form">
                                            <img  src="<?=base_url()?>resources/work_page_images/thumb/thumb_<?= $work_data->work_image_3; ?>" style="height: 150px;width: 100px;"></img>
                                        </p>
                                        <div class="fileUp">
                                            <input type="file" name="file3" value="" id="box3" onchange="javascript: document.getElementById('box3-txt').value = this.value" />
                                            <label for="box3"></label>
                                            <input type="text" id="box3-txt" name="hidden_file3" value="<?=@$work_data->work_image_3; ?>" readonly="true" />
                                            <!-- <input type="hidden" id="box3old-txt" name="hidden_file3old" value="<?=@$work_data->work_image_3; ?>" readonly="true" /> -->
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="error"> <?php echo form_error('hidden_file3')?> </div>
                        </div>                        

                        <div class="mu-flds-wrp">
                            <label>Description*</label>
                            <textarea  class="myeditor" name="desc3"><?php echo set_value('desc3')!=''?set_value('desc3'):$work_data->work_desc_3; ?></textarea>
                            <div class="error"> <?php echo form_error('desc3')?> </div>                        
                        </div>                        
                    </div>
                </div>
            </div>

            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Section 4</h2>
                    <p>Manage the section at page.</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">                        
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds">
                                <div class="my-form1">
                                    <div class="my-form1">
                                        <label for="box4">Image*</label>
                                        <p class="text-box my-form">
                                            <img  src="<?=base_url()?>resources/work_page_images/thumb/thumb_<?= $work_data->work_image_4; ?>" style="height: 150px;width: 100px;"></img>
                                        </p>
                                        <div class="fileUp">
                                            <input type="file" name="file4" value="" id="box4" onchange="javascript: document.getElementById('box4-txt').value = this.value" />
                                            <label for="box4"></label>
                                            <input type="text" id="box4-txt" name="hidden_file4" value="<?=@$work_data->work_image_4; ?>" readonly="true" />
                                            <!-- <input type="hidden" id="box4old-txt" name="hidden_file4old" value="<?=@$work_data->work_image_4; ?>" readonly="true" /> -->
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="error"> <?php echo form_error('hidden_file4')?></div>
                        </div>                        

                        <div class="mu-flds-wrp">
                            <label>Description*</label>
                            <textarea  class="myeditor" name="desc4"><?php echo set_value('desc4')!=''?set_value('desc4'):$work_data->work_desc_4; ?></textarea>
                            <div class="error"> <?php echo form_error('desc4')?></div>                        
                        </div>                        
                    </div>
                </div>
            </div>

            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Meta Information</h2>
                    <p> Set up the meta description. These help define how this page shows up on search engines. </p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Meta title</label>
                                <input type="text" value="<?php echo set_value('meta_title')!=''?set_value('meta_title'):$work_data->meta_title; ?>" name="meta_title" />
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Meta Keyword</label>
                                <input type="text" value="<?php echo set_value('meta_keywords')!=''?set_value('meta_keywords'):$work_data->meta_keywords; ?>" name="meta_keywords" />
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Meta description </label>
                                <textarea class="text_meta" name="meta_description"><?php echo set_value('meta_description')!=''?set_value('meta_description'):$work_data->meta_desc; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="mu-contnt-outer">
                    <div class="mu-contnt-lfLbl ad-size">
                        <h2>Page Status</h2>
                        <p>Manage your page status.</p>
                    </div>
                    <div class="mu-contntBx-wrp">
                        <div class="contntTop-row">
                            <div class="mu-flds-wrp">
                                <div class="mu-frmFlds_long">
                                    <label>Status*</label>
                                    <select name="work_status">
                                        <option value="">Select</option>
                                        <option value="1" <?php echo set_select( 'work_status', '1', $work_data->work_status == 1 ? true : ''); ?> >Active</option>
                                        <option value="0" <?php echo set_select( 'work_status', '0', $work_data->work_status == 0 ? true : ''); ?> >Inactive</option>
                                    </select>
                                </div>
                                <div class="error">
                                    <?php echo form_error('work_status') ?> </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- <div class="mu-fld-sub">
                <?php if($this->session->userdata('admin_status') == 1 ) { ?>
                <input type="submit" value="Submit" />
                <?php } ?>
                <a href="<?php echo base_url() ?>admin/pages">cancel</a> 
            </div> -->

            <div class="mu-fld-sub">
                    <?php if($this->session->userdata('admin_status') == 1 ) { ?>
                        <a data_attr="0" submit_url="<?=base_url()?>admin/pages/edit_work_page/1" class="submit_blog_form" href="javascript:void(0);" id="asd">Submit</a>
                        <a data_attr="1" submit_url="<?=base_url()?>admin/preview/what_is_educki_preview" class="submit_blog_form" href="javascript:void(0);" id="preview">Preview</a>
                    <a href="<?=base_url()?>admin/pages/sub_page_listing/4">cancel</a>
                    <?php } ?>
                </div>
            
            <!-- Box wihout label section ends here -->
   
            <?php echo form_close(); ?> </div>
    </div>
</div>
</body>
</html>