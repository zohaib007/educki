<?php echo form_open_multipart('', array('name' => 'billing_address', 'id' => 'add_billing_form')); ?>
<input type="hidden" name="userId" id="userId" value="<?=$userid?>" />
<textarea name="shipping_address" id="shipping_address" style="display: none;"></textarea>
<div class="wrapper">
    <div class="crt-wrap">
        <div class="cart-pro-con">
            <div class="cart-left-mian-hed">
                <h2>Shopping Cart</h2>
            </div>
            <div class="billing-payment">
                <div class="billing-edit-bar">
                    <span>Shipping address: <span>Address goes here</span></span><!--  id="shippingAddressEdit" -->
                    <a href="cart-shipping-address.html">Edit</a>
                </div>
                <div class="pay-info">
                    <h3>Payment Information</h3>
                    <div class="cash-method">
                        <input type="radio" id="r01" name="cs">
                        <label class="expand" for="r01"><span></span>
                            <p>PayPal</p>
                        </label>
                    </div>
                    <div class="exp-block">
                        <h5>PayPal is the safer, easir way to pay</h5>
                        <p> No matter where you shop, we keep your financial information secure
                        </p>
                        <img src="images/paypal-sheild.png" alt="" style="float:left">
                        <div class="paypal-img">
                            <a href="javascript:void"><img src="images/paypal-img.png" alt=""></a>
                            <p>The safer, easier way to pay</p>
                        </div>
                    </div>
                </div>
                <div class="bill-address">
                    <h3>Billing Address</h3>
                    <?php if(!empty($default_billing_address)){?>
                    <div class="cash-method">
                        <input type="radio" id="r02" name="cc" value="1" <?=($active==1)?'checked':''?>>
                        <label for="r02" class="expand2 useDefault" data-close="1"><span></span>
                            <p>Use Default</p>
                        </label>
                    </div>
                    <?php }?>
                    <div class="cash-method">
                        <input type="radio" id="r03" name="cc" value="2" <?=($active==2)?'checked':''?>>
                        <label for="r03" class="expand2 sameAsShippimg" data-close="1"><span></span>
                            <p>Same as Shipping Address</p>
                        </label>
                    </div>
                    <div class="cash-method">
                        <input type="radio" id="r04" name="cc" value="3" <?=($active==3)?'checked':''?>>
                        <label for="r04" class="expand2 differentBilling" data-close="0"><span></span>
                            <p>Use a Different Billing Address</p>
                        </label>
                    </div>

                    <div class="exp-block2" <?=($active==3)?'style="display: block;"':'style="display: none;"'?>>
                        <div class="crt-shp-addr-main">
                            <div class="crt-shp-addr-frm">
                                <div class="crt-shp-addr-frm-rw">
                                    <select name="select_address" id="select_address">
                                        <option value="">Select</option>
                                        <?php foreach ($address_list as $list) { ?>
                                            <option value="<?= $list->add_id ?>"><?=($default_address_list->nick_name == $list->nick_name) ? "Default Shipping Address" : $list->nick_name?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="crt-shp-addr-frm-rw">
                                    <input type="text"  name="nick_name" id="nick_name" value="<?=@$current->billing_nick_name?>" placeholder="Nickname *" />   
                                </div>
                                <div class="crt-shp-addr-frm-rw">
                                    <div class="crt-shp-addr-frm-inr mrg-rgt-0">
                                    <input type="text" name="first_name" id="first_name" value="<?=@$current->billing_first_name?>" placeholder="First Name *" />
                                    </div>
                                </div>
                                <div class="crt-shp-addr-frm-rw">
                                    <div class="crt-shp-addr-frm-inr">
                                    <input type="text" name="last_name" id="last_name" value="<?=@$current->billing_last_name?>" placeholder="Last Name *" />   
                                    </div>
                                </div>

                                <div class="crt-shp-addr-frm-rw">
                                    <div class="crt-shp-addr-frm-inr">
                                        <select  name="country" id="country">
                                            <option value="" >Country *</option>
                                            <?php foreach ($allcountries as $country) { ?>
                                            <option value="<?= $country['iso'] ?>"<?php echo (@$current->billing_country =='' && $country['iso']=='US') ? 'selected' : ""; ?> <?php echo (@$current->billing_country == $country['iso']) ? 'selected' : ""; ?>><?= $country['name'] ?></option>
                                                        <?php } ?>
                                        </select>
                                    </div>
                                </div>



                                <div class="crt-shp-addr-frm-rw usState">
                                    <div class="crt-shp-addr-frm-inr">
                                        <select  name="state" id="state" >
                                            <option value="">State *</option>
                                            <?php foreach ($allstate as $state) { ?>
                                            <option value="<?= $state['stat_id'] ?>" <?php echo (@$current->billing_state == $state['stat_id']) ? 'selected' : ""; ?>><?= $state['stat_name'] ?></option>
                                                            
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="crt-shp-addr-frm-rw otherState" >
                                    <div class="crt-shp-addr-frm-inr mrg-rgt-0">
                                        <input type="text" id="other_state" name="other_state" value="<?=@$current->billing_state_other?>" placeholder="State *" />
                                    </div>
                                </div>

                                <div class="crt-shp-addr-frm-rw">
                                    <div class="crt-shp-addr-frm-inr">
                                        <input type="text" name="street" id="street" value="<?=@$current->billing_street?>" placeholder="Street *" />
                                    </div>
                                </div>
                                <div class="crt-shp-addr-frm-rw">
                                    <div class="crt-shp-addr-frm-inr mrg-rgt-0">
                                        <input type="text" name="city" id="city" value="<?=@$current->billing_city?>" placeholder="City *" />
                                    </div>
                                </div>
                                <div class="crt-shp-addr-frm-rw">
                                    <div class="crt-shp-addr-frm-inr">
                                        <input type="text" name="zip_code" id="zip_code" value="<?=@$current->billing_zip?>" placeholder="Zip *" maxlength="11" />
                                    </div>
                                </div>
                                <div class="crt-shp-addr-frm-rw">
                                    <div class="crt-shp-addr-frm-inr mrg-rgt-0">
                                        <input type="text" name="phone" id="phone" value="<?=@$current->billing_phone?>" placeholder="Telephone" />
                                    </div>
                                </div>
                                <!-- <div class="sign-in-btns">
                                    <input class="submit" type="submit" value="Next" />
                                </div> -->
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
    </div>
</div>
<div class="crt-order-detail">
    <div class="wrapper">
        <div class="cart-right-main">
            <div class="boxgrey-rp"></div>
            <div class="cart-right-hed">
                <h6>Order Details</h6>
            </div>
            <div class="cart-right-lowr">
                <div class="cart-right-rw">
                    <div class="cart-right-rw-lft">
                        Subtotal
                    </div>
                    <div class="cart-right-rw-rgt">
                        $<?= number_format($cart_sub_total, 2, '.', '') ?>
                    </div>
                </div>
                <div class="cart-right-rw">
                    <div class="cart-right-rw-lft">
                        Discount
                    </div>
                    <div class="cart-right-rw-rgt">
                        $<?= number_format($discount_amount, 2, '.', '') ?>
                    </div>
                </div>
                <div class="cart-right-rw">
                    <div class="cart-right-rw-lft">
                        Shipping Total
                    </div>
                    <div class="cart-right-rw-rgt">
                        $<?= number_format($shipment_sub_total, 2, '.', '') ?>
                    </div>
                </div>
            </div>
            <div class="cart-right-rw">
                <div class="cart-right-rw-lft">
                    <h6>Total</h6>
                </div>
                <div class="cart-right-rw-rgt">
                    <h6>$<?= number_format($total_shipment_amount, 2, '.', '') ?></h6>
                </div>
            </div>
            <div class="cart-right-lowr2 center-block">
                <input class="prcd-btn submit" type="submit" value="Next">
            </div>
            <!--<div class="inpt-tp-no">
                        <input type="number" value="1">
                        <a href="#">Remove</a>
                    </div>-->
        </div>
    </div>
</div>
<?php echo form_close(); ?>