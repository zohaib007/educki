
<div class="wrapper">
    <div class="crt-wrap">
        <div class="cart-pro-con">
            <div class="cart-left-mian-hed">
                <h2>Shopping Cart</h2>
            </div>
            <div class="crt-order-main">
            	<div class="crt-order-main-hed">
                	<h3>Your Shopping Cart</h3>
                    <a href="cart.html">Edit</a>
                </div>
                
                <div class="crt-order-frst">

                	
                    <?php
                    foreach ($cart_product_details as $product) {
                        $product_details = getproduct_details($product->cart_prod_id);
                        if ($product_details != '') {
                            ?>
                            <div class="crt-order-frst-rw" id="row_<?= $product_details->prod_id ?>">

                                <div class="crt-order-frst-bx">
                                    <img src="<?= base_url() ?>resources/prod_images/<?= $product_details->img_name ?>" alt="" title="" class="img-responsive"/>
                                </div>

                                <div class="crt-order-frst-bx2">
                                    <div class="crt-order-frst-bx2-rw">
                                        <div class="crt-order-frst-bx2-rw-left">
                                            <h5>Title:</h5>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw-right">
                                            <p><?= $product_details->prod_title ?></p>
                                        </div>
                                    </div>

                                    <div class="crt-order-frst-bx2-rw">
                                        <div class="crt-order-frst-bx2-rw-left">
                                            <h5>Store Name:</h5>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw-right">
                                            <p><?= $product_details->store_name ?></p>
                                        </div>
                                    </div>

                                    <div class="crt-order-frst-bx2-rw">
                                        <div class="crt-order-frst-bx2-rw-left">
                                            <h5>Price:</h5>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw-right">
                                            <p>$<?= number_format($product->unit_prod_price, 2, '.', '') ?></p>
                                        </div>
                                    </div>

                                    <div class="crt-order-frst-bx2-rw">
                                        <div class="crt-order-frst-bx2-rw-left">
                                            <h5>Qty:</h5>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw-right">
                                            <p><?= $product->cart_prod_qnty ?></p>
                                        </div>
                                    </div>

                                    <div class="crt-order-frst-bx2-rw">
                                        <div class="crt-order-frst-bx2-rw-left">
                                            <h5>Shipping Type:</h5>
                                        </div>

                                        <div class="crt-order-frst-bx2-rw-right">
                                            <span><?= $product_details->shipping?><?=(!empty($product_details->prod_shipping_methods))?' ('.$product_details->prod_shipping_methods.')':''?></span>
                                        </div>
                                    </div>
                                    <?php if ($product_details->shipping == 'Offer free Shipping') { ?>
                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>Ready to ship in:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p><?= $product_details->free_ship_days ?> days.</p>
                                            </div>
                                        </div>
                                    <?php } elseif ($product_details->shipping == 'Charge for Shipping') { ?>
                                        <div class="crt-order-frst-bx2-rw">
                                            <div class="crt-order-frst-bx2-rw-left">
                                                <h5>Ready to ship in:</h5>
                                            </div>

                                            <div class="crt-order-frst-bx2-rw-right">
                                                <p><?= $product_details->ship_days ?> days.</p>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php
                                    $filters = json_decode($product->cart_details);
                                    if ($filters) {
                                        foreach ($filters as $filter) {
                                            ?>
                                            <div class="crt-order-frst-bx2-rw">
                                                <div class="crt-order-frst-bx2-rw-left">
                                                    <h5><?= $filter->filterName ?>:</h5>
                                                </div>

                                                <div class="crt-order-frst-bx2-rw-right">
                                                    <p><?= $filter->filtervalue ?></p>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                
                </div>
                
                <div class="crt-order-secnd">
                	<div class="crt-order-secnd-left">
                    	
                        <div class="crt-order-secnd-left-hed">
                        	<h4>Shipping</h4>
                        	<a href="cart-shipping-address.html">Edit</a>
                        </div>
                        <div class="crt-order-secnd-left-btm">
                            <div class="crt-order-frst-bx2-rw">
                                <div class="crt-order-frst-bx2-rw-left">
                                    <h5>Nickname:</h5>
                                </div>

                                <div class="crt-order-frst-bx2-rw-right">
                                    <p><?= $default_shipping_address->shipping_nick_name ?></p>
                                </div>
                            </div>

                            <div class="crt-order-frst-bx2-rw">
                                <div class="crt-order-frst-bx2-rw-left">
                                    <h5>First Name:</h5>
                                </div>

                                <div class="crt-order-frst-bx2-rw-right">
                                    <p><?= $default_shipping_address->shipping_first_name ?></p>
                                </div>
                            </div>

                            <div class="crt-order-frst-bx2-rw">
                                <div class="crt-order-frst-bx2-rw-left">
                                    <h5>Last Name:</h5>
                                </div>

                                <div class="crt-order-frst-bx2-rw-right">
                                    <p><?= $default_shipping_address->shipping_last_name ?></p>
                                </div>
                            </div>

                            <div class="crt-order-frst-bx2-rw">
                                <div class="crt-order-frst-bx2-rw-left">
                                    <h5>Telephone:</h5>
                                </div>

                                <div class="crt-order-frst-bx2-rw-right">
                                    <p><?= (!empty($default_shipping_address->shipping_phone))?$default_shipping_address->shipping_phone:"N/A" ?></p>
                                </div>
                            </div>                                        

                            <div class="crt-order-frst-bx2-rw">
                                <div class="crt-order-frst-bx2-rw-left">
                                    <h5>Street:</h5>
                                </div>

                                <div class="crt-order-frst-bx2-rw-right">
                                    <p><?= $default_shipping_address->shipping_street ?></p>
                                </div>
                            </div>

                            <div class="crt-order-frst-bx2-rw">
                                <div class="crt-order-frst-bx2-rw-left">
                                    <h5>City:</h5>
                                </div>

                                <div class="crt-order-frst-bx2-rw-right">
                                    <p><?= $default_shipping_address->shipping_city ?></p>
                                </div>
                            </div>

                            <div class="crt-order-frst-bx2-rw">
                                <div class="crt-order-frst-bx2-rw-left">
                                    <h5>State:</h5>
                                </div>

                                <div class="crt-order-frst-bx2-rw-right">
                                    <p>
                                        <?php
                                        if ($default_shipping_address->shipping_country == 'US') {
                                            echo get_state_name($default_shipping_address->shipping_state);
                                        } else {
                                            echo $default_shipping_address->shipping_state_other;
                                        }
                                        ?>
                                    </p>
                                </div>
                            </div>

                            <div class="crt-order-frst-bx2-rw">
                                <div class="crt-order-frst-bx2-rw-left">
                                    <h5>Country:</h5>
                                </div>

                                <div class="crt-order-frst-bx2-rw-right">
                                    <p><?= get_country_name($default_shipping_address->shipping_country); ?></p>
                                </div>
                            </div>

                            <div class="crt-order-frst-bx2-rw">
                                <div class="crt-order-frst-bx2-rw-left">
                                    <h5>Zip:</h5>
                                </div>

                                <div class="crt-order-frst-bx2-rw-right">
                                    <p><?= $default_shipping_address->shipping_zip ?></p>
                                </div>
                            </div>                                        

                        </div>
                        
                    </div>
                    
                    <div class="crt-order-secnd-left">
                    	
                        <div class="crt-order-secnd-left-hed">
                        	<h4>Billing</h4>
                        	<a href="cart-billing-payment.html">Edit</a>
                        </div>
                        <div class="crt-order-secnd-left-btm">

                            <div class="crt-order-frst-bx2-rw">
                                <div class="crt-order-frst-bx2-rw-left">
                                    <h5>Nickname:</h5>
                                </div>

                                <div class="crt-order-frst-bx2-rw-right">
                                    <p><?= $default_billing_address->billing_nick_name ?></p>
                                </div>
                            </div>

                            <div class="crt-order-frst-bx2-rw">
                                <div class="crt-order-frst-bx2-rw-left">
                                    <h5>First Name:</h5>
                                </div>

                                <div class="crt-order-frst-bx2-rw-right">
                                    <p><?= $default_billing_address->billing_first_name ?></p>
                                </div>
                            </div>

                            <div class="crt-order-frst-bx2-rw">
                                <div class="crt-order-frst-bx2-rw-left">
                                    <h5>Last Name:</h5>
                                </div>

                                <div class="crt-order-frst-bx2-rw-right">
                                    <p><?= $default_billing_address->billing_last_name ?></p>
                                </div>
                            </div>

                            <div class="crt-order-frst-bx2-rw">
                                <div class="crt-order-frst-bx2-rw-left">
                                    <h5>Telephone:</h5>
                                </div>

                                <div class="crt-order-frst-bx2-rw-right">
                                    <p><?= (!empty($default_billing_address->billing_phone))?$default_billing_address->billing_phone:"N/A" ?></p>
                                </div>
                            </div>

                            <div class="crt-order-frst-bx2-rw">
                                <div class="crt-order-frst-bx2-rw-left">
                                    <h5>Street:</h5>
                                </div>

                                <div class="crt-order-frst-bx2-rw-right">
                                    <p><?= $default_billing_address->billing_street ?></p>
                                </div>
                            </div>

                            <div class="crt-order-frst-bx2-rw">
                                <div class="crt-order-frst-bx2-rw-left">
                                    <h5>City:</h5>
                                </div>

                                <div class="crt-order-frst-bx2-rw-right">
                                    <p><?= $default_billing_address->billing_city ?></p>
                                </div>
                            </div>

                            <div class="crt-order-frst-bx2-rw">
                                <div class="crt-order-frst-bx2-rw-left">
                                    <h5>State:</h5>
                                </div>

                                <div class="crt-order-frst-bx2-rw-right">
                                    <p>
                                        <?php
                                        if ($default_billing_address->billing_country == 'US') {
                                            echo get_state_name($default_billing_address->billing_state);
                                        } else {
                                            echo $default_billing_address->billing_state_other;
                                        }
                                        ?>
                                    </p>
                                </div>
                            </div>

                            <div class="crt-order-frst-bx2-rw">
                                <div class="crt-order-frst-bx2-rw-left">
                                    <h5>Country:</h5>
                                </div>

                                <div class="crt-order-frst-bx2-rw-right">
                                    <p><?= get_country_name($default_billing_address->billing_country) ?></p>
                                </div>
                            </div>

                            <div class="crt-order-frst-bx2-rw">
                                <div class="crt-order-frst-bx2-rw-left">
                                    <h5>Zip:</h5>
                                </div>

                                <div class="crt-order-frst-bx2-rw-right">
                                    <p><?= $default_billing_address->billing_zip ?></p>
                                </div>
                            </div>
                                                                

                        </div>
                        
                    </div>
                
                </div>
                
                 <div class="crt-order-thrd">
                    
                    <!-- <div class="crt-order-secnd-left-hed">
                        <h4>Payment Information</h4>
                        <a href="07-4-a-cart-billing-and-payment.php">Edit</a>
                    </div> -->
                    <div class="crt-order-secnd-left">
                    	<div class="crt-order-secnd-left-btm">
                        	<!-- <div class="crt-order-frst-bx2-rw">
                            	<div class="crt-order-frst-bx2-rw-left">
                                	<h5>Name on Card:</h5>
                                </div>
                                
                                <div class="crt-order-frst-bx2-rw-right">
                                	<p>Visa</p>
                                </div>
                            </div>
                            
                            <div class="crt-order-frst-bx2-rw">
                            	<div class="crt-order-frst-bx2-rw-left">
                                	<h5>Card Number:</h5>
                                </div>
                                
                                <div class="crt-order-frst-bx2-rw-right">
                                	<p>123456789</p>
                                </div>
                            </div>
                            
                            <div class="crt-order-frst-bx2-rw">
                            	<div class="crt-order-frst-bx2-rw-left">
                                	<h5>Expiration Date:</h5>
                                </div>
                                
                                <div class="crt-order-frst-bx2-rw-right">
                                	<p>23-10-2017</p>
                                </div>
                            </div>
                            
                            <div class="crt-order-frst-bx2-rw">
                            	<div class="crt-order-frst-bx2-rw-left">
                                	<h5>Security Code:</h5>
                                </div>
                                
                                <div class="crt-order-frst-bx2-rw-right">
                                	<p>12345</p>
                                </div>
                            </div> -->
                            
                            <!-- <div class="crt-order-frst-bx2-rw">
                            	<div class="sign-in-btns">
                                	
                                </div>
                               
                               
                            </div> -->
                    	</div>
                    </div>
                     
                 </div>
            
            </div>
            
        </div>
    </div>
</div>
<div class="crt-order-detail">
	<div class="wrapper">
    	
        <div class="cart-right-main">
            <div class="boxgrey-rp"></div>
            <div class="cart-right-hed">
                <h6>Order Details</h6>
            </div>
            <div class="cart-right-lowr">
                <div class="cart-right-rw">
                    <div class="cart-right-rw-lft">
                        Subtotal
                    </div>
                    <div class="cart-right-rw-rgt">
                        $<?= number_format($cart_sub_total, 2, '.', '') ?>
                    </div>
                </div>
                
                <div class="cart-right-rw">
                    <div class="cart-right-rw-lft">
                        Discount
                    </div>
                    <div class="cart-right-rw-rgt">
                        <?= ($discount_amount != '') ? "$" . $discount_amount : "$0.00" ?>
                    </div>
                </div>
                
                <div class="cart-right-rw">
                    <div class="cart-right-rw-lft">
                        Shipping Total
                    </div>
                    <div class="cart-right-rw-rgt">
                        $<?= number_format($shipment_sub_total, 2, '.', '') ?>
                    </div>
                </div>
            </div>
            <div class="cart-right-rw">
                <div class="cart-right-rw-lft">
                    <h6>Total</h6>
                </div>
                <div class="cart-right-rw-rgt">
                    <h6>$<?= number_format($total_shipment_amount, 2, '.', '') ?></h6>
                </div>
            </div>
            <div class="cart-right-lowr2 center-block">
                <a href="javascript:void(0);" data-id="<?=$paypalInfo['paypal_order_id']?>" data-msg="<?=$paypalInfo['message']?>" data-url="<?=$paypalInfo['payPalURL']?>" class="prcd-btn cart-confirm">CONFIRM ORDER</a>
            </div>   
        </div>
    	
    </div>
</div>
    