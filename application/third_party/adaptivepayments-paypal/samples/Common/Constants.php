<?php
$config_class = new Configuration();
$get_config = $config_class->getConfig();


if($get_config['mode']=='sandbox')
{
	define('PAYPAL_REDIRECT_URL', 'https://www.sandbox.paypal.com/webscr&cmd=');
	define('DEVELOPER_PORTAL', 'https://developer.paypal.com');
}
else
{
	define('PAYPAL_REDIRECT_URL', 'https://www.paypal.com/cgi-bin/webscr&cmd=');
	define('DEVELOPER_PORTAL', 'https://developer.paypal.com');
}