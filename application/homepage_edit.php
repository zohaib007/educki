<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit Homepage</title>
<link rel="stylesheet" href="<?= base_url() ?>css/new.css" type="text/css" media="all" />
<?php include(ADMIN_INCLUDE_PATH."includes/js.php"); ?>
<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" />
<script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
<!-- tinymce configration start here.-->
<script>
var base_url='<?php echo base_url();?>';
var ser='<?php echo $_SERVER['DOCUMENT_ROOT'];?>educki/resources/tiny_upload';
</script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.js"></script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
<script src="<?php echo base_url(); ?>tiny/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>tiny/common.js"></script>
<!-- tinymce configration end here.-->

<script>
    function delete_gall_image(sel){
        //  alert(sel);
        $(".text-box").hide();
        $(".remove-box").hide();
        $("input[name='image']").val('');
        $.ajax({
            url:  base_url+'admin/pages/delete_page_img',
            type: 'POST',
            data: {nId: sel, <?=$this->security->get_csrf_token_name()?>: '<?=$this->security->get_csrf_hash()?>'},
        }).done(function() {
            console.log("success");
        }).fail(function() {
            console.log("error");
        }).always(function() {
            console.log("complete");
        });
    }
			 
    $('.my-form').on('click', '.remove-box', function(){
       
        $(this).parent().fadeOut("slow", function() {
            $(this).remove();
            $('.box-number').each(function(index){
                $(this).text( index + 1 );
            });
        });
        return false;
    });


</script>
</head>
<body>

<div class="container_p"> 
    
    <!-- Dashboard Left Side Begins Here -->
    <div class="left_wrp">
        <?php include(ADMIN_INCLUDE_PATH."includes/dash-left.php"); ?>
    </div>
    <!-- Dashboard Left Side Ends Here -->
    
    <div class="right-rp"> 
        
        <!-- Top Green Bar Section Begins Here -->
        <?php
        include(ADMIN_INCLUDE_PATH."includes/top_green_bar.php");
        ?>
        <!-- Top Green Bar Section Ends Here -->
        <div class="mu-contnt-wrp">
            <div class="mu-contnt-hdng">
                <h1>Edit Homepage</h1>
            </div>
            <!-- Bread crumbs starts here -->
            <div class="n-crums">
                <ul>
                    <li> <a href="<?php echo base_url() ?>admin/pages">Content Management</a> </li>
                    <li>
                        <div class="crms-sep">&gt;</div>
                    </li>
                    <li> <a href="javascript:void(0);"> Edit Homepage </a> </li>
                </ul>
            </div>
            <!-- Bread crumbs ends here --> 
            
            <?php echo form_open_multipart('',array('name'=>'EditHomePage')); ?> 
            
            <!-- Box along with label section begins here -->
            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Page Details</h2>
                    <p>Manage the content on homepage.</p>
                    <!-- <?php echo validation_errors();?> -->
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Category Title*</label>
                                <input type="text" value="<?php echo set_value('category_title')!=''?set_value('category_title'):$home_data->category_title; ?>" name="category_title" />
                            </div>
                        </div>
                        <div class="error"> <?php echo form_error('category_title')?> </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Featured Sellers Title*</label>
                                <input type="text" value="<?php echo set_value('feaSeller')!=''?set_value('feaSeller'):$home_data->feaSeller; ?>" name="feaSeller" />
                            </div>
                        </div>
                        <div class="error"> <?php echo form_error('feaSeller')?> </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Search Items Title*</label>
                                <input type="text" value="<?php echo set_value('searchtitle')!=''?set_value('searchtitle'):$home_data->searchtitle; ?>" name="searchtitle" />
                            </div>
                        </div>
                        <div class="error"> <?php echo form_error('searchtitle')?> </div>

                        <div class="mu-flds-wrp">
                            <label class="lone-set">Search Items Description* </label>
                            <textarea class="myeditor" name="searchtext" id="desc" ><?php echo set_value('searchtext')!=''?set_value('searchtext'):$home_data->searchtext; ?></textarea>
                        </div>
                        <div class="error"> <?php echo form_error('searchtext')?> </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Search Items Button Text*</label>
                                <input type="text" value="<?php echo set_value('searchButton')!=''?set_value('searchButton'):$home_data->searchButton; ?>" name="searchButton" />
                            </div>
                        </div>
                        <div class="error"> <?php echo form_error('searchButton')?> </div>
                    </div>
                </div>
            </div>
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Get Started</h2>
                    <p>Getting Started is as Easy as 1-2-3</p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">

                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Getting Start Title*</label>
                                <input type="text" value="<?php echo set_value('gettingStartTitle')!=''?set_value('gettingStartTitle'):$home_data->gettingStartTitle; ?>" name="gettingStartTitle" />
                            </div>
                        </div>
                        <div class="error"> <?php echo form_error('gettingStartTitle')?> </div>
                        
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds">
                                <div class="my-form1">
                                    <div class="my-form1">
                                        <label for="box1">Image 1*</label>
                                        <p class="text-box my-form">
                                            <img  src="<?= base_url() ?>/resources/home_image/thumb/thumb_<?=$home_data->start_image1; ?>"></img>
                                        </p>
                                        <div class="fileUp">
                                            <input type="file" name="file1" value="" id="box1" onchange="javascript: document.getElementById('box1-txt').value = this.value" />
                                            <label for="box1"></label>
                                            <input type="text" id="box1-txt" name="hidden_file1" value="<?=@$home_data->start_image1; ?>" readonly="true" />
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="error"> <?php echo form_error('hidden_file1')?> </div>

                        <div class="mu-flds-wrp">
                            <label>Description 1*</label>
                            <textarea  class="myeditor" name="startTitle1"><?php echo set_value('startTitle1')!=''?set_value('startTitle1'):$home_data->startTitle1; ?></textarea>
                        
                        </div>
                        <div class="error"> <?php echo form_error('startTitle1')?> </div>
                        
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds">
                                <div class="my-form1">
                                    <div class="my-form1">
                                        <label for="box1">Image 2*</label>
                                        <p class="text-box my-form">
                                            <img  src="<?= base_url() ?>/resources/home_image/thumb/thumb_<?=$home_data->start_image2; ?>"></img>
                                        </p>
                                        <div class="fileUp">
                                            <input type="file" name="file2" value="" id="box2" onchange="javascript: document.getElementById('box2-txt').value = this.value" />
                                            <label for="box2"></label>
                                            <input type="text" id="box2-txt" name="hidden_file2" value="<?=@$home_data->start_image2; ?>" readonly="true" />
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="error"> <?php echo form_error('hidden_file2')?> </div>

                        <div class="mu-flds-wrp">
                            
                            <label class="lone-set">Description 2*</label>
                            <textarea class="myeditor" name="startTitle2"><?php echo set_value('startTitle2')!=''?set_value('startTitle2'):$home_data->startTitle2; ?></textarea>
                        
                        </div>
                        <div class="error"> <?php echo form_error('startTitle2')?> </div>

                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds">
                                <div class="my-form1">
                                    <div class="my-form1">
                                        <label for="box1">Image 3*</label>
                                        <p class="text-box my-form">
                                            <img  src="<?= base_url() ?>/resources/home_image/thumb/thumb_<?=$home_data->start_image3; ?>"></img>
                                        </p>
                                        <div class="fileUp">
                                            <input type="file" name="file3" value="" id="box3" onchange="javascript: document.getElementById('box3-txt').value = this.value" />
                                            <label for="box3"></label>
                                            <input type="text" id="box3-txt" name="hidden_file3" value="<?=@$home_data->start_image3; ?>" readonly="true" />
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="error"> <?php echo form_error('hidden_file2')?> </div>

                        <div class="mu-flds-wrp">
                            
                            <label class="lone-set">Description 3*</label>
                            <textarea class="myeditor" name="startTitle3"><?php echo set_value('startTitle3')!=''?set_value('startTitle3'):$home_data->startTitle3; ?></textarea>
                        
                        </div>
                        <div class="error"> <?php echo form_error('startTitle3')?> </div>

                    </div>
                </div>
            </div>
            
            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Member Title</h2>
                    <p>Manage the member title on homepage.</p>
                    
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Member Title*</label>
                                <input type="text" value="<?php echo set_value('membertitle')!=''?set_value('membertitle'):$home_data->membertitle; ?>" name="membertitle" />
                            </div>
                        </div>
                        <div class="error"> <?php echo form_error('membertitle')?> </div>

                    </div>
                </div>
            </div>


            <div class="mu-contnt-outer">
                <div class="mu-contnt-lfLbl ad-size">
                    <h2>Meta Information</h2>
                    <p> Set up the meta description. These help define how this page shows up on search engines. </p>
                </div>
                <div class="mu-contntBx-wrp">
                    <div class="contntTop-row">
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Meta title</label>
                                <input type="text" value="<?php echo set_value('meta_title')!=''?set_value('meta_title'):$home_data->meta_title; ?>" name="meta_title" />
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Meta Keyword</label>
                                <input type="text" value="<?php echo set_value('meta_keywords')!=''?set_value('meta_keywords'):$home_data->meta_keywords; ?>" name="meta_keywords" />
                            </div>
                        </div>
                        <div class="mu-flds-wrp">
                            <div class="mu-frmFlds_long">
                                <label>Meta description </label>
                                <textarea class="text_meta" name="meta_description"><?php echo set_value('meta_description')!=''?set_value('meta_description'):$home_data->meta_description; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            

            <div class="mu-fld-sub">
                <?php if($this->session->userdata('admin_status') == 1 ) { ?>
                <input type="submit" value="Submit" />
                <?php } ?>
                <a href="<?php echo base_url() ?>admin/pages">cancel</a> </div>
            
            <!-- Box wihout label section ends here -->
   
            <?php echo form_close(); ?> </div>
    </div>
</div>
</body>
</html>