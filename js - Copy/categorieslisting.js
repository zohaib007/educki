 $(function() {
	  
	// Sortable Listing JS Starts 
	$(".sortable").sortable({
		revert: true
	});
	$(".sortable").disableSelection();
	// Sortable Listing JS Ends 
	
	// show and hide element starts
	$('.ul-ico-sub a').click(function() {
		var hiddenElement = $(this).parent().parent().parent().parent().find('.ul-lisub-list');
		var anchorElement = $(this);
		if(hiddenElement.css('display') == 'none'){
			hiddenElement.slideDown();
			anchorElement.addClass('active');
			setTimeout(function() { $('.sort-wrap').css('min-height', $('.ul-libtm').height()); }, 400);
		} else {
			hiddenElement.slideUp();
			anchorElement.removeClass('active');
			$('.sort-wrap').css('min-height', defaultHeight);
			setTimeout(function() { $('.sort-wrap').css('min-height', $('.ul-libtm').height()); }, 400);
		}
		return false;
	});
	// show and hide element ends
	
	//alert($('.ul-libtm').height());
	var defaultHeight = $('.ul-libtm').height();
	$('.sort-wrap').css('min-height', $('.ul-libtm').height());
	
  });