/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	config.toolbarGroups = [
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing', groups: [ 'selection', 'spellchecker', 'find', 'editing' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'paragraph', groups: [ 'list', 'align', 'indent', 'blocks', 'bidi', 'paragraph' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		'/',
		'/',
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];

	config.removeButtons = 'Save,NewPage,Preview,Print,Templates,Find,Replace,Form,TextField,Textarea,Select,Button,ImageButton,HiddenField,Outdent,Indent,Blockquote,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Flash,Smiley,PageBreak,Iframe,TextColor,BGColor,ShowBlocks,About,Radio,Checkbox';
	
	/*config.extraPlugins = 'notification';
	config.extraPlugins = 'lineutils';
	config.extraPlugins = 'notificationaggregator';
	config.extraPlugins = 'widget';
	config.extraPlugins = 'filetools';
	config.extraPlugins = 'uploadwidget';
	config.extraPlugins = 'uploadimage';*/
	//config.imageUploadUrl = '/uploader/upload.php?type=Images';
	
	config.extraPlugins = 'imageuploader';
	//config.imageUploadUrl = '/uploader/upload.php?type=Images';
	//config.filebrowserUploadUrl = "/VirtualDirectoryName/ControllerName/ActionName";
};