<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Oswald:700' rel='stylesheet' type='text/css'>

<link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo base_url(); ?>images/favicon.ico" type="image/x-icon">

<script src="<?php echo base_url(); ?>js/jquery-1.10.1.min.js"></script>

<script language="javascript" type="text/javascript">

onscroll = function() {
	var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
	if (scrollTop <= $('#header').height()) {
		
		if($('#header').hasClass('fixed')){
			$('#header').removeClass('fixed');
		}
		if($('#header').hasClass('sticky')){
			$('#header').removeClass('sticky');
		}
		
		$('#header').css('top','0px');
		$('#header-cover').css('display','none');
		
	} else if(scrollTop > $('#header').height()){
		//alert(scrollTop);
		$('#header').addClass('fixed');
		$('#header-cover').css('display','block');
		//$('#header-cover').css('height',($('#header').height()) + 'px');
		$('#header').css('top', '-' + ($('#header').height()) + 'px');
	}
	
	if(scrollTop > $('#header').height()+50){
		$('#header').addClass('sticky');
		//$('#header-cover').css('display','block');
	}
	
	
};


// For Min height to body DIV
$(window).ready(function() {
	
	/* Header Cover Height */
	$('#header-cover').css('height',($('#header').height()) + 'px');
	/* Header Cover Height */
	
	
	/* Min Height for body main cover */
	if(document.getElementById("header")){
    	var headerHeight = ($("#header").outerHeight() );
	} else{
		var headerHeight = 0;
	}
	//alert(headerHeight);
	if(document.getElementById("footer")){
    	var footerHeight = ($("#footer").outerHeight() );
	} else{
		var footerHeight = 0;
	}
	//alert(footerHeight);
	if(document.getElementById("bodyHeight")){
		var bodyminheight =  $(window).height() - (headerHeight + footerHeight);
		//alert(bodyminheight);
		document.getElementById("bodyHeight").style.minHeight = bodyminheight + "px" ;
	}
	/* Min Height for body main cover */
	
	
	/* Cart Link in top Dropdown Scroll Setting */
	if(document.getElementById("cdropScroll")){
		//442
		if($("#cdropScroll").outerHeight() > 450){
			$('#cdropScroll').css('overflow','auto');
			$('#cdropScroll').css('height','442px');
		}
	}
	/* Cart Link in top Dropdown Scroll Setting */
	
	
	/* For Making Height Equal in About Page */
	if($('.tc-ibin').length){
		$.fn.setAllToMaxHeight = function(){
			return this.height( Math.max.apply(this, $.map( this , function(e){ return $(e).height() }) ) );
		}
		$('.tc-ibin').setAllToMaxHeight();
	}
	/* For Making Height Equal in About Page */
	
	
	/* For Making Height Equal in About Page */
	if($('.mheight').length){
		$.fn.setAllToMaxHeight = function(){
			return this.height( Math.max.apply(this, $.map( this , function(e){ return $(e).height() }) ) );
		}
		$('.mheight').setAllToMaxHeight();
	}
	/* For Making Height Equal in About Page */
	
	
	/* For Making Height Equal in Fourth Section Home Page */
	if($('.sl-left').length){
		if($('.sec-last-img').length){
			if($('.sl-left').height() > $('.sec-last-img').height()){
				if($('.sec-last-grow').length){
					$('.sec-last-grow').css('min-height',$('.sl-left').height() + 'px');
				}
			}
		}
	}
	/* For Making Height Equal in Fourth Section Home Page */
	
	
	
	/* Image Height Assign to Div Height For Blog */
	if($('.imgHeight').length){
		if($('.subwrap').length){
			$('.subwrap').css('min-height',($('.imgHeight').height()-100) + 'px');	
		}
	}
	/* Image Height Assign to Div Height For Blog */
	
	
	/* For Making Height Equal in Product Detail Page */
	if($('.sug-proname').length){
		$.fn.setAllToMaxHeight = function(){
			return this.height( Math.max.apply(this, $.map( this , function(e){ return $(e).height() }) ) );
		}
		$('.sug-proname').setAllToMaxHeight();
	}
	/* For Making Height Equal in Product Detail Page */
	
});

$(window).resize(function() {
	
	/* For Making Height Equal in About Page */
    if($('.tc-ibin').length){
		$.fn.setAllToMaxHeight = function(){
			return this.height( Math.max.apply(this, $.map( this , function(e){ return $(e).height() }) ) );
		}
		$('.tc-ibin').setAllToMaxHeight();
	}
	/* For Making Height Equal in About Page */
	
	
	/* Image Height Assign to Div Height For Blog */
	if($('.imgHeight').length){
		if($('.subwrap').length){
			$('.subwrap').css('min-height',($('.imgHeight').height()-100) + 'px');	
		}
	}
	/* Image Height Assign to Div Height For Blog */
	
	/* For Making Height Equal in Fourth Section Home Page */
	if($('.sl-left').length){
		if($('.sec-last-img').length){
			if($('.sl-left').height() > $('.sec-last-img').height()){
				if($('.sec-last-grow').length){
					$('.sec-last-grow').css('min-height',$('.sl-left').height() + 'px');
				}
			}
		}
	}
	/* For Making Height Equal in Fourth Section Home Page */
	
	
});


</script>


<!-- For Menu Starts -->

<!--[if IE 9]>
    <script src="js/matchMedia.ie9.js"></script>
<![endif]-->
<!--[if LTE IE 8]>
    <script src="js/matchMedia.ie8.js"></script>
<![endif]-->

<link href="<?php echo base_url(); ?>css/jquery.fs.naver.css" rel="stylesheet" type="text/css" media="all" />
<script src="<?php echo base_url(); ?>js/jquery.fs.naver.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
	$(".naver a").on("click", function(e) {
		//e.preventDefault();
		//e.stopPropagation();
	});	
	
	$(".naver").naver({
		animated: true
	});
});
</script>

<!-- For Menu Ends -->
